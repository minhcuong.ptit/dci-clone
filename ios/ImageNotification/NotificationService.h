//
//  NotificationService.h
//  ImageNotification
//
//  Created by Nguyen Kiem Tan on 22/04/2022.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
