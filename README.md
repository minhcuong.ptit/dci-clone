# DCI Mobile Apps

Developed with Flutter.

## Prerequisite
* FlutterSDK (https://docs.flutter.dev/get-started/install)
* IDE
  * VSCode(with `Flutter` extension)
  * Android Studio/ IntelliJ IDEA (with `Flutter` plugin)


## Tech Stack
* Flutter
* RXDart
* BLoC
* GraphQL
* PubNub
* Firebase
* I18n

## Setting
### Local environment

To install the dependency packages

```
flutter pub get
```
----------


To generate network boiler-plates after changes, please run:

```bash
flutter pub run build_runner build --delete-conflicting-outputs
```

We use command-line argument `DCI_ENV` with valid values of [STAGING, HOTFIX, PRODUCTION] to reuse the same source code for all environments. Please use `--dart-define=...` to pass the environment value. The default is STAGING.


## Deployment
### Continuous Delivery

With `fastlane`, best practice is to have `bundle` as Ruby gem in your local environment.

To install:

```bash
bundle install
```

To build APK:

```bash
flutter build apk --split-per-abi --no-tree-shake-icons
```

To build AAB:

```bash
flutter build appbundle --no-tree-shake-icons
```

To build IPA:

```bash
flutter build ipa --no-tree-shake-icons --export-options-plist=fastlane/ExportOptions.plist
```

In order to build for production environment, append these to any of above command:

```bash
--dart-define="DCI_ENV=PRODUCTION"
```

To upload to Google Play:

```bash
bundle exec fastlane android uploadToGooglePlay
```

To upload to App Store:

```bash
bundle exec fastlane ios uploadToAppStore
```

### Troubleshooting
1. Upload App Store

If you can not connect to fastlane, let's deploy manual by Xcode, details here
https://page.investidea.tech/display/DCI/Deployment+process#Deploymentprocess-iOS

## Copyright

InvestIdea Tech 2022
