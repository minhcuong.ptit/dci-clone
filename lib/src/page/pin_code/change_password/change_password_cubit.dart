import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../../res/R.dart';
import '../../../data/network/request/update_password_request.dart';
import '../../../data/network/service/api_result.dart';
import '../../../data/network/service/network_exceptions.dart';
import '../../../data/preferences/app_preferences.dart';
import '../../../utils/const.dart';
import 'change_password.dart';

class ChangePasswordCubit extends Cubit<ChangePasswordState> with Validators {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;

  String? oldPasswordError;
  String? newPasswordError;
  String? confirmNewPasswordError;
  String? get uuid => appPreferences.getString(Const.ID);
  ChangePasswordCubit(
      {required this.appRepository, required this.graphqlRepository})
      : super(ChangePasswordInitial());

  void updatePinCode(String oldPin, String pin, String repeatedPin) async {
    if (state is ChangePasswordLoading) {
      return;
    }
    emit(ChangePasswordLoading());
    oldPasswordError = checkPin(oldPin);
    newPasswordError = checkPin(pin);
    confirmNewPasswordError = checkPin(repeatedPin);
    if (!Utils.isEmpty(oldPasswordError)) {
      emit(ChangePasswordValidation());
      return;
    }
    if (!Utils.isEmpty(newPasswordError)) {
      emit(ChangePasswordValidation());
      return;
    }
    if (!Utils.isEmpty(confirmNewPasswordError)) {
      emit(ChangePasswordValidation());
      return;
    }
    if (pin != repeatedPin) {
      confirmNewPasswordError = R.string.set_pin_repeat_not_match.tr();
      emit(ChangePasswordValidation());
      return;
    }

    ChangePasswordRequest request =
        ChangePasswordRequest(oldPinCode: oldPin, newPinCode: pin);
    ApiResult<dynamic> apiResult =
        await appRepository.changePinCode(uuid ?? '', request);
    apiResult.when(success: (data) {
      emit(ChangePasswordSuccess());
    }, failure: (NetworkExceptions error) {
      if (error is UnauthorizedRequest) {
        this.oldPasswordError = R.string.set_pin_not_match.tr();
      } else {
        var _error = NetworkExceptions.getErrorMessage(error);
        this.oldPasswordError = R.string.set_pin_not_match.tr();
      }
      emit(ChangePasswordFailure());
    });
  }

  void validateOldPin(String oldPinCode) {
    emit(ChangePasswordInitial());
    // reset errors
    oldPasswordError = null;
    oldPasswordError = checkPin(oldPinCode);
    //var pinCode = appPreferences.getString(Const.PIN_CODE);
    // if (oldPasswordError == null) {
    //   if (oldPinCode.isNotEmpty && pinCode != oldPinCode) {
    //     oldPasswordError = R.string.set_pin_not_match.tr();
    //   }
    // }
    emit(ChangePasswordValidation());
  }

  void validateNewPin(String pinCode) {
    emit(ChangePasswordInitial());
    newPasswordError = null;
    newPasswordError = checkPin(pinCode);
    emit(ChangePasswordValidation());
  }

  void validateConfirmNewPin(String pinCode, String repeatedPin) {
    emit(ChangePasswordInitial());
    confirmNewPasswordError = null;

    confirmNewPasswordError = checkPin(repeatedPin);
    if (newPasswordError == null && confirmNewPasswordError == null) {
      if (repeatedPin.isNotEmpty && pinCode != repeatedPin) {
        confirmNewPasswordError = R.string.set_confirm_pin_not_match.tr();
      }
    }
    emit(ChangePasswordValidation());
  }
}

AppPreferences appPreferences = AppPreferences();
