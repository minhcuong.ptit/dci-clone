import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/more/setting_account/setting_account_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/dci_event_handle.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';
import '../../../widgets/custom_appbar.dart';
import 'change_password.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  late ChangePasswordCubit _cubit;

  late TextEditingController _oldPinController;
  late TextEditingController _pinController;
  late TextEditingController _repeatPinController;

  @override
  void initState() {
    AppRepository appRepository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ChangePasswordCubit(
        appRepository: appRepository, graphqlRepository: graphqlRepository);
    _oldPinController = TextEditingController();
    _pinController = TextEditingController();
    _repeatPinController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _oldPinController.dispose();
    _pinController.dispose();
    _repeatPinController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white,
      appBar: appBar(context, R.string.change_password.tr().toUpperCase(),
          textStyle: Theme.of(context)
              .textTheme
              .bodyBold
              .apply(color: R.color.black)
              .copyWith(
                  fontSize: 14.sp, fontWeight: FontWeight.w700, height: 1.7),
          centerTitle: true),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<ChangePasswordCubit, ChangePasswordState>(
          listener: (context, state) {
            if (state is ChangePasswordSuccess) {
              NavigationUtils.pop(context);
              Utils.showToast(context, R.string.change_password_success.tr());
            }
            if (state is SettingAccountFailure) {
              NavigationUtils.pop(context);
              Utils.showToast(context, R.string.change_password_failure.tr());
            }
          },
          builder: (
            BuildContext context,
            ChangePasswordState state,
          ) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, ChangePasswordState state) {
    return KeyboardVisibilityBuilder(
      builder: (context, isKeyboardVisible) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: StackLoadingView(
            visibleLoading: state is ChangePasswordLoading,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 24.w),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 20.h),
                        Container(
                          padding: EdgeInsets.all(16.0),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border.all(color: R.color.white, width: 1),
                            borderRadius: BorderRadius.circular(8),
                            color: R.color.white,
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 8.h),
                                buildEnterOldPassword(context),
                                SizedBox(height: 8.h),
                                buildEnterNewPassword(context),
                                SizedBox(height: 12.h),
                                buildEnterConfirmNewPassword(context),
                              ]),
                        ),
                        SizedBox(
                          height: 54.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 18.w),
                          child: ButtonWidget(
                            height: 48.h,
                            textSize: 16.sp,
                            title: R.string.save_changes.tr(),
                            onPressed: _oldPinController.text.length >= 6 &&
                                    _pinController.text.length >= 6 &&
                                    _repeatPinController.text.length >= 6
                                ? () {
                                    _cubit.updatePinCode(
                                        _oldPinController.text,
                                        _pinController.text,
                                        _repeatPinController.text);
                                  }
                                : () {},
                            backgroundColor: _pinController.text.length >= 6 &&
                                    _repeatPinController.text.length >= 6 &&
                                    _oldPinController.text.length >= 6
                                ? R.color.orange
                                : R.color.lightGray,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildEnterOldPassword(BuildContext context) {
    return TextFieldWidget(
        autoFocus: true,
        isPassword: true,
        titleText: R.string.enter_your_old_password.tr(),
        controller: _oldPinController,
        hintText: R.string.enter_your_old_password_hint.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          LengthLimitingTextInputFormatter(20),
        ],
        keyboardType: TextInputType.text,
        onChanged: (_) {
          _cubit.validateOldPin(_oldPinController.text);
        },
        errorText: _cubit.oldPasswordError,
        customInputStyle: Theme.of(context).textTheme.bodyBold);
  }

  Widget buildEnterNewPassword(BuildContext context) {
    return TextFieldWidget(
        autoFocus: true,
        isPassword: true,
        titleText: R.string.enter_your_new_password.tr(),
        controller: _pinController,
        hintText: R.string.enter_your_new_password_hint.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          LengthLimitingTextInputFormatter(20),
        ],
        keyboardType: TextInputType.text,
        onChanged: (_) {
          _cubit.validateNewPin(_pinController.text);
        },
        errorText: _cubit.newPasswordError,
        customInputStyle: Theme.of(context).textTheme.bodyBold);
  }

  Widget buildEnterConfirmNewPassword(BuildContext context) {
    return TextFieldWidget(
        autoFocus: true,
        isPassword: true,
        titleText: R.string.enter_confirm_new_password.tr(),
        controller: _repeatPinController,
        hintText: R.string.enter_confirm_new_password_hint.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          LengthLimitingTextInputFormatter(20),
        ],
        keyboardType: TextInputType.text,
        onChanged: (_) {
          _cubit.validateConfirmNewPin(
              _pinController.text, _repeatPinController.text);
        },
        errorText: _cubit.confirmNewPasswordError,
        customInputStyle: Theme.of(context).textTheme.bodyBold);
  }
}
