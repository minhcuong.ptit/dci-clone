import 'package:equatable/equatable.dart';

abstract class ChangePasswordState extends Equatable {
  ChangePasswordState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class ChangePasswordInitial extends ChangePasswordState {
  @override
  String toString() => 'ChangePasswordInitial';
}

class ChangePasswordLoading extends ChangePasswordState {
  @override
  String toString() => 'ChangePasswordLoading';
}

class ChangePasswordFailure extends ChangePasswordState {
  @override
  String toString() => 'ChangePasswordFailure';
}

class ChangePasswordValidation extends ChangePasswordState {
  @override
  String toString() => 'ChangePasswordValidation';
}

class ChangePasswordSuccess extends ChangePasswordState {
  @override
  String toString() => 'ChangePasswordSuccess';
}
