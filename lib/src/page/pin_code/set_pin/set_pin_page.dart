import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/validators.dart';
import 'package:imi/src/widgets/background_page.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

import '../../../widgets/custom_appbar.dart';
import '../../authentication/authentication.dart';
import 'set_pin.dart';

enum SetPinType {
  FORGOT_PIN,
  CHANGE_PIN,
}

extension SetPinTypeExt on SetPinType {
  String get title {
    switch (this) {
      case SetPinType.FORGOT_PIN:
        return R.string.forgot_pin.tr();
      case SetPinType.CHANGE_PIN:
        return R.string.set_pin.tr();
    }
  }
}

class SetPinPage extends StatefulWidget {
  final SetPinType type;

  const SetPinPage({Key? key, required this.type}) : super(key: key);

  @override
  _SetPinPageState createState() => _SetPinPageState();
}

class _SetPinPageState extends State<SetPinPage> {
  late SetPinCubit _cubit;
  late AuthenticationCubit _authCubit;

  late TextEditingController _pinController;
  late TextEditingController _repeatPinController;

  @override
  void initState() {
    AppRepository appRepository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = SetPinCubit(
        appRepository: appRepository, graphqlRepository: graphqlRepository);
    _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    _pinController = TextEditingController();
    _repeatPinController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _pinController.dispose();
    _repeatPinController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<SetPinCubit, SetPinState>(
        listener: (context, state) {
          if (state is SetPinSuccess) {
            switch (widget.type) {
              case SetPinType.FORGOT_PIN:
                NavigationUtils.popToFirst(context);
                _authCubit.welcomeToLogin();
                return;
              case SetPinType.CHANGE_PIN:
                NavigationUtils.pop(context);
                return;
            }
          }
        },
        builder: (
          BuildContext context,
          SetPinState state,
        ) {
          return buildPage(context, state);
        },
      ),
    );
  }

  Widget buildPage(BuildContext context, SetPinState state) {
    return KeyboardVisibilityBuilder(
      builder: (context, isKeyboardVisible) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: StackLoadingView(
            visibleLoading: state is SetPinLoading,
            child: BackgroundPage(
              background: R.drawable.bg_welcome,
              child: Scaffold(
                backgroundColor: R.color.black.withOpacity(0.4),
                body: Stack(
                  children: [
                    SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 300.h,
                            ),
                            Text(
                              R.string.reset_password.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .apply(color: R.color.white)
                                  .copyWith(
                                    fontSize: 24.sp,
                                  ),
                            ),
                            SizedBox(height: 20.h),
                            Container(
                              padding: EdgeInsets.all(16.0),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: R.color.white, width: 1),
                                borderRadius: BorderRadius.circular(8),
                                color: R.color.white,
                              ),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 8.h),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        R.string.reset_password_description
                                            .tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .tooltip
                                            .copyWith(height: 14 / 32),
                                        textAlign: TextAlign.center,
                                        maxLines: 2,
                                      ),
                                    ),
                                    SizedBox(height: 8.h),
                                    buildEnterNewOTP(context),
                                    SizedBox(height: 12.h),
                                    buildEnterConfirmOTP(context),
                                  ]),
                            ),
                            SizedBox(
                              height: 54.h,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 18.w),
                              child: ButtonWidget(
                                height: 48.h,
                                textSize: 16.sp,
                                title: R.string.confirm.tr(),
                                uppercaseTitle: true,
                                onPressed: _pinController.text.length >= 6 &&
                                        _repeatPinController.text.length >= 6
                                    ? () {
                                        _cubit.updatePinCode(
                                            _pinController.text,
                                            _repeatPinController.text);
                                      }
                                    : () {},
                                backgroundColor: _pinController.text.length >=
                                            6 &&
                                        _repeatPinController.text.length >= 6
                                    ? R.color.secondaryButtonColor
                                    : R.color.lightGray,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                        top: MediaQuery.of(context).padding.top,
                        left: 16.w,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: R.color.white,
                          ),
                          onPressed: () {
                            NavigationUtils.pop(context);
                          },
                        )),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildEnterNewOTP(BuildContext context) {
    return TextFieldWidget(
        autoFocus: true,
        isPassword: true,
        titleText: R.string.please_enter_new_password.tr(),
        controller: _pinController,
        hintText: R.string.please_enter_new_password.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          FilteringTextInputFormatter.allow(
              Validators.defaultAlphanumericRegex),
          LengthLimitingTextInputFormatter(20),
        ],
        keyboardType: TextInputType.text,
        onChanged: _onChange,
        customInputStyle: Theme.of(context).textTheme.bodySmallText);
  }

  Widget buildEnterConfirmOTP(BuildContext context) {
    return TextFieldWidget(
        autoFocus: true,
        isPassword: true,
        titleText: R.string.please_enter_confirm_password.tr(),
        controller: _repeatPinController,
        hintText: R.string.please_enter_confirm_password.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          FilteringTextInputFormatter.allow(
              Validators.defaultAlphanumericRegex),
          LengthLimitingTextInputFormatter(20),
        ],
        keyboardType: TextInputType.text,
        onChanged: _onChange,
        errorText: _cubit.error,
        errorTextStyle: Theme.of(context).textTheme.bodySmallText.copyWith(
          color: R.color.red,fontSize: 12.sp
        ),
        customInputStyle: Theme.of(context).textTheme.bodySmallText);
  }

  void _onChange(_) {
    _cubit.validatePin(_pinController.text, _repeatPinController.text);
  }
}
