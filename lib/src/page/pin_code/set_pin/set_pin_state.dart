import 'package:equatable/equatable.dart';

abstract class SetPinState extends Equatable {
  SetPinState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class SetPinInitial extends SetPinState {
  @override
  String toString() => 'SetPinInitial';
}

class SetPinLoading extends SetPinState {
  @override
  String toString() => 'SetPinLoading';
}

class SetPinFailure extends SetPinState {
  @override
  String toString() => 'SetPinFailure';
}

class SetPinValidation extends SetPinState {
  @override
  String toString() => 'SetPinValidation';
}

class SetPinSuccess extends SetPinState {
  @override
  String toString() => 'SetPinSuccess';
}
