import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/update_profile_request_v2.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../../res/R.dart';
import '../../../data/network/service/api_result.dart';
import '../../../data/network/service/network_exceptions.dart';
import 'set_pin.dart';

class SetPinCubit extends Cubit<SetPinState> with Validators {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;

  String? error;

  SetPinCubit({required this.appRepository, required this.graphqlRepository})
      : super(SetPinInitial());

  void updatePinCode(String pin, String repeatedPin) async {
    if (state is SetPinLoading) {
      return;
    }
    emit(SetPinLoading());
    error = checkPin(pin);
    error = checkPin(repeatedPin);
    if (!Utils.isEmpty(error)) {
      emit(SetPinValidation());
      return;
    }
    if (!Utils.isEmpty(error)) {
      emit(SetPinValidation());
      return;
    }
    if (pin != repeatedPin) {
      error = R.string.set_pin_repeat_not_match.tr();
      emit(SetPinValidation());
      return;
    }
    UpdateProfileRequestV2 request = UpdateProfileRequestV2(pinCode: pin);
    ApiResult<dynamic> apiResult = await appRepository.updateProfile(request);
    apiResult.when(success: (data) {
      emit(SetPinSuccess());
    }, failure: (NetworkExceptions error) {
      if (error is UnauthorizedRequest) {
        this.error = R.string.verify_phone_number_otp_wrong.tr();
      } else {
        this.error = NetworkExceptions.getErrorMessage(error);
      }
      emit(SetPinFailure());
    });
  }

  void validatePin(String pinCode, String repeatedPin) {
    emit(SetPinInitial());
    // reset errors
    error = null;

    error = checkPin(pinCode);
    if (error == null) {
      if (repeatedPin.isNotEmpty && pinCode != repeatedPin) {
        error = R.string.set_pin_repeat_not_match.tr();
      }
    }
    emit(SetPinValidation());
  }
}
