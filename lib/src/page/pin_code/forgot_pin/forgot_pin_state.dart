import 'package:equatable/equatable.dart';
import 'package:imi/src/utils/enum.dart';

abstract class ForgotPinState extends Equatable {
  ForgotPinState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class ForgotPinInitial extends ForgotPinState {
  @override
  String toString() => 'ForgotPinInitial';
}

class ForgotPinLoading extends ForgotPinState {
  @override
  String toString() => 'ForgotPinLoading';
}

class ForgotPinPendingOTP extends ForgotPinState {
  final int remainingTime;

  ForgotPinPendingOTP({required this.remainingTime});

  @override
  String toString() => 'ForgotPinPendingOTP { remainingTime: $remainingTime }';

  @override
  List<Object> get props => [remainingTime];
}

class ForgotPinFailure extends ForgotPinState {
  final String error;

  ForgotPinFailure(this.error);

  @override
  String toString() => 'ForgotPinFailure { error: $error }';
}

class ForgotPinValidation extends ForgotPinState {
  @override
  String toString() => 'ForgotPinValidation';
}

class ForgotPinLoginSuccess extends ForgotPinState {
  final PersonProfileStatus status;

  ForgotPinLoginSuccess(this.status);

  @override
  String toString() => 'ForgotPinLoginSuccess { $status }';

  @override
  List<Object> get props => [status];
}
class ExistPhoneSuccess extends ForgotPinState {
  @override
  String toString() => 'ExistPhoneSuccess';
}

class ExistPhone extends ForgotPinState {
  @override
  String toString() => 'ExistPhone';
}
