import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/auth_user/sign_up/sign_up.dart';
import 'package:imi/src/page/pin_code/set_pin/set_pin.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/validators.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/search_list_nationality_popup.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

import '../../../utils/const.dart';
import '../../../utils/utils.dart';
import '../../../widgets/custom_appbar.dart';
import '../../authentication/authentication_cubit.dart';
import 'forgot_pin.dart';

class ForgotPinPage extends StatefulWidget {
  final ForgotPinType type;

  const ForgotPinPage({Key? key, required this.type}) : super(key: key);

  @override
  _ForgotPinPageState createState() => _ForgotPinPageState();
}

class _ForgotPinPageState extends State<ForgotPinPage> {
  late ForgotPinCubit _cubit;
  late AuthenticationCubit _authCubit;

  late TextEditingController _phoneController;
  late TextEditingController _otpController;

  @override
  void initState() {
    AppRepository appRepository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ForgotPinCubit(
        appRepository: appRepository, graphqlRepository: graphqlRepository);
    _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    _phoneController = TextEditingController();
    _otpController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBar(
          context,
          widget.type == ForgotPinType.SIGN_UP
              ? R.string.sign_up.tr()
              : R.string.forgot_pin.tr(),
          textStyle: Theme.of(context)
              .textTheme
              .subHeading1
              .apply(color: R.color.white)
              .copyWith(fontSize: 20.sp, fontWeight: FontWeight.w700),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconColor: R.color.white),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<ForgotPinCubit, ForgotPinState>(
          listener: (context, state) {
            if (state is ForgotPinFailure) {
              Utils.showSnackBar(context, state.error);
            }
            if (state is ExistPhoneSuccess) {
              if (_cubit.isWaitingOtp) {
                if (_otpController.text.isNotEmpty) {
                  _cubit.verifyOtp(
                      _phoneController.text.trim(), _otpController.text.trim());
                }
              } else if (_cubit.errorPhoneNumber == null &&
                  _phoneController.text.isNotEmpty) {
                _cubit.submitOtp(_phoneController.text);
              }
            }
            if (state is ForgotPinLoginSuccess) {
              switch (state.status) {
                case PersonProfileStatus.INIT:
                  if (widget.type == ForgotPinType.SIGN_UP) {
                    NavigationUtils.navigatePage(
                        context,
                        BlocProvider.value(
                          value: BlocProvider.of<AuthenticationCubit>(context),
                          child: SignUpPage(),
                        ));
                  } else {
                    NavigationUtils.replacePage(
                        context,
                        BlocProvider.value(
                          value: BlocProvider.of<AuthenticationCubit>(context),
                          child: SignUpPage(),
                        ));
                  }
                  return;
                case PersonProfileStatus.REGISTERED:
                  switch (widget.type) {
                    case ForgotPinType.SIGN_UP:
                      NavigationUtils.popToFirst(context);
                      _authCubit.login();
                      return;
                    case ForgotPinType.FORGOT_PIN:
                      NavigationUtils.navigatePage(
                        context,
                        BlocProvider.value(
                            value:
                                BlocProvider.of<AuthenticationCubit>(context),
                            child: SetPinPage(type: SetPinType.FORGOT_PIN)),
                      );
                      return;
                  }
              }
            }
          },
          builder: (
            BuildContext context,
            ForgotPinState state,
          ) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, ForgotPinState state) {
    double height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        SingleChildScrollView(
          child: Image.asset(
            widget.type == ForgotPinType.SIGN_UP
                ? R.drawable.bg_sign_up
                : R.drawable.bg_welcome,
            height: height,
            fit: BoxFit.cover,
          ),
        ),
        KeyboardVisibilityBuilder(
          builder: (context, isKeyboardVisible) {
            return GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: StackLoadingView(
                visibleLoading: state is ForgotPinLoading,
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 24.w),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 330.h,
                        ),
                        SizedBox(height: 20.h),
                        Container(
                          padding: EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: R.color.white, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: R.color.white,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 8.h),
                              if (ForgotPinType.FORGOT_PIN == widget.type) ...[
                                Text(
                                  !_cubit.isWaitingOtp
                                      ? R.string
                                          .enter_phone_number_to_reset_password
                                          .tr()
                                      : R.string.enter_otp.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodySmallText
                                      .copyWith(fontSize: 13),
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                ),
                                SizedBox(height: 8.h),
                              ],
                              Text(
                                R.string.phone_number.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .buttonSmall
                                    .copyWith(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500,
                                        color: R.color.textFieldTitle),
                              ),
                              SizedBox(height: 4.h),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                      child: buildEnterPhoneNumber(context)),
                                  SizedBox(width: 12.w),
                                  Visibility(
                                    visible: _cubit.isWaitingOtp,
                                    child: GestureDetector(
                                      onTap: () {
                                        if (_cubit.isWaitingOtp) {
                                          _otpController.text = "";
                                          _cubit.changePhoneNumber();
                                        }
                                      },
                                      child: Container(
                                          height: 27,
                                          margin: EdgeInsets.only(top: 10.h),
                                          padding: EdgeInsets.only(
                                              left: 12.w, right: 12.w),
                                          decoration: BoxDecoration(
                                            color: _cubit.isWaitingOtp
                                                ? Colors.transparent
                                                : (_cubit.errorPhoneNumber ==
                                                            null &&
                                                        _phoneController
                                                            .text.isNotEmpty
                                                    ? R.color.primaryColor
                                                    : R.color.lighterGray),
                                            borderRadius:
                                                BorderRadius.circular(48.h),
                                            border: Border.all(
                                                width: 1.h,
                                                color: _cubit.isWaitingOtp
                                                    ? R.color.black
                                                    : Colors.transparent),
                                          ),
                                          child: Column(
                                            children: [
                                              Text(
                                                _cubit.isWaitingOtp
                                                    ? R.string.text_change.tr()
                                                    : R.string.text_submit.tr(),
                                                textAlign: TextAlign.center,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .buttonSmall
                                                    .copyWith(
                                                        color:
                                                            _cubit.isWaitingOtp
                                                                ? R.color.black
                                                                : R.color.white,
                                                        fontWeight:
                                                            FontWeight.w500),
                                              ),
                                            ],
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                              Visibility(
                                visible: _cubit.isWaitingOtp,
                                child: Divider(
                                  height: 1,
                                  thickness: 1,
                                ),
                              ),
                              Visibility(
                                visible: !_cubit.isWaitingOtp &&
                                    ForgotPinType.SIGN_UP == widget.type,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 8.h),
                                  child: Text(
                                    R.string.valid_phone_number.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .labelSmallText
                                        .apply(color: R.color.darkGray)
                                        .copyWith(fontSize: 11.sp),
                                  ),
                                ),
                              ),
                              SizedBox(height: 4.h),
                              if (_cubit.isWaitingOtp) ...[
                                SizedBox(height: 12.h),
                                buildEnterOTP(context),
                                SizedBox(height: 4.h),
                              ],
                              if (_cubit.errorApi != null)
                                Text(
                                  _cubit.errorApi ?? "",
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyRegular
                                      .copyWith(
                                          fontSize: 11.sp, color: R.color.red),
                                ),
                              if (_cubit.isWaitingOtp) ...[
                                SizedBox(height: 12.h),
                                Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          if (_cubit.isWaitingOtp &&
                                              _cubit.start == 0) {
                                            _cubit.submitOtp(
                                                _phoneController.text.trim());
                                            _otpController.clear();
                                          }
                                        },
                                        child: Container(
                                          height: 32,
                                          margin: EdgeInsets.only(top: 8.h),
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12.w),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1.h,
                                                color: _cubit.start == 0
                                                    ? R.color.black
                                                    : R.color.lightShadesGray,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(48.h)),
                                          child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Icon(
                                                  Icons.refresh_outlined,
                                                  size: 24,
                                                  color: _cubit.start == 0
                                                      ? R.color.black
                                                      : R.color.lightShadesGray,
                                                ),
                                                SizedBox(width: 4.w),
                                                Text(
                                                  _cubit.start == 0
                                                      ? R.string
                                                          .verify_phone_number_otp_resend
                                                          .tr()
                                                      : R.string
                                                              .verify_phone_number_otp_resend_in
                                                              .tr() +
                                                          " ${_cubit.remainingResendOtpTime}",
                                                  textAlign: TextAlign.center,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .buttonSmall
                                                      .copyWith(
                                                          color: _cubit.start ==
                                                                  0
                                                              ? R.color.black
                                                              : R.color
                                                                  .lightShadesGray,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                ),
                                              ]),
                                        ),
                                      ),
                                    ]),
                              ],
                            ],
                          ),
                        ),
                        SizedBox(height: 20.h),
                        buildContinueButton(state, context),
                        Align(
                          alignment: Alignment.center,
                          child: IntrinsicWidth(
                            child: Text(
                              R.string.want_to_sign_up_for_an_account.tr() +
                                  "  ",
                              style: TextStyle(
                                  color: R.color.white,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14.sp),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: IntrinsicWidth(
                            child: InkWell(
                              onTap: (() {
                                Utils.launchURL(Utils.getTypeUrlLauncher(
                                    Const.SEE_THE_INSTRUCTIONS,
                                    Const.LAUNCH_TYPE_WEB));
                              }),
                              child: Text(
                                R.string.see_the_instructions.tr(),
                                style: TextStyle(
                                    color: R.color.orange,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14.sp,
                                height: 20/14),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }

  Widget buildContinueButton(ForgotPinState state, BuildContext context) {
    var _isEnableButton = false;
    var bgColor = R.color.lighterGray;
    if (!_cubit.isWaitingOtp) {
      _isEnableButton = validatePhoneNumber(_phoneController.text);
    } else {
      _isEnableButton = _otpController.text.isNotEmpty;
    }

    bgColor =
        _isEnableButton ? R.color.secondaryButtonColor : R.color.btnDisable;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      child: ButtonWidget(
          title: R.string.confirm.tr(),
          backgroundColor: bgColor,
          textSize: 16.sp,
          height: 48.h,
          uppercaseTitle: true,
          onPressed: () {
            if (_isEnableButton) {
              if (ForgotPinType.FORGOT_PIN == widget.type) {
                _cubit.checkPhoneForgot(_phoneController.text.trim());
              } else {
                if (_otpController.text.isNotEmpty) {
                  _cubit.verifyOtp(
                      _phoneController.text.trim(), _otpController.text.trim());
                } else if (_cubit.errorPhoneNumber == null &&
                    _phoneController.text.isNotEmpty) {
                  _cubit.submitOtp(_phoneController.text);
                }
              }
            }
          }),
    );
  }

  Widget buildEnterPhoneNumber(BuildContext context) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _phoneController,
      hintText: R.string.text_field_hint.tr(),
      textInputAction: TextInputAction.done,
      inputFormatters: [
        FilteringTextInputFormatter.allow(Validators.numberRegex),
        LengthLimitingTextInputFormatter(13),
      ],
      isEnable: !_cubit.isWaitingOtp,
      fillColor: R.color.white,
      errorText: _cubit.errorPhoneNumber,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 12),
      keyboardType: TextInputType.phone,
      icon: countryDropDown(),
      onChanged: _cubit.validatePhone,
      onSubmitted: (str) {
        if (validatePhoneNumber(str)) {
          if (ForgotPinType.FORGOT_PIN == widget.type) {
            _cubit.checkPhoneForgot(str ?? "");
          } else {
            _cubit.submitOtp(str ?? "");
          }
        }
        ;
      },
    );
  }

  bool validatePhoneNumber(str) {
    var _isEnableButton =
        _phoneController.text.isNotEmpty && _phoneController.text.length > 8;
    return _isEnableButton;
  }

  Widget buildEnterOTP(BuildContext context) {
    return TextFieldWidget(
        autoFocus: false,
        titleText: R.string.verify_phone_number_otp_hint_short.tr(),
        controller: _otpController,
        hintText: R.string.verify_phone_number_otp_hint.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          FilteringTextInputFormatter.allow(Validators.numberRegex),
          LengthLimitingTextInputFormatter(6),
        ],
        keyboardType: TextInputType.number,
        onChanged: _cubit.validateOtp,
        errorText: _cubit.errorOtp,
        customInputStyle: Theme.of(context).textTheme.bodySmallText);
  }

  Widget countryDropDown() {
    return GestureDetector(
        onTap: () {
          if (!_cubit.isWaitingOtp) {
            showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                backgroundColor: Colors.transparent,
                builder: (context) {
                  return SearchListNationalityPopup(
                    listData: _cubit.nationalities
                        .map((e) => "(${e.phonePrefix}) - ${e.name}")
                        .toList(),
                    hintText: R.string.search.tr(),
                    defaultIndex: _cubit.nationalities.indexWhere(
                        (element) => element == _cubit.selectedCountry),
                    onSelect: (int index) {
                      _cubit.selectCountryCode(_cubit.nationalities[index]);
                    },
                  );
                });
          }
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          padding: EdgeInsets.only(left: 0, right: 0, top: 8.h, bottom: 5.h),
          margin: EdgeInsets.zero,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(width: 4.w),
              Text(_cubit.selectedCountry?.phonePrefix ?? "",
                  style: Theme.of(context).textTheme.bodySmallText),
              SizedBox(width: 8.w),
              Container(
                margin: EdgeInsets.only(top: 3),
                color: R.color.lightShadesGray,
                width: 1.w,
                height: 18.h,
              ),
              SizedBox(width: 8.w),
            ],
          ),
        ));
  }
}
