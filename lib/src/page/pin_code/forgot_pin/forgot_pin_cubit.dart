import 'dart:async';

import 'package:collection/src/iterable_extensions.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/login_request.dart';
import 'package:imi/src/data/network/response/nationality_response.dart';
import 'package:imi/src/data/network/response/person_profile.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../data/network/repository/app_graphql_repository.dart';
import '../../../data/network/response/country.dart';
import '../../../data/network/response/login_response.dart';
import 'forgot_pin.dart';

class ForgotPinCubit extends Cubit<ForgotPinState> with Validators {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  List<Country> nationalities = [];
  Country? selectedCountry;

  String? errorPhoneNumber;
  String? errorOtp;
  String? errorApi;

  Timer? _timer;
  int start = 0;
  bool isWaitingOtp = false;

  String get remainingResendOtpTime => DateFormat("mm:ss")
      .format(DateTime.fromMillisecondsSinceEpoch(start * 1000));

  ForgotPinCubit({required this.appRepository, required this.graphqlRepository})
      : super(ForgotPinInitial()) {
    getNationality();
  }

  void getNationality() async {
    emit(ForgotPinLoading());
    try {
      ApiResult<List<Country>> apiResult = await appRepository.getCountries();
      apiResult.when(success: (List<Country> data) async {
        nationalities = data;
        selectedCountry = nationalities.firstWhereOrNull(
            (element) => element.phonePrefix == Const.DEFAULT_COUNTRY_CODE);
      }, failure: (NetworkExceptions error) async {
        emit(ForgotPinFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } catch (e) {
      logger.e(e);
    }
    emit(ForgotPinInitial());
  }

  void selectCountryCode(Country country) {
    if (!isWaitingOtp) {
      emit(ForgotPinLoading());
      this.selectedCountry = country;
      emit(ForgotPinInitial());
    }
  }

  void changePhoneNumber() {
    isWaitingOtp = false;
    errorApi = null;
    _timer?.cancel();
    emit(ForgotPinInitial());
  }

  void submitOtp(String? phone) async {
    _timer?.cancel();
    String? countryCode = selectedCountry?.phonePrefix;
    if (countryCode == null) {
      return;
    }
    emit(ForgotPinInitial());
    errorPhoneNumber = checkPhoneNumber(phone);
    emit(ForgotPinValidation());
    if (errorPhoneNumber != null) {
      return;
    }

    errorApi = null;
    emit(ForgotPinLoading());
    phone = Utils.getPhoneNumber(phone, countryCode);
    OtpChannel selectedChannel = OtpChannel.SMS;
    ApiResult<dynamic> apiResult = await appRepository
        .auth(LoginRequest(phone: phone, otpChannel: selectedChannel));
    apiResult.when(success: (dynamic data) {
      isWaitingOtp = true;
      emit(ForgotPinPendingOTP(remainingTime: 60));
      _countdown();
    }, failure: (NetworkExceptions error) {
      if (error is UnauthorizedRequest &&
          error.code == ServerError.user_is_banned_error) {
        errorApi = R.string.ban_description.tr(args: [error.reason ?? ""]);
      } else
        errorApi = NetworkExceptions.getErrorMessage(error);
      emit(ForgotPinInitial());
    });
  }

  void checkPhoneForgot(String phone) async {
    emit(ForgotPinLoading());
    phone = Utils.getPhoneNumber(phone, selectedCountry?.phonePrefix ?? "");
    ApiResult<PersonProfile> apiResult =
        await graphqlRepository.getExitProfilePhone(phone.replaceAll("+", ""));
    apiResult.when(success: (PersonProfile data) {
      emit(ExistPhoneSuccess());
    }, failure: (NetworkExceptions error) {
      errorPhoneNumber = "Số điện thoại không tồn tại";
      emit(ExistPhone());
    });
  }

  void verifyOtp(String phone, String? otp) async {
    if (state is ForgotPinLoading) {
      return;
    }
    emit(ForgotPinLoading());
    errorPhoneNumber = checkPhoneNumber(phone);
    errorOtp = null;
    errorApi = null;
    if (!Utils.isEmpty(errorPhoneNumber)) {
      emit(ForgotPinValidation());
      return;
    }
    if (!Utils.isEmpty(errorOtp)) {
      emit(ForgotPinValidation());
      return;
    }
    phone = Utils.getPhoneNumber(phone, selectedCountry?.phonePrefix ?? "");
    ApiResult<dynamic> apiResult = await appRepository
        .auth(LoginRequest(phone: phone.replaceAll("+", ""), otp: otp));
    apiResult.when(success: (dynamic data) {
      LoginResponse response = LoginResponse.fromJson(data);
      appPreferences.setLoginResponse(response);
      appPreferences.setData(Const.PHONE, phone);
      _getProfile();
    }, failure: (NetworkExceptions error) {
      if (error is UnauthorizedRequest) {
        errorApi = R.string.verify_phone_number_otp_wrong.tr();
      } else {
        errorApi = NetworkExceptions.getErrorMessage(error);
      }
      emit(ForgotPinInitial());
    });
  }

  void validatePhone(String? phone) {
    String? countryCode = selectedCountry?.phonePrefix;
    if (countryCode == null) {
      return;
    }
    emit(ForgotPinInitial());
    if (Utils.isEmpty(phone)) {
      errorPhoneNumber = R.string.please_enter_phone_number.tr();
    } else {
      phone = Utils.getPhoneNumber(phone, countryCode);
      errorPhoneNumber = checkPhoneNumber(phone);
    }
    errorApi = null;
    emit(ForgotPinValidation());
  }

  void validateOtp(String? otp) {
    emit(ForgotPinInitial());
    errorOtp = null;
    errorApi = null;
    emit(ForgotPinValidation());
  }

  void logout() {
    emit(ForgotPinLoading());
    appPreferences.clearData();
  }

  void _countdown() {
    start = 60;
    _timer = new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) {
        if (start == 0) {
          timer.cancel();
        } else {
          start--;
        }
        bool isLoading = state is ForgotPinLoading;
        emit(ForgotPinPendingOTP(remainingTime: start));
        if (isLoading) {
          emit(ForgotPinLoading());
        }
      },
    );
  }

  void _getProfile() async {
    ApiResult<PersonProfile> apiResult =
        await graphqlRepository.getMyPersonalProfile();
    apiResult.when(success: (PersonProfile data) {
      appPreferences.setPersonProfile(data);
      _timer?.cancel();
      emit(ForgotPinLoginSuccess(data.status));
    }, failure: (NetworkExceptions error) {
      appPreferences.clearData();
      if (error is UnauthorizedRequest) {
        errorApi = R.string.verify_phone_number_otp_wrong.tr();
      } else {
        errorApi = NetworkExceptions.getErrorMessage(error);
      }
      emit(ForgotPinInitial());
    });
  }
}
