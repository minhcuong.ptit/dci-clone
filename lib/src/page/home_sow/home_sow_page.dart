import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/clean_seed_history_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/response/plan_seed_response.dart';
import 'package:imi/src/page/clean_seeds/clean_seeds_page.dart';
import 'package:imi/src/page/detail_tutorial_snow/detail_tutorial_snow_page.dart';
import 'package:imi/src/page/home_sow/home_sow.dart';
import 'package:imi/src/page/home_sow/home_sow_cubit.dart';
import 'package:imi/src/page/new_sowing_seeds/new_sowing_seeds.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/time_ago.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeSowPage extends StatefulWidget {
  final bool? directFromGuidePoint;
  const HomeSowPage({Key? key,this.directFromGuidePoint}) : super(key: key);

  @override
  State<HomeSowPage> createState() => _HomeSowPageState();
}

class _HomeSowPageState extends State<HomeSowPage>
    with TickerProviderStateMixin {
  late TabController _tabController;
  RefreshController _refreshController = RefreshController();
  late HomeSowCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HomeSowCubit(graphqlRepository, repository);
    _tabController = new TabController(vsync: this, length: 2);
    if(widget.directFromGuidePoint??false){
      _tabController.index=1;
    }
    _cubit.getListPlanSeed();
    _cubit.getListTutorial();
    _cubit.getListCLeanSeed();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<HomeSowCubit, HomeSowState>(
          listener: (context, state) {
            if (state is! HomeSowLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
            // TODO: implement listener
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  void _onRefresh() {
    if (_tabController.index == 0) {
      _cubit.getListPlanSeed(isRefresh: true);
      _cubit.getListTutorial(isRefresh: true);
    } else if (_tabController.index == 1) {
      _cubit.getListCLeanSeed(isRefresh: true);
      _cubit.getListTutorial(isLoadMore: true);
    }
  }

  void _onLoading() {
    if (_tabController.index == 0) {
      _cubit.getListPlanSeed(isLoadMore: true);
    } else if (_tabController.index == 1) {
      _cubit.getListCLeanSeed(isLoadMore: true);
    }
  }

  Widget buildPage(BuildContext context, HomeSowState state) {
    int plan = _cubit.uncompletedTotal ?? 0;
    int clean = _cubit.uncompletedCleanTotal ?? 0;
    int total = plan +clean;
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar: appBar(context, R.string.seeding_book.tr().toUpperCase(),
          backgroundColor: R.color.primaryColor,
          titleColor: R.color.white,
          centerTitle: true,
          iconColor: R.color.white,
          rightWidget: Padding(
            padding: EdgeInsets.only(right: 28.w),
            child: InkWell(
              onTap: () {
                if(_cubit.uncompletedTotal != 0 ||
                    _cubit.uncompletedCleanTotal != 0) {
                  Utils.showToast(
                      context, R.string.you_have_unfinished_today.tr(
                      args: ["${total}"]));
                }else{}
              },
              child: Image.asset(
                (_cubit.uncompletedTotal != 0 ||
                        _cubit.uncompletedCleanTotal != 0)
                    ? R.drawable.ic_noti_white_on
                    : R.drawable.ic_no_notification,
                height: 23.h,
                width: 23.h,
              ),
            ),
          )),
      body: StackLoadingView(
        visibleLoading: state is HomeSowLoading,
        child: SmartRefresher(
          controller: _refreshController,
          enablePullUp: _cubit.nextToken != null,
          onRefresh: () {
            _onRefresh();
          },
          onLoading: () {
            _onLoading();
          },
          child: CustomScrollView(
            slivers: [
              buildButtonCreate(context, () {
                if (_tabController.index == 1) {
                  NavigationUtils.navigatePage(context, CleanSeedsPage())
                      .then((value) => _cubit.refreshCleanPlan());
                  return;
                } else {
                  NavigationUtils.navigatePage(context, NewSowingSeedsPage())
                      .then((value) => _cubit.refreshPlan());
                  return;
                }
              }),
              _sliverSpace(16),
              buildSlide(state),
              _sliverSpace(20),
              _tabBar(),
              _tabView(state)
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSlide(HomeSowState state) {
    return SliverToBoxAdapter(
      child: Visibility(
        visible: state is! GetListTutorialEmpty,
        child: Container(
          height: 150.h,
          padding: EdgeInsets.only(left: 20.h),
          child: ListView.builder(
              itemCount: _cubit.listTutorial.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, int index) {
                NewsData data = _cubit.listTutorial[index];
                return GestureDetector(
                  onTap: () {
                    NavigationUtils.navigatePage(
                        context,
                        DetailTutorialSowPage(
                          postId: _cubit.listTutorial[index].id ?? 0,
                        ));
                  },
                  child: Container(
                    width: 150.h,
                    decoration: BoxDecoration(
                        color: R.color.white,
                        borderRadius: BorderRadius.circular(8.h)),
                    margin: EdgeInsets.only(right: 8.h),
                    padding: EdgeInsets.all(4.h),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8.h),
                          child: CachedNetworkImage(
                            width: double.infinity,
                            height: 108.h,
                            fit: BoxFit.fill,
                            imageUrl: data.imageUrl ?? "",
                            placeholder: (_, __) {
                              return Text(
                                R.string.loading.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 14.sp),
                              );
                            },
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            height: 60.h,
                            width: 144.w,
                            decoration: BoxDecoration(
                              color: R.color.white,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(8.h),
                                  bottomRight: Radius.circular(8.h)),
                            ),
                            child: Text(
                              data.title ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      color: R.color.black,
                                      fontSize: 14.sp,
                                      height: 24.h / 14.sp),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }

  Widget buildButtonCreate(BuildContext context, VoidCallback create) {
    return SliverToBoxAdapter(
      child: GestureDetector(
        onTap: create,
        child: Container(
          height: 48.h,
          width: double.infinity,
          child: Stack(
            children: [
              Container(
                height: 24,
                width: double.infinity,
                color: R.color.primaryColor,
              ),
              Center(
                child: Container(
                  alignment: Alignment.center,
                  height: 48.h,
                  margin: EdgeInsets.symmetric(horizontal: 20.w),
                  //width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24.h),
                      color: R.color.white),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(CupertinoIcons.add,
                          size: 24.h, color: R.color.primaryColor),
                      SizedBox(width: 10.h),
                      Padding(
                        padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 8),
                        child: Text(
                          _tabController.index == 0
                              ? R.string.create_a_new_seeding.tr()
                              : R.string.create_a_new_clean_seed.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 16.sp, color: R.color.darkerBlue),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _sliverSpace(double height) {
    return SliverToBoxAdapter(
      child: SizedBox(height: height.h),
    );
  }

  Widget _tabBar() {
    final _textTheme =
        Theme.of(context).textTheme.medium500.copyWith(fontSize: 16.sp);
    return SliverPersistentHeader(
      pinned: true,
      floating: true,
      delegate: StickyTabBarDelegate(
        child: TabBar(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          indicatorColor: R.color.primaryColor,
          controller: _tabController,
          labelColor: R.color.primaryColor,
          labelPadding: EdgeInsets.only(bottom: 5.h),
          unselectedLabelColor: R.color.gray,
          labelStyle: _textTheme,
          onTap: (_) {
            if (_tabController.previousIndex != _tabController.index)
              setState(() {});
          },
          indicatorSize: TabBarIndicatorSize.label,
          tabs: [
            Text(R.string.plan_seed.tr()),
            Text(R.string.clean_seed.tr()),
          ],
        ),
      ),
    );
  }

  Widget _tabView(HomeSowState state) {
    switch (_tabController.index) {
      case 0:
        return SliverToBoxAdapter(
          child: ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 12.h),
            physics: NeverScrollableScrollPhysics(),
            itemCount: _cubit.listPlanSeed.length,
            itemBuilder: (BuildContext context, int index) {
              PlantSeedHistoriesData data = _cubit.listPlanSeed[index];
              return buildRow(
                callback: () {
                  NavigationUtils.rootNavigatePage(
                          context, NewSowingSeedsPage(planSeedId: data.id))
                      .then((value) => _cubit.refreshPlan());
                },
                image: R.drawable.ic_plan_seed,
                title: data.step1,
                time: data.createdDate,
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      children: List.generate(
                        _cubit.listPlanSeed[index].steps?.length ?? 0,
                        (count) => Container(
                          margin: EdgeInsets.only(right: 4.w),
                          padding: EdgeInsetsDirectional.fromSTEB(
                              4.w, 2.h, 4.w, 6.h),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: R.color.primaryColor),
                          child: Text(
                            "${_cubit.listPlanSeed[index].steps?[count]}",
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(fontSize: 8.sp, color: R.color.white),
                          ),
                        ),
                      ),
                    ),
                    if (_cubit.listPlanSeed[index].steps?.length == 1) ...[
                      Row(
                          children: List.generate(
                        _cubit.step2.length,
                        (count) => buildCircle(step: _cubit.step2[count]),
                      ))
                    ],
                    if (_cubit.listPlanSeed[index].steps?.length == 2) ...[
                      Row(
                          children: List.generate(
                        _cubit.step3.length,
                        (count) => buildCircle(step: _cubit.step3[count]),
                      ))
                    ],
                    if (_cubit.listPlanSeed[index].steps?.length == 3) ...[
                      Row(
                          children: List.generate(
                        _cubit.step4.length,
                        (count) => buildCircle(step: _cubit.step4[count]),
                      ))
                    ],
                  ],
                ),
              );
            },
          ),
        );
      case 1:
        return SliverToBoxAdapter(
            child: ListView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 12.h),
          physics: NeverScrollableScrollPhysics(),
          itemCount: _cubit.listCleanSeedHistory.length,
          itemBuilder: (BuildContext context, int index) {
            FetchCleanSeedHistoriesData data =
                _cubit.listCleanSeedHistory[index];
            return buildRow(
              callback: () {
                //todo
                NavigationUtils.navigatePage(
                    context,
                    CleanSeedsPage(
                      cleanSeedId: data.id,
                    )).then((value) {
                  _cubit.refreshCleanPlan();
                });
              },
              image: R.drawable.ic_clean_seed,
              title: data.step2,
              time: data.createdDate,
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    children: List.generate(
                      _cubit.listCleanSeedHistory[index].steps?.length ?? 0,
                      (count) => Container(
                        margin: EdgeInsets.only(right: 4.w),
                        padding:
                            EdgeInsetsDirectional.fromSTEB(4.w, 2.h, 4.w, 6.h),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: R.color.primaryColor),
                        child: Center(
                          child: Text(
                            "${_cubit.listCleanSeedHistory[index].steps?[count]}",
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(fontSize: 8.sp, color: R.color.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                  if (_cubit.listCleanSeedHistory[index].steps?.length ==
                      1) ...[
                    Row(
                        children: List.generate(
                      _cubit.step2.length,
                      (count) => buildCircle(step: _cubit.step2[count]),
                    ))
                  ],
                  if (_cubit.listCleanSeedHistory[index].steps?.length ==
                      2) ...[
                    Row(
                        children: List.generate(
                      _cubit.step3.length,
                      (count) => buildCircle(step: _cubit.step3[count]),
                    ))
                  ],
                  if (_cubit.listCleanSeedHistory[index].steps?.length ==
                      3) ...[
                    Row(
                        children: List.generate(
                      _cubit.step4.length,
                      (count) => buildCircle(step: _cubit.step4[count]),
                    ))
                  ],
                ],
              ),
            );
          },
        ));
      default:
        return SliverGrid.count(
          crossAxisCount: 2,
        );
    }
  }

  Widget buildCircle({int? step}) {
    return Container(
      margin: EdgeInsets.only(right: 4.w),
      padding: EdgeInsetsDirectional.fromSTEB(4.w, 2.h, 4.w, 6.h),
      decoration: BoxDecoration(
          shape: BoxShape.circle, color: R.color.gray.withOpacity(0.4)),
      child: Text(
        "${step}",
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 8.sp, color: R.color.white),
      ),
    );
  }

  Widget buildRow(
      {VoidCallback? callback,
      String? image,
      String? title,
      DateTime? time,
      Widget? widget}) {
    return InkWell(
      onTap: callback,
      child: Container(
        height: 57.h,
        margin: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 10.h),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(image ?? "", height: 50.h, width: 50.h),
            SizedBox(width: 8.w),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            title ?? "",
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(fontSize: 12.sp, height: 16 / 16),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                DateFormat("HH:mm dd-MM-yyyy")
                                    .format(time ?? DateTime.now()),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: R.color.grey,
                                        fontSize: 10.sp,
                                        height: 14 / 10),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            widget ?? SizedBox()
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 4.w),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.h),
                      color: R.color.lighterGray.withOpacity(0.1),
                    ),
                    padding: EdgeInsets.all(5.h),
                    child: Icon(
                      CupertinoIcons.chevron_right,
                      size: 12.h,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar child;

  StickyTabBarDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: R.color.lightestGray,
      child: this.child,
    );
  }

  @override
  double get maxExtent => this.child.preferredSize.height;

  @override
  double get minExtent => this.child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
