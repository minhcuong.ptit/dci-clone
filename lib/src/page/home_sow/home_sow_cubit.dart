import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/clean_seed_history_response.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/response/plan_seed_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/home_sow/home_sow_state.dart';
import 'package:imi/src/utils/enum.dart';

class HomeSowCubit extends Cubit<HomeSowState> {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;
  bool isSearch = true;
  List<FetchCleanSeedHistoriesData> listCleanSeedHistory = [];
  List<NewsData> listTutorial = [];
  List<PlantSeedHistoriesData> listPlanSeed = [];
  int? uncompletedTotal;
  int? uncompletedCleanTotal;
  String? nextToken;
  String? nextTokenClean;
  String? nextTokenTutorial;
  List<int> step2 = [2, 3, 4];
  List<int> step3 = [3, 4];
  List<int> step4 = [4];

  HomeSowCubit(this.graphqlRepository, this.repository)
      : super(HomeSowInitial());

  void refreshPlan(){
    emit(HomeSowLoading());
    getListPlanSeed();
    emit(HomeSowInitial());
  }

  void refreshCleanPlan(){
    emit(HomeSowLoading());
    getListCLeanSeed();
    emit(HomeSowInitial());
  }

  void getListCLeanSeed({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh || !isLoadMore ? HomeSowLoading() : HomeSowInitial());
    if (isLoadMore != true) {
      nextTokenClean = null;
      listCleanSeedHistory.clear();
    }
    ApiResult<FetchCleanSeedHistory> getLibrary =
        await graphqlRepository.getCleanSeedHistory(
      nextToken: nextTokenClean,
    );
    getLibrary.when(success: (FetchCleanSeedHistory data) async {
      if (data.data != null) {
        listCleanSeedHistory.addAll(data.data ?? []);
        nextTokenClean = data.nextToken;
        emit(HomeSowSuccess());
        uncompletedCleanTotal = data.uncompletedTotal;
      } else {
        emit(GetListCleanSeedEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeSowFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListTutorial({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? HomeSowLoading() : HomeSowInitial());
    if (isRefresh) {
      nextTokenTutorial = null;
      listTutorial.clear();
    }
    ApiResult<NewsDataFetch> getLibrary = await graphqlRepository.getNews(
      nextToken: nextTokenTutorial,
        typeCategory: NewsCategory.PLANT_SEED_GUIDE);
    getLibrary.when(success: (NewsDataFetch data) async {
      if (data.data != null) {
        listTutorial.addAll(data.data ?? []);
        nextTokenTutorial = data.nextToken;
        emit(HomeSowSuccess());
      } else {
        emit(GetListTutorialEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeSowFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListPlanSeed({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh || !isLoadMore ? HomeSowLoading() : HomeSowInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listPlanSeed.clear();
    }
    ApiResult<FetchPlantSeedHistory> getLibrary =
        await graphqlRepository.getPlanSeedHistory(
      nextToken: nextToken,
    );
    getLibrary.when(success: (FetchPlantSeedHistory data) async {
      if (data.data != null) {
        listPlanSeed.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(HomeSowSuccess());
        uncompletedTotal = data.uncompletedTotal ?? null;
      } else {
        emit(GetListSowSeedEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeSowFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
