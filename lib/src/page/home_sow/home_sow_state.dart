import 'package:equatable/equatable.dart';

abstract class HomeSowState {
  HomeSowState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class HomeSowInitial extends HomeSowState {
  @override
  String toString() => 'HomeSowInitial';
}
class HomeSowSuccess extends HomeSowState {
  @override
  String toString() => 'HomeSowSuccess';
}

class HomeSowLoading extends HomeSowState {
  @override
  String toString() => 'HomeSowLoading';
}

class HomeSowFailure extends HomeSowState {
  final String error;

  HomeSowFailure(this.error);

  @override
  String toString() => 'HomeSowFailure { error: $error }';
}

class GetListCleanSeedEmpty extends HomeSowState {
  @override
  String toString() {
    return 'GetListCleanSeedEmpty{}';
  }
}

class GetListSowSeedEmpty extends HomeSowState {
  @override
  String toString() {
    return 'GetListSowSeedEmpty{}';
  }
}

class GetListTutorialEmpty extends HomeSowState {
  @override
  String toString() {
    return 'GetListTutorialEmpty{}';
  }
}

