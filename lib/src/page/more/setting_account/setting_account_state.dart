import 'dart:io';

import 'package:equatable/equatable.dart';

abstract class SettingAccountState extends Equatable {
  SettingAccountState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialSettingAccountState extends SettingAccountState {}

class SettingAccountLoading extends SettingAccountState {
  @override
  String toString() => 'SettingAccountLoading';
}

class SettingAccountFailure extends SettingAccountState {
  final String? error;

  SettingAccountFailure(this.error);

  @override
  String toString() {
    return 'SettingAccountFailure {error: $error}';
  }
}

class SettingAccountSuccess extends SettingAccountState {

  @override
  String toString() => 'SettingAccountSuccess';
}

class ChangeLanguageSettingSuccess extends SettingAccountState {
  @override
  String toString() {
    return 'ChangeLanguageSettingSuccess{}';
  }
}