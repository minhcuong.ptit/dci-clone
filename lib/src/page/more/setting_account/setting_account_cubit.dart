import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/change_language_request.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/more/setting_account/setting_account_state.dart';
import 'package:imi/src/utils/const.dart';

import '../../../data/preferences/app_preferences.dart';

class SettingAccountCubit extends Cubit<SettingAccountState> {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;

  String? get name => appPreferences.getString(Const.FULL_NAME);

  String? get avatar => appPreferences.getString(Const.AVATAR);

  String get userUuid => appPreferences.getString(Const.ID) ?? "";

  bool get isLoggedIn => appPreferences.isLoggedIn;

  String get currentLanguage => appPreferences.appLanguage;

  int get currentIndex => currentLanguage == Const.LOCALE_EN ? 1 : 0;
  String? deviceToken = appPreferences.getString(Const.HEADER_KEY_DEVICE_TOKEN);

  SettingAccountCubit(this.graphqlRepository, this.repository)
      : super(InitialSettingAccountState());

  void refreshLanguage() {
    emit(SettingAccountLoading());
    emit(InitialSettingAccountState());
  }

  void selectLanguage() {
    emit(SettingAccountLoading());
    emit(ChangeLanguageSettingSuccess());
  }

  void changeLanguage() async {
    emit(SettingAccountLoading());
    ApiResult<dynamic> result =
        await repository.changeLanguage(ChangeLanguageRequest(
      deviceToken: deviceToken,
      userUuid: userUuid,
    ));
    result.when(success: (dynamic response) async {
      emit(SettingAccountSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(SettingAccountFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
