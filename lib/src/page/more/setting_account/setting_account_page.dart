import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/localization/localization.dart';
import 'package:imi/src/page/authentication/authentication_cubit.dart';
import 'package:imi/src/page/clean_account/clean_account.dart';
import 'package:imi/src/page/clean_account/clean_account_page.dart';
import 'package:imi/src/page/home_faq/home_faq.dart';
import 'package:imi/src/page/list_block_user/list_block_user_page.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/more/setting_account/setting_account.dart';
import 'package:imi/src/page/pin_code/change_password/change_password.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class SettingAccountPage extends StatefulWidget {
  const SettingAccountPage({Key? key}) : super(key: key);

  @override
  State<SettingAccountPage> createState() => _SettingAccountPageState();
}

class _SettingAccountPageState extends State<SettingAccountPage> {
  late SettingAccountCubit _cubit;
  late MainCubit _mainCubit;
  late AuthenticationCubit _authCubit;
  @override
  void initState() {
    _mainCubit = GetIt.I();
    _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = SettingAccountCubit(graphqlRepository, repository);
    _cubit.refreshLanguage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.settings.tr(), centerTitle: true,),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<SettingAccountCubit, SettingAccountState>(
          listener: (context, state) {
            if (state is ChangeLanguageSettingSuccess) {
              Localization.changeLanguage(context);
            }
            // TODO: implement listener
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, SettingAccountState state) {
    ValueChanged<int?> changeLanguageCallback = (index) {
      _cubit.selectLanguage();
      // _mainCubit.refreshAllTab();
    };
    return StackLoadingView(
      child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 15.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.h),
                color: Colors.white,
                child: GestureDetector(
                  onTap: () {
                    if (checkShowLoginDialog(context)) {
                      NavigationUtils.rootNavigatePage(
                          context,
                          BlocProvider.value(
                              value: _mainCubit,
                              child: ViewProfilePage(
                                communityId: null,
                              ))).then((value) {
                        if (value == Const.ACTION_CHANGE_TAB_MEMBERSHIP) {
                          Future.delayed(Duration(milliseconds: 200), () {
                            _mainCubit.selectTab(Const.SCREEN_MEMBERSHIP);
                          });
                        }
                      });
                    }
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AvatarWidget(
                        avatar: _cubit.avatar,
                        size: 64.h,
                      ),
                      SizedBox(height: 8.h),
                      Text(
                        _cubit.name ?? "",
                        style: Theme.of(context).textTheme.bodyBold,
                      ),
                      Text(
                        R.string.setting_view_profile.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .labelSmallText
                            .copyWith(color: R.color.lightShadesGray),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.h),
              otherRow(R.drawable.ic_lock, R.string.change_password.tr(), () {
                NavigationUtils.rootNavigatePage(context, ChangePasswordPage());
              }, false, ""),
              Container(
                  color: R.color.lightestGray,
                  height: 1.h,
                  margin: EdgeInsets.symmetric(horizontal: 16.h)),
              otherRow(
                  R.drawable.ic_block_user, R.string.manage_block_list.tr(),
                  () {
                NavigationUtils.rootNavigatePage(context, ListBlockUserPage());
              }, false, ""),
              Container(
                  color: R.color.lightestGray,
                  height: 1.h,
                  margin: EdgeInsets.symmetric(horizontal: 16.h)),
              otherRow(
                  R.drawable.ic_clean_account, R.string.delete_account.tr(),
                  () {
                NavigationUtils.navigatePage(
                    context,
                    BlocProvider.value(
                        value: _mainCubit, child: CleanAccountPage()));
              }, false, ""),
              Container(
                  color: R.color.lightestGray,
                  height: 1.h,
                  margin: EdgeInsets.symmetric(horizontal: 16.h)),
              otherRow(
                R.drawable.ic_translate,
                R.string.change_language.tr(),
                () {
                  showBarModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return SingleChildScrollView(
                          child: Container(
                            // color: R.color.white,
                            padding: EdgeInsets.all(20.h),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    if (_cubit.currentIndex == 0) {
                                      return;
                                    }
                                    changeLanguageCallback(0);
                                    _cubit.changeLanguage();
                                    NavigationUtils.pop(context);
                                  },
                                  behavior: HitTestBehavior.translucent,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(R.drawable.ic_vietnam,
                                          height: 32.h),
                                      SizedBox(width: 8.w),
                                      Expanded(
                                        child: Text(
                                          R.string.vietnamese.tr(),
                                          style: TextStyle(
                                              color: R.color.black,
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      SizedBox(width: 13.h),
                                      _cubit.currentIndex == 0
                                          ? Image.asset(R.drawable.ic_select,
                                              height: 25.h)
                                          : SizedBox(),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 15.h),
                                Container(height: 1, color: R.color.grey200),
                                SizedBox(height: 15.h),
                                GestureDetector(
                                    onTap: () {
                                      if (_cubit.currentIndex == 1) {
                                        return;
                                      }
                                      changeLanguageCallback(1);
                                      _cubit.changeLanguage();
                                      NavigationUtils.pop(context);
                                    },
                                    behavior: HitTestBehavior.translucent,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(R.drawable.ic_english,
                                            height: 32.h),
                                        SizedBox(width: 8.w),
                                        Expanded(
                                          child: Text(
                                            R.string.english.tr(),
                                            style: TextStyle(
                                                color: R.color.black,
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        SizedBox(width: 13.h),
                                        _cubit.currentIndex == 1
                                            ? Image.asset(
                                                R.drawable.ic_select,
                                                height: 25.h,
                                              )
                                            : SizedBox(),
                                      ],
                                    )),
                                SizedBox(height: 15.h),
                                // Container(height: 1, color: R.color.grey200),
                                // SizedBox(height: 15.h),
                                // GestureDetector(
                                //     onTap: () {
                                //       if (_cubit.currentIndex == 2) {
                                //         return;
                                //       }
                                //       changeLanguageCallback(2);
                                //       _cubit.changeLanguage();
                                //       NavigationUtils.pop(context);
                                //     },
                                //     behavior: HitTestBehavior.translucent,
                                //     child: Row(
                                //       mainAxisAlignment:
                                //           MainAxisAlignment.start,
                                //       crossAxisAlignment:
                                //           CrossAxisAlignment.center,
                                //       children: [
                                //         Image.asset(R.drawable.ic_china,
                                //             height: 32.h),
                                //         SizedBox(width: 8.w),
                                //         Expanded(
                                //           child: Text(
                                //             "中国人",
                                //             style: TextStyle(
                                //                 color: R.color.black,
                                //                 fontSize: 16.sp,
                                //                 fontWeight: FontWeight.w400),
                                //           ),
                                //         ),
                                //         SizedBox(width: 13.h),
                                //         _cubit.currentIndex == 2
                                //             ? Image.asset(
                                //                 R.drawable.ic_select,
                                //                 height: 25.h,
                                //               )
                                //             : SizedBox(),
                                //       ],
                                //     )),
                              ],
                            ),
                          ),
                        );
                      });
                },
                true,
                getLanguage(_cubit.currentIndex),
              ),
              Container(
                  color: R.color.lightestGray,
                  height: 1.h,
                  margin: EdgeInsets.symmetric(horizontal: 16.h)),
              otherRow(R.drawable.ic_help, R.string.frequently_asked_question.tr(), () {
                NavigationUtils.rootNavigatePage(context, HomeFaqPage());
              }, false, ""),
              Container(
                  color: R.color.lightestGray,
                  height: 1.h,
                  margin: EdgeInsets.symmetric(horizontal: 16.h)),
              SizedBox(height: 100.h),
              GestureDetector(
                onTap: () {
                  _cubit.changeLanguage();
                },
                child: Container(
                  height: 32.h,
                  margin: EdgeInsets.only(left: 16.w, right: 16.w),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(48.h),
                    border: Border.all(width: 1.h, color: R.color.blue),
                  ),
                  child: Text(
                    R.string.update_account.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .buttonSmall
                        .copyWith(color: R.color.blue),
                  ),
                ),
              ),
              SizedBox(height: 24.h),
            ],
          )),
      visibleLoading: state is SettingAccountLoading,
    );
  }

  Widget otherRow(String icon, String title, VoidCallback onTap, bool translate,
      String language) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 14.h, horizontal: 16.h),
        child: Row(
          children: [
            Image.asset(
              icon,
              height: 24.h,
              color: R.color.black,
            ),
            SizedBox(width: 8.w),
            Expanded(
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(color: R.color.black, fontSize: 11.sp),
              ),
            ),
            SizedBox(width: 16.w),
            Visibility(
              visible: translate,
              child: Text(
                language,
                style: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(color: R.color.blue, fontSize: 11.sp),
              ),
            ),
            SizedBox(width: 12.w),
            Icon(CupertinoIcons.right_chevron,
                color: R.color.black, size: 12.h),
          ],
        ),
      ),
    );
  }

  bool checkShowLoginDialog(BuildContext context) {
    bool isLoggedIn = _cubit.isLoggedIn;
    if (!isLoggedIn) {
      buildConfirmLogin(context, () {
        _authCubit.logout();
      });
    }
    return isLoggedIn;
  }

  void buildConfirmLogin(BuildContext context, VoidCallback loginCallback) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(R.string.confirm.tr()),
          content: Text(R.string.you_need_to_login.tr()),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(R.string.ok.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
                loginCallback();
              },
            ),
            CupertinoDialogAction(
              child: Text(R.string.cancel.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  String getLanguage(int index) {
    if (index == 0) {
      return R.string.vietnamese.tr();
    }
    if (index == 1) {
      return R.string.english.tr();
    }
    //if (index == 2) {
    //   return "中国人";
    // }
      return "";
  }
}
