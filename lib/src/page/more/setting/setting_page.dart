import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/main.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/history_point/history_point_page.dart';
import 'package:imi/src/page/list_order/list_order_page.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/page/more/setting/setting.dart';
import 'package:imi/src/page/more/setting_account/setting_account_page.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_page.dart';
import 'package:imi/src/utils/app_config.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/draggable_floating_action_button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import '../../pin_code/change_password/change_password.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  late SettingCubit _cubit;
  late MainCubit _mainCubit;
  late AuthenticationCubit _authCubit;
  final GlobalKey _parentKey = GlobalKey();
  final GlobalKey _toolTipKey = GlobalKey();

  @override
  void initState() {
    _cubit = BlocProvider.of<SettingCubit>(context);
    _mainCubit = GetIt.I();
    _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    super.initState();
    _cubit.getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: BlocProvider(
          create: (BuildContext context) => _cubit,
          child: BlocConsumer<SettingCubit, SettingState>(
            listener: (context, state) {
              if (state is SettingFailure) {
                Utils.showErrorSnackBar(context, state.error);
              }
              if (state is LogoutSuccess) {
                NavigationUtils.popToFirst(context);
                AuthenticationCubit authCubit =
                    BlocProvider.of<AuthenticationCubit>(context);
                authCubit.logout();
                _mainCubit.refreshAuth();
              }
            },
            builder: (context, state) {
              return Stack(
                key: _parentKey,
                children: [
                  Column(
                    children: [
                      Container(height: 42.h, color: R.color.white),
                      Expanded(child: pageWidget(context, state)),
                    ],
                  ),
                  DraggableFloatingActionButton(
                    child: GestureDetector(
                      onTap: () {
                        NavigationUtils.navigatePage(
                            context, ChatPage(chatType: ChatType.support));
                      },
                      onLongPress: () {
                        final dynamic _toolTip = _toolTipKey.currentState;
                        _toolTip.ensureTooltipVisible();
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            padding: EdgeInsets.all(2.h),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.h),
                                color: R.color.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: R.color.grey100, spreadRadius: 1)
                                ]),
                            child: Text(
                              R.string.dci_support.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      color: R.color.black,
                                      fontSize: 11.sp,
                                      height: 16.h / 11.sp),
                            ),
                          ),
                          SizedBox(height: 4.h),
                          Container(
                            width: 50,
                            height: 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: R.color.primaryColor,
                            ),
                            child: Image.asset(
                              R.drawable.ic_headphones,
                              height: 23.h,
                              width: 23.h,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                    ),
                    initialOffset: Offset(
                        context.width * 0.4,
                        context.height * 0.74),
                    parentKey: _parentKey,
                    onPressed: () {},
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget pageWidget(BuildContext context, SettingState state) {
    return StackLoadingView(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Scrollbar(
              child: SingleChildScrollView(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: R.color.white,
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            if (AppConfig.environment == Environment.STAGING) {
                              alice.showInspector();
                            }
                          },
                          child: Container(
                              margin: EdgeInsets.only(
                                  top: 0.h,
                                  bottom: 12.h,
                                  left: 0.w,
                                  right: 8.w),
                              child: Image.asset(
                                R.drawable.ic_logo_dci_66_41,
                                height: 41.h,
                              )),
                        ),
                        Spacer(),
                        GestureDetector(
                          onTap: () {
                            NavigationUtils.rootNavigatePage(
                                context, HistoryPointPage());
                          },
                          child: Row(
                            children: [
                              Image.asset(R.drawable.ic_diamond_point,
                                  height: 25.h, width: 25.h),
                              SizedBox(width: 5.w),
                              RichText(
                                text: TextSpan(
                                  text: "${Utils.formatMoney(_cubit.point)}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(
                                          fontSize: 18.sp,
                                          color: R.color.primaryColor),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' ${R.string.point_dci.tr()}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(
                                                fontSize: 14.sp,
                                                color: R.color.primaryColor)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(16.h),
                    color: Colors.white,
                    child: GestureDetector(
                      onTap: () {
                        if (checkShowLoginDialog(context)) {
                          NavigationUtils.rootNavigatePage(
                              context,
                              BlocProvider.value(
                                  value: _mainCubit,
                                  child: ViewProfilePage(
                                    communityId: null,
                                  ))).then((value) {
                            if (value == Const.ACTION_CHANGE_TAB_MEMBERSHIP) {
                              Future.delayed(Duration(milliseconds: 200), () {
                                _mainCubit.selectTab(Const.SCREEN_MEMBERSHIP);
                              });
                            }
                          });
                        }
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AvatarWidget(
                            avatar: _cubit.avatar,
                            size: 64.h,
                          ),
                          SizedBox(height: 8.h),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    _cubit.name ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyBold
                                        .copyWith(fontSize: 13),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      if (checkShowLoginDialog(context)) {
                                        NavigationUtils.rootNavigatePage(
                                            context,
                                            BlocProvider.value(
                                                value: _mainCubit,
                                                child: ViewProfilePage(
                                                  communityId:
                                                      _cubit.myCommunityId,
                                                ))).then((value) {
                                          if (value ==
                                              Const
                                                  .ACTION_CHANGE_TAB_MEMBERSHIP) {
                                            Future.delayed(
                                                Duration(milliseconds: 200),
                                                () {
                                              _mainCubit.selectTab(
                                                  Const.SCREEN_MEMBERSHIP);
                                            });
                                          }
                                        });
                                      }
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(top: 4.h, left: 4.w),
                                      child: Image.asset(
                                        R.drawable.ic_setting_edit_profile,
                                        height: 10.h,
                                        color: R.color.black,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Text(
                                R.string.setting_view_profile.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmallText
                                    .copyWith(color: R.color.lightShadesGray),
                              ),
                              SizedBox(height: 4.h),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 12.h),
                  Container(
                    color: R.color.white,
                    height: 1.h,
                    child: Container(
                        color: R.color.lightestGray,
                        height: 1.h,
                        margin: EdgeInsets.symmetric(horizontal: 16.w)),
                  ),
                  SizedBox(height: 20.h),
                  Container(
                    margin:
                        EdgeInsets.only(left: 16.w, right: 16.w, bottom: 10.h),
                    child: Text(
                      R.string.setting_other.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .bodySmallBold
                          .copyWith(color: R.color.shadesGray),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16.w, right: 16.w),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.h),
                    ),
                    child: ListView(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.all(16.h),
                        children: [
                          otherRow(
                              R.drawable.ic_product,
                              R.string.my_order.tr(),
                              () => NavigationUtils.rootNavigatePage(
                                  context, ListOrderPage())),
                          Container(
                              color: R.color.lightestGray,
                              height: 1.h,
                              margin: EdgeInsets.symmetric(vertical: 12.h)),
                          otherRow(
                              R.drawable.ic_setting_about,
                              R.string.setting_about_dci.tr(),
                              () => Utils.launchURL(Utils.getTypeUrlLauncher(
                                  Const.ABOUT, Const.LAUNCH_TYPE_WEB))),
                          Container(
                              color: R.color.lightestGray,
                              height: 1.h,
                              margin: EdgeInsets.symmetric(vertical: 12.h)),
                          otherRow(
                              R.drawable.ic_setting_privacy,
                              R.string.setting_privacy.tr(),
                              () => Utils.launchURL(Utils.getTypeUrlLauncher(
                                  Const.PRIVACY_URL, Const.LAUNCH_TYPE_WEB))),
                          Container(
                              color: R.color.lightestGray,
                              height: 1.h,
                              margin: EdgeInsets.symmetric(vertical: 12.h)),
                          otherRow(
                              R.drawable.ic_setting_term,
                              R.string.setting_terms.tr(),
                              () => Utils.launchURL(Utils.getTypeUrlLauncher(
                                  Const.TERM_URL, Const.LAUNCH_TYPE_WEB))),
                          Container(
                              color: R.color.lightestGray,
                              height: 1.h,
                              margin: EdgeInsets.symmetric(vertical: 12.h)),
                          otherRow(
                              R.drawable.ic_setting_help,
                              R.string.setting_help.tr(),
                              () => Utils.launchURL(Utils.getTypeUrlLauncher(
                                  Const.SUPPORT, Const.LAUNCH_TYPE_WEB))),
                          Container(
                              color: R.color.lightestGray,
                              height: 1.h,
                              margin: EdgeInsets.symmetric(vertical: 12.h)),
                          otherRow(
                              R.drawable.ic_setting,
                              R.string.settings.tr(),
                              () => NavigationUtils.rootNavigatePage(
                                  context, SettingAccountPage())),
                        ]),
                  ),
                ],
              )),
            ),
          ),
          SizedBox(height: 24.h),
          GestureDetector(
            onTap: () {
              _cubit.logout();
              appPreferences.removeData(Const.BOOL_AMBIENT_SOUND);
              appPreferences.removeData(Const.INDEX_BELL_START);
              appPreferences.removeData(Const.INDEX_BELL_END);
              appPreferences.removeData(Const.INDEX_BELL_DELAY);
              appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
              appPreferences.removeData(Const.FILE_AUDIO);
              appPreferences.removeData(Const.POPUP_BUY_COURSE_EVENT);
            },
            child: Container(
              height: 32.h,
              margin: EdgeInsets.only(left: 16.w, right: 16.w),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(48.h),
                border: Border.all(width: 1.h, color: R.color.black),
              ),
              child: Text(
                R.string.logout.tr().toUpperCase(),
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .buttonSmall
                    .copyWith(color: R.color.black, fontSize: 12),
              ),
            ),
          ),
          SizedBox(height: 24.h),
        ],
      ),
      visibleLoading: state is SettingsLoading,
    );
  }

  Widget otherRow(String icon, String title, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8.h),
        child: Row(
          children: [
            Image.asset(
              icon,
              height: 24.h,
              color: R.color.black,
            ),
            SizedBox(
              width: 8.w,
            ),
            Expanded(
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .bodySmallBold
                    .copyWith(fontSize: 12, height: 20 / 12),
              ),
            ),
            SizedBox(
              width: 16.w,
            ),
            Icon(
              CupertinoIcons.right_chevron,
              color: R.color.black,
              size: 12.h,
            ),
          ],
        ),
      ),
    );
  }

  Widget _point(String str) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: R.color.white,
          border: Border.all(color: R.color.pointShadowColor, width: 1),
          borderRadius: BorderRadius.all(Radius.circular(12.w)),
          boxShadow: [
            BoxShadow(
                color: R.color.pointShadowColor,
                offset: const Offset(0, 2),
                blurRadius: 1,
                spreadRadius: 0)
          ],
        ),
        child: Column(
          children: [
            SizedBox(
              height: 4.h,
            ),
            Image.asset(
              R.drawable.ic_setting_point_dci,
              width: 20.w,
            ),
            FittedBox(
              fit: BoxFit.fitWidth,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.w),
                child: Text(
                  str,
                  maxLines: 1,
                  style: Theme.of(context).textTheme.labelLargeText.copyWith(
                      fontSize: 10.sp,
                      fontWeight: FontWeight.w400,
                      height: 20 / 12),
                ),
              ),
            ),
            SizedBox(
              height: 4.h,
            )
          ],
        ),
      ),
    );
  }

  bool checkShowLoginDialog(BuildContext context) {
    bool isLoggedIn = _cubit.isLoggedIn;
    if (!isLoggedIn) {
      buildConfirmLogin(context, () {
        _authCubit.logout();
      });
    }
    return isLoggedIn;
  }

  void buildConfirmLogin(BuildContext context, VoidCallback loginCallback) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(R.string.confirm.tr()),
          content: Text(R.string.you_need_to_login.tr()),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(R.string.ok.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
                loginCallback();
              },
            ),
            CupertinoDialogAction(
              child: Text(R.string.cancel.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void showMessageDialog(BuildContext context) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(R.string.notification.tr()),
          content: Text(R.string.coming_soon.tr()),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(R.string.ok.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
              },
            ),
          ],
        );
      },
    );
  }
}
