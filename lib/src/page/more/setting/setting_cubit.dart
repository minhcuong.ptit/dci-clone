import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/logout_request.dart';
import 'package:imi/src/data/network/response/membership.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';

import '../../../data/network/response/club_response.dart';
import '../../../data/network/response/community_data.dart';
import '../../../data/network/response/person_profile.dart';
import 'setting.dart';

class SettingCubit extends Cubit<SettingState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;

  SettingCubit({required this.appRepository, required this.graphqlRepository})
      : super(InitialSettingsState()) {
    //_getMemberships();
    _getMyClubs();
  }

  int? get myCommunityId => appPreferences.getInt(Const.COMMUNITY_ID);

  String? get email => appPreferences.getString(Const.EMAIL);

  String? get name => appPreferences.getString(Const.FULL_NAME);

  String? get fullName => appPreferences.getString(Const.FULL_NAME);

  String? get avatar => appPreferences.getString(Const.AVATAR);

  int get point => appPreferences.getInt(Const.POINT) ?? 0;

  int get communityId => appPreferences.getInt(Const.COMMUNITY_ID) ?? 0;

  bool get isLoggedIn => appPreferences.isLoggedIn;

  Membership? currentMembership;
  CommunityData? myClub;
  int subClubCount = 0;

  void logout() async {
    emit(SettingsLoading());
    ApiResult<dynamic> registerResult = await appRepository.logout(
        LogoutRequest(
            refreshToken: appPreferences.loginResponse?.refreshToken));
    registerResult.when(success: (dynamic data) {
      appPreferences.clearData();
      emit(LogoutSuccess());
    }, failure: (NetworkExceptions error) {
      appPreferences.clearData();
      emit(LogoutSuccess());
      emit(SettingFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshSetting() {
    emit(SettingsLoading());
    emit(InitialSettingsState());
  }

  void getProfile() async {
    ApiResult<CommunityDataV2Response> getProfileTask =
        await graphqlRepository.getCommunityV2(communityId);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      appPreferences.saveCommunityV2(data.communityV2);
      emit(InitialSettingsState());
    }, failure: (NetworkExceptions error) {
      emit(SettingFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void _getMemberships() async {
    // ApiResult<List<Membership>> result =
    //     await graphqlRepository.getMemberships();
    // result.when(
    //     success: (response) async {
    //       emit(SettingsLoading());
    //       if (response.isNotEmpty) {
    //         currentMembership = response
    //             .firstWhereOrNull((element) => element.packageActive != null);
    //       }
    //       emit(InitialSettingsState());
    //     },
    //     failure: (NetworkExceptions error) async {});
  }

  void _getMyClubs() async {
    // ApiResult<Clubs> clubResult =
    //     await graphqlRepository.getMyClubs(Const.NETWORK_DEFAULT_LIMIT, null);
    // clubResult.when(
    //     success: (Clubs response) async {
    //       if (response.data != null && response.data?.isNotEmpty == true) {
    //         emit(SettingsLoading());
    //         myClub = response.data!.first;
    //         subClubCount = response.data!.length - 1;
    //         emit(InitialSettingsState());
    //       }
    //     },
    //     failure: (NetworkExceptions error) async {});
  }
}
