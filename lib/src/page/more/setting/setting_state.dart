import 'dart:io';

import 'package:equatable/equatable.dart';

abstract class SettingState extends Equatable {
  SettingState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialSettingsState extends SettingState {}

class SettingsLoading extends SettingState {
  @override
  String toString() => 'SettingsLoading';
}

class SettingFailure extends SettingState {
  final String? error;

  SettingFailure(this.error);

  @override
  String toString() {
    return 'SettingFailure {error: $error}';
  }
}

class GetSettingsSuccess extends SettingState {
  GetSettingsSuccess(File? file);

  @override
  String toString() => 'GetSettingsSuccess';
}

class LogoutSuccess extends SettingState {
  @override
  String toString() => 'LogoutSuccess';
}
