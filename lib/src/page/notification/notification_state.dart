abstract class NotificationState {}

class NotificationInitial extends NotificationState {}

class NotificationFailure extends NotificationState {
  final String error;

  NotificationFailure(this.error);

  @override
  String toString() => 'NotificationFailure { error: $error }';
}

class NotificationSuccess extends NotificationState {
  final String message;

  NotificationSuccess(this.message);

  @override
  String toString() => 'NotificationSuccess { message: $message }';
}

class NotificationLoading extends NotificationState {
  @override
  String toString() => 'NotificationLoading';
}

class GetListNotificationEmpty extends NotificationState {
  @override
  String toString() {
    return 'GetListNotificationEmpty{}';
  }
}

class GetNotificationSuccess extends NotificationState {
  @override
  String toString() => 'GetCategorySuccess';
}

class ReadNotificationSuccess extends NotificationState {
  @override
  String toString() => 'ReadNotificationSuccess';
}

class ReadAllNotificationSuccess extends NotificationState {
  @override
  String toString() => 'ReadAllNotificationSuccess';
}

class GetDetailChallengeSuccess extends NotificationState {
  @override
  String toString() => 'GetDetailChallengeSuccess';
}
