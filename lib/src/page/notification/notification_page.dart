import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/notification_response.dart';
import 'package:imi/src/page/community/community.dart';
import 'package:imi/src/page/dci_notification/detail_dci_notification_page.dart';
import 'package:imi/src/page/detail_order/detail_order_page.dart';
import 'package:imi/src/page/home_sow/home_sow_page.dart';
import 'package:imi/src/page/home_zen/home_zen_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/time_ago.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/pending_action.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:styled_text/styled_text.dart';

import '../club_info/club_information/club_information.dart';
import '../comment/comment_page.dart';
import '../history_point/history_point.dart';
import '../manage_reported_posts/manage_reported_posts.dart';
import 'notification.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  late NotificationCubit _cubit;
  late CommunityCubit _homeCubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = NotificationCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _homeCubit = CommunityCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.getListNotification(isRefresh: true, showLoadingIndicator: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<NotificationCubit, NotificationState>(
        listener: (context, state) {
          if (state is NotificationFailure) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
            Utils.showErrorSnackBar(context, state.error);
          }
          if (!(state is NotificationLoading)) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is ReadNotificationSuccess) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
            _homeCubit.unreadCount();
            _homeCubit.unreadNotification--;
            _homeCubit.refreshCommunity();
          }
          if (state is ReadAllNotificationSuccess) {
            _cubit.markAllAsRead();
            _homeCubit.unreadCount();
            _homeCubit.unreadNotification = 0;
            _homeCubit.refreshCommunity();
          }
        },
        builder: (context, state) => Scaffold(
          backgroundColor: R.color.red,
          body: buildPage(context, state),
          appBar: appBar(
            context,
            R.string.notifications.tr(),
            backgroundColor: R.color.primaryColor,
            iconColor: R.color.white,
            titleColor: R.color.white,
          ),
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, NotificationState state) {
    return Container(
      height: double.infinity,
      color: R.color.white,
      child: Stack(
        children: [
          SmartRefresher(
              enablePullUp: true,
              controller: _refreshController,
              onRefresh: () => _cubit.getListNotification(isRefresh: true),
              onLoading: () => _cubit.getListNotification(),
              child: (_cubit.listNotification ?? []).length == 0
                  ? Visibility(
                      visible: state is GetNotificationSuccess &&
                          _cubit.listNotification != null,
                      child: Center(
                          child: Text(
                        R.string.no_notification.tr(),
                      )),
                    )
                  : CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: 20.h,
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: GestureDetector(
                            onTap: () {
                              _cubit.markAllAsRead();
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.0.w),
                              child: Text(
                                R.string.mark_all_as_read.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp,
                                        color: R.color.primaryColor),
                              ),
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: 12.h,
                          ),
                        ),
                        SliverList(
                          delegate:
                              SliverChildBuilderDelegate((context, index) {
                            NotificationData data =
                                _cubit.listNotification![index];
                            return InkWell(
                              onTap: () {
                                // redirect trong màn list
                                _cubit.listNotification![index].read = true;
                                String redirectPage = getRedirect(data.params);
                                if (redirectPage ==
                                    RedirectPage.GROUP_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      ViewClubPage(
                                        communityId: int.parse(data.params!
                                            .firstWhere((element) =>
                                                element.key == "communityId")
                                            .value!),
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage.REQUEST_JOIN_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      ViewClubPage(
                                        communityId: int.parse(data.params!
                                            .firstWhere((element) =>
                                                element.key == "communityId")
                                            .value!),
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage.REPORT_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      ManageReportPostsPage(
                                        communityId: int.parse(data.params!
                                            .firstWhere((element) =>
                                                element.key == "communityId")
                                            .value!),
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage.CHALLENGE_PAGE.name) {
                                  _cubit.checkNavigateChallenge(
                                      context,
                                      (jsonDecode(data.params?[1].value ?? "")
                                          as Map)['challengeVersionId']);
                                  ;
                                } else if (redirectPage ==
                                    RedirectPage.PLAN_SEED_PAGE.name) {
                                  NavigationUtils.rootNavigatePage(
                                      context, HomeSowPage());
                                } else if (redirectPage ==
                                    RedirectPage.MEDITATION_PAGE.name) {
                                  NavigationUtils.rootNavigatePage(
                                      context, HomeZenPage());
                                } else if (redirectPage ==
                                    RedirectPage.MEDITATION_COFFEE_PAGE.name) {
                                  NavigationUtils.rootNavigatePage(
                                      context, HomeZenPage());
                                } else if (redirectPage ==
                                    RedirectPage
                                        .PRIVATE_NOTIFICATION_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      DciNotificationPage(
                                        notificationPrivateId: int.parse(data
                                            .params!
                                            .firstWhere((element) =>
                                                element.key ==
                                                "notificationPrivateId")
                                            .value!),
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage.ORDER_HISTORY_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      DetailOrderPage(
                                        orderHistoryId: int.parse(data.params!
                                            .firstWhere((element) =>
                                                element.key == "orderHistoryId")
                                            .value!),
                                        paymentType:
                                            "${data.params!.last.value}",
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage.ORDER_WAS_CANCELED.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      DetailOrderPage(
                                        orderHistoryId: int.parse(data.params!
                                            .firstWhere((element) =>
                                                element.key == "orderHistoryId")
                                            .value!),
                                        paymentType:
                                            "${data.params!.last.value}",
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage.COMMUNITY_POST_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context,
                                      CommentPage(
                                        postId: int.parse(data.params!
                                            .firstWhere((element) =>
                                                element.key == "postId")
                                            .value!),
                                        onCommentSuccess: (countComment) {},
                                        onLikeSuccess: (like, likeNumber) {},
                                        onRemovePostSuccess: () {},
                                        onBookmarkSuccess: (bool bookmark) {},
                                      ));
                                } else if (redirectPage ==
                                    RedirectPage
                                        .POINT_TRANSACTION_IN_PAGE.name) {
                                  NavigationUtils.navigatePage(
                                      context, HistoryPointPage());
                                }
                                _cubit.readNotification(data.id);
                              },
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16.w, vertical: 12.h),
                                    color: (data.read ?? true)
                                        ? R.color.white
                                        : R.color.lightNavy,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        AvatarWidget(
                                            avatar: data.image, size: 32.h),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              StyledText(
                                                text: data.body ?? "",
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .regular400
                                                    .copyWith(fontSize: 12.sp),
                                                tags: {
                                                  'bold': StyledTextTag(
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bold700
                                                          .copyWith(
                                                              fontSize: 11.sp,
                                                              height:
                                                                  20 / 11.sp))
                                                },
                                              ),
                                              Text(
                                                TimeAgo.timeAgoSinceDate(
                                                  DateTime
                                                      .fromMillisecondsSinceEpoch(
                                                          data.createdDate!),
                                                ),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .regular400
                                                    .copyWith(
                                                      fontSize: 10.sp,
                                                      color: R.color.grey500,
                                                    ),
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios,
                                          size: 16.h,
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    color: R.color.lightestGray,
                                    height: 1.h,
                                    margin: EdgeInsets.symmetric(
                                      horizontal: 16.w,
                                    ),
                                  )
                                ],
                              ),
                            );
                          }, childCount: _cubit.listNotification!.length),
                        ),
                        // ListView.separated(
                        //   shrinkWrap: true,
                        //   itemCount: _cubit.listNotification!.length,
                        //   separatorBuilder: (context, index) => Container(
                        //     color: R.color.lightestGray,
                        //     height: 1.h,
                        //     margin: EdgeInsets.symmetric(
                        //       horizontal: 16.w,
                        //     ),
                        //   ),
                        //   itemBuilder: (BuildContext context, int index) {
                        //     return SizedBox();
                        //   },
                        // ),
                      ],
                    )),
          Visibility(
            visible: state is NotificationLoading,
            child: PendingAction(),
          ),
        ],
      ),
    );
  }

  void buildConfirmDialog(BuildContext context, VoidCallback loginCallback) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(R.string.confirm.tr()),
          content: Text(R.string.mark_all_as_read.tr()),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(R.string.ok.tr()),
              onPressed: () {
                _cubit.readNotification(null);
                NavigationUtils.pop(context);
              },
            ),
            CupertinoDialogAction(
              child: Text(R.string.cancel.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  String getRedirect(List<Params>? params) {
    return params
            ?.firstWhere((element) => element.key == "redirectPage")
            .value ??
        "";
  }

  int getId(List<Params>? params, String key) {
    int id = int.parse(
        params?.firstWhere((element) => element.key == key).value ?? "0");
    return id;
  }
}
