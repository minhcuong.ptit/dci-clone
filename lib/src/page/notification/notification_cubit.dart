import 'package:bloc/bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/notification_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../dci_challenge_details/dci_challenge_page.dart';
import '../start_practice/start_practice.dart';
import 'notification.dart';

class NotificationCubit extends Cubit<NotificationState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<NotificationData>? listNotification;
  String? nextToken;
  bool noMoreNotifications = false;
  NotificationType type = NotificationType.ACCOUNT;

  NotificationCubit({required this.repository, required this.graphqlRepository})
      : super(NotificationInitial());

  void getListNotification(
      {bool isRefresh = false, bool showLoadingIndicator = false}) async {
    if (showLoadingIndicator) emit(NotificationLoading());
    if (isRefresh) {
      nextToken = null;
      listNotification = [];
    }
    listNotification ??= [];
    String userUuid = appPreferences.getString(Const.ID) ?? "";
    ApiResult<Notifications> notificationResult =
        await graphqlRepository.getNotifications(
            userUuid, null, Const.NETWORK_DEFAULT_LIMIT, nextToken);
    notificationResult.when(success: (Notifications response) async {
      if (response.data != null) {
        listNotification?.addAll(response.data!);
        nextToken = response.nextToken;
        emit(GetNotificationSuccess());
      } else {
        emit(GetListNotificationEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(NotificationFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void readNotification(int? id) {
    emit(NotificationLoading());
    repository.markNotificationRead(id, "READ");
    if (id != null) {
      emit(ReadNotificationSuccess());
    } else {
      emit(ReadAllNotificationSuccess());
    }
  }

  void markAllAsRead() {
    listNotification?.forEach((element) {
      element.read = true;
    });
    repository.markNotificationRead(null, "READ");
    emit(ReadNotificationSuccess());
  }

  void checkNavigateChallenge(context, int challengeId) async {
    final res = await graphqlRepository.getDetailChallenge(
        challengeVersionId: challengeId);
    res.when(success: (data) {
      if (data.fetchChallengeVersionById?.challengeHistory?.missionHistory
              ?.length ==
          0) {
        NavigationUtils.rootNavigatePage(
          context,
          StartPracticePage(challengeId: challengeId),
        );
      }else{
        NavigationUtils.rootNavigatePage(context,
            DCIChallengePage(showPopup: false, challengeVersionId: challengeId));
      }
      emit(GetDetailChallengeSuccess());
    }, failure: (e) {
      emit(NotificationFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }
}
