import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/response/start_challenge_response.dart';

abstract class StartPracticeState extends Equatable {
  @override
  List<Object> get props => [];
}

class StartPracticeInitial extends StartPracticeState {}

class StartPracticeLoading extends StartPracticeState {
  @override
  String toString() => 'StartPracticeLoading';
}

class StartPracticeSuccess extends StartPracticeState {
  final StartChallengeResponse? data;

  StartPracticeSuccess({this.data});
  @override
  String toString() {
    return 'StartPracticeSuccess';
  }
}

class StartPracticeFailure extends StartPracticeState {
  final String error;

  StartPracticeFailure(this.error);

  @override
  String toString() => 'StartPracticeFailure { error: $error }';
}

class GetChallengeSuccess extends StartPracticeState {
  @override
  String toString() {
    return 'GetChallengeSuccess';
  }
}

class ErrorSuccess extends StartPracticeState {
  @override
  String toString() {
    return 'ErrorSuccess';
  }
}
