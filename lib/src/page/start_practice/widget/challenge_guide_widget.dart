import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:url_launcher/url_launcher.dart';

class ChallengeGuideWidget extends StatefulWidget {
  final String? description;

  const ChallengeGuideWidget({Key? key, this.description}) : super(key: key);

  @override
  State<ChallengeGuideWidget> createState() => _ChallengeGuideWidgetState();
}

class _ChallengeGuideWidgetState extends State<ChallengeGuideWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          R.string.implementation_guide.tr(),
          style: Theme.of(context)
              .textTheme
              .bodyMedium
              ?.copyWith(fontSize: 18.h, color: R.color.black),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            NavigationUtils.pop(context);
          },
          child: Icon(
            CupertinoIcons.back,
            color: R.color.black,
          ),
        ),
        backgroundColor: R.color.white,
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 10.h),
          child: HtmlWidget(
            widget.description ?? "",
            onTapUrl: (url) async {
              if (await canLaunch(url)) {
                return await launch(
                  url,
                );
              } else {
                throw 'Could not launch $url';
              }
            },
            factoryBuilder: () => MyWidgetFactory(),
            enableCaching: true,
          ),
        ),
      ),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {}
