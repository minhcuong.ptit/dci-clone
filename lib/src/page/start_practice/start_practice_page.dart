import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_page.dart';
import 'package:imi/src/page/start_practice/start_practice.dart';
import 'package:imi/src/page/start_practice/widget/challenge_guide_widget.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';

class StartPracticePage extends StatefulWidget {
  final int? challengeId;

  const StartPracticePage({Key? key, this.challengeId}) : super(key: key);

  @override
  State<StartPracticePage> createState() => _StartPracticePageState();
}

class _StartPracticePageState extends State<StartPracticePage> {
  late StartPracticeCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit =
        StartPracticeCubit(repository, graphqlRepository, widget.challengeId);
    _cubit.getDetailChallenge();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<StartPracticeCubit, StartPracticeState>(
        listener: (BuildContext context, state) {
          if (state is StartPracticeSuccess && state.data != null) {
            NavigationUtils.pop(context);
            NavigationUtils.navigatePage(
                    context,
                    DCIChallengePage(
                        showPopup: true,
                        challengeVersionId:
                            state.data!.idChallengeVersion ?? 0))
                .then((value) => _cubit.refreshChallenge());
          } else if (state is ErrorSuccess) {
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return SingleChildScrollView(
                    padding:
                        EdgeInsets.symmetric(horizontal: 24.h, vertical: 20.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          R.string.notifications.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .bold700
                              .copyWith(fontSize: 16.sp),
                        ),
                        Text(
                          R.string.error_challenge.tr(
                              args: [_cubit.challengeVersionById?.name ?? ""]),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 11.sp),
                        ),
                        SizedBox(height: 20.h),
                        ButtonWidget(
                            padding: EdgeInsets.symmetric(vertical: 6.h),
                            backgroundColor: R.color.primaryColor,
                            textSize: 12.sp,
                            title: R.string.understanded.tr(),
                            onPressed: () {
                              NavigationUtils.pop(context);
                            })
                      ],
                    ),
                  );
                });
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            backgroundColor: R.color.grey100,
            appBar: AppBar(
              title: Text(R.string.dci_challenge.tr().toUpperCase()),
              automaticallyImplyLeading: false,
              centerTitle: true,
              backgroundColor: R.color.primaryColor,
              leading: InkWell(
                onTap: () {
                  NavigationUtils.pop(context);
                },
                child: Icon(
                  CupertinoIcons.back,
                  color: R.color.white,
                ),
              ),
            ),
            body: StackLoadingView(
              visibleLoading: state is StartPracticeLoading,
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 10.h),
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.h, vertical: 12.h),
                          decoration: BoxDecoration(
                            color: R.color.white,
                            borderRadius: BorderRadius.circular(10.h),
                            boxShadow: [
                              BoxShadow(
                                color: R.color.black.withOpacity(0.3),
                                blurRadius: 10,
                              ),
                            ],
                          ),
                          child: Column(
                            children: [
                              Text(
                                R.string.youre_watching.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 12.sp),
                              ),
                              Text(
                                _cubit.challengeVersionById?.name
                                        ?.toUpperCase() ??
                                    "",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 14.sp, color: R.color.blue),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 50.h),
                      GestureDetector(
                        onTap: () {
                          NavigationUtils.rootNavigatePage(
                              context,
                              ChallengeGuideWidget(
                                description:
                                    _cubit.challengeVersionById?.description ??
                                        "",
                              ));
                        },
                        child: Container(
                          height: 30.h,
                          width: 30.h,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: R.color.primaryColor,
                          ),
                          child: Icon(
                            CupertinoIcons.question,
                            color: R.color.white,
                            size: 18.h,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20.h),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(4.h),
                          decoration: BoxDecoration(
                              color: R.color.lightBlue,
                              borderRadius: BorderRadius.circular(8.h)),
                          height: 200.h,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.h),
                                child: CachedNetworkImage(
                                  width: double.infinity,
                                  height: 108.h,
                                  imageUrl:
                                      _cubit.challengeVersionById?.imageUrl ??
                                          "",
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  _cubit.challengeVersionById?.name ?? "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(
                                          fontSize: 12.sp,
                                          color: R.color.white),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              _cubit.challengeVersionById?.challengeHistory
                                          ?.missionHistory?.isEmpty ??
                                      false
                                  ? Text(
                                      R.string.havent_done.tr(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bold700
                                          .copyWith(
                                              fontSize: 12.sp,
                                              color: R.color.white),
                                    )
                                  : Visibility(
                                      visible: state is StartPracticeSuccess,
                                      child: Text(
                                        R.string.have_done.tr(args: [
                                          "${_cubit.challengeVersionById?.challengeHistory?.completedTotal ?? 0}"
                                        ]),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                      ),
                                    )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20.h),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 20.h),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: R.color.black.withOpacity(0.1),
                                  blurRadius: 10,
                                ),
                              ],
                              color: R.color.white,
                              borderRadius: BorderRadius.circular(8.h)),
                          height: 200.h,
                          child: Column(
                            children: [
                              SizedBox(height: 24.h),
                              Text(
                                R.string.number_of_days_to_do_the_challenge
                                    .tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 12.sp, color: R.color.black),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 16.h),
                              if (_cubit.challengeVersionById?.dayNo != null)
                                SizedBox(
                                  height: 40.h,
                                  width: 23 *
                                          _cubit.challengeVersionById!.dayNo
                                              .toString()
                                              .length +
                                      2 *
                                          (_cubit.challengeVersionById!.dayNo
                                                  .toString()
                                                  .length -
                                              1),
                                  child: ListView.separated(
                                      scrollDirection: Axis.horizontal,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemBuilder: (context, index) {
                                        return Container(
                                          width: 23,
                                          padding: EdgeInsets.only(bottom: 4),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            color: R.color.lightBlue,
                                          ),
                                          child: Text(
                                            _cubit.challengeVersionById?.dayNo
                                                    .toString()
                                                    .characters
                                                    .elementAt(index) ??
                                                "",
                                            style: Theme.of(context)
                                                .textTheme
                                                .medium500
                                                .copyWith(
                                                    fontSize: 26,
                                                    height: 36 / 26,
                                                    color: R.color.white),
                                            textAlign: TextAlign.center,
                                          ),
                                        );
                                      },
                                      separatorBuilder: (context, index) {
                                        return const SizedBox(
                                          width: 2,
                                        );
                                      },
                                      itemCount: _cubit
                                              .challengeVersionById?.dayNo
                                              .toString()
                                              .length ??
                                          0),
                                ),
                              Text(
                                R.string.day.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 12.sp, color: R.color.red),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 40.h),
                  ButtonWidget(
                    padding: EdgeInsets.symmetric(vertical: 12.h),
                    backgroundColor: R.color.primaryColor,
                    title: R.string.start.tr(),
                    onPressed: () {
                      showModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return SingleChildScrollView(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 24.h, vertical: 20.h),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    R.string.confirm.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(fontSize: 16.sp),
                                  ),
                                  Text(
                                    R.string.set_of_challenges.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(fontSize: 11.sp),
                                  ),
                                  SizedBox(height: 20.h),
                                  ButtonWidget(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 16.h),
                                      backgroundColor: R.color.primaryColor,
                                      textSize: 14.sp,
                                      title: R.string.yes.tr(),
                                      onPressed: () {
                                        NavigationUtils.pop(context);
                                        _cubit.startPractice(
                                            challengeId: _cubit
                                                .challengeVersionById?.id);
                                      }),
                                  SizedBox(height: 10.h),
                                  ButtonWidget(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 16.h),
                                      backgroundColor: R.color.white,
                                      borderColor: R.color.primaryColor,
                                      textColor: R.color.primaryColor,
                                      textSize: 14.sp,
                                      title: R.string.no.tr(),
                                      onPressed: () {
                                        NavigationUtils.pop(context);
                                      })
                                ],
                              ),
                            );
                          });
                      // NavigationUtils.navigatePage(
                      //     context, DCIChallengePage(challengeVersionId: 41));
                    },
                    textSize: 16.sp,
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {}
