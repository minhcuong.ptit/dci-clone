import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/detail_challenge_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/start_practice/start_practice.dart';
import 'package:imi/src/page/start_practice/start_practice_state.dart';
import 'package:imi/src/utils/const.dart';

class StartPracticeCubit extends Cubit<StartPracticeState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  ChallengeVersionById? challengeVersionById;
  int? challengeVersionId;

  StartPracticeCubit(
      this.repository, this.graphqlRepository, this.challengeVersionId)
      : super(StartPracticeInitial());

  void startPractice({int? challengeId}) async {
    emit(StartPracticeLoading());
    final result = await repository.getStartPractice(challengeId: challengeId);
    result.when(success: (data) {
      emit(StartPracticeSuccess(data: data));
    }, failure: (NetworkExceptions e) {
      if (e is BadRequest &&
          e.code ==
              "do_only_1_challenge_at_the_same_level_at_the_same_time_error") {
        emit(ErrorSuccess());
        return;
      }
      emit(StartPracticeFailure(""));
    });
  }

  void getDetailChallenge() async {
    emit(StartPracticeLoading());
    final result = await graphqlRepository.getDetailChallenge(
        challengeVersionId: challengeVersionId);
    result.when(success: (DetailChallengeData data) {
      challengeVersionById = data.fetchChallengeVersionById;
      emit(GetChallengeSuccess());
    }, failure: (e) {
      emit(StartPracticeFailure(""));
    });
  }

  void refreshChallenge() {
    emit(StartPracticeLoading());
    getDetailChallenge();
    emit(StartPracticeInitial());
  }
}
