import 'dart:ui' as ui;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/home_success_story/home_story.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/news_response.dart';
import '../../utils/const.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/utils.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/search/search_widget.dart';
import '../../widgets/stack_loading_view.dart';
import '../detail_story/detail_story_page.dart';

class HomeStoryPage extends StatefulWidget {
  const HomeStoryPage({Key? key}) : super(key: key);

  @override
  State<HomeStoryPage> createState() => _HomeStoryPageState();
}

class _HomeStoryPageState extends State<HomeStoryPage> {
  RefreshController _refreshController = RefreshController();
  late HomeStoryCubit _cubit;
  TextEditingController _searchTopicController = TextEditingController();
  TextEditingController _searchCategoryController = TextEditingController();
  TextEditingController _searchController = TextEditingController();
  bool hideAppBar = false;
  late double _appBarHeight;
  String _selectedMenu = R.string.category.tr();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HomeStoryCubit(repository, graphqlRepository);
    _cubit.searchStory();
    _cubit.getPostTopicStory();
    _cubit.getCategoryNews();
    _appBarHeight = AppBar().preferredSize.height;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _searchTopicController.dispose();
    _searchCategoryController.dispose();
    _searchController.dispose();
    _refreshController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<HomeStoryCubit, HomeStoryState>(
        listener: (context, state) {
          if (state is HomeStoryFailure) {
            Utils.showErrorSnackBar(context, state.error);
          }
          if (state is! HomeStoryLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          // TODO: implement listener
        },
        builder: (context, state) {
          return buildPage(context, state);
        },
      ),
    );
  }

  Widget buildPage(BuildContext context, HomeStoryState state) {
    return Scaffold(
        backgroundColor: R.color.lightestGray,
        body: StackLoadingView(
          visibleLoading: state is HomeStoryLoading,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: MediaQuery.of(context).padding.top,
                width: double.infinity,
                color: R.color.primaryColor,
              ),
              _appBar(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 12.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        _cubit.selectedMenu ?? "",
                        style: Theme.of(context).textTheme.medium500.copyWith(
                            fontSize: 16.sp,
                            color: R.color.neutral1,
                            height: 20 / 16),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    SizedBox(width: 10.w),
                    buildPopupMenu(),
                  ],
                ),
              ),
              Expanded(
                child: RefreshConfiguration(
                  enableScrollWhenRefreshCompleted: false,
                  child: SmartRefresher(
                    enablePullUp: _cubit.nextTokenSearch != null,
                    controller: _refreshController,
                    onRefresh: () {
                      if (_cubit.isSelect == 0) {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            typeIds: _cubit.topicCategory,
                            isRefresh: true);
                      } else if (_cubit.isSelect == 1) {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            topicIds: _cubit.topicPostSearch,
                            isRefresh: true);
                      } else {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            isRefresh: true);
                      }
                    },
                    onLoading: () {
                      if (_cubit.isSelect == 0) {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            typeIds: _cubit.topicCategory,
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 1) {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            topicIds: _cubit.topicPostSearch,
                            isLoadMore: true);
                      } else {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            isLoadMore: true);
                      }
                    },
                    child: buildSearchListCourse(context, state),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget buildSearchListCourse(BuildContext context, HomeStoryState state) {
    return state is HomeStoryLoading
        ? const SizedBox.shrink()
        : _cubit.listStorySearch.length == 0
            ? Center(
                child: Text(
                  R.string.no_result_search.tr(),
                  style: Theme.of(context).textTheme.bodyRegular,
                ),
              )
            : GridView.builder(
                primary: true,
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                physics: const BouncingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 16.h,
                    crossAxisSpacing: 12.h,
                    childAspectRatio: 4 / 4),
                itemCount: _cubit.listStorySearch.length,
                itemBuilder: (BuildContext ctx, index) {
                  NewsData data = _cubit.listStorySearch[index];
                  return GestureDetector(
                    onTap: () {
                      NavigationUtils.rootNavigatePage(
                          context,
                          DetailStoryPage(
                            _cubit.listStorySearch[index].id,
                          ));
                    },
                    child: buildItemCourse(
                        imageUrl: data.imageUrl, title: data.title),
                  );
                });
  }

  Widget buildItemCourse({String? imageUrl, String? title}) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.all(4.h),
          decoration: BoxDecoration(
              color: R.color.white, borderRadius: BorderRadius.circular(8.h)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(8.h),
                  child: CachedNetworkImage(
                    height: 108.h,
                    width: double.infinity,
                    imageUrl: imageUrl ?? "",
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      height: 108.h,
                      width: double.infinity,
                    ),
                    errorWidget: (context, url, error) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      height: 108.h,
                      width: double.infinity,
                    ),
                  )),
              SizedBox(height: 4.h),
              Text(
                title ?? "",
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.medium500.copyWith(
                    fontSize: 12.sp,
                    color: R.color.neutral1,
                    height: 16.h / 12.sp),
              ),
              SizedBox(height: 6.h),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildPopupMenu() {
    return PopupMenuButton<PopMenu>(
        offset: Offset(0, 32.h),
        constraints:
            BoxConstraints(minWidth: MediaQuery.of(context).size.width * 0.55),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: R.color.hintGray),
          borderRadius: BorderRadius.circular(10),
        ),
        onSelected: (PopMenu item) {
          setState(() {
            _selectedMenu = item.title();
            _cubit.onOrder();
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.h),
              color: _cubit.isOrder ? R.color.primaryColor : R.color.white),
          padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 20.h),
          child: Image.asset(
            R.drawable.ic_filter_list,
            height: 20.h,
            color: _cubit.isOrder ? R.color.white : R.color.primaryColor,
          ),
        ),
        itemBuilder: (BuildContext context) => List.generate(
            2,
            (index) => PopupMenuItem<PopMenu>(
                  enabled: false,
                  value: PopMenu.values[index],
                  child: StatefulBuilder(
                    builder:
                        (BuildContext context, StateSetter setStateRadio) =>
                            ValueListenableBuilder(
                      valueListenable: _cubit.radioChoice,
                      builder: (context, List<bool> value, _) => Container(
                        width: context.width * 0.7,
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                _cubit.selectFilter(index);
                                if (index == 0) {
                                  _cubit.clearFilter(index: 1);
                                } else if (index == 1) {
                                  _cubit.clearFilter(index: 0);
                                }
                              },
                              child: Row(
                                children: [
                                  Radio(
                                    value: _cubit.radioChoice.value[index],
                                    groupValue: true,
                                    onChanged: (value) {
                                      setStateRadio(() {
                                        _cubit.selectFilter(index);
                                      });
                                    },
                                  ),
                                  Text(
                                    _cubit.title[index],
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 14.sp,
                                            color:
                                                _cubit.radioChoice.value[index]
                                                    ? R.color.primaryColor
                                                    : R.color.black),
                                  ),
                                  Spacer(),
                                  index == 0
                                      ? _cubit.isSelect == 0
                                          ? Icon(
                                              Icons.arrow_drop_down_rounded,
                                            )
                                          : Icon(
                                              Icons.arrow_drop_up_rounded,
                                            )
                                      : const SizedBox.shrink(),
                                  index == 1
                                      ? _cubit.isSelect == 1
                                          ? Icon(
                                              Icons.arrow_drop_down_rounded,
                                            )
                                          : Icon(
                                              Icons.arrow_drop_up_rounded,
                                            )
                                      : const SizedBox.shrink()
                                ],
                              ),
                            ),
                            _cubit.isSelect == 0 && index == 0
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20, bottom: 5),
                                      child: Text(
                                        R.string.choose_a_suggested_category
                                            .tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelLargeText
                                            .copyWith(
                                                fontWeight: FontWeight.w300),
                                      ),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            _cubit.isSelect == 0 && index == 0
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: _searchCategoryController,
                                      style: Theme.of(context)
                                          .textTheme
                                          .regular400,
                                      decoration: InputDecoration(
                                        hintText: R.string.search.tr(),
                                        hintStyle: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(
                                                color: R.color.disableGray),
                                        border: InputBorder.none,
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: R.color.disableGray),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 10),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: R.color.disableGray),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setStateRadio(() {
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              _cubit.getCategoryNews(
                                                  searchKey:
                                                      _searchCategoryController
                                                          .text
                                                          .trim());
                                            });
                                          },
                                          icon: Icon(
                                            Icons.search,
                                            size: 20,
                                          ),
                                        ),
                                      ),
                                    ))
                                : const SizedBox.shrink(),
                            _cubit.isSelect == 0 && index == 0
                                ? ValueListenableBuilder(
                                    valueListenable:
                                        _cubit.loadingCategorySearch,
                                    builder: (context, value, _) =>
                                        _cubit.loadingCategorySearch.value
                                            ? CircularProgressIndicator()
                                            : ConstrainedBox(
                                                constraints: BoxConstraints(
                                                    maxWidth:
                                                        context.width * 0.7,
                                                    minWidth:
                                                        context.width * 0.7,
                                                    maxHeight: 180,
                                                    minHeight: 50),
                                                child: Scrollbar(
                                                  child: SingleChildScrollView(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 8),
                                                    physics:
                                                        const BouncingScrollPhysics(),
                                                    child:
                                                        _cubit.listCategoryStory
                                                                .isEmpty
                                                            ? Center(
                                                                child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top: 5),
                                                                child: Text(
                                                                  R.string
                                                                      .not_exist
                                                                      .tr(),
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .regular400,
                                                                ),
                                                              ))
                                                            : Wrap(
                                                                children: _cubit
                                                                    .listCategoryStory
                                                                    .asMap()
                                                                    .entries
                                                                    .map((e) =>
                                                                        ChoiceChip(
                                                                          avatar: _cubit.choiceChipCategory[e.key]
                                                                              ? Icon(
                                                                                  Icons.check,
                                                                                  color: R.color.zaffre,
                                                                                  size: 18,
                                                                                )
                                                                              : null,
                                                                          backgroundColor: R
                                                                              .color
                                                                              .white,
                                                                          selectedColor: R
                                                                              .color
                                                                              .white,
                                                                          label:
                                                                              Padding(
                                                                            padding:
                                                                                EdgeInsets.only(bottom: 1),
                                                                            child:
                                                                                Text(
                                                                              e.value.name ?? '',
                                                                              style: Theme.of(context).textTheme.subTitleRegular.copyWith(
                                                                                    fontSize: 12.sp,
                                                                                    color: _cubit.choiceChipCategory[e.key] ? R.color.zaffre : R.color.grey,
                                                                                  ),
                                                                            ),
                                                                          ),
                                                                          selected:
                                                                              _cubit.choiceChipCategory[e.key],
                                                                          shape: StadiumBorder(
                                                                              side: BorderSide(
                                                                            width:
                                                                                1,
                                                                            color: _cubit.choiceChipCategory[e.key]
                                                                                ? R.color.zaffre
                                                                                : R.color.grey,
                                                                          )),
                                                                          onSelected:
                                                                              (bool selected) {
                                                                            setStateRadio(() {
                                                                              _cubit.choiceChipCategory[e.key] = selected;
                                                                            });
                                                                          },
                                                                        ))
                                                                    .toList(),
                                                                spacing: 10,
                                                                alignment:
                                                                    WrapAlignment
                                                                        .start,
                                                                direction: Axis
                                                                    .horizontal,
                                                                textDirection: ui
                                                                    .TextDirection
                                                                    .ltr,
                                                              ),
                                                  ),
                                                ),
                                              ),
                                  )
                                : const SizedBox.shrink(),
                            _cubit.isSelect == 1 && index == 1
                                ? Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20, bottom: 10),
                                      child: Text(
                                        R.string.choose_suggested_topic.tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelLargeText
                                            .copyWith(
                                                fontWeight: FontWeight.w300),
                                      ),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            _cubit.isSelect == 1 && index == 1
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: SizedBox(
                                        width: context.width * 0.7,
                                        child: TextField(
                                          maxLines: 1,
                                          controller: _searchTopicController,
                                          style: Theme.of(context)
                                              .textTheme
                                              .regular400,
                                          decoration: InputDecoration(
                                            hintText: R.string.search.tr(),
                                            hintStyle: Theme.of(context)
                                                .textTheme
                                                .regular400
                                                .copyWith(
                                                    color: R.color.disableGray),
                                            border: InputBorder.none,
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: R.color.disableGray),
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    horizontal: 10),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: R.color.disableGray),
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            suffixIcon: IconButton(
                                              onPressed: () {
                                                setStateRadio(() {
                                                  FocusManager
                                                      .instance.primaryFocus
                                                      ?.unfocus();
                                                  _cubit.getPostTopicStory(
                                                      searchKey:
                                                          _searchTopicController
                                                              .text
                                                              .trim());
                                                });
                                              },
                                              icon: Icon(Icons.search),
                                            ),
                                          ),
                                        )))
                                : const SizedBox.shrink(),
                            _cubit.isSelect == 1 && index == 1
                                ? ValueListenableBuilder(
                                    valueListenable: _cubit.loadingSearch,
                                    builder: (context, value, _) =>
                                        _cubit.loadingSearch.value
                                            ? CircularProgressIndicator()
                                            : ConstrainedBox(
                                                constraints: BoxConstraints(
                                                    maxWidth:
                                                        context.width * 0.7,
                                                    minWidth:
                                                        context.width * 0.7,
                                                    maxHeight: 180,
                                                    minHeight: 50),
                                                child: Scrollbar(
                                                  child: SingleChildScrollView(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 8),
                                                    physics:
                                                        const BouncingScrollPhysics(),
                                                    child:
                                                        _cubit.listPostTopic
                                                                .isEmpty
                                                            ? Center(
                                                                child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top: 5),
                                                                child: Text(
                                                                  R.string
                                                                      .not_exist
                                                                      .tr(),
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .regular400,
                                                                ),
                                                              ))
                                                            : Wrap(
                                                                children: _cubit
                                                                    .listPostTopic
                                                                    .asMap()
                                                                    .entries
                                                                    .map((e) =>
                                                                        ChoiceChip(
                                                                          avatar: _cubit.choiceChip[e.key]
                                                                              ? Icon(
                                                                                  Icons.check,
                                                                                  color: R.color.zaffre,
                                                                                  size: 18,
                                                                                )
                                                                              : null,
                                                                          backgroundColor: R
                                                                              .color
                                                                              .white,
                                                                          selectedColor: R
                                                                              .color
                                                                              .white,
                                                                          label:
                                                                              Padding(
                                                                            padding:
                                                                                EdgeInsets.only(bottom: 1),
                                                                            child:
                                                                                Text(
                                                                              e.value?.topicName ?? '',
                                                                              style: Theme.of(context).textTheme.subTitleRegular.copyWith(
                                                                                    fontSize: 12.sp,
                                                                                    color: _cubit.choiceChip[e.key] ? R.color.zaffre : R.color.grey,
                                                                                  ),
                                                                            ),
                                                                          ),
                                                                          selected:
                                                                              _cubit.choiceChip[e.key],
                                                                          shape:
                                                                              StadiumBorder(side: BorderSide(width: 1, color: _cubit.choiceChip[e.key] ? R.color.zaffre : R.color.grey)),
                                                                          onSelected:
                                                                              (bool selected) {
                                                                            setStateRadio(() {
                                                                              _cubit.choiceChip[e.key] = selected;
                                                                            });
                                                                          },
                                                                        ))
                                                                    .toList(),
                                                                spacing: 10,
                                                                alignment:
                                                                    WrapAlignment
                                                                        .start,
                                                                direction: Axis
                                                                    .horizontal,
                                                                textDirection: ui
                                                                    .TextDirection
                                                                    .ltr,
                                                              ),
                                                  ),
                                                ),
                                              ),
                                  )
                                : const SizedBox.shrink(),
                            index == 1
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        ButtonWidget(
                                          backgroundColor: R.color.brightRed,
                                          padding:
                                              EdgeInsetsDirectional.fromSTEB(
                                                  12.w, 8.h, 12.w, 8.h),
                                          title: R.string.clear_filter.tr(),
                                          textStyle: Theme.of(context)
                                              .textTheme
                                              .bold700
                                              .copyWith(
                                                  fontSize: 12.sp,
                                                  color: R.color.white),
                                          onPressed: () {
                                            _cubit.changePopMenuTitle(0);
                                            _cubit.selectedMenu = "";
                                            _cubit.isSelect = null;
                                            _cubit.clearRadio();
                                            _cubit.clearFilter();
                                            _searchTopicController.clear();
                                            _searchCategoryController.clear();
                                            _searchController.clear();
                                            _cubit.searchStory(isRefresh: true);
                                            NavigationUtils.pop(context);
                                          },
                                        ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        ButtonWidget(
                                          backgroundColor: R.color.primaryColor,
                                          padding:
                                              EdgeInsetsDirectional.fromSTEB(
                                                  12.w, 8.h, 12.w, 8.h),
                                          title: R.string.apply_code.tr(),
                                          textStyle: Theme.of(context)
                                              .textTheme
                                              .bold700
                                              .copyWith(
                                                  fontSize: 12.sp,
                                                  color: R.color.white),
                                          onPressed: () {
                                            _cubit.changePopMenuTitle(
                                                _cubit.isSelect ?? 0);
                                            _cubit.isSelect == 0
                                                ? _cubit.getCategoryTopic()
                                                : null;
                                            _cubit.isSelect == 1
                                                ? _cubit.getPostTopic()
                                                : null;
                                            _cubit.searchStory(
                                                keyWord: _searchController.text
                                                    .trim(),
                                                topicIds: _cubit.isSelect == 1
                                                    ? _cubit.topicPostSearch
                                                    : [],
                                                typeIds: _cubit.isSelect == 0
                                                    ? _cubit.topicCategory
                                                    : [],
                                                isRefresh: true);
                                            NavigationUtils.pop(context);
                                          },
                                        )
                                      ],
                                    ),
                                  )
                                : const SizedBox.shrink()
                          ],
                        ),
                      ),
                    ),
                  ),
                )));
  }

  Widget _appBar() {
    return _cubit.isSearch
        ? Container(
            height: _appBarHeight,
            width: double.infinity,
            decoration: BoxDecoration(color: R.color.primaryColor, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.h), blurRadius: 1.h, color: R.color.gray)
            ]),
            child: Row(
              children: [
                SizedBox(
                  width: 16.w,
                ),
                GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: R.color.white,
                    size: 20.h,
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Text(
                      R.string.success_stories.tr().toUpperCase(),
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 16.sp, color: R.color.white),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _cubit.checkSearch();
                  },
                  child: Image.asset(R.drawable.ic_search,
                      width: 20.h, height: 20.h, color: R.color.white),
                ),
                SizedBox(width: 16.w),
              ],
            ),
          )
        : Stack(
            children: [
              Container(
                width: double.infinity,
                height: _appBarHeight,
                decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 2.h),
                          blurRadius: 1.h,
                          color: R.color.gray)
                    ]),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.h, top: 20.h),
                    child: GestureDetector(
                      onTap: () {
                        NavigationUtils.pop(context,
                            result: Const.ACTION_REFRESH);
                      },
                      child: Icon(CupertinoIcons.back, color: R.color.white),
                    ),
                  ),
                  Expanded(
                    child: SearchWidget(
                      onSubmit: (text) {
                        _cubit.searchStory(
                            keyWord: _searchController.text.trim(),
                            topicIds: (_cubit.isSelect == 0
                                ? _cubit.topicCategory
                                : _cubit.isSelect == 1
                                    ? _cubit.topicPostSearch
                                    : []),
                            isRefresh: true);
                      },
                      searchController: _searchController,
                      type: Const.COURSE,
                      onChange: (String) {},

                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
