import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/response/post_story_response.dart';
import 'package:imi/src/page/home_success_story/home_story_state.dart';
import 'package:imi/src/utils/enum.dart';

import '../../../res/R.dart';
import '../../data/database/app_database.dart';
import '../../data/database/sqflite/DBHelper.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/category_story_response.dart';
import '../../data/network/response/event_topics_response.dart';
import '../../data/network/response/news_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';

class HomeStoryCubit extends Cubit<HomeStoryState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool isSearch = true;
  List<NewsData> listStorySearch = [];
  ValueNotifier<List<bool>> radioChoiceTemp = ValueNotifier([false, false]);
  ValueNotifier<List<bool>> radioChoice = ValueNotifier([false, false]);
  ValueNotifier<bool> loadingSearch = ValueNotifier(false);
  ValueNotifier<bool> loadingCategorySearch = ValueNotifier(false);
  late List<bool> choiceChip;
  late List<bool> choiceChipCategory;
  List<PostTopicData?> listPostTopic = [];
  List<PostTopicData?> listPostTopicSelect = [];
  List<ByLanguageData> listCategoryStory = [];
  List<ByLanguageData> listCategoryStorySelect = [];
  List<int> topicPostSearch = [];
  List<int> topicCategory = [];
  List<String> title = [R.string.category.tr(), R.string.categories.tr()];
  String? selectedMenu;
  late AppDatabase database;
  final db = DBHelper();
  String? nextTokenSearch;
  bool isOrder = false;
  int? isSelect;

  HomeStoryCubit(this.repository, this.graphqlRepository)
      : super(HomeStoryInitial());

  void checkSearch() {
    emit(HomeStoryLoading());
    this.isSearch = !isSearch;
    emit(HomeStoryInitial());
  }

  void searchStory(
      {bool isRefresh = false,
      bool isLoadMore = false,
      String? keyWord,
      List<int>? topicIds,
      List<int>? typeIds,
      String? priceOrder,
      int? couponId}) async {
    emit(isRefresh || !isLoadMore ? HomeStoryLoading() : HomeStoryInitial());
    if (isLoadMore != true) {
      nextTokenSearch = null;
      listStorySearch.clear();
    }
    ApiResult<NewsDataFetch> getSearchStory = await graphqlRepository.getNews(
        typeCategory: NewsCategory.SUCCESS_GOAL,
        typeIds: typeIds,
        topicIds: topicIds,
        searchKey: keyWord != "\\" ? keyWord : keyWord?.replaceAll("\\", "\t"),
        nextToken: nextTokenSearch);
    getSearchStory.when(success: (NewsDataFetch data) async {
      listStorySearch.addAll(data.data ?? []);
      nextTokenSearch = data.nextToken;
      emit(SearchHomeStorySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeStoryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getPostTopicStory({
    String? searchKey,
  }) async {
    loadingSearch.value = true;
    ApiResult<FilterPostTopic> getSearchCourse =
        await graphqlRepository.getPostStory(
            searchKey: searchKey != "\\"
                ? searchKey
                : searchKey?.replaceAll("\\", "\t"));
    getSearchCourse.when(success: (FilterPostTopic data) async {
      topicPostSearch.clear();
      listPostTopic.clear();
      listPostTopicSelect.clear();
      listPostTopic.addAll(data.data ?? []);
      listPostTopicSelect.addAll(data.data ?? []);
      choiceChip = List.generate(listPostTopic.length, (index) => false);
      loadingSearch.value = false;
    }, failure: (NetworkExceptions error) async {
      emit(HomeStoryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getCategoryNews({
    String? searchKey,
  }) async {
    loadingCategorySearch.value = true;
    ApiResult<CategoriesByLanguage> getSearchCourse =
        await graphqlRepository.getCategoryStory(
            searchKey: searchKey != "\\"
                ? searchKey
                : searchKey?.replaceAll("\\", "\t"));
    getSearchCourse.when(success: (CategoriesByLanguage data) async {
      topicCategory.clear();
      listCategoryStory.clear();
      listCategoryStorySelect.clear();
      listCategoryStory.addAll(data.data ?? []);
      listCategoryStorySelect.addAll(data.data ?? []);
      choiceChipCategory =
          List.generate(listCategoryStory.length, (index) => false);
      loadingCategorySearch.value = false;
    }, failure: (NetworkExceptions error) async {
      emit(HomeStoryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void onOrder() {
    emit(HomeStoryLoading());
    isOrder = true;
    emit(HomeStoryInitial());
  }

  void selectFilter(int index) {
    emit(HomeStoryLoading());
    isSelect = index;
    for (int i = 0; i < radioChoiceTemp.value.length; i++) {
      if (index == i) {
        radioChoiceTemp.value[i] = true;
        continue;
      }
      radioChoiceTemp.value[i] = false;
    }
    radioChoice.value = List.from(radioChoiceTemp.value);
    emit(HomeStoryInitial());
  }

  void getPostTopic() {
    topicPostSearch.clear();
    for (int i = 0; i < choiceChip.length; i++) {
      if (choiceChip[i]) {
        topicPostSearch.add(listPostTopic[i]?.id ?? 0);
      }
    }
  }

  void getCategoryTopic() {
    topicCategory.clear();
    for (int i = 0; i < choiceChipCategory.length; i++) {
      if (choiceChipCategory[i]) {
        topicCategory.add(listCategoryStory[i].id ?? 0);
      }
    }
  }

  void changePopMenuTitle(int index) {
    emit(getPopupMenu());
    selectedMenu = title[index];
    emit(HomeStoryInitial());
  }

  void clearFilter({int? index}) {
    if (index == 0) {
      choiceChipCategory = choiceChipCategory.map((e) => e = false).toList();
    } else if (index == 1) {
      choiceChip = choiceChip.map((e) => e = false).toList();
    } else {
      choiceChipCategory = choiceChipCategory.map((e) => e = false).toList();
      choiceChip = choiceChip.map((e) => e = false).toList();
    }
  }

  void clearRadio() {
    radioChoice.value = radioChoice.value.map((e) => e = false).toList();
    radioChoiceTemp.value =
        radioChoiceTemp.value.map((e) => e = false).toList();
  }
}
