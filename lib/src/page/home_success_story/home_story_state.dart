
abstract class HomeStoryState {}

class HomeStoryInitial extends HomeStoryState {}

class HomeStoryLoading extends HomeStoryState {
  @override
  String toString() {
    return 'HomeStoryLoading{}';
  }
}

class getPopupMenu extends HomeStoryState {
  @override
  String toString() {
    return 'getPopupMenu{}';
  }
}

class HomeStoryFailure extends HomeStoryState {
  final String error;

  HomeStoryFailure(this.error);

  @override
  String toString() {
    return 'HomeStoryFailure{error: $error}';
  }
}

class HomeStorySuccess extends HomeStoryState {
  @override
  String toString() {
    return 'HomeStorySuccess{}';
  }
}

class SearchHomeStorySuccess extends HomeStoryState {
  @override
  String toString() {
    return 'SearchHomeStorySuccess{}';
  }
}

class ListSuccessStoryEmpty extends HomeStoryState {
  @override
  String toString() {
    return 'ListSuccessStoryEmpty{}';
  }
}
