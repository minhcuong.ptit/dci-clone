import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/src/data/network/response/related_question_response.dart';
import 'package:imi/src/page/detail_question/detail_question.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../utils/navigation_utils.dart';
import '../course_details/widget/describe_page.dart';

class DetailQuestionPage extends StatefulWidget {
  final int? questionId;
  final List<int>? topicIds;

  DetailQuestionPage({required this.questionId, required this.topicIds});

  @override
  State<DetailQuestionPage> createState() => _DetailQuestionPageState();
}

class _DetailQuestionPageState extends State<DetailQuestionPage> {
  late DetailQuestionCubit _cubit;
  RefreshController _controller = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = DetailQuestionCubit(graphqlRepository, repository,
        widget.questionId ?? 0, widget.topicIds ?? []);
    _cubit.detailQuestion();
    _cubit.getListRelatedQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.frequently_question.tr(),
          centerTitle: true,
          backgroundColor: R.color.primaryColor,
          titleColor: R.color.white,
          iconColor: R.color.white),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<DetailQuestionCubit, DetailQuestionState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, DetailQuestionState state) {
    return StackLoadingView(
      visibleLoading: state is DetailQuestionLoading,
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 12.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _cubit.questionData?.question ?? "",
                  style: Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: 18.sp,
                        height: 24.h / 18.sp,
                      ),
                ),
                SizedBox(height: 10.h),
                HtmlWidget(
                  _cubit.questionData?.answer ?? "",
                  textStyle: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 14.sp, height: 20.h / 14.sp),
                  onTapUrl: (url) async {
                    if (await canLaunch(url)) {
                      await launch(
                        url,
                      );
                    } else {
                      throw 'Could not launch $url';
                    }
                    return launch(
                      url,
                    );
                  },
                  factoryBuilder: () => MyWidgetFactory(),
                  enableCaching: true,
                ),
              ],
            ),
          ),
          SizedBox(height: 10.h),
          Container(height: 4.h, color: R.color.lightestGray),
          SizedBox(height: 10.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Text(
              R.string.related_question.tr(),
              style: Theme.of(context).textTheme.bold700.copyWith(
                    fontSize: 18.sp,
                    height: 24.h / 18.sp,
                  ),
            ),
          ),
          ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: _cubit.listRelatedQuestion.length,
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox();
            },
            itemBuilder: (BuildContext context, int index) {
              FetchQuestionsData data = _cubit.listRelatedQuestion[index];
              return InkWell(
                highlightColor: R.color.white,
                onTap: () {
                  NavigationUtils.navigatePage(
                      context,
                      DetailQuestionPage(
                        questionId: data.id,
                        topicIds: widget.topicIds,
                      ));
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.question ?? "",
                      style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 14.sp,
                            height: 24.h / 14.sp,
                          ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10.h),
                      color: R.color.lightGray,
                      height: 1.h,
                    ),
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
