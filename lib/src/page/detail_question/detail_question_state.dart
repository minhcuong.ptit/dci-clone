abstract class DetailQuestionState {
  DetailQuestionState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class DetailQuestionInitial extends DetailQuestionState {
  @override
  String toString() => 'DetailQuestionInitial';
}

class DetailQuestionSuccess extends DetailQuestionState {
  @override
  String toString() => 'DetailQuestionSuccess';
}

class DetailQuestionLoading extends DetailQuestionState {
  @override
  String toString() => 'DetailQuestionLoading';
}

class DetailQuestionFailure extends DetailQuestionState {
  final String error;

  DetailQuestionFailure(this.error);

  @override
  String toString() => 'DetailQuestionFailure { error: $error }';
}

class DetailQuestionEmpty extends DetailQuestionState {
  @override
  String toString() {
    return 'DetailQuestionEmpty{}';
  }
}
