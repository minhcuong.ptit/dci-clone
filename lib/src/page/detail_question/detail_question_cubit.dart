import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/page/detail_question/detail_question_state.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/question_response.dart';
import '../../data/network/response/related_question_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';

class DetailQuestionCubit extends Cubit<DetailQuestionState> {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;
  String? nextToken;
  FetchQuestionById? questionData;
  int questionId;
  List<int> topicIds;
  List<FetchQuestionsData> listRelatedQuestion = [];

  DetailQuestionCubit(
    this.graphqlRepository,
    this.repository,
    this.questionId,
    this.topicIds,
  ) : super(DetailQuestionInitial());

  void detailQuestion() async {
    emit(DetailQuestionLoading());
    ApiResult<FetchQuestionById> result =
        await graphqlRepository.getDetailQuestion(questionId: questionId);
    result.when(success: (FetchQuestionById data) async {
      questionData = data;
      emit(DetailQuestionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailQuestionFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListRelatedQuestion() async {
    emit(DetailQuestionLoading());
    ApiResult<RelatedQuestions> result = await graphqlRepository
        .getRelatedQuestion(excludeIds: [questionId], topicIds: topicIds);
    result.when(success: (RelatedQuestions data) async {
      listRelatedQuestion.addAll(data.data ?? []);
      emit(DetailQuestionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailQuestionFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
