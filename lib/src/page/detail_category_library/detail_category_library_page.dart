import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/home_tab/widget/item_home_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../utils/const.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/utils.dart';
import '../../widgets/search/search_widget.dart';
import '../../widgets/stack_loading_view.dart';
import '../detail_document_library/detail_document_library.dart';
import 'detail_category_library.dart';

class DetailCategoryLibrary extends StatefulWidget {
  final LibraryDocumentType libraryDocumentType;
  final String searchKey;
  final int categoryId;

  const DetailCategoryLibrary(
      {Key? key,
      required this.categoryId,
      required this.libraryDocumentType,
      required this.searchKey})
      : super(key: key);

  @override
  State<DetailCategoryLibrary> createState() => _DetailCategoryLibraryState();
}

class _DetailCategoryLibraryState extends State<DetailCategoryLibrary> {
  late DetailCategoryLibraryCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  TextEditingController _searchController = TextEditingController();
  late double _appBarHeight;
  bool isSearch = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = DetailCategoryLibraryCubit(repository, graphqlRepository);
    _cubit.getDocument(
        limit: 20,
        isPaging: true,
        categoryIds: [widget.categoryId],
        libraryDocumentType: widget.libraryDocumentType.name,
        isRefresh: true,
        keyWord: widget.searchKey);
    _appBarHeight = AppBar().preferredSize.height;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child:
          BlocConsumer<DetailCategoryLibraryCubit, DetailCategoryLibraryState>(
        listener: (context, state) {
          if (state is DetailCategoryLibraryFailure) {
            Utils.showErrorSnackBar(context, state.error);
          }
          if (state is! DetailCategoryLibraryLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          // TODO: implement listener
        },
        builder: (context, state) {
          return buildPage(context, state);
        },
      ),
    );
    ;
  }

  Widget buildPage(BuildContext context, DetailCategoryLibraryState state) {
    return Scaffold(
        backgroundColor: R.color.lightestGray,
        body: StackLoadingView(
          visibleLoading: state is DetailCategoryLibraryLoading,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: MediaQuery.of(context).padding.top,
                width: double.infinity,
                color: R.color.primaryColor,
              ),
              _appBar(),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: RefreshConfiguration(
                  enableLoadingWhenNoData: false,
                  child: SmartRefresher(
                    enablePullUp: _cubit.nextTokenSearch != null &&
                        state is DetailCategoryLibrarySuccess,
                    controller: _refreshController,
                    onRefresh: () {
                      _cubit.getDocument(
                          limit: 20,
                          isRefresh: true,
                          isPaging: true,
                          categoryIds: [widget.categoryId],
                          keyWord: isSearch
                              ? _searchController.text
                              : widget.searchKey);
                    },
                    onLoading: () {
                      _cubit.getDocument(
                          limit: 20,
                          isLoadMore: true,
                          isPaging: true,
                          categoryIds: [widget.categoryId],
                          keyWord: isSearch
                              ? _searchController.text
                              : widget.searchKey);
                    },
                    child: buildSearchListCourse(context, state),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget buildSearchListCourse(
      BuildContext context, DetailCategoryLibraryState state) {
    return state is DetailCategoryLibraryLoading
        ? const SizedBox.shrink()
        : _cubit.listDocument.length == 0
            ? Center(
                child: Text(
                  R.string.no_result_search.tr(),
                  style: Theme.of(context).textTheme.bodyRegular,
                ),
              )
            : GridView.builder(
                primary: true,
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                physics: const BouncingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 16.h,
                    crossAxisSpacing: 12.h,
                    childAspectRatio: 4 / 4),
                itemCount: _cubit.listDocument.length,
                itemBuilder: (BuildContext ctx, index) {
                  final data = _cubit.listDocument[index];
                  return GestureDetector(
                    onTap: () {
                      NavigationUtils.navigatePage(
                          context,
                          DetailDocumentPage(
                            link: data.link,
                            id: data.id,
                            isLocked: data.isLocked,
                          ));
                    },
                    child: CardItem(
                        containDate: false,
                        isVideo: data.type == DocumentType.VIDEO.name,
                        isEbook: data.type == DocumentType.E_BOOK.name,
                        isLockEbook: data.isLocked,
                        isLockVideo: data.isLocked,
                        typeVideo: data.type == DocumentType.VIDEO.name,
                        image: data.imageUrl,
                        title: data.title,
                        time: data.modifiedDate),
                  );
                });
  }

  Widget _appBar() {
    return !_cubit.showTextField
        ? Container(
            height: _appBarHeight,
            width: double.infinity,
            decoration: BoxDecoration(color: R.color.primaryColor, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.h), blurRadius: 1.h, color: R.color.gray)
            ]),
            child: Row(
              children: [
                SizedBox(
                  width: 16.w,
                ),
                GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: R.color.white,
                    size: 20.h,
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Text(
                      widget.libraryDocumentType.title,
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 16.sp, color: R.color.white),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _cubit.searching();
                  },
                  child: Image.asset(R.drawable.ic_search,
                      width: 20.h, height: 20.h, color: R.color.white),
                ),
                SizedBox(width: 16.w),
              ],
            ),
          )
        : Stack(
            children: [
              Container(
                width: double.infinity,
                height: _appBarHeight,
                decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 2.h),
                          blurRadius: 1.h,
                          color: R.color.gray)
                    ]),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.h, top: 20.h),
                    child: GestureDetector(
                      onTap: () {
                        NavigationUtils.pop(context,
                            result: Const.ACTION_REFRESH);
                      },
                      child: Icon(CupertinoIcons.back, color: R.color.white),
                    ),
                  ),
                  Expanded(
                    child: SearchWidget(
                      autoFocus: false,
                      onSubmit: (text) {
                        _cubit.getDocument(
                            limit: 20,
                            categoryIds: [widget.categoryId],
                            isPaging: true,
                            keyWord: _searchController.text,
                            isRefresh: true);
                        isSearch = true;
                      },
                      searchController: _searchController,
                      type: 'DETAIL_CATEGORY_LIBRARY',
                      onChange: (String) {},
                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
