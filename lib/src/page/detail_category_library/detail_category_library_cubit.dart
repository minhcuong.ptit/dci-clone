import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/utils/logger.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/library_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import 'detail_category_library.dart';

class DetailCategoryLibraryCubit extends Cubit<DetailCategoryLibraryState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool showTextField=false;
String? nextTokenSearch;
  List<LibraryData> listDocument = [];

  DetailCategoryLibraryCubit(this.repository, this.graphqlRepository)
      : super(DetailCategoryLibraryInitial());

  void searching() {
    emit(DetailCategoryLibraryLoading());
    showTextField = !showTextField;
    emit(DetailCategoryLibraryInitial());
  }


  void getDocument({
    bool isRefresh = false,
    bool isLoadMore = false,
    String? type,
    String? keyWord,
    bool? isPaging,
    List<int>? categoryIds,
    List<int>? topicIds,
    String? libraryDocumentType,
    String? privacy,
    int? limit,
  }) async {
    emit((isRefresh || !isLoadMore) ? DetailCategoryLibraryLoading() : DetailCategoryLibraryInitial());
    if(isLoadMore!=true){
      listDocument.clear();
      nextTokenSearch=null;
    }
    ApiResult<LibraryDataFetchDocuments> getLibrary =
    await graphqlRepository.getLibraryPage(
        limit: limit,
        categoryIds: categoryIds,
        searchKey: keyWord,
        type: type,
        libraryDocumentType: libraryDocumentType,
        isPaging:isPaging,
        topicIds: topicIds,
        privacy: privacy,
        nextToken: nextTokenSearch
    );
    getLibrary.when(success: (LibraryDataFetchDocuments data) async {
        listDocument.addAll(data.data?.toList()??[]);
        nextTokenSearch=data.nextToken;
      emit(DetailCategoryLibrarySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailCategoryLibraryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
