abstract class DetailCategoryLibraryState {}
class DetailCategoryLibraryInitial extends DetailCategoryLibraryState {}

class DetailCategoryLibraryLoading extends DetailCategoryLibraryState {
  @override
  String toString() {
    return 'DetailCategoryLibraryLoading{}';
  }
}

class DetailCategoryLibrarySuccess extends DetailCategoryLibraryState {
  @override
  String toString() {
    return 'DetailCategoryLibrarySuccess{}';
  }
}

class DetailCategoryLibraryFailure extends DetailCategoryLibraryState {
  final String error;

  DetailCategoryLibraryFailure(this.error);

  @override
  String toString() {
    return 'DetailCategoryLibraryFailure{error: $error}';
  }
}