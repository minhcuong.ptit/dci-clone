import 'package:equatable/equatable.dart';

abstract class LibraryState extends Equatable {
  @override
  List<Object> get props => [];
}

class LibraryInitial extends LibraryState {}

class LibraryLoading extends LibraryState {
  @override
  String toString() {
    return 'LibraryLoading{}';
  }
}

class LibraryFailure extends LibraryState {
  final String error;

  LibraryFailure(this.error);

  @override
  String toString() {
    return 'LibraryFailure{error: $error}';
  }
}

class LibrarySuccess extends LibraryState {
  @override
  String toString() {
    return 'LibrarySuccess{}';
  }
}

class LibraryStudentSuccess extends LibraryState {
  @override
  String toString() {
    return 'LibrarySuccess{}';
  }
}

class LibraryLeaderSuccess extends LibraryState {
  @override
  String toString() {
    return 'LibrarySuccess{}';
  }
}

class SearchLibrarySuccess extends LibraryState {
  @override
  String toString() {
    return 'LibrarySuccess{}';
  }
}

class GetDocumentTopicSuccess extends LibraryState {
  @override
  String toString() {
    return 'GetDocumentTopicSuccess{}';
  }
}