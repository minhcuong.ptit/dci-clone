import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/library/library.dart';
import 'package:imi/src/page/library/library_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';

import '../../../res/R.dart';
import '../../data/network/response/category_library_response.dart';
import '../../data/network/response/course_topics_response.dart';
import '../../data/network/response/document_topic_response.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/navigation_utils.dart';
import '../detail_library/detail_library.dart';

class LibraryCubit extends Cubit<LibraryState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<categoriesLibraryDataCategoriesByLanguageData?> documentCategories = [];
  List<documentTopicsDataFetchDocumentTopicsData?> documentTopics=[];
  List<int> categoryDocumentSearch = [];
  List<int> topicDocumentSearch = [];
  List<LibraryData> listLibrary = [];
  List<LibraryData> listDocumentPublicEbook = [];
  List<LibraryData> listDocumentPublicVideo = [];
  List<LibraryData> listDocumentStudentVideo = [];
  List<LibraryData> listDocumentStudentEbook = [];
  List<LibraryData> listDocumentLeaderVideo = [];
  List<LibraryData> listDocumentLeaderEbook = [];
  bool isFilter = false;
  int isSelect = 0;
  String format=DocumentType.VIDEO.name;
  String privacy='';
  late List<bool> choiceChipCategories;
  late List<bool> choiceChipTopics;
  ValueNotifier<bool> loadingSearchCategories = ValueNotifier(false);
  ValueNotifier<bool> loadingSearchTopic = ValueNotifier(false);
  ValueNotifier<List<bool>> radioChoiceTemp =
  ValueNotifier([true, false, false, false]);
  ValueNotifier<List<bool>> radioChoice =
  ValueNotifier([true, false, false, false]);
  ValueNotifier<List<bool>> formatChoiceTemp =
  ValueNotifier([true, false]);
  ValueNotifier<List<bool>> formatChoice =
  ValueNotifier([true, false]);
  ValueNotifier<List<bool>> privacyChoiceTemp =
  ValueNotifier([false]);
  ValueNotifier<List<bool>> privacyChoice =
  ValueNotifier([false]);
  bool noResult=false;
  List<String> title = [
    'Tài liệu',
    'Định dạng',
    'Danh mục',
    R.string.categories.tr(),
  ];
  List<String> formatTitle = [
    'Video',
    'Ebook',
  ];
  List<String> privacyTitle = [
    'Tài liệu mở',
  ];
  String? get leader => appPreferences.getString(Const.LEADER);
  String? get member => appPreferences.getString(Const.MEMBER);

  LibraryCubit(this.repository, this.graphqlRepository)
      : super(LibraryInitial());

  void initDocumentType(){
    if(member==MemberType.MEMBER.name){
      privacyTitle.add('Tài liệu dành cho học viên');
      privacyChoice.value.add(false);
      privacyChoiceTemp.value.add(false);
    }
    if(leader==MemberType.LEADER.name){
      privacyTitle.add('Tài liệu dành cho leader');
      privacyChoice.value.add(false);
      privacyChoiceTemp.value.add(false);
    }
  }

  void getDocument({
    bool isRefresh = false,
    bool isLoadMore = false,
    String? type,
    String? keyWord,
    bool? isPaging,
    List<int>? categoryIds,
    List<int>? topicIds,
    String? privacy
  }) async {
    emit((isRefresh || !isLoadMore) ? LibraryLoading() : LibraryInitial());
    listDocumentPublicEbook.clear();
    listDocumentPublicVideo.clear();
    listDocumentStudentVideo.clear();
    listDocumentStudentEbook.clear();
    listDocumentLeaderVideo.clear();
    listDocumentLeaderEbook.clear();
    ApiResult<LibraryDataFetchDocuments> getLibrary =
    await graphqlRepository.getLibraryPage(
      categoryIds: categoryIds,
      searchKey: keyWord,
      type: type,
      isPaging:isPaging,
      topicIds: topicIds,
      privacy: privacy
    );
    getLibrary.when(success: (LibraryDataFetchDocuments data) async {
      listDocumentPublicEbook=data.data?.where((element) => element.privacy==DocumentPrivacyScope.PUBLIC.name&&element.type==DocumentType.E_BOOK.name)
          .toList()??[];
      listDocumentPublicEbook.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
      listDocumentPublicVideo=data.data?.where((element) => element.privacy==DocumentPrivacyScope.PUBLIC.name&&element.type==DocumentType.VIDEO.name).toList()??[];
      listDocumentPublicVideo.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
      if(member==MemberType.MEMBER.name&&(privacy==DocumentPrivacyScope.MEMBER.name||privacy==null)){
        listDocumentStudentVideo=data.data?.where((element) =>element.privacy=='PRIVATE'&&element.type==DocumentType.VIDEO.name).toList()??[];
        listDocumentStudentVideo.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
        listDocumentStudentEbook=data.data?.where((element) => element.privacy=='PRIVATE'&&element.type==DocumentType.E_BOOK.name).toList()??[];
        listDocumentStudentEbook.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
      }
      if(leader==MemberType.LEADER.name&&(privacy==DocumentPrivacyScope.LEADER.name||privacy==null)){
        listDocumentLeaderVideo=data.data?.where((element) => element.isLeader==true&&element.type==DocumentType.VIDEO.name).toList()??[];
        listDocumentLeaderVideo.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
        listDocumentLeaderEbook=data.data?.where((element) => element.isLeader==true&&element.type==DocumentType.E_BOOK.name).toList()??[];
        listDocumentLeaderEbook.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
      }
      getResults();
      emit(LibrarySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(LibraryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

 void getResults(){
    if(listDocumentPublicEbook.isEmpty&&listDocumentPublicVideo.isEmpty&&listDocumentStudentVideo.isEmpty&&listDocumentStudentEbook.isEmpty
    &&listDocumentLeaderVideo.isEmpty&&listDocumentLeaderEbook.isEmpty
    ){
      noResult=true;
    }
    else{
      noResult=false;
    }
 }


  void getCategoryLibrary({
    String? searchKey,
    String? type
  }) async {
    loadingSearchCategories.value = true;
    final getSearchCourse =
    await graphqlRepository.getCategoryLibrary(type: type,searchKey: searchKey);
    getSearchCourse.when(success: (categoriesLibraryData data) async {
      categoryDocumentSearch.clear();
      documentCategories.clear();
      documentCategories.addAll(data.categoriesByLanguage?.data ?? []);
      choiceChipCategories = List.generate(documentCategories.length, (index) => false);
      loadingSearchCategories.value = false;
      emit(GetDocumentTopicSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(LibraryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getTopicsLibrary({
    String? searchKey,
  }) async {
    loadingSearchTopic.value = true;
    final getSearchCourse =
    await graphqlRepository.getTopicLibrary(searchKey: searchKey);
    getSearchCourse.when(success: (documentTopicsData data) async {
      topicDocumentSearch.clear();
      documentTopics.clear();
      documentTopics.addAll(data.fetchDocumentTopics?.data ?? []);
      choiceChipTopics = List.generate(documentTopics.length, (index) => false);
      loadingSearchTopic.value = false;
      emit(GetDocumentTopicSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(LibraryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectFilter(int index) {
    emit(LibraryLoading());
    isSelect = index;
    for (int i = 0; i < radioChoiceTemp.value.length; i++) {
      if (index == i) {
        radioChoiceTemp.value[i] = true;
        continue;
      }
      radioChoiceTemp.value[i] = false;
    }
    radioChoice.value = List.from(radioChoiceTemp.value);
    emit(LibraryInitial());
  }

  void selectFormat(int index) {
    emit(LibraryLoading());
    for (int i = 0; i < formatChoiceTemp.value.length; i++) {
      if (index == i) {
        formatChoiceTemp.value[i] = true;
        if(i==0){
          format=DocumentType.VIDEO.name;
          continue;
        }
        format=DocumentType.E_BOOK.name;
        continue;
      }
      formatChoiceTemp.value[i] = false;
    }
    formatChoice.value = List.from(formatChoiceTemp.value);
    emit(LibraryInitial());
  }

  void selectPrivacy(int index) {
    emit(LibraryLoading());
    for (int i = 0; i < privacyChoiceTemp.value.length; i++) {
      if(index==-1){
        privacyChoiceTemp.value[i] = false;
        privacy='';
        continue;
      }
      if (index == i) {
        privacyChoiceTemp.value[i] = true;
        if(i==0){
          privacy=DocumentPrivacyScope.PUBLIC.name;
          continue;
        }
        if(i==1){
          privacy=DocumentPrivacyScope.MEMBER.name;
          continue;
        }
        privacy=DocumentPrivacyScope.LEADER.name;
        continue;
      }
      privacyChoiceTemp.value[i] = false;
    }
    privacyChoice.value = List.from(privacyChoiceTemp.value);
    emit(LibraryInitial());
  }

   filterCategory() {
     categoryDocumentSearch.clear();
    for (int i = 0; i < choiceChipCategories.length; i++) {
      if (choiceChipCategories[i]) {
        categoryDocumentSearch.add(documentCategories[i]?.id ?? 0);
      }
    }
    return categoryDocumentSearch;
  }

  filterTopic() {
    topicDocumentSearch.clear();
    for (int i = 0; i < choiceChipTopics.length; i++) {
      if (choiceChipTopics[i]) {
        topicDocumentSearch.add(documentCategories[i]?.id ?? 0);
      }
    }
    return topicDocumentSearch;
  }

  void refreshPage(String text){
    if (isSelect == 0) {
      if(privacy==''){
        getDocument(isRefresh: true, isPaging: false,keyWord:text);
        return;
      }
      getDocument(
          isRefresh: true,
          privacy: privacy,
          isPaging: false,
          keyWord: text);
    }
    if (isSelect == 1) {
      getDocument(
          isRefresh: true,
          type: format,
          isPaging: false,
          keyWord: text);
    }
    if (isSelect == 2) {
      getDocument(
          isRefresh: true,
          categoryIds: filterCategory(),
          isPaging: false,
          keyWord: text);
    }
    if (isSelect == 3) {
      getDocument(
          isRefresh: true,
          categoryIds: filterTopic(),
          isPaging: false,
          keyWord: text);
    }
  }

  void refreshVideo() {
    emit(LibraryLoading());
    emit(LibraryInitial());
  }
}
