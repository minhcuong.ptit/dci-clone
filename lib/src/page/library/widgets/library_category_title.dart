import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';
class CategoryTitle extends StatelessWidget {
  final String title;
  final VoidCallback? showMore;
  const CategoryTitle({Key? key, required this.title, this.showMore}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 20.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              title ?? "",
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context)
                  .textTheme
                  .buttonLink
                  .copyWith(fontSize: 14.sp),
            ),
          ),
          GestureDetector(
            onTap: showMore,
            child: Text(
              R.string.more.tr(),
              style: Theme.of(context)
                  .textTheme
                  .bodyMediumText
                  .copyWith(fontSize: 14.sp, color: R.color.primaryColor),
            ),
          ),
        ],
      ),
    );;
  }
}
