import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/home_tab/widget/item_home_page.dart';

import '../../../data/network/response/library_response.dart';
import '../../../utils/enum.dart';
import '../../../utils/navigation_utils.dart';
import '../../detail_document_library/detail_document_library.dart';
import 'library_category_title.dart';

class CategoryLibrary extends StatelessWidget {
  final List<LibraryData> category;
  final String title;
  final VoidCallback? showMore;

  const CategoryLibrary(
      {Key? key, required this.category, required this.title, this.showMore})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return category.length != 0
        ? Column(
      crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CategoryTitle(
                title: title,
                showMore: showMore,
              ),
             const SizedBox(
                height: 16,
              ),
              Container(
                height: 185.h,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: category.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, int index) {
                      return GestureDetector(
                        onTap: () {
                          NavigationUtils.navigatePage(
                              context,
                              DetailDocumentPage(
                                link: category[index].link,
                                id: category[index].id,
                                isLocked: category[index].isLocked,
                              ));
                        },
                        child: CardItem(
                            containDate: false,
                            isVideo:
                                category[index].type == DocumentType.VIDEO.name,
                            isEbook: category[index].type ==
                                DocumentType.E_BOOK.name,
                            isLockEbook: category[index].isLocked,
                            isLockVideo: category[index].isLocked,
                            typeVideo:
                                category[index].type == DocumentType.VIDEO.name,
                            image: category[index].imageUrl,
                            title: category[index].title,
                            time: category[index].modifiedDate),
                      );
                    }),
              ),
              const SizedBox(
                height: 20,
              )
            ],
          )
        : const SizedBox.shrink();
  }
}
