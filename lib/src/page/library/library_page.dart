import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_page.dart';
import 'package:imi/src/page/detail_library/detail_library.dart';
import 'package:imi/src/page/library/library.dart';
import 'package:imi/src/page/library/widgets/category.dart';
import 'package:imi/src/page/library/widgets/library_category_title.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/search/search_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/search_popup_menu.dart';
import '../main/main.dart';

class LibraryPage extends StatefulWidget {
  const LibraryPage({Key? key}) : super(key: key);

  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {
  late LibraryCubit _cubit;
  RefreshController _refreshController = RefreshController();
  TextEditingController _searchController = TextEditingController();
  TextEditingController _searchDocumentCategoryController =
      TextEditingController();
  TextEditingController _searchDocumentTopicController =
      TextEditingController();
  late MainCubit _mainCubit;
  late double _appBarHeight;
  bool _isSearch = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _mainCubit = GetIt.I();
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    _appBarHeight = AppBar().preferredSize.height;
    _cubit = BlocProvider.of<LibraryCubit>(context);
    _cubit = LibraryCubit(repository, appGraphqlRepository);
    _cubit.initDocumentType();
    _cubit.getCategoryLibrary(type: CategoryType.DOCUMENT.name);
    _cubit.getTopicsLibrary();
    _cubit.getDocument(isRefresh: true, isPaging: false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _refreshController.dispose();
    _searchController.dispose();
    _searchDocumentTopicController.dispose();
    _cubit.close();
    super.dispose();
  }

  void NavigateToCategoryLibrary(context,LibraryDocumentType libraryDocumentType){
    NavigationUtils.navigatePage(
      context,
      DocumentCategoryPage(
          categoryIds: _cubit.isSelect==2?_cubit.filterCategory():[],
          type: libraryDocumentType,
          keyWord: _searchController.text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<LibraryCubit, LibraryState>(
        listener: (BuildContext context, state) {
          if (state is! LibraryLoading) {
            _refreshController.loadComplete();
            _refreshController.refreshCompleted();
          }
          if(state is LibraryFailure){
            Utils.showSnackBar(context, state.error);
          }
        },
        builder: (BuildContext context, LibraryState state) {
          return buildPage(context, state);
        },
      ),
    );
  }

  Widget buildPage(BuildContext context, LibraryState state) {
    return GestureDetector(
      onTap: () {
        Utils.hideKeyboard(context);
      },
      child: Scaffold(
        backgroundColor: R.color.lightestGray,
        appBar: AppBar(
          titleSpacing: 0,
          backgroundColor: R.color.primaryColor,
          toolbarHeight: 60.0,
          centerTitle: true,
          leading: GestureDetector(
            onTap: () async {
              _mainCubit.pop();
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Icon(Icons.arrow_back_ios),
            ),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      _isSearch = !_isSearch;
                    });
                  },
                  child: !_isSearch
                      ? Icon(Icons.search)
                      : const SizedBox.shrink()),
            ),
          ],
          title: _isSearch
              ? Container(
                  width: double.infinity,
                  height: _appBarHeight,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: R.color.white,
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(0, 2.h),
                            blurRadius: 1.h,
                            color: R.color.gray)
                      ]),
                  child: TextField(
                    controller: _searchController,
                    cursorColor: Colors.black,
                    onSubmitted: (text) => _cubit.getDocument(
                        keyWord: _searchController.text, isRefresh: true),
                    decoration: InputDecoration(
                        hintText: R.string.search.tr(),
                        hintStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: BorderSide(color: R.color.blue)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: BorderSide(color: R.color.blue)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: BorderSide(color: R.color.blue))),
                    style: Theme.of(context).textTheme.bodyRegular.copyWith(
                        color: R.color.black,
                        height: 20 / 14,
                        fontSize: 14,
                        fontWeight: FontWeight.w300),
                  ),
                )
              : Text(
                  R.string.library_dci.tr().toUpperCase(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 20.sp, color: R.color.white),
                ),
        ),
        body: Column(
          children: [
            Expanded(
              child: StackLoadingView(
                visibleLoading: state is LibraryLoading,
                child: SmartRefresher(
                  controller: _refreshController,
                  onRefresh: () {
                  _cubit.refreshPage(_searchController.text);
                  },
                  child: state is LibraryLoading
                      ? const SizedBox.shrink()
                      : buildListView(state),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildListView(LibraryState state) {
    return ListView(
      padding: EdgeInsets.only(left: 20.h, top: 15.h, bottom: 20.h),
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Row(
            children: [
              Spacer(),
              SizedBox(
                  width: 70,
                  child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _cubit.isFilter = !_cubit.isFilter;
                        });
                      },
                      child: buildPopupMenu(state))),
            ],
          ),
        ),
        _cubit.noResult&&state is LibrarySuccess
            ? Padding(
              padding: EdgeInsets.only(top: context.height*0.3),
              child: Center(
                child: Text(
                  R.string.no_result_search.tr(),
                  style: Theme.of(context).textTheme.bodyRegular,
                ),
              ),
            )
            : const SizedBox.shrink(),
        SizedBox(height: 12.h),
        CategoryLibrary(
          title: R.string.video_opening_document.tr(),
          showMore: () => NavigateToCategoryLibrary(context,LibraryDocumentType.PUBLIC_VIDEO),
          category: _cubit.listDocumentPublicVideo,
        ),
        CategoryLibrary(
          title: R.string.ebook_opening_document.tr(),
          showMore: () =>  NavigateToCategoryLibrary(context,LibraryDocumentType.PUBLIC_EBOOK),
          category: _cubit.listDocumentPublicEbook,
        ),
        CategoryLibrary(
          title: R.string.video_material_for_students.tr(),
          showMore: () => NavigateToCategoryLibrary(context,LibraryDocumentType.DOCUMENT_MEMBER_VIDEO),
          category: _cubit.listDocumentStudentVideo,
        ),
        CategoryLibrary(
          title: R.string.ebook_material_for_students.tr(),
          showMore: () => NavigateToCategoryLibrary(context,LibraryDocumentType.DOCUMENT_MEMBER_EBOOK),
          category: _cubit.listDocumentStudentEbook,
        ),
        if (_cubit.leader == MemberType.LEADER.name) ...[
          CategoryLibrary(
            title: R.string.video_material_for_leader.tr(),
            showMore: () => NavigateToCategoryLibrary(context,LibraryDocumentType.DOCUMENT_LEADER_VIDEO),
            category: _cubit.listDocumentLeaderVideo.toList(),
          ),
          CategoryLibrary(
            title: R.string.ebook_material_for_leader.tr(),
            showMore: () => NavigateToCategoryLibrary(context,LibraryDocumentType.DOCUMENT_LEADER_EBOOK),
            category: _cubit.listDocumentLeaderEbook.toList(),
          ),
        ]
      ],
    );
  }

  Widget buildPopupMenu(LibraryState state) {
    return PopupMenuButton<PopMenu>(
        offset: Offset(0, 32.h),
        constraints: BoxConstraints(minWidth: context.width * 0.55),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: R.color.hintGray),
          borderRadius: BorderRadius.circular(10),
        ),
        onSelected: (PopMenu item) {
          setState(() {
            // _selectedMenu = item.title();
            // _cubit.onOrder();
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.h),
              color: _cubit.isFilter ? R.color.primaryColor : R.color.white),
          padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 20.h),
          child: Image.asset(
            R.drawable.ic_filter_list,
            height: 20.h,
            color: _cubit.isFilter ? R.color.white : R.color.primaryColor,
          ),
        ),
        itemBuilder: (BuildContext context) => List.generate(
              4,
              (index) {
                return PopupMenuItem<PopMenu>(
                  enabled: false,
                  // value: PopMenu.values[index],
                  child: StatefulBuilder(
                    builder:
                        (BuildContext context, StateSetter setStateRadio) =>
                            ValueListenableBuilder(
                      valueListenable: _cubit.radioChoice,
                      builder: (context, List<bool> value, _) => Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              _cubit.selectFilter(index);
                            },
                            child: Row(
                              children: [
                                Radio(
                                  value: _cubit.radioChoice.value[index],
                                  groupValue: true,
                                  onChanged: (value) {
                                    setStateRadio(() {
                                      _cubit.selectFilter(index);
                                    });
                                  },
                                ),
                                Text(
                                  _cubit.title[index],
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 14.sp,
                                          color: _cubit.radioChoice.value[index]
                                              ? R.color.primaryColor
                                              : R.color.black),
                                ),
                                Spacer(),
                                _cubit.isSelect == index
                                    ? Icon(Icons.arrow_drop_up_rounded)
                                    : Icon(Icons.arrow_drop_down_rounded)
                              ],
                            ),
                          ),
                          _cubit.isSelect == 0 && index == 0
                              ? ValueListenableBuilder(
                                  valueListenable: _cubit.privacyChoice,
                                  builder: (context, List<bool> value, _) =>
                                      Column(
                                          children: _cubit.privacyChoice.value
                                              .asMap()
                                              .entries
                                              .map(
                                                (e) => Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20),
                                                  child: Row(
                                                    children: [
                                                      Radio(
                                                        value: _cubit
                                                            .privacyChoice
                                                            .value[e.key],
                                                        groupValue: true,
                                                        onChanged: (value) {
                                                          setStateRadio(() {
                                                            _cubit
                                                                .selectPrivacy(
                                                                    e.key);
                                                          });
                                                        },
                                                      ),
                                                      Text(
                                                        _cubit.privacyTitle[
                                                            e.key],
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .regular400
                                                            .copyWith(
                                                                fontSize: 14.sp,
                                                                color: _cubit
                                                                            .privacyChoice
                                                                            .value[
                                                                        e.key]
                                                                    ? R.color
                                                                        .primaryColor
                                                                    : R.color
                                                                        .black),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                              .toList()),
                                )
                              : const SizedBox(),
                          _cubit.isSelect == 1 && index == 1
                              ? ValueListenableBuilder(
                                  valueListenable: _cubit.formatChoice,
                                  builder: (context, List<bool> value, _) =>
                                      Column(
                                          children: _cubit.formatChoice.value
                                              .asMap()
                                              .entries
                                              .map(
                                                (e) => Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20),
                                                  child: Row(
                                                    children: [
                                                      Radio(
                                                        value: _cubit
                                                            .formatChoice
                                                            .value[e.key],
                                                        groupValue: true,
                                                        onChanged: (value) {
                                                          setStateRadio(() {
                                                            _cubit.selectFormat(
                                                                e.key);
                                                          });
                                                        },
                                                      ),
                                                      Text(
                                                        _cubit
                                                            .formatTitle[e.key],
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .regular400
                                                            .copyWith(
                                                                fontSize: 14.sp,
                                                                color: _cubit
                                                                            .formatChoice
                                                                            .value[
                                                                        e.key]
                                                                    ? R.color
                                                                        .primaryColor
                                                                    : R.color
                                                                        .black),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                              .toList()),
                                )
                              : const SizedBox(),
                          _cubit.isSelect == 2 && index == 2
                              ? Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, bottom: 10),
                                    child: Text(
                                      R.string.choose_suggested_category.tr(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelLargeText
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 2 && index == 2
                              ? SearchPopupMenu(
                                  textEditingController:
                                      _searchDocumentCategoryController,
                                  search: () {
                                    setStateRadio(() {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      _cubit.getCategoryLibrary(
                                          searchKey:
                                              _searchDocumentCategoryController
                                                  .text);
                                    });
                                  },
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 2 && index == 2
                              ? ValueListenableBuilder(
                                  valueListenable:
                                      _cubit.loadingSearchCategories,
                                  builder: (context, value, _) => _cubit
                                          .loadingSearchCategories.value
                                      ? CircularProgressIndicator()
                                      : ConstrainedBox(
                                          constraints: BoxConstraints(
                                              maxWidth: context.width * 0.7,
                                              maxHeight: 140,
                                              minHeight: 50),
                                          child: Scrollbar(
                                            child: NotificationListener<
                                                ScrollUpdateNotification>(
                                              // onNotification: (notification) {
                                              //   //How many pixels scrolled from pervious frame
                                              //   if(notification.metrics.atEdge){
                                              //     if(notification.metrics.pixels!=0&&state is GetTopicSuccess){
                                              //       //Loadmore
                                              //     }
                                              //   }
                                              //   return true;
                                              // },
                                              child: SingleChildScrollView(
                                                physics:
                                                    const BouncingScrollPhysics(),
                                                child: _cubit.documentCategories
                                                        .isEmpty
                                                    ? Center(
                                                        child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 5),
                                                        child: Text(
                                                          R.string.not_exist
                                                              .tr(),
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .regular400,
                                                        ),
                                                      ))
                                                    : Wrap(
                                                        children: _cubit
                                                            .documentCategories
                                                            .asMap()
                                                            .entries
                                                            .map(
                                                                (e) =>
                                                                    ChoiceChip(
                                                                      avatar: _cubit
                                                                              .choiceChipCategories[e.key]
                                                                          ? Icon(
                                                                              Icons.check,
                                                                              color: R.color.zaffre,
                                                                            )
                                                                          : null,
                                                                      backgroundColor: R
                                                                          .color
                                                                          .white,
                                                                      selectedColor: R
                                                                          .color
                                                                          .white,
                                                                      label:
                                                                          Padding(
                                                                        padding:
                                                                            EdgeInsets.only(bottom: 1),
                                                                        child:
                                                                            Text(
                                                                          e.value?.name ??
                                                                              '',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .subTitleRegular
                                                                              .copyWith(color: _cubit.choiceChipCategories[e.key] ? R.color.zaffre : R.color.grey),
                                                                        ),
                                                                      ),
                                                                      selected:
                                                                          _cubit
                                                                              .choiceChipCategories[e.key],
                                                                      shape: StadiumBorder(
                                                                          side: BorderSide(
                                                                              width: 1,
                                                                              color: _cubit.choiceChipCategories[e.key] ? R.color.zaffre : R.color.grey)),
                                                                      onSelected:
                                                                          (bool
                                                                              selected) {
                                                                        setStateRadio(
                                                                            () {
                                                                          _cubit.choiceChipCategories[e.key] =
                                                                              selected;
                                                                        });
                                                                      },
                                                                    ))
                                                            .toList(),
                                                        spacing: 10,
                                                      ),
                                              ),
                                            ),
                                          ),
                                        ),
                                )
                              : const SizedBox.shrink(),
                          //=========================Topic====================//
                          _cubit.isSelect == 3 && index == 3
                              ? Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, bottom: 10),
                                    child: Text(
                                      R.string.choose_suggested_topic.tr(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelLargeText
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 3 && index == 3
                              ? SearchPopupMenu(
                                  textEditingController:
                                      _searchDocumentTopicController,
                                  search: () {
                                    setStateRadio(() {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      _cubit.getTopicsLibrary(
                                          searchKey:
                                              _searchDocumentTopicController
                                                  .text);
                                    });
                                  },
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 3 && index == 3
                              ? ValueListenableBuilder(
                                  valueListenable: _cubit.loadingSearchTopic,
                                  builder: (context, value, _) =>
                                      _cubit.loadingSearchTopic.value
                                          ? CircularProgressIndicator()
                                          : ConstrainedBox(
                                              constraints: BoxConstraints(
                                                  maxWidth: context.width * 0.7,
                                                  maxHeight: 140,
                                                  minHeight: 50),
                                              child: Scrollbar(
                                                child: NotificationListener<
                                                    ScrollUpdateNotification>(
                                                  // onNotification: (notification) {
                                                  //   //How many pixels scrolled from pervious frame
                                                  //   if(notification.metrics.atEdge){
                                                  //     if(notification.metrics.pixels!=0&&state is GetTopicSuccess){
                                                  //       //Loadmore
                                                  //     }
                                                  //   }
                                                  //   return true;
                                                  // },
                                                  child: SingleChildScrollView(
                                                    physics:
                                                        const BouncingScrollPhysics(),
                                                    child:
                                                        _cubit.documentTopics
                                                                .isEmpty
                                                            ? Center(
                                                                child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top: 5),
                                                                child: Text(
                                                                  R.string
                                                                      .not_exist
                                                                      .tr(),
                                                                  style: Theme.of(
                                                                          context)
                                                                      .textTheme
                                                                      .regular400,
                                                                ),
                                                              ))
                                                            : Wrap(
                                                                children: _cubit
                                                                    .documentTopics
                                                                    .asMap()
                                                                    .entries
                                                                    .map((e) =>
                                                                        ChoiceChip(
                                                                          avatar: _cubit.choiceChipTopics[e.key]
                                                                              ? Icon(Icons.check, color: R.color.zaffre)
                                                                              : null,
                                                                          backgroundColor: R
                                                                              .color
                                                                              .white,
                                                                          selectedColor: R
                                                                              .color
                                                                              .white,
                                                                          label:
                                                                              Padding(
                                                                            padding:
                                                                                EdgeInsets.only(bottom: 1),
                                                                            child:
                                                                                Text(
                                                                              e.value?.topicName ?? '',
                                                                              style: Theme.of(context).textTheme.subTitleRegular.copyWith(color: _cubit.choiceChipTopics[e.key] ? R.color.zaffre : R.color.grey),
                                                                            ),
                                                                          ),
                                                                          selected:
                                                                              _cubit.choiceChipTopics[e.key],
                                                                          shape:
                                                                              StadiumBorder(side: BorderSide(width: 1, color: _cubit.choiceChipTopics[e.key] ? R.color.zaffre : R.color.grey)),
                                                                          onSelected:
                                                                              (bool selected) {
                                                                            setStateRadio(() {
                                                                              _cubit.choiceChipTopics[e.key] = selected;
                                                                            });
                                                                          },
                                                                        ))
                                                                    .toList(),
                                                                spacing: 10,
                                                              ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                )
                              : const SizedBox.shrink(),
                          index == 3
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      ButtonWidget(
                                        backgroundColor: R.color.brightRed,
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            12.w, 8.h, 12.w, 8.h),
                                        title: R.string.clear_filter.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          _cubit.selectFilter(-1);
                                          _cubit.selectPrivacy(0);
                                          _cubit.selectFormat(0);
                                          _searchDocumentTopicController
                                              .clear();
                                          _searchController.clear();
                                          _cubit.getCategoryLibrary();
                                          _cubit.getTopicsLibrary();
                                          _cubit.getDocument(
                                            isRefresh: true,
                                            isPaging: false,
                                          );
                                          NavigationUtils.pop(context);
                                        },
                                      ),
                                      const SizedBox(
                                        width: 15,
                                      ),
                                      ButtonWidget(
                                        backgroundColor: R.color.primaryColor,
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            12.w, 8.h, 12.w, 8.h),
                                        title: R.string.apply_code.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          if (_cubit.radioChoice.value[0]) {
                                            _cubit.getDocument(
                                                privacy: _cubit.privacy,
                                                isPaging: false,
                                                keyWord:
                                                    _searchController.text);
                                          }
                                          if (_cubit.radioChoice.value[1]) {
                                            _cubit.getDocument(
                                                type: _cubit.format,
                                                isPaging: false,
                                                keyWord:
                                                    _searchController.text);
                                          }
                                          if (_cubit.radioChoice.value[2]) {
                                            _cubit.getDocument(
                                                categoryIds:
                                                    _cubit.filterCategory(),
                                                isPaging: false,
                                                keyWord:
                                                    _searchController.text);
                                          }
                                          if (_cubit.radioChoice.value[3]) {
                                            _cubit.getDocument(
                                                categoryIds:
                                                    _cubit.filterTopic(),
                                                isPaging: false,
                                                keyWord:
                                                    _searchController.text);
                                          }
                                          NavigationUtils.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                )
                              : const SizedBox.shrink()
                        ],
                      ),
                    ),
                  ),
                );
              },
            ));
  }
}
