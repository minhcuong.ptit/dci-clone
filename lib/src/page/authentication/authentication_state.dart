import 'package:equatable/equatable.dart';

abstract class AuthenticationState {
  AuthenticationState() : super();
}

class AuthenticationUninitialized extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationOnBoarding extends AuthenticationState {
  @override
  String toString() => 'AuthenticationOnBoarding';
}

class AuthenticationAuthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationAuthenticated {}';
}

class AuthenticationUnauthenticated extends AuthenticationState {
  @override
  String toString() {
    return 'AuthenticationUnauthenticated';
  }
}

class AuthenticationWelcome extends AuthenticationState {
  @override
  String toString() {
    return 'AuthenticationWelcome';
  }
}

class AuthenticationLogin extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLogin';
}

class AuthenticationChooseCategory extends AuthenticationState {
  @override
  String toString() => 'AuthenticationChooseCategory';
}

class AuthenticationSignUp extends AuthenticationState {
  @override
  String toString() => 'AuthenticationSignUp';
}

class AuthenticationLoading extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLoading';
}

class ResetState extends AuthenticationState {}

class ChangeThemeSuccess extends AuthenticationState {
  final bool isDark;

  ChangeThemeSuccess(this.isDark);
  @override
  String toString() => 'ChangeThemeSuccess{isDark:$isDark}';
}
