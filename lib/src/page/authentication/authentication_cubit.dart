import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/main.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:injectable/injectable.dart';

import '../../data/network/response/person_profile.dart';

@LazySingleton()
class AuthenticationCubit extends Cubit<AuthenticationState> {
  AuthenticationCubit() : super(AuthenticationUninitialized());

  bool? get isFirstLaunch => appPreferences.getBool(Const.FIRST_LAUNCH);
  bool get isLoggedIn => appPreferences.isLoggedIn;
  String? get email => appPreferences.email;
  bool get isSelectedFavorite =>
      !Utils.isEmpty(appPreferences.getListString(Const.FAVORITE_CATEGORY));

  void openApp() {
    emit(AuthenticationUninitialized());
    if (isLoggedIn && (email?.isNotEmpty ?? false)) {
      emit(AuthenticationAuthenticated());
      return;
    }
    if (isFirstLaunch == false) {
      //emit(AuthenticationAuthenticated());
      // emit(AuthenticationWelcome(isLoggedIn ? FlowWelcome.login : FlowWelcome.firstLaunch));
      emit(AuthenticationWelcome());
    } else {
      emit(AuthenticationWelcome());
    }
  }

  void openWelcome() {
    emit(AuthenticationWelcome());
  }

  void welcomeToHome() {
    emit(AuthenticationUninitialized());
    emit(AuthenticationAuthenticated());
  }

  void welcomeToCategory() {
    emit(AuthenticationWelcome());
    emit(AuthenticationChooseCategory());
  }

  void welcomeToLogin() {
    emit(AuthenticationWelcome());
    emit(AuthenticationUnauthenticated());
  }

  void categoryToHome() {
    emit(AuthenticationChooseCategory());
    emit(AuthenticationAuthenticated());
  }

  void login() {
    emit(AuthenticationLoading());
    emit(AuthenticationAuthenticated());
  }

  void forceLogout() {
    appPreferences.clearData();
    alice.getNavigatorKey()?.currentState?.popUntil((route) => route.isFirst);
    emit(AuthenticationUnauthenticated());
  }

  void register() {
    emit(AuthenticationLoading());
    if (isSelectedFavorite) {
      emit(AuthenticationAuthenticated());
    } else {
      emit(AuthenticationChooseCategory());
    }
  }

  void logout() {
    emit(AuthenticationLoading());
    emit(AuthenticationWelcome());
  }
}
