import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';

class TextFieldWidgetV2 extends StatefulWidget {
  final int? maxLines;
  final int? maxLength;
  final String? hintText;
  final VoidCallback? onCopy;
  final bool? isExpanding;
  final VoidCallback onExpanded;
  final BuildContext? context;
  final TextEditingController? controller;

  TextFieldWidgetV2(
      {this.maxLines,
      this.maxLength,
      this.hintText,
      this.onCopy,
      this.isExpanding,
      required this.onExpanded,
      this.context,
      this.controller});

  @override
  State<TextFieldWidgetV2> createState() => _TextFieldWidgetV2State();
}

class _TextFieldWidgetV2State extends State<TextFieldWidgetV2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.h),
      padding: EdgeInsets.symmetric(horizontal: 15.h),
      decoration: BoxDecoration(
          border: Border.all(color: R.color.grey, width: 1),
          borderRadius: BorderRadius.circular(5.h)),
      child: Column(
        children: [
          TextField(
            controller: widget.controller,
            maxLines: widget.maxLines,
            maxLength: widget.maxLength ?? 2000,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12.sp, height: 18 / 12),
              hintText: widget.hintText ?? "",
            ),
            style: Theme.of(context).textTheme.regular400,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: widget.onExpanded,
                child: widget.isExpanding == true
                    ? Image.asset(
                        R.drawable.ic_expansion,
                        height: 14.h,
                        width: 18.w,
                      )
                    : Image.asset(
                        R.drawable.ic_collapse,
                        height: 14.h,
                        width: 18.w,
                      ),
              ),
              SizedBox(width: 10.h),
              GestureDetector(
                onTap: widget.onCopy,
                child: Image.asset(
                  R.drawable.ic_copy_v2,
                  height: 18.h,
                  width: 18.w,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
