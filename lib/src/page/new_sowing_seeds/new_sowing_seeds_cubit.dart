import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/detail_plan_seed_response.dart';
import 'package:imi/src/data/network/response/image_seed_data.dart';
import 'package:imi/src/data/network/response/plant_seed_goals_response.dart';
import 'package:imi/src/data/network/response/sowing_diary_response.dart';
import 'package:imi/src/data/network/response/target_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/new_sowing_seeds/new_sowing_seeds_state.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/const.dart';

class NewSowingSeedsCubit extends Cubit<NewSowingSeedsState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<FetchPlantSeedGoals> listTarget = [];
  PlantSeedHistory? plantSeedHistory;
  int plantHistoryId;
  String? errorText;
  List<bool> isInputEmpty = List.generate(3, (index) => true);
  bool _editable = true;

  FetchPlantSeedGoals? currentTarget;
  int? goalsId;
  ImageSeedData? imageSee;

  NewSowingSeedsCubit(
      this.repository, this.graphqlRepository, this.plantHistoryId)
      : super(NewSowingSeedsInitial());

  void getSeed(
      {String? controller1,
      String? controller2,
      String? controller3,
      String? controller4,
      String? controller5,
      String? controller6,
      String? controller7,
      String? controller8,
      String? controller9,
      String? controller10,
      int? currentStep}) async {
    emit(NewSowingSeedsLoading());
    ApiResult<dynamic> getSeed = await repository.createSeed(
      SeedingDiary(
        goalsId: currentTarget?.id ?? 0,
        step1: [SeedingDiaryStep1(key: "STEP_1_1", value: controller1)],
        step2: [
          SeedingDiaryStep2(key: "STEP_2_1", value: controller2),
          SeedingDiaryStep2(key: "STEP_2_2", value: controller3),
          SeedingDiaryStep2(key: "STEP_2_3", value: controller4)
        ],
        step3: [
          SeedingDiaryStep3(key: "STEP_3_1", value: controller5),
          SeedingDiaryStep3(key: "STEP_3_2", value: controller6),
          SeedingDiaryStep3(key: "STEP_3_3", value: controller7),
        ],
        step4: [
          SeedingDiaryStep4(key: "STEP_4_1", value: controller8),
          SeedingDiaryStep4(key: "STEP_4_2", value: controller9),
          SeedingDiaryStep4(key: "STEP_4_3", value: controller10),
        ],
        steps: List.generate(currentStep ?? 0, (index) => index + 1),
      ),
    );
    getSeed.when(success: (dynamic data) {
      emit(NewSowingSeedsSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(NewSowingSeedsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  String? validateText(String value) {
    if (value.isEmpty) {
      return R.string.please_enter_data.tr();
    }
    return null;
  }

  void validateInputChange(String? pass) {
    emit(NewSowingSeedsInitial());
    errorText = validateText(pass!);
    emit(ValidateErrorPassword(errorText));
  }

  String? validateTextV2(String value1, String value2, String value3) {
    if (value1.isEmpty && value2.isEmpty && value3.isEmpty) {
      return R.string.please_enter_data.tr();
    }
    return null;
  }

  void getDetailPlanSeed() async {
    emit(NewSowingSeedsLoading());
    ApiResult<PlantSeedHistory> getTarget =
        await graphqlRepository.detailPlanSeed(plantHistoryId: plantHistoryId);
    getTarget.when(success: (PlantSeedHistory data) {
      plantSeedHistory = data;
      goalsId = plantSeedHistory?.goalsId;
      emit(DetailSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(NewSowingSeedsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getTarget() async {
    emit(NewSowingSeedsLoading());
    ApiResult<PlantSeedGoalsData> getTarget =
        await graphqlRepository.getFetchPlantSeedGoals();
    getTarget.when(success: (PlantSeedGoalsData data) {
      listTarget = data.data ?? [];
      if (currentTarget == null && listTarget.isNotEmpty) {
        if (goalsId != null) {
          currentTarget = listTarget
              .firstWhere((element) => element.id == plantSeedHistory?.goalsId);
        } else {
          currentTarget = listTarget[0];
        }
      }
      emit(TargetSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(NewSowingSeedsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void updatePlanSeed(
      {String? controller1,
      String? controller2,
      String? controller3,
      String? controller4,
      String? controller5,
      String? controller6,
      String? controller7,
      String? controller8,
      String? controller9,
      String? controller10,
      int? currentStep}) async {
    emit(NewSowingSeedsLoading());
    ApiResult<dynamic> getSeed = await repository.updatePlanSeed(
      plantHistoryId,
      SeedingDiary(
        goalsId: currentTarget?.id ?? 0,
        step1: [SeedingDiaryStep1(key: "STEP_1_1", value: controller1)],
        step2: [
          SeedingDiaryStep2(key: "STEP_2_1", value: controller2),
          SeedingDiaryStep2(key: "STEP_2_2", value: controller3),
          SeedingDiaryStep2(key: "STEP_2_3", value: controller4)
        ],
        step3: [
          SeedingDiaryStep3(key: "STEP_3_1", value: controller5),
          SeedingDiaryStep3(key: "STEP_3_2", value: controller6),
          SeedingDiaryStep3(key: "STEP_3_3", value: controller7),
        ],
        step4: [
          SeedingDiaryStep4(key: "STEP_4_1", value: controller8),
          SeedingDiaryStep4(key: "STEP_4_2", value: controller9),
          SeedingDiaryStep4(key: "STEP_4_3", value: controller10),
        ],
        steps: List.generate(currentStep ?? 0, (index) => index + 1),
      ),
    );
    getSeed.when(success: (dynamic data) {
      emit(UpdatePlanSeedSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(NewSowingSeedsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void deletePlanSeed(int plantSeedHistoryId) async {
    emit(NewSowingSeedsLoading());
    ApiResult<dynamic> deletePlan =
        await repository.deletePlanSeed(plantSeedHistoryId);
    deletePlan.when(success: (dynamic data) {
      emit(DeletePlanSeedSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DeletePlanSeedFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void copyPlanSeed(
      {String? controller1,
      String? controller2,
      String? controller3,
      String? controller4,
      String? controller5,
      String? controller6,
      String? controller7,
      String? controller8,
      String? controller9,
      String? controller10,
      int? currentStep}) async {
    emit(NewSowingSeedsLoading());
    ApiResult<dynamic> getSeed = await repository.copyPlanSeed(
      plantHistoryId,
      SeedingDiary(
        goalsId: currentTarget?.id ?? 0,
        step1: [SeedingDiaryStep1(key: "STEP_1_1", value: controller1)],
        step2: [
          SeedingDiaryStep2(key: "STEP_2_1", value: controller2),
          SeedingDiaryStep2(key: "STEP_2_2", value: controller3),
          SeedingDiaryStep2(key: "STEP_2_3", value: controller4)
        ],
        step3: [
          SeedingDiaryStep3(key: "STEP_3_1", value: controller5),
          SeedingDiaryStep3(key: "STEP_3_2", value: controller6),
          SeedingDiaryStep3(key: "STEP_3_3", value: controller7),
        ],
        step4: [
          SeedingDiaryStep4(key: "STEP_4_1", value: controller8),
          SeedingDiaryStep4(key: "STEP_4_2", value: controller9),
          SeedingDiaryStep4(key: "STEP_4_3", value: controller10),
        ],
        steps: List.generate(currentStep ?? 0, (index) => index + 1),
      ),
    );
    getSeed.when(success: (dynamic data) {
      emit(CopyPlanSeedSuccess(data));
    }, failure: (NetworkExceptions error) async {
      emit(NewSowingSeedsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  bool get editable {
    if (!_editable) return _editable;
    if (plantSeedHistory?.createdDate != null)
      return _editable &&
          DateUtils.isSameDay(plantSeedHistory?.createdDate, DateTime.now());
    return _editable;
  }

  void getImageSeed() async {
    emit(NewSowingSeedsLoading());
    ApiResult<ImageSeedData> getTarget =
        await repository.getImageSeed(plantSeedHistoryId: plantHistoryId);
    getTarget.when(success: (ImageSeedData data) async {
      imageSee = data;
      emit(DetailSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(NewSowingSeedsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void downLoadSeed() async {
    emit(NewSowingSeedsLoading());
    if (imageSee?.url != null) {
      await ImageDownloader.downloadImage(imageSee?.url ?? "");
    }
    emit(DownLoadSeedsSuccess());
  }
}
