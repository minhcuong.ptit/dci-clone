import 'package:equatable/equatable.dart';

abstract class NewSowingSeedsState extends Equatable {
  @override
  List<Object> get props => [];
}

class NewSowingSeedsInitial extends NewSowingSeedsState {}

class NewSowingSeedsLoading extends NewSowingSeedsState {
  @override
  String toString() {
    return 'NewSowingSeedsLoading{}';
  }
}

class NewSowingSeedsFailure extends NewSowingSeedsState {
  final String error;

  NewSowingSeedsFailure(this.error);

  @override
  String toString() {
    return 'NewSowingSeedsFailure{error: $error}';
  }
}

class NewSowingSeedsSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'NewSowingSeedsSuccess{}';
  }
}

class TargetSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'TargetSuccess{}';
  }
}

class CleanSeedsInputChangeState extends NewSowingSeedsState {}

class SaveSeedsSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'SaveSeedsSuccess{}';
  }
}

class ValidateErrorPassword extends NewSowingSeedsState {
  final String? error;

  ValidateErrorPassword(this.error);

  @override
  String toString() => 'ValidateErrorPassword { error: $error }';
}

class DetailSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'DetailSuccess{}';
  }
}

class UpdatePlanSeedSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'UpdatePlanSeedSuccess{}';
  }
}

class DeletePlanSeedSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'DeletePlanSeedSuccess{}';
  }
}

class DeletePlanSeedFailure extends NewSowingSeedsState {
  final String error;

  DeletePlanSeedFailure(this.error);

  @override
  String toString() {
    return 'NewSowingSeedsFailure{error: $error}';
  }
}

class CopyPlanSeedSuccess extends NewSowingSeedsState {
  final int newCopiedSeedId;

  CopyPlanSeedSuccess(this.newCopiedSeedId);
}

class DownLoadSeedsSuccess extends NewSowingSeedsState {
  @override
  String toString() {
    return 'DownLoadSeedsSuccess{}';
  }
}