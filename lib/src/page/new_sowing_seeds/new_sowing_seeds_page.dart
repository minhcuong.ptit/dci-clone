import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/plan_seed_response.dart';
import 'package:imi/src/data/network/response/plant_seed_goals_response.dart';
import 'package:imi/src/data/network/response/target_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/new_sowing_seeds/new_sowing_seeds_cubit.dart';
import 'package:imi/src/page/new_sowing_seeds/new_sowing_seeds_state.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/post/post_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/boxed_text_field.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/dci_steps_widget.dart';
import 'package:imi/src/widgets/full_screen_image_widget.dart';
import 'package:imi/src/widgets/row_button_widget.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class NewSowingSeedsPage extends StatefulWidget {
  final int? planSeedId;

  NewSowingSeedsPage({this.planSeedId});

  @override
  State<NewSowingSeedsPage> createState() => _NewSowingSeedsPageState();
}

class _NewSowingSeedsPageState extends State<NewSowingSeedsPage> {
  late NewSowingSeedsCubit _cubit;
  late TextEditingController _decideController = TextEditingController();
  late TextEditingController _fertileController = TextEditingController();
  late TextEditingController _partnerController = TextEditingController();
  late TextEditingController _planController = TextEditingController();
  late TextEditingController _directActionController = TextEditingController();
  late TextEditingController _indirectActionController =
      TextEditingController();
  late TextEditingController _timeController = TextEditingController();
  late TextEditingController _myGoodJobController = TextEditingController();
  late TextEditingController _goodDeedsOfOthersController =
      TextEditingController();
  late TextEditingController _gratefulController = TextEditingController();
  bool setLoading = false;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    _cubit = NewSowingSeedsCubit(
        repository, appGraphqlRepository, widget.planSeedId ?? 0);
    if (widget.planSeedId != null) {
      _cubit.getDetailPlanSeed();
    }
    _cubit.getTarget();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          if (_cubit.editable && _hasChanges) {
            Utils.showCommonBottomSheet(
                context: context,
                title: R.string.note.tr(),
                formattedDetails: R.string.you_have_just_update.tr(),
                buttons: [
                  ButtonWidget(
                      backgroundColor: R.color.primaryColor,
                      padding: EdgeInsets.symmetric(vertical: 16.h),
                      title: R.string.yes.tr(),
                      textStyle: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 14.sp, color: R.color.white),
                      onPressed: _decideController.text.trim().isEmpty
                          ? null
                          : () {
                              if (_cubit.plantSeedHistory?.id != null) {
                                _cubit.updatePlanSeed(
                                    controller1: _decideController.text.trim(),
                                    controller2: _fertileController.text.trim(),
                                    controller3: _partnerController.text.trim(),
                                    controller4: _planController.text.trim(),
                                    controller5:
                                        _directActionController.text.trim(),
                                    controller6:
                                        _indirectActionController.text.trim(),
                                    controller7: _timeController.text.trim(),
                                    controller8:
                                        _myGoodJobController.text.trim(),
                                    controller9: _goodDeedsOfOthersController
                                        .text
                                        .trim(),
                                    controller10:
                                        _gratefulController.text.trim(),
                                    currentStep: _currentIndex + 1);
                              } else {
                                _cubit.getSeed(
                                    controller1: _decideController.text.trim(),
                                    controller2: _fertileController.text.trim(),
                                    controller3: _partnerController.text.trim(),
                                    controller4: _planController.text.trim(),
                                    controller5:
                                        _directActionController.text.trim(),
                                    controller6:
                                        _indirectActionController.text.trim(),
                                    controller7: _timeController.text.trim(),
                                    controller8:
                                        _myGoodJobController.text.trim(),
                                    controller9: _goodDeedsOfOthersController
                                        .text
                                        .trim(),
                                    controller10:
                                        _gratefulController.text.trim(),
                                    currentStep: _currentIndex + 1);
                              }
                              //NavigationUtils.pop(context);
                              NavigationUtils.pop(context,result: Const.ACTION_REFRESH);
                            }),
                  ButtonWidget(
                      backgroundColor: R.color.white,
                      borderColor: R.color.primaryColor,
                      padding: EdgeInsets.symmetric(vertical: 16.h),
                      title: R.string.no.tr(),
                      textStyle: Theme.of(context).textTheme.bold700.copyWith(
                          fontSize: 14.sp, color: R.color.primaryColor),
                      onPressed: () {
                        NavigationUtils.pop(context);
                        NavigationUtils.pop(context);
                      }),
                ]);
          } else {
            NavigationUtils.pop(context);
          }
          return true;
        },
        child: BlocProvider(
          create: (BuildContext context) => _cubit,
          child: BlocConsumer<NewSowingSeedsCubit, NewSowingSeedsState>(
            listener: (BuildContext context, state) {
              if (state is NewSowingSeedsSuccess) {
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                Utils.showToast(context, R.string.success_created_seed.tr());
                Utils.hideKeyboard(context);
              }
              if (state is DetailSuccess) {
                _decideController.text =
                    _cubit.plantSeedHistory?.step1?['STEP_1_1'] ?? "";
                _fertileController.text =
                    _cubit.plantSeedHistory?.step2?['STEP_2_1'] ?? "";
                _partnerController.text =
                    _cubit.plantSeedHistory?.step2?['STEP_2_2'] ?? "";
                _planController.text =
                    _cubit.plantSeedHistory?.step2?['STEP_2_3'] ?? "";
                _directActionController.text =
                    _cubit.plantSeedHistory?.step3?['STEP_3_1'] ?? "";
                _indirectActionController.text =
                    _cubit.plantSeedHistory?.step3?['STEP_3_2'] ?? "";
                _timeController.text =
                    _cubit.plantSeedHistory?.step3?['STEP_3_3'] ?? "";
                _myGoodJobController.text =
                    _cubit.plantSeedHistory?.step4?['STEP_4_1'] ?? "";
                _goodDeedsOfOthersController.text =
                    _cubit.plantSeedHistory?.step4?['STEP_4_2'] ?? "";
                _gratefulController.text =
                    _cubit.plantSeedHistory?.step4?['STEP_4_3'] ?? "";
              }

              if (state is UpdatePlanSeedSuccess) {
                Utils.showToast(
                    context, R.string.update_seeding_successfully.tr());
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                Utils.hideKeyboard(context);
              }
              if (state is DeletePlanSeedSuccess) {
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                Utils.showToast(context, R.string.remove_the_seed_success.tr());
                Utils.hideKeyboard(context);
              }
              if (state is NewSowingSeedsFailure) {
                Utils.showErrorSnackBar(
                    context, R.string.you_cannot_update.tr());
              }
              if (state is DeletePlanSeedFailure) {
                Utils.showErrorSnackBar(context,
                    R.string.you_cannot_delete_the_particle_of_past_date.tr());
              }
              if (state is CopyPlanSeedSuccess) {
                Utils.showToast(context, R.string.copy_succes.tr());
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                NavigationUtils.navigatePage(context,
                    NewSowingSeedsPage(planSeedId: state.newCopiedSeedId));
              }
              if (state is DownLoadSeedsSuccess) {
                Utils.showToast(context, R.string.save_file_image_success.tr());
              }
            },
            builder: (BuildContext context, state) {
              return state is !NewSowingSeedsLoading? Stack(
                children: [
                  Scaffold(
                    appBar: buildAppBar(),
                    body: GestureDetector(
                      onTap: () {
                        Utils.hideKeyboard(context);
                      },
                      child: CustomScrollView(
                        slivers: [
                          if (widget.planSeedId != null &&
                              _cubit.plantSeedHistory != null)
                            SliverToBoxAdapter(
                              child: Container(
                                padding: EdgeInsets.only(right: 20.w),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  DateFormat("HH:mm dd-MM-yyyy").format(
                                      _cubit.plantSeedHistory!.createdDate!),
                                ),
                              ),
                            ),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.only(left: 20.h),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    R.string.choose_a_target_type
                                        .tr(args: [": "]),
                                    style:
                                        Theme.of(context).textTheme.medium500,
                                  ),
                                  SizedBox(width: 5.h),
                                  Flexible(
                                      child: GestureDetector(
                                    onTap: () {
                                      showDialog(
                                          barrierColor:
                                              R.color.grey.withOpacity(0.5),
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Dialog(
                                              backgroundColor: R.color.white,
                                              insetPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 15.w,
                                                      vertical: 10.h),
                                              child: Container(
                                                height: 320.h,
                                                child: Scrollbar(
                                                  child: SingleChildScrollView(
                                                    physics: BouncingScrollPhysics(),
                                                    child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: List.generate(
                                                          _cubit.listTarget
                                                              .length,
                                                          (index) =>
                                                              GestureDetector(
                                                            onTap: () {
                                                              setState(() {
                                                                NavigationUtils
                                                                    .pop(
                                                                        context);
                                                                _cubit.currentTarget =
                                                                    _cubit.listTarget[
                                                                        index];
                                                              });
                                                            },
                                                            child: Padding(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          10.h),
                                                              child: Text(
                                                                _cubit
                                                                        .listTarget[
                                                                            index]
                                                                        .name ??
                                                                    "",
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .regular400
                                                                    .copyWith(
                                                                        height:
                                                                            24 /
                                                                                12,
                                                                        color: R
                                                                            .color
                                                                            .blue),
                                                              ),
                                                            ),
                                                          ),
                                                        )),
                                                  ),
                                                ),
                                              ),
                                            );
                                          });
                                    },
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          _cubit.currentTarget?.name ?? "",
                                          style: Theme.of(context)
                                              .textTheme
                                              .regular400
                                              .copyWith(
                                                  height: 20 / 12,
                                                  color: R.color.blue),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Icon(
                                          Icons.keyboard_arrow_down,
                                          size: 20.h,
                                        ),
                                      ],
                                    ),
                                  ))
                                ],
                              ),
                            ),
                          ),
                          SliverToBoxAdapter(child: SizedBox(height: 5.h)),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.h),
                              child: DCIStepsWidget(
                                currentIndex: _currentIndex,
                                saveButton: buildButton(),
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      buildTitle(context,
                                          title: R.string.decide_what_you_want
                                              .tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        maxLength: 500,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R.string.write.tr(),
                                        controller: _decideController,
                                        errorText: errorTextStep1
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        // _cubit.validateText(
                                        //     _decideController.text.trim()),
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      buildTitle(context,
                                          title: R.string.find.tr()),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description:
                                              R.string.choose_fertile.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText:
                                            R.string.list_fertile_lands.tr(),
                                        controller: _fertileController,
                                        errorText: errorTextStep2_2
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description:
                                              R.string.seeding_partner.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R.string.enter_the_name.tr(),
                                        controller: _partnerController,
                                        onSubmitted: (String) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description:
                                              R.string.make_a_plan.tr()),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R.string.list_your_plans.tr(),
                                        errorText: errorTextStep2_1
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        controller: _planController,
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                      SizedBox(height: 4.h),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      buildTitle(context,
                                          title: R
                                              .string.take_action_to_help_them
                                              .tr()),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description: R.string.direct.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        errorText: errorTextStep3_1
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        // _cubit.validateTextV2(
                                        //     _directActionController.text,
                                        //     _indirectActionController.text,
                                        //     _timeController.text),
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R
                                            .string.list_your_direct_actions
                                            .tr(),
                                        controller: _directActionController,
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description: R.string.indirect.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        errorText: errorTextStep3_1
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R
                                            .string.list_your_indirect_actions
                                            .tr(),
                                        controller: _indirectActionController,
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description:
                                              R.string.teach_sowing_seeds.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        errorText: errorTextStep3_1
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R
                                            .string.describe_specific_times
                                            .tr(),
                                        controller: _timeController,
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      buildTitle(context,
                                          title: R.string.coffee_meditation_to
                                              .tr()),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description: R
                                              .string.before_going_to_bed
                                              .tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R.string
                                            .list_the_good_deeds_of_your_day
                                            .tr(),
                                        controller: _myGoodJobController,
                                        errorText: errorTextStep4
                                            ? R.string.please_enter_data.tr()
                                            : null,
                                        onSubmitted: (text) {},
                                        onChanged: _cubit.validateInputChange,
                                      ),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description:
                                              R.string.acknowledge.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R
                                            .string.list_good_deeds_others_do
                                            .tr(),
                                        controller:
                                            _goodDeedsOfOthersController,
                                        onChanged: _cubit.validateInputChange,
                                        onSubmitted: (String) {},
                                      ),
                                      SizedBox(height: 4.h),
                                      buildDescription(context,
                                          description: R.string.grateful.tr()),
                                      SizedBox(height: 4.h),
                                      BoxedTextField(
                                        enable: _decideController.text
                                                .trim()
                                                .isEmpty
                                            ? false
                                            : true,
                                        width:
                                            MediaQuery.of(context).size.width -
                                                75.w,
                                        hintText: R.string.list_object.tr(),
                                        controller: _gratefulController,
                                        onChanged: _cubit.validateInputChange,
                                        onSubmitted: (String) {},
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: setLoading == true,
                    child: Container(
                      color: R.color.grey.withOpacity(0.8),
                      height: double.infinity,
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(
                            color: R.color.orange,
                          ),
                          SizedBox(height: 10.h),
                          Text(
                            R.string.please_await_a_second.tr(),
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                color: R.color.white, fontSize: 16.sp),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ):Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }

  bool get errorTextStep1 =>
      _decideController.text.isEmpty &&
      (_fertileController.text.isEmpty || _planController.text.isEmpty);

  bool get errorTextStep2_1 =>
      (_fertileController.text.isNotEmpty && _planController.text.isEmpty) ||
      ((_myGoodJobController.text.isNotEmpty ||
              _goodDeedsOfOthersController.text.isNotEmpty ||
              _gratefulController.text.isNotEmpty) &&
          _planController.text.isEmpty) ||
      ((_directActionController.text.isNotEmpty ||
              _indirectActionController.text.isNotEmpty ||
              _timeController.text.isNotEmpty) &&
          _planController.text.isEmpty);

  bool get errorTextStep2_2 =>
      (_planController.text.isNotEmpty && _fertileController.text.isEmpty) ||
      ((_myGoodJobController.text.isNotEmpty ||
              _goodDeedsOfOthersController.text.isNotEmpty ||
              _gratefulController.text.isNotEmpty) &&
          _fertileController.text.isEmpty) ||
      ((_directActionController.text.isNotEmpty ||
              _indirectActionController.text.isNotEmpty ||
              _timeController.text.isNotEmpty) &&
          _fertileController.text.isEmpty);

  bool get errorTextStep3_1 =>
      (_directActionController.text.isEmpty &&
          _indirectActionController.text.isEmpty &&
          _timeController.text.isEmpty) &&
      (_myGoodJobController.text.isNotEmpty ||
          _goodDeedsOfOthersController.text.isNotEmpty ||
          _gratefulController.text.isNotEmpty);

  bool get errorTextStep4 =>
      (_goodDeedsOfOthersController.text.isNotEmpty &&
          _myGoodJobController.text.isEmpty) ||
      (_gratefulController.text.isNotEmpty &&
          _myGoodJobController.text.isEmpty);

  Widget buildButton() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.h),
      child: Visibility(
        visible: _cubit.editable,
        child: ButtonWidget(
          title: R.string.save.tr(),
          onPressed: errorTextStep1 ||
                  (errorTextStep2_1 && _planController.text.isEmpty) ||
                  (errorTextStep2_2 && _fertileController.text.isEmpty) ||
                  (errorTextStep3_1) ||
                  errorTextStep4
              ? null
              : () {
                  if (_cubit.plantSeedHistory?.id != null) {
                    _cubit.updatePlanSeed(
                        controller1: _decideController.text.trim(),
                        controller2: _fertileController.text.trim(),
                        controller3: _partnerController.text.trim(),
                        controller4: _planController.text.trim(),
                        controller5: _directActionController.text.trim(),
                        controller6: _indirectActionController.text.trim(),
                        controller7: _timeController.text.trim(),
                        controller8: _myGoodJobController.text.trim(),
                        controller9: _goodDeedsOfOthersController.text.trim(),
                        controller10: _gratefulController.text.trim(),
                        currentStep: _currentIndex + 1);
                  } else {
                    _cubit.getSeed(
                        controller1: _decideController.text.trim(),
                        controller2: _fertileController.text.trim(),
                        controller3: _partnerController.text.trim(),
                        controller4: _planController.text.trim(),
                        controller5: _directActionController.text.trim(),
                        controller6: _indirectActionController.text.trim(),
                        controller7: _timeController.text.trim(),
                        controller8: _myGoodJobController.text.trim(),
                        controller9: _goodDeedsOfOthersController.text.trim(),
                        controller10: _gratefulController.text.trim(),
                        currentStep: _currentIndex + 1);
                  }
                },
          backgroundColor: R.color.secondaryButtonColor,
          uppercaseTitle: false,
          height: 40.h,
          textSize: 16.sp,
          // textStyle: Theme.of(context).textTheme.medium500.copyWith(
          //     fontSize: 16.sp, height: 24.h, color: R.color.white),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: R.color.primaryColor,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: 24.h,
        ),
        onPressed: () {
          if (_cubit.editable && _hasChanges) {
            buildShowCommonBottomSheet();
          } else {
            NavigationUtils.pop(context);
          }
        },
      ),
      actions: [
        if (widget.planSeedId != null)
          IconButton(
              onPressed: () {
                showBarModalBottomSheet(
                    expand: false,
                    context: context,
                    builder: (_) {
                      return buildShowMoreBottomSheet(context);
                    });
              },
              icon: Icon(
                Icons.more_vert_rounded,
                size: 24.h,
              ))
      ],
      title: Text(
        R.string.seeding_diary.tr().toUpperCase(),
        style: TextStyle(
            fontWeight: FontWeight.w700, fontSize: 16.sp, height: 24.h / 16.sp),
      ),
      centerTitle: true,
      bottom: PreferredSize(
        preferredSize: Size(112.h, 25.h),
        child: Align(
          alignment: Alignment.center,
          child: SizedBox(
            width: (17.h * 7),
            height: 25.h,
            child: ListView.separated(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.only(bottom: 8.h),
                itemBuilder: (context, index) {
                  if (index <= _currentIndex) {
                    return Container(
                        width: 17.h,
                        height: 17.h,
                        alignment: Alignment.center,
                        child: Text(
                          '${index + 1}',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.medium500.copyWith(
                              color: R.color.primaryColor,
                              fontSize: 10,
                              height: 12.44 / 10),
                        ),
                        decoration: BoxDecoration(
                          color: R.color.white,
                          borderRadius: BorderRadius.circular(100),
                          border: Border.all(width: 1.w, color: R.color.white),
                        ));
                  }
                  return Container(
                    width: 17.h,
                    height: 17.h,
                    alignment: Alignment.center,
                    child: Text(
                      '${index + 1}',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.regular400.copyWith(
                          color: R.color.white,
                          fontSize: 10,
                          height: 12.44 / 10),
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xFFC7D4FF).withOpacity(0.47),
                        border: Border.all(width: 1.w, color: R.color.white),
                        borderRadius: BorderRadius.circular(100)),
                  );
                },
                separatorBuilder: (context, index) {
                  return Row(
                    children: List.generate(17, (index) {
                      if (index.isEven)
                        return SizedBox(
                          width: 1.h,
                        );
                      return Container(
                        color: R.color.white,
                        width: 1.h,
                        height: 1.h,
                      );
                    }),
                  );
                },
                itemCount: 4),
          ),
        ),
      ),
    );
  }

  void buildShowCommonBottomSheet() {
    return Utils.showCommonBottomSheet(
        context: context,
        title: R.string.note.tr(),
        formattedDetails: R.string.you_have_just_update.tr(),
        buttons: [
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 16.h),
              title: R.string.yes.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 14.sp, color: R.color.white),
              onPressed: _decideController.text.trim().isEmpty
                  ? null
                  : () {
                      if (_cubit.plantSeedHistory?.id != null) {
                        _cubit.updatePlanSeed(
                            controller1: _decideController.text.trim(),
                            controller2: _fertileController.text.trim(),
                            controller3: _partnerController.text.trim(),
                            controller4: _planController.text.trim(),
                            controller5: _directActionController.text.trim(),
                            controller6: _indirectActionController.text.trim(),
                            controller7: _timeController.text.trim(),
                            controller8: _myGoodJobController.text.trim(),
                            controller9:
                                _goodDeedsOfOthersController.text.trim(),
                            controller10: _gratefulController.text.trim(),
                            currentStep: _currentIndex + 1);
                      } else {
                        _cubit.getSeed(
                            controller1: _decideController.text.trim(),
                            controller2: _fertileController.text.trim(),
                            controller3: _partnerController.text.trim(),
                            controller4: _planController.text.trim(),
                            controller5: _directActionController.text.trim(),
                            controller6: _indirectActionController.text.trim(),
                            controller7: _timeController.text.trim(),
                            controller8: _myGoodJobController.text,
                            controller9:
                                _goodDeedsOfOthersController.text.trim(),
                            controller10: _gratefulController.text.trim(),
                            currentStep: _currentIndex + 1);
                      }
                      //NavigationUtils.pop(context);
                      NavigationUtils.pop(context);
                    }),
          ButtonWidget(
              backgroundColor: R.color.white,
              borderColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 16.h),
              title: R.string.no.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 14.sp, color: R.color.primaryColor),
              onPressed: () {
                NavigationUtils.pop(context);
                NavigationUtils.pop(context);
              }),
        ]);
  }

  Widget buildTitle(BuildContext context, {String? title}) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 72.w,
      child: Text(
        title ?? "",
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 14.sp, height: 21 / 14),
        maxLines: 2,
      ),
    );
  }

  Widget buildDescription(BuildContext context, {String? description}) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 72.w,
      child: Text(
        description ?? "",
        style: Theme.of(context)
            .textTheme
            .regular400
            .copyWith(fontSize: 12.sp, height: 18 / 12),
      ),
    );
  }

  int get _currentIndex {
    if (_cubit.currentTarget == null)
      return -1;
    else {
      if (_myGoodJobController.text.isNotEmpty) return 3;
      if (_directActionController.text.trim().isNotEmpty ||
          _indirectActionController.text.trim().isNotEmpty ||
          _timeController.text.trim().isNotEmpty) return 2;
      if (_fertileController.text.trim().isNotEmpty ||
          _planController.text.trim().isNotEmpty) return 1;
    }
    return 0;
    // if (_decideController.text.trim().isNotEmpty) return -1;
    // if (_fertileController.text.trim().isNotEmpty ||
    //     _planController.text.trim().isNotEmpty) return 0;
    // if (_directActionController.text.trim().trim().isNotEmpty &&
    //     _indirectActionController.text.trim().isNotEmpty &&
    //     _timeController.text.trim().isNotEmpty) return 1;
    // if (_myGoodJobController.text.trim().isNotEmpty) return 2;
    // return 3;
  }

  Widget buildShowMoreBottomSheet(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 30.h, vertical: 35.h),
      child: Column(
        children: [
          RowButtonWidget(
            callback: () {
              buildConfirmDelete(context);
            },
            image: R.drawable.ic_delete_post,
            title: R.string.delete.tr(),
          ),
          SizedBox(height: 12.h),
          Divider(height: 1, color: R.color.gray),
          SizedBox(height: 12.h),
          RowButtonWidget(
            callback: () {
              _cubit.copyPlanSeed(
                  controller1: _decideController.text,
                  controller2: _fertileController.text,
                  controller3: _partnerController.text,
                  controller4: _planController.text,
                  controller5: _directActionController.text,
                  controller6: _indirectActionController.text,
                  controller7: _timeController.text,
                  controller8: _myGoodJobController.text,
                  controller9: _goodDeedsOfOthersController.text,
                  controller10: _gratefulController.text,
                  currentStep: _currentIndex + 1);
              NavigationUtils.pop(context);
            },
            image: R.drawable.ic_copy_seed,
            title: R.string.copy_into_a_new_particle.tr(),
          ),
          SizedBox(height: 12.h),
          Divider(height: 1, color: R.color.gray),
          SizedBox(height: 12.h),
          RowButtonWidget(
            callback: () {
              //to do
              _cubit.getImageSeed();
              setLoading = true;
              NavigationUtils.pop(context);
              Future.delayed(Duration(seconds: 5), () {
                setState(() {
                  setLoading = false;
                });

                if (_cubit.imageSee?.url != null) {
                  NavigationUtils.navigatePage(
                      context,
                      PostPage(
                        type: PostType.MEDIA,
                        imageData: _cubit.imageSee,
                      ));
                }
              });
            },
            image: R.drawable.ic_share_seed,
            title: R.string.share_into_post_on_the_community.tr(),
          ),
          SizedBox(height: 12.h),
          Divider(height: 1, color: R.color.gray),
          SizedBox(height: 12.h),
          RowButtonWidget(
            callback: () {
              _cubit.getImageSeed();
              Future.delayed(Duration(seconds: 2), () {
                _cubit.downLoadSeed();
              });
              NavigationUtils.pop(context);
            },
            image: R.drawable.ic_save_file,
            title: R.string.save_as_file.tr(),
          ),
        ],
      ),
    );
  }

  void buildConfirmDelete(BuildContext context) {
    return Utils.showCommonBottomSheet(
        context: context,
        title: R.string.confirm.tr(),
        formattedDetails: R.string.do_you_want_to_delete_seed.tr(),
        buttons: [
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 16.h),
              title: R.string.yes.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 14.sp, color: R.color.white),
              onPressed: () {
                _cubit.deletePlanSeed(widget.planSeedId ?? 0);
                NavigationUtils.pop(context);
                NavigationUtils.pop(context);
              }),
          ButtonWidget(
              backgroundColor: R.color.white,
              borderColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 16.h),
              title: R.string.no.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 14.sp, color: R.color.primaryColor),
              onPressed: () {
                NavigationUtils.pop(context);
                NavigationUtils.pop(context);
              }),
        ]);
  }

  bool get _hasChanges =>
      _decideController.text != _cubit.plantSeedHistory?.step1?['STEP_1_1'] ||
      _fertileController.text != _cubit.plantSeedHistory?.step2?['STEP_2_1'] ||
      _partnerController.text != _cubit.plantSeedHistory?.step2?['STEP_2_2'] ||
      _planController.text != _cubit.plantSeedHistory?.step2?['STEP_2_3'] ||
      _directActionController.text !=
          _cubit.plantSeedHistory?.step3?['STEP_3_1'] ||
      _indirectActionController.text !=
          _cubit.plantSeedHistory?.step3?['STEP_3_2'] ||
      _timeController.text != _cubit.plantSeedHistory?.step3?['STEP_3_3'] ||
      _myGoodJobController.text !=
          _cubit.plantSeedHistory?.step4?['STEP_4_1'] ||
      _goodDeedsOfOthersController.text !=
          _cubit.plantSeedHistory?.step4?['STEP_4_2'] ||
      _gratefulController.text != _cubit.plantSeedHistory?.step4?['STEP_4_3'];
}
