import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/page/clean_seeds/search_clean_seeds/search_clean_seeds_setting_cubit.dart';
import 'package:imi/src/page/clean_seeds/search_clean_seeds/search_clean_seeds_setting_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SearchCleanSeedsSettingPage extends StatefulWidget {
  const SearchCleanSeedsSettingPage({
    Key? key,
  }) : super(key: key);

  @override
  State<SearchCleanSeedsSettingPage> createState() =>
      _SearchCleanSeedsSettingPageState();
}

class _SearchCleanSeedsSettingPageState
    extends State<SearchCleanSeedsSettingPage> {
  late final TextEditingController _textController;
  late final RefreshController _refreshController;
  late final SearchCleanSeedsSettingCubit _cubit;
  int? selectedIndex;

  late ScrollController scrollController;

  int _count = 0;

  @override
  void initState() {
    // scroll to row by row height and separator height
    scrollController = ScrollController();
    _textController = TextEditingController();
    _refreshController = RefreshController();
    _cubit = SearchCleanSeedsSettingCubit(AppGraphqlRepository());
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    _textController.dispose();
    _refreshController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<SearchCleanSeedsSettingCubit,
          SearchCleanSeedsSettingState>(
        listener: (context, state) {},
        bloc: _cubit,
        builder: (context, state) {
          return StackLoadingView(
            visibleLoading: state is SearchCleanSeedsSettingLoadingState,
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 24.w, vertical: 8.h),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextFieldWidget(
                            autoFocus: false,
                            controller: _textController,
                            hintText: R.string.search.tr(),
                            border: 30.h,
                            fillColor: R.color.grey100,
                            borderSize: BorderSide.none,
                            padding: EdgeInsets.symmetric(horizontal: 15.h),
                            suffixIcon: Icon(Icons.search,
                                size: 25.h, color: R.color.textGreyColor),
                            onChanged: (text) {
                              _count++;
                              Future.delayed(const Duration(milliseconds: 600))
                                  .whenComplete(() {
                                _count--;
                                if (_count == 0) {
                                  _cubit.fetchCleanSeedsSetting(text ?? "");
                                }
                              });
                            },
                            onSubmitted: (text) {
                              _cubit.fetchCleanSeedsSetting(text ?? "");
                            },
                          ),
                        ),
                        SizedBox(
                          width: 16.w,
                        ),
                        GestureDetector(
                            onTap: () {
                              NavigationUtils.pop(context);
                            },
                            child: Icon(
                              Icons.close,
                              size: 25.h,
                              color: R.color.primaryColor,
                            ))
                      ],
                    ),
                  ),
                  Expanded(
                    child: SmartRefresher(
                      controller: _refreshController,
                      enablePullUp: true,
                      enablePullDown: false,
                      onLoading: () {
                        _cubit.fetchCleanSeedsSetting(_textController.text);
                        _refreshController.loadComplete();
                      },
                      child: Utils.isEmpty(_cubit.settings)
                          ? Center(
                              child: SizedBox.shrink(),
                            )
                          : Scrollbar(
                              child: ListView.separated(
                                controller: scrollController,
                                padding: EdgeInsets.symmetric(horizontal: 24.w),
                                shrinkWrap: true,
                                itemCount: _cubit.settings!.length,
                                itemBuilder: (context, index) {
                                  final item = _cubit.settings![index];
                                  return GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                      NavigationUtils.pop(context,
                                          result: item);
                                    },
                                    child: Container(
                                      color: R.color.white,
                                      padding: EdgeInsets.symmetric(
                                          vertical: 12.h, horizontal: 8.w),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            flex: 12,
                                            child: Text(
                                              item?.action ?? "",
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .medium500
                                                  .copyWith(fontSize: 14.sp),
                                            ),
                                          ),
                                          if (selectedIndex == index)
                                            Flexible(
                                              flex: 2,
                                              child: Visibility(
                                                  visible:
                                                      selectedIndex == index,
                                                  child: Icon(
                                                    CupertinoIcons
                                                        .checkmark_alt,
                                                    size: 20.h,
                                                  )),
                                            )
                                        ],
                                      ),
                                    ),
                                  );
                                },
                                separatorBuilder: (context, index) => Container(
                                  height: 1,
                                  color: R.color.lightestGray,
                                ),
                              ),
                            ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
