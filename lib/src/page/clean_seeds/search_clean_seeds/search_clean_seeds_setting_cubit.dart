import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/response/clean_seed_history_response.dart';
import 'package:imi/src/data/network/response/clean_seeds_setting_response.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/clean_seeds/search_clean_seeds/search_clean_seeds_setting_state.dart';

class SearchCleanSeedsSettingCubit extends Cubit<SearchCleanSeedsSettingState> {
  final AppGraphqlRepository graphqlRepository;

  SearchCleanSeedsSettingCubit(this.graphqlRepository)
      : super(SearchCleanSeedsSettingLoadingState()) {
    fetchCleanSeedsSetting("");
  }

  String? nextToken;
  String? lastSearchKey;
  List<Step1?>? settings;

  Future fetchCleanSeedsSetting(String searchKey) async {
    if (nextToken == null && (settings ?? []).isEmpty)
      emit(SearchCleanSeedsSettingLoadingState());
    if (searchKey != lastSearchKey) {
      lastSearchKey = searchKey;
      settings = [];
      nextToken = null;
    }
    final res = await graphqlRepository.getCleanSeedsSetting(
        nextToken: nextToken, searchKey: searchKey);
    res.when(success: (data) {
      settings ??= [];
      nextToken = data.cleanSeedSettings?.nextToken;
      settings?.addAll(data.cleanSeedSettings?.data ?? []);
      emit(SearchCleanSeedsSettingSuccessState());
    }, failure: (e) {
      emit(SearchCleanSeedsSettingFailureState(
          NetworkExceptions.getErrorMessage(e)));
    });
  }
}
