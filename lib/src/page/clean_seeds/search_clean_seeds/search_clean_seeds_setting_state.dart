abstract class SearchCleanSeedsSettingState {}

class SearchCleanSeedsSettingLoadingState extends SearchCleanSeedsSettingState {
}

class SearchCleanSeedsSettingSuccessState extends SearchCleanSeedsSettingState {
}

class SearchCleanSeedsSettingFailureState extends SearchCleanSeedsSettingState {
  final error;

  SearchCleanSeedsSettingFailureState(this.error);
}
