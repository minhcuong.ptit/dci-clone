import 'package:imi/src/data/network/response/clean_seed_history_response.dart';

abstract class CleanSeedsState {}

class CleanSeedsInitialState extends CleanSeedsState {}

class CleanSeedsLoadingState extends CleanSeedsState {}

class CleanSeedsFetchDataSuccessState extends CleanSeedsState {
  final FetchCleanSeedHistoriesData data;

  CleanSeedsFetchDataSuccessState(this.data);
}

class CleanSeedsCreateSuccessState extends CleanSeedsState {}

class CleanSeedsUpdateSuccessState extends CleanSeedsState {}

class CleanSeedsCopySuccessState extends CleanSeedsState {
  final int newCopiedSeedId;

  CleanSeedsCopySuccessState(this.newCopiedSeedId);
}

class CleanSeedsDeleteSuccessState extends CleanSeedsState {}

class CleanSeedsInputChangeState extends CleanSeedsState {}

class CleanSeedsFailureState extends CleanSeedsState {
  final error;

  CleanSeedsFailureState(this.error);
}

class ShareCleanSeedsSuccessState extends CleanSeedsState {}

class CleanSeedsSuccess extends CleanSeedsState {}
