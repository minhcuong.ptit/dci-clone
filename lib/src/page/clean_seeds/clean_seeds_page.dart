import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/clean_seeds/clean_seeds_cubit.dart';
import 'package:imi/src/page/clean_seeds/clean_seeds_state.dart';
import 'package:imi/src/page/clean_seeds/search_clean_seeds/search_clean_seeds_setting_page.dart';
import 'package:imi/src/page/post/post_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/boxed_text_field.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/dci_steps_widget.dart';
import 'package:imi/src/widgets/row_button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:styled_text/styled_text.dart';

import '../../../res/R.dart';
import '../../utils/utils.dart';

class CleanSeedsPage extends StatefulWidget {
  final int? cleanSeedId;
  const CleanSeedsPage({Key? key, this.cleanSeedId}) : super(key: key);

  @override
  _CleanSeedsPageState2 createState() => _CleanSeedsPageState2();
}

class _CleanSeedsPageState2 extends State<CleanSeedsPage> {
  late final TextEditingController _regretController;
  late final TextEditingController _engagementController;
  late final TextEditingController _compensationController;
  late final ScrollController _scrollController;
  late final CleanSeedsCubit _cubit;
  bool setLoading = false;

  final _stepWidgetKey = GlobalKey<DCIStepsWidgetState>();

  @override
  void initState() {
    super.initState();
    _regretController = TextEditingController();
    _engagementController = TextEditingController();
    _compensationController = TextEditingController();
    _scrollController = ScrollController();
    _cubit = CleanSeedsCubit(
        repository: AppRepository(),
        graphqlRepository: AppGraphqlRepository(),
        cleanSeedId: widget.cleanSeedId);
  }

  @override
  void dispose() {
    _regretController.dispose();
    _engagementController.dispose();
    _compensationController.dispose();
    _scrollController.dispose();
    _cubit.close();
    super.dispose();
  }

  void _showConfirmDelete(BuildContext context) {
    showBarModalBottomSheet(
        context: context,
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(24),
            child: IntrinsicHeight(
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      R.string.confirm.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 16.sp, height: 24 / 16.sp),
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    StyledText(
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 11.sp, height: 20 / 11.sp),
                      text: R.string.do_you_want_to_delete_clean_seed.tr(),
                      tags: {
                        'bold': StyledTextTag(
                            style: Theme.of(context)
                                .textTheme
                                .bold700
                                .copyWith(fontSize: 11.sp, height: 20 / 11.sp))
                      },
                    ),
                    SizedBox(
                      height: 16.h,
                    ),
                    ButtonWidget(
                        backgroundColor: R.color.primaryColor,
                        padding: EdgeInsets.symmetric(vertical: 16.h),
                        title: R.string.yes.tr(),
                        textStyle: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 14.sp, color: R.color.white),
                        onPressed: () {
                          _cubit.remove(widget.cleanSeedId!);
                          NavigationUtils.pop(context,result: Const.ACTION_REFRESH);
                        }),
                    SizedBox(
                      height: 16.h,
                    ),
                    ButtonWidget(
                        backgroundColor: R.color.white,
                        borderColor: R.color.primaryColor,
                        padding: EdgeInsets.symmetric(vertical: 16.h),
                        title: R.string.no.tr(),
                        textStyle: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 14.sp, color: R.color.primaryColor),
                        onPressed: () {
                          NavigationUtils.pop(context);
                        }),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget _buildShowMoreBottomSheet(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 30.h, vertical: 35.h),
      child: Column(
        children: [
          if (widget.cleanSeedId != null) ...[
            RowButtonWidget(
              callback: () {
                NavigationUtils.pop(context);
                _showConfirmDelete(context);
              },
              image: R.drawable.ic_delete_post,
              title: R.string.delete.tr(),
            ),
            SizedBox(height: 12.h),
            Divider(height: 1, color: R.color.gray),
          ],
          SizedBox(height: 12.h),
          RowButtonWidget(
            callback: () {
              _cubit.copySeed(
                  _regretController.text,
                  _engagementController.text,
                  _compensationController.text,
                  _currentIndex + 1);
              NavigationUtils.pop(context);
            },
            image: R.drawable.ic_copy_seed,
            title: R.string.copy_into_a_new_particle.tr(),
          ),
          SizedBox(height: 12.h),
          Divider(height: 1, color: R.color.gray),
          SizedBox(height: 12.h),
          RowButtonWidget(
            callback: () {
              _cubit.getImageCleanSeed();
              setLoading = true;
              NavigationUtils.pop(context);
              Future.delayed(Duration(seconds: 5), () {
                setState(() {
                  setLoading = false;
                });
                if (_cubit.imageSeedData?.url != null) {
                  NavigationUtils.navigatePage(
                      context,
                      PostPage(
                        type: PostType.MEDIA,
                        imageData: _cubit.imageSeedData,
                      ));
                }
              });
            },
            image: R.drawable.ic_share_seed,
            title: R.string.share_into_post_on_the_community.tr(),
          ),
          SizedBox(height: 12.h),
          Divider(height: 1, color: R.color.gray),
          SizedBox(height: 12.h),
          RowButtonWidget(
            callback: () {
              _cubit.getImageCleanSeed();
              Future.delayed(Duration(seconds: 2), () {
                _cubit.downLoadSeed();
              });
              NavigationUtils.pop(context);
            },
            image: R.drawable.ic_save_file,
            title: R.string.save_as_file.tr(),
          ),
        ],
      ),
    );
  }

  void _showPopMenu() {
    Utils.hideKeyboard(context);
    Utils.showCommonBottomSheet(
        context: context,
        title: R.string.be_careful.tr(),
        formattedDetails: R.string.discard_clean_seed_changes.tr(),
        buttons: [
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 16.h),
              title: R.string.yes.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 14.sp, color: R.color.white),
              onPressed: () {
                NavigationUtils.pop(context);
                if (!_isRegretError && !_isEngagementError) {
                  if (widget.cleanSeedId == null) {
                    _cubit.createCleanSeed(
                        _regretController.text,
                        _engagementController.text,
                        _compensationController.text,
                        _currentIndex + 1);
                  } else {
                    _cubit.updateCleanSeed(
                        widget.cleanSeedId!,
                        _regretController.text,
                        _engagementController.text,
                        _compensationController.text,
                        _currentIndex + 1);
                  }
                } else {
                  if (_isRegretError) {
                    _scrollController.animateTo(
                        _stepWidgetKey.currentState?.getStepLocation(1) ?? 0,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);
                  }
                }
              }),
          ButtonWidget(
              backgroundColor: R.color.white,
              borderColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 16.h),
              title: R.string.no.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 14.sp, color: R.color.primaryColor),
              onPressed: () {
                NavigationUtils.popByTime(context, 2);
              }),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          if (_cubit.currentStep1 != null && _cubit.editable && _hasChanges) {
            _showPopMenu();
            return false;
          }
          return true;
        },
        child: BlocConsumer<CleanSeedsCubit, CleanSeedsState>(
          bloc: _cubit,
          listener: (context, state) {
            if (state is CleanSeedsCreateSuccessState) {
              Utils.showToast(context, R.string.create_successfully.tr());
              NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
            } else if (state is CleanSeedsFailureState) {
              Utils.showErrorSnackBar(context, state.error);
            } else if (state is CleanSeedsDeleteSuccessState) {
              Utils.showToast(context, R.string.delete_successfully.tr());
              NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
            } else if (state is CleanSeedsUpdateSuccessState) {
              Utils.showToast(context, R.string.update_successfully.tr());
              NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
            } else if (state is CleanSeedsCopySuccessState) {
              NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
              NavigationUtils.navigatePage(
                  context,
                  CleanSeedsPage(
                    cleanSeedId: state.newCopiedSeedId,
                  ));
            } else if (state is ShareCleanSeedsSuccessState) {
              Utils.showToast(context, R.string.save_file_image_success.tr());
            }
          },
          builder: (context, state) {
            if (state is CleanSeedsFetchDataSuccessState) {
              _regretController.text = state.data.step2 ?? "";
              _engagementController.text = state.data.step3 ?? "";
              _compensationController.text = state.data.step4 ?? "";
            }
            return SafeArea(
                child: Stack(
              children: [
                Scaffold(
                  appBar: AppBar(
                    backgroundColor: R.color.primaryColor,
                    leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 24.h,
                      ),
                      onPressed: () {
                        if (_cubit.currentStep1 != null &&
                            _cubit.editable &&
                            _hasChanges) {
                          _showPopMenu();
                        } else {
                          NavigationUtils.pop(context);
                        }
                      },
                    ),
                    actions: [
                      if (widget.cleanSeedId != null)
                        IconButton(
                            onPressed: () {
                              showBarModalBottomSheet(
                                  context: context,
                                  builder: (_) {
                                    return _buildShowMoreBottomSheet(context);
                                  });
                            },
                            icon: Icon(
                              Icons.more_vert_rounded,
                              size: 24.h,
                            ))
                    ],
                    title: Text(
                      R.string.seed_removal_diary.tr().toUpperCase(),
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 16.sp,
                          height: 24.h / 16.sp),
                    ),
                    centerTitle: true,
                    bottom: PreferredSize(
                      preferredSize: Size(112.h, 25.h),
                      child: Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          width: (18.h * 7),
                          height: 25.h,
                          child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              padding: EdgeInsets.only(bottom: 8.h),
                              itemBuilder: (context, index) {
                                if (index <= _currentIndex) {
                                  return Container(
                                      width: 18.h,
                                      height: 18.h,
                                      alignment: Alignment.center,
                                      child: Text(
                                        '${index + 1}',
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .medium500
                                            .copyWith(
                                                color: R.color.primaryColor,
                                                fontSize: 10,
                                                height: 12.44 / 10),
                                      ),
                                      decoration: BoxDecoration(
                                        color: R.color.white,
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        border: Border.all(
                                            width: 1.w, color: R.color.white),
                                      ));
                                }
                                return Container(
                                  width: 18.h,
                                  height: 18.h,
                                  alignment: Alignment.center,
                                  child: Text(
                                    '${index + 1}',
                                    textAlign: TextAlign.center,
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            color: R.color.white,
                                            fontSize: 10,
                                            height: 12.44 / 10),
                                  ),
                                  decoration: BoxDecoration(
                                      color:
                                          Color(0xFFC7D4FF).withOpacity(0.47),
                                      border: Border.all(
                                          width: 1.w, color: R.color.white),
                                      borderRadius: BorderRadius.circular(100)),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return Row(
                                  children: List.generate(18, (index) {
                                    if (index.isEven)
                                      return SizedBox(
                                        width: 1.h,
                                      );
                                    return Container(
                                      color: R.color.white,
                                      width: 1.h,
                                      height: 1.h,
                                    );
                                  }),
                                );
                              },
                              itemCount: 4),
                        ),
                      ),
                    ),
                  ),
                  body: Form(
                    child: StackLoadingView(
                      visibleLoading: state is CleanSeedsLoadingState,
                      child: CustomScrollView(
                        controller: _scrollController,
                        slivers: [
                          SliverToBoxAdapter(
                            child: SizedBox(height: 16.h),
                          ),
                          // TODO: Show in view or edit mode
                          if (widget.cleanSeedId != null &&
                              _cubit.initialData != null)
                            SliverToBoxAdapter(
                              child: Align(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 20.w),
                                  child: Text(DateFormat("HH:mm dd-MM-yyyy")
                                      .format(
                                          _cubit.initialData!.createdDate!)),
                                ),
                                alignment: Alignment.centerRight,
                              ),
                            ),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: 20.w,
                                  right: 20.w,
                                  bottom: _cubit.editable ? 0 : 16.h),
                              child: DCIStepsWidget(
                                key: _stepWidgetKey,
                                currentIndex: _currentIndex,
                                saveButton: Visibility(
                                  visible: _cubit.editable,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(vertical: 16.h),
                                    child: ButtonWidget(
                                      title: R.string.save.tr(),
                                      onPressed: () {
                                        if (_cubit.currentStep1 != null &&
                                            !_isEngagementError &&
                                            _regretController.text.isNotEmpty &&
                                            !_isRegretError) {
                                          if (widget.cleanSeedId == null)
                                            _cubit.createCleanSeed(
                                                _regretController.text,
                                                _engagementController.text,
                                                _compensationController.text,
                                                _currentIndex + 1);
                                          else
                                            _cubit.updateCleanSeed(
                                                widget.cleanSeedId!,
                                                _regretController.text,
                                                _engagementController.text,
                                                _compensationController.text,
                                                _currentIndex + 1);
                                        }
                                      },
                                      backgroundColor: R
                                          .color.secondaryButtonColor
                                          .withOpacity(
                                              (_cubit.currentStep1 != null &&
                                                      !_isEngagementError &&
                                                      _regretController
                                                          .text.isNotEmpty &&
                                                      !_isRegretError)
                                                  ? 1
                                                  : 0.5),
                                      uppercaseTitle: false,
                                      height: 40.h,
                                      textSize: 16.sp,
                                      // textStyle: Theme.of(context).textTheme.medium500.copyWith(
                                      //     fontSize: 16.sp, height: 24.h, color: R.color.white),
                                    ),
                                  ),
                                ),
                                children: [
                                  LayoutBuilder(
                                      builder: (context, _) =>
                                          _recall(context)),
                                  _regret(context),
                                  _engagement(context),
                                  _compensation(context),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: setLoading == true,
                  child: Container(
                    color: R.color.grey.withOpacity(0.8),
                    height: double.infinity,
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(
                          color: R.color.orange,
                        ),
                        SizedBox(height: 10.h),
                        Text(
                          R.string.please_await_a_second.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .bold700
                              .copyWith(color: R.color.white, fontSize: 16.sp),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ));
          },
        ),
      ),
    );
  }

  Column _compensation(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          R.string.seed_compensation.tr(),
          style: Theme.of(context)
              .textTheme
              .medium500
              .copyWith(fontSize: 14.sp, height: 21.h / 14.sp),
        ),
        SizedBox(
          height: 8.h,
        ),
        BoxedTextField(
          width: context.width - 72.w,
          hintText: R.string.seed_compensation_description.tr(),
          textLength: _cubit.textLength[2],
          controller: _compensationController,
          onChanged: (value) {
            _cubit.onInputChanged(value, 2);
          },
          onSubmitted: (value) {
            _cubit.onInputChanged(value, 2);
          },
          enable: _cubit.currentStep1 != null && _cubit.editable,
        )
      ],
    );
  }

  bool get _isEngagementError => (_engagementController.text.isEmpty &&
      _compensationController.text.isNotEmpty);

  Column _engagement(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          R.string.create_engagement.tr(),
          style: Theme.of(context)
              .textTheme
              .medium500
              .copyWith(fontSize: 14.sp, height: 21.h / 14.sp),
        ),
        SizedBox(
          height: 8.h,
        ),
        BoxedTextField(
            width: context.width - 72.w,
            hintText: R.string.i_engage.tr(),
            textLength: _cubit.textLength[1],
            controller: _engagementController,
            onChanged: (value) {
              _cubit.onInputChanged(value, 1);
            },
            onSubmitted: (value) {
              _cubit.onInputChanged(value, 1);
            },
            enable: _cubit.currentStep1 != null && _cubit.editable,
            errorText: _isEngagementError ? R.string.please_fill_in.tr() : null)
      ],
    );
  }

  bool get _isRegretError =>
      _regretController.text.isEmpty &&
      (_engagementController.text.isNotEmpty ||
          _compensationController.text.isNotEmpty);

  Column _regret(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          R.string.smart_regret.tr(),
          style: Theme.of(context)
              .textTheme
              .medium500
              .copyWith(fontSize: 14.sp, height: 21.h / 14.sp),
        ),
        SizedBox(
          height: 8.h,
        ),
        BoxedTextField(
          width: context.width - 72.w,
          hintText: R.string.i_realise.tr(),
          maxLength: 500,
          textLength: _cubit.textLength[0],
          controller: _regretController,
          onChanged: (value) {
            _cubit.onInputChanged(value, 0);
          // setState((){
          //     _cubit.regetTextLength=value.length;
          //   });
          },
          onSubmitted: (value) {
            _cubit.onInputChanged(value, 0);
          },
          enable: _cubit.currentStep1 != null && _cubit.editable,
          errorText: _isRegretError ? R.string.please_fill_in.tr() : null,
        ),
      ],
    );
  }

  Column _recall(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          R.string.recall_pen.tr(),
          style: Theme.of(context)
              .textTheme
              .medium500
              .copyWith(fontSize: 14.sp, height: 21.h / 14.sp),
        ),
        SizedBox(
          height: 8.h,
        ),
        Text(
          R.string.choose_a_bad_action.tr(),
          style: Theme.of(context)
              .textTheme
              .medium500
              .copyWith(fontSize: 12.sp, height: 20.h / 12.sp),
        ),
        SizedBox(
          height: 4.h,
        ),
        GestureDetector(
          onTap: () {
            if (_cubit.editable)
              NavigationUtils.navigatePage(
                      context, SearchCleanSeedsSettingPage())
                  .then((value) {
                if (value != null)
                  setState(() {
                    _cubit.currentStep1 = value;
                  });
              });
          },
          child: Row(
            children: [
              Container(
                constraints: BoxConstraints(
                    maxWidth: context.width - 92.w),
                child: Text(
                  _cubit.currentStep1?.action ??
                      R.string.choose_a_bad_action.tr(),
                  style: Theme.of(context).textTheme.medium500.copyWith(
                      fontSize: 12.sp,
                      height: 20.h / 12.sp,
                      color: R.color.primaryColor),
                ),
              ),
              SizedBox(
                height: 4.w,
              ),
              Image.asset(
                R.drawable.ic_arrow_down,
                width: 16.h,
                height: 16.h,
                color: R.color.primaryColor,
              )
            ],
          ),
        ),
        SizedBox(
          height: 8.h,
        ),
        Text(
          R.string.need_cleaned_seed.tr(),
          style: Theme.of(context).textTheme.medium500.copyWith(
                fontSize: 12.sp,
                height: 20.h / 12.sp,
              ),
        ),
        SizedBox(
          height: 4.h,
        ),
        Text(
          _cubit.currentStep1?.seed?.name ??
              R.string.need_cleaned_seed_description.tr(),
          style: Theme.of(context).textTheme.medium500.copyWith(
              fontSize: 12.sp,
              height: 18.h / 12.sp,
              color: R.color.primaryColor),
        ),
        SizedBox(
          height: 16.h,
        ),
        Container(
          decoration: BoxDecoration(
              color: R.color.lightNavy,
              borderRadius: BorderRadius.circular(4.h)),
          padding: EdgeInsets.all(12.h),
          width: context.width - 72.w,
          child: Text(
            _cubit.currentStep1?.seed?.cleanSeedGuide ?? "",
            style: Theme.of(context).textTheme.regular400.copyWith(
                fontSize: 12.sp, height: 18.h / 12.sp, color: R.color.textNavy),
          ),
        ),
      ],
    );
  }

  int get _currentIndex {
    if (_cubit.currentStep1 == null)
      return -1;
    else {
      if (_compensationController.text.isNotEmpty) return 3;
      if (_engagementController.text.isNotEmpty) return 2;
      if (_regretController.text.isNotEmpty) return 1;
      return 0;
    }
  }

  bool get _hasChanges =>
      _cubit.currentStep1?.id != _cubit.initialData?.step1?.id ||
      _regretController.text != _cubit.initialData?.step2 ||
      _engagementController.text != _cubit.initialData?.step3 ||
      _compensationController.text != _cubit.initialData?.step4;
}
