import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:imi/res/R.dart';

import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/clean_seeds_request.dart';
import 'package:imi/src/data/network/response/clean_seed_history_response.dart';
import 'package:imi/src/data/network/response/clean_seeds_setting_response.dart';
import 'package:imi/src/data/network/response/image_seed_data.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/clean_seeds/clean_seeds_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/date_util.dart';

class CleanSeedsCubit extends Cubit<CleanSeedsState> {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;
  int? cleanSeedId;

  CleanSeedsCubit(
      {required this.graphqlRepository,
      required this.repository,
      this.cleanSeedId})
      : super(CleanSeedsInitialState()) {
    if (cleanSeedId != null) {
      getCleanSeedDetails(cleanSeedId??0);
    }
  }

  Step1? currentStep1;

  List<bool> isInputEmpty = List.generate(3, (index) => true);
  List<int> textLength = List.generate(3, (index) => 0);

  FetchCleanSeedHistoriesData? initialData;
  ImageSeedData? imageSeedData;

  bool _editable = true;
  bool get editable {
    if (initialData?.modifiedDate != null)
      return _editable &&
          DateUtils.isSameDay(initialData?.modifiedDate, DateTime.now());
    return _editable;
  }

  void onInputChanged(String val, int index) {
    emit(CleanSeedsInitialState());
    textLength[index]=val.length;
    if (val.isEmpty != isInputEmpty[index]) {
      isInputEmpty[index] = val.isEmpty;
      emit(CleanSeedsInputChangeState());
    }
  }

  Future<void> createCleanSeed(
      String step2, String step3, String step4, int currentStep) async {
    emit(CleanSeedsLoadingState());
    final res = await repository.createCleanSeed(CleanSeedsRequest(
        step1: CleanSeedsAction(actionId: currentStep1?.id ?? 0),
        step2: step2,
        step3: step3,
        step4: step4,
        steps: List.generate(currentStep, (index) => index + 1)));
    res.when(success: (_) {
      emit(CleanSeedsCreateSuccessState());
    }, failure: (e) {
      emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  Future<void> copySeed(
      String step2, String step3, String step4, int currentStep) async {
    emit(CleanSeedsLoadingState());
    final res = await repository.createCleanSeed(CleanSeedsRequest(
        step1: CleanSeedsAction(actionId: currentStep1?.id ?? 0),
        step2: step2,
        step3: step3,
        step4: step4,
        steps: List.generate(currentStep, (index) => index + 1)));
    res.when(success: (data) {
      emit(CleanSeedsCopySuccessState(data));
    }, failure: (e) {
      emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  Future updateCleanSeed(int cleanSeedId, String step2, String step3,
      String step4, int currentStep) async {
    checkEditable();
    if (editable) {
      emit(CleanSeedsLoadingState());
      final res = await repository.updateCleanSeed(
          cleanSeedId,
          CleanSeedsRequest(
              step1: CleanSeedsAction(actionId: currentStep1?.id ?? 0),
              step2: step2,
              step3: step3,
              step4: step4,
              steps: List.generate(currentStep, (index) => index + 1)));
      res.when(success: (_) {
        emit(CleanSeedsUpdateSuccessState());
        getCleanSeedDetails(cleanSeedId);
      }, failure: (e) {
        if (e is BadRequest && e.code == ServerError.update_history_error)
          emit(CleanSeedsFailureState(
              R.string.you_cannot_update_clean_seed.tr()));
        else
          emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(e)));
      });
    } else {
      emit(CleanSeedsFailureState(R.string.cannot_update_clean_seed.tr()));
    }
  }

  void checkEditable() {
    if (DateUtils.isSameDay(DateTime.now(), initialData?.modifiedDate)) {
      _editable = true;
    } else {
      _editable = false;
    }
  }

  Future getCleanSeedDetails(int cleanSeedId) async {
    emit(CleanSeedsLoadingState());
    final res =
        await graphqlRepository.getCleanSeedById(cleanSeedId: cleanSeedId);
    res.when(success: (data) {
      currentStep1 = data.step1;
      initialData = data;
      emit(CleanSeedsFetchDataSuccessState(data));
    }, failure: (e) {
      emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  Future remove(int cleanSeedId) async {
    emit(CleanSeedsLoadingState());
    final res = await repository.deleteCleanSeed(cleanSeedId);
    res.when(success: (data) {
      emit(CleanSeedsDeleteSuccessState());
    }, failure: (e) {
      if (e is BadRequest && e.code == ServerError.update_history_error)
        emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(e)));
      emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getImageCleanSeed() async {
    emit(CleanSeedsLoadingState());
    ApiResult<ImageSeedData> getTarget =
    await repository.getImageCleanSeed(cleanSeedHistoryId: cleanSeedId);
    getTarget.when(success: (ImageSeedData data) async{
      imageSeedData = data;
      emit(CleanSeedsSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CleanSeedsFailureState(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void downLoadSeed() async {
    emit(CleanSeedsLoadingState());
    if (imageSeedData?.url != null) {
      await ImageDownloader.downloadImage(imageSeedData?.url ?? "");
    }
    emit(ShareCleanSeedsSuccessState());
  }
}
