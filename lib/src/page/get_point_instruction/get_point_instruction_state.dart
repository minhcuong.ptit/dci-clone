abstract class GetPointInstructionState {}

class GetPointInstructionInitial extends GetPointInstructionState {}

class GetPointInstructionLoading extends GetPointInstructionState {
  @override
  String toString() {
    return 'GetPointInstructionLoading{}';
  }
}

class GetPointInstructionFailure extends GetPointInstructionState {
  final String error;

  GetPointInstructionFailure(this.error);

  @override
  String toString() {
    return 'HistoryPointFailure{error: $error}';
  }
}

class GetPointInstructionSuccess extends GetPointInstructionState {
  @override
  String toString() {
    return 'HistoryPointSuccess{}';
  }
}

class ListGetPointInstructionEmpty extends GetPointInstructionState {
  @override
  String toString() {
    return 'ListGetPointInstructionEmpty{}';
  }
}

class ListHistoryPointOutEmpty extends GetPointInstructionState {
  @override
  String toString() {
    return 'ListHistoryPointOutEmpty{}';
  }
}
