import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/page/course_details/course_details.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/course_response.dart';
import '../../data/network/response/point_action.dart';
import '../../data/network/response/point_action_category_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../utils/const.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../home_challenges/home_challenges.dart';
import '../home_event/home_event.dart';
import '../home_sow/home_sow.dart';
import '../home_study_group/home_study_group.dart';
import '../home_zen/home_zen.dart';
import '../list_course/list_course.dart';
import '../practice_dci/practice_dci.dart';
import 'get_point_instruction.dart';

class GetPointInstructionCubit extends Cubit<GetPointInstructionState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<PointActionDataFetchPointActionIn?> pointActionIn = [];
  List<PointActionDataFetchPointActionIn?> practiceDci = [];
  List<PointActionDataFetchPointActionIn?> action = [];
  String actionTitle='';
  String practiceTitle='';
  String interactionTitle='';
  List<PointCategoryDataFetchPointActionCategories?> pointCategory=[];
  String? nextToken;
  List<FetchCoursesData> listCourse = [];

  GetPointInstructionCubit(this.repository, this.graphqlRepository)
      : super(GetPointInstructionInitial());

  void getPointActionIn({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh || !isLoadMore
        ? GetPointInstructionLoading()
        : GetPointInstructionInitial());
    if (isLoadMore != true) {
      nextToken = null;
    }
    pointActionIn.clear();
    ApiResult<PointActionData> getPointActionIn =
        await graphqlRepository.getPointActionIn();
    getPointActionIn.when(success: (PointActionData data) async {
      if (data != null) {
        pointActionIn.addAll(data.fetchPointActionIn ?? []);
        practiceDci = pointActionIn
            .where((element) => element?.category?.type == 'PRACTICE')
            .toList();
        practiceTitle=pointActionIn.first?.category?.name??'';
        action = pointActionIn
            .where((element) => element?.category?.type == 'ACTION')
            .toList();
        actionTitle=action.first?.category?.name??'';
        interactionTitle=pointActionIn.where((element) => element?.category?.type=='SOCIAL_INTERACTION').first?.category?.name??'';
        emit(GetPointInstructionSuccess());
      } else {
        emit(ListGetPointInstructionEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(
          GetPointInstructionFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getPointActionCategories({
    bool isRefresh = false,
  }) async {
    emit(isRefresh
        ? GetPointInstructionLoading()
        : GetPointInstructionInitial());
    ApiResult<PointCategoryData> getPointActionIn =
    await graphqlRepository.getPointActionCategories();
    getPointActionIn.when(success: (PointCategoryData data) async {
      pointCategory.addAll(data.fetchPointActionCategories??[]);
      emit(GetPointInstructionSuccess());

    }, failure: (NetworkExceptions error) async {
      emit(
          GetPointInstructionFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

 void navigatePracticeDci(String typeOfAction, context) {
    if ( typeOfAction.contains('gieo hạt')) {
      NavigationUtils.rootNavigatePage(context, HomeSowPage());
    }
    if ( typeOfAction.contains('tẩy hạt')) {
      NavigationUtils.rootNavigatePage(context, HomeSowPage(directFromGuidePoint: true,));
    }
    if (typeOfAction.contains('thiền định')) {
      NavigationUtils.rootNavigatePage(context, HomeZenPage());
    }
    if (typeOfAction.contains('bộ thử thách')) {
      NavigationUtils.rootNavigatePage(context, HomeChallengesPage());
    }
    if(typeOfAction.contains('nhóm học tập')){
      getListCourse(context);
    }
  }
  void navigateAction(String typeOfAction,context){
    if (typeOfAction.contains('khóa học')) {
      NavigationUtils.rootNavigatePage(context, ListCoursePage());
    }
    if (typeOfAction.contains('sự kiện')) {
      NavigationUtils.rootNavigatePage(context, HomeEventPage());
    }
  }

  void getListCourse(context) async {
    ApiResult<FetchCourse> result =
    await graphqlRepository.getCourse(
        nextToken: nextToken,
        priorityOrder: Order.DESC.name,
        limit: Const.LIMIT);
    result.when(success: (FetchCourse data) async {
      listCourse = data.data ?? [];
      emit(GetPointInstructionSuccess());
      NavigationUtils.rootNavigatePage(context, HomeStudyGroupPage(listCourse: listCourse,));
    }, failure: (NetworkExceptions error) async {
      emit(GetPointInstructionFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
