import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/history_point/history_point.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:math' as math;
import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/history_point_response.dart';
import '../main/main.dart';
import 'get_point_instruction.dart';

class GetPointInstructionPage extends StatefulWidget {
  const GetPointInstructionPage({Key? key}) : super(key: key);

  @override
  State<GetPointInstructionPage> createState() =>
      _GetPointInstructionPageState();
}

class _GetPointInstructionPageState extends State<GetPointInstructionPage> {
  late GetPointInstructionCubit _cubit;
  RefreshController _refreshController = RefreshController();
  List<bool> showSubCategory = [true, true];
  late MainCubit _mainCubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _mainCubit = GetIt.I();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = GetPointInstructionCubit(repository, graphqlRepository);
    // _cubit.getPointActionCategories();
    _cubit.getPointActionIn();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.instructions_receiving_point_dci.tr(),
          backgroundColor: R.color.primaryColor,
          iconColor: R.color.white,
          titleColor: R.color.white,
          centerTitle: true),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<GetPointInstructionCubit, GetPointInstructionState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! GetPointInstructionLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, GetPointInstructionState state) {
    return StackLoadingView(
      visibleLoading: state is GetPointInstructionLoading,
      child: SmartRefresher(
        enablePullUp: true,
        controller: _refreshController,
        onRefresh: () {
          _cubit.getPointActionIn();
        },
        onLoading: () {},
        child:state is GetPointInstructionLoading ?const SizedBox() :Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  R.string.instructions_receiving_more_point_dci.tr(),
                  style: Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: 16.sp,
                        color: R.color.zaffre,
                      ),
                ),
               const SizedBox(
                  height: 34,
                ),
                Text(R.string.can_get_more_dci_point.tr(),
                    style: Theme.of(context).textTheme.bodySmallText),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      R.string.dci_activities.tr(),
                      style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 16.sp,
                            color: R.color.zaffre,
                          ),
                    ),
                    Text(
                      R.string.points_received.tr(),
                      style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 16.sp,
                            color: R.color.zaffre,
                          ),
                    )
                  ],
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: _cubit.pointActionIn.length,
                  itemBuilder: (context, int index) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: context.width * 0.6,
                          child: Text(
                            _cubit.pointActionIn[index]?.name ?? '',
                            style:
                                Theme.of(context).textTheme.buttonLink.copyWith(
                                      fontSize: 16.sp,
                                    ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          _cubit.pointActionIn[index]?.point.toString() ?? '0',
                          style:
                              Theme.of(context).textTheme.buttonLink.copyWith(
                                    fontSize: 16.sp,
                                  ),
                        )
                      ],
                    );
                  },
                ),
                const SizedBox(
                  height: 25,
                ),
                //Category Thực hành DCI
                // ListView.builder(
                //     shrinkWrap: true,
                //     physics: const NeverScrollableScrollPhysics(),
                //     itemCount: _cubit.pointCategory.length,
                //     itemBuilder:(context,index){
                //       return Category(
                //           _cubit.pointCategory[index]?.name??'', false, 0, showSubCategory[0]);
                //     } ),
                Category(
                   _cubit.practiceTitle, false, 0, showSubCategory[0]),
                showSubCategory[0]
                    ? ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: _cubit.practiceDci.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () =>
                                _cubit.navigatePracticeDci(_cubit.practiceDci[index]?.name??'', context),
                            child: SubCategory(
                              _cubit.practiceDci[index]?.name ?? '',
                              _cubit.practiceDci[index]?.point.toString() ?? '',
                            ),
                          );
                        },
                      )
                    : const SizedBox.shrink(),
                //Category hành động
                Category( _cubit.actionTitle, false, 1, showSubCategory[1]),
                showSubCategory[1]
                    ? ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: _cubit.action.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () => _cubit.navigateAction(_cubit.action[index]?.name??'', context),
                            child: SubCategory(
                              _cubit.action[index]?.name ?? '',
                              _cubit.action[index]?.point.toString() ?? '',
                            ),
                          );
                        },
                      )
                    : const SizedBox.shrink(),
                //Category tương tác cộng đồng
                GestureDetector(
                  onTap: () {
                    NavigationUtils.popToFirst(context);
                   _mainCubit.selectTab(1);
                  },
                  child: Category(
                     _cubit.interactionTitle, true, 3, false),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget Category(String title, bool isSocial, int index, bool show) {
    return Container(
      margin: EdgeInsets.only(bottom: 2),
      decoration: BoxDecoration(
          color: R.color.primaryColor, borderRadius: BorderRadius.circular(4)),
      height: 48,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 4),
              child: Text(
                title,
                style: Theme.of(context).textTheme.bold700.copyWith(
                      fontSize: 16.sp,
                      color: R.color.white,
                    ),
              ),
            ),
            isSocial
                ? const SizedBox.shrink()
                : GestureDetector(
                    onTap: () {
                      setState(() {
                        if (index == 0) {
                          showSubCategory[0] = !showSubCategory[0];
                        }
                        if (index == 1) {
                          showSubCategory[1] = !showSubCategory[1];
                        }
                      });
                    },
                    child: show
                        ? Transform.rotate(
                            angle: 90 * math.pi / 180,
                            child:const Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.white,
                              size: 18,
                            ),
                          )
                        :const Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.white,
                            size: 18,
                          ))
          ],
        ),
      ),
    );
  }

  Widget SubCategory(
    String title,
    String point,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 2),
      decoration: BoxDecoration(
          border: Border.all(color: R.color.light_grey, width: 1),
          borderRadius: BorderRadius.circular(4)),
      height: 48,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: Theme.of(context).textTheme.bold700.copyWith(
                    fontSize: 16.sp,
                  ),
            ),
            Text(
              point,
              style: Theme.of(context).textTheme.bold700.copyWith(
                    fontSize: 16.sp,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
