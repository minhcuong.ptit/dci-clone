import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/create_zen/create_zen.dart';
import 'package:imi/src/page/create_zen/detail_meditation_widget.dart';
import 'package:imi/src/page/setting_sound_meditation/setting_sound_meditation.dart';
import 'package:imi/src/page/time_meditation/time_meditation_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/page/zen/zen_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../data/preferences/app_preferences.dart';

class CreateZenPage extends StatefulWidget {
  final int meditationId;

  CreateZenPage({required this.meditationId});

  @override
  State<CreateZenPage> createState() => _CreateZenPageState();
}

class _CreateZenPageState extends State<CreateZenPage> {
  late CreateZenCubit _cubit;
  RefreshController _refreshController = RefreshController();
  bool timerUnlimited = false;

  bool get isHasMeditationWithMusic =>
      _cubit.fetchMeditationByIdData?.fileWithMusicDuration != null &&
      _cubit.fetchMeditationByIdData?.fileWithMusicDuration != 0;

  bool get isHasMeditationWithNoMusic =>
      _cubit.fetchMeditationByIdData?.fileWithNoMusicDuration != null &&
      _cubit.fetchMeditationByIdData?.fileWithNoMusicDuration != 0;

  Duration initialTimer = new Duration();

  int get hours => appPreferences.getInt(Const.HOURS) ?? 0;

  int get minutes => appPreferences.getInt(Const.MINUTES) ?? 0;

  int get seconds => appPreferences.getInt(Const.SECONDS) ?? 0;

  String? get timerStart => appPreferences.getString(Const.TIMER_START);

  String? get bellsStart => appPreferences.getString(Const.BELLS_START);

  String? get bellsEnd => appPreferences.getString(Const.BELLS_END);

  String? get bellsDelay => appPreferences.getString(Const.BELLS_DELAY);

  String? get audio => appPreferences.getString(Const.AUDIO);

  int? get timer => appPreferences.getInt(Const.TIMER);

  // bool? get timerUnlimited =>
  //     appPreferences.getBool(Const.TIMER_UNLIMITED) ?? false;

  String? get bellsAmbientSound =>
      appPreferences.getString(Const.BELLS_AMBIENT_SOUND_INDEX);

  // lastButtonPress = lastPressString!=null ? DateTime.parse(lastPressString) : DateTime.now();
  String timeStartMusic = "${0}  ${R.string.seconds.tr()}";
  String timeStartNoMusic = "${0}  ${R.string.seconds.tr()}";

  int get repetitionBellStart =>
      appPreferences.getInt(Const.REPETITION_BELL_START) ?? 0;

  int get repetitionBellEnd =>
      appPreferences.getInt(Const.REPETITION_BELL_END) ?? 0;

  int? get selectedBellsAmbientSound =>
      appPreferences.getInt(Const.INDEX_BELL_AMBIENT_SOUND);

  int get repetitionDelay =>
      appPreferences.getInt(Const.REPETITION_DELAY) ?? 100000000;

  bool get isNotContainAudio =>
      appPreferences.getInt(Const.INDEX_BELL_AMBIENT_SOUND) == 0 &&
      appPreferences.getString(Const.FILE_AUDIO) == null&&
          appPreferences.getBool(Const.BOOL_AMBIENT_SOUND) == true;

  bool get notChooseTimeMeditationYet =>
      appPreferences.getBool(Const.TIMER_UNLIMITED) == false &&
      appPreferences.getString(Const.TIMER_START) == null &&
      appPreferences.getInt(Const.TIMER) == null||appPreferences.getInt(Const.TIMER)==0;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    appPreferences.setData(Const.TIMER_UNLIMITED, false);
    appPreferences.removeData(Const.TIMER_START);
    appPreferences.removeData(Const.TIMER);
    appPreferences.removeData(Const.FILE_AUDIO);
    appPreferences.setData(Const.BOOL_AMBIENT_SOUND, true);
    appPreferences.setData(Const.BOOL_BELL_START, true);
    appPreferences.setData(Const.BOOL_BELL_END, true);
    appPreferences.setData(Const.BOOL_BELL_DELAY, true);
    appPreferences.setData(Const.INDEX_BELL_START, 0);
    appPreferences.setData(Const.INDEX_BELL_END, 0);
    appPreferences.setData(Const.INDEX_BELL_DELAY, 0);
    appPreferences.setData(Const.INDEX_BELL_AMBIENT_SOUND, 0);
    _cubit = CreateZenCubit(repository, graphqlRepository, widget.meditationId);
    _cubit.getDetailMeditation();
    _cubit.getAudio();
  }

  @override
  void dispose() {
    _cubit.close();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar: appBar(
        context,
        R.string.practice_of_meditation.tr().toUpperCase(),
        onPop: () async {
          NavigationUtils.pop(context);
          appPreferences.removeData(Const.TIMER_START);
          appPreferences.removeData(Const.TIMER);
          appPreferences.setData(Const.TIMER_UNLIMITED, false);
          appPreferences.removeData(Const.INDEX_BELL_START);
          appPreferences.removeData(Const.INDEX_BELL_END);
          appPreferences.removeData(Const.INDEX_BELL_DELAY);
          appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
          appPreferences.setData(Const.BOOL_BELL_START, true);
          appPreferences.setData(Const.BOOL_BELL_DELAY, true);
          appPreferences.setData(Const.BOOL_AMBIENT_SOUND, true);
          appPreferences.setData(Const.BOOL_BELL_END, true);
          appPreferences.removeData(Const.FILE_AUDIO);
          appPreferences.removeData(Const.AUDIO);
        },
        backgroundColor: R.color.primaryColor,
        titleColor: R.color.white,
        centerTitle: true,
        iconColor: R.color.white,
      ),
      body: WillPopScope(
        onWillPop: () async {
          NavigationUtils.pop(context);
          appPreferences.removeData(Const.TIMER_START);
          appPreferences.removeData(Const.TIMER);
          appPreferences.setData(Const.TIMER_UNLIMITED, false);
          appPreferences.removeData(Const.INDEX_BELL_START);
          appPreferences.removeData(Const.INDEX_BELL_END);
          appPreferences.removeData(Const.INDEX_BELL_DELAY);
          appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
          appPreferences.setData(Const.BOOL_BELL_START, true);
          appPreferences.setData(Const.BOOL_BELL_DELAY, true);
          appPreferences.setData(Const.BOOL_AMBIENT_SOUND, true);
          appPreferences.setData(Const.BOOL_BELL_END, true);
          appPreferences.removeData(Const.FILE_AUDIO);
          appPreferences.removeData(Const.AUDIO);
          return true;
        },
        child: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<CreateZenCubit, CreateZenState>(
            listener: (context, state) {
              // TODO: implement listener
            },
            builder: (context, state) {
              return buildPage(context, state);
            },
          ),
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, CreateZenState state) {
    List<String> listSecond = ["0", "5", "10", "20", "30", "60"];
    int second = _cubit.fetchMeditationByIdData?.fileWithMusicDuration ?? 0;
    int secondNoMusic =
        _cubit.fetchMeditationByIdData?.fileWithNoMusicDuration ?? 0;
    return StackLoadingView(
      visibleLoading: state is CreateZenLoading,
      child: ListView(
        padding: EdgeInsets.all(20.h),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: RichText(
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(
                    text: "${R.string.you_choose.tr()} ",
                    style: Theme.of(context)
                        .textTheme
                        .medium500
                        .copyWith(fontSize: 16.sp, color: R.color.black),
                    children: <TextSpan>[
                      TextSpan(
                          text: _cubit.fetchMeditationByIdData?.title ?? '',
                          style: Theme.of(context).textTheme.medium500.copyWith(
                              fontSize: 16.sp, color: R.color.primaryColor)),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 10.w),
              GestureDetector(
                  onTap: () {
                    NavigationUtils.navigatePage(
                        context,
                        TutorialMeditationWidget(
                            _cubit.fetchMeditationByIdData));
                  },
                  child: Image.asset(R.drawable.ic_help_zen,
                      height: 24.h, width: 24.h)),
            ],
          ),
          SizedBox(height: 20.h),
          Text(
            R.string.set_sound_and_time.tr(),
            style: Theme.of(context)
                .textTheme
                .medium500
                .copyWith(fontSize: 16.sp, color: R.color.primaryColor),
          ),
          SizedBox(height: 20.h),
          isHasMeditationWithMusic
              ? buildCreateZen(
                  title: R.string.meditate_with_the_guide_file_with_music.tr(),
                  time: "${Duration(seconds: second).inHours % 3600 == 0 ? "" : "${Duration(seconds: second).inHours % 3600} " + R.string.hour.tr().toLowerCase()} " +
                      "${Duration(seconds: second).inMinutes % 60 == 0 ? "" : "${Duration(seconds: second).inMinutes % 60} " + R.string.minutes.tr().toLowerCase()} " +
                      "${Duration(seconds: second).inSeconds % 60 == 0 ? "" : "${Duration(seconds: second).inSeconds % 60} " + R.string.seconds.tr().toLowerCase()}",
                  callback: () {
                    _cubit.checkZen(0);
                  },
                  color: _cubit.selectIndex == 0
                      ? R.color.primaryColor
                      : R.color.lightBlue)
              : const SizedBox.shrink(),
          Visibility(
              visible: _cubit.selectIndex == 0,
              child: Column(
                children: [
                  SizedBox(height: 15.h),
                  Row(
                    children: [
                      Text(
                        R.string.start_up_time.tr(),
                        style: Theme.of(context).textTheme.medium500.copyWith(
                            fontSize: 12.sp,
                            color: R.color.black,
                            height: 20 / 16),
                      ),
                      SizedBox(width: 60.w),
                      CupertinoPageScaffold(
                        child: Center(
                          child: Container(
                            color: R.color.lightestGray,
                            height: 50.h,
                            width: 70.w,
                            child: CupertinoPicker(
                              selectionOverlay: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.h),
                                    color: R.color.blue.withOpacity(0.1),
                                    border: Border.all(color: R.color.blue)),
                              ),
                              itemExtent: 30.h,
                              useMagnifier: false,
                              scrollController:
                                  FixedExtentScrollController(initialItem: 0),
                              children: List.generate(
                                  listSecond.length,
                                  (index) =>
                                      buildTextSecond(listSecond[index])),
                              onSelectedItemChanged: (value) {
                                setState(() {
                                  timeStartMusic = listSecond[value];
                                  appPreferences.setData(
                                      Const.TIMER_START_MUSIC, timeStartMusic);
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              )),
          isHasMeditationWithMusic
              ? SizedBox(height: 15.h)
              : const SizedBox.shrink(),
          isHasMeditationWithNoMusic
              ? buildCreateZen(
                  title:
                      R.string.meditate_with_the_guide_file_with_no_music.tr(),
                  time:
                      "${Duration(seconds: secondNoMusic).inHours % 3600 == 0 ? "" : "${Duration(seconds: secondNoMusic).inHours % 3600} " + R.string.hour.tr().toLowerCase()} ${Duration(seconds: secondNoMusic).inMinutes % 60 == 0 ? "" : "${Duration(seconds: secondNoMusic).inMinutes % 60} ${R.string.minutes.tr().toLowerCase()}"} ${Duration(seconds: secondNoMusic).inSeconds % 60 == 0 ? "" : "${Duration(seconds: secondNoMusic).inSeconds % 60} ${R.string.seconds.tr().toLowerCase()}"}",
                  callback: () {
                    _cubit.checkZen(1);
                  },
                  color: _cubit.selectIndex == 1
                      ? R.color.primaryColor
                      : R.color.lightBlue)
              : const SizedBox.shrink(),
          Visibility(
              visible: _cubit.selectIndex == 1,
              child: Column(
                children: [
                  SizedBox(height: 15.h),
                  Row(
                    children: [
                      Text(
                        R.string.start_up_time.tr(),
                        style: Theme.of(context).textTheme.medium500.copyWith(
                            fontSize: 12.sp,
                            color: R.color.black,
                            height: 20 / 16),
                      ),
                      SizedBox(width: 60.w),
                      CupertinoPageScaffold(
                        child: Center(
                          child: Container(
                            color: R.color.lightestGray,
                            height: 50.h,
                            width: 70.w,
                            child: CupertinoPicker(
                              selectionOverlay: Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.h),
                                    color: R.color.blue.withOpacity(0.1),
                                    border: Border.all(color: R.color.blue)),
                              ),
                              itemExtent: 30.h,
                              useMagnifier: false,
                              scrollController:
                                  FixedExtentScrollController(initialItem: 0),
                              children: List.generate(
                                  listSecond.length,
                                  (index) =>
                                      buildTextSecond(listSecond[index])),
                              onSelectedItemChanged: (value) {
                                setState(() {
                                  timeStartNoMusic = listSecond[value];
                                  appPreferences.setData(
                                      Const.TIMER_START_NO_MUSIC,
                                      timeStartNoMusic);
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              )),
          isHasMeditationWithNoMusic
              ? SizedBox(height: 15.h)
              : const SizedBox.shrink(),
          buildCreateZen(
              title: R.string.meditation_does_not_file.tr(),
              time: R.string.set_your_own_meditation_time.tr(),
              callback: () {
                _cubit.checkZen(2);
              },
              color: _cubit.selectIndex == 2
                  ? R.color.primaryColor
                  : R.color.lightBlue),
          Visibility(
              visible: _cubit.selectIndex == 2,
              child: GestureDetector(
                onTap: () {
                  NavigationUtils.navigatePage(
                          context, const TimeMeditationPage())
                      .then((value) => _cubit.refresh());
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 15.h),
                    buildSettingZen(R.string.set_meditation_time.tr(), () {
                      NavigationUtils.rootNavigatePage(
                              context, const TimeMeditationPage())
                          .then((value) => _cubit.refresh());
                    }),
                    SizedBox(height: 5.h),
                    Row(
                      children: [
                        Text(
                          R.string.switch_on.tr(args: [" "]) +
                              "${timerStart ?? 0}" +
                              " " +
                              R.string.seconds.tr() +
                              " - " +
                              R.string.zen.tr(args: [" "]),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 12.sp,
                                  color: R.color.primaryColor,
                                  height: 24 / 16),
                        ),
                        Text(
                          appPreferences.getBool(Const.TIMER_UNLIMITED) == false
                              ? DateUtil.parseDateToString(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      timer ?? 0,
                                      isUtc: true),
                                  "HH:mm:ss")
                              : R.string.unlimited_time.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 12.sp,
                                  color: R.color.primaryColor,
                                  height: 24 / 16),
                        ),
                      ],
                    ),
                    SizedBox(height: 10.h),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: R.color.lighterGray,
                    ),
                    SizedBox(height: 15.h),
                    buildSettingZen(R.string.ambient_sound_settings.tr(), () {
                      NavigationUtils.rootNavigatePage(
                          context, const SettingSoundMeditationPage());
                    }),
                    SizedBox(height: 15.h),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: R.color.lighterGray,
                    ),
                  ],
                ),
              )),
          SizedBox(height: 150.h),
          Visibility(
            visible: _cubit.selectIndex == 0 ||
                _cubit.selectIndex == 1 ||
                _cubit.selectIndex == 2,
            child: ButtonWidget(
                title: R.string.start.tr(),
                textSize: 14.sp,
                backgroundColor: R.color.primaryColor,
                padding: EdgeInsets.symmetric(
                  vertical: 14.h,
                ),
                onPressed: () {
                  if(notChooseTimeMeditationYet&&_cubit.selectIndex==2){
                   setState((){
                     Utils.showErrorSnackBar(context, 'Bạn chưa chọn thời gian thiền');
                   });
                    return;
                  }
                  if (_cubit.selectIndex == 0) {
                    NavigationUtils.navigatePage(
                        context,
                        ZenPage(
                          index: _cubit.selectIndex ?? 0,
                          meditationId: widget.meditationId,
                          totalTime: Duration(
                              seconds: _cubit.fetchMeditationByIdData
                                      ?.fileWithMusicDuration ??
                                  0),
                          option: ZenOption(
                              delay: int.parse(_cubit.timeMusic),
                              backgroundMusic: null,
                              startOption: ZenRepeatOption(
                                  musicPath: _cubit.fetchMeditationByIdData
                                      ?.fileWithMusicUrl,
                                  repeatTimes: 1),
                              endOption: ZenRepeatOption(
                                  musicPath: null, repeatTimes: 0),
                              intervalOption: ZenIntervalOption(
                                  musicPath: null,
                                  intervalDuration: Duration(minutes: 50))),
                        ));
                  } else if (_cubit.selectIndex == 1) {
                    NavigationUtils.navigatePage(
                        context,
                        ZenPage(
                          index: _cubit.selectIndex ?? 0,
                          meditationId: widget.meditationId,
                          totalTime: Duration(
                              seconds: _cubit.fetchMeditationByIdData
                                      ?.fileWithNoMusicDuration ??
                                  0),
                          option: ZenOption(
                            delay: int.parse(_cubit.timeNoMusic),
                            backgroundMusic: null,
                            startOption: ZenRepeatOption(
                                musicPath: _cubit.fetchMeditationByIdData
                                    ?.fileWithNoMusicUrl,
                                repeatTimes: 1),
                            endOption: ZenRepeatOption(
                                musicPath: null, repeatTimes: 0),
                            intervalOption: ZenIntervalOption(
                              musicPath: null,
                              intervalDuration: Duration(minutes: 50),
                            ),
                          ),
                        ));
                  } else if (_cubit.selectIndex == 2) {
                    NavigationUtils.navigatePage(
                        context,
                        ZenPage(
                          index: _cubit.selectIndex ?? 0,
                          meditationId: widget.meditationId,
                          totalTime:
                              appPreferences.getBool(Const.TIMER_UNLIMITED) ==
                                      true
                                  ? null
                                  : Duration(
                                      hours: hours,
                                      minutes: minutes,
                                      seconds: seconds),
                          option: ZenOption(
                              delay: int.parse(timerStart!),
                              backgroundMusic: isNotContainAudio
                                  ? null
                                  : selectedBellsAmbientSound == null
                                      ? audio
                                      : _cubit
                                          .listAudio[selectedBellsAmbientSound!]
                                          .fileUrl,
                              startOption: ZenRepeatOption(
                                  musicPath: bellsStart,
                                  repeatTimes: repetitionBellStart),
                              endOption: ZenRepeatOption(
                                  musicPath: bellsEnd,
                                  repeatTimes: repetitionBellEnd),
                              intervalOption: ZenIntervalOption(
                                  musicPath: bellsDelay,
                                  intervalDuration:
                                      Duration(minutes: repetitionDelay))),
                        ));
                  } else {}
                }),
          )
        ],
      ),
    );
  }

  Widget buildSettingZen(String title, VoidCallback callback) {
    return GestureDetector(
      onTap: callback,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.medium500.copyWith(
                fontSize: 12.sp, color: R.color.black, height: 20 / 16),
          ),
          IconButton(
              padding: EdgeInsets.all(3),
              constraints: BoxConstraints(),
              onPressed: callback,
              icon: Icon(CupertinoIcons.chevron_right, size: 12))
        ],
      ),
    );
  }

  Widget buildTextSecond(String second) {
    return Text(
      "$second ${R.string.seconds.tr()}",
      style: Theme.of(context)
          .textTheme
          .medium500
          .copyWith(fontSize: 12.sp, color: R.color.blue, height: 24.h / 12.sp),
    );
  }

  Widget buildCreateZen(
      {String? title, String? time, VoidCallback? callback, Color? color}) {
    return InkWell(
      onTap: callback,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.h, vertical: 10.h),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40.h), color: color),
        child: Row(
          children: [
            Image.asset(R.drawable.ic_zen, height: 24.h, width: 25.h),
            SizedBox(width: 16.w),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.65,
                  child: Text(
                    title ?? "",
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.medium500.copyWith(
                        fontSize: 14.sp, color: R.color.white, height: 20 / 16),
                  ),
                ),
                Text(
                  time ?? "",
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 12.sp, color: R.color.white, height: 17 / 14),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
