abstract class CreateZenState {
  CreateZenState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class CreateZenInitial extends CreateZenState {
  @override
  String toString() => 'CreateZenInitial';
}

class CreateZenSuccess extends CreateZenState {
  @override
  String toString() => 'CreateZenSuccess';
}

class CreateZenLoading extends CreateZenState {
  @override
  String toString() => 'CreateZenLoading';
}

class CreateZenFailure extends CreateZenState {
  final String error;

  CreateZenFailure(this.error);

  @override
  String toString() => 'CreateZenFailure { error: $error }';
}

class CreateAudioSuccess extends CreateZenState {
  @override
  String toString() => 'CreateAudioSuccess';
}
