import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/detail_meditation_response.dart';
import 'package:imi/src/page/course_details/widget/describe_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

class TutorialMeditationWidget extends StatelessWidget {
  final FetchMeditationByIdData? data;


  TutorialMeditationWidget(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar(
          context,
          "Hướng dẫn",
          backgroundColor: R.color.white,
          titleColor: R.color.black,
          centerTitle: true,
          iconColor: R.color.black,
        ),
        body: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 20.h),
          children: [
            Text(data?.title ?? "",
                style: Theme.of(context).textTheme.bold700.copyWith(
                      fontSize: 16.sp,
                    )),
            SizedBox(height: 15.h),
            HtmlWidget(
             data?.description ?? "",
              textStyle: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12.sp),
              onTapUrl: (url) async {
                if (await canLaunch(url)) {
                  await launch(
                    url,
                  );
                } else {
                  throw 'Could not launch $url';
                }
                return launch(
                  url,
                );
              },
              factoryBuilder: () => MyWidgetFactory(),
              enableCaching: true,
            ),
            SizedBox(height: 20.h),
          ],
        ));
  }
}
