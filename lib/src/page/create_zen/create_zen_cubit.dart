import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/audio_response.dart';
import 'package:imi/src/data/network/response/detail_meditation_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/create_zen/create_zen_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/utils.dart';

import '../../data/preferences/app_preferences.dart';

class CreateZenCubit extends Cubit<CreateZenState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  int? selectIndex;
  int meditationId;
  FetchMeditationByIdData? fetchMeditationByIdData;

  String get timeMusic =>
      appPreferences.getString(Const.TIMER_START_MUSIC) ?? "0";

  String get timeNoMusic =>
      appPreferences.getString(Const.TIMER_START_NO_MUSIC) ?? "0";
  bool isSetTime = false;

  List<AudioDataFetchAudio> listAudio = [];

  CreateZenCubit(this.appRepository, this.graphqlRepository, this.meditationId)
      : super(CreateZenInitial());

  void checkZen(int index) {
    emit(CreateZenLoading());
    this.selectIndex = index;
    emit(CreateZenInitial());
  }

  void refresh() {
    emit(CreateZenLoading());
    emit(CreateZenInitial());
  }

  static String getTime({String? hours, String? minutes, String? seconds}) {
    String result = "";
    if (!Utils.isEmpty(hours)) {
      result += hours!;
    }
    if (!Utils.isEmpty(minutes)) {
      if (!Utils.isEmpty(result)) result += ":";
      result += minutes!;
    }
    if (!Utils.isEmpty(seconds)) {
      if (!Utils.isEmpty(result)) result += ":";
      result += seconds!;
    }
    return result;
  }

  void getDetailMeditation() async {
    emit(CreateZenLoading());
    ApiResult<FetchMeditationByIdData> getMeditation =
        await graphqlRepository.getDetailMeditation(meditationId: meditationId);
    getMeditation.when(success: (FetchMeditationByIdData data) async {
      fetchMeditationByIdData = data;
      emit(CreateZenSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CreateZenFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void setTime() {
    emit(CreateZenLoading());
    appPreferences.removeData(Const.TIMER_START_MUSIC);
    appPreferences.removeData(Const.TIMER_START_NO_MUSIC);
    emit(CreateZenInitial());
  }

  void getAudio() async {
    emit(CreateZenLoading());
    ApiResult<AudioData> apiResult =
        await graphqlRepository.getAudio(audioType: AudioType.ENVIRONMENT);
    apiResult.when(success: (AudioData data) {
      listAudio = data.fetchAudio ?? [];
      emit(CreateAudioSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CreateZenFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
