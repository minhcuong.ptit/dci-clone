import 'package:flutter/material.dart';

@immutable
abstract class HomeStudyGroupState {
  HomeStudyGroupState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialHomeStudyGroupState extends HomeStudyGroupState {}

class HomeStudyGroupLoading extends HomeStudyGroupState {
  @override
  String toString() => 'HomeStudyGroupLoading';
}

class GetListCategorySuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunityHomeSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'GetListCommunityHomeSuccess';
  }
}

class HomeStudyGroupSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'HomeStudyGroupSuccess';
  }
}

class GetGroupCompleteSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'GetGroupCompleteSuccess';
  }
}

class GetGroupActiveSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'GetGroupActiveSuccess';
  }
}

class UpdateLevelIdSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'UpdateLevelIdSuccess';
  }
}

class ChooseCommunitySuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

class HomeStudyGroupFailure extends HomeStudyGroupState {
  final String error;

  HomeStudyGroupFailure(this.error);

  @override
  String toString() => 'BodyParameterFailure { error: $error }';
}

class SubmitHomeStudyGroupSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'SubmitHomeStudyGroupSuccess{}';
  }
}

class GetListCourseSuccess extends HomeStudyGroupState {
  @override
  String toString() {
    return 'GetListCourseSuccess';
  }
}

class GetListCourseEmpty extends HomeStudyGroupState {
  @override
  String toString() {
    return 'GetListCourseEmpty';
  }
}
