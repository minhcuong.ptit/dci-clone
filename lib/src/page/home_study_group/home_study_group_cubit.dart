import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'dart:developer';
import '../../data/network/response/list_dci_community_response.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/logger.dart';
import 'home_study_group.dart';

class HomeStudyGroupCubit extends Cubit<HomeStudyGroupState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityDataV2> tempListCommunitySuggestion = [];
  List<CommunityDataV2> listCommunityActive = [];
  List<CommunityDataV2> listCommunityCompleted = [];
  Map<String, List<CommunityDataV2>> listCommunitySuggestion = {};
  CommunityDataV2? communityDataV2;
  List<int> levelIds = [];
  Set<int> idCommunity = {};
  bool isHasSuggestion = false;
  List<FetchCoursesData> listCourse = [];
  String? nextToken;
  AppPreferences appPreferences = AppPreferences();
  bool isNoneFollowing =false;
  HomeStudyGroupCubit(
      {required this.repository, required this.graphqlRepository})
      : super(InitialHomeStudyGroupState());

  void getActiveGroup()async{
    emit(HomeStudyGroupLoading());
    ApiResult<dynamic> apiResult = await  graphqlRepository.getDCICommunities(CommunityType.STUDY_GROUP,limit: Const.LIMIT,
        status: CommunityStatus.ACTIVE, nextToken: null);
    apiResult.when(success: (dynamic data) async {
      if (data is ListDCICommunityResponse) {
        listCommunityActive = data.dciCommunities?.data ?? [];
        getCompleteGroup();
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeStudyGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
  void getCompleteGroup()async{
    emit(HomeStudyGroupLoading());
    ApiResult<dynamic> apiResult = await  graphqlRepository.getDCICommunities(CommunityType.STUDY_GROUP,limit: Const.LIMIT,
        status: CommunityStatus.COMPLETED, nextToken: null);
    apiResult.when(success: (dynamic data) async {
      if (data is ListDCICommunityResponse) {
        listCommunityCompleted = data.dciCommunities?.data ?? [];
        // getListCommunitiesSuggestion();
        getLevelId();
        // emit(GetGroupCompleteSuccess());
        // getListCommunitiesSuggestion(levelIds:appPreferences
        //         .getListString(Const.DCI_LEVEL_ID)
        //         .map((e) => int.parse(e))
        //         .toList());
        emit(GetGroupCompleteSuccess());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeStudyGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  int mySortComparison(CategoryData a, CategoryData b) {
    return (a.isSelected == b.isSelected) ? -1 : 1;
  }

  void getListCommunitiesSuggestion(
      {bool isRefresh = false, List<int>? levelIds}) async {
    // emit(HomeStudyGroupLoading());
    ApiResult<dynamic> apiResult = await graphqlRepository.getDCICommunities(
        CommunityType.STUDY_GROUP,
        limit: Const.LIMIT,
        isValid: true,
        levelIds: levelIds ?? []);
    apiResult.when(success: (dynamic data) async {
      if (data is ListDCICommunityResponse) {
        isHasSuggestion=false;
        tempListCommunitySuggestion = data.dciCommunities?.data ?? [];
        listCommunitySuggestion = tempListCommunitySuggestion.groupBy(
            (m) => m.communityInfo?.communityLevel?.first?.levelName ?? '');
        if (tempListCommunitySuggestion.isNotEmpty) {
          isHasSuggestion = true;
        }
        isNoneFollowing=listCommunityCompleted.isEmpty&&listCommunityActive.isEmpty;
        emit(HomeStudyGroupSuccess());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeStudyGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getLevelId()async{
    // emit(HomeStudyGroupLoading());
    ApiResult<CommunityDataV2Response> getProfileTask=await graphqlRepository
        .getCommunityV2(appPreferences.personProfile?.communityV2?.id ?? 0);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      appPreferences.saveCommunityV2(data.communityV2);
      getListCommunitiesSuggestion(levelIds: appPreferences
          .getListString(Const.DCI_LEVEL_ID)
          .map((e) => int.parse(e))
          .toList());
    }, failure: (NetworkExceptions error) async {
      emit(HomeStudyGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}


