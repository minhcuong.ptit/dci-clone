
import 'package:bloc/bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/list_dci_community_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/home_study_group/widget/group_widget_state.dart';
import 'package:imi/src/utils/enum.dart';

class GroupWidgetCubit extends Cubit<GroupWidgetState> {
  GroupWidgetCubit(this.repository, this.graphqlRepository)
      : super(GroupWidgetInitial());

  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityDataV2> listCommunitySuggestion = [];

  void getListCommunitiesSuggestion(
      {bool isRefresh = false,
      int? levelIds,
      CommunityStatus? communityStatus}) async {
    emit(GroupLoading());
    ApiResult<dynamic> apiResult = await graphqlRepository.getDCICommunities(
        CommunityType.STUDY_GROUP,
        status: communityStatus,
        isValid: true,
        levelIds: levelIds == null ? null : [levelIds]);
    apiResult.when(success: (dynamic data) async {
      if (data is ListDCICommunityResponse) {
        listCommunitySuggestion = data.dciCommunities?.data ?? [];
      }
      emit(GroupSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(GroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
