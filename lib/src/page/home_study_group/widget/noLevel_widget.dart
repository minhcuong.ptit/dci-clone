import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/page/course_details/course_details_page.dart';
import 'package:imi/src/page/list_course/list_course.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';

class NoLevelWidget extends StatelessWidget {
  final List<FetchCoursesData> data;

  const NoLevelWidget({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      //mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(height: 24.h),
        Image.asset(
          R.drawable.ic_person_none,
          height: 80.h,
          width: 80.w,
        ),
        SizedBox(height: 24.h),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 30.h),
          child: Text(
            R.string.no_study_group.tr(),
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 13.sp, height: 24.h / 13.sp),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 50.h),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.h),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  R.string.course_for_you.tr().toUpperCase(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 16.sp, height: 20.h / 16.sp),
                ),
              ),
              GestureDetector(
                onTap: () {
                  NavigationUtils.rootNavigatePage(context, ListCoursePage());
                },
                child: Text(R.string.more.tr(),
                    style: Theme.of(context).textTheme.medium500.copyWith(
                        fontSize: 14.sp,
                        height: 20.h / 14.sp,
                        color: R.color.primaryColor)),
              )
            ],
          ),
        ),
        SizedBox(height: 10.h),
        GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 16.h,
            crossAxisSpacing: 12.h,
            childAspectRatio: 17 / 20,
          ),
          padding: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 20.h),
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                NavigationUtils.rootNavigatePage(
                    context,
                    CourseDetailsPage(
                      courseId: data[index].id,
                      productId: data[index].productId??0,
                    ));
              },
              child: buildItemCourse(context,
                  imageUrl: data[index].avatarUrl,
                  title: data[index].name,
                  price: "${data[index].currentFee}",
                  discount: "${data[index].preferentialFee}",
                  hot: data[index].isHot),
            );
          },
        )
      ],
    );
  }

  Widget buildItemCourse(BuildContext context,
      {String? imageUrl,
      String? title,
      String? price,
      String? discount,
      bool? hot}) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.all(4.h),
          decoration: BoxDecoration(
              color: R.color.white, borderRadius: BorderRadius.circular(8.h)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8.h),
                child: CachedNetworkImage(
                  height: 120.h,
                  width: double.infinity,
                  imageUrl: imageUrl ?? "",
                  fit: BoxFit.cover,
                  placeholder: (_, __) {
                    return Text(
                      R.string.loading.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp),
                    );
                  },
                ),
              ),
              SizedBox(height: 4.h),
              Text(
                title ?? "",
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.medium500.copyWith(
                    fontSize: 12.sp, color: R.color.neutral1, height: 16 / 12),
              ),
              SizedBox(height: 6.h),
              Spacer(),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "${Utils.formatMoney(price)} VND",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.regular400.copyWith(
                          fontSize: 10.sp,
                          color: R.color.gray,
                          decoration: TextDecoration.lineThrough),
                    ),
                  ),
                  SizedBox(width: 10.w),
                  Text(
                    "${Utils.formatMoney(discount)} VND",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12.sp, color: R.color.red, height: 18 / 12),
                  ),
                ],
              )
            ],
          ),
        ),
        hot == true
            ? Positioned(
                top: 6,
                left: -6,
                child: Stack(
                  children: [
                    Image.asset(R.drawable.ic_hot, height: 18.h, width: 34.w),
                  ],
                ),
              )
            : SizedBox(),
      ],
    );
  }
}
