import 'package:equatable/equatable.dart';

abstract class GroupWidgetState extends Equatable {
  @override
  List<Object> get props => [];
}

class GroupWidgetInitial extends GroupWidgetState {}

class GroupLoading extends GroupWidgetState {
  @override
  String toString() => 'GroupLoading';
}

class GroupFailure extends GroupWidgetState {
  final String error;

  GroupFailure(this.error);

  @override
  String toString() => 'GroupWidgetState { error: $error }';
}

class GroupSuccess extends GroupWidgetState {
  @override
  String toString() {
    return 'GroupSuccess{}';
  }
}
