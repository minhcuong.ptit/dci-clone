import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/page/club_info/club_information/club_information_page.dart';
import 'package:imi/src/page/home_study_group/home_study_group_cubit.dart';
import 'package:imi/src/page/home_study_group/widget/group_widget_cubit.dart';
import 'package:imi/src/page/home_study_group/widget/group_widget_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/study_group_widget.dart';

class GroupWidget extends StatefulWidget {
  final CommunityLevel? communityLevel;
  final VoidCallback onViewAll;
  final CommunityStatus? communityStatus;
  final String? title;

  const GroupWidget(
      {Key? key,
      this.communityLevel,
      required this.onViewAll,
      this.communityStatus,
      this.title})
      : super(key: key);

  @override
  State<GroupWidget> createState() => _GroupWidgetState();
}

class _GroupWidgetState extends State<GroupWidget> {
  late GroupWidgetCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = GroupWidgetCubit(repository, graphqlRepository);
    _cubit.getListCommunitiesSuggestion(
        levelIds: widget.communityLevel?.level,
        communityStatus: widget.communityStatus);
  }

  @override
  Widget build(BuildContext context) {
    final maxWidthOfCell = (ScreenUtil().screenWidth - 24.w - 16.w) / 2;
    return BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<GroupWidgetCubit, GroupWidgetState>(
          listener: (BuildContext context, state) {},
          builder: (BuildContext context, state) {
            return _cubit.listCommunitySuggestion.isEmpty
                ? const SizedBox.shrink()
                : SizedBox(
                    height: 250.h,
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                              widget.title ??
                                  widget.communityLevel?.levelName ??
                                  "",
                              style: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 13.sp, height: 24.h / 13.sp),
                            )),
                            GestureDetector(
                              onTap: widget.onViewAll,
                              child: Text(
                                R.string.view_all.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 11.sp,
                                        height: 24.h / 11.sp,
                                        color: R.color.primaryColor),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 8.h),
                        Expanded(
                          child: ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            //physics: NeverScrollableScrollPhysics(),
                            itemCount: _cubit.listCommunitySuggestion.length,
                            itemBuilder: (context, index) {
                              CommunityDataV2 data =
                                  _cubit.listCommunitySuggestion[index];
                              // bool isSelected = _cubit.listSelectedCommunity
                              //         .indexWhere((element) => element.id == data.id) >=
                              //     0;
                              bool isSelected = false;
                              return Container(
                                margin: EdgeInsets.only(right: 10.h),
                                child: StudyGroupWidget(
                                  width: maxWidthOfCell,
                                  data: data,
                                  onTap: () {
                                    NavigationUtils.navigatePage(
                                        context,
                                        ViewClubPage(
                                          communityId: _cubit
                                                  .listCommunitySuggestion[
                                                      index]
                                                  .id ??
                                              0,
                                        ));
                                    // gotoClubDetail(data.id);
                                    // _cubit.selectCommunity(data);
                                  },
                                  isSelected: isSelected,
                                  isShowStar: true,
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  );
          },
        ));
  }
}
