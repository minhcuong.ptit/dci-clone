import 'package:equatable/equatable.dart';

abstract class FilterGroupState extends Equatable {
  @override
  List<Object> get props => [];
}

class FilterGroupInitial extends FilterGroupState {}

class FilterGroupLoading extends FilterGroupState {
  @override
  String toString() => 'FilterGroupLoading';
}

class FilterGroupFailure extends FilterGroupState {
  final String error;

  FilterGroupFailure(this.error);

  @override
  String toString() => 'FilterGroupFailure { error: $error }';
}

class FilterGroupSuccess extends FilterGroupState {
  @override
  String toString() {
    return 'FilterGroupSuccess{}';
  }
}

class FilterSuccess extends FilterGroupState {
  @override
  String toString() {
    return 'FilterSuccess{}';
  }
}

class SelectTimeSuccess extends FilterGroupState {
  @override
  String toString() {
    return 'SelectTimeSuccess{}';
  }
}

class ChooseStudySuccess extends FilterGroupState {
  @override
  String toString() {
    return 'ChooseStudySuccess{}';
  }
}

class ChooseDaySuccess extends FilterGroupState {
  @override
  String toString() {
    return 'ChooseDaySuccess{}';
  }
}

class SearchStudySuccess extends FilterGroupState {
  @override
  String toString() {
    return 'SearchStudySuccess{}';
  }
}
