import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/level_data.dart';
import 'package:imi/src/data/network/response/list_dci_community_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/home_study_group/filter_group/filter_group.dart';
import 'package:imi/src/page/home_study_group/filter_group/filter_group_state.dart';
import 'package:imi/src/utils/enum.dart';

class FilterGroupCubit extends Cubit<FilterGroupState> {
  FilterGroupCubit(this.repository, this.graphqlRepository)
      : super(FilterGroupInitial());
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityDataV2> listCommunitySuggestion = [];
  List<CommunityDataV2> listCommunityLevel = [];
  CommunityInfo? communityInfo;
  List<LevelData> levels = [];
  Map<LevelData, bool> isChooseStudy = {};
  List<bool> isChooseDay =
      List.generate(DayOfWeek.values.length, (index) => false);

  DateTime? startTime;
  DateTime? endTime;
  String? nextToken;
  String? nextTokenSearch;

  //final int? levelId;
  //final int? communityId;

  void getListCommunitiesSuggestion({
    bool isRefresh = false,
    bool isLoadMore = false,
    List<int>? levelIds,
    List<String>? dayOfWeek,
    String? keyWord,
    // int? communityId
  }) async {
    emit((isRefresh) ? FilterGroupInitial() : FilterGroupLoading());
    if (isLoadMore != true) {
      nextToken = null;
      listCommunitySuggestion.clear();
    }
    levelIds ??= [];
    if (levelIds.isEmpty)
      isChooseStudy.forEach((key, value) {
        if (value) levelIds!.add(key.id ?? 0);
      });
    final apiResult = await graphqlRepository.getDCICommunities(
      CommunityType.STUDY_GROUP,
      nextToken: nextToken,
      searchKey: keyWord,
      //communityId: communityId,
      isValid: true,
      levelIds: levelIds,
      dayOfWeek: dayOfWeek,
      startTime: startTime?.millisecondsSinceEpoch,
      endTime: endTime?.millisecondsSinceEpoch,
    );
    apiResult.when(success: (data) async {
      if (data.dciCommunities?.data != null)
        listCommunitySuggestion.addAll(data.dciCommunities?.data ?? []);
      nextToken = data.dciCommunities?.nextToken;
      emit(FilterGroupSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(FilterGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getLevels(int levelId) async {
    emit(FilterGroupLoading());
    ApiResult<List<LevelData>> apiResult = await repository.getLevels();
    apiResult.when(success: (List<LevelData> data) async {
      levels = data;
      data.forEach((element) {
        isChooseStudy[element] = (element.id == levelId);
      });
      emit(FilterSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(FilterGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectStartTime(DateTime time) {
    emit(FilterGroupLoading());
    this.startTime = time;
    emit(SelectTimeSuccess());
  }

  void selectEndTime(DateTime time) {
    emit(FilterGroupLoading());
    this.endTime = time;
    emit(SelectTimeSuccess());
  }

  void selectStudy(bool isSelected, int index) {
    emit(FilterGroupLoading());
    this.isChooseStudy[levels[index]] = isSelected;
    emit(ChooseStudySuccess());
  }

  void selectDay(bool isSelected, int index) {
    emit(FilterGroupLoading());
    this.isChooseDay[index] = isSelected;
    emit(ChooseDaySuccess());
  }
}
