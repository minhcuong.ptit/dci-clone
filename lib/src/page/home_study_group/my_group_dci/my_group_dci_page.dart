import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/page/club_info/club_information/club_information_page.dart';
import 'package:imi/src/page/home_study_group/my_group_dci/my_group_dci.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/study_group_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MyGroupDciPage extends StatefulWidget {
  final List<int>? levelIds;
  final CommunityStatus? status;

  const MyGroupDciPage({Key? key, this.status, this.levelIds})
      : super(key: key);

  @override
  State<MyGroupDciPage> createState() => _MyGroupDciPageState();
}

class _MyGroupDciPageState extends State<MyGroupDciPage> {
  late MyGroupDciCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  final TextEditingController _searchController = TextEditingController();
  bool hideAppBar = false;
  bool isFilter = false;
  bool isCollapse = true;
  List<DayOfWeek> listDay = DayOfWeek.values;
  List<String> listDaySearch = [];
  Timer? _debounce;
  Duration initialTimer = new Duration();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = MyGroupDciCubit(repository, graphqlRepository);
    _cubit.getListGroup(
      status: widget.status,
      levelIds: widget.levelIds??[]
    );
    _cubit.getLevels(widget.levelIds ?? []);
  }

  @override
  void dispose() {
    _searchController.dispose();
    _refreshController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        NavigationUtils.pop(context);
        Utils.hideKeyboard(context);
        return true;
      },
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<MyGroupDciCubit, MyGroupDciState>(
          listener: (BuildContext context, state) {
            if (state is! MyGroupDciLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (BuildContext context, state) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: R.color.primaryColor,
                automaticallyImplyLeading: false,
                title: Stack(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          child: Icon(
                            CupertinoIcons.back,
                            color: R.color.white,
                          ),
                          onTap: () {
                            Utils.hideKeyboard(context);
                            NavigationUtils.pop(context);
                          },
                        ),
                        Text(
                          widget.status == CommunityStatus.COMPLETED
                              ? R.string.groups_done_appbar.tr().toUpperCase()
                              : widget.status == CommunityStatus.ACTIVE
                                  ? R.string.groups_active_appbar
                                      .tr()
                                      .toUpperCase()
                                  : R.string.groups_suggestion
                                      .tr()
                                      .toUpperCase(),
                          style: Theme.of(context)
                              .textTheme
                              .medium500
                              .copyWith(fontSize: 18.sp, color: R.color.white),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              hideAppBar = !hideAppBar;
                            });
                          },
                          child: Icon(
                            CupertinoIcons.search,
                            size: 22.h,
                            color: R.color.white,
                          ),
                        )
                      ],
                    ),
                    Visibility(
                      visible: hideAppBar,
                      child: Container(
                        color: R.color.primaryColor,
                        child: Row(
                          children: [
                            GestureDetector(
                              child: Icon(
                                CupertinoIcons.back,
                                color: R.color.white,
                              ),
                              onTap: () {
                                Utils.hideKeyboard(context);
                                NavigationUtils.pop(context);
                              },
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15.h, vertical: 5.h),
                                margin: EdgeInsets.symmetric(horizontal: 10.h),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25.h),
                                  color: R.color.white,
                                ),
                                child: Row(
                                  children: [
                                    InkWell(
                                        onTap: () {
                                          _cubit.getListGroup(
                                              isRefresh: true,
                                              keyWord:
                                                  _searchController.text.trim(),
                                              status: widget.status);
                                        },
                                        child: Icon(CupertinoIcons.search,
                                            size: 20.h, color: R.color.black)),
                                    SizedBox(width: 5.h),
                                    Expanded(
                                      child: CupertinoSearchTextField(
                                        autofocus: true,
                                        padding: const EdgeInsetsDirectional
                                            .fromSTEB(0, 5, 5, 5),
                                        decoration: BoxDecoration(
                                            color: R.color.white,
                                            borderRadius:
                                                BorderRadius.circular(30.h)),
                                        controller: _searchController,
                                        prefixIcon: const SizedBox(),
                                        placeholder: R.string.search.tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyRegular
                                            .copyWith(
                                              color: R.color.black,
                                            ),
                                        // onChanged: _onSearchChanged,
                                        onSubmitted: (text) {
                                          _cubit.getListGroup(
                                            isRefresh: true,
                                              keyWord: text,
                                              status: widget.status);
                                          Utils.hideKeyboard(context);
                                          //  Utils.hideKeyboard(context);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              body: Stack(
                children: [
                  StackLoadingView(
                    visibleLoading: state is MyGroupDciLoading,
                    child: SmartRefresher(
                      enablePullUp: true,
                      controller: _refreshController,
                      onRefresh: () {
                        _cubit.getListGroup(
                          isRefresh: true,
                          status: widget.status,
                          dayOfWeek: listDaySearch,
                          keyWord: _searchController.text.trim(),
                        );
                      },
                      onLoading: () {
                     _cubit.listStudyGroup.length>=20?   _cubit.getListGroup(
                          isLoadMore: true,
                          status: widget.status,
                          dayOfWeek: listDaySearch,
                          keyWord: _searchController.text.trim(),
                        ):null;
                      },
                      child: ListView(
                        padding: EdgeInsets.symmetric(horizontal: 20.h),
                        children: [
                          SizedBox(height: 5.h),
                          SizedBox(height: 10.h),
                          (_cubit.state is! MyGroupDciLoading) &&
                                  _cubit.listStudyGroup.isEmpty
                              ? Visibility(
                                  visible: state is MyGroupDciSuccess,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.2),
                                    child: Center(
                                      child: Text(
                                        R.string.there_are_no_suggested_groups
                                            .tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(fontSize: 14.sp),
                                      ),
                                    ),
                                  ),
                                )
                              : homeStudyGroupsSuggestionGrid()
                        ],
                      ),
                    ),
                  ),
                  buildFilterCourse(context)
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  void _onSearchChanged(String text) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      _cubit.getListGroup(keyWord: text.trim(), status: widget.status);
    });
  }

  Widget homeStudyGroupsSuggestionGrid() {
    final maxWidthOfCell = (ScreenUtil().screenWidth - 24.w - 16.w) / 2;
    return GridView.builder(
      primary: true,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: _cubit.listStudyGroup.length,
      itemBuilder: (context, index) {
        CommunityDataV2 data = _cubit.listStudyGroup[index];
        // bool isSelected = _cubit.listSelectedCommunity
        //         .indexWhere((element) => element.id == data.id) >=
        //     0;
        bool isSelected = false;
        return StudyGroupWidget(
          width: maxWidthOfCell,
          data: data,
          onTap: () {
            NavigationUtils.rootNavigatePage(
                context,
                ViewClubPage(
                  communityId: data.id ?? 0,
                ));
            //gotoClubDetail(data.id);
            //_cubit.selectCommunity(data);
          },
          isSelected: isSelected,
          isShowStar: true,
        );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 8.w,
          mainAxisSpacing: 16.h,
          crossAxisCount: 2,
          childAspectRatio: 8 / 10),
    );
  }

  Widget buildFilterCourse(BuildContext context) {
    return Visibility(
      visible: isFilter,
      child: Positioned(
        top: 40.h,
        left: 46.h,
        right: 20.h,
        bottom: isCollapse ? 100.h : MediaQuery.of(context).size.height * 0.5,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8.h, vertical: 10.h),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.h),
            color: R.color.white,
            border: Border.all(width: 1, color: R.color.grey100),
            boxShadow: [
              BoxShadow(
                color: R.color.grey100,
                spreadRadius: 1,
                blurRadius: 5,
                offset: const Offset(0, -3),
              ),
            ],
          ),
          child: Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  R.string.living_time.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, height: 16.h / 12.sp),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(height: 8.h),
              Container(
                padding: EdgeInsets.symmetric(vertical: 8.h),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: R.color.grey100),
                  borderRadius: BorderRadius.circular(5.h),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: List.generate(
                          listDay.length,
                          (index) => GestureDetector(
                                onTap: () {
                                  _cubit.selectDay(
                                      !_cubit.isChooseDay[index], index);
                                  if (_cubit.isChooseDay[index] == false) {
                                    listDaySearch
                                        .remove(DayOfWeek.values[index].name);
                                  } else {
                                    listDaySearch
                                        .add(DayOfWeek.values[index].name);
                                  }
                                },
                                child: Container(
                                  width: 30.w,
                                  height: 30.h,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: _cubit.isChooseDay[index]
                                          ? R.color.primaryColor
                                          : R.color.white,
                                      borderRadius: BorderRadius.circular(3.h),
                                      border:
                                          Border.all(color: R.color.grey100)),
                                  child: Text(
                                    listDay[index].title,
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                          fontSize: 14.sp,
                                          height: 18.h / 14.sp,
                                          color: _cubit.isChooseDay[index]
                                              ? R.color.white
                                              : R.color.black,
                                        ),
                                  ),
                                ),
                              )),
                    ),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: R.color.grey100,
                      margin: EdgeInsets.all(10.h),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              R.string.start_time_frame.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, height: 16.h / 12.sp),
                            ),
                          ),
                          SizedBox(width: 7.h),
                          GestureDetector(
                            onTap: () async {
                              TimeOfDay? pickedTime = await showTimePicker(
                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              if (pickedTime != null) {
                                DateTime? parsedTime = DateFormat.Hm().parse(
                                    pickedTime.format(context).toString());
                                _cubit.selectStartTime(parsedTime);
                                Utils.hideKeyboard(context);
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.h, horizontal: 10.h),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: R.color.grey100, width: 1),
                                borderRadius: BorderRadius.circular(5.h),
                              ),
                              child: Text(
                                _cubit.startTime == null
                                    ? "00:00"
                                    : DateUtil.parseDateToString(
                                        _cubit.startTime, Const.TIME_FORMAT),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp, height: 18.h / 12.sp),
                              ),
                            ),
                          ),
                          SizedBox(width: 7.h),
                          Image.asset(R.drawable.ic_arrow_right, width: 22.h),
                          SizedBox(width: 7.h),
                          GestureDetector(
                            onTap: () async {
                              TimeOfDay? pickedTime = await showTimePicker(
                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              if (pickedTime != null) {
                                DateTime? parsedTime = DateFormat.Hm().parse(
                                    pickedTime.format(context).toString());
                                _cubit.selectEndTime(parsedTime);
                                Utils.hideKeyboard(context);
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.h, horizontal: 10.h),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: R.color.grey100, width: 1),
                                borderRadius: BorderRadius.circular(5.h),
                              ),
                              child: Text(
                                _cubit.endTime == null
                                    ? "00:00"
                                    : DateUtil.parseDateToString(
                                        _cubit.endTime, Const.TIME_FORMAT),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp, height: 18.h / 12.sp),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 8.h),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  R.string.study_program.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, height: 16.h / 12.sp),
                ),
              ),
              SizedBox(height: 8.h),
              Visibility(
                visible: isCollapse,
                child: Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: _cubit.levels.length,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          onTap: () {
                            _cubit.selectStudy(
                                !(_cubit.isChooseStudy[_cubit.levels[index]] ??
                                    false),
                                index);
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 5.h),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (_cubit.isChooseStudy[_cubit.levels[index]] ??
                                        false)
                                    ? Icon(
                                        Icons.check_box,
                                        size: 20.h,
                                        color: R.color.primaryColor,
                                      )
                                    : Icon(
                                        Icons.check_box_outline_blank,
                                        size: 20.h,
                                        color: R.color.grey,
                                      ),
                                SizedBox(width: 8.h),
                                Expanded(
                                  child: Text(
                                    _cubit.levels[index].name ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            height: 18.h / 12.sp,
                                            color: (_cubit.isChooseStudy[
                                                        _cubit.levels[index]] ??
                                                    false)
                                                ? R.color.primaryColor
                                                : R.color.black),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    isCollapse = !isCollapse;
                  });
                },
                child: Text(
                  isCollapse ? R.string.less.tr() : R.string.view_all.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, color: R.color.primaryColor),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                color: R.color.grey100,
                height: 1,
                margin: EdgeInsets.symmetric(vertical: 10.h),
              ),
              Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _cubit.isChooseStudy.clear();
                          for (int i = 0; i < listDay.length; i++) {
                            _cubit.selectDay(!_cubit.isChooseDay[i], i);
                            _cubit.isChooseDay[i] = false;
                            listDaySearch.remove(DayOfWeek.values[i].name);
                          }
                        });
                      },
                      child: Text(
                        R.string.clear_filter.tr(),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 12.sp,
                            height: 18.h / 12.sp,
                            color: R.color.primaryColor),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      isFilter = false;
                      _cubit.getListGroup(
                          dayOfWeek: listDaySearch, status: widget.status);
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.h, vertical: 5.h),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.h),
                          color: R.color.primaryColor),
                      child: Text(
                        R.string.search.tr(),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 12.sp,
                            height: 16.h / 12.sp,
                            color: R.color.white),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
