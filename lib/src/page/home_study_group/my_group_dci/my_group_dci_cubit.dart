import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/level_data.dart';
import 'package:imi/src/data/network/response/list_dci_community_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/home_study_group/filter_group/filter_group.dart';
import 'package:imi/src/page/home_study_group/my_group_dci/my_group_dci.dart';
import 'package:imi/src/page/home_study_group/my_group_dci/my_group_dci_state.dart';
import 'package:imi/src/utils/enum.dart';

import '../../../utils/const.dart';

class MyGroupDciCubit extends Cubit<MyGroupDciState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  MyGroupDciCubit(this.repository, this.graphqlRepository)
      : super(MyGroupDciInitial());
  List<CommunityDataV2> listStudyGroup = [];
  Map<LevelData, bool> isChooseStudy = {};
  List<LevelData> levels = [];
  List<bool> isChooseDay =
      List.generate(DayOfWeek.values.length, (index) => false);
  String? nextToken;
  DateTime? startTime;
  DateTime? endTime;
  List<String> listDayOfWeek = [
    "MONDAY",
    "TUESDAY",
    "WEDNESDAY",
    "THURSDAY",
    "FRIDAY",
    "SATURDAY",
    "SUNDAY"
  ];

  void getListGroup({
    bool isRefresh = false,
    bool isLoadMore = false,
    List<int>? levelIds,
    CommunityStatus? status,
    List<String>? dayOfWeek,
    String? keyWord,
    // int? communityId
  }) async {
    emit(isRefresh ? MyGroupDciLoading() : MyGroupDciInitial());
    if (isRefresh) {
      nextToken = null;
      listStudyGroup.clear();
    }
    levelIds ??= [];
    if (levelIds.isEmpty)
      isChooseStudy.forEach((key, value) {
        if (value) levelIds!.add(key.id ?? 0);
      });
    final apiResult = await graphqlRepository.getDCICommunities(
      CommunityType.STUDY_GROUP,
      nextToken: nextToken,
      limit: Const.NETWORK_DEFAULT_LIMIT,
      status: status,
      searchKey: keyWord,
      //communityId: communityId,
      isValid: true,
      levelIds: levelIds,
      dayOfWeek: dayOfWeek,
      startTime: startTime?.millisecondsSinceEpoch,
      endTime: endTime?.millisecondsSinceEpoch,
    );
    apiResult.when(success: (ListDCICommunityResponse data) async {
      if (data.dciCommunities?.data != null) {
        listStudyGroup.addAll(data.dciCommunities?.data ?? []);
        nextToken = data.dciCommunities?.nextToken;
        emit(MyGroupDciSuccess());
      } else {
        emit(FilterMyGroupDciEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(MyGroupDciFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getLevels(List<int?> levelId) async {
    emit(MyGroupDciLoading());
    ApiResult<List<LevelData>> apiResult = await repository.getLevels();
    apiResult.when(success: (List<LevelData> data) async {
      levels = data;
      data.forEach((element) {
        isChooseStudy[element] = levelId.contains(element.id);
      });
      emit(FilterMyGroupDciSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(MyGroupDciFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectStudy(bool isSelected, int index) {
    emit(MyGroupDciLoading());
    this.isChooseStudy[levels[index]] = isSelected;
    emit(MyGroupDciInitial());
  }

  void selectStartTime(DateTime time) {
    emit(MyGroupDciLoading());
    this.startTime = time;
    emit(MyGroupDciInitial());
  }

  void selectEndTime(DateTime time) {
    emit(MyGroupDciLoading());
    this.endTime = time;
    emit(MyGroupDciInitial());
  }

  void selectDay(bool isSelected, int index) {
    emit(MyGroupDciLoading());
    this.isChooseDay[index] = isSelected;
    emit(MyGroupDciInitial());
  }

  List<CommunityDataV2> getData(String text) {
    if (text.isNotEmpty) listStudyGroup;
    return listStudyGroup;
  }
}
