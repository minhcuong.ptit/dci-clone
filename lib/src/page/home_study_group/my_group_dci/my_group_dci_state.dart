import 'package:equatable/equatable.dart';

abstract class MyGroupDciState extends Equatable {
  @override
  List<Object> get props => [];
}

class MyGroupDciInitial extends MyGroupDciState {}

class MyGroupDciLoading extends MyGroupDciState {
  @override
  String toString() => 'MyGroupDciLoading';
}

class MyGroupDciFailure extends MyGroupDciState {
  final String error;

  MyGroupDciFailure(this.error);

  @override
  String toString() => 'MyGroupDciFailure { error: $error }';
}

class MyGroupDciSuccess extends MyGroupDciState {
  @override
  String toString() {
    return 'MyGroupDciSuccess{}';
  }
}

class FilterMyGroupDciSuccess extends MyGroupDciState {
  @override
  String toString() {
    return 'FilterMyGroupDciSuccess{}';
  }
}

class FilterMyGroupDciEmpty extends MyGroupDciState {
  @override
  String toString() {
    return 'FilterMyGroupDciEmpty{}';
  }
}
