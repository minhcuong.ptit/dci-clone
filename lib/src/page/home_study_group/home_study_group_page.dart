import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/course_details/course_details_page.dart';
import 'package:imi/src/page/home_study_group/filter_group/filter_group_page.dart';
import 'package:imi/src/page/home_study_group/my_group_dci/my_group_dci_page.dart';
import 'package:imi/src/page/home_study_group/widget/group_widget_page.dart';
import 'package:imi/src/page/home_study_group/widget/noLevel_widget.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../utils/const.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/study_group_widget.dart';
import '../club_info/club_information/club_information.dart';
import '../study_group/study_group.dart';
import 'home_study_group.dart';

class HomeStudyGroupPage extends StatefulWidget {
  final List<FetchCoursesData>? listCourse;
  final VoidCallback? onRefresh;

  const HomeStudyGroupPage({Key? key, this.onRefresh, this.listCourse})
      : super(key: key);

  @override
  _HomeStudyGroupPageState createState() => _HomeStudyGroupPageState();
}

class _HomeStudyGroupPageState extends State<HomeStudyGroupPage> {
  late HomeStudyGroupCubit _cubit;
  final RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HomeStudyGroupCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.getActiveGroup();
    // _cubit.getCompleteGroup();
    // _cubit.getListCommunitiesSuggestion(levelIds: appPreferences
    //     .getListString(Const.DCI_LEVEL_ID)
    //     .map((e) => int.parse(e))
    //     .toList());
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<HomeStudyGroupCubit, HomeStudyGroupState>(
        listener: (context, state) {
          if (state is! HomeStudyGroupLoading) {
            _refreshController.refreshCompleted();
          }
          if (state is! SubmitHomeStudyGroupSuccess) {
            if (widget.onRefresh != null) {
              widget.onRefresh!();
            }
          }
          if (state is HomeStudyGroupFailure) {
            Utils.showErrorSnackBar(context, state.error);
          }
        },
        builder: (context, state) {
          return _buildPage(context, state);
        },
      ),
    );
  }

  Widget _buildPage(BuildContext context, HomeStudyGroupState state) {
    return Scaffold(
        backgroundColor: R.color.white,
        appBar: appBar(context, R.string.study_groups_dci.tr().toUpperCase(),
            backgroundColor: R.color.primaryColor,
            iconColor: R.color.white,
            elevation: 0,
            centerTitle: true,
            textStyle: Theme.of(context)
                .textTheme
                .bodyBold
                .apply(color: R.color.white)
                .copyWith(fontSize: 16.sp, fontWeight: FontWeight.w700)),
        body: StackLoadingView(
          visibleLoading: state is HomeStudyGroupLoading,
          child: SmartRefresher(
              controller: _refreshController,
              onRefresh: () {
                _cubit.getLevelId();
                //_cubit.getListCommunitiesSuggestion(isRefresh: true);
              },
              child: state is! HomeStudyGroupLoading
                  ? (!appPreferences.hasDCILevel)
                      ? Visibility(
                          visible: state is! HomeStudyGroupLoading,
                          child: NoLevelWidget(data: widget.listCourse ?? []))
                      : _cubit.isNoneFollowing
                          ? _contentForNoneFollowing(state)
                          : _contentForContainFollowing(state)
                  : const SizedBox.shrink()),
        ));
  }

  Widget _contentForNoneFollowing(HomeStudyGroupState state) {
    return ListView(
      padding: EdgeInsets.fromLTRB(24.w, 16.h, 24.w, 0.h),
      shrinkWrap: true,
      children: [
        headerNoneFollow(),
        SizedBox(
          height: 16.h,
        ),
        //HomeStudyGroupsSuggestionGrid(),
        // if (_cubit.communityDataV2?.communityInfo?.communityLevel != null)
        //   ..._cubit.communityDataV2!.communityInfo!.communityLevel!
        //       .map((e) => GroupWidget(
        //             onViewAll: () {
        //               NavigationUtils.rootNavigatePage(
        //                   context,
        //                   FilterGroupPage(
        //                     id: _cubit.communityDataV2?.id,
        //                     levelIds: e?.level,
        //                   ));
        //             },
        //             communityLevel: e,
        //           ))
        //       .toList()
      ],
    );
  }

  Widget _contentForContainFollowing(HomeStudyGroupState state) {
    final maxWidthOfCell = (ScreenUtil().screenWidth - 24.w - 16.w) / 2;
    return ListView(
      shrinkWrap: true,
      children: [
        category(
            title: R.string.groups_active.tr(),
            typeOfStudyGroup: _cubit.listCommunityActive,
            communityStatus: CommunityStatus.ACTIVE),
        category(
            title: R.string.groups_done.tr(),
            typeOfStudyGroup: _cubit.listCommunityCompleted,
            communityStatus: CommunityStatus.COMPLETED),
        // groupsCompleted(),
        SizedBox(
          height: 16.h,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 24, vertical: 10.h),
          height: 1,
          color: R.color.grey100,
        ),
        state is HomeStudyGroupSuccess
            ? Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Text(
                  R.string.suggest_other_study_groups.tr(),
                  style: Theme.of(context).textTheme.medium500.copyWith(
                      fontSize: 12.sp,
                      height: 24.h / 12.sp,
                      color: R.color.primaryColor),
                ),
              )
            : Center(
                child: SizedBox(
                  width: 20,
                  height: 20,
                  child: CupertinoActivityIndicator(
                    animating: true,
                    color: R.color.primaryColor,
                  ),
                ),
              ),
        SizedBox(
          height: 8.h,
        ),
        !_cubit.isHasSuggestion && state is HomeStudyGroupSuccess
            ? Container(
                margin: EdgeInsets.symmetric(horizontal: 40.w),
                child: Text(
                  R.string.no_group_match_dci_program.tr(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodySmallText.copyWith(
                      fontSize: 12.sp,
                      height: 24.h / 12.sp,
                      color: R.color.shadesGray),
                ),
              )
            : const SizedBox.shrink(),
        if (_cubit.isHasSuggestion)
          ..._cubit.listCommunitySuggestion.entries
              .map((e) => category(
                  title: e.value.first.communityInfo?.communityLevel?.first
                          ?.levelName ??
                      '',
                  typeOfStudyGroup: e.value,
                  levelIds: e.value.first.communityInfo?.communityLevel
                          ?.map((e) => e?.level ?? 0)
                          .toList() ??
                      []))
              .toList(),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20.h, vertical: 10.h),
          child: ButtonWidget(
              textSize: 14.sp,
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 14.h),
              title: R.string.view_all_suggested_groups.tr().toUpperCase(),
              onPressed: () {
                NavigationUtils.rootNavigatePage(
                    context, const StudyGroupPage());
              }),
        )
      ],
    );
  }

  Widget category(
      {required String title,
      required List<CommunityDataV2> typeOfStudyGroup,
      CommunityStatus? communityStatus,
      List<int>? levelIds}) {
    final maxWidthOfCell = (ScreenUtil().screenWidth - 24.w - 16.w) / 2;
    if (_cubit.listCommunityActive.isEmpty) return const SizedBox.shrink();
    return typeOfStudyGroup.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: SizedBox(
              height: 250.h,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          title,
                          style: Theme.of(context).textTheme.bold700.copyWith(
                                fontSize: 13.sp,
                                height: 24.h / 13.sp,
                              ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          NavigationUtils.rootNavigatePage(
                            context,
                            MyGroupDciPage(
                                status: communityStatus, levelIds: levelIds),
                          );
                        },
                        child: Text(
                          R.string.view_all.tr(),
                          style:
                              Theme.of(context).textTheme.regular400.copyWith(
                                    fontSize: 11.sp,
                                    height: 24.h / 11.sp,
                                    color: R.color.primaryColor,
                                  ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 8.h),
                  Expanded(
                    child: ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      physics: const BouncingScrollPhysics(),
                      itemCount: typeOfStudyGroup.length,
                      itemBuilder: (context, index) {
                        // bool isSelected = _cubit.listSelectedCommunity
                        //         .indexWhere((element) => element.id == data.id) >=
                        //     0;
                        bool isSelected = false;
                        return Container(
                          margin: EdgeInsets.only(right: 10.h),
                          child: StudyGroupWidget(
                            width: maxWidthOfCell,
                            data: typeOfStudyGroup[index],
                            onTap: () {
                              NavigationUtils.navigatePage(
                                  context,
                                  ViewClubPage(
                                    communityId:
                                        typeOfStudyGroup[index].id ?? 0,
                                  ));
                              // gotoClubDetail(data.id);
                              // _cubit.selectCommunity(data);
                            },
                            isSelected: isSelected,
                            isShowStar: true,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
        : const SizedBox.shrink();
  }

  Widget headerNoneFollow() {
    return Container(
      height: 230.h,
      color: R.color.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            R.drawable.ic_person_none,
            height: 80.h,
            width: 80.h,
          ),
          SizedBox(
            height: 16.h,
          ),
          Text(
            R.string.study_groups_you_not_follow.tr(),
            style: Theme.of(context)
                .textTheme
                .labelSmallText
                .copyWith(color: R.color.lightShadesGray, fontSize: 13.sp),
          ),
          Text(
            R.string.here_are_the_suggested_groups_for_you.tr(),
            style: Theme.of(context)
                .textTheme
                .labelSmallText
                .copyWith(color: R.color.lightShadesGray, fontSize: 13.sp),
          ),
          SizedBox(height: 30.h),
          ButtonWidget(
              textSize: 14.sp,
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 14.h),
              title: R.string.view_all_suggested_groups.tr().toUpperCase(),
              onPressed: () {
                NavigationUtils.rootNavigatePage(
                    context, const StudyGroupPage());
              })
        ],
      ),
    );
  }

  void gotoClubDetail(communityId) {
    NavigationUtils.navigatePage(
        context,
        ViewClubPage(
          communityId: communityId,
        )).then((value) {
      // _cubit.getListFollow(
      //     isRefresh: true, type: widget.type);
    });
  }
}
