import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/chat/chat/pending_file_widget.dart';
import 'package:imi/src/page/chat/chat/pending_images_widget.dart';
import 'package:imi/src/page/chat/chat/pending_text_widget.dart';
import 'package:imi/src/page/chat/chat/show_image.dart';
import 'package:imi/src/page/club_info/club_information/club_information.dart';
import 'package:imi/src/page/pin_code/change_password/change_password_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/rich_input.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:intl/intl.dart';
import 'package:pubnub/pubnub.dart';
import 'package:styled_text/styled_text.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../data/network/repository/app_graphql_repository.dart';
import '../../../data/network/request/change_status_request.dart';
import '../../../widgets/avatar_widget.dart';
import '../../../widgets/text_input_chat.dart';
import '../../profile/view_profile/view_profile.dart';

final DateFormat _dateFormat = DateFormat("HH:mm, dd/MM/yyyy");
final DateFormat _timeFormat = DateFormat("HH:mm");

class ChatPage extends StatefulWidget {
  final ChatType chatType;
  final String? userUID;
  final String? channelId;
  final String? channelName;
  final int? communityId;
  final int? timeToken;
  final bool? checkPageHomeChat;

  const ChatPage({
    Key? key,
    required this.chatType,
    this.userUID,
    this.channelId,
    this.channelName,
    this.communityId,
    this.timeToken,
    this.checkPageHomeChat,
  }) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  late final ScrollController _scrollController;
  late final RichInputController _textEditingController;
  final GlobalKey<AnimatedListState> _key = GlobalKey();
  late final ChatCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = ChatCubit(widget.chatType, AppGraphqlRepository());
    if (widget.chatType == ChatType.private) {
      List<String> uuids = [];
      uuids.add(widget.userUID ?? '');
      _cubit.initChat(
          userUID: widget.userUID,
          channelId: widget.channelId,
          context: context);
      _cubit.getProfileUser(uuids: uuids);
    }
    if (widget.chatType == ChatType.group) {
      _cubit.initChat(
          userUID: widget.userUID,
          channelId: widget.channelId,
          context: context);
      _cubit.getListUserChatGroup(widget.communityId!);
    }
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.offset ==
            _scrollController.position.maxScrollExtent) {
          if (_cubit.canLoadMore)
            _cubit.loadMore(_cubit.channelIdSupport != ''
                ? _cubit.channelIdSupport
                : widget.channelId ?? '');
        }
      });
    _textEditingController = RichInputController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _textEditingController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
          context,
          widget.channelName ??
              R.string.dci_support_assistant.tr().toUpperCase(),
          titleColor: R.color.white,
          iconColor: R.color.white,
          centerTitle: true,
          backgroundColor: R.color.primaryColor, directToProfile: () {
        widget.chatType == ChatType.group
            ? NavigationUtils.navigatePage(
                context, ViewClubPage(communityId: widget.communityId ?? 0))
            : widget.chatType == ChatType.support
                ? null
                : NavigationUtils.navigatePage(
                    context, ViewProfilePage(communityId: widget.communityId));
      }, onPop: () {
        if (widget.checkPageHomeChat == true) {
          _cubit.checkRead = false;
          NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
        } else {
          NavigationUtils.pop(context);
        }
      }),
      backgroundColor: R.color.chatBackground,
      body: WillPopScope(
        onWillPop: () async {
          NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
          return true;
        },
        child: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<ChatCubit, ChatState>(
            listener: (context, state) {
              if (state is ChatNewMessageState) {
                _key.currentState?.insertItem(0,
                    duration: const Duration(milliseconds: 100));
              } else if (state is ChatLoadMoreMessageState) {
                for (int i = state.oldLength; i < _cubit.messages.length; i++) {
                  _key.currentState?.insertItem(i,
                      duration: const Duration(milliseconds: 100));
                }
              } else if (state is ChatFailureState) {
                Utils.showSnackBar(context, state.error);
              } else if (state is ChatRemovePendingMessageState) {
                _key.currentState?.removeItem(
                    state.index, (_, __) => const SizedBox.shrink(),
                    duration: const Duration(milliseconds: 100));
              }
            },
            buildWhen: (previous, current) {
              return current is ChatSuccessState || current is ChatLoadingState;
            },
            builder: (context, state) {
              return GestureDetector(
                onTap: () {
                  Utils.hideKeyboard(context);
                  _cubit.hideDropDown();
                },
                child: StackLoadingView(
                  visibleLoading: state is ChatLoadingState,
                  child: state is! ChatLoadingState
                      ? Column(
                          children: [
                            Expanded(child: _buildChatDetails(state)),
                            SizedBox(
                              height: 4.h,
                            ),
                            _buildInput(_cubit),
                            SizedBox(
                              height: MediaQuery.of(context).padding.bottom,
                            )
                          ],
                        )
                      : SizedBox(),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildChatDetails(state) {
    return BlocBuilder<ChatCubit, ChatState>(
      buildWhen: (previous, current) => current is ChatSuccessState,
      builder: (context, state) {
        return _cubit.messages.isNotEmpty
            ? AnimatedList(
                key: _key,
                controller: _scrollController,
                itemBuilder: (context, index, animation) {
                  if (index < _cubit.pendingMessages.length) {
                    final temp = _cubit.pendingMessages[index];
                    if (temp.type == MessageContentType.TEXT) {
                      return PendingTextWidget(
                          content: temp.content.toString(),
                          sendAt: temp.sendAt);
                    } else if (temp.type == MessageContentType.FILE) {
                      return PendingFileChat(file: temp.content as XFile);
                    } else if (temp.type == MessageContentType.IMAGE) {
                      return PendingImageWidget(
                          path: (temp.content as XFile).path);
                    }
                    return const SizedBox();
                  }
                  index -= _cubit.pendingMessages.length;
                  if (index == (_cubit.messages.length)) {
                    return _scrollController.offset > 40
                        ? !_cubit.canLoadMore
                            ? const SizedBox()
                            : Center(
                                child: SizedBox(
                                  child: CircularProgressIndicator(
                                      color: R.color.primaryColor),
                                  width: 40,
                                  height: 40,
                                ),
                              )
                        : const SizedBox();
                  }
                  bool isSender = _cubit.messages[index].content['sendBy'] ==
                      appPreferences.getString(Const.ID);
                  bool showAvatar = (index == _cubit.messages.length - 1) ||
                      _cubit.messages[index].content['sendBy'] !=
                          _cubit.messages[index + 1].content['sendBy'] ||
                      _cubit.messages[index + 1].content['type'] ==
                          MessageContentType.ADMIN_JOINED.name;
                  showAvatar &= !isSender;

                  DateTime? d, d1, d2 = null;
                  d1 =
                      _cubit.messages[index].publishedAt.toDateTime().toLocal();
                  if (index == _cubit.messages.length - 1)
                    d = _cubit.messages[index].publishedAt
                        .toDateTime()
                        .toLocal();
                  else {
                    d2 = _cubit.messages[index + 1].publishedAt
                        .toDateTime()
                        .toLocal();
                    if (!DateUtils.isSameDay(d1, d2)) {
                      d = d1;
                    }
                  }

                  return _ChatItem(
                      chatType: widget.chatType,
                      communityId: widget.communityId ?? 0,
                      index: index,
                      channelId: widget.channelId ?? '',
                      isSender: isSender,
                      showAvatar: showAvatar,
                      data: _cubit.messages[index],
                      date: d,
                      publishAt: d1,
                      showName:
                          showAvatar && ChatType.private != widget.chatType);
                },
                reverse: true,
                initialItemCount:
                    (_cubit.messages.length + _cubit.pendingMessages.length) +
                        1,
              )
            : _cubit.isNotConversationYet
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          R.drawable.not_coversation_yet,
                          width: context.width / 2,
                        ),
                        SizedBox(
                          height: 25.h,
                        ),
                        Text(
                          R.string.send_a_message_to_start.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmallText
                              .copyWith(
                                color: R.color.lighterGray,
                              ),
                        ),
                        Text(
                          R.string.conversation.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmallText
                              .copyWith(
                                color: R.color.lighterGray,
                              ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox();
      },
    );
  }

  Widget _buildInput(ChatCubit cubit) {
    return ChatInputField(
      chatInputController: _textEditingController,
      sendText: (String text) => _cubit.sendMessage(text),
      sendFile: (XFile file) {
        _cubit.sendFile(file);
      },
      sendImage: (XFile file) {
        _cubit.sendImage(file);
      },
    );
  }
}

class _ChatItem extends StatefulWidget {
  final int communityId;
  final ChatType chatType;
  final int index;
  final String channelId;
  final bool isSender;
  final bool showAvatar;
  final bool showName;
  final DateTime? date;
  final DateTime? publishAt;
  final BaseMessage data;

  const _ChatItem({
    Key? key,
    required this.isSender,
    required this.showAvatar,
    this.date,
    required this.data,
    required this.showName,
    this.publishAt,
    required this.channelId,
    required this.index,
    required this.communityId,
    required this.chatType,
  }) : super(key: key);

  @override
  State<_ChatItem> createState() => _ChatItemState();
}

class _ChatItemState extends State<_ChatItem> {
  AppRepository appRepository = AppRepository();
  String? _selectedReaction;
  final FocusNode focusNode = FocusNode();
  late final ChatCubit _cubit;
  final layerLink = LayerLink();
  int _progress = 0;
  bool isDownload = false;
  bool isDownloadFile = false;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ChatCubit>(context);
    ImageDownloader.callback(onProgressUpdate: (String? imageId, int progress) {
      setState(() {
        _progress = progress;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatCubit, ChatState>(
      builder: (context, state) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 4.h),
        child: _buildContent(context, state),
      ),
    );
  }

  Widget _buildContent(context, ChatState state) {
    if (widget.data.content['type'] == MessageContentType.ADMIN_DONE.name) {
      return SizedBox.shrink();
    }
    if (widget.data.content['type'] == MessageContentType.ADMIN_JOINED.name) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<String>(
          future: _cubit.getNameByUUID(widget.data.content['sendBy']),
          builder: (context, snapshot) => Center(
            child: StyledText(
              text:
                  '<bold>${snapshot.data ?? ''}</bold> ${R.string.assistant_join_the_chat.tr()}',
              tags: {
                'bold': StyledTextTag(
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: R.color.darkGrey))
              },
            ),
          ),
        ),
      );
    }
    List<Widget> children = [
      const Spacer(),
      // content
      _getContent(state),
      SizedBox(
        width: 4.w,
      ),
      (widget.showAvatar && !widget.isSender)
          ? FutureBuilder<String>(
              future: _cubit.getAvatarByUUID(widget.data.content['sendBy']),
              builder: (context, snapshot) => GestureDetector(
                onTap: () {
                  if (widget.chatType != ChatType.support) {
                    if (widget.chatType == ChatType.group) {
                      var index = _cubit.profileUser.indexWhere((element) =>
                          element.userUuid == widget.data.content['sendBy']);
                      NavigationUtils.navigatePage(
                          context,
                          ViewProfilePage(
                              communityId: _cubit.profileUser[index].id));
                      return;
                    }
                    NavigationUtils.navigatePage(context,
                        ViewProfilePage(communityId: widget.communityId));
                  }
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: widget.data.content['sendBy'] != null &&
                          snapshot.data != null
                      ? AvatarWidget(
                          avatar: snapshot.data!,
                          size: 24.h,
                        )
                      : CircleAvatar(
                          radius: 12.h,
                          backgroundImage: AssetImage(R.drawable.ic_DCI),
                        ),
                ),
              ),
            )
          : SizedBox(
              height: widget.isSender ? 0.h : 24.h,
              width: widget.isSender ? 0.h : 24.h,
            )
    ];
    return Column(
      children: [
        Visibility(
          visible: widget.date != null,
          child: Container(
            padding: EdgeInsets.only(bottom: 2, right: 16, left: 16),
            margin: EdgeInsets.only(bottom: 8.h, top: 16.h),
            decoration: BoxDecoration(
                color: R.color.secondaryDarkGrey,
                borderRadius: BorderRadius.circular(100)),
            child: Column(
              children: [
                Text(
                  _dateFormat.format(widget.date ?? DateTime(2000)),
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      color: R.color.white, fontSize: 10, height: 16 / 10),
                ),
              ],
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widget.isSender ? children : children.reversed.toList(),
        ),
      ],
    );
  }

  Widget _getContent(ChatState state) {
    if ((widget.data.content['images'] ?? []).isNotEmpty)
      return _buildImageContent(context);
    if (widget.data.content['content'] != null ||
        widget.data.content['sendBy'] == null)
      return _buildTextContent(context, state);
    if ((widget.data.content['files'] ?? []).isNotEmpty)
      return _buildFileContent(context);
    return const SizedBox();
  }

  Widget _buildImageContent(context) {
    final imageRow = [
      GestureDetector(
        onTap: () async {
          NavigationUtils.navigatePage(
              context, ShowImage(widget.data.content['images'].first));
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(9.h),
          child: SizedBox(
            height: 108,
            width: 144,
            child: CachedNetworkImage(
              imageUrl: widget.data.content['images'].first,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      SizedBox(
        width: 8.w,
      ),
      GestureDetector(
        onTap: () async {
          setState(() {
            isDownload = true;
          });
          var isDowloadImageSuccess = await appRepository
              .downLoadImage(widget.data.content['images'].first);
          if (isDowloadImageSuccess) {
            Fluttertoast.showToast(msg: R.string.save_file_image_success.tr());
          }
          setState(() {
            isDownload = false;
          });
        },
        child: isDownload
            ? SizedBox(
                width: 14, height: 14, child: CircularProgressIndicator())
            : Container(
                height: 20.h,
                width: 20.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: R.color.white,
                    image: DecorationImage(
                      image: AssetImage(R.drawable.ic_download),
                      fit: BoxFit.cover,
                    ),
                    boxShadow: [BoxShadow(blurRadius: 0.2)]),
              ),
      ),
    ];
    return CompositedTransformTarget(
      link: layerLink,
      child: Stack(
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Visibility(
              visible: widget.showName && !widget.isSender,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 12.w),
                decoration: BoxDecoration(
                    color: R.color.white,
                    borderRadius: BorderRadius.circular(15.h),
                    boxShadow: widget.isSender
                        ? []
                        : [
                            BoxShadow(
                                offset: const Offset(0, 2),
                                blurRadius: 16,
                                color: Color(0xFFD0D0D0).withOpacity(0.18))
                          ]),
                child: FutureBuilder<String>(
                  future: _cubit.getNameByUUID(widget.data.content['sendBy']),
                  builder: (context, snapshot) => Text(
                    snapshot.data ?? "",
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12,
                        height: 16 / 12,
                        color: R.color.tertiaryOrange),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 4.h,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children:
                  !widget.isSender ? imageRow : imageRow.reversed.toList(),
            ),
            SizedBox(
              height: 4.h,
            ),
            Container(
              margin: EdgeInsets.only(
                  left: widget.isSender ? 24.w : 0,
                  right: !widget.isSender ? 24.w : 0),
              padding: EdgeInsets.symmetric(horizontal: 3.w),
              decoration: BoxDecoration(
                  color: R.color.secondaryDarkGrey,
                  borderRadius: BorderRadius.circular(100)),
              child: Text(
                  _timeFormat
                      .format(widget.data.publishedAt.toDateTime().toLocal()),
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 10, height: 12 / 10, color: R.color.white)),
            )
          ]),
          // Positioned(
          //     bottom: 5,
          //     right: widget.isSender ? 10 : 40,
          //     child: _buildReaction(context)),
          Positioned(
              bottom: 5,
              right: widget.isSender ? 10 : 40,
              child: _selectedReaction != null
                  ? Image.asset(
                      _selectedReaction ?? '',
                      width: 20.w,
                    )
                  : SizedBox.shrink()),
        ],
      ),
    );
  }

  Widget _buildTextContent(context, ChatState state) {
    bool isDeleted = false;
    if (state is DeleteMessage) {
      if (_cubit.timeTokenDeletedMessage
          .contains(widget.data.publishedAt.toString())) {
        isDeleted = true;
      }
    }
    return widget.data.content['content'] == 'change status'
        ? SizedBox.shrink()
        : Stack(
            children: [
    FutureBuilder<bool>(
    future: _cubit.checkDeletedMessage(
    widget.data.content['sendBy'],
        widget.data.publishedAt),
    builder: (context, snapshotDelete)=>
                Container(
                  margin: EdgeInsets.only(
                    left: widget.isSender ? 50 : 0,
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 2.h),
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.6),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      border: (snapshotDelete.data==true||isDeleted)?Border.all(width: 1,color: R.color.grey):null,
                      color: widget.isSender ? R.color.skyBlue :(snapshotDelete.data==true||isDeleted)?Colors.transparent: R.color.white,
                      boxShadow: [
                        if (!widget.isSender)
                          BoxShadow(
                              offset: const Offset(0, 4),
                              blurRadius: 16,
                              color: Color(0xFFD0D0D0).withOpacity(0.18))
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Visibility(
                          visible: (widget.showName && !widget.isSender) ||
                              widget.data.content['sendBy'] == null,
                          child: FutureBuilder<String>(
                            future: _cubit
                                .getNameByUUID(widget.data.content['sendBy']),
                            builder: (context, snapshot) => Text(
                              snapshot.data ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12,
                                      height: 16 / 12,
                                      color: R.color.tertiaryOrange),
                            ),
                          )),
             Linkify(
                          onOpen: (link) async {
                            _cubit.loadingWhenOpenLink();
                            if (await canLaunchUrl(Uri.parse(link.url))) {
                              await launchUrl(Uri.parse(link.url),
                                  mode: LaunchMode.externalApplication);
                            } else {
                              throw 'Could not launch $link';
                            }
                            _cubit.closeloading();
                          },
                          text: snapshotDelete.data == true || isDeleted
                              ? 'Tin nhắn đã bị xóa'
                              : widget.data.content['sendBy'] == null
                                  ? _getContentByAdmin(
                                      widget.data.content['content'] ?? "")
                                  : widget.data.content['content'],
                          style:snapshotDelete.data == true || isDeleted?TextStyle(
                            fontStyle: FontStyle.italic
                          ): Theme.of(context).textTheme.regular400.copyWith(
                              fontSize: 14,
                              height: 16 / 12,
                              color: widget.isSender
                                  ? R.color.white
                                  : R.color.black),
                          linkStyle: TextStyle(color: R.color.primaryColor),
                        ),

                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        _timeFormat.format(widget.publishAt!),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 10,
                            height: 14 / 10,
                            color: widget.isSender
                                ? R.color.white
                                : R.color.secondaryDarkGrey),
                      )
                    ],
                  ),
                ),
              ),
              // Positioned(
              //   bottom: 0.h,
              //   right: 3.w,
              //   child: _buildReaction(context),
              // ),
              Positioned(
                  bottom: 0.h,
                  right: 3.w,
                  child: _selectedReaction != null
                      ? Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: R.color.white),
                          child: Image.asset(
                            _selectedReaction ?? '',
                            width: 14.w,
                          ),
                        )
                      : SizedBox.shrink()),
            ],
          );
  }

  String _getContentByAdmin(String pattern) {
    if (pattern.startsWith(Const.SUPPORT_PATTERN))
      return R.string.dci_support_question.tr();
    else if (pattern.startsWith(Const.SUPPORT_DESCRIPTION_PATTERN))
      return R.string.dci_support_description.tr();
    return "";
  }

  Widget _buildFileContent(context) {
    final children = [
      GestureDetector(
        onTap: () async {
          setState(() {
            isDownloadFile = true;
          });
          var res = await appRepository.downloadPdf(
              widget.data.content['files'].first['link'],
              widget.data.content['files'].first['name']);
          setState(() {
            isDownloadFile = false;
          });
        },
        child: isDownloadFile
            ? SizedBox(
                width: 14, height: 14, child: CircularProgressIndicator())
            : Container(
                height: 20.h,
                width: 20.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: R.color.white,
                    image: DecorationImage(
                      image: AssetImage(R.drawable.ic_download),
                      fit: BoxFit.cover,
                    ),
                    boxShadow: [BoxShadow(blurRadius: 0.2)]),
              ),
      ),
      SizedBox(
        width: 8.w,
      ),
      Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 2.h),
        constraints: BoxConstraints(maxWidth: context.width * 0.6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: widget.isSender ? R.color.skyBlue : R.color.white,
            boxShadow: [
              if (!widget.isSender)
                BoxShadow(
                    offset: const Offset(0, 4),
                    blurRadius: 16,
                    color: Color(0xFFD0D0D0).withOpacity(0.18))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
                visible: widget.showName,
                child: FutureBuilder<String>(
                  future: _cubit.getNameByUUID(widget.data.content['sendBy']),
                  builder: (context, snapshot) => Text(
                    snapshot.data ?? "",
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12,
                        height: 16 / 12,
                        color: R.color.tertiaryOrange),
                  ),
                )),
            RichText(
                text: TextSpan(
              children: [
                TextSpan(
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      ;
                    },
                  text: widget.data.content['files'].first['name'],
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 12,
                      decoration: TextDecoration.underline,
                      height: 16 / 12,
                      color: widget.isSender ? R.color.white : R.color.black),
                )
              ],
            )),
            SizedBox(
              height: 2,
            ),
            Text(
              _timeFormat.format(widget.publishAt!),
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontSize: 10,
                  height: 14 / 10,
                  color: widget.isSender
                      ? R.color.white
                      : R.color.secondaryDarkGrey),
            )
          ],
        ),
      )
    ];
    return CompositedTransformTarget(
      link: layerLink,
      child: Stack(
        children: [
          Row(
            children: widget.isSender ? children : children.reversed.toList(),
          ),
          // Positioned(
          //     bottom: 0.h,
          //     right: widget.isSender ? 10 : 40,
          //     child: _buildReaction(context)),
          Positioned(
              bottom: 0.h,
              right: widget.isSender ? 10 : 40,
              child: _selectedReaction != null
                  ? Image.asset(
                      _selectedReaction ?? '',
                      width: 14.w,
                    )
                  : SizedBox.shrink()),
        ],
      ),
    );
  }
}
