import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:photo_view/photo_view.dart';

class ShowImage extends StatefulWidget {
  final url;

  ShowImage(this.url);

  @override
  State<ShowImage> createState() => _ShowImageState();
}

class _ShowImageState extends State<ShowImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      child: Stack(
        children: [
          Container(
            width: context.width,
            height: context.height,
            child: Center(
              child: Hero(
                tag: 'image',
                child: PhotoView(
                  minScale: PhotoViewComputedScale.contained * 0.8,
                  maxScale: PhotoViewComputedScale.covered * 1.0,
                  initialScale: PhotoViewComputedScale.contained,
                  imageProvider: NetworkImage(
                    widget.url,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 30,
            left: 15,
            child: IconButton(
                onPressed: () { Navigator.pop(context);},
                icon: const Icon(
                  Icons.arrow_back_rounded,
                  color: Colors.white,
                  size: 30,
                )),
          ),
        ],
      ),
      onTap: () {
        Navigator.pop(context);
      },
    ));
  }
}
