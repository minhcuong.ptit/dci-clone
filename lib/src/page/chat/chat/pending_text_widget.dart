import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:url_launcher/url_launcher.dart';

final DateFormat _timeFormat = DateFormat("HH:mm");

class PendingTextWidget extends StatelessWidget {
  final String content;
  final DateTime sendAt;
  const PendingTextWidget(
      {Key? key, required this.content, required this.sendAt})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 8.h),
            padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 2.h),
            constraints: BoxConstraints(
                maxWidth: context.width * 0.6),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: R.color.skyBlue,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Linkify(
                  onOpen: (link) async {
                    if (await canLaunchUrl(Uri.parse(link.url))) {
                      await launchUrl(Uri.parse(link.url));
                    } else {
                      throw 'Could not launch $link';
                    }
                  },
                  text: content,
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 14, height: 16 / 12, color: R.color.white),
                  linkStyle: TextStyle(color: R.color.primaryColor),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  _timeFormat.format(sendAt),
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 10, height: 14 / 10, color: R.color.white),
                )
              ],
            ),
          ),
          SizedBox(
            width: 8.w,
          ),
          SizedBox(
            height: 10.h,
            width: 10.h,
            child: CircularProgressIndicator(
              strokeWidth: 1,
              color: R.color.primaryColor,
            ),
          )
        ],
      ),
    );
  }
}
