part of 'chat_cubit.dart';

abstract class ChatState {}

class ChatInitialState extends ChatState {}

class ChatLoadingState extends ChatState {}

class ChatLoadingOpenLinkState extends ChatState {}

class ChatSuccessState extends ChatState {}

class ChatGetProfileSuccessState extends ChatState {}

class ChatNewMessageState extends ChatState {}

class ChatLoadMoreMessageState extends ChatState {
  final int oldLength;

  ChatLoadMoreMessageState(
    this.oldLength,
  );
}

class ChatRemovePendingMessageState extends ChatState {
  final int index;

  ChatRemovePendingMessageState(this.index);
}

class DeleteMessage extends ChatState {
  final String index;

  DeleteMessage(this.index);
}

class ChatFailureState extends ChatState {
  final String error;

  ChatFailureState(this.error);
}

class NotificationChatSuccessState extends ChatState {}
