import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/change_status_request.dart';
import 'package:imi/src/data/network/request/notification_chat_request.dart';
import 'package:imi/src/data/network/request/time_token_read_messages_request.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/pin_code/change_password/change_password_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:pubnub/pubnub.dart';
import '../../../../data/network/repository/app_graphql_repository.dart';
import '../../../../data/network/response/member_community.dart';
import '../../../../data/network/response/profile_chat_user_response.dart';
import '../../../../data/network/response/un_read_count_response.dart';
import '../../../../data/network/service/api_result.dart';
import 'package:imi/src/utils/app_config.dart' as config;
import 'dart:developer';
part 'chat_state.dart';

enum ChatType { group, private, support }

enum MessageContentType {
  IMAGE,
  TEXT,
  ADMIN_JOINED,
  ADMIN_DONE,
  FILE,
  CHANGE_STATUS,
  USER_REQUEST
}

enum TypeChatNotification {
  CONTENT_FILE,
  CONTENT_IMAGE,
}

class ChatCubit extends Cubit<ChatState> {
  bool hideKeyBoard = false;
  final AppRepository _appRepository = AppRepository();
  final AppGraphqlRepository graphqlRepository;
  final ChatType _chatType;
  late final PubNub _pubNub;
  late final Channel? _chatChannel;
  late final BatchHistoryResult? batchChannel;
  String? channelId;
  String channelIdSupport = '';
  String status = '';
  List<ProfileUserChatChannel> profileUser = [];
  PaginatedChannelHistory? _history;
  var hisTest;
  List<BaseMessage> _oldMessage = [];
  List<BaseMessage> _newMessage = [];
  List<BatchHistoryResultEntry> _oldMessage1=[];
  List<BatchHistoryResultEntry> _newMessage1=[];
  late BatchHistoryResultEntry test;
  late FetchMessageActionsResult messAction;
  bool isNotConversationYet = false;
  List<PendingMessage> pendingMessages = [];
  OverlayEntry? entry;
  int count = 0;
  int chunkSize = 20;
  bool isChangeStatus = false;
  List<String> timeTokenDeletedMessage=[];
  List<BaseMessage> get messages => [..._newMessage.reversed, ..._oldMessage]
    ..sort((a, b) => (-a.publishedAt.value + b.publishedAt.value).toInt());

  List<BatchHistoryResultEntry> get messages1 => [..._newMessage1.reversed, ..._oldMessage1]
    ..sort((a, b) => (-a.timetoken.value + b.timetoken.value).toInt());

  bool get canLoadMore => _history?.hasMore ?? false;
  bool isLoadingMore = false;

  String? get userId => appPreferences.getString(Const.ID);
  bool checkRead = true;

  ChatCubit(this._chatType, this.graphqlRepository)
      : super(ChatLoadingState()) {
    if (_chatType == ChatType.support) {
      profileUser.add(ProfileUserChatChannel(
          userUuid: null, fullName: R.string.dci_vietnam.tr()));
      _initSupportChat();
    }
  }

  void hideDropDown() {
    entry?.remove();
    entry = null;
  }

  void _initSupportChat() async {
    final res1 = await _appRepository.createSupportChat();
    res1.when(
        success: (_) {},
        failure: (_) async {
          final res2 = await graphqlRepository.fetchCustomerSupport();
          res2.when(
              success: (data) {
                initChat(channelId: data.channelPubnubId);
                status = data.status;
                channelIdSupport = data.channelPubnubId;
                // if(data.status=='DONE'){
                //   changeStatusSupport();
                //   _appRepository.changeStatus(ChangeStatusRequest(status: 'WAITING'),channelIdSupport );
                // }
              },
              failure: (_) {});
        });
  }

  void initChat({String? userUID, String? channelId, context}) async {
    if (channelId != null) {
      _initChannel(channelId, context);
      return;
    }
    final res = await _appRepository.createPrivateChannel(userUID ?? "");
    res.when(success: (data) {
      if (data is Map && data['id'] != null) {
        _initChannel(data['id'], context);
      }
    }, failure: (e) {
      emit(ChatFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  Future<String> getNameByUUID(String? uuid) async {
    String prefix = "";
    if (ChatType.support == _chatType &&
        uuid != appPreferences.getString(Const.ID))
      prefix = R.string.assistant.tr() + " ";
    int index = profileUser.indexWhere((element) => element.userUuid == uuid);
    if (index >= 0) return prefix + (profileUser[index].fullName ?? "");
    if (uuid != null) await getProfileUser(uuids: [uuid]);

    return prefix +
        (profileUser
                .firstWhere((element) => element.userUuid == uuid,
                    orElse: () => ProfileUserChatChannel(fullName: ""))
                .fullName ??
            "");
  }

  Future<String> getAvatarByUUID(String? uuid) async {
    int index = profileUser.indexWhere((element) => element.userUuid == uuid);
    if (index >= 0) return profileUser[index].avatarUrl ?? "";
    if (uuid != null) await getProfileUser(uuids: [uuid]);
    return profileUser
            .firstWhere((element) => element.userUuid == uuid,
                orElse: () => ProfileUserChatChannel(avatarUrl: ""))
            .avatarUrl ??
        "";
  }

  Future<void> getProfileUser(
      {required List<String> uuids, bool isRefresh = false}) async {
    ApiResult<List<ProfileUserChatChannel>> res =
        await graphqlRepository.fetchProfilesByUserUuids(uuids);
    res.when(success: (List<ProfileUserChatChannel> data) {
      profileUser.addAll(data);
      emit(ChatSuccessState());
    }, failure: (NetworkExceptions error) {
      ChatFailureState(NetworkExceptions.getErrorMessage(error));
    });
  }

  void getListUserChatGroup(int communityId) async {
    ApiResult<List<ListUserGroup>> res =
        await graphqlRepository.fetchGroupMember(communityId: communityId);
    res.when(success: (List<ListUserGroup> data) {
      for (int i = 0; i < data.length; i++) {
        profileUser.add(ProfileUserChatChannel(
            id: data[i].communityV2!.id,
            avatarUrl: data[i].communityV2!.avatarUrl,
            fullName: data[i].communityV2!.name,
            userUuid: data[i].userUuid));
      }
      emit(ChatSuccessState());
    }, failure: (NetworkExceptions error) {
      ChatFailureState(NetworkExceptions.getErrorMessage(error));
    });
  }

  void changeStatusSupport() {
    _pubNub.publish('ADMIN_CHAT_SUPPORT_CHANNEL', {
      "sendBy": appPreferences.getString(Const.ID),
      "type": MessageContentType.CHANGE_STATUS.name,
      "channelId": channelId
    });
  }

  Future<bool> checkDeletedMessage(String? uuids,Timetoken timeToken)async{
    if(uuids==appPreferences.getString(Const.ID)){
      return false;
    }
    BatchHistoryResultEntry check=_oldMessage1.where((element) => element.timetoken==timeToken).first;
    if(check.actions!.length>1){
      return true;
    }
    return false;
  }

  void sendMessage(String s) {
    s = s.trim();
    final temp = PendingMessage(
        type: MessageContentType.TEXT, content: s, sendAt: DateTime.now());
    pendingMessages.insert(0, temp);
    emit(ChatNewMessageState());
    _chatChannel?.publish({
      "content": s,
      "files": [],
      "images": [],
      "version": "1.0.0",
      "sendBy": appPreferences.getString(Const.ID),
      "type": MessageContentType.TEXT.name,
      "channelId": channelId
    }).then((value) async {
      if (_chatType == ChatType.support) {
        final res2 = await graphqlRepository.fetchCustomerSupport();
        res2.when(
            success: (data) {
              initChat(channelId: data.channelPubnubId);
              status = data.status;
              channelIdSupport = data.channelPubnubId;
              if (data.status == 'DONE') {
                // changeStatusSupport();
                _appRepository.changeStatus(
                    ChangeStatusRequest(status: 'WAITING'), channelIdSupport);
              }
            },
            failure: (_) {});
      }
      notificationMessages(channelId, s, null);
    });
  }

  Future<String?> _prepareSendFile(XFile file, MessageContentType type) async {
    final temp =
        PendingMessage(type: type, content: file, sendAt: DateTime.now());
    pendingMessages.insert(0, temp);
    emit(ChatNewMessageState());
    final value = await _appRepository.getListUploadUrl([file.name],
        moduleName: "CHAT", channelPubnubId: channelId, isPublic: true);
    return value.when(success: (data) async {
      if (data.isNotEmpty) {
        final res = await _appRepository.uploadImage(
            data.first.url ?? "", File(file.path));
        return res.when(success: (linkImage) {
          emit(ChatRemovePendingMessageState(pendingMessages.indexOf(temp)));
          pendingMessages.remove(temp);
          return "https://" + linkImage.realUri.host + linkImage.realUri.path;
        }, failure: (_) {
          emit(ChatFailureState(R.string.error_on_uploading_file.tr()));
          emit(ChatRemovePendingMessageState(pendingMessages.indexOf(temp)));
          pendingMessages.remove(temp);
          return null;
        });
      }
      return null;
    }, failure: (e) {
      emit(ChatFailureState(R.string.error_on_uploading_file.tr()));
      emit(ChatRemovePendingMessageState(pendingMessages.indexOf(temp)));
      pendingMessages.remove(temp);
      return null;
    });
  }

  void sendFile(XFile file) async {
    final data = await _prepareSendFile(file, MessageContentType.FILE);
    if (data == null) return;
    _chatChannel?.publish({
      "content": null,
      "files": [
        {"name": file.name, "link": data}
      ],
      "images": [],
      "version": "1.0.0",
      "sendBy": appPreferences.getString(Const.ID),
      "type": MessageContentType.FILE.name,
      "channelId": channelId
    }).then((value) async {
      if (_chatType == ChatType.support) {
        final res2 = await graphqlRepository.fetchCustomerSupport();
        res2.when(
            success: (data) {
              initChat(channelId: data.channelPubnubId);
              status = data.status;
              channelIdSupport = data.channelPubnubId;
              if (data.status == 'DONE') {
                // changeStatusSupport();
                _appRepository.changeStatus(
                    ChangeStatusRequest(status: 'WAITING'), channelIdSupport);
              }
            },
            failure: (_) {});
      }
      notificationMessages(
          channelId, null, TypeChatNotification.CONTENT_FILE.name);
    });
  }

  void sendImage(XFile image) async {
    final data = await _prepareSendFile(image, MessageContentType.IMAGE);
    if (data == null) return;
    _chatChannel?.publish({
      "content": null,
      "files": [],
      "images": [data],
      "version": "1.0.0",
      "sendBy": appPreferences.getString(Const.ID),
      "type": MessageContentType.IMAGE.name,
      "channelId": channelId
    }).then((value) async {
      if (_chatType == ChatType.support) {
        final res2 = await graphqlRepository.fetchCustomerSupport();
        res2.when(
            success: (data) {
              initChat(channelId: data.channelPubnubId);
              status = data.status;
              channelIdSupport = data.channelPubnubId;
              if (data.status == 'DONE') {
                // changeStatusSupport();
                _appRepository.changeStatus(
                    ChangeStatusRequest(status: 'WAITING'), channelIdSupport);
              }
            },
            failure: (_) {});
      }
      notificationMessages(
          channelId, null, TypeChatNotification.CONTENT_IMAGE.name);
    });
  }

  void _sendSupportQuestion() {
    _chatChannel?.publish({
      "content": Const.SUPPORT_PATTERN,
      "files": [],
      "images": [],
      "version": "1.0.0",
      "sendBy": null,
      "type": MessageContentType.TEXT.name,
      "channelId": channelId
    }).then((value) {
    });
  }

  void _sendSupportWaiting() {
    _chatChannel?.publish({
      "content": Const.SUPPORT_DESCRIPTION_PATTERN,
      "files": [],
      "images": [],
      "version": "1.0.0",
      "sendBy": null,
      "type": MessageContentType.TEXT.name,
      "channelId": channelId
    }).then((value) {
    });
  }

  void _initChannel(String channelId, context) async {
    this.channelId = channelId;
    _pubNub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey:
                config.AppConfig.environment.pubNubSubscribeKey,
            publishKey:
                config.AppConfig.environment.pubNubPublishKey,
            userId: UserId(appPreferences.getString(Const.ID) ?? "")));
    _chatChannel = _pubNub.channel(channelId);
    batchChannel = await _pubNub.batch.fetchMessages({channelId},
        count: chunkSize,
        includeMessageActions: true,
        includeMessageType: true,
        includeMeta: true);
    _oldMessage1=batchChannel?.channels[channelId.toString()] as List<BatchHistoryResultEntry>;
    _history = _chatChannel?.history(
        chunkSize: chunkSize, order: ChannelHistoryOrder.descending);
    _history?.more().then((value) async {
      _oldMessage = _history!.messages.reversed.toList();
      if (_oldMessage.isEmpty) {
        isNotConversationYet = true;
      }
      if (_oldMessage.length == 0 && _chatType == ChatType.support) {
        _sendSupportQuestion();
      }
      if (_oldMessage.isNotEmpty) {
        readMessage(channelId, _oldMessage.first.publishedAt.value.toInt());//sau khi khơi tạo
      }
      emit(ChatSuccessState());
    });
    _pubNub.subscribe(channels: {channelId}).messages.listen((event) async {
      if (event.messageType == MessageType.messageAction) {
        timeTokenDeletedMessage.add(event.payload['data']['messageTimetoken'].toString());
        emit(DeleteMessage(event.payload['data']['messageTimetoken']));
      }
          if (_chatChannel != null) {
            if (!MessageContentType.values
                .any((element) => element.name == event.content['type']))
              return;
            isNotConversationYet = false;
            _newMessage.add(event);
            if (checkRead) {
              readMessage(channelId,
                  event.publishedAt.value.toInt()); // sau khi gửi hoặc nhận
            }
            if (event.content['type'] == MessageContentType.TEXT.name) {
              int tmp = pendingMessages.indexWhere(
                  (element) => element.content == event.content['content']);
              if (tmp >= 0) {
                pendingMessages.removeAt(tmp);
                emit(ChatRemovePendingMessageState(tmp));
              }
            }
            if (messages.length == 1) {
              emit(ChatSuccessState());
            } else {
              if (_chatType == ChatType.support && messages.length == 2) {
                await Future.delayed(Duration(microseconds: 500), () {
                  _sendSupportWaiting();
                });
              }
              emit(ChatNewMessageState());
            }
            if (event.content['sendBy'] == appPreferences.getString(Const.ID)) {
              if (_chatType == ChatType.support) {
                _appRepository.updateMessageSeen(channelId,
                    event.publishedAt.value.toInt(), 'CUSTOMER_SUPPORT');
              } else {
                _appRepository.updateMessageSeen(
                    channelId,
                    event.publishedAt.value.toInt(),
                    ChatType.private.name.toUpperCase());
              }
            }
          }
        });
  }

  void getListReaction(String channelId) async {
    batchChannel = await _pubNub.batch.fetchMessages({channelId},
        count: chunkSize,
        includeMessageActions: true,
        includeMessageType: true,
        includeMeta: true);
    for (int i = 0;
        i <
            int.parse(
                batchChannel?.channels.values.toList()[0].length.toString() ??
                    '0');
        i++) {
      List? actionsMess =
          batchChannel?.channels.values.toList()[0][i].actions?.values.toList();
      for (int j = 0;
          j < int.parse(actionsMess?.length.toString() ?? '0');
          j++) {
        if (actionsMess![j].keys.toString() != '(read)') {
          if (actionsMess[j].keys.toString() != null &&
              actionsMess[j].keys.toString().length > 1) {
            String action = actionsMess[j].keys.toString().substring(1);
            action = action.substring(0, action.length - 1);
          }
        }
      }
    }
  }

  void getMessageAction(){

  }


  void loadMore(String channelId) async {
    count++;
    if (_history?.hasMore ?? true && !isLoadingMore) {
      isLoadingMore = true;
      _history?.more().then((value) async {
        int temp = _oldMessage.length;
        _oldMessage = _history!.messages;
        BatchHistoryResult? batchChannelTemp = await _pubNub.batch
            .fetchMessages({channelId},
            count: chunkSize,
            start: messages[count * 20 - 1].publishedAt,
            end: messages[messages.length - 1].publishedAt,
            includeMessageActions: true,
            includeMessageType: true,
            includeMeta: true);
        _oldMessage1.addAll(batchChannelTemp.channels[channelId.toString()] as List<BatchHistoryResultEntry>);
        emit(ChatLoadMoreMessageState(temp+_newMessage.length));
      }).whenComplete(() {
        isLoadingMore = false;
      });
    }
  }

  void readMessage(String channelId, int time) async {
    _pubNub
        .addMessageAction(
      channel: channelId,
      timetoken: Timetoken(BigInt.from(time)),
      value: "read",
      type: "receipt",
    )
        .then((value) {
      if (_chatType != ChatType.support)
        timeTokenRead(channelId, int.parse(value.action.actionTimetoken));
    });
  }

  void reactionMessage(
      String channelId, int time, String typeOfReaction) async {
    _pubNub
        .addMessageAction(
          channel: channelId,
          timetoken: Timetoken(BigInt.from(time)),
          value: typeOfReaction,
          type: "reaction",
        )
        .then((value) => {})
        .catchError((error, stackTrace) => {});
  }

  void deleteAction(int messageTime, int actionTime, String channel,
      String typeOfReaction) async {
    _pubNub
        .deleteMessageAction(channel,
            messageTimetoken: Timetoken(BigInt.from(messageTime)),
            actionTimetoken: Timetoken(BigInt.from(actionTime)))
        .then((value) {
      reactionMessage(channel, actionTime, typeOfReaction);
    }).onError((error, stackTrace) {});
  }

  void timeTokenRead(String? channelId, int? timeToken) async {
    final res = await _appRepository.timeTokenRead(TimeTokenReadRequest(
      channelId: channelId,
      timeToken: timeToken,
      userUuid: userId,
    ));
    res.when(success: (dynamic data) {
      emit(ChatSuccessState());
    }, failure: (NetworkExceptions error) {
      ChatFailureState(NetworkExceptions.getErrorMessage(error));
    });
  }

  void notificationMessages(
      String? channelId, String? content, String? contentType) async {
    final res = await _appRepository.notificationChat(NotificationChatRequest(
      channelId: channelId,
      content: content,
      contentType: contentType,
    ));
    res.when(success: (dynamic data) {
      emit(NotificationChatSuccessState());
    }, failure: (NetworkExceptions error) {
      ChatFailureState(NetworkExceptions.getErrorMessage(error));
    });
  }

  void getReadTotalCount() async {
    ApiResult<TotalCount> result =
    await graphqlRepository.unreadCount(userId ?? "");
    result.when(success: (TotalCount data) async {
      appPreferences.setData(Const.COUNT_MESSAGE, data.count);
      emit(ChatSuccessState());
    }, failure: (NetworkExceptions error) async {
      emit(ChatFailureState(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void loadingWhenOpenLink() async {
    emit(ChatLoadingState());
  }

  void closeloading() {
    emit(ChatSuccessState());
  }
}

class PendingMessage {
  final MessageContentType type;
  final Object content;
  final DateTime sendAt;

  PendingMessage(
      {required this.type, required this.content, required this.sendAt});
}

class Message{
   late BaseMessage mess;
   late bool isDelete;
}

