import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/utils/custom_theme.dart';

class PendingFileChat extends StatelessWidget {
  final XFile file;
  const PendingFileChat({Key? key, required this.file}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 4.h),
            constraints: BoxConstraints(
                maxWidth: context.width * 0.6),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: R.color.skyBlue,
            ),
            child: Text(
              file.name,
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontSize: 12,
                  decoration: TextDecoration.underline,
                  height: 16 / 12,
                  color: R.color.white),
            ),
          ),
          SizedBox(
            width: 8.w,
          ),
          SizedBox(
            height: 10.h,
            width: 10.h,
            child: CircularProgressIndicator(
              strokeWidth: 1,
              color: R.color.primaryColor,
            ),
          )
        ],
      ),
    );
  }
}
