import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';

class PendingImageWidget extends StatelessWidget {
  final String path;
  const PendingImageWidget({Key? key, required this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(9.h),
            child: SizedBox(
              height: 108,
              width: 144,
              child: Image.asset(
                path,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            width: 8.w,
          ),
          SizedBox(
            height: 10.h,
            width: 10.h,
            child: CircularProgressIndicator(
              strokeWidth: 1,
              color: R.color.primaryColor,
            ),
          )
        ],
      ),
    );
  }
}
