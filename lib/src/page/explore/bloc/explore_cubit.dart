import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../data/network/repository/app_graphql_repository.dart';
import '../../../data/network/repository/app_repository.dart';
import '../explore.dart';

class ExploreCubit extends Cubit<ExploreState> {

  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  ExploreCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(ExploreInitial());
}