import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'explore.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({Key? key}) : super(key: key);

  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  late ExploreCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ExploreCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: BlocProvider(
          create: (context) => _cubit,
          child: BlocListener<ExploreCubit, ExploreState>(
            listener: (context, state) {},
            child: BlocBuilder<ExploreCubit, ExploreState>(
              builder: (context, state) {
                return _buildPage(context, state);
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, ExploreState state) {
    return Container();
  }
}
