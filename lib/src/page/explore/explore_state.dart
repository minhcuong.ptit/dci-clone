import 'package:equatable/equatable.dart';

abstract class ExploreState extends Equatable {
  const ExploreState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class ExploreInitial extends ExploreState {
  @override
  String toString() => 'ExploreInitial';
}

class ExploreUnInitial extends ExploreState {
  @override
  String toString() => 'ExploreUnInitial';
}

class ExploreLoading extends ExploreState {
  @override
  String toString() => 'ExploreLoading';
}

class ExploreSuccess extends ExploreState {
  final String? message;

  const ExploreSuccess({this.message});

  @override
  String toString() => 'ExploreSuccess { message: $message }';
}

class ExploreFailure extends ExploreState {
  final String error;

  const ExploreFailure(this.error);

  @override
  String toString() => 'ExploreFailure { error: $error }';
}