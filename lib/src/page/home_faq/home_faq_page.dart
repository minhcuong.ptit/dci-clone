import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/response/list_topic_response.dart';
import 'package:imi/src/page/home_faq/home_faq.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../utils/utils.dart';
import '../detail_topic/detail_topic.dart';

class HomeFaqPage extends StatefulWidget {
  const HomeFaqPage({Key? key}) : super(key: key);

  @override
  State<HomeFaqPage> createState() => _HomeFaqPageState();
}

class _HomeFaqPageState extends State<HomeFaqPage> {
  late HomeFaqCubit _cubit;
  RefreshController _controller = RefreshController();
  TextEditingController _searchController = TextEditingController();
  bool hideAppBar = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HomeFaqCubit(graphqlRepository, repository);
    _cubit.initDatabase();
    _cubit.getListTopicFaq();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          backgroundColor: R.color.primaryColor,
          title: Stack(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        NavigationUtils.pop(context);
                      },
                      child: Icon(
                        CupertinoIcons.back,
                        color: R.color.white,
                        size: 24.h,
                      ),
                    ),
                    Text(
                      R.string.frequently_question.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 16.sp, color: R.color.white),
                    ),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            hideAppBar = !hideAppBar;
                          });
                        },
                        icon: Icon(
                          CupertinoIcons.search,
                          size: 24.h,
                          color: R.color.white,
                        )),
                  ],
                ),
              ),
              buildSearchAppbar(context)
            ],
          )),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<HomeFaqCubit, HomeFaqState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! HomeFaqLoading) {
              _controller.refreshCompleted();
              _controller.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, HomeFaqState state) {
    final listKeyword = _cubit.listPreviousKeyword;
    return Stack(
      children: [
        StackLoadingView(
          visibleLoading: state is HomeFaqLoading,
          child: SmartRefresher(
            controller: _controller,
            enablePullUp: true,
            onRefresh: () {
              _cubit.getListTopicFaq(isRefresh: true);
            },
            onLoading: () {
              _cubit.getListTopicFaq(isLoadMore: true);
            },
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 12.h),
              children: [
                Text(
                  R.string.topics_of_interest.tr(),
                  style: Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: 18.sp,
                        height: 24.h / 18.sp,
                      ),
                ),
                SizedBox(height: 10.h),
                GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 18.h,
                    crossAxisSpacing: 18.h,
                    childAspectRatio: 18 / 20,
                  ),
                  itemCount: _cubit.listTopic.length,
                  itemBuilder: (BuildContext context, int index) {
                    FetchTopicsData data = _cubit.listTopic[index];
                    return InkWell(
                      onTap: () {
                        NavigationUtils.navigatePage(
                            context,
                            DetailTopicPage(
                              topicIds: [data.id ?? 0],
                              titleTopic: data.name,
                            ));
                      },
                      child: Container(
                        padding: EdgeInsets.all(4.h),
                        decoration: BoxDecoration(
                            color: R.color.white,
                            borderRadius: BorderRadius.circular(8.h)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8.h),
                              child: CachedNetworkImage(
                                height: 110.h,
                                width: double.infinity,
                                imageUrl: data.imageUrl ?? "",
                                fit: BoxFit.cover,
                                placeholder: (_, __) {
                                  return Text(
                                    R.string.loading.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(fontSize: 14.sp),
                                  );
                                },
                              ),
                            ),
                            SizedBox(height: 4.h),
                            Text(
                              data.name ?? "",
                              textAlign: TextAlign.start,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .medium500
                                  .copyWith(
                                      fontSize: 12.sp,
                                      color: R.color.neutral1,
                                      height: 18.h / 12.sp),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        Visibility(
          visible: hideAppBar,
          child: listKeyword.length == 0
              ? SizedBox()
              : Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.h),
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.h, vertical: 10.h),
                  decoration: BoxDecoration(
                    border: Border.all(color: R.color.grey300, width: 1),
                    borderRadius: BorderRadius.circular(8.h),
                    color: R.color.white,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: listKeyword.length + 1,
                    itemBuilder: (context, index) {
                      if (index == 0) {
                        return Row(
                          children: [
                            Expanded(
                              child: Text(
                                R.string.recent_search.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 12.sp),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                _cubit.deleteAll();
                              },
                              child: Text(
                                R.string.delete_all.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp,
                                        color: R.color.primaryColor),
                              ),
                            )
                          ],
                        );
                      } else {
                        final keyword = listKeyword[index - 1];
                        return Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  _searchController.text =
                                      keyword.keyWord ?? "";
                                  NavigationUtils.navigatePage(
                                      context,
                                      DetailTopicPage(
                                        searchKey:
                                            _searchController.text.trim(),
                                      ));
                                },
                                child: Text(
                                  keyword.keyWord ?? "",
                                  style: Theme.of(context).textTheme.regular400,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                            GestureDetector(
                                onTap: () {
                                  _cubit.deleteSearchSuggest(keyword.id ?? 0);
                                },
                                child: Icon(
                                  Icons.clear,
                                  size: 14.h,
                                ))
                          ],
                        );
                      }
                    },
                  ),
                ),
        )
      ],
    );
  }

  Widget buildSearchAppbar(BuildContext context) {
    return Visibility(
      visible: hideAppBar || (!Utils.isEmpty(_searchController.text)),
      child: Container(
        color: R.color.primaryColor,
        child: Row(
          children: [
            GestureDetector(
              onTap: () {
                NavigationUtils.pop(context);
              },
              child: Icon(
                CupertinoIcons.back,
                color: R.color.white,
                size: 24.h,
              ),
            ),
            SizedBox(width: 5.h),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15.h, vertical: 5.h),
                margin: EdgeInsets.symmetric(vertical: 8.h),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.h),
                  color: R.color.white,
                  boxShadow: [
                    BoxShadow(
                      color: R.color.black.withOpacity(0.1),
                      blurRadius: 5,
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    InkWell(
                        onTap: () {
                          _cubit.searchSuggest(
                              keyWord: _searchController.text.trim());
                          NavigationUtils.navigatePage(
                              context,
                              DetailTopicPage(
                                searchKey: _searchController.text.trim(),
                              ));
                        },
                        child: Icon(CupertinoIcons.search,
                            size: 24.h, color: R.color.grey)),
                    SizedBox(width: 5.h),
                    Expanded(
                      child: CupertinoSearchTextField(
                        autofocus: true,
                        padding: EdgeInsetsDirectional.fromSTEB(0, 5, 5, 5),
                        decoration: BoxDecoration(
                            color: R.color.white,
                            borderRadius: BorderRadius.circular(30.h)),
                        controller: _searchController,
                        prefixIcon: SizedBox(),
                        placeholder: R.string.search.tr(),
                        style: Theme.of(context).textTheme.bodyRegular.copyWith(
                              color: R.color.black,
                            ),
                        onChanged: (text) {},
                        onSubmitted: (text) {
                          _cubit.searchSuggest(keyWord: text);
                          FocusScope.of(context).requestFocus(new FocusNode());
                          NavigationUtils.navigatePage(
                              context,
                              DetailTopicPage(
                                searchKey: _searchController.text.trim(),
                              )).then((value) => _searchController.clear());
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
