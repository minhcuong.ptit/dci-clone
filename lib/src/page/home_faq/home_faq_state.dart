abstract class HomeFaqState {
  HomeFaqState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class HomeFaqInitial extends HomeFaqState {
  @override
  String toString() => 'HomeFaqInitial';
}

class HomeFaqSuccess extends HomeFaqState {
  @override
  String toString() => 'HomeFaqSuccess';
}

class HomeFaqLoading extends HomeFaqState {
  @override
  String toString() => 'HomeFaqLoading';
}

class HomeFaqFailure extends HomeFaqState {
  final String error;

  HomeFaqFailure(this.error);

  @override
  String toString() => 'HomeFaqFailure { error: $error }';
}

class HomeSearchSuccess extends HomeFaqState {
  @override
  String toString() => 'HomeSearchSuccess';
}