import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/home_faq/home_faq_state.dart';

import '../../data/database/app_database.dart';
import '../../data/database/entities/search_keyword_entity.dart';
import '../../data/database/sqflite/DBHelper.dart';
import '../../data/database/sqflite/search_suggest.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/response/list_topic_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../utils/utils.dart';

class HomeFaqCubit extends Cubit<HomeFaqState> {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;
  List<FetchTopicsData> listTopic = [];
  String? nextToken;
  final db = DBHelper();
  late AppDatabase database;
  List<SearchSuggest> listPreviousKeyword = [];

  HomeFaqCubit(this.graphqlRepository, this.repository)
      : super(HomeFaqInitial());

  void getListTopicFaq({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh || !isLoadMore ? HomeFaqLoading() : HomeFaqInitial());
    if (isRefresh) {
      nextToken = null;
      listTopic.clear();
    }
    ApiResult<FetchTopics> result =
        await graphqlRepository.getListTopic(nextToken: nextToken);
    result.when(success: (FetchTopics data) async {
      listTopic.addAll(data.data ?? []);
      nextToken = data.nextToken;
      emit(HomeFaqSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeFaqFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void initDatabase() async {
    database =
    await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    await getPreviousSearch();
  }

  void searchSuggest({String? keyWord, String? type}) async {
    emit(HomeFaqLoading());
    await db.save(SearchSuggest(keyWord: keyWord, type: "TOPIC"));
    emit(HomeFaqInitial());
    getPreviousSearch();
  }

  void deleteSearchSuggest(int id) async {
    emit(HomeFaqLoading());
    await db.delete(id);
    emit(HomeFaqInitial());
    getPreviousSearch();
  }

  void deleteAll() async {
    emit(HomeFaqLoading());
    await db.deleteAll();
    emit(HomeFaqInitial());
    getPreviousSearch();
  }

  Future getPreviousSearch({bool isRefresh = false}) async {
    emit(isRefresh ? HomeFaqInitial() : HomeFaqLoading());
    listPreviousKeyword =
    await db.getSearchSuggests(keyword: "", type: "TOPIC");
    emit(HomeSearchSuccess());
  }
}
