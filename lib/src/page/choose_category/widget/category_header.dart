import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';

class CategoryHeader extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 2.h),
              child: Stack(
                children: [
                  Image.asset(
                    R.drawable.bg_category,
                    width: MediaQuery.of(context).size.width,
                    height: 192.h,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: Center(
                          child: Text(
                            R.string.setup_favorite.tr().toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .title2
                                .apply(color: R.color.white)
                                .copyWith(height: 27 / 20, fontSize: 20.sp),
                            textAlign: TextAlign.center,
                          )))
                ],
              ),
            ),
            Positioned(
                bottom: 0.h,
                left: 0.w,
                right: 0.w,
                child: Container(
                  height: 20.h,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(26.w),
                        topRight: Radius.circular(26.w),
                      ),
                      color: Colors.white),
                )),
          ],
        ),
      ],
    );
  }

}