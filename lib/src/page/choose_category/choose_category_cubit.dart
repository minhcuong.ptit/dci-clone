import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/submit_favorite_request.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/list_content_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/choose_category/choose_category_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';

import '../../data/preferences/app_preferences.dart';

class ChooseCategoryCubit extends Cubit<ChooseCategoryState> {
  List<CategoryData> listCategory = [];
  List<CategoryData> listSelectedCategory = [];
  List<CommunityDataV2> listCommunity = [];
  List<CommunityDataV2> listSelectedCommunity = [];
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  List<int> get listFavoriteCategory => appPreferences
      .getListString(Const.SAVE_CATEGORY)
      .map((e) => int.parse(e))
      .toList();

  List<int> get listFavoriteCommunity => appPreferences
      .getListString(Const.FAVORITE_COMMUNITY)
      .map((e) => int.parse(e))
      .toList();

  ChooseCategoryCubit(this.graphqlRepository, this.repository)
      : super(InitialChooseCategoryState());

  // void getListContent() async {
  //   emit(GetListCommunitySuccess());
  // }

  void selectCategory(CategoryData selectedCategory) {
    emit(ChooseCategoryLoading());
    int index = listSelectedCategory
        .indexWhere((element) => element.id == selectedCategory.id);
    if (index >= 0) {
      listSelectedCategory.removeAt(index);
    } else {
      listSelectedCategory.add(selectedCategory);
    }
    emit(ChooseCategorySuccess());
  }
  void applyFavoriteCategory({bool isCancel = false}) {
    emit(ChooseCategoryLoading());
    if (isCancel)
      listSelectedCategory =
          listCategory.where((element) => listFavoriteCategory.contains(element.id)).toList();
    appPreferences.setData(
        Const.SAVE_CATEGORY, (listSelectedCategory).map((e) => e.id).toList());
    emit(ChooseCategorySuccess());
  }

  void getListContent({bool isRefresh = false}) async {
    emit(isRefresh ? InitialChooseCategoryState() : ChooseCategoryLoading());
    ApiResult<ListContentResponse> contentTask =
        await graphqlRepository.getListContent(listFavoriteCategory);
    contentTask.when(success: (ListContentResponse data) async {
      //listCommunity = data.communities?.data ?? [];
      listCategory = data.categories ?? [];
      listSelectedCategory = listCategory
          .where((element) => listFavoriteCategory.contains(element.id))
          .toList();
      listSelectedCommunity = listCommunity
          .where((element) => listFavoriteCommunity.contains(element.id))
          .toList();
      emit(GetListCommunitySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ChooseCategoryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void submitFavorite() async {
    emit(ChooseCategoryLoading());
    try {
      List<int> listSelectedCategoryId =
          listSelectedCategory.map((e) => e.id!).toList();
      List<int> listSelectedCommunityId =
          listSelectedCommunity.map((e) => e.id!).toList();
      SubmitFavoriteRequest request = SubmitFavoriteRequest(
        categories: listSelectedCategoryId,
        communities: listSelectedCommunityId,
      );
      ApiResult<dynamic> submitFavoriteTask =
          await repository.submitFavorite(request);
      submitFavoriteTask.when(success: (dynamic data) async {
        emit(FavoriteSuccess());
        //getHomeTimeline(isRefresh: true);
      }, failure: (NetworkExceptions error) async {
        emit(ChooseCategoryFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } catch (e) {
      logger.e(e);
      //emit(HomeInitial());
    }
  }
}
