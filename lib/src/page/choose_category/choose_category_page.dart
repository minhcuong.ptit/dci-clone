import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/page/choose_category/choose_category_cubit.dart';
import 'package:imi/src/page/choose_category/choose_category_state.dart';
import 'package:imi/src/page/choose_category/widget/category_header.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';

import '../../widgets/button_widget.dart';
import '../../widgets/category_widget.dart';
import '../../widgets/custom_appbar.dart';

class ChooseCategoryPage extends StatefulWidget {
  final List<CategoryData> listCategory;
  final List<CategoryData> listSelectedCategory;

  const ChooseCategoryPage(
      {Key? key,
      required this.listCategory,
      required this.listSelectedCategory})
      : super(key: key);
  @override
  _ChooseCategoryPageState createState() => _ChooseCategoryPageState();
}

class _ChooseCategoryPageState extends State<ChooseCategoryPage> {
  late ChooseCategoryCubit _cubit;
  int _columnNumber = 2;
  int _rowNumber = 0;
  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ChooseCategoryCubit(graphqlRepository, repository);
    _cubit.listCategory = List.from(widget.listCategory);
    _cubit.listSelectedCategory = List.from(widget.listSelectedCategory);
    _cubit.getListContent();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<ChooseCategoryCubit, ChooseCategoryState>(
        listener: (context, state) {
          if (state is! ChooseCategoryLoading) {}
          if (state is FavoriteSuccess) {
            // NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
            NavigationUtils.pop(context,
                result: _cubit.listSelectedCategory);
          }
          },
        builder: (context, state) {
          return _buildPage(context, state);
        },
      ),
    );
  }

  Widget _buildPage(BuildContext context, ChooseCategoryState state) {
    return Scaffold(
      appBar: appBar(context, '',
          elevation: 0,
          onPop: (){ NavigationUtils.pop(context,result: _cubit.listSelectedCategory);},
          backgroundColor: Colors.transparent,
          iconColor: R.color.white),
      extendBodyBehindAppBar: true,
      backgroundColor: R.color.white,
      body: StackLoadingView(
        visibleLoading: state is ChooseCategoryLoading,
        child: Column(
          children: [
            CategoryHeader(),
            Expanded(
              child: _buildListItem(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 16.h),
          _categoryWidget(),
          SizedBox(height: 24.h),
          SizedBox(
            width: 250.w,
            child: ButtonWidget(
                textSize: 16.sp,
                height: 48.h,
                uppercaseTitle: true,
                padding: EdgeInsets.only(top: 5.h),
                title: R.string.save.tr(),
                onPressed: _cubit.listSelectedCategory.length < 3
                    ? null
                    : () {
                        if (Navigator.canPop(context)) {
                          _cubit.submitFavorite();
                        }
                      }),
          ),
          SizedBox(
            height: 70.h,
          )
        ],
      ),
    );
  }

  Widget _categoryWidget() {
    if (_cubit.listCategory.isEmpty) {
      return SizedBox();
    }
    _rowNumber = (_cubit.listCategory.length / _columnNumber).ceil();
    var listWidget = <Widget>[];
    for (var i = 0; i < _rowNumber; i++) {
      listWidget.add(Padding(
        padding: EdgeInsets.only(top: 16.h),
        child: _buildRow(context, i),
      ));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          children: listWidget,
        ),
      ],
    );
  }

  Widget _buildRow(BuildContext context, int line) {
    var list = <Widget>[];
    for (var i = _columnNumber * line; i < _cubit.listCategory.length; i++) {
      list.add(Expanded(child: _buildItemOfRow(context, i)));
      if (list.length >= _columnNumber) break;
    }
    while (list.length < _columnNumber) {
      list.add(Expanded(child: Container()));
    }
    list.insert(
        1,
        SizedBox(
          width: 16.w,
        ));
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: list);
  }

  Widget _buildItemOfRow(BuildContext context, index) {
    CategoryData e = _cubit.listCategory[index];
    bool isSelected = _cubit.listSelectedCategory
            .indexWhere((element) => element.id == e.id) >=
        0;
    return CategoryWidget(
      data: e,
      isLoadMore: false,
      isSelected: isSelected,
      onTap: () {
        _cubit.selectCategory(e);
        _cubit.applyFavoriteCategory();
      },
      shadow: BoxShadow(
        color: R.color.shadowColor,
        spreadRadius: 0,
        blurRadius: 3.h,
        offset: Offset(0, 3), // changes position of shadow
      ),
    );
  }
}
