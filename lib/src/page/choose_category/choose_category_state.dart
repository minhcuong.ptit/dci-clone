import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ChooseCategoryState extends Equatable {
  ChooseCategoryState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialChooseCategoryState extends ChooseCategoryState {}

class ChooseCategoryLoading extends ChooseCategoryState {
  @override
  String toString() => 'BodyParameterLoading';
}

class GetListCategorySuccess extends ChooseCategoryState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends ChooseCategoryState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class FavoriteSuccess extends ChooseCategoryState {
  @override
  String toString() {
    return 'FavoriteSuccess';
  }
}

class ChooseCategorySuccess extends ChooseCategoryState {
  @override
  String toString() {
    return 'ChooseCategorySuccess';
  }
}

class ChooseCategoryFailure extends ChooseCategoryState {
  final String error;

  ChooseCategoryFailure(this.error);

  @override
  String toString() => 'BodyParameterFailure { error: $error }';
}
