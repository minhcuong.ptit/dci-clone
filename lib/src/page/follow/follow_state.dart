
import 'package:equatable/equatable.dart';

abstract class FollowState extends Equatable {
  @override
  List<Object> get props => [];
}

class FollowInitial extends FollowState {}

class FollowFailure extends FollowState {
  final String error;

  FollowFailure(this.error);

  @override
  String toString() => 'FollowFailure { error: $error }';
}

class FollowSuccess extends FollowState {
  final String message;

  FollowSuccess(this.message);

  @override
  String toString() => 'FollowSuccess { message: $message }';
}

class FollowLoading extends FollowState {
  @override
  String toString() => 'FollowLoading';
}

class GetListFollowEmpty extends FollowState {
  @override
  String toString() {
    return 'GetListFollowEmpty{}';
  }
}

class GetListFollowSuccess extends FollowState {
  @override
  String toString() => 'GetListFollowSuccess';
}
