import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/follow_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/club_info/club_information/club_information_page.dart';
import 'package:imi/src/page/profile/view_profile/view_profile.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/user_list_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'follow.dart';

enum FollowType { FOLLOWER, FOLLOWING }

class FollowPage extends StatefulWidget {
  final FollowType type;
  final int communityId;
  final int numberNo;

  const FollowPage(
      {Key? key,
      required this.type,
      required this.communityId,
      required this.numberNo})
      : super(key: key);

  @override
  _FollowPageState createState() => _FollowPageState();
}

class _FollowPageState extends State<FollowPage> {
  late FollowCubit _cubit;

  RefreshController _refreshController = RefreshController();
  late bool _isOwner;

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = FollowCubit(
        repository: repository,
        graphqlRepository: graphqlRepository,
        communityID: widget.communityId);
    _cubit.getListFollow(type: widget.type);
    super.initState();

    _isOwner =
        (widget.communityId == appPreferences.getInt(Const.COMMUNITY_ID));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<FollowCubit, FollowState>(
        listener: (context, state) {
          if (state is FollowFailure)
            Utils.showErrorSnackBar(context, state.error);
          if (state is! FollowLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is FollowSuccess) {}
          if (state is GetListFollowEmpty) {}
        },
        builder: (BuildContext context, FollowState state) {
          int count = 0;
          if (!_isOwner || widget.type == FollowType.FOLLOWER) {
            count = widget.numberNo;
          } else {
            count = widget.numberNo - _cubit.unfollowCount;
          }
          return Scaffold(
              body: buildPage(context, state),
              appBar: appBar(
                  context,
                  count.toString() +
                      " " +
                      (FollowType.FOLLOWER == widget.type
                          ? R.string.followers.tr()
                          : R.string.following.tr()),
                  backgroundColor: R.color.white,
                  iconColor: R.color.black,
                  elevation: 0.5.h,
                  centerTitle: false,
                  leadingWidget: IconButton(
                      onPressed: () => {
                            NavigationUtils.pop(context,
                                result: Const.ACTION_REFRESH)
                          },
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 18.h,
                      ))));
        },
      ),
    );
  }

  Widget buildPage(BuildContext context, FollowState state) {
    return Container(
      height: double.infinity,
      color: R.color.baseWhite,
      child: StackLoadingView(
        visibleLoading: state is FollowLoading,
        child: RefreshConfiguration(
          enableScrollWhenRefreshCompleted: false,
          child: SmartRefresher(
            enablePullUp: _cubit.nextToken != null,
            controller: _refreshController,
            onRefresh: () =>
                _cubit.getListFollow(type: widget.type, isRefresh: true),
            onLoading: () =>
             _cubit.getListFollow(type: widget.type, isLoadMore: true),
            child: _cubit.listFollow.length == 0
                ? Visibility(
                    visible: state is GetListFollowEmpty,
                    child: Center(
                        child: Text(
                      widget.type == FollowType.FOLLOWER
                          ? R.string.no_follower.tr()
                          : R.string.no_following.tr(),
                    )),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    itemCount: _cubit.listFollow.length,
                    physics:state is FollowLoading? NeverScrollableScrollPhysics():BouncingScrollPhysics(),
                    separatorBuilder: (context, index) => Container(
                      color: R.color.grey,
                      height: 0.5.h,
                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      FollowDataV2 data = _cubit.listFollow[index];
                      return UserListWidget(
                        data: data,
                        isCurrentUser: widget.communityId ==
                            AppPreferences().getInt(Const.COMMUNITY_ID),
                        userListType: widget.type == FollowType.FOLLOWER
                            ? UserListType.follower
                            : UserListType.following,
                        followCallback: () {
                          follow(data);
                        },
                        profileCallback: () {
                          if (data.type == CommunityType.INDIVIDUAL.name)
                            NavigationUtils.navigatePage(
                                context,
                                ViewProfilePage(
                                  communityId: data.id,
                                )).then((value) {
                              _cubit.getListFollow(
                                  isRefresh: true, type: widget.type);
                            });
                          else if (data.type == CommunityType.CLUB.name ||
                              data.type == CommunityType.BASIC_GROUP.name ||
                              data.type == CommunityType.STUDY_GROUP.name) {
                            NavigationUtils.navigatePage(
                                context,
                                ViewClubPage(
                                  communityId: data.id ?? 0,
                                )).then((value) {
                              _cubit.getListFollow(
                                  isRefresh: true, type: widget.type);
                            });
                          }
                        },
                        checkStudyGroup: data.type == CommunityType.STUDY_GROUP.name,
                        checkMember: data.groupView?.myRoles != null,
                      );
                    },
                  ),
          ),
        ),
      ),
    );
  }

  void follow(FollowDataV2 data) {
    _cubit.follow(data.id!, data.userView!.isFollowing == true ? false : true);
    data.userView?.isFollowing =
        data.userView!.isFollowing == true ? false : true;
  }
}
