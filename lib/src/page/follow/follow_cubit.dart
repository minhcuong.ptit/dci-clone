import 'package:bloc/bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/response/follow_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';

import 'follow.dart';

class FollowCubit extends Cubit<FollowState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<FollowDataV2> listFollow = [];
  String? nextToken;
  int? _communityID;
  bool? isFollowers;
  int _unfollowCount = 0;
  bool disableScroll=false;
  int get unfollowCount => _unfollowCount;

  int get communityID =>
      _communityID ??
      0; // int? get mCommunityId => appPreferences.getInt(Const.COMMUNITY_ID);

  FollowCubit(
      {required this.repository,
      required this.graphqlRepository,
      required int communityID})
      : super(FollowInitial()) {
    _communityID = communityID;
  }

  void getListFollow({
    bool isRefresh = false,
    bool isLoadMore = false,
    required FollowType type,
  }) async {
    disableScroll=true;
    this.isFollowers = (type == FollowType.FOLLOWER);
    emit((isRefresh || isLoadMore) ? FollowLoading() : FollowInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listFollow.clear();
    }
    ApiResult<FollowsV2> followResult = await graphqlRepository.getFollows(
        isFollowers, communityID, Const.NETWORK_DEFAULT_LIMIT, nextToken);
    followResult.when(success: (FollowsV2 response) async {
      if (response.data != null && response.data?.isNotEmpty == true) {
        listFollow.addAll(response.data ?? []);
        nextToken = response.nextToken;
        disableScroll=false;
        emit(GetListFollowSuccess());
      }
      else  {
        disableScroll=false;
        emit(GetListFollowEmpty());
      }
      // if(listFollow.isEmpty){
      //   emit(GetListFollowEmpty());
      // }
    }, failure: (NetworkExceptions error) async {
      emit(FollowFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future<void> follow(int? communityId, bool isFollow) async {
    emit(FollowLoading());
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityId,
        action: isFollow ? Const.FOLLOW : Const.UNFOLLOW));
    result.when(success: (dynamic response) async {
      if (!isFollow) {
        _unfollowCount++;
      } else {
        _unfollowCount--;
      }
      emit(FollowSuccess("success"));
    }, failure: (NetworkExceptions error) async {
      emit(FollowFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
