import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/auth_user/profile_avatar/profile_avatar_page.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/back_icon_button.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pinput/pin_put/pin_put.dart';

import 'otp.dart';

class OtpPage extends StatefulWidget {
  final String phoneNumber;

  const OtpPage({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  late OtpCubit _cubit;
  String? lastPhoneNumber;

  late TextEditingController _otpController;

  @override
  void initState() {
    AppRepository appRepository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = OtpCubit(
        repository: appRepository, graphqlRepository: graphqlRepository);
    _cubit.countdown();
    _otpController = TextEditingController();
    lastPhoneNumber = appPreferences.getString(Const.PHONE);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<OtpCubit, OtpState>(
          listener: (context, state) {
            if (state is GetProfileSuccess) {
              String? status = state.status;
              if (status == Const.REGISTERED) {
                NavigationUtils.popToFirst(context);
                AuthenticationCubit authCubit =
                    BlocProvider.of<AuthenticationCubit>(context);
                authCubit.login();
              } else {
                NavigationUtils.replacePage(
                    context,
                    BlocProvider.value(
                        value: BlocProvider.of<AuthenticationCubit>(context),
                        child: ProfileAvatarPage()));
              }
              // NavigationUtils.popToFirst(context);
              // AuthenticationCubit authCubit =
              //     BlocProvider.of<AuthenticationCubit>(context);
              // authCubit.login();
            }
            if (state is OtpFailure) {
              Utils.showErrorSnackBar(context, state.error);
            }
          },
          builder: (
            BuildContext context,
            OtpState state,
          ) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, OtpState state) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: StackLoadingView(
        visibleLoading: state is OtpLoading,
        child: Container(
          padding: EdgeInsets.all(24.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 70.h,
                      ),
                      Row(
                        children: [
                          BackIconButton(),
                        ],
                      ),
                      SizedBox(
                        height: 45.h,
                      ),
                      Text(
                        R.string.verification_sent.tr(),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: R.color.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 24.sp,
                          height: 1.20833,
                        ),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        R.string.enter_your_otp.tr(),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: R.color.textGreyColor,
                          fontWeight: FontWeight.w400,
                          fontSize: 14.sp,
                          height: 1.35714,
                        ),
                      ),
                      SizedBox(
                        height: 35.h,
                      ),
                      buildEnterOtp(context),
                      SizedBox(height: 25.h),
                      buildResendButton(state, context),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
              buildContinueButton(state, context),
              SizedBox(height: Platform.isIOS ? 20.h : 10.h),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildContinueButton(OtpState state, BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: ButtonWidget(
          title: R.string.text_continue.tr(),
          backgroundColor:
              (state is OtpLoading || !Utils.isEmpty(_cubit.errorOtp))
                  ? R.color.gray
                  : R.color.primaryColor,
          onPressed: () =>
              (state is OtpLoading || !Utils.isEmpty(_cubit.errorOtp))
                  ? null
                  : _cubit.verifyOtp(widget.phoneNumber, _otpController.text)),
    );
  }

  Widget buildResendButton(OtpState state, BuildContext context) {
    return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RichText(
              text: TextSpan(
                text: R.string.resend_otp.tr() + "  ",
                style: TextStyle(
                  color: R.color.textTitleGray,
                  fontWeight: FontWeight.w500,
                  fontSize: 16.sp,
                  letterSpacing: 0.4,
                  height: 1.375,
                ),
                children: <TextSpan>[
                  TextSpan(
                      text: formatHHMMSS(_cubit.start),
                      style: TextStyle(
                        color: R.color.primaryColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 16.sp,
                        letterSpacing: 0.4,
                        height: 1.375,
                      )),
                ],
              ),
            ),
            IconButton(
                onPressed: _cubit.start > 0 || state is OtpLoading
                    ? null
                    : () {
                        _cubit.countdown();
                        _otpController.clear();
                      },
                icon: Icon(
                  Icons.refresh,
                  color: _cubit.start > 0 ? R.color.hintGray : R.color.black,
                  size: 20.h,
                ))
          ],
        ));
  }

  Widget buildEnterOtp(BuildContext context) {
    return PinPut(
      fieldsCount: 6,
      // onSubmit: (String pin) =>
      //     _cubit.verifyOtp(widget.phoneNumber, _otpController.text),
      controller: _otpController,
      autofocus: true,
      eachFieldHeight: 60.h,
      eachFieldWidth: 45.h,
      separator: SizedBox(
        width: 5,
      ),
      // eachFieldWidth: 40.h,
      onChanged: _cubit.validateOtp,
      textStyle: TextStyle(
        color: R.color.primaryColor,
        fontWeight: FontWeight.w500,
        fontSize: 27.sp,
      ),
      submittedFieldDecoration: BoxDecoration(
        border: Border.all(color: R.color.borderColor, width: 1),
        borderRadius: BorderRadius.circular(15.0),
      ),
      selectedFieldDecoration: BoxDecoration(
        border: Border.all(color: R.color.primaryColor, width: 1),
        borderRadius: BorderRadius.circular(15.0),
      ),
      followingFieldDecoration: BoxDecoration(
        border: Border.all(color: R.color.borderColor, width: 1),
        borderRadius: BorderRadius.circular(15.0),
      ),
    );
  }

  String formatHHMMSS(int seconds) {
    int hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    int minutes = (seconds / 60).truncate();

    String hoursStr = (hours).toString().padLeft(2, '0');
    String minutesStr = (minutes).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    if (hours == 0) {
      return "$minutesStr:$secondsStr";
    }

    return "$hoursStr:$minutesStr:$secondsStr";
  }
}
