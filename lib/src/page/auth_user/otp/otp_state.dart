import 'package:equatable/equatable.dart';

abstract class OtpState extends Equatable {
  OtpState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class OtpInitial extends OtpState {
  @override
  String toString() => 'OtpInitial';
}

class OtpLoading extends OtpState {
  @override
  String toString() => 'OtpLoading';
}

class OtpSuccess extends OtpState {
  final String? message;

  OtpSuccess({this.message});

  @override
  String toString() => 'OtpSuccess { message: $message }';
}

class GetProfileSuccess extends OtpState {
  final String? status;

  GetProfileSuccess(this.status);

  @override
  String toString() => 'GetProfileSuccess {status: $status}';
}

class OtpFailure extends OtpState {
  final String error;

  OtpFailure(this.error);

  @override
  String toString() => 'OtpFailure { error: $error }';
}

class ValidateErrorEmail extends OtpState {
  final String? error;

  ValidateErrorEmail(this.error);

  @override
  String toString() => 'ValidateErrorEmail { error: $error }';
}

class ValidateErrorPassword extends OtpState {
  final String? error;

  ValidateErrorPassword(this.error);

  @override
  String toString() => 'ValidateErrorPassword { error: $error }';
}
