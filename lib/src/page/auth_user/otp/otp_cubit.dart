import 'dart:async';
import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/login_request.dart';
import 'package:imi/src/data/network/request/submit_favorite_request.dart';
import 'package:imi/src/data/network/response/login_response.dart';
import 'package:imi/src/data/network/response/user_data.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import 'otp.dart';

class OtpCubit extends Cubit<OtpState> with Validators {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  String? errorOtp = "error";
  Timer? _timer;
  static const TIME_COUNT = 20;
  int start = TIME_COUNT;

  List<String> get listFavoriteCategory =>
      appPreferences.getListString(Const.FAVORITE_CATEGORY);

  List<String> get listFavoriteCommunity =>
      appPreferences.getListString(Const.FAVORITE_COMMUNITY);

  // String get phone => _pref.getString(Const.PHONE) ?? "";
  //
  // String get email => _pref.getString(Const.PHONE) ?? "";

  OtpCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(OtpInitial());

  void verifyOtp(String phone, String? otp) async {
    emit(OtpLoading());
    String? errorUsername = checkPhoneNumber(phone);
    String? errorOtp = checkPin(otp);
    if (!Utils.isEmpty(errorUsername)) {
      emit(OtpFailure(errorUsername!));
      return;
    }
    if (!Utils.isEmpty(errorOtp)) {
      emit(OtpFailure(errorOtp!));
      return;
    }
    ApiResult<dynamic> apiResult = await repository
        .auth(LoginRequest(phone: phone.replaceAll("+", ""), otp: otp));
    apiResult.when(success: (dynamic data) {
      LoginResponse response = LoginResponse.fromJson(data);
      appPreferences.setLoginResponse(response);
      appPreferences.setData(Const.PHONE, phone);
      emit(OtpSuccess());
      getProfile();
      submitFavorite();
    }, failure: (NetworkExceptions error) {
      emit(OtpFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getProfile() async {
    emit(OtpLoading());
    ApiResult<UserData> apiResult = await graphqlRepository.getMeStatus();
    apiResult.when(success: (UserData data) {
      appPreferences.setData(Const.ID, data.userUuid);
      appPreferences.setData(Const.FULL_NAME, data.fullName);
      emit(GetProfileSuccess(data.status));
    }, failure: (NetworkExceptions error) {
      emit(OtpFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void submitFavorite() async {
    emit(OtpLoading());
    try {
      List<int> listSelectedCategory =
          listFavoriteCategory.map((e) => int.parse(e)).toList();
      List<int> listSelectedCommunity =
          listFavoriteCommunity.map((e) => int.parse(e)).toList();
      SubmitFavoriteRequest request = SubmitFavoriteRequest(
        categories: listSelectedCategory,
        communities: listSelectedCommunity,
      );
      ApiResult<dynamic> submitFavoriteTask =
          await repository.submitFavorite(request);
      submitFavoriteTask.when(success: (dynamic data) async {
        emit(OtpInitial());
      }, failure: (NetworkExceptions error) async {
        emit(OtpFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } catch (e) {
      logger.e(e);
      emit(OtpInitial());
    }
  }

  void validateOtp(String? otp) {
    emit(OtpInitial());
    errorOtp = checkPin(otp);
    emit(ValidateErrorPassword(errorOtp));
  }

  void countdown() {
    emit(OtpLoading());
    start = TIME_COUNT;
    _timer = new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) {
        emit(OtpLoading());
        if (start == 0) {
          timer.cancel();
        } else {
          start--;
        }
        emit(OtpInitial());
      },
    );
    emit(OtpInitial());
  }

  @override
  Future<void> close() {
    _timer?.cancel();
    return super.close();
  }
}
