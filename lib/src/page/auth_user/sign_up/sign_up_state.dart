import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class SignUpState extends Equatable {
  SignUpState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialSignUpState extends SignUpState {
  @override
  String toString() {
    return 'InitialSignUpState{}';
  }
}

class SignUpLoading extends SignUpState {
  @override
  String toString() => 'SignUpLoading';
}

class SignUpSuccess extends SignUpState {
  final String? message;

  SignUpSuccess({this.message});

  @override
  String toString() => 'SignUpSuccess {$message}';
}

class SignUpFailure extends SignUpState {
  final String? error;

  SignUpFailure(this.error);

  @override
  String toString() => 'SignUpFailure { error: $error }';
}

class SelectedDateSuccess extends SignUpState {
  @override
  String toString() {
    return 'SelectedDateSuccess{}';
  }
}

class ValidateError extends SignUpState {
  @override
  String toString() {
    return 'ValidateError {}';
  }
}

class GetProvinceSuccess extends SignUpState {
  @override
  String toString() => 'GetProvinceSuccess';
}
