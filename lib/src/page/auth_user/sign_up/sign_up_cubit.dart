import 'dart:io';

import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/update_profile_request_v2.dart';
import 'package:imi/src/data/network/response/city_response.dart';
import 'package:imi/src/data/network/response/upload_list_url_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../data/network/response/list_membership_response.dart';
import '../../../data/network/response/membership_information_response.dart';
import '../../../utils/enum.dart';
import 'sign_up.dart';

class SignUpCubit extends Cubit<SignUpState> with Validators {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  String? errorEmail;
  String? errorName;
  String? errorDob;
  String? errorPin;
  String? errorProvince;
  DateTime? selectedDob;
  bool isSelectTerm = false;
  late ImagePicker _imagePicker;
  XFile? file;
  CityData? selectedProvince;
  String? _uploadUrl;
  List<CityData> listCity = [];
  bool selectedGenderMale =
      (appPreferences.getString(Const.GENDER) ?? '').toLowerCase() ==
          Const.MALE;
  String get status => appPreferences.getString(Const.USER_STATUS) ?? "";
  String get fullname => appPreferences.getString(Const.FULL_NAME) ?? "";
  String get email => appPreferences.getString(Const.EMAIL) ?? "";
  String get birthday => appPreferences.getString(Const.DOB) ?? "";
  String get province => appPreferences.getString(Const.PROVINCE) ?? "";
  String get avatar => appPreferences.getString(Const.AVATAR) ?? "";
  String get userId => appPreferences.getString(Const.ID) ?? "";
  String get phone => appPreferences.getString(Const.PHONE) ?? "";

  SignUpCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(InitialSignUpState()) {
    _imagePicker = ImagePicker();
  }

  void pickAvatar(bool isCamera) async {
    emit(SignUpLoading());
    try {
      file = await _imagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery,
      );
    } catch (e) {
      logger.e(e);
      emit(InitialSignUpState());
    }
    emit(InitialSignUpState());
  }

  void checkTerm() {
    emit(InitialSignUpState());
    isSelectTerm = !isSelectTerm;
    emit(ValidateError());
  }

  void validateEmail(String? email) {
    emit(InitialSignUpState());
    errorEmail = checkEmail(email);
    emit(ValidateError());
  }

  void validateName(String? name) {
    emit(InitialSignUpState());
    errorName = checkName(name);
    emit(ValidateError());
  }

  void validatePin(String? pin) {
    emit(InitialSignUpState());
    errorPin = checkPin(pin);
    emit(ValidateError());
  }

  void selectGender(bool? isMale) {
    emit(SignUpLoading());
    selectedGenderMale = isMale == true;
    emit(InitialSignUpState());
  }

  void selectDob(DateTime date) {
    emit(SignUpLoading());
    selectedDob = date;
    errorDob = null;
    emit(SelectedDateSuccess());
  }

  void selectCity(CityData? city) {
    emit(SignUpLoading());
    this.selectedProvince = city;
    if (Utils.isEmpty(selectedProvince)) {
      errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
    } else {
      errorProvince = null;
    }
    emit(InitialSignUpState());
  }

  void signUp(String name, String email, String pinCode) async {
    emit(SignUpLoading());
    errorName = checkName(name);
    errorEmail = checkEmail(email);
    errorPin = checkPin(pinCode);
    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorPin)) {
      emit(ValidateError());
      return;
    }
    if (Utils.isEmpty(selectedDob)) {
      errorDob = R.string.please_enter_your_dob.tr();
      emit(ValidateError());
      return;
    } else {
      errorDob = null;
    }
    // if (Utils.isEmpty(selectedProvince)) {
    //   errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
    //   emit(ValidateError());
    //   return;
    // } else {
    //   errorProvince = null;
    // }
    if (file != null) {
      await getUploadUrl(name, email, pinCode);
    } else {
      registerProfile(name, email, pinCode);
    }
  }

  void registerProfile(String name, String email, String pinCode,
      {String? keyImage}) async {
    emit(SignUpLoading());
    ApiResult<dynamic> registerResult = await repository.updateProfile(
        UpdateProfileRequestV2(
            userUuid: userId,
            status: Const.REGISTERED,
            fullName: name,
            email: email,
            pinCode: pinCode,
            phone: phone,
            gender: selectedGenderMale ? Const.MALE : Const.FEMALE,
            provinceId: selectedProvince?.cityId,
            avatarUrl: keyImage,
            birthday: selectedDob?.toIso8601String()));
    registerResult.when(success: (dynamic data) {
      emit(SignUpSuccess());
      appPreferences.setData(Const.FULL_NAME, name);
      appPreferences.setData(Const.EMAIL, email);
      appPreferences.setData(Const.PIN_CODE, pinCode);
      appPreferences.setData(Const.PROVINCE, selectedProvince?.cityName);
      appPreferences.setData(Const.DOB,
          DateUtil.parseDateToString(selectedDob, Const.DATE_FORMAT));
      appPreferences.setData(
          Const.GENDER, selectedGenderMale ? Const.MALE : Const.FEMALE);
    }, failure: (NetworkExceptions error) {
      emit(SignUpFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getCity() async {
    emit(SignUpLoading());
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListProvince();
    apiResult.when(success: (List<CityData> data) async {
      listCity = data;
      emit(GetProvinceSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(SignUpFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future getUploadUrl(String name, String email, String pinCode) async {
    emit(SignUpLoading());
    ApiResult<List<UploadListUrlResponse>> listUrlResult =
        await repository.getListUploadUrl([file?.name ?? ""]);
    listUrlResult.when(success: (List<UploadListUrlResponse> response) async {
      String? key = response.first.key;
      String? url = response.first.url;
      if (url != null) {
        await repository.uploadImage(url, File(file!.path));
        registerProfile(name, email, pinCode, keyImage: key);
      } else {
        registerProfile(name, email, pinCode);
      }
    }, failure: (NetworkExceptions error) {
      emit(SignUpFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshSignUp() {
    emit(SignUpLoading());
    emit(InitialSignUpState());
  }

  List<CityData> searchCity(String? name) {
    if (Utils.isEmpty(name)) return listCity;
    return listCity
        .where(
          (e) => Utils.convertVNtoText(e.cityName ?? "").contains(
            Utils.convertVNtoText(name!),
          ),
        )
        .toList();
  }

  MyMembership? myMembership;
  MembershipInformation? membershipInformation;

  void getListMembership() async {
    emit(InitialSignUpState());
    ApiResult<ListMembershipResponse> listMembershipTask =
        await graphqlRepository.getListMembership();
    listMembershipTask.when(success: (ListMembershipResponse data) async {
      data.myMembership ??
          [].forEach((element) {
            if (element.membershipType == MembershipType.KTA &&
                element.userPackageId != null) {
              myMembership = element;
              getMembershipInfo(element);
              return;
            }
            emit(InitialSignUpState());
          });
    }, failure: (NetworkExceptions error) async {
      emit(SignUpFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getMembershipInfo(MyMembership myMembership) async {
    emit(InitialSignUpState());
    List<ApiResult<Object>> apiResults = await Future.wait([
      graphqlRepository.getMembershipInformation(
          membershipId: myMembership.informationId,
          membershipType: myMembership.membershipType),
      repository.getDetailPackage(myMembership.packageCode)
    ]);
    for (int i = 0; i < apiResults.length; i++) {
      ApiResult apiResult = apiResults[i];
      apiResult.when(success: (dynamic data) async {
        if (data is MembershipInformation) {
          membershipInformation = data;
        }
        emit(SignUpSuccess());
      }, failure: (NetworkExceptions error) async {
        emit(SignUpFailure(NetworkExceptions.getErrorMessage(error)));
      });
    }
  }
}
