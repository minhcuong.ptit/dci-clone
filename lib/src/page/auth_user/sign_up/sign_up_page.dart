import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/dropdown_column_widget.dart';
import 'package:imi/src/widgets/search_list_data_popup.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../widgets/avatar_widget.dart';
import '../../../widgets/term_policy_widget.dart';
import 'sign_up.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  late TextEditingController _pinController = TextEditingController();
  FocusNode _emailFocus = FocusNode();
  FocusNode _dobFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();
  late SignUpCubit _cubit;

  @override
  void initState() {
    AppRepository appRepository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    _cubit = SignUpCubit(
        repository: appRepository, graphqlRepository: appGraphqlRepository);
    _cubit.getCity();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: appBar(context, '',
            backgroundColor: Colors.transparent,
            iconColor: R.color.white,
            titleColor: R.color.white,
            centerTitle: true),
        body: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<SignUpCubit, SignUpState>(
              listener: (context, state) {
            if (state is SelectedDateSuccess)
              _dobController.text = DateUtil.parseDateToString(
                  _cubit.selectedDob, Const.DATE_FORMAT);
            if (state is SignUpFailure) {
              Utils.showErrorSnackBar(context, state.error!);
            }
            if (state is SignUpSuccess) {
              NavigationUtils.popToFirst(context);
              AuthenticationCubit authCubit =
                  BlocProvider.of<AuthenticationCubit>(context);
              authCubit.welcomeToHome();
            }
          }, builder: (
            BuildContext context,
            SignUpState state,
          ) {
            return buildPage(context, state);
          }),
        ));
  }

  Widget buildPage(BuildContext context, SignUpState state) {
    var _headerMaxHeight = context.width * (286.0 / 361.0);
    var _paddingTop = MediaQuery.of(context).viewPadding.top;
    return KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
      return GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Stack(
          children: [
            StackLoadingView(
              visibleLoading: state is SignUpLoading,
              child: ListView(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  padding: EdgeInsets.only(bottom: 24.h),
                  children: <Widget>[
                    Container(
                      height: _headerMaxHeight,
                      width: double.infinity,
                      child: Stack(
                        children: [
                          Image.asset(
                            R.drawable.bg_profile,
                            height: _headerMaxHeight,
                            width: double.infinity,
                            fit: BoxFit.fitWidth,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: _paddingTop + 48.h),
                            child: Column(
                              children: [
                                Text(
                                  R.string.profile_information.tr(),
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyBold
                                      .apply(color: R.color.white)
                                      .copyWith(
                                          fontSize: 17.sp,
                                          fontWeight: FontWeight.w700),
                                ),
                                SizedBox(
                                  height: 30.h,
                                ),
                                avatarWidget(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.h),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24.0),
                      child: Column(
                        children: [
                          buildGender(context, state),
                          buildEnterName(context, state),
                          buildEnterEmail(context, state),
                          buildEnterDob(context, state),
                          //buildDropdownProvince(context, state),
                          buildEnterPIN(context),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 44.h),
                            alignment: Alignment.center,
                            child: TermPolicyWidget(
                              checkCallback: () {
                                _cubit.checkTerm();
                              },
                            ),
                          ),
                          buildSignUp(state, context),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).padding.bottom,
                    )
                  ]),
            ),
          ],
        ),
      );
    });
  }

  Widget buildSignUp(SignUpState state, BuildContext context) {
    bool isDisable = state is SignUpLoading || !_cubit.isSelectTerm;
    return ButtonWidget(
        textSize: 16.sp,
        height: 48.h,
        uppercaseTitle: true,
        backgroundColor: R.color.primaryColor,
        padding: EdgeInsets.only(top: 5.h),
        title: R.string.create_account.tr(),
        onPressed: isDisable ? null : () => signUp());
  }

  Widget buildEnterPIN(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldWidget(
            autoFocus: false,
            controller: _pinController,
            hintText: '${R.string.pin_number.tr()}*',
            textInputAction: TextInputAction.go,
            inputFormatters: [
              LengthLimitingTextInputFormatter(20),
            ],
            isPassword: true,
            keyboardType: TextInputType.text,
            onChanged: _cubit.validatePin,
            errorText: _cubit.errorPin,
            errorTextStyle: Theme.of(context).textTheme.bodySmallText.copyWith(
              color: R.color.red,fontSize: 10
          )),
          SizedBox(
            height: 4.h,
          ),
          Text(
            R.string.pin_number_description.tr(),
            style: Theme.of(context)
                .textTheme
                .labelMiniText
                .copyWith(color: R.color.shadesGray),
          )
        ],
      ),
    );
  }

  Widget buildEnterName(BuildContext context, SignUpState state) {
    return Padding(
      padding: EdgeInsets.only(top: 14.h),
      child: TextFieldWidget(
        autoFocus: false,
        // isEnable: !(state is SignUpLoading),
        controller: _nameController,
        inputFormatters: [LengthLimitingTextInputFormatter(255)],
        focusNode: _nameFocus,
        isRequired: true,
        hintText: '${R.string.full_name.tr()}*',
        errorText: _cubit.errorName,
        errorTextStyle: Theme.of(context).textTheme.bodySmallText.copyWith(
            color: R.color.red,fontSize: 10
        ),
        onChanged: _cubit.validateName,
        onSubmitted: state is SignUpLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _nameFocus, _emailFocus),
      ),
    );
  }

  Widget buildEnterEmail(BuildContext context, SignUpState state) {
    return Padding(
      padding: EdgeInsets.only(top: 24.h),
      child: TextFieldWidget(
        autoFocus: false,
        // isEnable: !(state is SignUpLoading),
        controller: _emailController,
        focusNode: _emailFocus,
        keyboardType: TextInputType.emailAddress,
        isRequired: true,
        inputFormatters: [LengthLimitingTextInputFormatter(255)],
        hintText: '${R.string.email.tr()}*',
        errorText: _cubit.errorEmail,
        errorTextStyle: Theme.of(context).textTheme.bodySmallText.copyWith(
            color: R.color.red,fontSize: 10
        ),
        onChanged: _cubit.validateEmail,
        onSubmitted:
            state is SignUpLoading ? null : (_) => Utils.hideKeyboard(context),
      ),
    );
  }

  Widget buildGender(BuildContext context, SignUpState state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${R.string.gender.tr()}*',
          style: Theme.of(context)
              .textTheme
              .labelSmallText
              .copyWith(color: R.color.shadesGray),
        ),
        SizedBox(height: 4.h),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Radio(
                    value: true,
                    groupValue: _cubit.selectedGenderMale,
                    activeColor: R.color.black,
                    onChanged: (bool? value) => _cubit.selectGender(value),
                    visualDensity: const VisualDensity(
                      horizontal: VisualDensity.minimumDensity,
                      vertical: VisualDensity.minimumDensity,
                    ),
                  ),
                  Text(
                    R.string.male.tr(),
                    style: Theme.of(context).textTheme.bodySmallText,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Radio(
                    value: false,
                    groupValue: _cubit.selectedGenderMale,
                    activeColor: R.color.black,
                    onChanged: (bool? value) => _cubit.selectGender(value),
                    visualDensity: const VisualDensity(
                      horizontal: VisualDensity.minimumDensity,
                      vertical: VisualDensity.minimumDensity,
                    ),
                  ),
                  Text(
                    R.string.female.tr(),
                    style: Theme.of(context).textTheme.bodySmallText,
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget buildEnterDob(BuildContext context, SignUpState state) {
    return Padding(
      padding: EdgeInsets.only(top: 20.h),
      child: TextFieldWidget(
          autoFocus: false,
          controller: _dobController,
          readOnly: true,
          isRequired: true,
          // will disable paste operation
          onTap: pickDob,
          // hintText: R.string.hint_dob.tr(),
          textInputAction: TextInputAction.next,
          keyboardType: TextInputType.text,
          hintText: '${R.string.dob.tr()}*',
          errorText: _cubit.errorDob,
          suffixIcon: GestureDetector(
            onTap: pickDob,
            child: Icon(
              CupertinoIcons.calendar,
              color: R.color.black,
              size: 20.h,
            ),
          )),
    );
  }

  void pickDob() {
    DatePicker.showDatePicker(context,
        minTime: DateTime(1900, 1, 1),
        maxTime: DateTime.now(),
        showTitleActions: true,
        locale: appPreferences.locale,
        currentTime: _cubit.selectedDob, onConfirm: (date) {
      _cubit.selectDob(date);
    });
  }

  Widget buildDropdownProvince(BuildContext context, SignUpState state) {
    List<String> listProvince =
        _cubit.listCity.map((e) => e.cityName ?? "").toList();
    return Padding(
      padding: EdgeInsets.only(top: 20.h),
      child: DropdownColumnWidget(
        isRequired: false,
        onTap: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listProvince,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.province.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectCity(_cubit.listCity[index]);
                  },
                );
              });
        },
        hintText: R.string.province.tr(),
        labelText: R.string.province.tr(),
        currentValue: _cubit.selectedProvince?.cityName ?? _cubit.province,
        listData: listProvince,
        errorText: _cubit.errorProvince,
        selectedValue: (text) {},
      ),
    );
  }

  Widget avatarWidget() {
    return Align(
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Stack(
            children: [
              _cubit.file != null
                  ? CircleAvatar(
                      radius: 60.h,
                      backgroundColor: R.color.white,
                      backgroundImage: FileImage(File(_cubit.file!.path)))
                  : _cubit.avatar != ""
                      ? AvatarWidget(
                          size: 80.h,
                          avatar: _cubit.avatar,
                        )
                      : CircleAvatar(
                          radius: 60.h,
                          backgroundColor: R.color.white,
                          backgroundImage: AssetImage(_cubit.selectedGenderMale
                              ? R.drawable.ic_avatar_male
                              : R.drawable.ic_avatar_female),
                        ),
              Positioned(
                  right: 0,
                  bottom: 0,
                  child: InkWell(
                      onTap: () {
                        showBottomSheet();
                      },
                      child: Image.asset(R.drawable.ic_camera_profile)))
            ],
          )
        ],
      ),
    );
  }

  void showBottomSheet() {
    showBarModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(24.h),
          decoration: BoxDecoration(
            color: R.color.white,
          ),
          child: Column(
            children: [
              SizedBox(
                height: 10.h,
              ),
              InkWell(
                onTap: () {
                  _cubit.pickAvatar(true);
                  NavigationUtils.pop(context);
                },
                child: buildButton(
                  title: R.string.take_a_photo.tr(),
                  icon: R.drawable.ic_camera,
                ),
              ),
              SizedBox(height: 16.h),
              Container(height: 1.h, color: R.color.lightestGray),
              SizedBox(height: 16.h),
              InkWell(
                onTap: () {
                  _cubit.pickAvatar(false);
                  NavigationUtils.pop(context);
                },
                child: buildButton(
                  title: R.string.choose_a_photo.tr(),
                  icon: R.drawable.ic_gallery,
                ),
              ),
              SizedBox(height: 30.h),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildButton({String? title, required String icon}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Image.asset(icon, width: 20.h),
        SizedBox(width: 20.w),
        Expanded(
          child: Text(
            title ?? "",
            style: Theme.of(context).textTheme.headline4,
            textAlign: TextAlign.start,
            maxLines: 1,
          ),
        ),
      ],
    );
  }

  void signUp() {
    _cubit.signUp(_nameController.text.trim(), _emailController.text.trim(),
        _pinController.text.trim());
  }
}
