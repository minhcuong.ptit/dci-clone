import 'package:collection/src/iterable_extensions.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/login_request.dart';
import 'package:imi/src/data/network/response/country.dart';
import 'package:imi/src/data/network/response/nationality_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../data/network/response/login_response.dart';
import '../../../data/network/response/person_profile.dart';
import '../../../widgets/search_list_data_popup.dart';
import 'login.dart';

class LoginCubit extends Cubit<LoginState> with Validators {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  String? errorPhoneNumber;
  String? errorPin;
  String? errorApi;
  String? errorAccount;
  List<Country> nationalities = [];
  Country? selectedCountry;
 bool getCountrySuccess=false;
  LoginCubit({required this.appRepository, required this.graphqlRepository})
      : super(LoginInitial()) {
    // getNationality();
  }

  bool get canLogin => (errorPin ?? errorPhoneNumber) == null;

  void getNationality(context) async {
    emit(LoginLoading());
    try {
      ApiResult<List<Country>> apiResult = await appRepository.getCountries();
      apiResult.when(success: (List<Country> data) async {
        nationalities = data;
        selectedCountry = nationalities.firstWhereOrNull(
            (element) => element.phonePrefix == Const.DEFAULT_COUNTRY_CODE);
        getCountrySuccess=true;
        showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return SearchListDataPopup(
                listData: nationalities
                    .map((e) => "(${e.phonePrefix}) - ${e.name}")
                    .toList(),
                hintText: R.string.search.tr(),
                defaultIndex: nationalities.indexWhere(
                        (element) => element == selectedCountry),
                onSelect: (int index) {
                  selectCountryCode(nationalities[index]);
                },
              );
            });
        emit(LoginGetNationalitySuccess());
     },
          failure: (NetworkExceptions error) async {
        emit(LoginFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } catch (e) {
      emit(LoginFailure(e.toString()));
    }
    emit(LoginInitial());
  }

  void selectCountryCode(Country country) {
    emit(LoginLoading());
    this.errorApi = null;
    errorAccount = null;
    this.selectedCountry = country;
    emit(LoginInitial());
  }

  void verifyOtp(String? phone, String? pin) async {
    String? countryCode = selectedCountry?.countryCode;
    if (countryCode == null) {
      countryCode='+84';
    }
    this.errorApi = null;
    errorAccount = null;
    emit(LoginLoading());
    String? errorPhone = checkPhoneNumber("$countryCode$phone");
    if (!Utils.isEmpty(errorPhone)) {
      emit(LoginFailure(errorPhone!));
      return;
    }
    phone = Utils.getPhoneNumber(phone, countryCode);
    ApiResult<dynamic> apiResult = await appRepository.auth(LoginRequest(
      phone: phone,
      pin: pin,
    ));
    apiResult.when(success: (dynamic data) {
      LoginResponse response = LoginResponse.fromJson(data);
      appPreferences.setLoginResponse(response);
      appPreferences.setData(Const.PHONE, phone);
      appPreferences.setData(Const.PIN_CODE, pin);
      _getProfile();
    }, failure: (NetworkExceptions error) {
      if (error is UnauthorizedRequest) {
        if (error.code == ServerError.user_is_banned_error)
          errorAccount =
              R.string.ban_description.tr(args: [error.reason ?? ""]);
        else
          errorApi = R.string.verify_phone_number_pin_wrong.tr();
      } else if (error is BadRequest &&
          error.code == ServerError.user_is_banned_error) {
        errorAccount = R.string.ban_description.tr(args: [error.message ?? ""]);
      } else {
        errorApi = NetworkExceptions.getErrorMessage(error);
      }
      emit(LoginFailure(""));
    });
  }

  void validatePhone(String? phone) {
    String? countryCode = selectedCountry?.countryCode;
    if (countryCode == null) {
      return;
    }
    emit(LoginInitial());
    if (Utils.isEmpty(phone)) {
      errorPhoneNumber = R.string.please_enter_phone_number.tr();
      emit(ValidateErrorEmail(errorPhoneNumber));
      return;
    }
    phone = Utils.getPhoneNumber(phone, countryCode);
    errorPhoneNumber = checkPhoneNumber(phone);
    emit(ValidateErrorEmail(errorPhoneNumber));
  }

  void validatePin(String? pin) {
    emit(LoginLoading());
    errorPin = checkPin(pin);
    errorApi = null;
    errorAccount = null;
    emit(LoginInitial());
  }

  void logout() {
    emit(LoginLoading());
    appPreferences.clearData();
    emit(ValidateErrorEmail(errorPhoneNumber));
  }

  void _getProfile() async {
    ApiResult<PersonProfile> apiResult =
        await graphqlRepository.getMyPersonalProfile();
    apiResult.when(success: (PersonProfile data) {
      appPreferences.setPersonProfile(data);
      graphqlRepository.getMyProfile().then((value) {
        value.when(success: (data1) {
          graphqlRepository
              .getCommunityV2(data1.myCommunity?.id ?? 0)
              .then((value) {
            value.when(success: (data2) {
              appPreferences.saveCommunityV2(data2.communityV2);
              emit(LoginSuccess(data.status));
            }, failure: (e) {
              emit(LoginFailure(NetworkExceptions.getErrorMessage(e)));
            });
          });
        }, failure: (e) {
          emit(LoginFailure(NetworkExceptions.getErrorMessage(e)));
        });
      });
      emit(LoginSuccess(data.status));
    }, failure: (NetworkExceptions error) {
      appPreferences.clearData();
      emit(LoginFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
