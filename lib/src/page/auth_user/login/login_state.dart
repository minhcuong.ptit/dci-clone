import 'package:equatable/equatable.dart';

import '../../../utils/enum.dart';

abstract class LoginState extends Equatable {
  LoginState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {
  @override
  String toString() => 'LoginInitial';
}

class LoginLoading extends LoginState {
  @override
  String toString() => 'LoginLoading';
}

class LoginSuccess extends LoginState {
  final PersonProfileStatus status;

  LoginSuccess(this.status);

  @override
  String toString() => 'LoginSuccess { $status }';

  @override
  List<Object> get props => [status];
}

class LoginFailure extends LoginState {
  final String error;

  LoginFailure(this.error);

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ValidateErrorEmail extends LoginState {
  final String? error;

  ValidateErrorEmail(this.error);

  @override
  String toString() => 'ValidateErrorEmail { error: $error }';
}

class ValidateErrorPassword extends LoginState {
  final String? error;

  ValidateErrorPassword(this.error);

  @override
  String toString() => 'ValidateErrorPassword { error: $error }';
}

class LoginGetNationalitySuccess extends LoginState {
  @override
  String toString() => 'LoginGetNationalitySuccess';
}
