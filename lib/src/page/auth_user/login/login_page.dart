import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';
import 'package:imi/src/widgets/background_page.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/search_list_data_popup.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

import '../../../utils/const.dart';
import '../../../utils/enum.dart';
import '../../../widgets/custom_appbar.dart';
import '../../../widgets/search_list_nationality_popup.dart';
import '../../pin_code/forgot_pin/forgot_pin_page.dart';
import '../sign_up/sign_up.dart';
import 'login.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback? onRefresh;

  const LoginPage({Key? key, this.onRefresh}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late LoginCubit _cubit;

  late TextEditingController _phoneController;
  late TextEditingController _pinController;
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  @override
  void initState() {
    AppRepository appRepository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = LoginCubit(
        appRepository: appRepository, graphqlRepository: graphqlRepository);
    _phoneController = TextEditingController();
    _pinController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _phoneFocus.dispose();
    _passwordFocus.dispose();
    _phoneController.dispose();
    _pinController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Utils.hideKeyboard(context);
      },
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<LoginCubit, LoginState>(
          listener: (context, state) {
            if (state is LoginSuccess) {
              switch (state.status) {
                // new account, to go Sign Up page
                case PersonProfileStatus.INIT:
                  NavigationUtils.navigatePage(context, const SignUpPage());
                  return;
                // existing account, go to Home page
                case PersonProfileStatus.REGISTERED:
                  if (widget.onRefresh != null) {
                    widget.onRefresh!();
                  }
                  BlocProvider.of<AuthenticationCubit>(context).login();
                  return;
                default:
                  return;
              }
            }
            if (state is LoginFailure) {
              Utils.showSnackBar(context, state.error);
            }
          },
          builder: (
            BuildContext context,
            LoginState state,
          ) {
            return StackLoadingView(
              visibleLoading: state is LoginLoading,
              child: BackgroundPage(
                  background: R.drawable.bg_welcome,
                  child: buildPage(context, state)),
            );
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, LoginState state) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: R.color.black.withOpacity(0.4),
      appBar: appBar(context, R.string.login.tr(),
          textStyle: Theme.of(context)
              .textTheme
              .subHeading1
              .apply(color: R.color.white)
              .copyWith(fontSize: 20.sp, fontWeight: FontWeight.w700),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          leadingWidget: IconButton(
              icon: Icon(
                Icons.arrow_back_ios_new,
                color: R.color.white,
                size: 24,
              ),
              onPressed: () {
                BlocProvider.of<AuthenticationCubit>(context).openWelcome();
              },
            ),
          iconColor: R.color.white),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 200.h,
            ),
            _inputFields(),
            SizedBox(
              height: 12.h,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  NavigationUtils.navigatePage(
                      context, const ForgotPinPage(type: ForgotPinType.FORGOT_PIN));
                },
                child: Padding(
                  padding: EdgeInsets.only(right: 24.w),
                  child: Text(
                    R.string.forgot_password.tr(),
                    style: TextStyle(
                        color: R.color.white,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 36.w),
              child: ButtonWidget(
                title: R.string.login.tr(),
                height: 48.h,
                textSize: 16.sp,
                onPressed: _cubit.canLogin &&
                        _pinController.text.isNotEmpty &&
                        _phoneController.text.length <= 13 &&
                        _phoneController.text.length >= 9
                    ? login
                    : () {},
                backgroundColor: _cubit.canLogin &&
                        _pinController.text.isNotEmpty &&
                        _phoneController.text.length <= 13 &&
                        _phoneController.text.length >= 9
                    ? R.color.secondaryButtonColor
                    : R.color.lightGray,
              ),
            ),
            SizedBox(
              height: 12.h,
            ),
            Align(
              alignment: Alignment.center,
              child: IntrinsicWidth(
                child: Row(
                  children: [
                    Text(
                      "${R.string.register_description.tr()}  ",
                      style: TextStyle(
                          color: R.color.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 14.sp),
                    ),
                    InkWell(
                      onTap: (() {
                        NavigationUtils.navigatePage(
                            context,
                            BlocProvider.value(
                              value:
                                  BlocProvider.of<AuthenticationCubit>(context),
                              child:const ForgotPinPage(
                                type: ForgotPinType.SIGN_UP,
                              ),
                            ));
                      }),
                      child: Text(
                        R.string.register.tr(),
                        style: TextStyle(
                            color: R.color.orange,
                            fontWeight: FontWeight.w700,
                            fontSize: 14.sp),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _inputFields() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: R.color.white,
          borderRadius: const BorderRadius.all(Radius.circular(8))),
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      padding: EdgeInsets.all(16.w),
      child: Column(
        children: [
          buildEnterPhoneNumber(context),
          SizedBox(
            height: 16.h,
          ),
          buildEnterPassword(context)
        ],
      ),
    );
  }

  Widget buildEnterPhoneNumber(BuildContext context) {
    return Column(
      children: [
        TextFieldWidget(
          autoFocus: false,
          controller: _phoneController,
          focusNode: _phoneFocus,
          isRequired: false,
          titleText: R.string.phone_number.tr(),
          hintText: R.string.phone_number_hint.tr(),
          textInputAction: TextInputAction.next,
          inputFormatters: [
            FilteringTextInputFormatter.allow(Validators.numberRegex),
            LengthLimitingTextInputFormatter(12)
          ],
          keyboardType: TextInputType.phone,
          icon: countryDropDown(context),
          customInputStyle: const TextStyle().copyWith(
              color: R.color.black, fontSize: 14, height: 24 / 14,fontWeight: FontWeight.w400),
          // icon: icUsername,
          onChanged: _cubit.validatePhone,
          onSubmitted: (_) {
            _passwordFocus.requestFocus();
          },
        ),
        if (_cubit.errorAccount != null)
          Text(
            _cubit.errorAccount!,
            style: Theme.of(context).textTheme.bodySmallText.copyWith(
                  color: R.color.red,
                ),
            textAlign: TextAlign.center,
          ),
      ],
    );
  }

  Widget countryDropDown(context) {
    return GestureDetector(
        onTap: () {
        _cubit.getCountrySuccess?
        showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return SearchListDataPopup(
                listData: _cubit.nationalities
                    .map((e) => "(${e.phonePrefix}) - ${e.name}")
                    .toList(),
                hintText: R.string.search.tr(),
                defaultIndex: _cubit.nationalities.indexWhere(
                        (element) => element == _cubit.selectedCountry),
                onSelect: (int index) {
                  _cubit.selectCountryCode(_cubit.nationalities[index]);
                },
              );
            }): _cubit.getNationality(context)
        ;
       // _cubit.getCountrySuccess?

        },
        child: Container(
          margin: EdgeInsets.zero,
          child: Padding(
            padding: const EdgeInsets.only(top:2.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(width: 4.w),
                Text(
                _cubit.selectedCountry!=null?_cubit.selectedCountry?.phonePrefix??'+84':  Const.DEFAULT_COUNTRY_CODE,
                  textAlign: TextAlign.center,
                  style: const TextStyle().copyWith(
                      color: R.color.textBlack, fontSize: 14, height: 24 / 14),
                ),
                SizedBox(width: 8.w),
                Container(
                  margin:const EdgeInsets.only(top: 3),
                  color: R.color.lightShadesGray,
                  width: 1.w,
                  height: 18,
                ),
                SizedBox(width: 8.w),
              ],
            ),
          ),
        ));
  }

  Widget buildEnterPassword(BuildContext context) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _pinController,
      focusNode: _passwordFocus,
      hintText: "******",
      titleText: R.string.password.tr(),
      textInputAction: TextInputAction.go,
      inputFormatters: [
        LengthLimitingTextInputFormatter(20),
      ],
      isPassword: true,
      keyboardType: TextInputType.text,
      onChanged: _cubit.validatePin,
      errorText: _cubit.errorApi ?? _cubit.errorPin,
        errorTextStyle: Theme.of(context).textTheme.bodySmallText.copyWith(
            color: R.color.red,fontSize: 12
        ),
      onSubmitted: (_) {
        login();
      },
    );
  }

  void login() {
    Utils.hideKeyboard(context);
    _cubit.verifyOtp(_phoneController.text.trim(), _pinController.text.trim());
  }
}
