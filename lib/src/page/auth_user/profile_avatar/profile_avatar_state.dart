import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ProfileAvatarState extends Equatable {
  ProfileAvatarState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialProfileAvatarState extends ProfileAvatarState {}

class ProfileAvatarLoading extends ProfileAvatarState {
  @override
  String toString() => 'ProfileAvatarLoading';
}

class ProfileAvatarSuccess extends ProfileAvatarState {
  @override
  String toString() {
    return 'ProfileAvatarSuccess';
  }
}

class ProfileAvatarFailure extends ProfileAvatarState {
  final String error;

  ProfileAvatarFailure(this.error);

  @override
  String toString() => 'ProfileAvatarFailure { error: $error }';
}
