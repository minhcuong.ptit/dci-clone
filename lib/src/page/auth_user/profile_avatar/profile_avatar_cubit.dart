import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/logger.dart';

import 'profile_avatar.dart';

class ProfileAvatarCubit extends Cubit<ProfileAvatarState> {
  final AppRepository repository;
  late ImagePicker _imagePicker;
  XFile? file;

  ProfileAvatarCubit(this.repository) : super(InitialProfileAvatarState()) {
    _imagePicker = ImagePicker();
  }

  void pickAvatar(bool isCamera) async {
    emit(ProfileAvatarLoading());
    try {
      file = await _imagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery,
      );
    } catch (e) {
      logger.e(e);
    }
    emit(InitialProfileAvatarState());
  }
}
