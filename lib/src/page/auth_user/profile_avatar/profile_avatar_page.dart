import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/auth_user/profile_avatar/profile_avatar.dart';
import 'package:imi/src/page/auth_user/sign_up/sign_up_page.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';

class ProfileAvatarPage extends StatefulWidget {
  const ProfileAvatarPage({Key? key}) : super(key: key);

  @override
  _ProfileAvatarPageState createState() => _ProfileAvatarPageState();
}

class _ProfileAvatarPageState extends State<ProfileAvatarPage> {
  late ProfileAvatarCubit _cubit;

  @override
  void initState() {
    AppRepository repository = AppRepository();
    _cubit = ProfileAvatarCubit(repository);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<ProfileAvatarCubit, ProfileAvatarState>(
            listener: (context, state) {
              if (state is ProfileAvatarFailure)
                Utils.showErrorSnackBar(context, state.error);
            },
            builder: (context, state) {
              return buildPage(state);
            },
          ),
        ),
      ),
    );
  }

  Widget buildPage(ProfileAvatarState state) {
    return StackLoadingView(
      visibleLoading: state is ProfileAvatarLoading,
      child: Column(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(24.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text(
                      R.string.create_your_profile.tr(),
                      style: TextStyle(
                        fontSize: 24.sp,
                        fontWeight: FontWeight.bold,
                        color: R.color.black,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.h),
                  Container(
                    width: 200.w,
                    child: Text(
                      R.string.choose_or_take.tr(),
                      style: TextStyle(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w400,
                        color: R.color.textGreyColor,
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                  ),
                  SizedBox(height: 100.h),
                  avatarWidget(),
                  SizedBox(height: 50),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(24.h),
            child: ButtonWidget(
                title: R.string.next.tr(),
                onPressed: () {
                  // NavigationUtils.navigatePage(
                  //     context,
                  //     BlocProvider.value(
                  //         value: BlocProvider.of<AuthenticationCubit>(context),
                  //         child: SignUpPage(
                            // avatar: _cubit.file,
                          // )));
                }),
          ),
          SizedBox(height: 20.h),
        ],
      ),
    );
  }

  Widget avatarWidget() {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        _cubit.file != null
            ? CircleAvatar(
                radius: 100.h,
                backgroundColor: R.color.white,
                backgroundImage: FileImage(File(_cubit.file!.path)))
            : CircleAvatar(
                radius: 100.h,
                backgroundColor: R.color.white,
                backgroundImage: AssetImage(R.drawable.ic_avatar),
              ),
        Positioned(
          right: 0,
          bottom: 0,
          child: GestureDetector(
            onTap: () {
              showBottomSheet();
            },
            child: Container(
              width: 66.h,
              height: 66.h,
              padding: EdgeInsets.all(20.h),
              decoration: BoxDecoration(
                color: R.color.white,
                shape: BoxShape.circle,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: R.color.shadowColor,
                      blurRadius: 15,
                      offset: Offset(0.0, 5))
                ],
              ),
              child: Icon(
                Icons.camera_alt,
                color: R.color.grey,
                size: 27.h,
              ),
            ),
          ),
        ),
      ],
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(30.h),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.h),
              topRight: Radius.circular(30.h),
            ),
            color: R.color.white,
          ),
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  _cubit.pickAvatar(true);
                  NavigationUtils.pop(context);
                },
                child: buildButton(
                  title: R.string.take_a_photo.tr(),
                  icon: CupertinoIcons.camera_fill,
                ),
              ),
              SizedBox(height: 30.h),
              InkWell(
                onTap: () {
                  _cubit.pickAvatar(false);
                  NavigationUtils.pop(context);
                },
                child: buildButton(
                  title: R.string.choose_a_photo.tr(),
                  icon: CupertinoIcons.photo,
                ),
              ),
              SizedBox(height: 30.h),
            ],
          ),
        ),
      ),
    );
  }

  Container buildButton({String? title, IconData? icon}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: R.color.primaryColor,
      ),
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon, color: R.color.white, size: 25.h),
            SizedBox(width: 15.h),
            Text(
              title ?? "",
              style: TextStyle(
                fontSize: 15.sp,
                fontWeight: FontWeight.w600,
                color: R.color.white,
              ),
              textAlign: TextAlign.center,
              maxLines: 1,
            ),
          ],
        ),
      ),
    );
  }
}
