import 'package:equatable/equatable.dart';

abstract class ManageMembersState {}

class ManageMembersSuccessState extends ManageMembersState {}

class ManageMembersFailureState extends ManageMembersState {
  final error;
  ManageMembersFailureState(this.error);
}

class ManageMembersLoadingState extends ManageMembersState
    implements Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => [];

  @override
  // TODO: implement stringify
  bool? get stringify => null;
}
