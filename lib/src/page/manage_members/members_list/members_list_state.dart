abstract class MembersListState {}

class MembersListSuccessState extends MembersListState {}

class MembersListLoadingState extends MembersListState {
  final bool showIndicator;
  MembersListLoadingState(this.showIndicator);
}

class MembersListErrorState extends MembersListState {
  final error;
  MembersListErrorState(this.error);
}
