import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/members_list.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';

class AssignRolesBottomSheet extends StatefulWidget {
  final MemberOfCommunityData? e;
  final List<String?> roles;
  final Function(bool, String) onSelect;
  final Function(String) onRemove;
  final bool isAdmin;
  const AssignRolesBottomSheet(
      {required this.e,
      required this.roles,
      required this.isAdmin,
      required this.onSelect,
      required this.onRemove,
      Key? key})
      : super(key: key);

  @override
  _AssignRolesBottomSheetState createState() => _AssignRolesBottomSheetState();
}

class _AssignRolesBottomSheetState extends State<AssignRolesBottomSheet> {
  late List<String?> roles;

  @override
  void initState() {
    super.initState();
    roles = [...(widget.e?.groupRoles ?? [])];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: IntrinsicHeight(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.w, vertical: 8.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 16.h,
            ),
            Text(
              R.string.assign_roles.tr(),
              style: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 16.sp, height: 24.h / 16.sp),
            ),
            SizedBox(
              height: 8.h,
            ),
            Text(
              R.string.assign_roles_details.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 11.sp, height: 20.h / 11.sp),
            ),
            if (widget.isAdmin) ...[
              _options(GroupMemberRole.ADMIN.name),
              Container(
                height: 1.h,
                width: double.infinity,
                color: R.color.lightestGray,
              ),
              if (widget.e?.communityV2?.communityInfo?.communityRole
                      ?.contains(MemberType.LEADER) ??
                  false) ...[
                _options(GroupMemberRole.LEADER.name),
                Container(
                  height: 1.h,
                  width: double.infinity,
                  color: R.color.lightestGray,
                ),
              ]
            ],
            _options(GroupMemberRole.SECRETARY.name),
            Container(
              height: 1.h,
              width: double.infinity,
              color: R.color.lightestGray,
            ),
            _options(GroupMemberRole.TREASURER.name),
            Container(
              height: 1.h,
              width: double.infinity,
              color: R.color.lightestGray,
            ),
            if (widget.isAdmin)
              GestureDetector(
                onTap: () {
                  NavigationUtils.pop(context);
                  Utils.showConfirmationBottomSheet(
                      context: context,
                      title: R.string.remove_member.tr(),
                      formatedDetails: R.string.remove_member_details
                          .tr(args: [widget.e?.communityV2?.name ?? ""]),
                      onConfirm: () {
                        widget.onRemove(widget.e?.userUuid ?? "");
                      });
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.h),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 12,
                      ),
                      Image.asset(
                        R.drawable.ic_cancel,
                        width: 24.h,
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        R.string.remove_from_group.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 13.sp),
                      )
                    ],
                  ),
                ),
              )
          ],
        ),
      ),
    ));
  }

  Widget _options(String role) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 4.h),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Checkbox(
              value: roles.contains(role),
              onChanged: (val) {
                if (val != null) {
                  if (val)
                    roles.add(role);
                  else
                    roles.removeWhere((element) => element == role);
                  widget.onSelect(val, role);
                  setState(() {});
                }
              }),
          Text(
            role.toLowerCase().tr(),
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 13.sp),
          )
        ],
      ),
    );
  }
}
