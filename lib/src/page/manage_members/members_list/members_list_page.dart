import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/response/members_list.dart';
import 'package:imi/src/page/manage_members/members_list/assign_roles_bottom_sheet.dart';
import 'package:imi/src/page/pin_code/change_password/change_password_cubit.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/manage_members/members_list/members_list_cubit.dart';
import 'package:imi/src/page/manage_members/members_list/members_list_state.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';

import '../../../../res/R.dart';
import '../../../utils/utils.dart';
import '../../../widgets/button_widget.dart';

class MembersListPage extends StatefulWidget {
  final List<String?> roles;
  final int clubCommunityId;
  const MembersListPage(
      {Key? key, required this.clubCommunityId, required this.roles})
      : super(key: key);

  @override
  _MembersListPageState createState() => _MembersListPageState();
}

class _MembersListPageState extends State<MembersListPage> {
  late final MembersListCubit _cubit;
  final RefreshController _refreshController = RefreshController();

  bool get _isAdmin => widget.roles.contains(GroupMemberRole.ADMIN.name);
  bool get _isLeader => widget.roles.contains(GroupMemberRole.LEADER.name);
  int get _myCommunity => appPreferences.getInt(Const.COMMUNITY_ID) ?? 0;

  @override
  void initState() {
    super.initState();
    _cubit = new MembersListCubit(
        appRepository: AppRepository(),
        graphqlRepository: AppGraphqlRepository(),
        clubCommunityId: widget.clubCommunityId)
      ..getMembers(showIndicator: true);
  }

  @override
  void dispose() {
    _cubit.close();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MembersListCubit, MembersListState>(
        bloc: _cubit,
        builder: (context, state) {
          return StackLoadingView(
            visibleLoading:
                state is MembersListLoadingState && state.showIndicator,
            child: SmartRefresher(
              controller: _refreshController,
              enablePullUp: !_cubit.noMoreData,
              onRefresh: () {
                _cubit.getMembers(isRefresh: true, showIndicator: false);
              },
              onLoading: () {
                _cubit.getMembers();
              },
              child: (_cubit.data != null && _cubit.data!.isNotEmpty)
                  ? ListView.builder(
                      itemCount: _cubit.data!.length,
                      itemBuilder: (context, index) {
                        final e = _cubit.data![index];
                        return Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.h),
                          child: InkWell(
                            onTap: (){
                              NavigationUtils.navigatePage(
                                  context,
                                  ViewProfilePage(
                                    communityId: e?.communityV2?.id,
                                  ));
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                AvatarWidget(
                                    avatar: e?.communityV2?.avatarUrl,
                                    size: 40.h),
                                SizedBox(
                                  width: 8.w,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        e?.communityV2?.name ?? "",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 11.sp,
                                                height: 20.h / 11.sp),
                                      ),
                                      _roles(e?.groupRoles ?? [])
                                    ],
                                  ),
                                ),
                                if (e?.communityV2?.id != _myCommunity)
                                  ButtonWidget(
                                    title:
                                        (e?.communityV2?.userView?.isFollowing ??
                                                false)
                                            ? R.string.following.tr()
                                            : R.string.follow.tr(),
                                    onPressed: () {
                                      _cubit.follow(
                                          e?.communityV2?.id ?? 0,
                                          !(e?.communityV2?.userView
                                                  ?.isFollowing ??
                                              false));
                                    },
                                    uppercaseTitle: false,
                                    textSize: 12.sp,
                                    radius: 8.r,
                                    textColor: R.color.orange,
                                    backgroundColor: R.color.white,
                                    borderColor: R.color.orange,
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(
                                            color: R.color.orange, height: 1.2),
                                    height: 33.h,
                                    width: 93.w,
                                  ),
                                SizedBox(
                                  width: 8.w,
                                ),
                                if ((_isAdmin || _isLeader) &&
                                    e?.communityV2?.id != _myCommunity)
                                  InkWell(
                                    child: Icon(Icons.more_horiz_rounded),
                                    onTap: () {
                                      _showAssignRoleBottomSheet(e);
                                    },
                                  )
                              ],
                            ),
                          ),
                        );
                      },
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                    )
                  : Align(
                      alignment: Alignment.center,
                      child: _cubit.data != null && _cubit.data!.isEmpty
                          ? Text(R.string.no_member.tr(),
                              style: Theme.of(context).textTheme.regular400)
                          : SizedBox(),
                    ),
            ),
          );
        },
        listener: (context, state) {
          if (state is MembersListErrorState) {
            if (state.error == ServerError.unassign_role_error) {
              Utils.showConfirmationBottomSheet(
                  context: context,
                  title: R.string.demote_members.tr(),
                  formatedDetails: R.string.demote_members_fail.tr(),
                  showCancelButton: false,
                  confirmationDescription: R.string.understanded.tr(),
                  onConfirm: () {});
            } else if (state.error == ServerError.delete_member_error) {
              Utils.showConfirmationBottomSheet(
                  context: context,
                  title: R.string.remove_member.tr(),
                  formatedDetails: R.string.demote_members_fail.tr(),
                  showCancelButton: false,
                  confirmationDescription: R.string.understanded.tr(),
                  onConfirm: () {});
            } else
              Utils.showErrorSnackBar(context, state.error);
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          } else if (state is MembersListSuccessState) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
        });
  }

  Widget _roles(List<String?> roles) {
    roles = []..addAll(roles);
    if (roles.length > 1 && roles.contains(GroupMemberRole.MEMBER.name)) {
      roles.removeWhere((element) => element == GroupMemberRole.MEMBER.name);
    }
    if (roles.contains(GroupMemberRole.ADMIN.name) ||
        roles.contains(GroupMemberRole.LEADER.name)) {
      roles = roles
          .takeWhile((value) =>
              value == GroupMemberRole.ADMIN.name ||
              value == GroupMemberRole.LEADER.name)
          .toList();
    } else if (roles.contains(GroupMemberRole.TREASURER.name)) {
      roles = [GroupMemberRole.TREASURER.name];
    } else if (roles.contains(GroupMemberRole.SECRETARY.name)) {
      roles = [GroupMemberRole.SECRETARY.name];
    }
    final res = <Widget>[];
    for (int i = 0; i < roles.length * 2 - 1; i++) {
      if (i % 2 == 1) {
        res.add(Icon(
          Icons.circle,
          size: 6,
          color: R.color.black,
        ));
      } else {
        res.add(Text(
          (roles[i ~/ 2] ?? GroupMemberRole.MEMBER.name).toLowerCase().tr(),
          style: Theme.of(context)
              .textTheme
              .regular400
              .copyWith(fontSize: 10.sp, height: 16.h / 10.sp),
        ));
      }
    }
    return Wrap(
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: res,
      spacing: 4,
    );
  }

  int _sortByRole(a, b) {
    int a_index =
        GroupMemberRole.values.indexWhere((element) => element.name == a);
    int b_index =
        GroupMemberRole.values.indexWhere((element) => element.name == b);
    return a_index - b_index;
  }

  void _showAssignRoleBottomSheet(MemberOfCommunityData? e) async {
    List<String?> newRoles = <String?>[]..addAll(e?.groupRoles ?? []);
    final oldRoles = <String?>[...(e?.groupRoles ?? [])]..sort(_sortByRole);
    showBarModalBottomSheet(
        context: context,
        builder: (context) {
          return AssignRolesBottomSheet(
              e: e,
              roles: newRoles,
              isAdmin: _isAdmin,
              onSelect: (selected, role) {
                if (selected) {
                  newRoles.add(role);
                  newRoles.sort(_sortByRole);
                  logger.d(newRoles);
                } else {
                  newRoles.removeWhere((element) => element == role);
                  newRoles.sort(_sortByRole);
                  logger.d(newRoles);
                }
              },
              onRemove: (userUuid) {
                _cubit.removeUsers(userUuid);
                NavigationUtils.pop(context);
              });
        }).then((_) {
      bool isChanged = newRoles.length != oldRoles.length;
      outer:
      for (var i in newRoles) {
        for (var j in oldRoles) {
          if (i == j) continue outer;
        }
        isChanged = true;
        break;
      }

      if (isChanged) {
        final notInNewRoles =
            oldRoles.where((element) => !newRoles.contains(element));
        final notInOldRoles =
            newRoles.where((element) => !oldRoles.contains(element));

        bool isDemoted = notInNewRoles.isNotEmpty &&
            notInOldRoles.isEmpty &&
            oldRoles.length > newRoles.length;

        var temp = newRoles.map((e) => e?.toLowerCase().tr()).toList();
        if (newRoles.length > 1 &&
            newRoles.contains(GroupMemberRole.MEMBER.name)) {
          temp.removeWhere((value) =>
              value == GroupMemberRole.MEMBER.name.toLowerCase().tr());
        } else if (newRoles.length == 1 &&
            newRoles.contains(GroupMemberRole.MEMBER.name)) {
          isDemoted = true;
        } else if (newRoles.isEmpty) {
          newRoles.add(GroupMemberRole.MEMBER.name);
        }

        Utils.showConfirmationBottomSheet(
            context: context,
            title: isDemoted
                ? R.string.demote_members.tr()
                : R.string.assign_roles.tr(),
            formatedDetails: isDemoted
                ? R.string.demote_members_details.tr(args: [
                    notInNewRoles
                        .map((e) => (e ?? "").toLowerCase().tr())
                        .join("/"),
                    e?.communityV2?.name ?? ""
                  ])
                : R.string.assign_roles_confirm_details
                    .tr(args: [e?.communityV2?.name ?? "", temp.join(", ")]),
            onConfirm: () {
              e?.groupRoles = oldRoles;
              _cubit.assignRoles(e?.userUuid ?? "",
                  newRoles.map<String>((e) => e ?? "").toList());
            });
      }
    });
  }
}
