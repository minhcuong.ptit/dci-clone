import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/manage_members/members_list/members_list_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';

import '../../../data/network/response/members_list.dart';

class MembersListCubit extends Cubit<MembersListState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  final int clubCommunityId;

  MembersListCubit(
      {required this.appRepository,
      required this.graphqlRepository,
      required this.clubCommunityId})
      : super(MembersListLoadingState(true));

  List<MemberOfCommunityData?>? data;
  String? _nextToken;
  bool _noMoreData = false;
  bool get noMoreData => _noMoreData;

  bool hasUniqueLeader = false;
  bool hasUniqueAdmin = false;

  void getMembers({bool isRefresh = false, bool showIndicator = false}) async {
    emit(MembersListLoadingState(showIndicator));
    if (isRefresh) {
      _nextToken = null;
    }

    final res = await graphqlRepository.getMembers(
        communityId: clubCommunityId, nextToken: _nextToken);
    if (isRefresh) data?.clear();
    res.when(success: (members) {
      hasUniqueAdmin = members.adminNo == 1;
      hasUniqueLeader = members.leaderNo == 1;
      data ??= [];
      if (members.data?.isNotEmpty ?? false) {
        _nextToken = members.nextToken;
        data?.addAll(members.data?.map((e) {
              if (!(e?.groupRoles?.contains(GroupMemberRole.MEMBER.name) ??
                  true)) {
                e?.groupRoles?.add(GroupMemberRole.MEMBER.name);
              }
              return e;
            }) ??
            []);
      } else {
        _noMoreData = true;
      }
      emit(MembersListSuccessState());
    }, failure: (e) {
      emit(MembersListErrorState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void follow(int? communityId, bool isFollow) async {
    emit(MembersListLoadingState(true));
    ApiResult<dynamic> result = await appRepository.follow(FollowRequest(
        communityId: communityId,
        action: isFollow ? Const.FOLLOW : Const.UNFOLLOW));
    result.when(success: (dynamic response) async {
      final temp = data
          ?.firstWhere((element) => element?.communityV2?.id == communityId);
      temp?.communityV2?.userView?.isFollowing = isFollow;
      if (isFollow)
        temp?.communityV2?.communityInfo?.followerNo =
            (temp.communityV2?.communityInfo?.followerNo ?? 0) + 1;
      else
        temp?.communityV2?.communityInfo?.followerNo =
            (temp.communityV2?.communityInfo?.followerNo ?? 0) - 1;
      emit(MembersListSuccessState());
    }, failure: (NetworkExceptions error) async {
      emit(MembersListErrorState(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void assignRoles(String userUuid, List<String> roles) async {
    emit(MembersListLoadingState(true));
    final res = await appRepository.updateDeleteMember(
        memberAction: MemberActionType.UPDATE.name,
        userUuid: userUuid,
        roles: roles,
        communityId: clubCommunityId);
    res.when(success: (_) {
      data?.firstWhere((element) => element?.userUuid == userUuid)?.groupRoles =
          roles;
      emit(MembersListSuccessState());
    }, failure: (e) {
      if (e is BadRequest && e.code != null) {
        emit(MembersListErrorState(e.code));
      } else {
        emit(MembersListErrorState(NetworkExceptions.getErrorMessage(e)));
      }
    });
  }

  void removeUsers(String userUuid) async {
    emit(MembersListLoadingState(true));
    final res = await appRepository.updateDeleteMember(
        memberAction: MemberActionType.DELETE.name,
        userUuid: userUuid,
        roles: null,
        communityId: clubCommunityId);
    res.when(success: (_) {
      data?.removeWhere((e) => e?.userUuid == userUuid);
      emit(MembersListSuccessState());
    }, failure: (e) {
      if (e is BadRequest && e.code != null) {
        emit(MembersListErrorState(e.code));
      } else
        emit(MembersListErrorState(NetworkExceptions.getErrorMessage(e)));
    });
  }
}
