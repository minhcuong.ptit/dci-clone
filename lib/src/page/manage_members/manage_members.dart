import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/page/manage_members/manage_members_cubit.dart';
import 'package:imi/src/page/manage_members/manage_members_state.dart';
import 'package:imi/src/page/manage_members/members_list/members_list_page.dart';
import 'package:imi/src/page/manage_members/requests_list/requests_list_page.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/custom_tabbar_controller.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';

class ManageMembersPage extends StatefulWidget {
  final int clubCommunityId;
  final int index;
  const ManageMembersPage(
      {Key? key, required this.clubCommunityId, this.index = 0})
      : super(key: key);

  @override
  _ManageMembersPageState createState() => _ManageMembersPageState();
}

class _ManageMembersPageState extends State<ManageMembersPage>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;
  late final ManageMembersCubit _cubit;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _cubit = ManageMembersCubit(AppGraphqlRepository())
      ..getCommunity(widget.clubCommunityId);

    if (widget.index > 0)
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _tabController.animateTo(widget.index);
      });
  }

  @override
  void dispose() {
    _cubit.close();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar(context, R.string.member.tr()),
        body: BlocConsumer<ManageMembersCubit, ManageMembersState>(
          bloc: _cubit,
          listener: (context, state) {
            if (state is ManageMembersFailureState) {
              Utils.showErrorSnackBar(context, state.error);
            }
          },
          buildWhen: (_, state) => !(state is ManageMembersFailureState),
          listenWhen: (_, state) => state is ManageMembersFailureState,
          builder: (context, state) {
            if (state is ManageMembersLoadingState) {
              return StackLoadingView(child: SizedBox(), visibleLoading: true);
            }
            return _cubit.roles.contains(GroupMemberRole.ADMIN.name) ||
                    _cubit.roles.contains(GroupMemberRole.LEADER.name)
                ? Column(
                    children: [
                      CustomTabBarController(
                        tabBarItems: [
                          TabBarItem(title: R.string.member.tr()),
                          TabBarItem(title: R.string.request.tr())
                        ],
                        tabController: _tabController,
                        onTabChange: (index) {},
                      ),
                      Expanded(
                          child: TabBarView(
                        controller: _tabController,
                        children: [
                          MembersListPage(
                              clubCommunityId: widget.clubCommunityId,
                              roles: _cubit.roles),
                          RequestsListPage(
                            clubCommunityId: widget.clubCommunityId,
                          )
                        ],
                      ))
                    ],
                  )
                : MembersListPage(
                    clubCommunityId: widget.clubCommunityId,
                    roles: _cubit.roles);
          },
        ));
  }
}
