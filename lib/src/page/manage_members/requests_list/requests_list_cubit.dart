import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/response/members_list.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/manage_members/requests_list/requests_list_state.dart';
import 'package:imi/src/utils/enum.dart';

import '../../../data/network/repository/app_graphql_repository.dart';
import '../../../data/network/repository/app_repository.dart';
import '../../../data/network/response/request_to_join.dart';

class RequestsListCubit extends Cubit<RequestsListState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  final int clubCommunityId;
  RequestsListCubit(
      {required this.appRepository,
      required this.graphqlRepository,
      required this.clubCommunityId})
      : super(RequestsListLoadingState(true));

  List<MemberOfCommunityData?>? data;
  String? nextToken;
  bool _noMoreRequest = false;
  bool get noMoreRequest => _noMoreRequest;

  void getRequests({bool isRefresh = false}) async {
    emit(RequestsListLoadingState(true));
    data ??= [];
    if (isRefresh) {
      nextToken = null;
      data!.clear();
    }
    final res = await graphqlRepository.getRequestToJoin(
        communityId: clubCommunityId, nextToken: nextToken);

    res.when(success: (requests) {
      if ((requests.data ?? []).isNotEmpty) {
        nextToken = requests.nextToken;
        data!.addAll(requests.data ?? []);
      } else {
        _noMoreRequest = true;
      }
      emit(RequestsListSuccessState());
    }, failure: (NetworkExceptions e) {
      emit(RequestsListFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void approve({required bool isApproved, required String userUuid}) async {
    emit(RequestsListLoadingState(true));
    final res = await appRepository.approveRequest(
        isApproved ? AdminAction.APPROVED : AdminAction.REJECTED,
        clubCommunityId,
        userUuid);
    res.when(success: (_) {
      data?.removeWhere((e) {
        return e?.userUuid == userUuid;
      });
      emit(RequestsListSuccessState());
    }, failure: (e) {
      emit(RequestsListFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }
}
