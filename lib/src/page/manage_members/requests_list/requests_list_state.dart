abstract class RequestsListState {}

class RequestsListFailureState extends RequestsListState {
  final message;
  RequestsListFailureState(this.message);

  get error => null;
}

class RequestsListLoadingState extends RequestsListState {
  final bool showIndicator;
  RequestsListLoadingState(this.showIndicator);
}

class RequestsListSuccessState extends RequestsListState {}
