import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/members_list.dart';
import 'package:imi/src/data/network/response/request_to_join.dart';
import 'package:imi/src/page/manage_members/requests_list/requests_list_cubit.dart';
import 'package:imi/src/page/manage_members/requests_list/requests_list_state.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../data/network/repository/app_graphql_repository.dart';
import '../../../data/network/repository/app_repository.dart';
import '../../../utils/utils.dart';
import '../../../widgets/stack_loading_view.dart';

class RequestsListPage extends StatefulWidget {
  final int clubCommunityId;
  const RequestsListPage({Key? key, required this.clubCommunityId})
      : super(key: key);

  @override
  _RequestsListPageState createState() => _RequestsListPageState();
}

class _RequestsListPageState extends State<RequestsListPage> {
  late final RequestsListCubit _cubit;
  final RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    _cubit = new RequestsListCubit(
        appRepository: AppRepository(),
        graphqlRepository: AppGraphqlRepository(),
        clubCommunityId: widget.clubCommunityId)
      ..getRequests();
  }

  @override
  void dispose() {
    _cubit.close();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RequestsListCubit, RequestsListState>(
        bloc: _cubit,
        builder: (context, state) {
          return StackLoadingView(
            visibleLoading: state is RequestsListLoadingState&&state.showIndicator,
            child: SmartRefresher(
              controller: _refreshController,
              enablePullUp: _cubit.noMoreRequest,
              onRefresh: () {
                _cubit.getRequests(isRefresh: true);
              },
              onLoading: () {
                _cubit.getRequests(isRefresh: false);
              },
              child:state is RequestsListLoadingState?SizedBox.shrink(): _cubit.data != null && _cubit.data!.isNotEmpty
                  ? ListView.builder(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.w, vertical: 24.h),
                      itemBuilder: (context, index) {
                        final e = _cubit.data![index];
                        return _buildItem(e);
                      },
                      itemCount: _cubit.data!.length,
                    )
                  : Align(
                      alignment: Alignment.center,
                      child: Text(
                          (_cubit.data != null && _cubit.data!.isEmpty)
                              ? R.string.empty_requests_list.tr()
                              : "",
                          style: Theme.of(context).textTheme.regular400),
                    ),
            ),
          );
        },
        listener: (context, state) {
          if (state is RequestsListFailureState) {
            _refreshController.loadComplete();
            _refreshController.refreshCompleted();
            Utils.showErrorSnackBar(context, state.error);
          } else if (state is RequestsListSuccessState) {
            _refreshController.loadComplete();
            _refreshController.refreshCompleted();
          }
        });
  }

  Widget _buildItem(MemberOfCommunityData? e) {
    return InkWell(
      onTap: () {
        NavigationUtils.navigatePage(
            context, ViewProfilePage(communityId: e?.communityV2?.id ?? 0));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            AvatarWidget(avatar: e?.communityV2?.avatarUrl, size: 40.h),
            SizedBox(
              width: 8.w,
            ),
            Text(
              e?.communityV2?.name ?? "",
              style:
                  Theme.of(context).textTheme.bold700.copyWith(fontSize: 14.sp),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                _cubit.approve(isApproved: false, userUuid: e?.userUuid ?? "");
              },
              child: Image.asset(
                R.drawable.ic_reject,
                width: 24.w,
              ),
            ),
            SizedBox(
              width: 8.w,
            ),
            GestureDetector(
              onTap: () {
                _cubit.approve(isApproved: true, userUuid: e?.userUuid ?? "");
              },
              child: Image.asset(
                R.drawable.ic_approve,
                width: 24.w,
              ),
            )
          ],
        ),
      ),
    );
  }
}
