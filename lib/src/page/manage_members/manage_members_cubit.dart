import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';

import '../../data/network/response/community_data.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import 'manage_members_state.dart';

class ManageMembersCubit extends Cubit<ManageMembersState> {
  final AppGraphqlRepository graphqlRepository;
  ManageMembersCubit(this.graphqlRepository)
      : super(ManageMembersLoadingState());

  List<String> roles = [];

  void getCommunity(int clubCommunityID) async {
    emit(ManageMembersLoadingState());
    ApiResult<CommunityDataV2Response> getProfileTask =
        await graphqlRepository.getCommunityV2(clubCommunityID);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      roles = data.communityV2?.groupView?.myRoles ?? [];
      emit(ManageMembersSuccessState());
    }, failure: (NetworkExceptions error) async {
      emit(ManageMembersFailureState(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
