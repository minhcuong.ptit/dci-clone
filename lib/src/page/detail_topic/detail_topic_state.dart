abstract class ListTopicState {
  ListTopicState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class ListTopicInitial extends ListTopicState {
  @override
  String toString() => 'ListTopicInitial';
}

class ListTopicSuccess extends ListTopicState {
  @override
  String toString() => 'ListTopicSuccess';
}

class ListSearchTopicSuccess extends ListTopicState {
  @override
  String toString() => 'ListSearchTopicSuccess';
}

class ListTopicLoading extends ListTopicState {
  @override
  String toString() => 'ListTopicLoading';
}

class ListTopicFailure extends ListTopicState {
  final String error;

  ListTopicFailure(this.error);

  @override
  String toString() => 'ListTopicFailure { error: $error }';
}

class ListTopicEmpty extends ListTopicState {
  @override
  String toString() {
    return 'ListTopicEmpty{}';
  }
}

class SearchTopicSuccess extends ListTopicState {
  @override
  String toString() {
    return 'SearchTopicSuccess{}';
  }
}

class SearchTopicEmpty extends ListTopicState {
  @override
  String toString() => 'SearchTopicEmpty';
}
