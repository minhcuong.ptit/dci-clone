import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/response/list_question_response.dart';
import 'package:imi/src/page/detail_topic/detail_topic_state.dart';

import '../../data/database/app_database.dart';
import '../../data/database/entities/search_keyword_entity.dart';
import '../../data/database/sqflite/DBHelper.dart';
import '../../data/database/sqflite/search_suggest.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import '../../utils/utils.dart';

class ListTopicCubit extends Cubit<ListTopicState> {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;
  final db = DBHelper();
  String? nextTokenSearch;
  List<int> topicIds;
  List<FetchQuestionsData> searchListQuestion = [];
  List<SearchSuggest> listPreviousKeyword = [];
  late AppDatabase database;
  Timer? _debounce;

  String? get name => appPreferences.getString(Const.USERNAME);

  ListTopicCubit(this.graphqlRepository, this.repository, this.topicIds)
      : super(ListTopicInitial());

  void initDatabase() async {
    database =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    await getPreviousSearch();
    //getPopularClub();
  }

  void getSearchListQuestionTopic(
      {bool isRefresh = false,
      bool isLoadMore = false,
      String? searchKey}) async {
    emit(isRefresh ? ListTopicLoading() : ListTopicInitial());
    if (isLoadMore != true) {
      nextTokenSearch = null;
      searchListQuestion.clear();
    }
    ApiResult<FetchQuestions> result = await graphqlRepository.getListQuestion(
        nextToken: nextTokenSearch,
        searchKey: searchKey ?? null,
        topicIds: topicIds == 0 ? [] : topicIds);
    result.when(success: (FetchQuestions data) async {
      if (data.data?.length != 0) {
        searchListQuestion.addAll(data.data ?? []);
        nextTokenSearch = data.nextToken;
        emit(ListSearchTopicSuccess());
      } else {
        emit(SearchTopicEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(ListTopicFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void onSearchChanged(String text) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      addKeyword(keyword: text);
    });
  }

  void addKeyword({String? keyword}) async {
    emit(ListTopicLoading());
    if (!Utils.isEmpty(keyword?.trim())) {
      var item =
          await database.searchDao.findKeywordByText(keyword!.trim()).first;
      if (item == null) {
        await database.searchDao
            .insertSearchKeyword(SearchKeywordEntity(0, keyword.trim()));
      }
    }
    emit(ListTopicInitial());
    getSearchListQuestionTopic(searchKey: keyword, isRefresh: true);
  }

  void searchSuggest({String? keyWord, String? type}) async {
    emit(ListTopicLoading());
    await db.save(SearchSuggest(keyWord: keyWord, type: "TOPIC"));
    emit(ListTopicInitial());
    getPreviousSearch();
  }

  void deleteSearchSuggest(int id) async {
    emit(ListTopicLoading());
    await db.delete(id);
    emit(ListTopicInitial());
    getPreviousSearch();
  }

  void deleteAll() async {
    emit(ListTopicLoading());
    await db.deleteAll();
    emit(ListTopicInitial());
    getPreviousSearch();
  }

  Future getPreviousSearch({bool isRefresh = false}) async {
    emit(isRefresh ? ListTopicInitial() : ListTopicLoading());
    listPreviousKeyword =
        await db.getSearchSuggests(keyword: "", type: "TOPIC");
    emit(SearchTopicSuccess());
  }
}
