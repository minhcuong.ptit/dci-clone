import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/detail_topic/detail_topic_cubit.dart';
import 'package:imi/src/page/detail_topic/detail_topic_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/list_question_response.dart';
import '../../utils/const.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/utils.dart';
import '../detail_question/detail_question.dart';

class DetailTopicPage extends StatefulWidget {
  final List<int>? topicIds;
  final String? titleTopic;
  final String? searchKey;

  DetailTopicPage({this.topicIds, this.titleTopic, this.searchKey});

  @override
  State<DetailTopicPage> createState() => _DetailTopicPageState();
}

class _DetailTopicPageState extends State<DetailTopicPage> {
  late ListTopicCubit _cubit;
  RefreshController _controller = RefreshController();
  TextEditingController _searchController = TextEditingController();
  bool hideAppBar = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();

    _cubit =
        ListTopicCubit(graphqlRepository, repository, widget.topicIds ?? []);
    _cubit.initDatabase();

    _searchController.text = widget.searchKey ?? "";
    if (widget.topicIds == null) {
      _cubit.getSearchListQuestionTopic(searchKey: widget.searchKey);
    } else {
      _cubit.getSearchListQuestionTopic();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          centerTitle: true,
          backgroundColor: R.color.primaryColor,
          title: Stack(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        NavigationUtils.pop(context);
                      },
                      child: Icon(
                        CupertinoIcons.back,
                        color: R.color.white,
                        size: 24.h,
                      ),
                    ),
                    Text(
                      R.string.frequently_question.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 16.sp, color: R.color.white),
                    ),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            hideAppBar = !hideAppBar;
                          });
                        },
                        icon: Icon(
                          CupertinoIcons.search,
                          size: 24.h,
                          color: R.color.white,
                        )),
                  ],
                ),
              ),
              buildSearchAppbar(context)
            ],
          )),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<ListTopicCubit, ListTopicState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! ListTopicLoading) {
              _controller.refreshCompleted();
              _controller.loadComplete();
            }
            if (widget.searchKey != "") {
              Utils.hideKeyboard(context);
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, ListTopicState state) {
    return StackLoadingView(
      visibleLoading: state is ListTopicLoading,
      child: RefreshConfiguration(
        enableScrollWhenRefreshCompleted: false,
        child: SmartRefresher(
          controller: _controller,
          enablePullUp:
              _cubit.nextTokenSearch != null && state is ListSearchTopicSuccess,
          onRefresh: () {
            _cubit.getSearchListQuestionTopic(
                searchKey: _searchController.text, isRefresh: true);
          },
          onLoading: () {
            _cubit.getSearchListQuestionTopic(
                searchKey: _searchController.text, isLoadMore: true);
          },
          child: buildSearchListQuestion(context, state),
        ),
      ),
    );
  }

  Widget buildSearchListQuestion(BuildContext context, ListTopicState state) {
    return state is ListTopicLoading
        ? SizedBox.shrink()
        : _cubit.searchListQuestion.length == 0 && state is SearchTopicEmpty
            ? Center(
                child: Text(
                  R.string.no_information_found.tr(),
                  style: Theme.of(context).textTheme.bodyRegular,
                ),
              )
            : ListView(
                padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 12.h),
                children: [
                  Visibility(
                    visible: state is! SearchTopicEmpty &&
                        _cubit.searchListQuestion.length != 0 &&
                        _searchController.text.isNotEmpty,
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: Text(
                        R.string.search_results.tr(args: [" "]) +
                            "(${_cubit.searchListQuestion.length})",
                        style: Theme.of(context)
                            .textTheme
                            .tooltip
                            .copyWith(color: R.color.primaryColor),
                      ),
                    ),
                  ),
                  SizedBox(height: 15.h),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: _cubit.searchListQuestion.length,
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox();
                    },
                    itemBuilder: (BuildContext context, int index) {
                      FetchQuestionsData data =
                          _cubit.searchListQuestion[index];
                      return InkWell(
                        highlightColor: R.color.white,
                        onTap: () {
                          NavigationUtils.navigatePage(
                              context,
                              DetailQuestionPage(
                                questionId: data.id,
                                topicIds:
                                    data.topics?.map((e) => e.id ?? 0).toList(),
                              ));
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data.question ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                    fontSize: 14.sp,
                                    height: 24.h / 14.sp,
                                  ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 10.h),
                              color: R.color.lightGray,
                              height: 1.h,
                            ),
                          ],
                        ),
                      );
                    },
                  )
                ],
              );
  }

  Widget buildSearchAppbar(BuildContext context) {
    return Visibility(
      visible: hideAppBar || (!Utils.isEmpty(_searchController.text)),
      child: Container(
        color: R.color.primaryColor,
        child: Row(
          children: [
            GestureDetector(
              onTap: () {
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
              },
              child: Icon(
                CupertinoIcons.back,
                color: R.color.white,
                size: 24.h,
              ),
            ),
            SizedBox(width: 5.h),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15.h, vertical: 5.h),
                margin: EdgeInsets.symmetric(vertical: 8.h),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.h),
                  color: R.color.white,
                  boxShadow: [
                    BoxShadow(
                      color: R.color.black.withOpacity(0.1),
                      blurRadius: 5,
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    InkWell(
                        onTap: () {
                          _cubit.getSearchListQuestionTopic(
                              searchKey: _searchController.text);
                        },
                        child: Icon(CupertinoIcons.search,
                            size: 24.h, color: R.color.grey)),
                    SizedBox(width: 5.h),
                    Expanded(
                      child: CupertinoSearchTextField(
                        autofocus: true,
                        padding: EdgeInsetsDirectional.fromSTEB(0, 5, 5, 5),
                        decoration: BoxDecoration(
                            color: R.color.white,
                            borderRadius: BorderRadius.circular(30.h)),
                        controller: _searchController,
                        prefixIcon: SizedBox(),
                        placeholder: R.string.search.tr(),
                        style: Theme.of(context).textTheme.bodyRegular.copyWith(
                              color: R.color.black,
                            ),
                        onChanged: (text) {
                          _cubit.onSearchChanged(_searchController.text);
                        },
                        onSubmitted: (text) {
                          _cubit.searchSuggest(keyWord: text);
                          Utils.hideKeyboard(context);
                          FocusScope.of(context).requestFocus(new FocusNode());
                          Utils.hideKeyboard(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
