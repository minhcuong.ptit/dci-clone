import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/news_details/news_details.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fwfh_webview/fwfh_webview.dart';

class NewsDetailsPage extends StatefulWidget {
  final int? newsId;

  NewsDetailsPage({this.newsId});

  @override
  State<NewsDetailsPage> createState() => _NewsDetailsPageState();
}

class _NewsDetailsPageState extends State<NewsDetailsPage> {
  late NewsDetailsCubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    // TODO: implement initState
    super.initState();
    _cubit =
        NewsDetailsCubit(repository, appGraphqlRepository, widget.newsId ?? 0);
    _cubit.getNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.grey100,
      appBar: appBar(context, ""),
      body: BlocProvider(
        create: (BuildContext context) => _cubit,
        child: BlocConsumer<NewsDetailsCubit, NewsDetailsState>(
          listener: (BuildContext context, state) {
            if (state is! NewsDetailsLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (BuildContext context, state) {
            return StackLoadingView(
              visibleLoading: state is NewsDetailsLoading,
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () {
                  //_cubit.getNewsDetail(isRefresh: true);
                  _cubit.getNews(isRefresh: true);
                },
                onLoading: () {
                  //_cubit.getNewsDetail(isLoadMore: true);
                  _cubit.getNews(isLoadMore: true);
                },
                child: Scrollbar(
                  child: ListView(
                    padding: EdgeInsets.symmetric(horizontal: 20.h),
                    children: [
                      state is NewsErrorSuccess
                          ? Visibility(
                              visible: state is! NewsDetailsLoading,
                              child: Padding(
                                padding: EdgeInsets.only(top: 30.h, bottom: 10.h),
                                child: Text(
                                  R.string.this_content_is_no_longer_active.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 14.sp),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )
                          : Visibility(
                              visible: state is NewsDetailsSuccess,
                              child: Column(
                                children: [
                                  Text(
                                    _cubit.newsDetail?.title ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(fontSize: 16.sp),
                                  ),
                                  SizedBox(height: 13.h),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: RichText(
                                          text: TextSpan(
                                            text: DateUtil.parseDateToString(
                                                    DateTime
                                                        .fromMillisecondsSinceEpoch(
                                                            _cubit.newsDetail
                                                                    ?.createdDate ??
                                                                0),
                                                    Const.DATE_FORMAT) +
                                                " ",
                                            style: Theme.of(context)
                                                .textTheme
                                                .medium500
                                                .copyWith(color: R.color.grey),
                                            children: [
                                              TextSpan(
                                                text: R.string.written_by
                                                    .tr(args: [" "]),
                                              ),
                                              TextSpan(
                                                  text:
                                                      _cubit.newsDetail?.author ??
                                                          "",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .medium500
                                                      .copyWith(
                                                          color: R.color.black
                                                              .withOpacity(0.6))),
                                            ],
                                          ),
                                        ),
                                      ),
                                      RichText(
                                        text: TextSpan(
                                          text: R.string.view.tr(args: [": "]),
                                          style: Theme.of(context)
                                              .textTheme
                                              .regular400
                                              .copyWith(
                                                  fontSize: 12.sp,
                                                  color: R.color.grey),
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: "${_cubit.newsDetail?.views}",
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 12.h),
                                  Visibility(
                                    visible: state is NewsDetailsSuccess,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10.h),
                                      child: CachedNetworkImage(
                                        width: double.infinity,
                                        height: 240.h,
                                        fit: BoxFit.fill,
                                        imageUrl:
                                            _cubit.newsDetail?.imageUrl ?? "",
                                        placeholder: (_, __) {
                                          return Text(
                                            R.string.loading.tr(),
                                            style: Theme.of(context)
                                                .textTheme
                                                .regular400
                                                .copyWith(fontSize: 14.sp),
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 15.h),
                                  HtmlWidget(
                                    _cubit.newsDetail?.content ?? "",
                                    textStyle: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(fontSize: 12.sp),
                                    onTapUrl: (url) async {
                                      if (await canLaunch(url)) {
                                        return await launch(
                                          url,
                                        );
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    },
                                    factoryBuilder: () => MyWidgetFactory(),
                                    enableCaching: true,
                                  ),
                                  SizedBox(height: 15.h),
                                  Container(height: 1, color: R.color.grey300),
                                  SizedBox(height: 5.h),
                                ],
                              ),
                            ),
                      Text(
                        R.string.recent_posts.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .medium500
                            .copyWith(fontSize: 16.sp),
                      ),
                      SizedBox(height: 20.h),
                      buildListNews()
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget buildListNews() {
    return ListView.builder(
      shrinkWrap: true, //
      physics: NeverScrollableScrollPhysics(),
      itemCount: _cubit.listNews.length,
      itemBuilder: (context, int index) {
        return GestureDetector(
          onTap: () {
            NavigationUtils.pop(context);
            NavigationUtils.rootNavigatePage(
                context,
                NewsDetailsPage(
                  newsId: _cubit.listNews[index].id,
                ));
          },
          child: Container(
            height: 73.h,
            margin: EdgeInsets.only(bottom: 12.h),
            padding: EdgeInsets.symmetric(horizontal: 4.h),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.h),
                color: R.color.white),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.h),
                  child: CachedNetworkImage(
                    width: 68.h,
                    height: 63.h,
                    fit: BoxFit.cover,
                    imageUrl: _cubit.listNews[index].imageUrl ?? "",
                    placeholder: (_, __) {
                      return Text(
                        R.string.loading.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .regular400
                            .copyWith(fontSize: 14.sp),
                      );
                    },
                  ),
                ),
                SizedBox(width: 8.h),
                Expanded(
                  child: Container(
                    height: 70.h,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _cubit.listNews[index].title ?? "",
                          style: Theme.of(context)
                              .textTheme
                              .medium500
                              .copyWith(height: 18 / 12),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                                child: Text(
                              _cubit.listNews[index].author ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(fontSize: 10.sp, height: 15 / 10),
                            )),
                            Text(
                              DateUtil.parseDateToString(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      _cubit.listNews[index].createdDate ?? 0),
                                  Const.DATE_FORMAT_REQUEST),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(fontSize: 10.sp, height: 15 / 10),
                            ),
                            SizedBox(
                              width: 12.h,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {}
