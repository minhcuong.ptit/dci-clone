import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/news_details_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/news_details/news_details.dart';
import 'package:imi/src/page/news_details/news_details_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';

class NewsDetailsCubit extends Cubit<NewsDetailsState> {
  final int postId;
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  NewsDetailsData? newsDetail;
  List<NewsData> listNews = [];
  String? _nextToken;

  NewsDetailsCubit(this.repository, this.graphqlRepository, this.postId)
      : super(NewsDetailsInitial());

  void getNewsDetail({bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || isLoadMore)
        ? NewsDetailsInitial()
        : NewsDetailsLoading());
    ApiResult<NewsDetailsData> getNewsDetail =
        await graphqlRepository.getNewsDetail(postId: postId);
    getNewsDetail.when(success: (NewsDetailsData data) {
      newsDetail = data;
      emit(NewsDetailsSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest &&
          error.code == ServerError.news_is_not_published) {
        emit(NewsErrorSuccess());
        return;
      }
      emit(NewsDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getNews({bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || isLoadMore)
        ? NewsDetailsInitial()
        : NewsDetailsLoading());
    if (isRefresh) {
      _nextToken = null;
      listNews = [];
    }
    final getNews = await graphqlRepository.getNews(
        typeCategory: NewsCategory.NEWS,
        limit: 4,
        nextToken: _nextToken,
        excludeIds: [postId]);
    getNews.when(success: (NewsDataFetch data) async {
      listNews = data.data ?? [];
      emit(NewsSuccess());
      getNewsDetail();
    }, failure: (NetworkExceptions error) async {
      emit(NewsDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
