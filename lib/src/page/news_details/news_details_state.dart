import 'package:equatable/equatable.dart';

abstract class NewsDetailsState extends Equatable {
  @override
  List<Object> get props => [];
}

class NewsDetailsInitial extends NewsDetailsState {}

class NewsDetailsLoading extends NewsDetailsState {
  @override
  String toString() {
    return 'NewsDetailsLoading{}';
  }
}

class NewsDetailsFailure extends NewsDetailsState {
  final String error;

  NewsDetailsFailure(this.error);

  @override
  String toString() {
    return 'NewsDetailsFailure{error: $error}';
  }
}

class NewsDetailsSuccess extends NewsDetailsState {
  @override
  String toString() {
    return 'NewsDetailsSuccess{}';
  }
}

class NewsSuccess extends NewsDetailsState {
  @override
  String toString() {
    return 'NewsSuccess{}';
  }
}

class NewsErrorSuccess extends NewsDetailsState {
  @override
  String toString() {
    return 'NewsErrorSuccess{}';
  }
}
