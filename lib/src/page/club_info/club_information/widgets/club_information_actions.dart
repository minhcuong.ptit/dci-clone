import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/club_info/club_information/club_information_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';

class ClubInformationActionButtons extends StatefulWidget {
  final int? communityId;

  const ClubInformationActionButtons({Key? key, this.communityId})
      : super(key: key);

  @override
  State<ClubInformationActionButtons> createState() =>
      _ClubInformationActionButtonsState();
}

class _ClubInformationActionButtonsState
    extends State<ClubInformationActionButtons> {
  late ViewClubCubit _cubit;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _cubit = BlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    if (_cubit.data?.groupView?.request?.inviter != null &&
        _cubit.data?.groupView?.request?.isRequest != null) {
      return SizedBox();
    }
    if (_cubit.data?.groupView?.myRoles == null &&
        _cubit.data?.type == CommunityType.STUDY_GROUP.name) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: ButtonWidget(
            title: (_cubit.data?.groupView?.request?.isRequest ?? false)
                ? R.string.requested.tr()
                : R.string.join.tr(),
            uppercaseTitle: false,
            backgroundColor: R.color.white,
            textColor: R.color.black,
            borderColor: R.color.black,
            padding: EdgeInsets.symmetric(vertical: 6.h),
            textSize: 12.sp,
            onPressed: () {
              if (!(_cubit.data?.groupView?.request?.isRequest ?? false)) {
                _cubit.requestToJoin();
              }
            }),
      );
    }

    double width = MediaQuery.of(context).size.width / 2 - 24;
    Widget child = SizedBox.shrink();
    if (_cubit.data?.type == CommunityType.BASIC_GROUP.name) {
      child = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (_cubit.data?.groupView?.myRoles == null) ...[
            if (_cubit.data?.groupView?.request?.inviter == null) ...[
              ButtonWidget(
                  title: (_cubit.data?.groupView?.request?.isRequest ?? false)
                      ? R.string.requested.tr()
                      : R.string.join.tr(),
                  uppercaseTitle: false,
                  backgroundColor: R.color.white,
                  borderColor: R.color.black,
                  textColor: R.color.black,
                  width: width,
                  height: 32.h,
                  padding: EdgeInsets.symmetric(horizontal: 6.h),
                  textSize: 14.sp,
                  onPressed: () {
                    if (!(_cubit.data?.groupView?.request?.isRequest ??
                        false)) {
                      _cubit.requestToJoin();
                    }
                  }),
              SizedBox(
                width: 16.w,
              ),
            ],
            ButtonWidget(
                title: (_cubit.data?.userView?.isFollowing ?? true)
                    ? R.string.following.tr()
                    : R.string.follow.tr(),
                width: _cubit.data?.groupView?.request?.inviter != null
                    ? MediaQuery.of(context).size.width - 24
                    : width,
                height: 32.h,
                uppercaseTitle: false,
                padding: EdgeInsets.symmetric(horizontal: 6.h),
                borderColor: R.color.lightBlue,
                backgroundColor: R.color.lightBlue,
                textSize: 14.sp,
                onPressed: () {
                  _cubit.follow(widget.communityId,
                      !(_cubit.data?.userView?.isFollowing ?? true));
                })
          ],
          if (_cubit.data?.groupView?.myRoles
                  ?.contains(GroupMemberRole.ADMIN.name) ??
              false)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    Image.asset(
                      R.drawable.ic_leader,
                      width: 24.w,
                      height: 24.w,
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Text(
                      R.string.admin.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 12.sp),
                    )
                  ],
                ),
                SizedBox(width: 30.w),
                InkWell(
                  highlightColor: R.color.white,
                  onTap: () {
                    _cubit.getGroup();
                  },
                  child: Row(
                    children: [
                      Image.asset(
                        R.drawable.ic_message_off,
                        width: 24.w,
                        height: 24.w,
                      ),
                      SizedBox(
                        width: 8.w,
                      ),
                      Text(
                        R.string.chat_group.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 12.sp),
                      )
                    ],
                  ),
                ),
              ],
            ),
          if (_cubit.data?.groupView?.myRoles
                  ?.contains(GroupMemberRole.MEMBER.name) ??
              false)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    Image.asset(
                      R.drawable.ic_member,
                      width: 24.w,
                      height: 24.w,
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Text(
                      R.string.member.tr(),
                      style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 12.sp,
                          ),
                    )
                  ],
                ),
                SizedBox(
                  width: 30.w,
                ),
                InkWell(
                  highlightColor: R.color.white,
                  onTap: () {
                    _cubit.getGroup();
                  },
                  child: Row(
                    children: [
                      Image.asset(
                        R.drawable.ic_message_off,
                        width: 24.w,
                        height: 24.w,
                      ),
                      SizedBox(
                        width: 8.w,
                      ),
                      Text(
                        R.string.chat_group.tr(),
                        style: Theme.of(context).textTheme.bold700.copyWith(
                              fontSize: 12.sp,
                            ),
                      )
                    ],
                  ),
                ),
              ],
            ),
        ],
      );
    } else if (_cubit.data?.type == CommunityType.STUDY_GROUP.name) {
      late String resource;
      late String role;
      if ((_cubit.data?.groupView?.myRoles ?? [])
          .contains(GroupMemberRole.ADMIN.name)) {
        resource = R.drawable.ic_leader;
        role = R.string.admin.tr();
      } else if ((_cubit.data?.groupView?.myRoles ?? [])
          .contains(GroupMemberRole.LEADER.name)) {
        resource = R.drawable.ic_leader;
        role = R.string.leader.tr();
      } else if ((_cubit.data?.groupView?.myRoles ?? [])
          .contains(GroupMemberRole.MEMBER.name)) {
        resource = R.drawable.ic_member;
        role = R.string.member.tr();
      } else if ((_cubit.data?.groupView?.myRoles ?? [])
          .contains(GroupMemberRole.SECRETARY.name)) {
        resource = R.drawable.ic_member;
        role = R.string.secretary.tr();
      } else if ((_cubit.data?.groupView?.myRoles ?? [])
          .contains(GroupMemberRole.TREASURER.name)) {
        resource = R.drawable.ic_member;
        role = R.string.treasurer.tr();
      }
      child = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Image.asset(
                resource,
                width: 24.w,
                height: 24.w,
              ),
              SizedBox(
                width: 8.w,
              ),
              Text(
                role,
                style: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(fontSize: 12.sp),
              )
            ],
          ),
          SizedBox(width: 30),
          InkWell(
            highlightColor: R.color.white,
            onTap: () {
              _cubit.getGroup();
            },
            child: Row(
              children: [
                Image.asset(
                  R.drawable.ic_message_off,
                  width: 24.w,
                  height: 24.w,
                ),
                SizedBox(
                  width: 8.w,
                ),
                Text(
                  R.string.chat_group.tr(),
                  style: Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: 12.sp,
                      ),
                )
              ],
            ),
          ),
        ],
      );
    }
    return SizedBox(width: double.infinity, child: child);
  }
}
