import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/page/club_info/club_information/club_information_cubit.dart';
import 'package:imi/src/utils/enum.dart';

import '../../../../../res/R.dart';
import '../../../../utils/navigation_utils.dart';
import '../../../../widgets/community_info_widget.dart';
import '../../../follow/follow.dart';
import '../../../manage_members/manage_members.dart';

class ClubInformationStat extends StatelessWidget {
  final int? communityId;
  final VoidCallback onTapPosts;
  const ClubInformationStat(
      {Key? key, this.communityId, required this.onTapPosts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ViewClubCubit _cubit = BlocProvider.of(context);
    return CommunityInfoWidget(
      details: [
        _cubit.data?.communityInfo?.memberNo ?? 0,
        if (_cubit.data?.type == CommunityType.BASIC_GROUP.name)
          _cubit.data?.communityInfo?.followerNo ?? 0,
        _cubit.data?.communityInfo?.postNo ?? 0,
      ],
      titles: [
        R.string.member.tr(),
        if (_cubit.data?.type == CommunityType.BASIC_GROUP.name)
          R.string.followers.tr(),
        R.string.post.tr(),
      ],
      callbacks: [
        () {
          NavigationUtils.navigatePage(
              context,
              ManageMembersPage(
                clubCommunityId: communityId ?? 0,
              ));
        },
        if (_cubit.data?.type == CommunityType.BASIC_GROUP.name)
          () {
            NavigationUtils.navigatePage(
                    context,
                    FollowPage(
                        type: FollowType.FOLLOWER,
                        communityId: communityId ?? 0,
                        numberNo: _cubit.data?.communityInfo?.followerNo ?? 0))
                .then((value) {
              _cubit.getCommunity();
            });
          },
        onTapPosts,
      ],
    );
  }
}
