import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/club_info/club_information/club_information_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';

class ClubInvitationWidget extends StatelessWidget {
  const ClubInvitationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ViewClubCubit _cubit = BlocProvider.of(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 24.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            R.string.join_group.tr(),
            style: Theme.of(context)
                .textTheme
                .bold700
                .copyWith(fontSize: 16.sp, height: 24 / 16.sp),
          ),
          SizedBox(
            height: 8.h,
          ),
          Text(
            R.string.invited_to_join_group.tr(),
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 16.sp, height: 24 / 16.sp),
          ),
          SizedBox(
            height: 36.h,
          ),
          Row(
            children: [
              Expanded(
                  child: ButtonWidget(
                title: R.string.reject.tr(),
                onPressed: () {
                  _cubit.acceptToJoin(false);
                },
                uppercaseTitle: false,
                textSize: 12.sp,
                radius: 48,
                backgroundColor: R.color.white,
                textColor: R.color.black,
                borderColor: R.color.black,
                height: 32.h,
              )),
              SizedBox(
                width: 16.w,
              ),
              Expanded(
                  child: ButtonWidget(
                title: R.string.accept.tr(),
                onPressed: () {
                  _cubit.acceptToJoin(true);
                },
                uppercaseTitle: false,
                textSize: 12.sp,
                textColor: R.color.white,
                backgroundColor: R.color.lightBlue,
                radius: 48,
                height: 32.h,
              )),
            ],
          )
        ],
      ),
    );
  }
}
