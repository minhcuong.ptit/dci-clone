import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/page/club_info/club_information/club_information_cubit.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/category_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../res/R.dart';
import '../../../../utils/const.dart';
import '../../../../widgets/custom_expandable_text.dart';

class ClubInformationDetails extends StatelessWidget {
  const ClubInformationDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ViewClubCubit _cubit = BlocProvider.of(context);
    bool isStudyGroup = _cubit.data?.type == CommunityType.STUDY_GROUP.name;
    DateFormat df = DateFormat("dd/MM/yyyy");
    DateFormat tf = DateFormat("HH:mm");
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0.w),
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              R.string.descriptions_and_rules.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12.sp, color: R.color.textGray),
            ),
            SizedBox(
              height: 8.h,
            ),
            CustomExpandableText(
              maxLines: 3,
              text: _cubit.data?.description ??
                  R.string.have_not_updated_yet.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 14.sp, color: R.color.textBlack),
            ),
            if (isStudyGroup) ...[
              SizedBox(
                height: 16.h,
              ),
              Text(
                R.string.active_time.tr(),
                style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 12.sp,
                      color: R.color.textGray,
                    ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                R.string.from_to.tr(args: [
                  df.format(DateTime.fromMillisecondsSinceEpoch(
                          _cubit.data?.groupView?.event?.startDate ?? 0,
                          isUtc: true)
                      .toUtc()),
                  df.format(DateTime.fromMillisecondsSinceEpoch(
                          _cubit.data?.groupView?.event?.endDate ?? 0,
                          isUtc: true)
                      .toUtc()),
                ]),
                style: Theme.of(context)
                    .textTheme
                    .regular400
                    .copyWith(fontSize: 14.sp, color: R.color.textBlack),
              ),
            ],
            if (isStudyGroup) ...[
              SizedBox(
                height: 16.h,
              ),
              Text(
                R.string.active_time_details.tr(),
                style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 12.sp,
                      color: R.color.textGray,
                    ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                R.string.from_to_details.tr(args: [
                  tf.format(DateTime.fromMillisecondsSinceEpoch(
                      _cubit.data?.groupView?.event?.startTime ?? 0)),
                  tf.format(DateTime.fromMillisecondsSinceEpoch(
                      _cubit.data?.groupView?.event?.endTime ?? 0)),
                  // _cubit.data?.groupView?.event?.dayOfWeek
                  if (context.locale.languageCode == Const.LOCALE_EN)
                    _cubit.data?.groupView?.event?.dayOfWeek?.titleDayOfWeek
                            .toLowerCase() ??
                        ""
                  else
                    _cubit.data?.groupView?.event?.dayOfWeek?.titleDayOfWeek ??
                        DayOfWeek.MONDAY.name
                ]),
                style: Theme.of(context)
                    .textTheme
                    .regular400
                    .copyWith(fontSize: 14.sp, color: R.color.textBlack),
              ),
            ],
            SizedBox(
              height: 16.h,
            ),
            if (isStudyGroup) ...[
              _bomMembers(context, _cubit, GroupMemberRole.LEADER,
                  R.string.leader.tr()),
              _bomMembers(context, _cubit, GroupMemberRole.TREASURER,
                  R.string.treasurer.tr()),
              _bomMembers(context, _cubit, GroupMemberRole.SECRETARY,
                  R.string.secretary.tr()),
            ],
            Text(
              R.string.links.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12.sp, color: R.color.textGray),
            ),
            _links(context, _cubit),
            SizedBox(
              height: 16.h,
            ),
            Text(
              R.string.categories.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12.sp, color: R.color.textGray),
            ),
            _favoriteCategories(context, _cubit),
            SizedBox(
              height: 50.h,
            ),
          ],
        ),
      ),
    );
  }

  // String _getIndexOfWeekDay(String dayOfWeek) {
  //   int index = 0;
  //   for (final i in DayOfWeek.values) {
  //     if (i.name == dayOfWeek) {
  //       index = i.index + 2;
  //     }
  //   }
  //   if (index == 8) return "Chủ nhật";
  //   return "Thứ $index";
  // }

  String _extractLink(String link) {
    if (link.contains("fb.com") || link.contains("facebook.com")) {
      return R.drawable.ic_web;
    } else if (link.contains("youtube.com") || link.contains("youtu.be")) {
      return R.drawable.ic_youtube;
    } else if (link.contains("zalo.me")) {
      return R.drawable.ic_zalo;
    } else if (link.contains("zoom.us")) {
      return R.drawable.ic_zoom;
    } else
      return R.drawable.ic_web;
  }

  Widget _links(context, ViewClubCubit _cubit) {
    if ((_cubit.data?.socialLinks ?? []).isEmpty) {
      return Text(
        R.string.have_not_updated_yet.tr(),
        style: Theme.of(context).textTheme.regular400.copyWith(fontSize: 14.sp),
      );
    }
    return Column(
      children: (_cubit.data?.socialLinks ?? [])
          .map((e) => InkWell(
                onTap: () {
                  launch(e?.url ?? "");
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 12.h),
                  child: Row(
                    children: [
                      Image.asset(
                        _extractLink(e?.url ?? ""),
                        width: 24.w,
                        height: 24.w,
                      ),
                      SizedBox(
                        width: 8.w,
                      ),
                      Expanded(
                        child: Container(
                          constraints: BoxConstraints(
                              maxWidth:
                                  MediaQuery.of(context).size.width * 0.7),
                          child: Text(
                            e?.name ?? "",
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                  fontSize: 12.sp,
                                  color: R.color.textBlack,
                                  height: 16.h / 12.sp,
                                  overflow: TextOverflow.ellipsis,
                                ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8.w,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 12.h,
                      )
                    ],
                  ),
                ),
              ))
          .toList(),
    );
  }

  Widget _favoriteCategories(context, _cubit) {
    if ((_cubit.data?.categories ?? []).isEmpty) {
      return Text(
        R.string.have_not_updated_yet.tr(),
        style: Theme.of(context).textTheme.medium500.copyWith(fontSize: 14.sp),
      );
    }
    return Wrap(
      children: List.generate(
          _cubit.data?.categories?.length ?? 0,
          (index) => CategoryWidget(
              data: _cubit.data?.categories?[index] ?? CategoryData())),
    );
  }

  Widget _bomMembers(
      context, ViewClubCubit _cubit, GroupMemberRole type, String role) {
    final temp = _cubit.data?.groupView?.bomMembers ?? [];
    if (temp.any((element) =>
        element?.role == type.name && (element?.bomMembers ?? []).isNotEmpty)) {
      final bom = temp.firstWhere((e) => e?.role == type.name);
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            role,
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 12.sp, color: R.color.textGray),
          ),
          ...(bom?.bomMembers ?? [])
              .map((e) => GestureDetector(
                    onTap: () {
                      NavigationUtils.navigatePage(context,
                          ViewProfilePage(communityId: e?.communityId ?? 0));
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.h),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AvatarWidget(avatar: e?.avatarUrl, size: 36.h),
                          SizedBox(
                            width: 10.w,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                e?.fullName ?? "",
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(fontSize: 11.sp),
                              ),
                              Text(
                                e?.phone ?? "",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 10.sp),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ))
              .toList(),
          SizedBox(
            height: 16.h,
          ),
        ],
      );
    }
    return SizedBox.shrink();
  }
}
