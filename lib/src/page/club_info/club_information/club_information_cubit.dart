import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/admin_update_community_request.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/request/group_member_request.dart';
import 'package:imi/src/data/network/request/reaction_request.dart';
import 'package:imi/src/data/network/request/report_request.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/media_response.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/response/report_reasons_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../../data/network/response/upload_list_url_response.dart';
import 'club_information_state.dart';

class ViewClubCubit extends Cubit<ViewClubState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  List<String> get listCategory =>
      appPreferences.getListString(Const.FAVORITE_CATEGORY);

  bool isOwner(int? profileCommunityId) =>
      profileCommunityId == null ||
      (profileCommunityId) == appPreferences.getInt(Const.COMMUNITY_ID);

  List<PostData> listPost = [];
  String? nextPostsToken;
  List<FetchReportReason> listReportPost = [];
  int? clubCommunityID;
  List<MediaData> listMedia = [];
  String? nextMediaToken;
  bool _isLoadingPosts = false;
  bool _isLoadingMedia = false;

  bool get isLoadingPosts => _isLoadingPosts;

  bool get isLoadingMedia => _isLoadingMedia;

  bool get isAdminLeader => _isAdmin || _isLeader;

  bool get isAdmin => _isAdmin;

  bool get _isAdmin =>
      (data?.groupView?.myRoles?.contains(GroupMemberRole.ADMIN.name) ?? false);

  bool get _isLeader =>
      (data?.groupView?.myRoles?.contains(GroupMemberRole.LEADER.name) ??
          false);

  CommunityDataV2? data;

  String? get userId => appPreferences.getString(Const.ID);
  String? channelId;

  ViewClubCubit(
      {required this.repository,
      required this.graphqlRepository,
      this.clubCommunityID})
      : super(InitialClubInformationState());

  void refreshPosts() {
    emit(ClubInformationLoadingState());
    getListPost();
    emit(ClubInformationSuccess());
  }

  void getCommunity({bool isRefresh = false}) async {
    emit(isRefresh
        ? InitialClubInformationState()
        : ClubInformationLoadingState());
    ApiResult<CommunityDataV2Response> getProfileTask =
        await graphqlRepository.getCommunityV2(clubCommunityID ?? 0);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      this.data = data.communityV2;
      emit(ClubInformationSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListPost({bool isRefresh = false, bool isLoading = false}) async {
    emit(!isRefresh || !isLoading
        ? ClubInformationLoadingState()
        : InitialClubInformationState());
    if (isLoading != true) {
      listPost.clear();
      nextPostsToken = null;
    }
    ApiResult<PostPageData?> getListPostTask =
        await graphqlRepository.getListPost(nextPostsToken,
            communityId: clubCommunityID ?? 0,limit: Const.LIMIT);

    getListPostTask.when(success: (PostPageData? data) async {
      if (data?.data?.length != 0) {
        listPost.addAll(data?.data ?? []);
        nextPostsToken = data?.nextToken;
        emit(ClubInformationSuccess());
      } else {
        emit(GetListPostEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future<void> getMedia({bool isRefresh = false}) async {
    if (_isLoadingMedia) return;
    _isLoadingMedia = true;
    if (isRefresh) {
      nextMediaToken = null;
    }
    ApiResult<CommunityMedia?> getListMediaTask = await graphqlRepository
        .getMedia(nextMediaToken, communityId: clubCommunityID ?? 0);
    _isLoadingMedia = false;

    getListMediaTask.when(success: (CommunityMedia? data) async {
      if (isRefresh) {
        listMedia = data?.data ?? [];
      } else {
        listMedia.addAll(data?.data ?? []);
      }
      nextMediaToken = data?.nextToken;
      emit(ClubInformationSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void reaction(int postId, bool like) async {
    emit(ClubInformationLoadingState());
    ApiResult<dynamic> result = await repository.reaction(ReactionRequest(
        postId: postId, action: like ? Const.LIKE : Const.DISLIKE));
    result.when(success: (dynamic response) async {
      final post = listPost.firstWhere((e) => e.id == postId.toString());
      post.isLike = like;
      if (like) {
        post.likeNumber = (post.likeNumber ?? 0) + 1;
      } else {
        post.likeNumber = (post.likeNumber ?? 1) - 1;
      }
      emit(ClubInformationSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          getListPost(isRefresh: true);
          getCommunity(isRefresh: true);
        }
        return;
      }
      emit(ClubInformationFailureState(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void follow(int? communityId, bool isFollow) async {
    emit(ClubInformationLoadingState());
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityId,
        action: isFollow ? Const.FOLLOW : Const.UNFOLLOW));
    result.when(success: (dynamic response) async {
      data?.userView?.isFollowing = isFollow;
      if (isFollow)
        data?.communityInfo?.followerNo =
            (data?.communityInfo?.followerNo ?? 0) + 1;
      else
        data?.communityInfo?.followerNo =
            (data?.communityInfo?.followerNo ?? 0) - 1;
      emit(ClubInformationSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void requestToJoin() async {
    String? uuid = appPreferences.getString(Const.ID);
    if (uuid != null) {
      emit(ClubInformationLoadingState());
      final res = await repository.requestToJoinGroup(
          GroupMemberActionRequest(userUuid: [uuid], clubId: data?.id ?? 0));
      res.when(success: (response) {
        if (data?.type == CommunityType.STUDY_GROUP.name) {
          data?.groupView?.request?.isRequest = true;
          emit(ClubInformationSuccess());
        } else {
          getCommunity();
        }
      }, failure: (e) {
        if (e is BadRequest && e.code == ServerError.level_error) {
          emit(ClubInformationLevelFailureState(e.code));
        } else
          emit(ClubInformationFailureState(
              NetworkExceptions.getErrorMessage(e)));
      });
    }
  }

  void acceptToJoin(bool isAccepted) async {
    emit(ClubInformationLoadingState());
    final res = await repository.approveInvitation(
        isAccepted ? AdminAction.APPROVED : AdminAction.REJECTED,
        clubCommunityID ?? 0,
        appPreferences.getString(Const.ID) ?? "");
    res.when(success: (_) {
      getCommunity();
    }, failure: (e) {
      if (e is BadRequest && e.code == ServerError.level_error) {
        emit(ClubInformationLevelFailureState(e.code));
      } else
        emit(ClubInformationFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void leaveGroup() async {
    emit(ClubInformationLoadingState());
    final res = await repository.updateDeleteMember(
        memberAction: MemberActionType.LEAVE.name,
        userUuid: appPreferences.getString(Const.ID) ?? "",
        roles: data?.groupView?.myRoles,
        communityId: clubCommunityID ?? 0);
    res.when(success: (_) {
      emit(ClubInformationLeaveGroupSuccessState());
      getCommunity(
        isRefresh: true,
      );
    }, failure: (e) {
      emit(ClubInformationLeaveGroupFailureState());
    });
  }

  void getReportPost() async {
    emit(ClubInformationLoadingState());
    ApiResult<ReportReasonsData> reportPost = await graphqlRepository
        .getReportReasons(reasonType: ReportReasonType.POST.name);
    reportPost.when(success: (ReportReasonsData data) {
      listReportPost = data.fetchReportReasons!;
      emit(ReportReasonsSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void report({int? reasonId, String? postId}) async {
    emit(ClubInformationLoadingState());
    ApiResult<dynamic> result = await repository.reportPost(
        ReportRequest(
            reasonId: reasonId, postType: ReportReasonType.INDIVIDUAL.name),
        postId: postId);
    result.when(success: (dynamic data) {
      emit(ReportPostUserSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void pickImage({required bool isCamera, required bool isChangeAvatar}) async {
    final picker = ImagePicker();
    final file = await picker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery);
    if (file != null) {
      emit(ClubInformationLoadingState());
      ApiResult<List<UploadListUrlResponse>> listUrlResult =
          await repository.getListUploadUrl([file.name]);
      listUrlResult.when(success: (data) async {
        if (data.isNotEmpty) {
          await repository.uploadImage(data.first.url ?? "", File(file.path));
          isChangeAvatar
              ? await repository.adminupdateCommunity(
                  AdminUpdateCommunity(avatarUrl: data.first.key),
                  clubCommunityID)
              : await repository.adminupdateCommunity(
                  AdminUpdateCommunity(imageUrl: data.first.key),
                  clubCommunityID);
          getCommunity();
        }
      }, failure: (e) {
        emit(ClubInformationFailureState(NetworkExceptions.getErrorMessage(e)));
      });
    }
  }

  void getGroup() async {
    emit(ClubInformationLoadingState());
    ApiResult<dynamic> result =
        await repository.getGroupChannel(clubCommunityID ?? 0);
    result.when(success: (dynamic data) {
      channelId = data['id'];
      emit(GetChannelSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ClubInformationFailureState(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future<Uint8List> generateThumbnail(String? url) async {
    String? fileName = await VideoThumbnail.thumbnailFile(
      video: url ?? '',
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG,
      maxHeight: 350,
      quality: 75,
    );
    final file = File(fileName!);
    return file.readAsBytesSync();
  }
}
