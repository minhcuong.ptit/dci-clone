import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/club_info/club_information/club_information_cubit.dart';
import 'package:imi/src/page/club_info/club_information/club_information_state.dart';
import 'package:imi/src/page/club_info/club_information/widgets/club_information_actions.dart';
import 'package:imi/src/page/club_info/club_information/widgets/club_information_details.dart';
import 'package:imi/src/page/club_info/club_information/widgets/club_information_stats.dart';
import 'package:imi/src/page/club_info/club_information/widgets/club_invitation_widget.dart';
import 'package:imi/src/page/home_sow/home_sow_page.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/manage_members/manage_members.dart';
import 'package:imi/src/page/manage_reported_posts/manage_reported_posts_page.dart';
import 'package:imi/src/page/post/post_widget/post_widget.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/page/study_group_invite/study_group_invite.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/bottom_confirm_dialog.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/popup_report_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:io';

import '../../../data/network/response/post_data.dart';
import '../../../utils/const.dart';
import '../../../widgets/simple_choice_popup.dart';
import '../../comment/comment_page.dart';
import '../../edit_group_configuration/edit_group_configuration_page.dart';

class ViewClubPage extends StatefulWidget {
  final int communityId;
  final bool? checkGroup;

  const ViewClubPage({Key? key, required this.communityId, this.checkGroup})
      : super(key: key);

  @override
  _ViewClubPageState createState() => _ViewClubPageState();
}

class _ViewClubPageState extends State<ViewClubPage>
    with TickerProviderStateMixin {
  final RefreshController _refreshController = RefreshController();
  late ViewClubCubit _cubit;

  late TabController _tabController;
  late MainCubit _mainCubit;
  late AuthenticationCubit _authCubit;
  String? _appbarTitle;

  @override
  void initState() {
    super.initState();
    try {
      _mainCubit = BlocProvider.of<MainCubit>(context);
      _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    } catch (e) {}
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ViewClubCubit(
        repository: repository,
        graphqlRepository: graphqlRepository,
        clubCommunityID: widget.communityId);

    _cubit.getCommunity();
    _cubit.getListPost();
    _cubit.getMedia();
    _cubit.getReportPost();

    _tabController = new TabController(vsync: this, length: 3);
    if (widget.checkGroup == true) {
      _tabController.index = 1;
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void _pickAvatar(bool isAvatar) {
    showBarModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      expand: false,
      builder: (context) => SingleChildScrollView(
        child: SimpleChoicePopup(
          choices: [
            SimpleChoice(
              icon: R.drawable.ic_camera,
              title: R.string.profile_camera.tr(),
              onChosen: () =>
                  _cubit.pickImage(isCamera: true, isChangeAvatar: isAvatar),
            ),
            SimpleChoice(
                icon: R.drawable.ic_gallery,
                title: R.string.profile_gallery.tr(),
                onChosen: () => _cubit.pickImage(
                    isCamera: false, isChangeAvatar: isAvatar)),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<ViewClubCubit, ViewClubState>(
        listener: (context, state) {
          if (state is! ClubInformationLoadingState) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is ClubInformationFailureState) {
            Utils.showErrorSnackBar(context, state.error);
          }
          if (state is ClubInformationLeaveGroupSuccessState) {
            Utils.showToast(context, R.string.leave_group_success.tr(),
                fromTop: 50.h);
          }
          if (state is ClubInformationLevelFailureState) {
            _showLevelError();
          }
          if (state is ClubInformationLeaveGroupFailureState) {
            _showLeaveError();
          }
          if (state is ReportPostUserSuccess) {
            Utils.showToast(context, R.string.report_post_success.tr());
            // _cubit.refreshPostTimeline();
          }
          if (state is GetChannelSuccess) {
            NavigationUtils.navigatePage(
                context,
                ChatPage(
                  chatType: ChatType.group,
                  communityId: widget.communityId,
                  channelName: _cubit.data?.name,
                  channelId: _cubit.channelId,
                ));
          }
        },
        builder: (context, state) {
          setTitle();
          return Scaffold(
            backgroundColor: R.color.white,
            floatingActionButton: Visibility(
              visible: _tabController.index != 0 &&
                  _cubit.data?.groupView?.myRoles != null,
              child: GestureDetector(
                onTap: () {
                  NavigationUtils.openCreatePostPopup(context,
                          targetCommunityId: widget.communityId)
                      .then((value) {
                    _cubit.refreshPosts();
                  });
                  //NavigationUtils.navigatePage(context, MeditationDetailReportPage());
                },
                child: Container(
                  width: 42.w,
                  height: 42.w,
                  margin: EdgeInsets.only(bottom: 16.h),
                  decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.add_rounded,
                    color: R.color.white,
                  ),
                ),
              ),
            ),
            appBar: appBar(context, _appbarTitle ?? '',
                leadingWidget: IconButton(
                    onPressed: () {
                      NavigationUtils.pop(context);
                    },
                    icon: Icon(Icons.arrow_back_ios)),
                rightWidget: _options(),
                centerTitle: true),
            body: buildPage(context, state),
          );
        },
      ),
    );
  }

  Widget? _options() {
    return IconButton(
      onPressed: () {
        showModalBottomSheet(
            context: context,
            builder: (context) {
              return IntrinsicHeight(
                child: SafeArea(
                  child: Column(
                    children: [
                      if (_cubit.isAdminLeader)
                        InkWell(
                          onTap: () {
                            NavigationUtils.navigatePage(
                                context,
                                ManageMembersPage(
                                  clubCommunityId: widget.communityId,
                                ));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(16.h),
                            child: Row(
                              children: [
                                Image.asset(
                                  R.drawable.members,
                                  width: 24.h,
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                                Text(
                                  R.string.manage_members.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(fontSize: 13, height: 24 / 13),
                                )
                              ],
                            ),
                          ),
                        ),
                      if (_cubit.data?.groupView?.myRoles != null)
                        InkWell(
                          onTap: () {
                            NavigationUtils.pop(context);
                            NavigationUtils.rootNavigatePage(
                                context,
                                StudyGroupInvitePage(
                                  clubCommunityId: widget.communityId,
                                ));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(16.h),
                            child: Row(
                              children: [
                                Image.asset(
                                  R.drawable.ic_person_dci,
                                  width: 24.h,
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                                Text(
                                  R.string.invite_to_group.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(fontSize: 13, height: 24 / 13),
                                )
                              ],
                            ),
                          ),
                        ),
                      if (_cubit.isAdmin)
                        InkWell(
                          onTap: () {
                            NavigationUtils.pop(context);
                            NavigationUtils.navigatePage(
                                context,
                                EditGroupConfigurationPage(
                                  typeOfGroup: _cubit.data!.type!,
                                  communityId: widget.communityId,
                                ));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(16.h),
                            child: Row(
                              children: [
                                Image.asset(
                                  R.drawable.ic_edit,
                                  width: 24.h,
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                                Text(
                                  R.string.edit_group_configuration.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(fontSize: 13, height: 24 / 13),
                                )
                              ],
                            ),
                          ),
                        ),
                      // if (_cubit.isAdminLeader)
                      //   InkWell(
                      //     child: Padding(
                      //       padding: EdgeInsets.all(16.h),
                      //       child: Row(
                      //         children: [
                      //           Image.asset(
                      //             R.drawable.ic_edit,
                      //             width: 24.h,
                      //           ),
                      //           SizedBox(
                      //             width: 16.w,
                      //           ),
                      //           Text(
                      //             R.string.edit_group.tr(),
                      //             style: Theme.of(context)
                      //                 .textTheme
                      //                 .bold700
                      //                 .copyWith(fontSize: 13, height: 24 / 13),
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // if (_cubit.isAdminLeader)
                      //   InkWell(
                      //     child: Padding(
                      //       padding: EdgeInsets.all(16.h),
                      //       child: Row(
                      //         children: [
                      //           Image.asset(
                      //             R.drawable.ic_setting,
                      //             width: 24.h,
                      //           ),
                      //           SizedBox(
                      //             width: 16.w,
                      //           ),
                      //           Text(
                      //             R.string.group_settings.tr(),
                      //             style: Theme.of(context)
                      //                 .textTheme
                      //                 .bold700
                      //                 .copyWith(fontSize: 13, height: 24 / 13),
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      if (_cubit.data?.groupView?.myRoles != null)
                        InkWell(
                          onTap: () {
                            NavigationUtils.pop(context);
                            showModalBottomSheet(
                                context: context,
                                builder: (context) => BottomConfirmDialog(
                                    title: R.string
                                        .leave_group_confirmation_question
                                        .tr(),
                                    description: R
                                        .string.leave_group_confirmation_details
                                        .tr(),
                                    confirmString: R.string.confirm.tr(),
                                    rejectString: R.string.cancel.tr(),
                                    onConfirm: (isConfirmed) {
                                      if (isConfirmed) {
                                        _cubit.leaveGroup();
                                      }
                                      NavigationUtils.pop(context);
                                    }));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(16.h),
                            child: Row(
                              children: [
                                Image.asset(
                                  R.drawable.ic_leave,
                                  width: 24.h,
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                                Text(
                                  R.string.leave_group.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(fontSize: 13, height: 24 / 13),
                                )
                              ],
                            ),
                          ),
                        ),
                      InkWell(
                        onTap: () {
                          Clipboard.setData(
                                  ClipboardData(text: _cubit.data?.dynamicLink))
                              .whenComplete(() => NavigationUtils.pop(context));
                        },
                        child: Padding(
                          padding: EdgeInsets.all(16.h),
                          child: Row(
                            children: [
                              Image.asset(
                                R.drawable.ic_link,
                                width: 24.h,
                              ),
                              SizedBox(
                                width: 16.w,
                              ),
                              Text(
                                R.string.copy_link.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(fontSize: 13, height: 24 / 13),
                              )
                            ],
                          ),
                        ),
                      ),
                      if (_cubit.isAdminLeader)
                        InkWell(
                          onTap: () {
                            NavigationUtils.pop(context);
                            NavigationUtils.rootNavigatePage(
                                context,
                                ManageReportPostsPage(
                                  communityId: widget.communityId,
                                ));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(16.h),
                            child: Row(
                              children: [
                                Image.asset(
                                  R.drawable.ic_report_user,
                                  width: 24.h,
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                                Text(
                                  R.string.reported.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(fontSize: 13, height: 24 / 13),
                                )
                              ],
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              );
            });
      },
      icon: Icon(Icons.more_vert_outlined),
    );
  }

  void setTitle() {
    if (_appbarTitle?.isEmpty ?? true)
      WidgetsBinding.instance.addPostFrameCallback((_) {
        setState(() {
          if (_cubit.data?.type != null) {
            _appbarTitle = (_cubit.data?.type == CommunityType.STUDY_GROUP.name)
                ? R.string.study_groups_appbar.tr().toUpperCase()
                : R.string.dci_community.tr().toUpperCase();
          }
        });
      });
  }

  Widget buildPage(BuildContext context, ViewClubState state) {
    return GestureDetector(
      onTap: () => Utils.hideKeyboard(context),
      child: StackLoadingView(
        visibleLoading: state is ClubInformationLoadingState,
        child: SmartRefresher(
            controller: _refreshController,
            enablePullUp: _tabController.index != 0,
            primary: true,
            onRefresh: () {
              // _cubit.getMyCommunity(isRefresh: true);
              _cubit.getCommunity(isRefresh: true);

              if (_tabController.index == 1) {
                _cubit.getListPost(isRefresh: true);
              } else if (_tabController.index == 2) {
                _cubit.getMedia(isRefresh: true);
              }
            },
            onLoading: () {
              if (_tabController.index == 1) {
                _cubit.getListPost(isLoading: true);
              } else if (_tabController.index == 2) {
                _cubit.getMedia();
              }
            },
            child: CustomScrollView(
              slivers: [
                _avatarAndBackground(),
                _sliverSpace(16),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0.w),
                        child: Text(
                          _cubit.data?.name ?? "",
                          maxLines: 100,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.bold700.copyWith(
                              fontSize: 18.sp, color: R.color.textBlack),
                        ),
                      ),
                      if (_cubit.data?.type ==
                          CommunityType.STUDY_GROUP.name) ...[
                        Padding(
                          padding: EdgeInsets.only(top: 8.h),
                          child: Text(
                            '${_cubit.data?.communityInfo?.communityLevel?.first?.levelName ?? 0}',
                            style: Theme.of(context)
                                .textTheme
                                .labelSmallText
                                .copyWith(
                                    height: 22 / 18,
                                    fontSize: 16.sp,
                                    color: R.color.shadesGray,
                                    fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    ],
                  ),
                ),
                _sliverSpace(16),
                SliverToBoxAdapter(
                    child: ClubInformationStat(
                  communityId: widget.communityId,
                  onTapPosts: () {
                    if (_tabController.index != 1 &&
                        _cubit.data?.groupView?.myRoles != null) {
                      _tabController.animateTo(1);
                      setState(() {});
                    }
                  },
                )),
                _sliverSpace(16),
                SliverToBoxAdapter(
                  child: ClubInformationActionButtons(
                      communityId: widget.communityId),
                ),
                _sliverSpace(16),
                if ((_cubit.data?.groupView?.request?.isRequest ?? false) &&
                    (_cubit.data?.groupView?.request?.inviter != null)) ...[
                  SliverToBoxAdapter(
                    child: ClubInvitationWidget(),
                  ),
                  _sliverSpace(16),
                ],
                _tabBar(),
                _sliverSpace(16),
                _tabView(context, state)
              ],
            )),
      ),
    );
  }

  Widget _sliverSpace(double height) {
    return SliverToBoxAdapter(
      child: SizedBox(height: height.h),
    );
  }

  Widget _avatarAndBackground() {
    return SliverToBoxAdapter(
      child: SizedBox(
        height: 181.h,
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: 136.h,
              child:  CachedNetworkImage(
                      imageUrl: _cubit.data?.imageUrl ?? "",fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          Image.asset(
                            R.drawable.bg_group,
                            fit: BoxFit.cover,
                          ),
                      errorWidget: (context, url, error) => Image.asset(
                        R.drawable.bg_group,
                        fit: BoxFit.cover,
                      ),
                    ),
            ),
            _cubit.isAdmin
                ? _changeAvatar(
                    top: 10.h,
                    right: 20.w,
                    isAvatar: false,
                  )
                : SizedBox.shrink(),
            Positioned(
              top: 91.h,
              left: MediaQuery.of(context).size.width / 2 - 45.h,
              child: Container(
                child: AvatarWidget(
                  size: 90.h,
                  avatar: _cubit.data?.avatarUrl,
                ),
              ),
            ),
            _cubit.isAdmin
                ? _changeAvatar(
                    top: 150.h,
                    right: MediaQuery.of(context).size.width / 2 - 42.w,
                    isAvatar: true,
                  )
                : SizedBox.shrink()
          ],
        ),
      ),
    );
  }

  Widget _changeAvatar(
      {required double top, required double right, required bool isAvatar}) {
    return Positioned(
      top: top,
      right: right,
      child: GestureDetector(
        onTap: () => _pickAvatar(isAvatar),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: R.color.white,
              width: 2,
            ),
            shape: BoxShape.circle,
          ),
          child: CircleAvatar(
            backgroundColor: R.color.grey300,
            radius: 12,
            child: Image.asset(
              isAvatar ? R.drawable.ic_camera : R.drawable.ic_edit,
              height: isAvatar ? 16.h : 22.h,
              width: isAvatar ? 16.w : 22.w,
            ),
          ),
        ),
      ),
    );
  }

  Widget _tabBar() {
    final _textTheme = Theme.of(context)
        .textTheme
        .bold700
        .copyWith(fontSize: 12.sp, color: R.color.gray);
    if (_cubit.data?.groupView?.myRoles == null &&
        _cubit.data?.privacy == GroupPrivacy.PRIVATE.name) {
      return SliverToBoxAdapter(
        child: Align(
          alignment: Alignment.center,
          child: Column(
            children: [
              Text(R.string.profile.tr(), style: _textTheme),
              SizedBox(
                height: 12.h,
              ),
              Container(
                height: 1.h,
                color: R.color.lightBlue,
                width: 50.w,
              )
            ],
          ),
        ),
      );
    }
    return SliverPersistentHeader(
      pinned: true,
      floating: true,
      delegate: StickyTabBarDelegate(
          child: TabBar(
        indicatorColor: R.color.lightBlue,
        controller: _tabController,
        onTap: (_) {
          if (_tabController.previousIndex != _tabController.index)
            setState(() {});
        },
        indicatorSize: TabBarIndicatorSize.label,
        tabs: [
          Tab(
            child: Text(R.string.profile.tr().toUpperCase(), style: _textTheme),
          ),
          Tab(
            child: Text(R.string.post.tr().toUpperCase(), style: _textTheme),
          ),
          Tab(
            child: Text(R.string.media.tr().toUpperCase(),
                textAlign: TextAlign.center, style: _textTheme),
          ),
        ],
      )),
    );
  }

  void gotoPostDetail(PostData post) {
    NavigationUtils.rootNavigatePage(
        context,
        CommentPage(
          postId: int.parse(post.id ?? '0'),
          onCommentSuccess: (countComment) {
            post.commentNumber = countComment;
            _cubit.refreshPosts();
          },
          onLikeSuccess: (like, likeNumber) {
            post.isLike = like;
            post.likeNumber = likeNumber;
            _cubit.refreshPosts();
          },
          onRemovePostSuccess: () {
            _cubit.refreshPosts();
            _cubit.getCommunity();
          },
          onBookmarkSuccess: (bool bookmark) {
            post.isBookmark = bookmark;
          },
        ));
  }

  Widget _tabView(BuildContext ctx, ViewClubState state) {
    switch (_tabController.index) {
      case 0:
        return SliverToBoxAdapter(
          child: ClubInformationDetails(),
        );
      case 1:
        if (_cubit.listPost.length == 0 &&
            state is! ClubInformationLoadingState) {
          return SliverToBoxAdapter(
              child: Visibility(
            visible: state is GetListPostEmpty,
            child: Padding(
              padding: EdgeInsets.only(top: 16.0.h),
              child: Align(
                  alignment: Alignment.center,
                  child: Text(R.string.no_data_to_show.tr())),
            ),
          ));
        }
        return SliverList(delegate: SliverChildBuilderDelegate((ctx, index) {
          if (index >= _cubit.listPost.length) return null;
          final post = _cubit.listPost[index];
          return Column(
            children: [
              PostWidget(
                reportPost: () {
                  showBarModalBottomSheet(
                      context: context,
                      builder: (_) {
                        return Container(
                          height: 480.h,
                          padding: EdgeInsets.symmetric(
                              horizontal: 20.h, vertical: 20.h),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                R.string.article_report.tr(),
                                style: Theme.of(_)
                                    .textTheme
                                    .bold700
                                    .copyWith(fontSize: 16.sp),
                              ),
                              Expanded(
                                child: Text(
                                  R.string.reason_report.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 11.sp),
                                ),
                              ),
                              ListView.separated(
                                shrinkWrap: true,
                                itemCount: _cubit.listReportPost.length,
                                itemBuilder: (context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      NavigationUtils.pop(context);
                                      showBarModalBottomSheet(
                                          context: context,
                                          builder: (context) {
                                            return PopupWidget(
                                              title:
                                                  R.string.article_report.tr(),
                                              reasons: R.string.bad_post.tr(),
                                              onReport: () {
                                                NavigationUtils.pop(context);
                                                _cubit.report(
                                                    postId: post.id,
                                                    reasonId: _cubit
                                                        .listReportPost[index]
                                                        .id);
                                              },
                                            );
                                          });
                                    },
                                    child: Text(
                                      _cubit.listReportPost[index].reason ?? "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bold700
                                          .copyWith(fontSize: 13.sp),
                                    ),
                                  );
                                  // }
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return Divider();
                                },
                              ),
                            ],
                          ),
                        );
                      });
                },
                data: post,
                isFromGroupPage: true,
                viewProfileCallback: () {
                  if (post.communityId != null) {
                    NavigationUtils.navigatePage(context,
                        ViewProfilePage(communityId: post.communityId));
                  }
                },
                likeCallback: () {
                  if (post.isLike ?? false) {
                    _cubit.reaction(int.parse(post.id ?? "0"), false);
                  } else {
                    _cubit.reaction(int.parse(post.id ?? "0"), true);
                  }
                },
                shareCallback: () {},
                moreCallback: () {},
                showLikeCallback: () {},
                posDetailCallback: () {
                  gotoPostDetail(post);
                },
                commentCallback: () {
                  gotoPostDetail(post);
                },
                reportCallback: () {},
                onBookmarkSuccess: (bool bookmark) {
                  setState(() {
                    post.isBookmark = bookmark;
                  });
                },
              ),
              Container(
                height: 8.h,
                color: R.color.greyF4,
              )
            ],
          );
        }));
      default:
        if (_cubit.listMedia.isEmpty) {
          return SliverToBoxAdapter(
              child: Padding(
            padding: EdgeInsets.only(top: 16.0.h),
            child: Align(
                alignment: Alignment.center,
                child: Text(R.string.no_data_to_show.tr())),
          ));
        } else {
          final res = <Widget>[];
          for (int i = 0; i < _cubit.listMedia.length; i++) {
            final e = _cubit.listMedia[i];
            res.add(InkWell(
              onTap: () {
                if (e.communityId != null) {
                  NavigationUtils.rootNavigatePage(
                      context,
                      CommentPage(
                        postId: e.postId!,
                        onCommentSuccess: (countComment) {},
                        onLikeSuccess: (bool like, int likeNumber) {},
                        onRemovePostSuccess: () {
                          _cubit.getListPost();
                        },
                        onBookmarkSuccess: (bool bookmark) {},
                      ));
                }
              },
              child: Container(
                width: context.width / 3,
                height:context.height / 3,
                padding: EdgeInsets.all(8.h),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: (e.s3Key!.contains('.jpg?') ||
                          e.s3Key!.contains('.png?'))
                      ? e.s3Key == null || state is ClubInformationLoadingState
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 20.h,
                                  width: 20.w,
                                  child: CupertinoActivityIndicator(
                                    animating: true,
                                    color: R.color.primaryColor,
                                  ),
                                ),
                              ],
                            )
                          : CachedNetworkImage(
                              memCacheHeight: 180,
                              memCacheWidth: 130,
                              imageUrl: e.s3Key ?? "",
                              fit: BoxFit.cover,
                            )
                      : Stack(
                          children: [
                            FutureBuilder<Uint8List>(
                              future: _cubit.generateThumbnail(e.s3Key),
                              builder: (context, snapshot) => snapshot.data !=
                                      null
                                  ? Image.memory(
                                      snapshot.data!,
                                      height: 350,
                                      width: MediaQuery.of(context).size.width,
                                    )
                                  : Center(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 40.h,
                                            width: 40.w,
                                            child: CupertinoActivityIndicator(
                                              animating: true,
                                              color: R.color.primaryColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                            ),
                            Visibility(
                              visible: state is! ClubInformationLoadingState,
                              child: Positioned(
                                  top: 40.h,
                                  left: 38.w,
                                  child: Icon(
                                    CupertinoIcons.play_circle,
                                    color: R.color.white,
                                    size: 40.h,
                                  )),
                            )
                          ],
                        ),
                ),
              ),
            ));
          }
          return SliverGrid.count(
            crossAxisCount: 3,
            children: res,
          );
        }
    }
  }

  void _showLevelError() {
    Utils.showCommonBottomSheet(
        context: context,
        title: R.string.annoucement_from_dci.tr(),
        formattedDetails: R.string.annoucement_from_dci_details.tr(args: [
          _cubit.data?.communityInfo?.communityLevel?.first?.level
                  ?.toString() ??
              "0"
        ]),
        buttons: [
          ButtonWidget(
            title: R.string.see_my_learned_program.tr(),
            onPressed: () {
              NavigationUtils.pop(context);
              NavigationUtils.navigatePage(
                  context, ViewProfilePage(communityId: null));
            },
            backgroundColor: R.color.lightBlue,
            textSize: 14.sp,
            padding: EdgeInsets.symmetric(vertical: 16.h),
          ),
          ButtonWidget(
            title: R.string.understanded.tr(),
            onPressed: () {
              NavigationUtils.pop(context);
            },
            textSize: 14.sp,
            backgroundColor: R.color.orange,
            padding: EdgeInsets.symmetric(vertical: 16.h),
          )
        ]);
  }

  void _showLeaveError() {
    Utils.showCommonBottomSheet(
        context: context,
        title: R.string.annoucement.tr(),
        formattedDetails: R.string.leave_group_error.tr(),
        buttons: [
          ButtonWidget(
            title: R.string.understanded.tr(),
            onPressed: () {
              NavigationUtils.pop(context);
            },
            textColor: R.color.primaryColor,
            backgroundColor: R.color.white,
            borderColor: R.color.primaryColor,
            textSize: 14.sp,
            padding: EdgeInsets.symmetric(vertical: 16.h),
          ),
        ]);
  }
}

class StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar child;

  StickyTabBarDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: this.child,
    );
  }

  @override
  double get maxExtent => this.child.preferredSize.height;

  @override
  double get minExtent => this.child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
