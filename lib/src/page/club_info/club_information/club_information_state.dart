import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ViewClubState {
  ViewClubState() : super();
}

class InitialClubInformationState extends ViewClubState {
  @override
  String toString() {
    return 'InitialClubInformationState{}';
  }
}

class ClubInformationLevelFailureState extends ViewClubState {
  final String? error;

  ClubInformationLevelFailureState(this.error);

  @override
  String toString() {
    return 'ClubInformationFailure{error: $error}';
  }
}

class ClubInformationLeaveGroupSuccessState extends ViewClubState {
  ClubInformationLeaveGroupSuccessState();
}

class ClubInformationLeaveGroupFailureState extends ViewClubState {
  ClubInformationLeaveGroupFailureState();
}

class ClubInformationFailureState extends ViewClubState {
  final String? error;

  ClubInformationFailureState(this.error);

  @override
  String toString() {
    return 'ClubInformationFailure{error: $error}';
  }
}

class ClubInformationLoadingState extends ViewClubState implements Equatable {
  @override
  String toString() {
    return 'ClubInformationLoading{}';
  }

  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class ClubInformationSuccess extends ViewClubState {
  @override
  String toString() {
    return 'ClubInformationSuccess{}';
  }
}

class GetDataSuccess extends ViewClubState {
  @override
  String toString() {
    return 'GetDataSuccess: {}';
  }
}

class DownloadPdfSuccess extends ViewClubState {
  @override
  String toString() {
    return 'DownloadPdfSuccess: {}';
  }
}

class BillClubSuccess extends ViewClubState {
  final String? html;

  BillClubSuccess(this.html);

  @override
  String toString() {
    return 'BillClubSuccess: {html: $html}';
  }
}

class LeaveClubSuccess extends ViewClubState {
  @override
  String toString() {
    return 'LeaveClubSuccess: {}';
  }
}

class ReportReasonsSuccess extends ViewClubState {
  @override
  String toString() {
    return 'ReportReasonsSuccess: {}';
  }
}

class ReportPostUserSuccess extends ViewClubState {
  @override
  String toString() {
    return 'ReportPostUserSuccess: {}';
  }
}

class ClubGetAssociationInvitationCountSuccess extends ViewClubState {
  @override
  String toString() {
    return 'ClubGetAssociationInvitationCountSuccess: {}';
  }
}

class GetChannelSuccess extends ViewClubState {
  @override
  String toString() {
    return 'GetChannelSuccess: {}';
  }
}

class GetListPostEmpty extends ViewClubState {
  @override
  String toString() {
    return 'GetListPostEmpty: {}';
  }
}