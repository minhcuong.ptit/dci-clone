abstract class DetailDciNotificationState {}
class DetailDciNotificationInitial extends DetailDciNotificationState {}

class DetailDciNotificationLoading extends DetailDciNotificationState {
  @override
  String toString() {
    return 'DetailDciNotificationLoading{}';
  }
}

class DetailDciNotificationFailure extends DetailDciNotificationState {
  final String error;

  DetailDciNotificationFailure(this.error);

  @override
  String toString() {
    return 'DetailDciNotificationFailure{error: $error}';
  }
}

class DetailDciNotificationSuccess extends DetailDciNotificationState {
  @override
  String toString() {
    return 'DetailDciNotificationSuccess{}';
  }
}