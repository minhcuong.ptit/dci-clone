import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/dci_notification/detail_dci_notification_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../widgets/custom_appbar.dart';
import 'detail_dci_notification_cubit.dart';

class DciNotificationPage extends StatefulWidget {
  final int notificationPrivateId;

  const DciNotificationPage({Key? key, required this.notificationPrivateId})
      : super(key: key);

  @override
  State<DciNotificationPage> createState() => _DciNotificationPageState();
}

class _DciNotificationPageState extends State<DciNotificationPage> {
  RefreshController _refreshController = RefreshController();
  late DetailDciNotificationCubit _cubit;
  DateFormat dateFormat = DateFormat("HH:mm dd/MM/yyyy");

  @override
  void initState() {
    // TODO: implement initState
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    _cubit = DetailDciNotificationCubit(repository, appGraphqlRepository);
    _cubit.getDetailDciNotification(id: widget.notificationPrivateId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.notification_from_dci.tr().toUpperCase(),
          centerTitle: true,
          backgroundColor: R.color.primaryColor,
          iconColor: R.color.white,
          titleColor: R.color.white),
      body: BlocProvider(
        create: (BuildContext context) => _cubit,
        child:
            BlocConsumer<DetailDciNotificationCubit, DetailDciNotificationState>(
              listener: (context, state) {
                if(state is DetailDciNotificationLoading){
                  _refreshController.refreshCompleted();
                  _refreshController.loadComplete();
                }
              },
          builder: ((context, state) {
            return StackLoadingView(
              visibleLoading: state is DetailDciNotificationLoading,
              child: SmartRefresher(
                onRefresh: (){
                  _cubit.getDetailDciNotification(id: widget.notificationPrivateId);
                },
                controller: _refreshController,
                child: state is DetailDciNotificationLoading
                    ? SizedBox.shrink()
                    : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _cubit.detailNotification?.body?.toUpperCase() ?? '',
                          style: Theme.of(context).textTheme.title2,
                        ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Row(
                                children: [
                                  Image.asset(
                                    R.drawable.ic_promoter,
                                    width: 24.w,
                                  ),
                                  SizedBox(
                                    width: 12.w,
                                  ),
                                  Text(
                                    dateFormat
                                        .format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                _cubit.detailNotification
                                                        ?.createdDate ??
                                                    0))
                                        .toString(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmallText
                                        .copyWith(color: R.color.gray600),
                                  ),
                                ],
                              ),
                        SizedBox(
                          height: 12.h,
                        ),
                        Text(
                          _cubit.detailNotification?.text ?? '',
                          style: Theme.of(context)
                              .textTheme
                              .bodySmallText
                              .copyWith(color: R.color.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
