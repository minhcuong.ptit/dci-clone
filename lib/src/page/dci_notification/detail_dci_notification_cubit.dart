
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/dci_notification_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import 'detail_dci_notification_state.dart';

class DetailDciNotificationCubit extends Cubit<DetailDciNotificationState> {
   final AppRepository repository;
    GetPrivateNotificationDetailById? detailNotification;
   final AppGraphqlRepository graphqlRepository;
   DetailDciNotificationCubit(this.repository, this.graphqlRepository)
   : super(DetailDciNotificationInitial());
  void getDetailDciNotification({required int id,bool isRefresh=false})async{
    emit(DetailDciNotificationLoading());
    ApiResult<GetPrivateNotificationDetailById> res =await graphqlRepository.getDetailNotificationDciAdmin(id);
    res.when(success: (GetPrivateNotificationDetailById data){
      detailNotification=data;
      emit(DetailDciNotificationSuccess());
    }, failure: (NetworkExceptions error){
      DetailDciNotificationFailure(NetworkExceptions.getErrorMessage(error));
    });
  }
}
