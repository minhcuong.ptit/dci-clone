import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/page/community/widget/search_bar.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_search_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/study_group_widget.dart';
import '../club_info/club_information/club_information.dart';
import 'study_group.dart';

class StudyGroupPage extends StatefulWidget {
  final CommunityStatus? communityStatus;
  final VoidCallback? onRefresh;
  final List<int?>? levelIds;

  const StudyGroupPage(
      {Key? key, this.onRefresh, this.communityStatus, this.levelIds})
      : super(key: key);

  @override
  _StudyGroupPageState createState() => _StudyGroupPageState();
}

class _StudyGroupPageState extends State<StudyGroupPage> {
  late StudyGroupCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  final ScrollController scrollController = ScrollController();
  final TextEditingController _controller = TextEditingController();
  bool hideAppBar = false;
  bool isFilter = false;
  bool isCollapse = true;
  List<DayOfWeek> listDay = DayOfWeek.values;
  List<String> listDaySearch = [];
  Timer? _debounce;

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = StudyGroupCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.communityStatus = widget.communityStatus;
    _cubit.getListContent();
    scrollController.addListener(_onScroll);
    _cubit.getLevels(widget.levelIds ?? []);
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    scrollController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        NavigationUtils.pop(context);
        Utils.hideKeyboard(context);
        return true;
      },
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<StudyGroupCubit, StudyGroupState>(
          listener: (context, state) {
            if (state is! StudyGroupLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
            if (state is! SubmitStudyGroupSuccess) {
              if (widget.onRefresh != null) {
                widget.onRefresh!();
              }
            }
            if (state is StudyGroupFailure) {
              Utils.showErrorSnackBar(context, state.error);
            }
          },
          builder: (context, state) {
            return _buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, StudyGroupState state) {
    return Scaffold(
      backgroundColor: R.color.white,
      appBar: AppBar(
        backgroundColor: R.color.primaryColor,
        automaticallyImplyLeading: false,
        title: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  child: Icon(
                    CupertinoIcons.back,
                    color: R.color.white,
                  ),
                  onTap: () {
                    Utils.hideKeyboard(context);
                    NavigationUtils.pop(context);
                  },
                ),
                Text(
                  R.string.groups_suggestion.tr().toUpperCase(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 18.sp, color: R.color.white),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      hideAppBar = !hideAppBar;
                    });
                  },
                  child: Icon(
                    CupertinoIcons.search,
                    size: 22.h,
                    color: R.color.white,
                  ),
                )
              ],
            ),
            Visibility(
              visible: hideAppBar,
              child: Container(
                color: R.color.primaryColor,
                child: Row(
                  children: [
                    InkWell(
                      child: Icon(
                        CupertinoIcons.back,
                        color: R.color.white,
                      ),
                      onTap: () {
                        Utils.hideKeyboard(context);
                        NavigationUtils.pop(context);
                      },
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 15.h, vertical: 5.h),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10.h, vertical: 8.h),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.h),
                          color: R.color.white,
                        ),
                        child: Row(
                          children: [
                            InkWell(
                                onTap: () {
                                  clearFilter();
                                  _cubit.getListContent(isRefresh: true,
                                      keyWord: _controller.text.trim());
                                },
                                child: Icon(CupertinoIcons.search,
                                    size: 20.h, color: R.color.black)),
                            SizedBox(width: 5.h),
                            Expanded(
                              child: CupertinoSearchTextField(
                                autofocus: true,
                                padding:
                                    const EdgeInsetsDirectional.fromSTEB(0, 5, 5, 5),
                                decoration: BoxDecoration(
                                    color: R.color.white,
                                    borderRadius: BorderRadius.circular(30.h)),
                                controller: _controller,
                                prefixIcon: const SizedBox.shrink(),
                                placeholder: R.string.search.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyRegular
                                    .copyWith(
                                      color: R.color.black,
                                    ),
                                // onChanged: _onSearchChanged(),
                                onSubmitted: (text) {
                                  Utils.hideKeyboard(context);
                                  clearFilter();
                                  _cubit.getListContent(isRefresh: true,dayOfWeek: listDaySearch,keyWord:  _controller.text.trim());
                                  //  Utils.hideKeyboard(context);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          StackLoadingView(
            visibleLoading: state is StudyGroupLoading,
            child: SmartRefresher(
              enablePullUp: true,
              controller: _refreshController,
              onRefresh: () {
                  _cubit.getListContent(
                      isRefresh: true,dayOfWeek: listDaySearch, keyWord: _controller.text.trim());
              },
              onLoading: () {
                     _cubit.listCommunity.length>=20? _cubit.getListContent(
                      loadMore: true,dayOfWeek: listDaySearch, keyWord: _controller.text.trim()):null;
              },
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                children: [
                  SizedBox(height: 5.h),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          R.string.filter_by_suggestions.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .medium500
                              .copyWith(fontSize: 16.sp, height: 20.h / 16.sp),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isFilter = !isFilter;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 5.h, horizontal: 20.h),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: R.color.grey200),
                            boxShadow: [
                              BoxShadow(
                                color: R.color.grey100,
                                spreadRadius: 5,
                                blurRadius: 10,
                                offset: const Offset(4, 6),
                              ),
                            ],
                            color: isFilter == false
                                ? R.color.white
                                : R.color.primaryColor,
                            borderRadius: BorderRadius.circular(30.h),
                          ),
                          child: Image.asset(
                            R.drawable.ic_filter_list,
                            color: isFilter == false
                                ? R.color.black
                                : R.color.white,
                            height: 20.h,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.h),
                  (_cubit.state is! StudyGroupLoading) &&
                          _cubit.listCommunity.isEmpty
                      ? Visibility(
                          visible: state is GetListCommunitySuccess,
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.2),
                            child: Center(
                              child: Text(
                                R.string.there_are_no_suggested_groups.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 14.sp),
                              ),
                            ),
                          ),
                        )
                      : homeStudyGroupsSuggestionGrid()
                ],
              ),
            ),
          ),
          buildFilterCourse(context)
        ],
      ),
    );
  }

  Widget homeStudyGroupsSuggestionGrid() {
    final maxWidthOfCell = (ScreenUtil().screenWidth - 24.w - 16.w) / 2;
    return GridView.builder(
      primary: true,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: _cubit.listCommunity.length,
      itemBuilder: (context, index) {
        CommunityDataV2 data = _cubit.listCommunity[index];
        // bool isSelected = _cubit.listSelectedCommunity
        //         .indexWhere((element) => element.id == data.id) >=
        //     0;
        bool isSelected = false;
        return StudyGroupWidget(
          width: maxWidthOfCell,
          data: data,
          onTap: () {
            NavigationUtils.rootNavigatePage(
                context,
                ViewClubPage(
                  communityId: data.id ?? 0,
                ));
            //gotoClubDetail(data.id);
            //_cubit.selectCommunity(data);
          },
          isSelected: isSelected,
          isShowStar: true,
        );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 8.w,
          mainAxisSpacing: 16.h,
          crossAxisCount: 2,
          childAspectRatio: 8 / 10),
    );
  }

  Widget buildFilterCourse(BuildContext context) {
    return Visibility(
      visible: isFilter,
      child: Positioned(
        top: 40.h,
        left: 46.h,
        right: 20.h,
        bottom: isCollapse ? 100.h : MediaQuery.of(context).size.height * 0.5,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8.h, vertical: 10.h),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.h),
            color: R.color.white,
            border: Border.all(width: 1, color: R.color.grey100),
            boxShadow: [
              BoxShadow(
                color: R.color.grey100,
                spreadRadius: 1,
                blurRadius: 5,
                offset: const Offset(0, -3),
              ),
            ],
          ),
          child: Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  R.string.living_time.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, height: 16.h / 12.sp),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(height: 8.h),
              Container(
                padding: EdgeInsets.symmetric(vertical: 8.h),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: R.color.grey100),
                  borderRadius: BorderRadius.circular(5.h),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: List.generate(
                          listDay.length,
                          (index) => GestureDetector(
                                onTap: () {
                                  _cubit.selectDay(
                                      !_cubit.isChooseDay[index], index);
                                  if (_cubit.isChooseDay[index] == false) {
                                    listDaySearch
                                        .remove(DayOfWeek.values[index].name);
                                  } else {
                                    listDaySearch
                                        .add(DayOfWeek.values[index].name);
                                  }
                                },
                                child: Container(
                                  width: 30.w,
                                  height: 30.h,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: _cubit.isChooseDay[index]
                                          ? R.color.primaryColor
                                          : R.color.white,
                                      borderRadius: BorderRadius.circular(3.h),
                                      border:
                                          Border.all(color: R.color.grey100)),
                                  child: Text(
                                    listDay[index].title,
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                          fontSize: 14.sp,
                                          height: 18.h / 14.sp,
                                          color: _cubit.isChooseDay[index]
                                              ? R.color.white
                                              : R.color.black,
                                        ),
                                  ),
                                ),
                              )),
                    ),
                    Container(
                      height: 1,
                      width: double.infinity,
                      color: R.color.grey100,
                      margin: EdgeInsets.all(10.h),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.h),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              R.string.start_time_frame.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, height: 16.h / 12.sp),
                            ),
                          ),
                          SizedBox(width: 7.h),
                          GestureDetector(
                            onTap: () async {
                              TimeOfDay? pickedTime = await showTimePicker(
                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              if (pickedTime != null) {
                                DateTime? parsedTime = DateFormat.Hm().parse(
                                    pickedTime.format(context).toString());
                                _cubit.selectStartTime(parsedTime);
                                Utils.hideKeyboard(context);
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.h, horizontal: 10.h),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: R.color.grey100, width: 1),
                                borderRadius: BorderRadius.circular(5.h),
                              ),
                              child: Text(
                                _cubit.startTime == null
                                    ? "00:00"
                                    : DateUtil.parseDateToString(
                                        _cubit.startTime, Const.TIME_FORMAT),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp, height: 18.h / 12.sp),
                              ),
                            ),
                          ),
                          SizedBox(width: 7.h),
                          Image.asset(R.drawable.ic_arrow_right, width: 22.h),
                          SizedBox(width: 7.h),
                          GestureDetector(
                            onTap: () async {
                              TimeOfDay? pickedTime = await showTimePicker(
                                initialTime: TimeOfDay.now(),
                                context: context,
                              );
                              if (pickedTime != null) {
                                DateTime? parsedTime = DateFormat.Hm().parse(
                                    pickedTime.format(context).toString());
                                _cubit.selectEndTime(parsedTime);
                                Utils.hideKeyboard(context);
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.h, horizontal: 10.h),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: R.color.grey100, width: 1),
                                borderRadius: BorderRadius.circular(5.h),
                              ),
                              child: Text(
                                _cubit.endTime == null
                                    ? "00:00"
                                    : DateUtil.parseDateToString(
                                        _cubit.endTime, Const.TIME_FORMAT),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp, height: 18.h / 12.sp),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 8.h),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  R.string.study_program.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, height: 16.h / 12.sp),
                ),
              ),
              SizedBox(height: 8.h),
              Visibility(
                visible: isCollapse,
                child: Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: _cubit.levels.length,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          onTap: () {
                            _cubit.selectStudy(
                                !(_cubit.isChooseStudy[_cubit.levels[index]] ??
                                    false),
                                index);
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 5.h),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (_cubit.isChooseStudy[_cubit.levels[index]] ??
                                        false)
                                    ? Icon(
                                        Icons.check_box,
                                        size: 20.h,
                                        color: R.color.primaryColor,
                                      )
                                    : Icon(
                                        Icons.check_box_outline_blank,
                                        size: 20.h,
                                        color: R.color.grey,
                                      ),
                                SizedBox(width: 8.h),
                                Expanded(
                                  child: Text(
                                    _cubit.levels[index].name ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            height: 18.h / 12.sp,
                                            color: (_cubit.isChooseStudy[
                                                        _cubit.levels[index]] ??
                                                    false)
                                                ? R.color.primaryColor
                                                : R.color.black),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    isCollapse = !isCollapse;
                  });
                },
                child: Text(
                  isCollapse ? R.string.less.tr() : R.string.view_all.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, color: R.color.primaryColor),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                color: R.color.grey100,
                height: 1,
                margin: EdgeInsets.symmetric(vertical: 10.h),
              ),
              Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          clearFilter();
                          isFilter=false;
                        });
                      },
                      child: Text(
                        R.string.clear_filter.tr(),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 12.sp,
                            height: 18.h / 12.sp,
                            color: R.color.primaryColor),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      isFilter = false;
                      _cubit.getListContent(isRefresh: true,dayOfWeek: listDaySearch,keyWord:  _controller.text.trim());
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.h, vertical: 5.h),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.h),
                          color: R.color.primaryColor),
                      child: Text(
                        R.string.search.tr(),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 12.sp,
                            height: 16.h / 12.sp,
                            color: R.color.white),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onScroll() {
    var _positionPixels = scrollController.position;
    if (_positionPixels.atEdge &&
        _positionPixels.pixels != 0 &&
        _cubit.canRequestLoadMore) {
      _cubit.getListContent(loadMore: true);
    }
  }

  void gotoClubDetail(communityId) {
    NavigationUtils.navigatePage(
        context,
        ViewClubPage(
          communityId: communityId,
        )).then((value) {
      // _cubit.getListFollow(
      //     isRefresh: true, type: widget.type);
    });
  }

  void _onSearchChanged(String text) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      _cubit.getListContent(keyWord: text.trim());
    });
  }

  void clearFilter(){
    _cubit.isChooseStudy.clear();
    for (int i = 0; i < listDay.length; i++) {
      _cubit.selectDay(!_cubit.isChooseDay[i], i);
      _cubit.isChooseDay[i] = false;
      listDaySearch.remove(DayOfWeek.values[i].name);
    }
    _cubit.startTime = null;
    _cubit.endTime = null;
  }
}
