import 'package:flutter/material.dart';

@immutable
abstract class StudyGroupState {
  StudyGroupState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialStudyGroupState extends StudyGroupState {}

class StudyGroupLoading extends StudyGroupState {
  @override
  String toString() => 'StudyGroupLoading';
}

class GetListCategorySuccess extends StudyGroupState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends StudyGroupState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class StudyGroupSuccess extends StudyGroupState {
  @override
  String toString() {
    return 'StudyGroupSuccess';
  }
}

class ChooseCommunitySuccess extends StudyGroupState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

class StudyGroupFailure extends StudyGroupState {
  final String error;

  StudyGroupFailure(this.error);

  @override
  String toString() => 'BodyParameterFailure { error: $error }';
}

class SubmitStudyGroupSuccess extends StudyGroupState {
  @override
  String toString() {
    return 'SubmitStudyGroupSuccess{}';
  }
}

class GetLevelSuccess extends StudyGroupState {
  @override
  String toString() {
    return 'GetLevelSuccess{}';
  }
}
