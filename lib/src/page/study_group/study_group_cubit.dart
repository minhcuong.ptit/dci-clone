import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/level_data.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';

import '../../data/network/response/list_dci_community_response.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/logger.dart';
import 'study_group.dart';

class StudyGroupCubit extends Cubit<StudyGroupState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityDataV2> listCommunity = [];
  AppPreferences appPreferences = AppPreferences();
  CommunityStatus? communityStatus;
  String? nextToken;
  bool hasNext = false;
  bool _isLoading = false;

  bool get canRequestLoadMore => !_isLoading && hasNext;
  int limit = Const.NETWORK_DEFAULT_LIMIT;
  String? _searchKey;
  bool _isValid = true;
  Map<LevelData, bool> isChooseStudy = {};
  List<LevelData> levels = [];
  List<bool> isChooseDay =
      List.generate(DayOfWeek.values.length, (index) => false);
  DateTime? startTime;
  DateTime? endTime;

  StudyGroupCubit({required this.repository, required this.graphqlRepository})
      : super(InitialStudyGroupState());

  void search(searchKey) {
    _searchKey = searchKey;
    getListContent(isRefresh: true);
  }

  void getListContent({
    bool isRefresh = false,
    bool loadMore = false,
    List<int>? levelIds,
    List<String>? dayOfWeek,
    String? keyWord,
  }) async {
    emit(isRefresh ? StudyGroupLoading() : InitialStudyGroupState());
    if (isRefresh == true) {
      nextToken = null;
      listCommunity.clear();
    }
    levelIds ??= [];
    if (levelIds.isEmpty)
      isChooseStudy.forEach((key, value) {
        if (value) levelIds!.add(key.id ?? 0);
      });
    var _isGroupSuggestion = communityStatus == null;
    ApiResult<ListDCICommunityResponse> contentTask =
        await graphqlRepository.getDCICommunities(CommunityType.STUDY_GROUP,
            status: communityStatus,
            levelIds: levelIds,
            limit: Const.NETWORK_DEFAULT_LIMIT,
            nextToken: nextToken,
            searchKey: keyWord,
            startTime: startTime?.millisecondsSinceEpoch,
            endTime: endTime?.millisecondsSinceEpoch,
            dayOfWeek: dayOfWeek,
            isValid: _isGroupSuggestion ? _isValid : null);
    contentTask.when(success: (ListDCICommunityResponse data) async {
      listCommunity.addAll(data.dciCommunities?.data ?? []);
      nextToken = data.dciCommunities?.nextToken;
      emit(GetListCommunitySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(StudyGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  int mySortComparison(CategoryData a, CategoryData b) {
    return (a.isSelected == b.isSelected) ? -1 : 1;
  }

  void selectCommunity(CommunityDataV2 selectedCommunity) {
    emit(StudyGroupLoading());
    emit(StudyGroupSuccess());
  }

  void getLevels(List<int?> levelId) async {
    emit(StudyGroupLoading());
    ApiResult<List<LevelData>> apiResult = await repository.getLevels();
    apiResult.when(success: (List<LevelData> data) async {
      levels = data;
      // data.forEach((element) {
      //   isChooseStudy[element] = levelId.contains(element.id);
      // });
      emit(GetLevelSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(StudyGroupFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectStudy(bool isSelected, int index) {
    emit(StudyGroupLoading());
    this.isChooseStudy[levels[index]] = isSelected;
    emit(InitialStudyGroupState());
  }

  void selectStartTime(DateTime time) {
    emit(StudyGroupLoading());
    this.startTime = time;
    emit(InitialStudyGroupState());
  }

  void selectEndTime(DateTime time) {
    emit(StudyGroupLoading());
    this.endTime = time;
    emit(InitialStudyGroupState());
  }

  void selectDay(bool isSelected, int index) {
    emit(StudyGroupLoading());
    this.isChooseDay[index] = isSelected;
    emit(InitialStudyGroupState());
  }
}
