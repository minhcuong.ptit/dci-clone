import 'package:equatable/equatable.dart';

abstract class ListBlockUserState extends Equatable {
  @override
  List<Object> get props => [];
}

class ListBlockUserInitial extends ListBlockUserState {}

class ListBlockUserLoading extends ListBlockUserState {
  @override
  String toString() {
    return 'ListBlockUserLoading{}';
  }
}

class ListBlockUserFailure extends ListBlockUserState {
  final String error;

  ListBlockUserFailure(this.error);

  @override
  String toString() {
    return 'ListBlockUserFailure{error: $error}';
  }
}

class ListBlockUserSuccess extends ListBlockUserState {
  @override
  String toString() {
    return 'ListBlockUserSuccess{}';
  }
}

class UnBlockUserSuccess extends ListBlockUserState {
  @override
  String toString() {
    return 'UnBlockUserSuccess{}';
  }
}
