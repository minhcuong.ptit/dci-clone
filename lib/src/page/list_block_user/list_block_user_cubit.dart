import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/list_block_user.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/list_block_user/list_block_user.dart';
import 'package:imi/src/page/list_block_user/list_block_user_state.dart';

class ListBlockUserCubit extends Cubit<ListBlockUserState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<ListBlockData> listBlockUser = [];
  FetchBlockedUsers? fetchBlockedUsers;
  String? _nextToken;

  ListBlockUserCubit(this.repository, this.graphqlRepository)
      : super(ListBlockUserInitial());

  void getListBlockUser({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? ListBlockUserLoading() : ListBlockUserInitial());
    if (isRefresh) {
      _nextToken = null;
      listBlockUser.clear();
    }
    final result = await graphqlRepository.getListBlock(nextToken: _nextToken);
    result.when(success: (dynamic data) {
      if (data is FetchBlockedUsers) listBlockUser = data.data ?? [];
      fetchBlockedUsers = data;
      emit(ListBlockUserSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ListBlockUserFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void unBlockUser({String? userUuid, String? action}) async {
    emit(ListBlockUserLoading());
    ApiResult<dynamic> blockUer =
        await repository.blockUser(userUuid: userUuid, action: action);
    blockUer.when(success: (dynamic data) {
      emit(UnBlockUserSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ListBlockUserFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshListBlock() {
    emit(ListBlockUserLoading());
    getListBlockUser();
    emit(ListBlockUserInitial());
  }
}
