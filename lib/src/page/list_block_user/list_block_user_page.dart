import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/list_block_user/list_block_user.dart';
import 'package:imi/src/page/profile/view_profile/view_profile.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListBlockUserPage extends StatefulWidget {
  const ListBlockUserPage({Key? key}) : super(key: key);

  @override
  State<ListBlockUserPage> createState() => _ListBlockUserPageState();
}

class _ListBlockUserPageState extends State<ListBlockUserPage> {
  late ListBlockUserCubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    // TODO: implement initState
    super.initState();
    _cubit = ListBlockUserCubit(repository, appGraphqlRepository);
    _cubit.getListBlockUser();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<ListBlockUserCubit, ListBlockUserState>(
        listener: (BuildContext context, state) {
          if (state is! ListBlockUserLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is UnBlockUserSuccess) {
            _cubit.refreshListBlock();
          }
          if (state is UnBlockUserSuccess) {
            Utils.showToast(context, R.string.unblock_success.tr());
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            backgroundColor: R.color.grey100,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: R.color.white,
              automaticallyImplyLeading: false,
              title: Visibility(
                visible: state is ListBlockUserSuccess,
                child: _cubit.fetchBlockedUsers?.total == 0
                    ? Text(
                        R.string.list_of_blocked_users.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 13.sp, color: R.color.black),
                      )
                    : Text(
                        "${_cubit.fetchBlockedUsers?.total}" +
                            R.string.blocked_user.tr(args: [" "]),
                        style: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 13.sp, color: R.color.black),
                      ),
              ),
              leading: GestureDetector(
                onTap: () {
                  NavigationUtils.pop(context);
                },
                child: Icon(
                  Icons.arrow_back,
                  color: R.color.black,
                ),
              ),
            ),
            body: StackLoadingView(
              visibleLoading: state is ListBlockUserLoading,
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () => _cubit.getListBlockUser(isRefresh: true),
                onLoading: () => _cubit.getListBlockUser(isLoadMore: true),
                child: _cubit.fetchBlockedUsers?.total == 0
                    ? Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height * 0.2),
                        child: Text(
                          R.string.no_users_are_blocked.tr(),
                          style: Theme.of(context).textTheme.regular400,
                          textAlign: TextAlign.center,
                        ),
                      )
                    : ListView.separated(
                        padding: EdgeInsets.all(16.h),
                        itemBuilder: (context, int index) {
                          return Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  NavigationUtils.rootNavigatePage(
                                      context,
                                      ViewProfilePage(
                                          communityId: _cubit
                                              .listBlockUser[index]
                                              .profile
                                              ?.communityV2
                                              ?.id));
                                },
                                child: AvatarWidget(
                                  avatar: _cubit.listBlockUser[index].profile
                                          ?.avatarUrl ??
                                      "",
                                  size: 50.h,
                                ),
                              ),
                              SizedBox(width: 10.h),
                              Expanded(
                                child: Text(
                                  _cubit.listBlockUser[index].profile
                                          ?.fullName ??
                                      "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(fontSize: 11.sp),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      builder: (context) {
                                        return SingleChildScrollView(
                                          padding: EdgeInsets.only(
                                              top: 40.h,
                                              right: 24.h,
                                              left: 24.h,
                                              bottom: 24.h),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                R.string.unblock_users.tr(),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bold700
                                                    .copyWith(fontSize: 16.sp),
                                              ),
                                              SizedBox(height: 8.h),
                                              RichText(
                                                text: TextSpan(
                                                  text: R.string
                                                      .confirm_unblock_users
                                                      .tr(args: [" "]),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .regular400
                                                      .copyWith(
                                                          fontSize: 11.sp),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: _cubit
                                                            .listBlockUser[
                                                                index]
                                                            .profile
                                                            ?.fullName,
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bold700
                                                            .copyWith(
                                                                fontSize:
                                                                    11.sp)),
                                                    TextSpan(text: " ?")
                                                  ],
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 24.h),
                                                color: R.color.grey300,
                                                height: 1,
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: ButtonWidget(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                vertical: 6.h),
                                                        textSize: 12.sp,
                                                        backgroundColor:
                                                            R.color.white,
                                                        borderColor: R
                                                            .color.primaryColor,
                                                        uppercaseTitle: false,
                                                        textColor: R
                                                            .color.primaryColor,
                                                        title: R.string.cancel
                                                            .tr(),
                                                        onPressed: () {
                                                          NavigationUtils.pop(
                                                              context);
                                                        }),
                                                  ),
                                                  SizedBox(width: 30.h),
                                                  Expanded(
                                                      child: ButtonWidget(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 6.h),
                                                    textSize: 12.sp,
                                                    backgroundColor:
                                                        R.color.primaryColor,
                                                    textColor: R.color.white,
                                                    uppercaseTitle: false,
                                                    title:
                                                        R.string.unblock.tr(),
                                                    onPressed: () {
                                                      NavigationUtils.pop(
                                                          context);
                                                      _cubit.unBlockUser(
                                                          userUuid: _cubit
                                                              .listBlockUser[
                                                                  index]
                                                              .profile
                                                              ?.userUuid,
                                                          action:
                                                              Const.UNBLOCK);
                                                    },
                                                  ))
                                                ],
                                              )
                                            ],
                                          ),
                                        );
                                      });
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 9.h, horizontal: 24.h),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5.h),
                                      border: Border.all(color: R.color.red)),
                                  child: Text(
                                    R.string.unblock.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(
                                            fontSize: 12.sp,
                                            height: 16.h / 12.sp,
                                            color: R.color.red),
                                  ),
                                ),
                              )
                            ],
                          );
                        },
                        separatorBuilder: (context, int index) => Divider(),
                        itemCount: _cubit.listBlockUser.length,
                      ),
              ),
            ),
          );
        },
      ),
    );
  }
}
