import 'package:equatable/equatable.dart';

abstract class SettingSoundMeditationState extends Equatable {
  @override
  List<Object> get props => [];
}

class SettingSoundMeditationInitial extends SettingSoundMeditationState {}

class SettingSoundMeditationLoading extends SettingSoundMeditationState {
  @override
  String toString() => 'SettingSoundMeditationLoading';
}

class SettingSoundMeditationSuccess extends SettingSoundMeditationState {
  @override
  String toString() {
    return 'SettingSoundMeditationSuccess';
  }
}

class SettingSoundMeditationFailure extends SettingSoundMeditationState {
  final String error;

  SettingSoundMeditationFailure(this.error);

  @override
  String toString() => 'SettingSoundMeditationFailure { error: $error }';
}

class chooseSoundFromMobileLoading extends SettingSoundMeditationState {
  @override
  String toString() => 'chooseSoundFromMobileLoading';
}

class chooseSoundFromMobileSuccess extends SettingSoundMeditationState {
  @override
  String toString() => 'chooseSoundFromMobileSuccess';
}
