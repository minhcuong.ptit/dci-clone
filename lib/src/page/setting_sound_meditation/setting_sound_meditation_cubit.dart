import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/audio_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/setting_sound_meditation/setting_sound_meditation.dart';
import 'package:imi/src/page/setting_sound_meditation/setting_sound_meditation_state.dart';
import 'package:imi/src/utils/enum.dart';

class SettingSoundMeditationCubit extends Cubit<SettingSoundMeditationState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<String> listStartSound = ["B.1", "B.2", "B.3", "B.4"];
  List<String> listFinishSound = ["K.1", "K.2", "K.3", "K.4"];
  List<String> listSound = ["C.1", "C.2", "C.3", "C.4"];
  List<String> listAmbientSound = ["DCI.1", "DCI.2", "DCI.3", "DCI.4"];
  List<int> listTurn = [1, 2, 3];
  List<int> listDelay = [5, 10, 15, 30, 60];
  List<String> listSoundMeditation = [
    "assets/2.Chime.wav",
    "assets/3.Meditation_bell_sound_effect.mp3",
    "assets/Bell_Chime_Sound.wav",
    "assets/Meditation_Bell_Sounds_Effect.mp3"
  ];
  bool muteBellsStart = false;
  bool muteBellsEnd = false;
  bool muteBellsDelay = false;
  bool muteAmbientSound = false;
  bool sourceFromMobie=false;
  int selectedBellsStartIndex = 0;
  int selectedBellsEndIndex = 0;
  int selectedBellsIndex = 0;
  int selectedAmbientSoundIndex = 0;

  List<AudioDataFetchAudio> listAudio = [];

  SettingSoundMeditationCubit(this.repository, this.graphqlRepository)
      : super(SettingSoundMeditationInitial());

  void selectedBellsStart(int index, String sound) {
    emit(SettingSoundMeditationLoading());
    this.selectedBellsStartIndex = index;
    this.listSoundMeditation[index] = sound;
    emit(SettingSoundMeditationInitial());
  }

  void selectedBellsEnd(int index, String sound) {
    emit(SettingSoundMeditationLoading());
    this.selectedBellsEndIndex = index;
    this.listSoundMeditation[index] = sound;
    emit(SettingSoundMeditationInitial());
  }

  void selectedSound(int index, String sound) {
    emit(SettingSoundMeditationLoading());
    this.selectedBellsIndex = index;
    this.listSoundMeditation[index] = sound;
    emit(SettingSoundMeditationInitial());
  }

  void selectedAmbientSound(int index, String sound) {
    emit(SettingSoundMeditationLoading());
    this.selectedAmbientSoundIndex = index;
    this.listAudio[index].fileUrl = sound;
    sourceFromMobie=false;
    emit(SettingSoundMeditationInitial());
  }

   chooseFileFromMobie() {
    emit(chooseSoundFromMobileLoading());
    sourceFromMobie=true;
    emit(chooseSoundFromMobileSuccess());
  }

  void getAudio() async {
    emit(SettingSoundMeditationLoading());
    ApiResult<AudioData> apiResult =
        await graphqlRepository.getAudio(audioType: AudioType.ENVIRONMENT);
    apiResult.when(success: (AudioData data) {
      listAudio = data.fetchAudio ?? [];
      emit(SettingSoundMeditationSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(SettingSoundMeditationFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }
}
