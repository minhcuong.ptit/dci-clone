import 'dart:developer';

import 'package:audioplayers/audioplayers.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/res/generated/colors.g.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/setting_sound_meditation/setting_sound_meditation.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'dart:math' as math;

import 'package:imi/src/widgets/stack_loading_view.dart';

class SettingSoundMeditationPage extends StatefulWidget {
  const SettingSoundMeditationPage({Key? key}) : super(key: key);

  @override
  State<SettingSoundMeditationPage> createState() =>
      _SettingSoundMeditationPageState();
}

class _SettingSoundMeditationPageState extends State<SettingSoundMeditationPage>
    with WidgetsBindingObserver {
  late SettingSoundMeditationCubit _cubit;
  PlatformFile? file;
  FilePickerResult? result;
  int? _chosenTurnBellsStart;
  int? _chosenTurnBellsEnd;
  int? _chosenTurnBells;

  bool? get muteBellsStart => appPreferences.getBool(Const.BOOL_BELL_START);

  bool? get muteBellsEnd => appPreferences.getBool(Const.BOOL_BELL_END);

  bool? get muteBellsDelay => appPreferences.getBool(Const.BOOL_BELL_DELAY);

  bool? get muteAmbientSound =>
      appPreferences.getBool(Const.BOOL_AMBIENT_SOUND);

  int? get selectedBellsStart => appPreferences.getInt(Const.INDEX_BELL_START);

  int? get selectedBellsEnd => appPreferences.getInt(Const.INDEX_BELL_END);

  int? get selectedBellsDelay => appPreferences.getInt(Const.INDEX_BELL_DELAY);

  int? get selectedBellsAmbientSound =>
      appPreferences.getInt(Const.INDEX_BELL_AMBIENT_SOUND);

  int? get repetitionBellStart =>
      appPreferences.getInt(Const.REPETITION_BELL_START);

  int? get repetitionBellEnd =>
      appPreferences.getInt(Const.REPETITION_BELL_END);

  int? get repetitionDelay => appPreferences.getInt(Const.REPETITION_DELAY);

  String get nameAudio => appPreferences.getString(Const.FILE_AUDIO) ?? "";
  String fileNameAudio = "";

  AudioCache player = AudioCache();
  AudioPlayer audioPlayer = AudioPlayer();
  bool isPlaying = false;

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = SettingSoundMeditationCubit(repository, graphqlRepository);
    _cubit.muteBellsStart = muteBellsStart ?? false;
    _cubit.muteBellsEnd = muteBellsEnd ?? false;
    _cubit.muteBellsDelay = muteBellsDelay ?? false;
    _cubit.muteAmbientSound = muteAmbientSound ?? false;
    _cubit.selectedBellsStartIndex = selectedBellsStart ?? 0;
    _cubit.selectedBellsEndIndex = selectedBellsEnd ?? 0;
    _cubit.selectedBellsIndex = selectedBellsDelay ?? 0;
    _cubit.selectedAmbientSoundIndex = selectedBellsAmbientSound ?? 0;
    _cubit.sourceFromMobie=nameAudio==""?false:true;
    log('test ${appPreferences.getString(Const.FILE_AUDIO)}');
    if(_cubit.sourceFromMobie){
      _cubit.muteAmbientSound = false;
    }
    _chosenTurnBellsStart = repetitionBellStart ?? 1;
    _chosenTurnBellsEnd = repetitionBellEnd ?? 1;
    _chosenTurnBells = repetitionDelay ?? 5;
    fileNameAudio = nameAudio;

    // if (nameAudio != "") {
    //   _cubit.muteAmbientSound = true;
    //   // appPreferences.setData(Const.BOOL_AMBIENT_SOUND, _cubit.muteAmbientSound);
    // }
    // if (selectedBellsAmbientSound == null) {
    //   // _cubit.muteAmbientSound = true;
    //   appPreferences.setData(Const.BOOL_AMBIENT_SOUND, _cubit.muteAmbientSound);
    // }
    if (selectedBellsDelay == null) {
      // _cubit.muteBellsDelay = true;
      appPreferences.setData(Const.BOOL_BELL_DELAY, _cubit.muteBellsDelay);
    }
    _cubit.getAudio();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    state = AppLifecycleState.paused;
    audioPlayer.pause();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        NavigationUtils.pop(context);
        // appPreferences.removeData(Const.INDEX_BELL_START);
        // appPreferences.removeData(Const.INDEX_BELL_END);
        // appPreferences.removeData(Const.INDEX_BELL_DELAY);
        // appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
        // appPreferences.removeData(Const.FILE_AUDIO);
        audioPlayer.pause();
        return true;
      },
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<SettingSoundMeditationCubit,
            SettingSoundMeditationState>(
          listener: (BuildContext context, state) {},
          builder: (BuildContext context, state) {
            return Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: R.color.primaryColor,
                centerTitle: true,
                title: Text(R.string.setting_sound.tr().toUpperCase()),
                leading: GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context);
                    // appPreferences.removeData(Const.INDEX_BELL_START);
                    // appPreferences.removeData(Const.INDEX_BELL_END);
                    // appPreferences.removeData(Const.INDEX_BELL_DELAY);
                    // appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
                    // appPreferences.removeData(Const.FILE_AUDIO);
                    audioPlayer.pause();
                  },
                  child: Icon(Icons.arrow_back),
                ),
              ),
              body: buildPage(context, state),
            );
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, SettingSoundMeditationState state) {
    return StackLoadingView(
      visibleLoading: state is SettingSoundMeditationLoading,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: 24.h, horizontal: 15.h),
        children: [
          buildBellsStart(context),
          SizedBox(height: 20.h),
          buildBellsEnd(context),
          SizedBox(height: 20.h),
          buildBellsDelay(context),
          SizedBox(height: 20.h),
          buildAmbientSound(context),
          SizedBox(height: 20.h),
          ButtonWidget(
            backgroundColor: R.color.primaryColor,
            textSize: 14.sp,
            padding: EdgeInsets.symmetric(vertical: 14.h),
            title: R.string.save.tr(),
            onPressed: () {
              NavigationUtils.pop(context);
              audioPlayer.pause();
              if (muteBellsStart == true) {
                appPreferences.removeData(Const.BOOL_BELL_START);
                appPreferences.removeData(Const.BELLS_START);
                appPreferences.setData(
                    Const.BOOL_BELL_START, _cubit.muteBellsStart);
              }
              if (muteBellsEnd == true) {
                appPreferences.removeData(Const.BOOL_BELL_END);
                appPreferences.removeData(Const.BELLS_END);
                appPreferences.setData(
                    Const.BOOL_BELL_END, _cubit.muteBellsEnd);
              }
              if (muteBellsDelay == true) {
                appPreferences.removeData(Const.BOOL_BELL_DELAY);
                appPreferences.removeData(Const.BELLS_DELAY);
              }
              if (muteAmbientSound == true) {
                // appPreferences.removeData(Const.BOOL_AMBIENT_SOUND);
                appPreferences.removeData(Const.BELLS_AMBIENT_SOUND_INDEX);
                appPreferences.setData(
                    Const.BELLS_AMBIENT_SOUND_INDEX, file?.path);

              }
              if(muteAmbientSound==false){
                if(!_cubit.sourceFromMobie){
                  appPreferences.setData(
                      Const.INDEX_BELL_AMBIENT_SOUND,
                      _cubit.selectedAmbientSoundIndex);
                  appPreferences.removeData(Const.FILE_AUDIO);
                }
              }
              // else {
                appPreferences.setData(
                    Const.BOOL_BELL_START, _cubit.muteBellsStart);
                appPreferences.setData(
                    Const.BOOL_BELL_END, _cubit.muteBellsEnd);
                appPreferences.setData(
                    Const.BOOL_BELL_DELAY, _cubit.muteBellsDelay);
                appPreferences.setData(
                    Const.BOOL_AMBIENT_SOUND, _cubit.muteAmbientSound);

                appPreferences.setData(
                    Const.INDEX_BELL_DELAY, _cubit.selectedBellsIndex);
                appPreferences.setData(
                    Const.INDEX_BELL_END, _cubit.selectedBellsEndIndex);
                appPreferences.setData(
                    Const.INDEX_BELL_START, _cubit.selectedBellsStartIndex);
              // }
            },
          ),
          SizedBox(height: 50.h)
        ],
      ),
    );
  }

  Widget buildAmbientSound(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 10.h, vertical: 15.h),
      decoration: BoxDecoration(
          color: R.color.grey100, borderRadius: BorderRadius.circular(10.h)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            R.string.ambient_sound.tr(),
            style: Theme.of(context)
                .textTheme
                .medium500
                .copyWith(fontSize: 14.sp, height: 24.h / 14.sp),
          ),
          SizedBox(height: 18.h),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    _cubit.muteAmbientSound = !_cubit.muteAmbientSound;
                    // appPreferences.setData(
                    //     Const.BOOL_AMBIENT_SOUND, _cubit.muteAmbientSound);
                    appPreferences.removeData(Const.FILE_AUDIO);
                    appPreferences.removeData(Const.AUDIO);
                    //appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
                    if(_cubit.muteAmbientSound){
                      audioPlayer.pause();
                      return;
                    }
                    if(!_cubit.sourceFromMobie){
                      audioPlayer.play(
                          _cubit.listAudio[_cubit.selectedAmbientSoundIndex].fileUrl ??
                              "");
                    }
                    if(_cubit.sourceFromMobie){
                      audioPlayer.play(file?.path??'');
                    }
                  });
                },
                child: Icon(
                  _cubit.muteAmbientSound == true
                      ? CupertinoIcons.speaker_slash
                      : CupertinoIcons.speaker_3,
                  size: 35.h,
                ),
              ),
              SizedBox(width: 6.w),
              Expanded(
                child: SizedBox(
                  width: context.width - 75.w,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: List.generate(
                          _cubit.listAudio.length,
                          (index) => GestureDetector(
                                onTap: _cubit.muteAmbientSound == true
                                    ? null
                                    : () async {
                                        _cubit.selectedAmbientSound(
                                            index,
                                            _cubit.listAudio[index].fileUrl ??
                                                "");
                                        // appPreferences.setData(
                                        //     Const.BELLS_AMBIENT_SOUND_INDEX,
                                        //     _cubit.listAudio[index].fileUrl ??
                                        //         "");
                                        appPreferences.setData(
                                            Const.INDEX_BELL_AMBIENT_SOUND,
                                            index);
                                        _cubit.selectedAmbientSoundIndex=index;
                                        audioPlayer.pause();
                                        audioPlayer.play(
                                            _cubit.listAudio[index].fileUrl ??
                                                "");
                                        // setState(() {
                                        //   // isPlaying = !isPlaying;
                                        //   if (isPlaying) {
                                        //     audioPlayer.pause();
                                        //     audioPlayer.play(
                                        //         _cubit.listAudio[index].fileUrl ??
                                        //             "");
                                        //   }
                                        // });

                                        // else {
                                        //   audioPlayer.pause();
                                        //   audioPlayer.play(
                                        //       _cubit.listAudio[index].fileUrl ??
                                        //           "");
                                        // }
                                      },
                                child: Container(
                                  height: 55.h,
                                  width: 55.h,
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 10.h),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(70.h),
                                    // shape: BoxShape.circle,
                                    color: _cubit.muteAmbientSound == true
                                        ? R.color.primaryColor.withOpacity(0.4)
                                        : _cubit.selectedAmbientSoundIndex ==
                                                index && !_cubit.sourceFromMobie
                                            ? R.color.primaryColor
                                            : R.color.primaryColor
                                                .withOpacity(0.4),
                                  ),
                                  //padding: EdgeInsets.all(15.h),
                                  child: Text(
                                    "DCI." + "${index + 1}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(
                                            fontSize: 14.sp,
                                            color: R.color.white,
                                            height: 22.h / 16.sp),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 15.h),
          fileNameAudio == "" ? SizedBox() : fileAudio(fileName: fileNameAudio),
          SizedBox(height: 15.h),
          GestureDetector(
            onTap: () async {
              var result =
                  await FilePicker.platform.pickFiles(type: FileType.audio);
              if (result == null) return;
              file = result.files.first;
              setState(() {
                _cubit.muteAmbientSound = false;
                _cubit.chooseFileFromMobie();
                appPreferences.setData(
                    Const.BOOL_AMBIENT_SOUND, _cubit.muteAmbientSound);
                fileNameAudio = file?.name ?? "";
                appPreferences.setData(Const.FILE_AUDIO, file?.name);
              });
              appPreferences.setData(Const.AUDIO, file?.path);
              appPreferences.removeData(Const.INDEX_BELL_AMBIENT_SOUND);
              audioPlayer.play(file?.path??'');
            },
            child: Row(
              children: [
                Icon(
                  Icons.add,
                  size: 32.h,
                  color: R.color.primaryColor,
                ),
                Expanded(
                  child: Text(
                    R.string.select_sound_from_mobile.tr(),
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 16.sp,
                        height: 24.h / 16.sp,
                        color: R.color.primaryColor),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget fileAudio({String? fileName}) {
    return GestureDetector(
      onTap: (){
        _cubit.chooseFileFromMobie();
        audioPlayer.pause();
        audioPlayer.play(file?.path??'');
      },
      child: Container(
        width: double.infinity,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color:_cubit.sourceFromMobie&!_cubit.muteAmbientSound? R.color.primaryColor:R.color.primaryColor.withOpacity(0.4),
          borderRadius: BorderRadius.circular(10.h),
        ),
        padding: EdgeInsets.all(10.h),
        child: Row(
          children: [
            Icon(
              CupertinoIcons.music_note,
              size: 17.h,
              color: R.color.white,
            ),
            SizedBox(width: 5.h),
            Flexible(
              child: Text(
                fileName ?? "",
                style: Theme.of(context).textTheme.medium500.copyWith(
                    color: R.color.white, fontSize: 14.sp, height: 16.h / 14.sp),
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBellsDelay(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.h, vertical: 15.h),
      decoration: BoxDecoration(
          color: R.color.grey100, borderRadius: BorderRadius.circular(10.h)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  R.string.alarms_every_interval.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 14.sp, height: 24.h / 14.sp),
                ),
              ),
              DropdownButtonHideUnderline(
                child: Container(
                  height: 25.h,
                  width: 90.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.h),
                      border: Border.all(color: R.color.black)),
                  child: DropdownButton<int>(
                    icon: Transform.rotate(
                      angle: 90 * math.pi / 180,
                      child: Icon(
                        Icons.code,
                        size: 16.h,
                      ),
                    ),
                    value: _chosenTurnBells,
                    //elevation: 5,
                    style: TextStyle(color: R.color.black),

                    items: _cubit.listDelay
                        .map<DropdownMenuItem<int>>((int? value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.h),
                          child: Text(
                            "${value}" + " " + R.string.minutes.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 12.sp, height: 16.h / 12.sp),
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (int? value) {
                      setState(() {
                        _chosenTurnBells = value!;
                        appPreferences.setData(Const.REPETITION_DELAY, value);
                      });
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 18.h),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    _cubit.muteBellsDelay = !_cubit.muteBellsDelay;
                    // appPreferences.setData(
                    //     Const.BOOL_BELL_DELAY, _cubit.muteBellsDelay);
                    if (_cubit.muteBellsDelay == true) {
                      appPreferences.removeData(Const.BELLS_DELAY);
                    }
                    audioPlayer.pause();
                  });
                },
                child: Icon(
                  _cubit.muteBellsDelay == true
                      ? CupertinoIcons.speaker_slash
                      : CupertinoIcons.speaker_3,
                  size: 35.h,
                ),
              ),
              SizedBox(width: 5.w),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(
                      _cubit.listSound.length,
                      (index) => GestureDetector(
                            onTap: _cubit.muteBellsDelay == true
                                ? null
                                : () async {
                                    _cubit.selectedSound(index,
                                        _cubit.listSoundMeditation[index]);
                                    appPreferences.setData(Const.BELLS_DELAY,
                                        _cubit.listSoundMeditation[index]);
                                    // appPreferences.setData(
                                    //     Const.INDEX_BELL_DELAY, index);
                                    _cubit.selectedBellsIndex = index;
                                    setState(() {
                                      isPlaying = !isPlaying;
                                    });
                                    if (isPlaying == false) {
                                      audioPlayer.pause();
                                      audioPlayer = await player.play(_cubit
                                          .listSoundMeditation[index]
                                          .replaceAll("assets/", ""));
                                    } else {
                                      audioPlayer.pause();
                                      audioPlayer = await player.play(_cubit
                                          .listSoundMeditation[index]
                                          .replaceAll("assets/", ""));
                                    }
                                  },
                            child: Container(
                              height: 55.h,
                              width: 55.h,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(70.h),
                                color: _cubit.muteBellsDelay == true
                                    ? R.color.primaryColor.withOpacity(0.4)
                                    : _cubit.selectedBellsIndex == index
                                        ? R.color.primaryColor
                                        : R.color.primaryColor.withOpacity(0.4),
                              ),
                              child: Text(
                                _cubit.listSound[index],
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        fontSize: 14.sp,
                                        color: R.color.white,
                                        height: 22.h / 16.sp),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildBellsEnd(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.h, vertical: 15.h),
      decoration: BoxDecoration(
          color: R.color.grey100, borderRadius: BorderRadius.circular(10.h)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  R.string.end_bell.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 14.sp, height: 24.h / 14.sp),
                ),
              ),
              DropdownButtonHideUnderline(
                child: Container(
                  height: 25.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.h),
                      border: Border.all(color: R.color.black)),
                  child: DropdownButton<int>(
                    icon: Transform.rotate(
                      angle: 90 * math.pi / 180,
                      child: Icon(
                        Icons.code,
                        size: 18.h,
                      ),
                    ),
                    value: _chosenTurnBellsEnd,
                    //elevation: 5,
                    style: TextStyle(color: R.color.black),

                    items: _cubit.listTurn
                        .map<DropdownMenuItem<int>>((int? value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.h),
                          child: Text(
                            "${value}" + " " + R.string.turn.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 12.sp, height: 16.h / 12.sp),
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (int? value) {
                      setState(() {
                        _chosenTurnBellsEnd = value!;
                        appPreferences.setData(
                            Const.REPETITION_BELL_END, value);
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 18.h),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    _cubit.muteBellsEnd = !_cubit.muteBellsEnd;
                    // appPreferences.setData(
                    //     Const.BOOL_BELL_END, _cubit.muteBellsEnd);
                    if (_cubit.muteBellsEnd == true) {
                      appPreferences.removeData(Const.BELLS_END);
                    }
                    audioPlayer.pause();
                  });
                },
                child: Icon(
                  _cubit.muteBellsEnd == true
                      ? CupertinoIcons.speaker_slash
                      : CupertinoIcons.speaker_3,
                  size: 35.h,
                ),
              ),
              SizedBox(width: 5.w),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(
                      _cubit.listFinishSound.length,
                      (index) => GestureDetector(
                            onTap: _cubit.muteBellsEnd == true
                                ? null
                                : () async {
                                    _cubit.selectedBellsEnd(index,
                                        _cubit.listSoundMeditation[index]);
                                    appPreferences.setData(Const.BELLS_END,
                                        _cubit.listSoundMeditation[index]);
                                    // appPreferences.setData(
                                    //     Const.INDEX_BELL_END, index);
                                    _cubit.selectedBellsEndIndex=index;
                                    setState(() {
                                      isPlaying = !isPlaying;
                                    });
                                    if (isPlaying == false) {
                                      audioPlayer.pause();
                                      audioPlayer = await player.play(_cubit
                                          .listSoundMeditation[index]
                                          .replaceAll("assets/", ""));
                                    } else {
                                      audioPlayer.pause();
                                      audioPlayer = await player.play(_cubit
                                          .listSoundMeditation[index]
                                          .replaceAll("assets/", ""));
                                    }
                                  },
                            child: Container(
                              height: 55.h,
                              width: 55.h,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(70.h),
                                color: _cubit.muteBellsEnd == true
                                    ? R.color.primaryColor.withOpacity(0.4)
                                    : _cubit.selectedBellsEndIndex == index
                                        ? R.color.primaryColor
                                        : R.color.primaryColor.withOpacity(0.4),
                              ),
                              child: Text(
                                _cubit.listFinishSound[index],
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        fontSize: 14.sp,
                                        color: R.color.white,
                                        height: 22.h / 16.sp),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget buildBellsStart(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.h, vertical: 15.h),
      decoration: BoxDecoration(
          color: R.color.grey100, borderRadius: BorderRadius.circular(10.h)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  R.string.start_bell.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 14.sp, height: 24.h / 14.sp),
                ),
              ),
              DropdownButtonHideUnderline(
                child: Container(
                  height: 25.h,
                  width: 80.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.h),
                      border: Border.all(color: R.color.black)),
                  child: DropdownButton<int>(
                    icon: Transform.rotate(
                      angle: 90 * math.pi / 180,
                      child: Icon(
                        Icons.code,
                        size: 18.h,
                      ),
                    ),
                    value: _chosenTurnBellsStart,
                    //elevation: 5,
                    style: TextStyle(color: R.color.black),

                    items: _cubit.listTurn
                        .map<DropdownMenuItem<int>>((int? value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.h),
                          child: Text(
                            "${value}" + " " + R.string.turn.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 12.sp, height: 16.h / 12.sp),
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (int? value) {
                      setState(() {
                        _chosenTurnBellsStart = value!;
                        appPreferences.setData(
                            Const.REPETITION_BELL_START, value);
                      });
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 18.h),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    _cubit.muteBellsStart = !_cubit.muteBellsStart;
                    // appPreferences.setData(
                    //     Const.BOOL_BELL_START, _cubit.muteBellsStart);
                    if (_cubit.muteBellsStart == true) {
                      appPreferences.removeData(Const.BELLS_START);
                    }
                    audioPlayer.pause();
                  });
                },
                child: Icon(
                  _cubit.muteBellsStart == true
                      ? CupertinoIcons.speaker_slash
                      : CupertinoIcons.speaker_3,
                  size: 35.h,
                ),
              ),
              SizedBox(width: 5.w),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(
                      _cubit.listStartSound.length,
                      (index) => GestureDetector(
                            onTap: _cubit.muteBellsStart == true
                                ? null
                                : () async {
                                    _cubit.selectedBellsStart(index,
                                        _cubit.listSoundMeditation[index]);
                                    appPreferences.setData(Const.BELLS_START,
                                        _cubit.listSoundMeditation[index]);
                                    _cubit.selectedBellsStartIndex=index;
                                    // appPreferences.setData(
                                    //     Const.INDEX_BELL_START, index);
                                    setState(() {
                                      isPlaying = !isPlaying;
                                    });
                                    if (isPlaying == false) {
                                      audioPlayer.pause();
                                      audioPlayer = await player.play(_cubit
                                          .listSoundMeditation[index]
                                          .replaceAll("assets/", ""));
                                    } else {
                                      audioPlayer.pause();
                                      audioPlayer = await player.play(_cubit
                                          .listSoundMeditation[index]
                                          .replaceAll("assets/", ""));
                                    }
                                  },
                            child: Container(
                              height: 55.h,
                              width: 55.h,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(70.h),
                                color: _cubit.muteBellsStart == true
                                    ? R.color.primaryColor.withOpacity(0.4)
                                    : _cubit.selectedBellsStartIndex == index
                                        ? R.color.primaryColor
                                        : R.color.primaryColor.withOpacity(0.4),
                              ),
                              child: Text(
                                _cubit.listStartSound[index],
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        fontSize: 14.sp,
                                        color: R.color.white,
                                        height: 22.h / 16.sp),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
