import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../res/colors.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/custom_tabbar_controller.dart';
import 'meditation_detail_report.dart';

class MeditationDetailReportPage extends StatefulWidget {
  const MeditationDetailReportPage({Key? key}) : super(key: key);

  @override
  _MeditationDetailReportPageState createState() =>
      _MeditationDetailReportPageState();
}

class _MeditationDetailReportPageState extends State<MeditationDetailReportPage>
    with TickerProviderStateMixin {
  late MeditationDetailReportCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  late TabController _tabController;
  List<int> id = [];
  late final ScrollController _scrollController;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 3);
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = MeditationDetailReportCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    super.initState();
    _scrollController = ScrollController(initialScrollOffset: 1000);
    _cubit.getListMeditation();
    _cubit.getReportMeditation(meditationIds: id);
    _cubit.getDetailReportMeditation(reportType: ReportType.DAY);
  }

  BarChartGroupData generateBarGroup(
    int x,
    double value,
    Color color,
    BorderRadius borderRadius,
  ) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          toY: value,
          color: color,
          width: 7.w,
          borderRadius: borderRadius,
        ),
      ],
      showingTooltipIndicators: [x != 0 ? 0 : 1],
    );
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<MeditationDetailReportCubit,
            MeditationDetailReportState>(
          listener: (context, state) {
            if (state is MeditationDetailReportFailure)
              Utils.showErrorSnackBar(context, state.error);
            if (state is! MeditationDetailReportLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return _buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildTabBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
      child: CustomTabBarController(
        onTabChange: (index) {
          if (index == 0) {
            _cubit.getDetailReportMeditation(
                reportType: ReportType.DAY, meditationIds: id);
          } else if (index == 1) {
            _cubit.getDetailReportMeditation(
                reportType: ReportType.WEEK, meditationIds: id);
          } else if (index == 2) {
            _cubit.getDetailReportMeditation(
                reportType: ReportType.MONTH, meditationIds: id);
          }
        },
        tabController: _tabController,
        tabBarItems: [
          TabBarItem(title: R.string.day.tr()),
          TabBarItem(title: R.string.week.tr()),
          TabBarItem(title: R.string.month.tr()),
        ],
        physics: NeverScrollableScrollPhysics(),
      ),
    );
  }

  Widget buildTabBarView() {
    return TabBarView(
      controller: _tabController,
      physics: NeverScrollableScrollPhysics(),
      children: [widgetDays(0), widgetDays(1), widgetDays(2)],
    );
  }

  Widget widgetDays(int index) {
    return ListView(
      children: [
        buildChartDuration(
            index, index == 2 ? getTitleDurationMonth : getTitleDuration),
        SizedBox(height: 20.h),
        Container(height: 1.h, color: R.color.grey),
        SizedBox(height: 20.h),
        buildChartCount(index, index == 2 ? getCountMonth : getTitlesCount),
      ],
    );
  }

  Widget _buildPage(BuildContext context, MeditationDetailReportState state) {
    return Scaffold(
      backgroundColor: R.color.white,
      appBar: appBar(context, R.string.report_progress.tr(),
          backgroundColor: AppColors.primaryColor,
          iconColor: R.color.white,
          elevation: 0,
          centerTitle: true,
          textStyle: Theme.of(context)
              .textTheme
              .bodyBold
              .apply(color: R.color.white)
              .copyWith(fontSize: 16.sp, fontWeight: FontWeight.w700)),
      body: StackLoadingView(
        visibleLoading: state is MeditationDetailReportLoading,
        child: Column(
          children: [
            Expanded(
              child: SmartRefresher(
                  controller: _refreshController,
                  enablePullUp: _cubit.nextToken != null,
                  onRefresh: () {
                    _cubit.getListMeditation(isRefresh: true);
                  },
                  onLoading: () {
                    _cubit.getListMeditation(isLoadMore: true);
                  },
                  child: _buildContent()),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(height: 8.h),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(child: SizedBox.shrink()),
              Text(
                R.string.filter_by_type_of_meditation.tr(),
                style: Theme.of(context).textTheme.labelSmallText.copyWith(
                    color: R.color.darkGray,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w400,
                    height: 1),
              ),
              SizedBox(width: 16.w),
              dropdownFilter()
            ],
          ),
          SizedBox(height: 20.h),
          Row(
            children: [
              Expanded(
                  child: _widgetMeditation(
                      R.string.total_number_of_days_meditated.tr(),
                      _cubit.fetchGeneralMeditationReport?.totalDays ?? 0)),
              SizedBox(width: 7.w),
              Expanded(
                  child: _widgetMeditation(
                      R.string.number_of_consecutive_days_of_meditation.tr(),
                      _cubit.fetchGeneralMeditationReport?.dayStreak ?? 0)),
              SizedBox(width: 7.w),
              Expanded(
                  child: _widgetMeditation(R.string.total_hours_meditated.tr(),
                      _cubit.fetchGeneralMeditationReport?.totalHours ?? 0)),
            ],
          ),
          SizedBox(height: 20.h),
          buildTabBar(),
          Expanded(child: buildTabBarView()),
        ],
      ),
    );
  }

  Widget dropdownFilter() {
    return DropdownButtonHideUnderline(
      child: _cubit.listMeditation.length != 0
          ? DropdownButton2(
              dropdownPadding: EdgeInsets.zero,
              focusColor: R.color.white,
              dropdownDecoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(8)),
              customButton: Image.asset(
                R.drawable.ic_filter_list,
                width: 24.h,
                height: 24.h,
              ),
              items: [
                DropdownMenuItem<Widget>(
                    enabled: false,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () {
                                  id.clear();
                                  NavigationUtils.pop(context);
                                  _cubit.getReportMeditation();
                                  _cubit.getDetailReportMeditation(
                                      reportType: ReportType.DAY,
                                      meditationIds: id);
                                  _tabController.index = 0;
                                },
                                child: Text(
                                  R.string.clear_filter.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 12.sp,
                                          color: R.color.primaryColor),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                              SizedBox(width: 20.w),
                              InkWell(
                                onTap: () {
                                  _cubit.getReportMeditation(meditationIds: id);
                                  _cubit.getDetailReportMeditation(
                                      reportType: ReportType.DAY,
                                      meditationIds: id);
                                  _tabController.index = 0;
                                  NavigationUtils.pop(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(48.h),
                                      color: R.color.primaryColor),
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      15.h, 2.h, 15.h, 4.h),
                                  child: Text(
                                    R.string.filter.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: R.color.white),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(height: 1.h, color: R.color.grey),
                      ],
                    )),
                ..._cubit.listMeditation.map((item) {
                  return DropdownMenuItem<int>(
                    value: item.id,
                    enabled: false,
                    child: StatefulBuilder(
                      builder: (context, menuSetState) {
                        final _isSelected = id.contains(item.id);
                        return InkWell(
                          onTap: () {
                            _isSelected
                                ? id.remove(item.id)
                                : id.add(item.id ?? 0);
                            setState(() {});
                            menuSetState(() {});
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.h, vertical: 10.h),
                            child: Row(
                              children: [
                                _isSelected
                                    ? Image.asset(
                                        R.drawable.ic_check_box,
                                        height: 16.h,
                                      )
                                    : Image.asset(
                                        R.drawable.ic_un_ckeck_box,
                                        height: 16.h,
                                      ),
                                SizedBox(width: 8.w),
                                Expanded(
                                  child: Text(
                                    item.title ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: _isSelected
                                                ? R.color.primaryColor
                                                : R.color.grey),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }),
              ],
              value: id.isEmpty ? null : id.last,
              onChanged: (value) {},
              dropdownWidth: 200.w,
              dropdownMaxHeight: 250.h,
              enableFeedback: true,
              itemPadding: EdgeInsets.zero,
              selectedItemBuilder: (context) {
                return _cubit.listMeditation.map(
                  (item) {
                    return Container(
                      alignment: AlignmentDirectional.center,
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        id.join(', '),
                        style: TextStyle(
                          fontSize: 14,
                          overflow: TextOverflow.ellipsis,
                        ),
                        maxLines: 1,
                      ),
                    );
                  },
                ).toList();
              },
            )
          : SizedBox(),
    );
  }

  Widget _widgetMeditation(title, num) {
    return Container(
        decoration: BoxDecoration(
          color: R.color.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          boxShadow: [
            BoxShadow(
              color: R.color.shadowColor,
              offset: Offset(0.0, 8),
              blurRadius: 10,
            )
          ],
        ),
        child: Column(
          children: [
            SizedBox(height: 8.h),
            Text(
              title,
              style: Theme.of(context).textTheme.bodySmallText.copyWith(
                  color: R.color.darkGrey,
                  fontSize: 10.sp,
                  fontWeight: FontWeight.w500,
                  height: 16 / 10),
              textAlign: TextAlign.center,
            ),
            Text(
              '$num',
              style: Theme.of(context).textTheme.bodySmallText.copyWith(
                  color: R.color.primaryColor,
                  fontSize: 20.sp,
                  fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 8.h)
          ],
        ));
  }

  Widget getTitleDuration(double value, TitleMeta meta) {
    final index = value.toInt();
    String text = DateFormat("dd").format(DateTime.fromMillisecondsSinceEpoch(
        _cubit.detailReportMeditation?.durationData?[index].horizontal ?? 0));
    return SideTitleWidget(
      axisSide: meta.axisSide,
      angle: 0,
      space: 4,
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 12.sp, height: 24 / 10),
      ),
    );
  }

  Widget getTitleDurationMonth(double value, TitleMeta meta) {
    final index = value.toInt();
    String text = DateFormat("M").format(DateTime.fromMillisecondsSinceEpoch(
        _cubit.detailReportMeditation?.durationData?[index].horizontal ?? 0));
    return SideTitleWidget(
      axisSide: meta.axisSide,
      angle: 0,
      space: 4,
      child: Text(
        "T" + text,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 12.sp, height: 24 / 10),
      ),
    );
  }

  Widget getTitlesCount(double value, TitleMeta meta) {
    final index = value.toInt();
    String text = DateFormat("dd").format(DateTime.fromMillisecondsSinceEpoch(
        _cubit.detailReportMeditation?.countData?[index].horizontal ?? 0));
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 4,
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 12.sp, height: 24 / 10),
      ),
    );
  }

  Widget getCountMonth(double value, TitleMeta meta) {
    final index = value.toInt();
    String text = DateFormat("M").format(DateTime.fromMillisecondsSinceEpoch(
        _cubit.detailReportMeditation?.countData?[index].horizontal ?? 0));
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 4,
      child: Text(
        "T" + text,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 12.sp, height: 24 / 10),
      ),
    );
  }

  Widget getDurationWeek(double value, TitleMeta meta) {
    final index = value.toInt();
    String text = "${index + 1}";
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 4,
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 12.sp, height: 24 / 10),
      ),
    );
  }

  Widget buildChartDuration(int index, GetTitleWidgetFunction widget) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (index == 0) ...[
          buildTitleChart(R.string.duration_of_meditation_per_day.tr())
        ],
        if (index == 1) ...[
          buildTitleChart(R.string.meditation_time_per_week.tr())
        ],
        if (index == 2) ...[
          buildTitleChart(R.string.meditation_duration_per_month.tr())
        ],
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
              3,
              (index) => Image.asset(
                    R.drawable.ic_diamond,
                    height: 24.h,
                    width: 24.w,
                  )),
        ),
        SizedBox(height: 10.h),
        SingleChildScrollView(
          controller: _scrollController,
          scrollDirection: Axis.horizontal,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
            height: 300.h,
            width: index == 0
                ? MediaQuery.of(context).size.width.w + 300.w
                : MediaQuery.of(context).size.width.w -
                    (index == 2 ? 60.w : 100.w),
            child: _cubit.detailReportMeditation?.durationData != null
                ? BarChart(
                    BarChartData(
                      alignment: BarChartAlignment.spaceBetween,
                      borderData: FlBorderData(
                        show: true,
                        border: Border.symmetric(
                          horizontal: BorderSide(
                              color: R.color.primaryColor, width: 0.5),
                        ),
                      ),
                      titlesData: FlTitlesData(
                        show: true,
                        leftTitles: AxisTitles(
                          drawBehindEverything: true,
                          sideTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 30,
                            getTitlesWidget: (value, meta) {
                              return Text(
                                value.toInt().toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(fontSize: 12.sp, height: 24 / 12),
                                textAlign: TextAlign.left,
                              );
                            },
                          ),
                        ),
                        bottomTitles: AxisTitles(
                          sideTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 40,
                            getTitlesWidget:
                                index == 1 ? getDurationWeek : widget,
                          ),
                        ),
                        rightTitles: AxisTitles(),
                        topTitles: AxisTitles(),
                      ),
                      gridData: FlGridData(
                        show: true,
                        drawVerticalLine: false,
                        getDrawingHorizontalLine: (value) => FlLine(
                          color: R.color.primaryColor,
                          dashArray: null,
                          strokeWidth: 0.5,
                        ),
                      ),
                      barGroups:
                          _cubit.detailReportMeditation?.durationData != null
                              ? _cubit.detailReportMeditation?.durationData
                                  ?.asMap()
                                  .entries
                                  .map((e) {
                                  final index = e.key;
                                  final data = e.value;
                                  return generateBarGroup(
                                      index,
                                      double.parse("${data.vertical}"),
                                      R.color.blueChart,
                                      BorderRadius.zero);
                                }).toList()
                              : null,
                      maxY: _cubit.detailReportMeditation?.durationData != null
                          ? (double.parse(
                                "${_cubit.detailReportMeditation?.durationData?.map((e) => e.vertical).reduce((value, element) => (value! > element!) ? value : element)} ",
                              ) +
                              15)
                          : 0,
                      barTouchData: BarTouchData(
                        enabled: true,
                        handleBuiltInTouches: false,
                        touchTooltipData: BarTouchTooltipData(
                            fitInsideHorizontally: true,
                            tooltipBgColor: Colors.transparent,
                            tooltipMargin: 0,
                            tooltipPadding: EdgeInsets.zero,
                            getTooltipItem: (
                              BarChartGroupData group,
                              int groupIndex,
                              BarChartRodData rod,
                              int rodIndex,
                            ) {
                              return BarTooltipItem(
                                rod.toY == 0 ? "" : rod.toY.round().toString(),
                                TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: rod.color!,
                                    fontSize: 10.sp,
                                    shadows: const [
                                      Shadow(
                                        color: Colors.black26,
                                      )
                                    ]),
                              );
                            }),
                      ),
                    ),
                  )
                : SizedBox(),
          ),
        ),
      ],
    );
  }

  Widget buildChartCount(int index, GetTitleWidgetFunction widget) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (index == 0) ...[
          buildTitleChart(R.string.number_of_meditation_per_day.tr())
        ],
        if (index == 1) ...[
          buildTitleChart(R.string.number_of_meditation_per_week.tr())
        ],
        if (index == 2) ...[
          buildTitleChart(R.string.number_of_meditation_per_month.tr())
        ],
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
              3,
              (index) => Image.asset(
                    R.drawable.ic_diamond,
                    height: 24.h,
                    width: 24.w,
                  )),
        ),
        SizedBox(height: 10.h),
        SingleChildScrollView(
          controller: _scrollController,
          scrollDirection: Axis.horizontal,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
            height: 300.h,
            width: index == 0
                ? MediaQuery.of(context).size.width.w + 200.w
                : MediaQuery.of(context).size.width.w -
                    (index == 2 ? 60.w : 100.w),
            child: _cubit.detailReportMeditation?.countData != null
                ? BarChart(
                    BarChartData(
                      alignment: BarChartAlignment.spaceBetween,
                      borderData: FlBorderData(
                        show: true,
                        border: Border.symmetric(
                          horizontal: BorderSide(
                              color: R.color.primaryColor, width: 0.5),
                        ),
                      ),
                      titlesData: FlTitlesData(
                        show: true,
                        leftTitles: AxisTitles(
                          drawBehindEverything: true,
                          sideTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 40,
                            getTitlesWidget: (value, meta) {
                              return Text(
                                value.toInt().toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(fontSize: 12.sp, height: 24 / 12),
                                textAlign: TextAlign.left,
                              );
                            },
                          ),
                        ),
                        bottomTitles: AxisTitles(
                          sideTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 40,
                            getTitlesWidget:
                                index == 1 ? getDurationWeek : widget,
                          ),
                        ),
                        rightTitles: AxisTitles(),
                        topTitles: AxisTitles(),
                      ),
                      gridData: FlGridData(
                        show: true,
                        drawVerticalLine: false,
                        getDrawingHorizontalLine: (value) => FlLine(
                          color: R.color.primaryColor,
                          dashArray: null,
                          strokeWidth: 0.5,
                        ),
                      ),
                      barGroups:
                          _cubit.detailReportMeditation?.countData != null
                              ? _cubit.detailReportMeditation?.countData
                                  ?.asMap()
                                  .entries
                                  .map((e) {
                                  final index = e.key;
                                  final data = e.value;
                                  return generateBarGroup(
                                      index,
                                      double.parse("${data.vertical}"),
                                      R.color.blueChart,
                                      BorderRadius.zero);
                                }).toList()
                              : null,
                      maxY: _cubit.detailReportMeditation?.countData != null
                          ? (double.parse(
                                "${_cubit.detailReportMeditation?.countData?.map((e) => e.vertical).reduce((value, element) => (value! > element!) ? value : element)} ",
                              ) +
                              15)
                          : 0,
                      barTouchData: BarTouchData(
                        enabled: true,
                        handleBuiltInTouches: false,
                        touchTooltipData: BarTouchTooltipData(
                            tooltipBgColor: Colors.transparent,
                            tooltipMargin: 0,
                            tooltipPadding: EdgeInsets.zero,
                            getTooltipItem: (
                              BarChartGroupData group,
                              int groupIndex,
                              BarChartRodData rod,
                              int rodIndex,
                            ) {
                              return BarTooltipItem(
                                rod.toY == 0 ? "" : rod.toY.round().toString(),
                                TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: rod.color!,
                                    fontSize: 10.sp,
                                    shadows: const [
                                      Shadow(
                                        color: Colors.black26,
                                      )
                                    ]),
                              );
                            }),
                      ),
                    ),
                  )
                : SizedBox(),
          ),
        ),
      ],
    );
  }

  Widget buildTitleChart(String title) {
    return Text(title,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 14.sp, height: 24 / 12));
  }
}
