import 'package:flutter/material.dart';

@immutable
abstract class MeditationDetailReportState {
  MeditationDetailReportState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialMeditationDetailReportState extends MeditationDetailReportState {}

class MeditationDetailReportLoading extends MeditationDetailReportState {
  @override
  String toString() => 'MeditationDetailReportLoading';
}

class GetListCategorySuccess extends MeditationDetailReportState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends MeditationDetailReportState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class MeditationDetailReportSuccess extends MeditationDetailReportState {
  @override
  String toString() {
    return 'MeditationDetailReportSuccess';
  }
}

class ChooseCommunitySuccess extends MeditationDetailReportState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

class MeditationDetailReportFailure extends MeditationDetailReportState {
  final String error;

  MeditationDetailReportFailure(this.error);

  @override
  String toString() => 'BodyParameterFailure { error: $error }';
}

class SubmitMeditationDetailReportSuccess extends MeditationDetailReportState {
  @override
  String toString() {
    return 'SubmitMeditationDetailReportSuccess{}';
  }
}
