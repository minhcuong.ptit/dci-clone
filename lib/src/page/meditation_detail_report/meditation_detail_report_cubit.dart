import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/meditation_response.dart';
import 'package:imi/src/data/network/response/report_meditation_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/utils/enum.dart';

import '../../data/network/response/detail_report_meditation_response.dart';
import 'meditation_detail_report.dart';

class MeditationDetailReportCubit extends Cubit<MeditationDetailReportState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  FetchGeneralMeditationReport? fetchGeneralMeditationReport;
  List<FetchMeditationsData> listMeditation = [];
  FetchDetailMeditationReport? detailReportMeditation;
  String? nextToken;
  bool isSelectItem = false;

  MeditationDetailReportCubit(
      {required this.repository, required this.graphqlRepository})
      : super(InitialMeditationDetailReportState());

  void getReportMeditation({List<int>? meditationIds}) async {
    emit(MeditationDetailReportLoading());
    ApiResult<FetchGeneralMeditationReport> getMeditation =
        await graphqlRepository.getReportMeditation(
            meditationIds: meditationIds);
    getMeditation.when(success: (FetchGeneralMeditationReport data) async {
      fetchGeneralMeditationReport = data;
      emit(MeditationDetailReportSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(MeditationDetailReportFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListMeditation({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh
        ? MeditationDetailReportLoading()
        : InitialMeditationDetailReportState());
    if (isLoadMore != true) {
      nextToken = null;
      listMeditation.clear();
    }
    ApiResult<FetchMeditation> getMeditation =
        await graphqlRepository.getMeditation(nextToken: nextToken);
    getMeditation.when(success: (FetchMeditation data) async {
      if (data.data != null) {
        listMeditation.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(MeditationDetailReportSuccess());
      }
    }, failure: (NetworkExceptions error) async {
      emit(MeditationDetailReportFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshReport() {
    emit(MeditationDetailReportLoading());
    getReportMeditation();
    emit(InitialMeditationDetailReportState());
  }

  void getDetailReportMeditation({ReportType? reportType,List<int>? meditationIds}) async {
    emit(MeditationDetailReportLoading());
    ApiResult<FetchDetailMeditationReport> getMeditation =
        await graphqlRepository.getDetailReportMeditation(
            reportType: reportType,meditationIds: meditationIds);
    getMeditation.when(success: (FetchDetailMeditationReport data) async {
      detailReportMeditation = data;
      emit(MeditationDetailReportSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(MeditationDetailReportFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }
}
