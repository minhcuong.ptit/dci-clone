import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_cubit.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_state.dart';
import 'package:imi/src/page/search_user_chat/search_user_detail/search_user_detail_cubit.dart';
import 'package:imi/src/page/search_user_chat/search_user_detail/search_user_detail_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar_messages_widget.dart';
import 'package:imi/src/widgets/search/search_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SearchUserChatPage extends StatefulWidget {
  const SearchUserChatPage({Key? key}) : super(key: key);

  @override
  State<SearchUserChatPage> createState() => _SearchUserChatPageState();
}

class _SearchUserChatPageState extends State<SearchUserChatPage>
    with
        AutomaticKeepAliveClientMixin<SearchUserChatPage>,
        SingleTickerProviderStateMixin {
  late SearchUserChatCubit _cubit;
  TextEditingController _searchController = TextEditingController();
  ScrollController _bodyScrollController = ScrollController();
  RefreshController _refreshController = RefreshController();
  TabController? _tabController;
  Timer? _debounce;
  late List<SearchUserDetailCubit> nestedBlocs;

  @override
  bool get wantKeepAlive => false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = SearchUserChatCubit(repository, graphqlRepository);
    _bodyScrollController = ScrollController();
    nestedBlocs = List.generate(
        4,
        (index) => SearchUserDetailCubit(_cubit.communityType(index))
          ..search(keyword: ""));
  }

  @override
  void dispose() {
    super.dispose();
    _searchController.dispose();
    _bodyScrollController.dispose();
    _refreshController.dispose();
    _cubit.close();
    nestedBlocs.forEach((element) {
      element.close();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: R.color.primaryColor,
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<SearchUserChatCubit, SearchUserChatState>(
            listener: (BuildContext context, state) {
              if (state is! SearchUserChatLoading) {
                _refreshController.refreshCompleted();
                _refreshController.loadComplete();
              }
            },
            builder: (BuildContext context, state) {
              return SafeArea(
                top: true,
                child: Scaffold(
                  appBar: appBarMessages(context, onSelectedIcon: () {
                    NavigationUtils.pop(context);
                  },
                      onCustomerCare: () {},
                      iconBack: CupertinoIcons.back,
                      size: 25.h),
                  body: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SearchWidget(
                        autoFocus: false,
                        fillColor: R.color.grey100,
                        searchController: _searchController,
                        onSubmit: (text) {
                          nestedBlocs.forEach((element) {
                            element.search(
                                keyword: text.replaceAll("\\", "\t"));
                          });
                        },
                        type: Const.SEARCH_USER_MESSAGES,
                        onChange: (text) {
                          nestedBlocs.forEach((element) {
                            element.search(
                                keyword: text.replaceAll("\\", "\t"));
                          });
                        },
                      ),
                      SizedBox(height: 10.h),
                      BlocBuilder<SearchUserChatCubit, SearchUserChatState>(
                        buildWhen: (previous, current) {
                          return current is SearchUserChatCountChange;
                        },
                        builder: (context, state) {
                          return TabBar(
                            isScrollable: true,
                            onTap: (value) {
                              _cubit.onTapChange(value);
                              _bodyScrollController.animateTo(
                                  value * MediaQuery.of(context).size.width,
                                  duration: const Duration(milliseconds: 200),
                                  curve: Curves.linear);
                            },
                            tabs: List.generate(4, (index) {
                              return Container(
                                height: 24,
                                padding:
                                    EdgeInsets.fromLTRB(12.w, 2.h, 12.w, 0.h),
                                decoration: BoxDecoration(
                                    color: _cubit.currentTab == index
                                        ? R.color.lightBlue
                                        : R.color.white,
                                    borderRadius: BorderRadius.circular(24)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      _cubit.listCategory[index],
                                      style: Theme.of(context)
                                          .textTheme
                                          .medium500
                                          .copyWith(
                                              color: _cubit.currentTab == index
                                                  ? R.color.white
                                                  : R.color.grey,
                                              fontSize: 10.sp,
                                              height: 1.0),
                                    ),
                                    SizedBox(width: 3.h),
                                    Text(
                                      "(${_cubit.count[index]})",
                                      style: Theme.of(context)
                                          .textTheme
                                          .medium500
                                          .copyWith(
                                              fontSize: 10.sp,
                                              height: 1,
                                              color: _cubit.currentTab == index
                                                  ? R.color.white
                                                  : R.color.grey),
                                    ),
                                    Visibility(
                                      visible: _cubit.currentTab == index,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.w, bottom: 2.h),
                                        child: Image.asset(
                                          R.drawable.ic_check_true,
                                          color: R.color.white,
                                          width: 11.h,
                                          height: 11.h,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            }),
                            controller: _tabController,
                            indicatorSize: TabBarIndicatorSize.tab,
                          );
                        },
                      ),
                      SizedBox(height: 10.h),
                      Expanded(
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            return SingleChildScrollView(
                              controller: _bodyScrollController,
                              scrollDirection: Axis.horizontal,
                              physics: const NeverScrollableScrollPhysics(),
                              child: SizedBox(
                                height: constraints.maxHeight,
                                width: constraints.maxWidth * 4,
                                child: Row(
                                  children: List.generate(4, (index) {
                                    return SizedBox(
                                      height: constraints.maxHeight,
                                      width: constraints.maxWidth,
                                      child: BlocProvider(
                                        key: ValueKey(index),
                                        create: (context) => nestedBlocs[index],
                                        child: SearchUserDetailPage(
                                          type: _cubit.communityType(index),
                                        ),
                                      ),
                                    );
                                  }),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  // Widget buildListDciChat() {
  //   return ListView.builder(
  //       padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 10.h),
  //       itemCount: length(_cubit.tabIndex),
  //       shrinkWrap: true,
  //       physics: NeverScrollableScrollPhysics(),
  //       itemBuilder: (context, int index) {
  //         return Padding(
  //           padding: EdgeInsets.only(bottom: 16.h),
  //           child: InkWell(
  //             highlightColor: R.color.white,
  //             onTap: () {
  //               if (_cubit.tabIndex == 0) {
  //                 NavigationUtils.rootNavigatePage(
  //                     context,
  //                     ChatPage(
  //                       chatType:
  //                           _cubit.listAllCommunity[index].userUuid != null
  //                               ? ChatType.private
  //                               : ChatType.group,
  //                       channelId:
  //                           _cubit.listAllCommunity[index].channelPubnubId,
  //                       userUID: _cubit.listAllCommunity[index].userUuid,
  //                       communityId:
  //                           _cubit.listAllCommunity[index].communityId ?? 0,
  //                       channelName:
  //                           _cubit.listAllCommunity[index].communityName ?? "",
  //                       timeToken: _cubit.listAllCommunity[index].lastMessage
  //                               ?.timeToken ??
  //                           0,
  //                     ));
  //               } else if (_cubit.tabIndex == 1) {
  //                 NavigationUtils.rootNavigatePage(
  //                     context,
  //                     ChatPage(
  //                       chatType:
  //                           _cubit.listStudyGroupCommunity[index].userUuid !=
  //                                   null
  //                               ? ChatType.private
  //                               : ChatType.group,
  //                       channelId: _cubit
  //                           .listStudyGroupCommunity[index].channelPubnubId,
  //                       userUID: _cubit.listStudyGroupCommunity[index].userUuid,
  //                       communityId:
  //                           _cubit.listStudyGroupCommunity[index].communityId ??
  //                               0,
  //                       channelName: _cubit
  //                               .listStudyGroupCommunity[index].communityName ??
  //                           "",
  //                       timeToken: _cubit.listStudyGroupCommunity[index]
  //                               .lastMessage?.timeToken ??
  //                           0,
  //                     ));
  //               } else if (_cubit.tabIndex == 2) {
  //                 NavigationUtils.rootNavigatePage(
  //                     context,
  //                     ChatPage(
  //                       chatType:
  //                           _cubit.listBasicGroupCommunity[index].userUuid !=
  //                                   null
  //                               ? ChatType.private
  //                               : ChatType.group,
  //                       channelId: _cubit
  //                           .listBasicGroupCommunity[index].channelPubnubId,
  //                       userUID: _cubit.listBasicGroupCommunity[index].userUuid,
  //                       communityId:
  //                           _cubit.listBasicGroupCommunity[index].communityId ??
  //                               0,
  //                       channelName: _cubit
  //                               .listBasicGroupCommunity[index].communityName ??
  //                           "",
  //                       timeToken: _cubit.listBasicGroupCommunity[index]
  //                               .lastMessage?.timeToken ??
  //                           0,
  //                     ));
  //               } else if (_cubit.tabIndex == 3) {
  //                 NavigationUtils.rootNavigatePage(
  //                     context,
  //                     ChatPage(
  //                       chatType:
  //                           _cubit.listUserCommunity[index].userUuid != null
  //                               ? ChatType.private
  //                               : ChatType.group,
  //                       channelId:
  //                           _cubit.listUserCommunity[index].channelPubnubId,
  //                       userUID: _cubit.listUserCommunity[index].userUuid,
  //                       communityId:
  //                           _cubit.listUserCommunity[index].communityId ?? 0,
  //                       channelName:
  //                           _cubit.listUserCommunity[index].communityName ?? "",
  //                       timeToken: _cubit.listUserCommunity[index].lastMessage
  //                               ?.timeToken ??
  //                           0,
  //                     ));
  //               }
  //             },
  //             child: Row(
  //               children: [
  //                 if (_cubit.tabIndex == 0) ...[
  //                   AvatarWidget(
  //                       avatar: _cubit.listAllCommunity[index].communityImage,
  //                       size: 60.h),
  //                   SizedBox(width: 8.h),
  //                   Expanded(
  //                     child: Text(
  //                       _cubit.listAllCommunity[index].communityName ?? "",
  //                       style: Theme.of(context)
  //                           .textTheme
  //                           .regular400
  //                           .copyWith(fontSize: 14.sp),
  //                       maxLines: 2,
  //                       overflow: TextOverflow.ellipsis,
  //                     ),
  //                   )
  //                 ],
  //                 if (_cubit.tabIndex == 1) ...[
  //                   AvatarWidget(
  //                       avatar: _cubit
  //                           .listStudyGroupCommunity[index].communityImage,
  //                       size: 60.h),
  //                   SizedBox(width: 8.h),
  //                   Expanded(
  //                     child: Text(
  //                       _cubit.listStudyGroupCommunity[index].communityName ??
  //                           "",
  //                       style: Theme.of(context)
  //                           .textTheme
  //                           .regular400
  //                           .copyWith(fontSize: 14.sp),
  //                       maxLines: 2,
  //                       overflow: TextOverflow.ellipsis,
  //                     ),
  //                   )
  //                 ],
  //                 if (_cubit.tabIndex == 2) ...[
  //                   AvatarWidget(
  //                       avatar: _cubit
  //                           .listBasicGroupCommunity[index].communityImage,
  //                       size: 60.h),
  //                   SizedBox(width: 8.h),
  //                   Expanded(
  //                     child: Text(
  //                       _cubit.listBasicGroupCommunity[index].communityName ??
  //                           "",
  //                       style: Theme.of(context)
  //                           .textTheme
  //                           .regular400
  //                           .copyWith(fontSize: 14.sp),
  //                       maxLines: 2,
  //                       overflow: TextOverflow.ellipsis,
  //                     ),
  //                   )
  //                 ],
  //                 if (_cubit.tabIndex == 3) ...[
  //                   AvatarWidget(
  //                       avatar: _cubit.listUserCommunity[index].communityImage,
  //                       size: 60.h),
  //                   SizedBox(width: 8.h),
  //                   Expanded(
  //                     child: Text(
  //                       _cubit.listUserCommunity[index].communityName ?? "",
  //                       style: Theme.of(context)
  //                           .textTheme
  //                           .regular400
  //                           .copyWith(fontSize: 14.sp),
  //                       maxLines: 2,
  //                       overflow: TextOverflow.ellipsis,
  //                     ),
  //                   )
  //                 ],
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }

  void onSearchChanged(String text) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      // _cubit.searchCommunity(_cubit.tabIndex, keyWord: text);
    });
  }

// int? length(int index) {
//   int? length;
//   if (index == 0) {
//     length = _cubit.listAllCommunity.length;
//   } else if (index == 1) {
//     length = _cubit.listStudyGroupCommunity.length;
//   } else if (index == 2) {
//     length = _cubit.listBasicGroupCommunity.length;
//   } else if (index == 3) {
//     length = _cubit.listUserCommunity.length;
//   }
//   return length;
// }
}
