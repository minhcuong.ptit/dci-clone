import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/search_user_chat_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_state.dart';
import 'package:imi/src/utils/enum.dart';

class SearchUserChatCubit extends Cubit<SearchUserChatState> {
  SearchUserChatCubit(this.repository, this.graphqlRepository)
      : super(SearchUserChatInitial());
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<FetchJoinedChannelsDataDetail> listSearchCommunity = [];
  FetchJoinedChannelsData? fetchJoinedChannelsData;
  String? nextToken;
  List<int> count = [0, 0, 0, 0];
  List<String> listCategory = [
    R.string.all.tr(),
    R.string.study_groups_appbar.tr(),
    R.string.community_group.tr(),
    R.string.user.tr(),
  ];

  int currentTab = 0;

  CommunityType? communityType(int index) {
    CommunityType? communityType;
    switch (index) {
      case 0:
        communityType = null;
        break;
      case 1:
        communityType = CommunityType.STUDY_GROUP;
        break;
      case 2:
        communityType = CommunityType.BASIC_GROUP;
        break;
      case 3:
        communityType = CommunityType.INDIVIDUAL;
        break;
    }
    return communityType;
  }

  void setCount(CommunityType? type, int count) {
    emit(SearchUserChatLoading());
    if (type == null) {
      this.count[0] = count;
    } else if (type == CommunityType.STUDY_GROUP) {
      this.count[1] = count;
    } else if (type == CommunityType.BASIC_GROUP) {
      this.count[2] = count;
    } else if (type == CommunityType.INDIVIDUAL) {
      this.count[3] = count;
    }
    emit(SearchUserChatCountChange());
  }

  void onTapChange(int index) {
    currentTab = index;
    emit(SearchUserChatCountChange());
  }
}
