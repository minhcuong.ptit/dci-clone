part of 'search_user_detail_cubit.dart';

abstract class SearchUserDetailState {}

class SearchUserDetailInitial extends SearchUserDetailState {}

class SearchUserDetailSuccess extends SearchUserDetailState {
  final int count;
  final CommunityType? type;
  SearchUserDetailSuccess(this.type, this.count);

  @override
  String toString() {
    return 'SearchUserDetailSuccess{}';
  }
}

class SearchUserDetailFailure extends SearchUserDetailState {
  final String error;

  SearchUserDetailFailure(this.error);

  @override
  String toString() {
    return 'SearchUserDetailFailure{error: $error}';
  }
}

class SearchUserDetailLoading extends SearchUserDetailState {
  @override
  String toString() {
    return 'SearchUserDetailLoading{}';
  }
}
