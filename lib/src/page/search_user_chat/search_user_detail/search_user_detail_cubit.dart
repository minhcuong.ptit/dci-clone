import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/response/search_user_chat_response.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';

part 'search_user_detail_state.dart';

class SearchUserDetailCubit extends Cubit<SearchUserDetailState> {
  final CommunityType? communityType;
  final AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
  List<FetchJoinedChannelsDataDetail> listSearchCommunity = [];
  String? _nextToken;
  String? lastSearchKeyWord;
  bool cancelLoadMore = true;

  SearchUserDetailCubit(this.communityType) : super(SearchUserDetailInitial());

  void search({String? keyword, bool isLoadMore = false}) async {
    emit(!isLoadMore ? SearchUserDetailLoading() : SearchUserDetailInitial());
    if ((lastSearchKeyWord == keyword || keyword == null) && !isLoadMore) {
      return;
    }
    if (keyword != null) {
      lastSearchKeyWord = keyword;
    }
    if (isLoadMore != true) {
      _nextToken = null;
      listSearchCommunity.clear();
    }
    final res = await graphqlRepository.getSearchUserChat(
        communityType: communityType,
        searchKey: lastSearchKeyWord,
        nextToken: _nextToken);
    res.when(success: (FetchJoinedChannelsData data) {
      listSearchCommunity.addAll(data.data ?? []);
      _nextToken = data.nextToken;
      if (data.data!.length < Const.NETWORK_DEFAULT_LIMIT) {
        cancelLoadMore = false;
      }
      emit(SearchUserDetailSuccess(communityType, data.count ?? 0));
    }, failure: (NetworkExceptions error) async {
      emit(SearchUserDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
