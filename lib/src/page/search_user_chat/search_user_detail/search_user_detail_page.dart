import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_cubit.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_state.dart';
import 'package:imi/src/page/search_user_chat/search_user_detail/search_user_detail_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SearchUserDetailPage extends StatefulWidget {
  final CommunityType? type;

  const SearchUserDetailPage({Key? key, required this.type}) : super(key: key);

  @override
  State<SearchUserDetailPage> createState() => _SearchUserDetailPageState();
}

class _SearchUserDetailPageState extends State<SearchUserDetailPage>
    with AutomaticKeepAliveClientMixin<SearchUserDetailPage> {
  late final SearchUserDetailCubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _refreshController.dispose();
    _cubit.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white,
      body: BlocConsumer<SearchUserDetailCubit, SearchUserDetailState>(
        bloc: _cubit,
        listener: (context, state) {
          if (state is SearchUserDetailSuccess) {
            BlocProvider.of<SearchUserChatCubit>(context)
                .setCount(state.type, state.count);
          }
        },
        builder: (context, state) {
          return state is! SearchUserDetailLoading &&
                  _cubit.listSearchCommunity.isEmpty
              ? Visibility(
                  visible: state is SearchUserDetailSuccess,
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.2),
                    child: Center(
                      child: Text(
                        R.string.no_search_results.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .regular400
                            .copyWith(fontSize: 14.sp, color: R.color.grey),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
              : SmartRefresher(
                  controller: _refreshController,
                  enablePullDown: false,
                  enablePullUp: _cubit.cancelLoadMore,
                  onLoading: () => _cubit.search(isLoadMore: true),
                  child: state is SearchUserDetailLoading
                      ? Padding(
                          padding: EdgeInsets.only(bottom: 50.h),
                          child: Center(
                            child: CircularProgressIndicator(
                                color: R.color.primaryColor),
                          ),
                        )
                      : ListView.builder(
                          itemCount: _cubit.listSearchCommunity.length,
                          padding: EdgeInsets.symmetric(horizontal: 20.h),
                          itemBuilder: (context, int index) {
                            return Padding(
                              padding: EdgeInsets.only(bottom: 15.h),
                              child: Row(
                                children: [
                                  AvatarWidget(
                                      avatar: _cubit.listSearchCommunity[index]
                                          .communityImage,
                                      size: 60.h),
                                  SizedBox(width: 8.h),
                                  Expanded(
                                    child: Text(
                                      _cubit.listSearchCommunity[index]
                                              .communityName ??
                                          "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .regular400
                                          .copyWith(fontSize: 14.sp),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            );
                          }),
                );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
