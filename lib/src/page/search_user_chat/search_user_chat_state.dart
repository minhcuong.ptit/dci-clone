import 'package:equatable/equatable.dart';
import 'package:imi/src/utils/enum.dart';

abstract class SearchUserChatState {}

class SearchUserChatInitial extends SearchUserChatState {}

class SearchUserChatCountChange extends SearchUserChatState {}

class SearchUserChatLoading extends SearchUserChatState {
  @override
  String toString() {
    return 'SearchUserChatLoading{}';
  }
}

class SearchUserChatFailure extends SearchUserChatState {
  final String error;

  SearchUserChatFailure(this.error);

  @override
  String toString() {
    return 'SearchUserChatFailure{error: $error}';
  }
}

class SearchUserChatSuccess extends SearchUserChatState {
  @override
  String toString() {
    return 'SearchUserChatSuccess{}';
  }
}

class SearchAllSuccess extends SearchUserChatState {
  @override
  String toString() {
    return 'SearchAllSuccess{}';
  }
}

class SearchStudyGroupSuccess extends SearchUserChatState {
  @override
  String toString() {
    return 'SearchStudyGroupSuccess{}';
  }
}

class SearchBasicGroupSuccess extends SearchUserChatState {
  @override
  String toString() {
    return 'SearchBasicGroupSuccess{}';
  }
}

class SearchChatSuccess extends SearchUserChatState {
  @override
  String toString() {
    return 'SearchChatSuccess{}';
  }
}
