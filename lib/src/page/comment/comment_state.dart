import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CommentState extends Equatable {
  CommentState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialCommentState extends CommentState {
  @override
  String toString() {
    return 'InitialCommentState{}';
  }
}

class CommentFailure extends CommentState {
  final String? error;

  CommentFailure(this.error);

  @override
  String toString() {
    return 'CommentFailure{error: $error}';
  }
}

class CommentLoading extends CommentState {
  @override
  String toString() {
    return 'CommentLoading{}';
  }
}

class CommentLikePost extends CommentState {
  @override
  String toString() {
    return 'CommentLikePost{}';
  }
}

class CommentBookmarkPost extends CommentState {
  @override
  String toString() {
    return 'CommentBookmarkPost{}';
  }
}

class GetCommentSuccess extends CommentState {
  @override
  String toString() {
    return 'GetCommentSuccess{}';
  }
}

class CommentSuccess extends CommentState {
  @override
  String toString() {
    return 'CommentSuccess{}';
  }
}

class EditCommentSuccess extends CommentState {
  @override
  String toString() {
    return 'EditCommentSuccess{}';
  }
}

class CommentReactionSuccess extends CommentState {
  @override
  String toString() {
    return 'CommentReactionSuccess{}';
  }
}

class CommentBookmarkSuccess extends CommentState {
  @override
  String toString() {
    return 'CommentBookmarkSuccess{}';
  }
}

class RemovePostSuccess extends CommentState {
  @override
  String toString() {
    return 'RemovePostSuccess{}';
  }
}

class RemoveCommentSuccess extends CommentState {
  @override
  String toString() {
    return 'RemoveCommentSuccess{}';
  }
}

class DeleteCommentGroundSuccess extends CommentState {
  @override
  String toString() {
    return 'DeleteCommentGroundSuccess{}';
  }
}

class GetReportSuccess extends CommentState {
  @override
  String toString() {
    return 'GetReportSuccess{}';
  }
}

class GetReportUserSuccess extends CommentState {
  @override
  String toString() {
    return 'GetReportUserSuccess{}';
  }
}

class GetReportGroupSuccess extends CommentState {
  @override
  String toString() {
    return 'GetReportGroupSuccess{}';
  }
}

class BlockedPostSuccess extends CommentState {
  @override
  String toString() => 'BlockedPostSuccess';
}

class PostEmpty extends CommentState {
  @override
  String toString() => 'PostEmpty';
}
