import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/comment.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/page/comment/comment_cubit.dart';
import 'package:imi/src/page/comment/comment_state.dart';
import 'package:imi/src/page/community/community.dart';
import 'package:imi/src/page/home_tab/home_tab.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/custom_expandable_text.dart';
import 'package:imi/src/widgets/dci_popup.dart';
import 'package:imi/src/widgets/full_screen_image_widget.dart';
import 'package:imi/src/widgets/pending_action.dart';
import 'package:imi/src/widgets/popup_report_widget.dart';
import 'package:imi/src/widgets/rich_input.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import '../../utils/dci_event_handle.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/time_ago.dart';
import '../../widgets/comment_tap_popup.dart';
import '../../widgets/detail_post_popup.dart';
import '../../widgets/remove_post_popup.dart';
import '../club_info/club_information/club_information.dart';
import '../likes/likes_page.dart';
import '../main/main.dart';
import '../post/post.dart';
import '../post/post_widget/post_widget.dart';
import '../profile/view_profile/view_profile.dart';

class CommentPage extends StatefulWidget {
  final bool? isNotification;
  final int? postId;
  final PostData? post;
  final ValueChanged<int> onCommentSuccess;
  final VoidCallback onRemovePostSuccess;
  final Function(bool like, int likeNumber) onLikeSuccess;
  final Function(bool bookmark) onBookmarkSuccess;

  const CommentPage({
    Key? key,
    this.post,
    required this.onCommentSuccess,
    required this.onLikeSuccess,
    this.postId,
    required this.onRemovePostSuccess,
    required this.onBookmarkSuccess, this.isNotification,
  }) : super(key: key);

  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  RefreshController _refreshController = RefreshController();
  RichInputController _controller = RichInputController();
  FocusNode _focusNode = FocusNode();
  late CommentCubit _cubit;
  late int _postId;
  List<String> myRoles = [];

  String? get uuid => appPreferences.getString(Const.ID);
  late ValueNotifier<bool> _isShowMenu;

  @override
  void initState() {
    super.initState();
    _isShowMenu = ValueNotifier(false);
    _postId = widget.postId ?? int.parse(widget.post?.id ?? '0');
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = CommentCubit(
        repository: repository,
        graphqlRepository: graphqlRepository,
        postId: _postId);
    _cubit.post = widget.post;
    _cubit.getData(postId: _postId);
    DCIEventHandler().subscribe(_onPostListener);
  }

  @override
  void dispose() {
    _isShowMenu.dispose();
    DCIEventHandler().unsubscribe(_onPostListener);
    super.dispose();
  }

  void _onPostListener(PostEventEvent event) {
    if (mounted && (event.postEventType == PostEventType.CREATE_NEW_PAGE)) {
      _cubit.getData(postId: _postId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: ()async{
        widget.isNotification==true?NavigationUtils.pushAndRemoveUtilPage(context,MainPage() ):null;
          return true;
        },
        child: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<CommentCubit, CommentState>(
            listener: (context, state) {
              if (state is! CommentLoading) {
                _refreshController.refreshCompleted();
              }
              if (state is CommentFailure)
                Utils.showErrorSnackBar(context, state.error);
              if (state is CommentSuccess) {
                int totalComment = (_cubit.post?.commentNumber ?? 0) + 1;
                _cubit.post?.commentNumber = totalComment;
                widget.onCommentSuccess(totalComment);
              }
              if (state is EditCommentSuccess) {
                _cubit.refreshEdit();
              }
              if (state is CommentReactionSuccess) {
                widget.onLikeSuccess(
                    _cubit.post?.isLike ?? false, _cubit.post?.likeNumber ?? 0);
              }
              if (state is CommentBookmarkSuccess) {
                if (mounted) {
                  setState(() {
                    _cubit.post?.isBookmark = !(_cubit.post?.isBookmark ?? false);
                  });
                  widget.onBookmarkSuccess(_cubit.post?.isBookmark ?? false);
                }
              }
              if (state is GetCommentSuccess) {
                widget.onCommentSuccess(_cubit.post?.commentNumber ?? 0);
                myRoles = _cubit.post?.communityV2?.groupView?.myRoles ?? [];
                _isShowMenu.value = ((uuid == _cubit.post?.userId) ||
                    (myRoles.contains(GroupMemberRole.ADMIN.name)) ||
                    (myRoles.contains(GroupMemberRole.LEADER.name)));
              }
              if (state is RemovePostSuccess) {
                NavigationUtils.pop(context,result: Const.ACTION_REFRESH);
                widget.onRemovePostSuccess();
              }
              if (state is DeleteCommentGroundSuccess) {
                // widget.onCommentSuccess(_cubit.post?.commentNumber ?? 0);
                _cubit.refreshDelete();
                // _cubit.refreshDelete();
              }
              if (state is GetReportGroupSuccess) {
                Utils.showToast(context, R.string.report_post_success.tr());
              }
              if (state is BlockedPostSuccess) {
                _cubit.isBlockPost = !_cubit.isBlockPost;
              }
              if (state is CommentSuccess) {
                Utils.hideKeyboard(context);
              }
            },
            builder: (context, state) {
              return Scaffold(
                appBar: appBar(context, R.string.post_detail.tr().toUpperCase(),
                    textStyle: Theme.of(context)
                        .textTheme
                        .bodyBold
                        .apply(color: R.color.textFieldTitle)
                        .copyWith(fontSize: 16.sp, fontWeight: FontWeight.w700),
                    leadingWidget: GestureDetector(
                        onTap: ()async{
                          widget.isNotification==true? NavigationUtils.pushAndRemoveUtilPage(context,MainPage()):
                          NavigationUtils.pop(context);
                        },
                        child: Icon(Icons.arrow_back_ios)),
                    rightWidget: _cubit.isBlockPost == true
                        ? SizedBox()
                        : _buildIconMenu(),
                    centerTitle: true),
                body: buildPage(context, state),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, CommentState state) {
    myRoles = _cubit.post?.communityV2?.groupView?.myRoles ?? [];
    return  state is PostEmpty
        ? Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.symmetric(
              vertical: 20.h,
            ),
            child: Text(
              R.string.the_article_does_not_exist.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 14.sp),
            ),
          )
        : Stack(
            children: [
              Container(
                height: double.infinity,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SmartRefresher(
                        controller: _refreshController,
                        onRefresh: () =>
                            _cubit.getData(postId: _postId, isRefresh: true),
                        child: ListView(
                          padding: EdgeInsets.zero,
                          shrinkWrap: true,
                          children: [
                            postWidget(_cubit.post, state, context),
                            Container(
                              height: 8.h,
                              color: R.color.grey50,
                            ),
                            Visibility(
                                visible: !Utils.isEmpty(_cubit.listComment),
                                child: Container(
                                  padding: EdgeInsets.all(16.h),
                                  child: Text(
                                    _cubit.post?.commentNumber == 1
                                        ? R.string.number_comment
                                            .tr(args: ["1"])
                                        : R.string.number_comment.tr(
                                            args: [
                                              "${_cubit.post?.commentNumber}"
                                            ],
                                          ),
                                    style:
                                        Theme.of(context).textTheme.subHeading1,
                                  ),
                                )),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.h, vertical: 8.h),
                              itemCount: _cubit.listComment.isEmpty
                                  ? 0
                                  : (_cubit.listComment.length + 1),
                              itemBuilder: (BuildContext context, int index) {
                                if (index < _cubit.listComment.length) {
                                  CommentData data = _cubit.listComment[index];
                                  return buildComment(data, data);
                                } else {
                                  return Visibility(
                                    visible: _cubit.hasNext,
                                    // visible: true,
                                    child: GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: () => _cubit.getComment(
                                          postId: _postId, isLoadMore: true),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            CupertinoIcons.arrow_down_circle,
                                            size: 16.h,
                                            color: R.color.black,
                                          ),
                                          SizedBox(
                                            width: 12.w,
                                          ),
                                          Text(
                                            R.string.load_other_comments.tr(),
                                            style: Theme.of(context)
                                                .textTheme
                                                .labelSmallText,
                                            textAlign: TextAlign.start,
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                            SizedBox(
                              height: 5.h,
                            ),
                          ],
                        ),
                      ),
                    ),
                    buildInputComment(state),
                    SizedBox(
                      height: MediaQuery.of(context).padding.bottom + 5,
                    ),
                  ],
                ),
              ),
              Visibility(
                  visible: state is CommentLoading, child: PendingAction()),
            ],
          );
  }

  Widget _buildIconMenu() {
    return ValueListenableBuilder(
      valueListenable: _isShowMenu,
      builder: (context, dynamic value, child) {
        return Visibility(
          visible: true, //_isShowMenu.value,
          child: IconButton(
              onPressed: () {
                openPostPopup(context);
              },
              icon: Icon(Icons.more_vert_rounded, color: R.color.black)),
        );
      },
    );
  }

  Widget buildInputComment(CommentState state) {
    return KeyboardVisibilityBuilder(
      builder: (context, isKeyBoardVisible) {
        if (isKeyBoardVisible &&
            !_focusNode.hasFocus &&
            state is! CommentLoading) {
          _focusNode.requestFocus();
        }
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.h),
          child: Container(
            constraints: BoxConstraints(maxHeight: 100.h),
            child: TextField(
              focusNode: _focusNode,
              textInputAction: TextInputAction.send,
              controller: _controller,
              cursorColor: R.color.gray,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                // fillColor: R.color.grey50,
                // filled: true,
                prefixIcon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      if (_controller.text.trim().isEmpty) return;
                      if (_cubit.isEditCommit ?? true) {
                        _cubit.updateComment(_cubit.commentEdit?.id ?? 0,
                            _controller.text.trim());
                      } else {
                        _cubit.comment(
                          _postId,
                          _controller.text.trim(),
                        );
                      }
                      _controller.clear();
                      Utils.hideKeyboard(context);
                    },
                    child: Image.asset(
                      R.drawable.ic_send,
                      height: 16.h,
                      color: Utils.isEmpty(_controller.text.trim())
                          ? R.color.black
                          : R.color.orange,
                    ),
                  ),
                ),
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 14, vertical: 8),
                hintText: R.string.comment_here.tr(),
                hintStyle: Theme.of(context)
                    .textTheme
                    .bodyRegular
                    .copyWith(color: R.color.shadesGray),
                disabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1, color: R.color.borderColor),
                ),
                focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: R.color.borderColor)),
                enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: R.color.borderColor)),
                border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(width: 1, color: R.color.borderColor)),
              ),
              style: Theme.of(context).textTheme.bodyRegular,
              onSubmitted: (text) {
                if (text.isEmpty) return;
                if (_cubit.isEditCommit ?? true) {
                  _cubit.updateComment(
                      _cubit.commentEdit?.id ?? 0, _controller.text.trim());
                } else {
                  _cubit.comment(
                    _postId,
                    _controller.text.trim(),
                  );
                }
                _controller.clear();
                Utils.hideKeyboard(context);
              },
              onChanged: (text) {
                if (text.isEmpty) {
                  _cubit.parentComment = null;
                  _cubit.isEditCommit = false;
                  _cubit.commentEdit = null;
                }
                _cubit.refreshComment();
              },
            ),
          ),
        );
      },
    );
  }

  Widget postWidget(PostData? data, CommentState state, BuildContext context) {
    List<String?> listImage = data?.backgroundImages ?? [data?.backgroundImage];
    if (data == null) {
      return SizedBox.shrink();
    }
    return PostWidget(
      reportPost: () {},
      data: data,
      isInPostDetails: true,
      isShowMore: false,
      isShowMenu: false,
      isLoggedIn: _cubit.isLoggedIn,
      viewProfileCallback: () {
        if (data.communityId != null) {
          NavigationUtils.rootNavigatePage(
              context,
              ViewProfilePage(
                communityId: data.communityId,
              )).then((value) {
            // if (value == Const.ACTION_CHANGE_TAB_MEMBERSHIP) {
            //   _mainCubit.selectTab(Const.SCREEN_MEMBERSHIP);
            // }
          });
        }
      },
      viewClubCallback: () {
        if (data.communityV2?.id != null) {
          NavigationUtils.navigatePage(
              context,
              ViewClubPage(
                communityId: data.communityV2?.id ?? 0,
              )).then((value) {});
        }
      },
      posDetailCallback: () {
        // if(_cubit.post?.backgroundImage !=null)
        // NavigationUtils.navigatePage(context,
        //     FullScreenImageWidget(image: listImage));
      },
      likeCallback: () async {
        if (!(state is CommentLikePost)) {
          bool like = data.isLike == true ? false : true;
          await _cubit.reaction(int.parse(data.id!), like);
          setState(() {
            data.isLike = like;
            data.likeNumber = like == true
                ? ((data.likeNumber ?? 0) + 1)
                : ((data.likeNumber ?? 0) - 1);
          });
        }
      },
      showLikeCallback: () {
        NavigationUtils.rootNavigatePage(
            context,
            LikesPage(
                postId: int.parse(data.id ?? "0"),
                likeCount: data.likeNumber ?? 0));
      },
      shareCallback: () {},
      moreCallback: () {},
      commentCallback: () {},
      reportCallback: () {
        // _cubit.listPost.removeAt(index - 1);
        // setState(() {});
      },
      onBookmarkSuccess: (bool bookmark) {},
    );
  }

  void replyUser(CommentData data, CommentData parentComment) {
    _controller.clear();
    _focusNode.requestFocus();
    RichBlock block = RichBlock(
      text: data.fullName ?? '',
      data: data.id?.toString() ?? "",
      style: TextStyle(
        fontSize: 14.h,
        fontWeight: FontWeight.w400,
        color: R.color.primaryColor,
      ),
    );
    _controller.insertBlock(block);
    _cubit.updateReply(parentComment);
  }

  Widget buildComment(CommentData data, CommentData parentData) {
    var _uuId = data.ownerProfile?.userUuid ?? '';
    bool _isShowPopup = ((uuid == _uuId) ||
        (myRoles.contains(GroupMemberRole.ADMIN.name)) ||
        (myRoles.contains(GroupMemberRole.LEADER.name)));
    bool isParent = data.id == parentData.id;
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onLongPress: !_isShowPopup
          ? null
          : () {
              onTapCommentComment(context, !isParent, data, parentData);
            },
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AvatarWidget(
              avatar: data.avatar,
              size: 24.w,
            ),
            SizedBox(width: 8.w),
            Expanded(
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data.fullName ?? "",
                              style: Theme.of(context).textTheme.bodySmallBold,
                            ),
                            buildTextComment(data.text ?? "",
                                parentData.fullName ?? "", isParent)
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 16.h,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                              data.createdDate == null
                                  ? ""
                                  : TimeAgo.timeAgoSinceDate(
                                      DateTime.fromMillisecondsSinceEpoch(
                                              data.createdDate!,
                                              isUtc: true)
                                          .toUtc()),
                              textAlign: TextAlign.end,
                              style: Theme.of(context)
                                  .textTheme
                                  .labelMarco
                                  .copyWith(color: R.color.darkGray)),
                          SizedBox(height: 8.h),
                          GestureDetector(
                            onTap: () {
                              replyUser(data, parentData);
                            },
                            child: Text(
                              R.string.reply.tr(),
                              textAlign: TextAlign.end,
                              style: Theme.of(context).textTheme.labelMiniText,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Container(
                    color: R.color.lightestGray,
                    height: 1.h,
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      physics: NeverScrollableScrollPhysics(),
                      reverse: true,
                      itemCount: Utils.isEmpty(data.replies?.data)
                          ? 0
                          : ((data.replies?.data?.length ?? 0) + 1),
                      itemBuilder: (BuildContext context, int index) {
                        if (index == data.replies?.data?.length) {
                          return Visibility(
                            visible: data.replies?.hasNext == true,
                            // visible: true,
                            child: GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () => _cubit.getReply(data,
                                  nextToken: data.replies!.nextToken),
                              child: Container(
                                margin: EdgeInsets.only(bottom: 15.h),
                                child: Text(
                                  R.string.show_replies.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodySmallText
                                      .copyWith(color: R.color.primaryColor),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                          );
                        } else {
                          CommentData childData = data.replies!.data![index];
                          return buildComment(childData, data);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTextComment(String text, String fullName, bool isParent) {
    if (!text.contains(fullName) ||
        (text.contains(fullName) && text.indexOf(fullName) > 0)) {
      return CustomExpandableText(
        text: text,
        maxLines: 3,
      );
    }
    int start = text.indexOf(fullName);
    int end = start + fullName.trim().length;
    return CustomExpandableText(
      text: text.substring(end, text.length),
      maxLines: 3,
      prefixText: text.substring(start, end),
    );
  }

  void openPostPopup(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => SingleChildScrollView(
                child: DCIPopup(
              child: DetailPostPopup(
                onReportPost: () {
                  showBarModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return SingleChildScrollView(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20.h, vertical: 20.h),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                R.string.article_report.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(fontSize: 16.sp),
                              ),
                              Text(
                                R.string.reason_report.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 11.sp),
                              ),
                              SizedBox(height: 10.h),
                              ListView.separated(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: _cubit.listReportPost.length,
                                itemBuilder: (context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      NavigationUtils.pop(context);
                                      showBarModalBottomSheet(
                                          context: context,
                                          builder: (context) {
                                            return PopupWidget(
                                              title:
                                                  R.string.article_report.tr(),
                                              reasons: R.string
                                                  .post_deletion_confirmation
                                                  .tr(),
                                              onReport: () {
                                                NavigationUtils.pop(context);
                                                if (_cubit.post?.type ==
                                                    ReportReasonType
                                                        .INDIVIDUAL.name) {
                                                  _cubit.report(
                                                      type: ReportReasonType
                                                          .INDIVIDUAL.name,
                                                      postId: _cubit.post?.id,
                                                      reasonId: _cubit
                                                          .listReportPost[index]
                                                          .id);
                                                } else if (_cubit.post?.type ==
                                                    ReportReasonType
                                                        .GROUP.name) {
                                                  _cubit.report(
                                                      type: ReportReasonType
                                                          .GROUP.name,
                                                      postId: _cubit.post?.id,
                                                      reasonId: _cubit
                                                          .listReportPost[index]
                                                          .id);
                                                }
                                              },
                                            );
                                            
                                          });
                                    },
                                    child: Text(
                                      _cubit.listReportPost[index].reason ?? "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bold700
                                          .copyWith(fontSize: 13.sp),
                                    ),
                                  );
                                  // }
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return Divider();
                                },
                              ),
                            ],
                          ),
                        );
                      });
                },
                post: _cubit.post,
                onDelete: () {
                  openPostDeletePopup(context);
                },
                onEdit: () {
                  NavigationUtils.rootNavigatePage(
                      context,
                      PostPage(
                        type: _cubit.post?.metadata?.type ?? PostType.FREETEXT,
                        targetCommunityId: null,
                        postData: _cubit.post,
                      ));
                },
                showEdit: (uuid == _cubit.post?.userId),
                showDelete: ((uuid == _cubit.post?.userId) ||
                    (myRoles.contains(GroupMemberRole.ADMIN.name)) ||
                    (myRoles.contains(GroupMemberRole.LEADER.name))),
                onBookmark: () {
                  bool bookmarked =
                      _cubit.post?.isBookmark == true ? false : true;
                  _cubit.bookmark(
                      int.parse(_cubit.post?.id ?? '0'), bookmarked);
                },
              ),
            )));
  }

  void openPostDeletePopup(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => SingleChildScrollView(child: DCIPopup(
              child: RemoveConfirmPopup(
                onOk: () {
                  _cubit.delete(_postId);
                },
              ),
            )));
  }

  void onTapCommentComment(BuildContext context, bool isReply, CommentData data,
      CommentData parentData) {
    bool isParent = data.id == parentData.id;
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => SingleChildScrollView(
                child: DCIPopup(
              child: CommentTapPopup(
                isReply: isReply,
                onDelete: () {
                  commentDeletePopup(context, data, parentData);
                },
                onEdit: () {
                  if (isParent) {
                    _controller.text = ' ${data.text ?? ''}';
                    _focusNode.requestFocus();
                    _cubit.commentEdit = data;
                    _cubit.isEditCommit = true;
                  } else {
                    replyUser(data, parentData);
                    _controller.text = ' ${data.text ?? ''}';
                    _cubit.commentEdit = data;
                    _cubit.isEditCommit = true;
                  }
                },
                showEdit: (uuid == data.ownerProfile?.userUuid),
              ),
            )));
  }

  void commentDeletePopup(
      BuildContext context, CommentData data, CommentData parentData) {
    bool isParent = data.id == parentData.id;
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => SingleChildScrollView(
                child: DCIPopup(
              child: RemoveConfirmPopup(
                title: isParent
                    ? R.string.delete_comment.tr()
                    : R.string.delete_reply.tr(),
                content: isParent
                    ? R.string.delete_comment_confirm.tr()
                    : R.string.delete_reply_confirm.tr(),
                onOk: () {
                  // _cubit.deleteComment(data.id ?? 0, parentData.id ?? 0,
                  //     _cubit.post?.communityId ?? 0);
                  _cubit.deleteCommentGround(
                      data.id ?? 0, _cubit.post?.communityV2?.id ?? 0);
                },
              ),
            )));
  }
}
