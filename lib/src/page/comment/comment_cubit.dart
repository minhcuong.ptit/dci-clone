import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/report_request.dart';
import 'package:imi/src/data/network/response/comment.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/response/report_reasons_response.dart';
import 'package:imi/src/data/network/response/send_comment_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/comment/comment_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';

import '../../data/network/request/comment_request.dart';
import '../../data/network/request/follow_request.dart';
import '../../data/network/request/reaction_request.dart';
import '../../data/network/response/community_data.dart';
import '../../data/network/response/person_profile.dart';
import '../../utils/enum.dart';

class CommentCubit extends Cubit<CommentState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommentData> listComment = [];
  String? nextToken;
  bool hasNext = false;
  CommentData? parentComment = null;
  CommentData? commentEdit;
  PostData? post;
  bool? isEditCommit = false;

  bool get isLoggedIn => appPreferences.isLoggedIn;
  List<FetchReportReason> listReportPost = [];
  bool isBlockPost = false;
  bool isCheckLimit = false;

  String? get fullName => appPreferences.getString(Const.FULL_NAME);

  String? get uuid => appPreferences.getString(Const.ID);

  String? get avatar => appPreferences.getString(Const.AVATAR);
  int postId;

  CommentCubit(
      {required this.repository,
      required this.graphqlRepository,
      required this.postId})
      : super(InitialCommentState());

  void refreshComment() {
    emit(CommentLoading());
    emit(InitialCommentState());
  }

  void updateReply(CommentData? parentComment) {
    emit(CommentLoading());
    this.parentComment = parentComment;
    emit(InitialCommentState());
  }

  void getData({int? postId, bool isRefresh = false}) async {
    emit(isRefresh ? InitialCommentState() : CommentLoading());
    nextToken = null;
    hasNext = false;
    listComment.clear();
    List<ApiResult<dynamic>> apiResults = await Future.wait([
      graphqlRepository.getComment(
          postId: postId,
          nextToken: nextToken,
          limit: isCheckLimit ? Const.NETWORK_DEFAULT_LIMIT : 3),
      graphqlRepository.getPostDetail(postId: postId),
    ]);
    for (int i = 0; i < apiResults.length; i++) {
      ApiResult apiResult = apiResults[i];
      apiResult.when(success: (dynamic data) async {
        if (data is CommentPage) {
          listComment.addAll(data.data ?? []);
          nextToken = data.nextToken;
          hasNext = data.hasNext ?? false;
        }
        if (data is PostData) {
          post = data;
        }
        if (data == null) {
          emit(PostEmpty());
          return;
        }
        if (i == apiResults.length - 1) emit(GetCommentSuccess());
        getReportPost();
      }, failure: (NetworkExceptions error) async {
        if (error is BadRequest && error.code == ServerError.unavailable_post) {
          emit(BlockedPostSuccess());
          return;
        }
        emit(CommentFailure(""));
      });
    }
  }

  void refreshEdit() {
    emit(CommentLoading());
    getComment(postId: int.parse(post?.id ?? ""));
    emit(InitialCommentState());
  }

  void refreshDelete() {
    emit(CommentLoading());
    getData(postId: int.parse(post?.id ?? ""));
    // getComment(postId: int.parse(post?.id ?? ""));
    emit(InitialCommentState());
  }

  void getComment(
      {int? postId,
      int? parentId,
      bool isRefresh = false,
      bool isLoadMore = false}) async {
    emit(isRefresh ? InitialCommentState() : CommentLoading());
    if (isLoadMore != true) {
      nextToken = null;
      hasNext = false;
      listComment.clear();
    }
    ApiResult<CommentPage> listCommentTask = await graphqlRepository.getComment(
        postId: postId, parentId: parentId, nextToken: nextToken, limit: 3);
    listCommentTask.when(success: (CommentPage data) async {
      listComment.addAll(data.data ?? []);
      nextToken = data.nextToken;
      hasNext = data.hasNext ?? false;
      emit(GetCommentSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getReply(CommentData commentData, {String? nextToken}) async {
    emit(CommentLoading());
    isCheckLimit = true;
    ApiResult<CommentPage> listCommentTask = await graphqlRepository.getComment(
        postId: postId,
        parentId: commentData.id,
        nextToken: nextToken,
        limit: isCheckLimit ? Const.NETWORK_DEFAULT_LIMIT : 3);
    listCommentTask.when(success: (CommentPage data) async {
      try {
        CommentData firstComment =
            listComment.firstWhere((element) => element.id == commentData.id);
        firstComment.replies?.nextToken = data.nextToken;
        firstComment.replies?.hasNext = data.hasNext;
        firstComment.replies?.data?.addAll(data.data ?? []);
      } catch (e) {
        logger.e(e);
      }
      emit(GetCommentSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void comment(int postId, String text) async {
    emit(CommentLoading());
    ApiResult<SendCommentResponse> registerResult =
        await repository.comment(postId, text, parentId: parentComment?.id);
    registerResult.when(success: (SendCommentResponse data) {
      CommentData newComment = CommentData(
          id: data.id,
          text: text,
          fullName: fullName,
          avatar: avatar,
          createdDate: DateTime.now().millisecondsSinceEpoch,
          ownerProfile: PersonProfile(
              status: PersonProfileStatus.REGISTERED, userUuid: uuid));
      if (parentComment?.id == null) {
        listComment.insert(0, newComment);
      } else {
        if (parentComment?.replies == null) {
          parentComment?.replies = CommentPage(data: []);
        }
        if (parentComment?.replies?.data == null) {
          parentComment?.replies?.data = [];
        }
        parentComment?.replies?.data?.insert(0, newComment);
      }
      parentComment = null;
      emit(CommentSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          emit(PostEmpty());
        }
        return;
      }
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void updateComment(int commentId, String text) async {
    emit(CommentLoading());
    var request = CommentRequest(
        postId: commentId, text: text, parentId: parentComment?.id);
    ApiResult<dynamic> result = await repository.updateComment(request);
    result.when(success: (dynamic data) {
      isEditCommit = false;
      if (parentComment?.id == null) {
        listComment.forEach((element) {
          if (element.id == commentId) {
            element.text = text;
          }
        });
      } else {
        if (parentComment?.replies == null) {
          parentComment?.replies = CommentPage(data: []);
        }
        if (parentComment?.replies?.data == null) {
          parentComment?.replies?.data = [];
        }
        parentComment?.replies?.data?.forEach((element) {
          if (element.id == commentId) {
            element.text = text;
          }
        });
      }
      parentComment = null;
      emit(EditCommentSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          emit(PostEmpty());
        }
        return;
      }
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future reaction(int postId, bool like) async {
    emit(CommentLikePost());
    ApiResult<dynamic> result = await repository.reaction(ReactionRequest(
        postId: postId, action: like ? Const.LIKE : Const.DISLIKE));
    result.when(success: (dynamic response) async {
      emit(CommentReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          emit(PostEmpty());
        }
        return;
      }
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void bookmark(int postId, bool like) async {
    emit(CommentBookmarkPost());
    ApiResult<dynamic> result = await repository.bookmark(ReactionRequest(
        postId: postId, action: like ? Const.BOOKMARK : Const.REMOVE));
    result.when(success: (dynamic response) async {
      emit(CommentBookmarkSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          emit(PostEmpty());
        }
        return;
      }
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void delete(int postId) async {
    emit(CommentLoading());
    ApiResult<dynamic> result = await repository.removePost(postId);
    result.when(success: (dynamic response) async {
      emit(RemovePostSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          emit(PostEmpty());
        }
        return;
      }
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void deleteComment(int commentId, int parentId, int communityId) async {
    emit(CommentLoading());
    ApiResult<dynamic> result =
        await repository.removeComment(commentId, communityId);
    result.when(success: (dynamic response) async {
      bool isParent = commentId == parentId;
      if (isParent) {
        listComment.removeWhere((item) => item.id == commentId);
        post?.commentNumber = listComment.length;
        listComment.forEach((element) {
          post?.commentNumber =
              (post?.commentNumber ?? 0) + (element.replies?.data ?? []).length;
        });
      } else {
        listComment.forEach((element) {
          if (element.id == parentId) {
            element.replies?.data?.removeWhere((item) => item.id == commentId);
          }
        });
        post?.commentNumber =
            (post?.commentNumber ?? 0) > 0 ? (post?.commentNumber ?? 0) - 1 : 0;
      }

      emit(RemoveCommentSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          emit(PostEmpty());
        }
        return;
      } else {
        emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
      }
    });
  }

  void deleteCommentGround(int commentId, int communityId) async {
    emit(CommentLoading());
    ApiResult<dynamic> result =
        await repository.removeComment(commentId, communityId);
    result.when(success: (dynamic response) async {
      emit(DeleteCommentGroundSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void follow(CommunityData? communityData) async {
    emit(CommentLoading());
    bool isFollowing = communityData?.userView?.isFollowing == true;
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityData?.id,
        action: isFollowing ? Const.UNFOLLOW : Const.FOLLOW));
    result.when(success: (dynamic response) async {
      emit(CommentReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getReportPost() async {
    emit(CommentLoading());
    ApiResult<ReportReasonsData> reportPost = await graphqlRepository
        .getReportReasons(reasonType: ReportReasonType.POST.name);
    reportPost.when(success: (ReportReasonsData data) {
      listReportPost = data.fetchReportReasons!;
      emit(GetReportSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void report({int? reasonId, String? postId, String? type}) async {
    emit(CommentLoading());
    ApiResult<dynamic> result = await repository.reportPost(
        ReportRequest(reasonId: reasonId, postType: type),
        postId: postId);
    result.when(success: (dynamic data) {
      emit(GetReportGroupSuccess());
    }, failure: (NetworkExceptions error) async {
      // ReportReasonType.INDIVIDUAL.name
      emit(CommentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
