abstract class DetailOrderState {}

class DetailOrderInitial extends DetailOrderState {}

class DetailOrderLoading extends DetailOrderState {
  @override
  String toString() {
    return 'DetailOrderLoading{}';
  }
}

class DetailOrderFailure extends DetailOrderState {
  final String error;

  DetailOrderFailure(this.error);

  @override
  String toString() {
    return 'DetailOrderFailure{error: $error}';
  }
}

class PaymentOrderFailure extends DetailOrderState {
  final String error;

  PaymentOrderFailure(this.error);

  @override
  String toString() {
    return 'PaymentOrderFailure{error: $error}';
  }
}

class DetailOrderSuccess extends DetailOrderState {
  @override
  String toString() {
    return 'DetailOrderSuccess{}';
  }
}

class DetailOrderLoadedSuccess extends DetailOrderState {
  @override
  String toString() {
    return 'DetailOrderLoadedSuccess{}';
  }
}

class StatusPaid extends DetailOrderState {
  @override
  String toString() {
    return 'StatusPaid{}';
  }
}

class StatusOrder extends DetailOrderState {
  @override
  String toString() {
    return 'StatusOrder{}';
  }
}

class CannotCancelOrderError extends DetailOrderState {}

class GetPointRecieveSuccess extends DetailOrderState {
  @override
  String toString() {
    return 'GetPointRecieveSuccess{}';
  }
}

class PaymentOnlineSuccess extends DetailOrderState {
  @override
  String toString() {
    return 'PaymentOnlineSuccess {}';
  }
}

class ValidateError extends DetailOrderState {
  @override
  String toString() {
    return 'ValidateError {}';
  }
}

class PaymentMinError extends DetailOrderState {
  @override
  String toString() {
    return 'PaymentMinError {}';
  }
}

class EmailValidatePayment extends DetailOrderState {
  @override
  String toString() {
    return 'EmailValidatePayment {}';
  }
}
