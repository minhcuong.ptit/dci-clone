import 'dart:math';
import 'dart:developer' as dev;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/detail_event/widget/infor_event_order.dart';
import 'package:imi/src/page/detail_order/detail_order.dart';
import 'package:imi/src/page/detail_order/detail_order_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/payment_web_view_widget.dart';
import 'package:imi/src/widgets/receive_point_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../data/network/response/order_information_response.dart';
import '../../data/preferences/app_preferences.dart';
import '../../widgets/text_field_widget.dart';
import '../news_details/news_details_page.dart';
import '../webview_page/webview_page.dart';

class DetailOrderPage extends StatefulWidget {
  final int orderHistoryId;
  bool? isShowPopUp;
  String? typePoint;
  int? amountPoint;
  String? paymentType;

  DetailOrderPage(
      {required this.orderHistoryId,
      this.isShowPopUp,
      this.typePoint,
      this.amountPoint,
      this.paymentType});

  @override
  State<DetailOrderPage> createState() => _DetailOrderPageState();
}

class _DetailOrderPageState extends State<DetailOrderPage> {
  late DetailOrderCubit _cubit;
  final RefreshController _refreshController = RefreshController();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _countryController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _addressController = TextEditingController();

  FocusNode _countryNode = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();
  FocusNode _addressFocus = FocusNode();
  FocusNode _cityFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = DetailOrderCubit(repository, graphqlRepository);
    _cubit.init(widget.orderHistoryId);
    _cubit.getPayment();
    // WidgetsBinding.instance
    //     .addPostFrameCallback((_) =>_cubit.showPopup?_showPopUp():null);
    _nameController.text = _cubit.name ?? "";
    _phoneController.text = _cubit.phone ?? "";
    _emailController.text = _cubit.email ?? "";
    _countryController.text = _cubit.country ?? "";
    _cityController.text = _cubit.province ?? "";
    _addressController.text = _cubit.address ?? "";
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _countryNode.dispose();
    _cityFocus.dispose();
    _nameFocus.dispose();
    _emailFocus.dispose();
    _phoneFocus.dispose();
    _addressFocus.dispose();
    _nameController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _countryController.dispose();
    _cityController.dispose();
    _addressController.dispose();
    _cubit.close();
    super.dispose();
  }

  _showPopUp() async {
    await Future.delayed(Duration(milliseconds: 50));
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return GestureDetector(
            onTap: () {
              NavigationUtils.pop(context);
            },
            child: new ReceivePointWidget(
              point: _cubit.pointReceived,
              typePoint: _cubit.typeReceivePoint,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.detail_order.tr(), centerTitle: true,
          onPop: () {
        NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
      }),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<DetailOrderCubit, DetailOrderState>(
          listener: (context, state) {
            if (state is! DetailOrderLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
            if (state is DetailOrderLoadedSuccess &&
                _cubit.orderHistory?.status == OrderStatus.PAID.name &&
                _cubit.showPopup &&
                _cubit.orderHistory?.isSeen == false) {
              _showPopUp();
            }
            if (state is DetailOrderFailure) {
              Utils.showSnackBar(context, state.error);
            }
            if (state is StatusPaid) {
              showBarModalBottomSheet(
                  context: context,
                  builder: (_) {
                    return SafeArea(
                      child: IntrinsicHeight(
                          child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 40.h,
                            ),
                            Text(R.string.information.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 16.sp, height: 24.h / 16.sp)),
                            SizedBox(
                              height: 8.h,
                            ),
                            Text(R.string.you_cannot_cancel_this_order.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 11.sp, height: 20.h / 16.sp)),
                            SizedBox(height: 16.h),
                            ButtonWidget(
                                title: R.string.understanded.tr(),
                                backgroundColor: R.color.white,
                                borderColor: R.color.primaryColor,
                                padding: EdgeInsets.symmetric(vertical: 16.h),
                                textStyle: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        color: R.color.primaryColor,
                                        fontSize: 14.sp,
                                        height: 24.h / 14.sp),
                                onPressed: () {
                                  NavigationUtils.pop(context);
                                  _cubit.init(widget.orderHistoryId);
                                }),
                            SizedBox(height: 20.h),
                          ],
                        ),
                      )),
                    );
                  });
            }
            if (state is StatusOrder) {
              _showCancelBottomSheet();
            }
            if (state is PaymentOnlineSuccess) {
              NavigationUtils.pop(context);
              NavigationUtils.navigatePage(
                      context,
                      WebViewPayment(
                        _cubit.paymentOnline?.checkoutUrl ?? "",
                      ))
                  .then((value) => _cubit.refreshDetail(widget.orderHistoryId));
            }
            if (state is PaymentMinError) {
              Utils.showToast(context, _cubit.errorPayment);
              NavigationUtils.pop(context);
              return;
            }
            if (state is EmailValidatePayment) {
              Utils.showToast(context, R.string.buyer_email_malformed.tr());
              NavigationUtils.pop(context);
              return;
            }
            if (state is PaymentOrderFailure) {
              Utils.showToast(context, R.string.order_payment_failed.tr());
              NavigationUtils.pop(context);
              return;
            }
            // TODO: implement listener
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, DetailOrderState state) {
    return StackLoadingView(
      visibleLoading: state is DetailOrderLoading,
      child: SmartRefresher(
        controller: _refreshController,
        onRefresh: () {
          _cubit.getDetailOrderCourse(widget.orderHistoryId, isRefresh: true);
          _nameController.text = _cubit.name ?? "";
          _phoneController.text = _cubit.phone ?? "";
          _emailController.text = _cubit.email ?? "";
          _countryController.text = _cubit.country ?? "";
          _cityController.text = _cubit.province ?? "";
          _addressController.text = _cubit.address ?? "";
          _cubit.validateAddress(_addressController.text);
          _cubit.validateName(_nameController.text);
          _cubit.validatePhone(_phoneController.text);
          _cubit.validateEmail(_emailController.text);
          _cubit.validateCountry(_countryController.text);
          _cubit.validateCity(_cityController.text);
        },
        onLoading: () => _cubit.getDetailOrderCourse(widget.orderHistoryId,
            isLoadMore: true),
        child: _cubit.lastAudit != null
            ? ListView(
                children: [
                  buildRichText(
                    title: R.string.order_code.tr(),
                    description: _cubit.orderHistory?.code,
                    color: R.color.black,
                  ),
                  buildRichText(
                    title: R.string.status_order.tr(),
                    description: _cubit.lastAudit?.status ==
                            OrderStatus.WAITING_PAYMENT.name
                        ? R.string.wait_for_pay.tr().toUpperCase()
                        : _cubit.lastAudit?.status == OrderStatus.PAID.name
                            ? R.string.paid.tr().toUpperCase()
                            : _cubit.lastAudit?.status ==
                                    OrderStatus.CANCELED.name
                                ? R.string.canceled.tr().toUpperCase()
                                : "",
                    color: _cubit.lastAudit?.status ==
                            OrderStatus.WAITING_PAYMENT.name
                        ? R.color.blue
                        : _cubit.lastAudit?.status == OrderStatus.PAID.name
                            ? R.color.green
                            : R.color.orange,
                  ),
                  buildRichText(
                      title: R.string.order_day.tr(),
                      description: DateUtil.parseDateToString(
                          DateTime.fromMillisecondsSinceEpoch(
                              _cubit.orderHistory?.createdDate ?? 0,
                              isUtc: false),
                          "HH:mm dd/MM/yyyy"),
                      fontWeight: FontWeight.w400,
                      height: 20.h / 14.sp),
                  Visibility(
                    visible: _cubit.lastAudit?.status == OrderStatus.PAID.name,
                    child: buildRichText(
                        title: R.string.pay_day.tr(),
                        description: DateUtil.parseDateToString(
                            DateTime.fromMillisecondsSinceEpoch(
                                _cubit.lastAudit?.createdDate ?? 0,
                                isUtc: true),
                            "HH:mm dd/MM/yyyy"),
                        fontWeight: FontWeight.w400,
                        height: 20.h / 14.sp),
                  ),
                  Visibility(
                    visible:
                        _cubit.lastAudit?.status == OrderStatus.CANCELED.name,
                    child: buildRichText(
                        title: R.string.cancel_by.tr(),
                        description: _cubit.lastAudit?.createdBySide ==
                                AuditSide.ADMIN_DASHBOARD.name
                            ? "DCI"
                            : R.string.client.tr(),
                        fontWeight: FontWeight.w400,
                        height: 20.h / 14.sp,
                        color: R.color.orange),
                  ),
                  Visibility(
                    visible:
                        _cubit.lastAudit?.status == OrderStatus.CANCELED.name,
                    child: buildRichText(
                        title: R.string.cancel_when.tr(),
                        description: DateUtil.parseDateToString(
                            DateTime.fromMillisecondsSinceEpoch(
                                _cubit.lastAudit?.createdDate ?? 0,
                                isUtc: true),
                            "HH:mm dd/MM/yyyy"),
                        fontWeight: FontWeight.w400,
                        height: 20.h / 14.sp),
                  ),
                  Visibility(
                    visible:
                        _cubit.lastAudit?.status == OrderStatus.CANCELED.name,
                    child: buildRichText(
                        title: R.string.cancel_reason.tr(),
                        description: _cubit.lastAudit?.cancelReason,
                        fontWeight: FontWeight.w400,
                        height: 20.h / 14.sp,
                        maxLine: 5),
                  ),
                  Visibility(
                    visible:
                        _cubit.lastAudit?.status != OrderStatus.CANCELED.name,
                    child: buildRichText(
                        title: R.string.payments.tr(),
                        maxLine: 2,
                        description:
                            "${PaymentType.values.firstWhereOrNull((element) => element.name == widget.paymentType)?.title()} ${R.string.by_alepy.tr()}",
                        fontWeight: FontWeight.w400,
                        height: 20.h / 14.sp),
                  ),
                  SizedBox(height: 15.h),
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 20.w),
                      height: 1,
                      color: R.color.lightestGray),
                  SizedBox(height: 15.h),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: _cubit.orderHistory?.orderItems?.length ?? 0,
                    separatorBuilder: (BuildContext context, int index) =>
                        Container(
                      margin: EdgeInsets.symmetric(vertical: 5.h),
                      height: 4.h,
                      color: R.color.lightestGray,
                    ),
                    itemBuilder: (context, int index) {
                      OrderItems? data =
                          _cubit.orderHistory?.orderItems?[index];

                      return Container(
                        color: R.color.white,
                        margin: EdgeInsets.only(bottom: 5.h),
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data?.name ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 14.sp,
                                      color: R.color.dark_blue),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            SizedBox(height: 5.h),
                            Row(
                              children: [
                                ClipRRect(
                                  child: CachedNetworkImage(
                                    fit: BoxFit.cover,
                                    imageUrl: data?.imageUrl ?? '',
                                    height: 86.h,
                                    width: 117.w,
                                  ),
                                  borderRadius: BorderRadius.circular(12.h),
                                ),
                                SizedBox(width: 5.h),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      buildTextWidget(context,
                                          title: R.string.price.tr(),
                                          fontSize: 11.sp,
                                          color: R.color.grey),
                                      Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 5.h),
                                        child: buildTextWidget(context,
                                            title: R.string.amount.tr() + ": ",
                                            fontSize: 11.sp,
                                            color: R.color.grey),
                                      ),
                                      buildTextWidget(context,
                                          title: R.string.discount_total.tr(),
                                          color: R.color.grey,
                                          fontSize: 11.sp),
                                    ],
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    buildTextWidget(context,
                                        title:
                                            "${Utils.formatMoney(data?.price)} VND",
                                        color: R.color.orange,
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500),
                                    buildTextWidget(context,
                                        title: "${data?.quantity}",
                                        color: R.color.black,
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500),
                                    buildTextWidget(context,
                                        title:
                                            "${Utils.formatMoney(data?.totalDiscountPrice)} VND",
                                        color: R.color.orange,
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500),
                                  ],
                                )
                              ],
                            ),
                            Container(
                                margin: EdgeInsets.symmetric(vertical: 8.h),
                                height: 1,
                                color: R.color.lightestGray),
                            Row(
                              children: [
                                SizedBox(width: 117.w),
                                SizedBox(width: 5.w),
                                Expanded(
                                  child: buildTextWidget(context,
                                      title: R.string.provisional.tr() + ":",
                                      fontSize: 12.sp,
                                      color: R.color.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(width: 5.w),
                                buildTextWidget(context,
                                    title:
                                        "${(((data?.price)! * (data?.quantity)!) - data!.totalDiscountPrice!) < 0 ? 0 : Utils.formatMoney(((data.price)! * (data.quantity)!) - data.totalDiscountPrice!)} VND",
                                    color: R.color.orange,
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 5.h),
                    height: 4.h,
                    color: R.color.lightestGray,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          R.string.discount_code.tr(),
                          style: Theme.of(context).textTheme.medium500.copyWith(
                                fontSize: 12.sp,
                                color: R.color.darkGrey,
                              ),
                        ),
                        _cubit.orderHistory?.coupons != null
                            ? ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: _cubit.orderHistory?.coupons?.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Text(
                                    _cubit.orderHistory?.coupons?[index].code ??
                                        "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(
                                            fontSize: 13.sp,
                                            color: R.color.secondaryButtonColor,
                                            height: 24.h / 13.sp),
                                  );
                                },
                              )
                            : SizedBox(),
                        Text(
                          R.string.detail_payment.tr(),
                          style: Theme.of(context).textTheme.medium500.copyWith(
                                fontSize: 12.sp,
                                color: R.color.darkGrey,
                              ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTextWidget(context,
                                title: "${R.string.provisional.tr()}:",
                                fontSize: 13.sp,
                                color: R.color.grey),
                            buildTextWidget(context,
                                title:
                                    "${Utils.formatMoney((_cubit.orderHistory?.totalPrice))} VND",
                                color: R.color.primaryColor,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400),
                          ],
                        ),
                        Visibility(
                          visible: _cubit.orderHistory?.totalSingleCouponDiscount!=0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              buildTextWidget(context,
                                  title: "${R.string.total_single_discount.tr()}:",
                                  fontSize: 13.sp,
                                  color: R.color.grey),
                              buildTextWidget(context,
                                  title:
                                      "${Utils.formatMoney(_cubit.orderHistory?.totalSingleCouponDiscount)} VND",
                                  color: R.color.primaryColor,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400),
                            ],
                          ),
                        ),
                        Visibility(
                          visible:
                              _cubit.orderHistory?.totalComboCouponDiscount !=
                                  0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              buildTextWidget(context,
                                  title: "${R.string.total_combo_discount.tr()}:",
                                  fontSize: 13.sp,
                                  color: R.color.grey),
                              buildTextWidget(context,
                                  title:
                                      "${Utils.formatMoney(_cubit.orderHistory?.totalComboCouponDiscount)} VND",
                                  color: R.color.primaryColor,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400),
                            ],
                          ),
                        ),
                        _cubit.orderHistory?.usePoint == true
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  buildTextWidget(context,
                                      title: "${R.string.used_dci_score.tr()}:",
                                      fontSize: 13.sp,
                                      color: R.color.grey),
                                  buildTextWidget(context,
                                      title:
                                          "${Utils.formatMoney(_cubit.orderHistory?.usedPointAmount)} VND",
                                      color: R.color.primaryColor,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400),
                                ],
                              )
                            : const SizedBox(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTextWidget(context,
                                title: "${R.string.total_payment.tr()}:",
                                fontSize: 16.sp,
                                color: R.color.black,
                                fontWeight: FontWeight.w700),
                            buildTextWidget(context,
                                title:
                                    "${(Utils.formatMoney(_cubit.orderHistory?.finalPrice))} VND",
                                color: R.color.tertiaryOrange,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w700),
                          ],
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: _cubit.lastAudit?.status ==
                        OrderStatus.WAITING_PAYMENT.name,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.w, vertical: 10.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (widget.paymentType == PaymentType.SUPPORT) ...[
                            buildTextSuggestion(
                                R.string.you_are_choosing_form.tr(),
                                R.string.dci_will_contact_to_support_payment
                                    .tr(),
                                2),
                            SizedBox(height: 5.h),
                            InkWell(
                              highlightColor: R.color.white,
                              onTap: () {
                                if (_cubit.hideButtonPayment == false) {
                                  _callNumber();
                                }
                              },
                              child: buildTextSuggestion(
                                  _cubit.hideButtonPayment == true &&
                                          state is! DetailOrderLoading
                                      ? R.string
                                          .dci_will_contact_and_support_you_as_soon_as_possible
                                          .tr()
                                      : R.string.contact_dci_hotline.tr(),
                                  _cubit.hideButtonPayment == true &&
                                          state is! DetailOrderLoading
                                      ? R.string.direct_payment.tr()
                                      : " 0946.098.468",
                                  3,
                                  color: _cubit.hideButtonPayment == true &&
                                          state is! DetailOrderLoading
                                      ? false
                                      : true),
                            ),
                          ],
                          SizedBox(height: 15.h),
                          Visibility(
                            visible: _cubit.hideButtonPayment == true,
                            child: ButtonWidget(
                              title: R.string.direct_payment.tr(),
                              uppercaseTitle: true,
                              onPressed: () {
                                showMaterialModalBottomSheet(
                                  context: context,
                                  backgroundColor: Colors.transparent,
                                  builder: (context) => BlocProvider.value(
                                    child: buildAddToCart(context, state),
                                    value: _cubit,
                                  ),
                                );
                              },
                              backgroundColor: R.color.primaryColor,
                              textSize: 12.sp,
                              padding: EdgeInsets.symmetric(vertical: 9.h),
                            ),
                          ),
                          SizedBox(height: 15.h),
                          if (_cubit.cancelOrderReasons.isNotEmpty)
                            ButtonWidget(
                              title: R.string.cancel_order.tr(),
                              uppercaseTitle: true,
                              onPressed: () {
                                _cubit.getStatusOrder(widget.orderHistoryId);
                              },
                              backgroundColor: R.color.tertiaryOrange,
                              textSize: 12.sp,
                              padding: EdgeInsets.symmetric(vertical: 9.h),
                            ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : SizedBox(),
      ),
    );
  }

  Widget buildTextSuggestion(String? textSmall, String? textBool, int? maxLine,
      {bool? color}) {
    return RichText(
      maxLines: maxLine,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        text: textSmall,
        style: Theme.of(context).textTheme.regular400.copyWith(
              fontSize: 12.sp,
              height: 18.h / 12.sp,
            ),
        children: <TextSpan>[
          TextSpan(
            text: textBool,
            style: Theme.of(context).textTheme.bold700.copyWith(
                fontWeight: FontWeight.w700,
                fontSize: 12.sp,
                height: 18.h / 12.sp,
                color: color == true ? R.color.primaryColor : R.color.black),
          ),
        ],
      ),
    );
  }

  Widget buildTextWidget(BuildContext context,
      {String? title, double? fontSize, Color? color, FontWeight? fontWeight}) {
    return Text(
      title ?? "",
      style: Theme.of(context)
          .textTheme
          .regular400
          .copyWith(fontSize: fontSize, color: color, fontWeight: fontWeight),
    );
  }

  Widget buildRichText(
      {String? title,
      String? description,
      FontWeight? fontWeight,
      Color? color,
      double? height,
      int? maxLine}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: RichText(
        maxLines: maxLine ?? 1,
        overflow: TextOverflow.ellipsis,
        text: TextSpan(
          text: title,
          style: Theme.of(context).textTheme.regular400.copyWith(
                fontSize: 14.sp,
                height: 20.h / 14.sp,
              ),
          children: <TextSpan>[
            TextSpan(
              text: description,
              style: Theme.of(context).textTheme.bold700.copyWith(
                  fontWeight: fontWeight ?? FontWeight.w700,
                  fontSize: 14.sp,
                  height: height ?? 24.h / 14.sp,
                  color: color ?? R.color.secondaryButtonColor),
            ),
          ],
        ),
      ),
    );
  }

  void _showCancelBottomSheet() {
    int chosenReasonId = -1;
    showBarModalBottomSheet(
        context: context,
        builder: (_) {
          return StatefulBuilder(
            builder: (context, nestedSetState) {
              return SafeArea(
                bottom: true,
                child: IntrinsicHeight(
                  child: Column(
                    children: [
                      SizedBox(height: 40),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24.w),
                        child: Row(
                          children: [
                            Text(
                              R.string.cancellation_reason.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .medium500
                                  .copyWith(
                                      fontSize: 16.sp, height: 24.h / 16.sp),
                            ),
                            const Spacer(),
                            GestureDetector(
                              onTap: () {
                                NavigationUtils.pop(context);
                              },
                              child: Image.asset(
                                R.drawable.ic_cancel,
                                height: 24.h,
                                width: 24.h,
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 12.h, horizontal: 24.w),
                        child: Row(
                          children: [
                            Image.asset(
                              R.drawable.ic_alert_infor,
                              height: 20.h,
                              width: 20.h,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Expanded(
                              child: Text(
                                R.string.order_cancellation_warning.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 11.sp,
                                        height: 20.h / 11.sp,
                                        color: R.color.tertiaryOrange),
                              ),
                            )
                          ],
                        ),
                      ),
                      ..._cubit.cancelOrderReasons.map((e) => GestureDetector(
                            onTap: () {
                              nestedSetState(() {
                                if (chosenReasonId != e.id)
                                  chosenReasonId = e.id;
                                else
                                  chosenReasonId = -1;
                              });
                            },
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: 24.w, right: 24.w, top: 12.h),
                              child: Row(
                                children: [
                                  Container(
                                    width: 13.33.h,
                                    height: 13.33.h,
                                    margin: EdgeInsets.all(3.33.h),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        border: Border.all(
                                            color: R.color.primaryBlack,
                                            width: 1.h)),
                                    child: AnimatedContainer(
                                      duration:
                                          const Duration(milliseconds: 200),
                                      margin: EdgeInsets.all(2.5.h),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: chosenReasonId == e.id
                                            ? R.color.primaryColor
                                            : R.color.white,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8.w,
                                  ),
                                  Expanded(
                                    child: Text(
                                      e.reason,
                                      style: Theme.of(context)
                                          .textTheme
                                          .regular400
                                          .copyWith(
                                              fontSize: 11.sp,
                                              height: 20.h / 11.sp,
                                              color: R.color.primaryBlack),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )),
                      SizedBox(
                        height: 60.h,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 24.w, vertical: 20),
                        child: ButtonWidget(
                          title: R.string.confirm.tr(),
                          textStyle: Theme.of(context)
                              .textTheme
                              .bold700
                              .copyWith(
                                  fontSize: 14.sp,
                                  height: 24 / 14.sp,
                                  color: R.color.white),
                          padding: EdgeInsets.symmetric(vertical: 10.h),
                          onPressed: () {
                            if (chosenReasonId >= 0) {
                              _cubit.cancelOrder(
                                  orderHistoryId: widget.orderHistoryId,
                                  cancelReasonId: chosenReasonId);
                              NavigationUtils.pop(context);
                            }
                          },
                          backgroundColor: chosenReasonId >= 0
                              ? R.color.brightRed
                              : R.color.darkGray,
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  Widget buildAddToCart(BuildContext context, DetailOrderState state) {
    return BlocBuilder<DetailOrderCubit, DetailOrderState>(
      buildWhen: (previous, next) =>
          next is ValidateError ||
          next is DetailOrderLoadedSuccess ||
          next is DetailOrderLoading,
      builder: (context, state) {
        final _cubit = BlocProvider.of<DetailOrderCubit>(context);
        return SingleChildScrollView(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            padding: EdgeInsets.all(20.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24.h),
                topRight: Radius.circular(24.h),
              ),
              color: R.color.white,
            ),
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        R.string.billing_information.tr().toUpperCase(),
                        style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 16,
                            height: 21 / 14,
                            color: R.color.black),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    SizedBox(height: 10.h),
                    InformationDetailOrder(R.string.first_and_last_name.tr()),
                    SizedBox(height: 2.h),
                    _buildEnterName(context, state),
                    SizedBox(height: 10.h),
                    InformationDetailOrder(R.string.phone_number.tr()),
                    SizedBox(height: 2.h),
                    _buildEnterPhone(context, state),
                    SizedBox(height: 10.h),
                    InformationDetailOrder(R.string.email.tr()),
                    SizedBox(height: 2.h),
                    _buildEnterEmail(context, state),
                    SizedBox(height: 10.h),
                    InformationDetailOrder(R.string.country.tr()),
                    SizedBox(height: 4.h),
                    _buildEnterCountry(context, state),
                    SizedBox(height: 10.h),
                    InformationDetailOrder(R.string.city.tr()),
                    SizedBox(height: 4.h),
                    _buildEnterCity(context, state),
                    SizedBox(height: 10.h),
                    InformationDetailOrder(R.string.address.tr()),
                    SizedBox(height: 4.h),
                    _buildEnterAddress(context, state),
                    SizedBox(height: 50.h),
                    ButtonWidget(
                        backgroundColor: R.color.primaryColor,
                        padding:
                            EdgeInsetsDirectional.fromSTEB(5.w, 4.h, 5.w, 10.h),
                        title: R.string.save_information_and_pay
                            .tr()
                            .toUpperCase(),
                        textStyle: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 12.sp,
                            color: R.color.white,
                            height: 20 / 10),
                        showCircleIndicator: state is DetailOrderLoading,
                        onPressed: state is DetailOrderLoading
                            ? null
                            : () {
                                _cubit.createOrder(
                                    id: widget.orderHistoryId,
                                    paymentType: widget.paymentType,
                                    buyerName: _nameController.text.trim(),
                                    buyerEmail: _emailController.text.trim(),
                                    buyerPhone: _phoneController.text.trim(),
                                    buyerAddress:
                                        _addressController.text.trim(),
                                    buyerCity: _cityController.text.trim(),
                                    buyerCountry:
                                        _countryController.text.trim());
                              }),
                  ],
                ),
                Positioned(
                    top: 0.h,
                    right: 0.w,
                    child: InkWell(
                      onTap: () {
                        NavigationUtils.pop(context);
                      },
                      child: Icon(
                        Icons.close,
                        size: 24.h,
                      ),
                    ))
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildEnterName(BuildContext context, DetailOrderState state) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _nameController,
      inputFormatters: [LengthLimitingTextInputFormatter(100)],
      focusNode: _nameFocus,
      isRequired: true,
      border: 1,
      hintText: '${R.string.full_name.tr()}*',
      errorText: _cubit.errorName,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 10),
      onChanged: _cubit.validateName,
      onSubmitted: state is DetailOrderLoading
          ? null
          : (_) =>
              Utils.navigateNextFocusChange(context, _nameFocus, _emailFocus),
    );
  }

  Widget _buildEnterPhone(BuildContext context, DetailOrderState state) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _phoneController,
      focusNode: _phoneFocus,
      keyboardType: TextInputType.number,
      inputFormatters: [LengthLimitingTextInputFormatter(10)],
      isRequired: true,
      hintText: R.string.text_field_hint.tr(),
      errorText: _cubit.errorPhoneNumber,
      onChanged: _cubit.validatePhone,
      border: 1,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 10),
      onSubmitted: state is DetailOrderLoading
          ? null
          : (_) =>
              Utils.navigateNextFocusChange(context, _phoneFocus, _emailFocus),
    );
  }

  Widget _buildEnterEmail(BuildContext context, DetailOrderState state) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _emailController,
      focusNode: _emailFocus,
      keyboardType: TextInputType.emailAddress,
      isRequired: true,
      border: 1,
      inputFormatters: [LengthLimitingTextInputFormatter(100)],
      hintText: R.string.enter_your_email.tr(),
      errorText: _cubit.errorEmail,
      onChanged: _cubit.validateEmail,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 10),
      onTap: () {},
      onSubmitted: state is DetailOrderLoading
          ? null
          : (_) =>
              Utils.navigateNextFocusChange(context, _emailFocus, _countryNode),
    );
  }

  Widget _buildEnterCountry(BuildContext context, DetailOrderState state) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _countryController,
      focusNode: _countryNode,
      keyboardType: TextInputType.text,
      isRequired: true,
      border: 1,
      inputFormatters: [LengthLimitingTextInputFormatter(100)],
      hintText: R.string.please_enter_country.tr(),
      errorText: _cubit.errorCountry,
      onChanged: _cubit.validateCountry,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 10),
      onTap: () {},
      onSubmitted: state is DetailOrderLoading
          ? null
          : (_) =>
              Utils.navigateNextFocusChange(context, _countryNode, _cityFocus),
    );
  }

  Widget _buildEnterCity(BuildContext context, DetailOrderState state) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _cityController,
      focusNode: _cityFocus,
      keyboardType: TextInputType.text,
      isRequired: true,
      border: 1,
      inputFormatters: [LengthLimitingTextInputFormatter(100)],
      hintText: R.string.please_enter_city.tr(),
      errorText: _cubit.errorCity,
      onChanged: _cubit.validateCity,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 10),
      onTap: () {},
      onSubmitted: state is DetailOrderLoading
          ? null
          : (_) =>
              Utils.navigateNextFocusChange(context, _cityFocus, _addressFocus),
    );
  }

  Widget _buildEnterAddress(BuildContext context, DetailOrderState state) {
    return TextFieldWidget(
      autoFocus: false,
      controller: _addressController,
      focusNode: _addressFocus,
      keyboardType: TextInputType.text,
      isRequired: true,
      border: 1,
      inputFormatters: [LengthLimitingTextInputFormatter(100)],
      hintText: R.string.text_empty.tr(args: ["${R.string.address.tr()}"]),
      errorText: _cubit.errorAddress,
      onChanged: _cubit.validateAddress,
      errorTextStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(color: R.color.red, fontSize: 10),
      onSubmitted: (_) => Utils.hideKeyboard(context),
    );
  }
}

void _callNumber() async {
  String url = "tel://" + "0946098468";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not call 0946098468';
  }
}
