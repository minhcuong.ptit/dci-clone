import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/cancel_order_response.dart';
import 'package:imi/src/data/network/response/detail_order_response.dart';
import 'package:imi/src/data/network/response/order_information_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/detail_order/detail_order_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../res/R.dart';
import '../../data/network/request/payment_request.dart';
import '../../data/network/response/last_audit_response.dart';
import '../../data/network/response/order_history_DTO.dart';
import '../../data/network/response/payment_online_response.dart';
import '../../data/network/response/payment_response.dart';
import '../../data/network/response/point_action.dart';
import '../../data/network/response/status_order_response.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/enum.dart';

class DetailOrderCubit extends Cubit<DetailOrderState> with Validators {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  LastAudit? lastAudit;
  OrderHistoryDTO? orderHistory;
  List<OrderItems> listItem = [];
  List<CancelOrderReason> cancelOrderReasons = [];
  bool? hideButtonPayment = false;
  List<FetchPaymentTypes> listPayment = [];
  int pointReceived = 0;
  bool isContainEvent = false;
  bool isContainCourse = false;
  bool isHasConfigCourse = true;
  bool isHasConfigEvent = true;
  String typeReceivePoint = '';
  bool showPopup = false;
  GetLastAuditByOrderHistoryId? statusOrder;
  int pointReceiveCourse = 0;
  int pointReceiveEvent = 0;
  AlepayResponse? paymentOnline;

  String? get name => appPreferences.getString(Const.FULL_NAME);

  String? get phone => appPreferences.getString(Const.PHONE);

  String? get email => appPreferences.getString(Const.EMAIL);

  String? get province => appPreferences.getString(Const.PROVINCE);

  String? get country => appPreferences.getString(Const.COUNTRY_NAME);

  String? get address => appPreferences.getString(Const.ADDRESS);

  String? errorName;
  String? errorEmail;
  String? errorPhoneNumber;
  String? errorCountry;
  String? errorCity;
  String? errorAddress;
  String? errorPayment;

  DetailOrderCubit(this.repository, this.graphqlRepository)
      : super(DetailOrderInitial());

  void init(int orderHistoryId) {
    getPointActionIn(orderId:orderHistoryId );
    // getDetailOrderCourse(orderHistoryId);
    getListCancelReason();
  }

  void refreshDetail(orderHistoryId){
    emit(DetailOrderLoading());
    getDetailOrderCourse(orderHistoryId);
    emit(DetailOrderInitial());
  }

  void getDetailOrderCourse( orderHistoryId,
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit(!isRefresh && !isLoadMore
        ? DetailOrderLoading()
        : DetailOrderInitial());
    // getPointActionIn();
    ApiResult<FetchOrderHistoryByIdV2> result = await graphqlRepository
        .getDetailOrderCourse(orderHistoryId: orderHistoryId);
    result.when(success: (FetchOrderHistoryByIdV2 data) async {
      lastAudit = data.lastAudit;
      orderHistory = data.orderHistoryDTO;
      listItem = data.orderHistoryDTO?.orderItems ?? [];
      pointReceived = 0;
      for (int i = 0; i < listItem.length; i++) {
        pointReceived = pointReceived +
            (listItem[i].productType == ProductType.EVENT.name
                ? pointReceiveEvent
                : pointReceiveCourse);
        if (listItem[i].productType == ProductType.EVENT.name &&
            isHasConfigEvent) {
          typeReceivePoint = 'EVENT';
          isContainEvent = true;
        }
        if (listItem[i].productType == ProductType.COURSE.name &&
            isHasConfigCourse) {
          typeReceivePoint = 'COURSE';
          isContainCourse = true;
        }
      }
      if (isContainEvent && isContainCourse) {
        typeReceivePoint = 'EVENT_COURSE';
      }
      if (pointReceived != 0) {
        showPopup = true;
      }
      emit(DetailOrderLoadedSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailOrderFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }


  num getExchangeRate() {
    return orderHistory?.pointExchangeRate ?? 0;
  }

  num getUsedPointAmount() {
    return orderHistory?.usedPointAmount ?? 0;
  }



  void getListCancelReason() async {
    final res = await graphqlRepository.getListCancelOrderReason();
    res.when(
        success: (data) {
          cancelOrderReasons = data;
          emit(DetailOrderSuccess());
        },
        failure: (_) {});
  }

  void cancelOrder(
      {required int orderHistoryId, required int cancelReasonId}) async {
    final res = await repository.cancelOrderHistory(
        orderId: orderHistoryId, cancelReasonId: cancelReasonId);
    res.when(success: (_) {
      getDetailOrderCourse(orderHistoryId);
    }, failure: (e) {
      if (e is BadRequest) {
        if (e.code == ServerError.can_not_cancel_order_error) {
          emit(CannotCancelOrderError());
        }
        getDetailOrderCourse(orderHistoryId);
        return;
      }
      emit(DetailOrderFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getPayment() async {
    emit(DetailOrderLoading());
    final res = await graphqlRepository.getPayment();
    res.when(success: (List<FetchPaymentTypes> data) {
      listPayment = data;
      listPayment.forEach((element) {
        if (element.status == CommonStatus.ACTIVE.name) {
          hideButtonPayment = true;
        }
      });
      emit(DetailOrderSuccess());
    }, failure: (e) {
      emit(DetailOrderFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getStatusOrder(int orderHistoryId) async {
    emit(DetailOrderLoading());
    final res = await graphqlRepository.getStatusOrder(orderHistoryId);
    res.when(success: (GetLastAuditByOrderHistoryId data) {
      if (data.status == "PAID") {
        emit(StatusPaid());
      } else {
        emit(StatusOrder());
      }
    }, failure: (e) {
      emit(DetailOrderFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getPointActionIn({
    bool isRefresh = false,
    bool isLoadMore = false,
    required int orderId,
  }) async {
    emit(!isRefresh || !isLoadMore
        ? DetailOrderLoading()
        : DetailOrderInitial());
    if (isLoadMore != true) {
      // nextToken = null;
    }
    ApiResult<PointActionData> getPointActionIn =
        await graphqlRepository.getPointActionIn();
    getPointActionIn.when(success: (PointActionData data) async {
        if (data.fetchPointActionIn?.indexWhere(
                (element) => element?.name == 'Tham gia khóa học') ==
            -1) {
          pointReceiveCourse = 0;
          isHasConfigCourse = false;
        } else {
          pointReceiveCourse = data.fetchPointActionIn
                  ?.where((element) => element?.name == 'Tham gia khóa học')
                  .first
                  ?.point ??
              0;
        }
        if (data.fetchPointActionIn?.indexWhere(
                (element) => element?.name == 'Tham gia sự kiện') ==
            -1) {
          pointReceiveEvent = 0;
          isHasConfigEvent = false;
        } else {
          pointReceiveEvent = data.fetchPointActionIn
                  ?.where((element) => element?.name == 'Tham gia sự kiện')
                  .first
                  ?.point ??
              0;
        }
        getDetailOrderCourse(orderId);
    }, failure: (NetworkExceptions error) async {
      emit(DetailOrderFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void validateEmail(String? email) {
    errorEmail = checkEmail(email?.trim());
    emit(ValidateError());
  }

  void validateName(String? name) {
    errorName = checkName(name?.trim());
    emit(ValidateError());
  }

  void validatePhone(String? phone) {
    if (Utils.isEmpty(phone)) {
      errorPhoneNumber = R.string.please_enter_phone_number.tr();
    } else {
      errorPhoneNumber = checkPhoneNumber(phone);
    }
    emit(ValidateError());
  }

  void validateCountry(String? country) {
    errorCountry =
        checkTextEmpty(country?.trim(), title: R.string.country.tr());
    emit(ValidateError());
  }

  void validateCity(String? city) {
    errorCity = checkTextEmpty(city?.trim(), title: R.string.city.tr());
    emit(ValidateError());
  }

  void validateAddress(String? address) {
    errorAddress =
        checkTextEmpty(address?.trim(), title: R.string.address.tr());
    emit(ValidateError());
  }

  void createOrder({
    int? id,
    String? paymentType,
    String? buyerName,
    String? buyerEmail,
    String? buyerPhone,
    String? buyerAddress,
    String? buyerCity,
    String? buyerCountry,
  }) async {
    errorName = checkName(buyerName);
    errorPhoneNumber = checkPhoneNumber(buyerPhone);
    errorEmail = checkEmail(buyerEmail);
    errorCountry = checkTextEmpty(buyerCountry, title: R.string.country.tr());
    errorCity = checkTextEmpty(buyerCity, title: R.string.city.tr());
    errorAddress = checkTextEmpty(buyerAddress, title: R.string.address.tr());
    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorPhoneNumber)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorCountry)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorCity)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorAddress)) {
      emit(ValidateError());
      return;
    }
    emit(DetailOrderLoading());
    ApiResult<AlepayResponse> apiResult = await repository.paymentOnline(
        PaymentRequest(
            id: id,
            paymentType: paymentType,
            buyerName: buyerName,
            buyerEmail: buyerEmail,
            buyerPhone: buyerPhone,
            buyerAddress: buyerAddress,
            buyerCity: buyerCity,
            buyerCountry: buyerCountry));
    apiResult.when(success: (AlepayResponse response) async {
      paymentOnline = response;
      if (response.code == 126) {
        emit(EmailValidatePayment());
        return;
      }
      emit(PaymentOnlineSuccess());
    }, failure: (NetworkExceptions e) async {
      if (e is BadRequest) {
        if (e.code == ServerError.min_price_payment_error) {
          errorPayment = R.string.min_price_payment_error.tr();
        }
        emit(PaymentMinError());
        return;
      }
      emit(PaymentOrderFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }
}
