import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_cubit.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_state.dart';
import 'package:imi/src/page/detail_document_library/widgets/level_widget.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/webview_pdf.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class DetailDocumentPage extends StatefulWidget {
  final String? link;
  final bool? isLocked;
  final int? id;

  DetailDocumentPage({this.link, this.isLocked, this.id});

  @override
  _DetailDocumentPageState createState() => _DetailDocumentPageState();
}

class _DetailDocumentPageState extends State<DetailDocumentPage> {
  late DetailDocumentCubit _cubit;
  late YoutubePlayerController _controller;
  bool _isFullscreen = false;
  AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
  AppRepository repository = AppRepository();
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: convertUrlToId(widget.link ?? "") ?? "",
      params: YoutubePlayerParams(
        showControls: true,
        showFullscreenButton: true,
      ),
    );
    _cubit = DetailDocumentCubit(repository, graphqlRepository);
    _cubit.getDetailLibrary(widget.id ?? 0);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: R.color.white,
        // appBar: appBar(context, "", backgroundColor: R.color.white),
        body: BlocProvider(
          create: (BuildContext context) => _cubit,
          child: BlocConsumer<DetailDocumentCubit, DetailDocumentState>(
            listener: (BuildContext context, state) {
              if (state is! DetailDocumentLoading) {
                _refreshController.refreshCompleted();
                _refreshController.loadComplete();
              }
            },
            builder: (BuildContext context, DetailDocumentState state) {
              return StackLoadingView(
                visibleLoading: state is DetailDocumentLoading,
                child: SmartRefresher(
                  controller: _refreshController,
                  onRefresh: () =>
                      _cubit.getDetailLibrary(widget.id ?? 0, isRefresh: true),
                  onLoading: () =>
                      _cubit.getDetailLibrary(widget.id ?? 0, isLoadMore: true),
                  child: state is DetailDocumentErrorSuccess
                      ? Padding(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.3),
                          child: Text(
                            R.string.this_content_is_no_longer_active.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(fontSize: 14.sp),
                            textAlign: TextAlign.center,
                          ),
                        )
                      : Visibility(
                          visible: state is DetailDocumentSuccess,
                          child: Column(
                            children: [
                              buildThumbnail(context),
                              buildImage(state),
                              buildPage(context, state),
                            ],
                          ),
                        ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildImage(DetailDocumentState state) {
    return Visibility(
      child: Stack(
        children: [
          Visibility(
            child: Container(
              height: 240,
              width: double.infinity,
              child: CachedNetworkImage(
                imageUrl: _cubit.listDetailDocument?.imageUrl ?? "",
                fit: BoxFit.fill,
                placeholder: (context, url) => Container(
                  height: 240,
                  width: double.infinity,
                  child: Image.asset(R.drawable.ic_default_banner,
                      fit: BoxFit.cover, width: double.infinity),
                ),
                errorWidget: (context, url, error) => Container(
                  height: 240,
                  width: double.infinity,
                  child: Image.asset(R.drawable.ic_default_banner,
                      fit: BoxFit.cover, width: double.infinity),
                ),
              ),
            ),
            visible: state is DetailDocumentSuccess,
          ),
          _cubit.listDetailDocument?.isLocked == true
              ? Positioned(
                  top: 20.h,
                  right: 50.h,
                  child: Container(
                    width: 35.h,
                    height: 35.h,
                    child: Icon(
                      CupertinoIcons.lock,
                      size: 20.h,
                      color: R.color.blue,
                    ),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: R.color.white),
                  ))
              : SizedBox(),
          Positioned(
              top: 20,
              right: 30,
              child: buildButtonClose(() {
                NavigationUtils.pop(context);
              }))
        ],
      ),
      visible: _cubit.listDetailDocument?.type == DocumentType.E_BOOK.name,
    );
  }

  Widget buildButtonClose(VoidCallback callback) {
    return InkWell(
      highlightColor: R.color.white,
      onTap: callback,
      child: Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: R.color.darkGrey.withOpacity(0.8)),
          padding: EdgeInsets.all(4),
          child: Icon(
            Icons.clear,
            size: 14.h,
            color: R.color.white,
          )),
    );
  }

  Widget buildThumbnail(BuildContext context) {
    return Visibility(
      child: Stack(
        children: [
          YoutubePlayerIFrame(
            controller: _controller,
            aspectRatio: 16 / 9,
          ),
          _cubit.listDetailDocument?.isLocked == true
              ? Stack(
                  children: [
                    SizedBox(
                      child: ClipRRect(
                        child: CachedNetworkImage(
                          imageUrl: getYoutubeThumbnail(
                                  _cubit.listDetailDocument?.link ?? "") ??
                              "",
                          placeholder: (context, url) => Container(
                            width: double.infinity,
                            height: MediaQuery.of(context).size.width / 16 * 9,
                            child: Image.asset(
                              R.drawable.ic_default_banner,
                              fit: BoxFit.cover,
                            ),
                          ),
                          errorWidget: (context, url, error) => Container(
                            width: double.infinity,
                            height: MediaQuery.of(context).size.width / 16 * 9,
                            child: Image.asset(
                              R.drawable.ic_default_banner,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        borderRadius: BorderRadius.circular(10.h),
                      ),
                      width: double.infinity,
                      height: MediaQuery.of(context).size.width / 16 * 9,
                    ),
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Icon(
                        CupertinoIcons.play_circle_fill,
                        size: 45.h,
                        color: R.color.white,
                      ),
                    ),
                    Positioned(
                        top: 15.h,
                        right: 15.h,
                        child: Container(
                          width: 28.h,
                          height: 28.h,
                          child: Icon(
                            CupertinoIcons.lock,
                            size: 18.h,
                            color: R.color.blue,
                          ),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: R.color.white),
                        ))
                  ],
                )
              : SizedBox(),
          Positioned(
              top: 60,
              right: 30,
              child: buildButtonClose(() {
                NavigationUtils.pop(context);
              }))
        ],
      ),
      visible: _cubit.listDetailDocument?.type == DocumentType.VIDEO.name,
    );
  }

  Widget buildPage(BuildContext context, DetailDocumentState state) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: R.color.white, borderRadius: BorderRadius.circular(10.h)),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                // shrinkWrap: true,
                children: [
                  _cubit.listDetailDocument?.leaderTypes?.length == 0
                      ? const SizedBox.shrink()
                      : LevelWidget(
                          desc: (_cubit.listDetailDocument?.leaderTypes ??
                                  <LeaderType>[])
                              .map((e) => e.title())
                              .join(", "),
                          title: R.string.level_leader.tr(args: [" "]),
                        ),
                  _cubit.listDetailDocument?.levels?.length == 0
                      ? const SizedBox.shrink()
                      : LevelWidget(
                          desc: _cubit.listDetailDocument?.levels
                                  ?.map((e) => e.name)
                                  .join(", ") ??
                              "",
                          title: R.string.student_level.tr(args: [" "]),
                        ),
                  Text(
                    _cubit.listDetailDocument?.title ?? "",
                    style: Theme.of(context).textTheme.subTitleRegular,
                  ),
                  SizedBox(height: 8.h),
                  Text(
                    _cubit.listDetailDocument?.description ?? "",
                    style: Theme.of(context).textTheme.labelLargeText.copyWith(
                        color: R.color.gray, fontWeight: FontWeight.normal),
                  ),
                  SizedBox(height: 10.h),
                  //buildOptions(),
                  SizedBox(height: 20.h),
                ],
                padding: EdgeInsets.symmetric(horizontal: 25.h, vertical: 10.h),
              ),
            ),
            _cubit.listDetailDocument?.type == DocumentType.E_BOOK.name
                ? Container(
                    child: ButtonWidget(
                      title: R.string.read.tr(),
                      onPressed: _cubit.listDetailDocument?.isLocked == true
                          ? null
                          : () {
                              NavigationUtils.navigatePage(
                                  context,
                                  WebViewPdf(
                                      _cubit.listDetailDocument?.link ?? ""));
                            },
                      padding: EdgeInsets.symmetric(vertical: 10.h),
                      backgroundColor: R.color.primaryColor,
                      textSize: 20,
                      uppercaseTitle: false,
                    ),
                    padding: EdgeInsets.only(
                        left: 24.h,
                        right: 24.h,
                        bottom: MediaQuery.of(context).padding.bottom),
                  )
                : const SizedBox.shrink(),
            SizedBox(
              height: 20.h,
            )
          ],
        ),
      ),
    );
  }

  Widget buildOptions() {
    return Wrap(
      spacing: 6.0,
      runSpacing: 6.0,
      direction: Axis.horizontal,
      children: List.generate(
          _cubit.listDetailDocument?.categories?.length ?? 0,
          (index) => Container(
                decoration: BoxDecoration(
                    border: Border.all(color: R.color.grey),
                    borderRadius: BorderRadius.circular(30.h)),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(7, 5, 4, 8),
                  child: Text(
                    _cubit.listDetailDocument?.categories?[index].name ?? "",
                    style: Theme.of(context)
                        .textTheme
                        .medium500
                        .copyWith(fontSize: 10.sp),
                  ),
                ),
              )),
    );
  }

  RegExp reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
  String Function(Match) mathFunc = (Match match) => '${match[1]},';

  String? getYoutubeThumbnail(String videoUrl) {
    final Uri? uri = Uri.tryParse(videoUrl);
    if (uri == null) {
      return null;
    }

    return 'https://img.youtube.com/vi/${uri.queryParameters['v']}/0.jpg';
  }

  static String? convertUrlToId(String url, {bool trimWhitespaces = true}) {
    if (!url.contains("http") && (url.length == 11)) return url;
    if (trimWhitespaces) url = url.trim();

    for (var exp in [
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(r"^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$")
    ]) {
      Match? match = exp.firstMatch(url);
      if (match != null && match.groupCount >= 1) return match.group(1);
    }
    return null;
  }
}
