import 'package:bloc/bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/detail_library_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_state.dart';
import 'package:imi/src/utils/const.dart';

class DetailDocumentCubit extends Cubit<DetailDocumentState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  FetchDocumentData? listDetailDocument;
  int index = 0;

  DetailDocumentCubit(this.repository, this.graphqlRepository)
      : super(DetailDocumentInitial());

  void getDetailLibrary(int documentId,
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit((!isRefresh && !isLoadMore)
        ? DetailDocumentLoading()
        : DetailDocumentInitial());
    ApiResult<FetchDocumentData> getLibrary =
        await graphqlRepository.getDetailLibrary(documentId: documentId);
    getLibrary.when(success: (FetchDocumentData data) async {
      listDetailDocument = data;
      emit(DetailDocumentSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest &&
          error.code == ServerError.document_is_not_published) {
        emit(DetailDocumentErrorSuccess());
        return;
      }
      emit(DetailDocumentFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
