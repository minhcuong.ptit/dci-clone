import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';

class LevelWidget extends StatelessWidget {
  final String title;
  final String desc;
  const LevelWidget({Key? key,required this.title,required this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      maxLines: 10,
      text: TextSpan(
        text: title,
        style: Theme.of(context)
            .textTheme
            .labelLargeText
            .copyWith(color: R.color.lightBlue, fontStyle: FontStyle.italic),
        children: <TextSpan>[
          TextSpan(
            text: desc,
            style: Theme.of(context).textTheme.labelLargeText.copyWith(
                color: R.color.lightBlue, fontStyle: FontStyle.italic),
          )
        ],
      ),
    );
  }
}
