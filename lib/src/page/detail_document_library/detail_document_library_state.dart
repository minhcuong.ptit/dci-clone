import 'package:equatable/equatable.dart';

abstract class DetailDocumentState extends Equatable {
  @override
  List<Object> get props => [];
}

class DetailDocumentInitial extends DetailDocumentState {}

class DetailDocumentLoading extends DetailDocumentState {
  @override
  String toString() {
    return 'LibraryLoading{}';
  }
}

class DetailDocumentFailure extends DetailDocumentState {
  final String error;

  DetailDocumentFailure(this.error);

  @override
  String toString() {
    return 'DetailDocumentFailure{error: $error}';
  }
}

class DetailDocumentSuccess extends DetailDocumentState {
  @override
  String toString() {
    return 'DetailDocumentSuccess{}';
  }
}

class DetailDocumentErrorSuccess extends DetailDocumentState {
  @override
  String toString() {
    return 'DetailDocumentErrorSuccess{}';
  }
}
