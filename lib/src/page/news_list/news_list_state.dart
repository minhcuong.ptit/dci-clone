abstract class NewsListState {}

class NewsListLoadingState extends NewsListState {}

class NewsListFailureState extends NewsListState {
  final error;
  NewsListFailureState(this.error);
}

class NewsListSuccessState extends NewsListState {}
