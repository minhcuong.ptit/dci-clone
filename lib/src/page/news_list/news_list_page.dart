import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/page/news_details/news_details.dart';
import 'package:imi/src/page/news_list/news_list_cubit.dart';
import 'package:imi/src/page/news_list/news_list_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../utils/const.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/search/search_widget.dart';

class NewsListPage extends StatefulWidget {
  const NewsListPage({Key? key}) : super(key: key);

  @override
  _NewsListPageState createState() => _NewsListPageState();
}

class _NewsListPageState extends State<NewsListPage> {
  late final NewsListCubit _cubit;
  late final RefreshController _refreshController;
  late final TextEditingController _textEditingController;
  late double _appBarHeight;

  @override
  void initState() {
    super.initState();
    _cubit = NewsListCubit(
        appRepository: AppRepository(),
        graphqlRepository: AppGraphqlRepository());
    _refreshController = RefreshController();
    _textEditingController = TextEditingController();
    _appBarHeight = AppBar().preferredSize.height;
  }

  @override
  void dispose() {
    _cubit.close();
    _refreshController.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<NewsListCubit, NewsListState>(
        bloc: _cubit,
        builder: (context, state) {
          return Scaffold(
            backgroundColor: R.color.lightestGray,
            body: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).padding.top,
                  width: double.infinity,
                  color: R.color.primaryColor,
                ),
                Expanded(
                  child: Stack(
                    children: [
                      Positioned(
                        top: _appBarHeight,
                        height: MediaQuery.of(context).size.height -
                            _appBarHeight -
                            MediaQuery.of(context).padding.top,
                        width: MediaQuery.of(context).size.width,
                        child: StackLoadingView(
                            child: Column(
                              children: [
                                _tabBar(),
                                _newsItems(state),
                              ],
                            ),
                            visibleLoading: state is NewsListLoadingState),
                      ),
                      _appBar(),
                    ],
                  ),
                )
              ],
            ),
          );
        },
        listener: (context, state) {
          if (state is NewsListFailureState) {
            Utils.showErrorSnackBar(context, state.error);
          }
        });
  }

  Widget _appBar() {
    return !_cubit.isSearching
        ? Container(
            height: _appBarHeight,
            width: double.infinity,
            decoration: BoxDecoration(color: R.color.primaryColor, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.h), blurRadius: 1.h, color: R.color.gray)
            ]),
            child: Row(
              children: [
                SizedBox(
                  width: 16.w,
                ),
                GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: R.color.white,
                    size: 20.h,
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Text(
                      R.string.news.tr().toUpperCase(),
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 20.sp, color: R.color.white),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _cubit.onSearch();
                  },
                  child: Image.asset(
                    R.drawable.ic_search,
                    width: 20.h,
                    height: 20.h,
                    color: R.color.white,
                  ),
                ),
                SizedBox(
                  width: 16.w,
                ),
              ],
            ),
          )
        : Stack(
            children: [
              Container(
                width: double.infinity,
                height: _appBarHeight,
                decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 2.h),
                          blurRadius: 1.h,
                          color: R.color.gray)
                    ]),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.h, top: 16.h),
                    child: InkWell(
                      onTap: () {
                        NavigationUtils.pop(context);
                      },
                      child: Icon(CupertinoIcons.back, color: R.color.white),
                    ),
                  ),
                  Expanded(
                    child: SearchWidget(
                      onSubmit: (text) {
                        _cubit.getNews(index: _cubit.tabIndex, keyword: text);
                      },
                      searchController: _textEditingController,
                      type: Const.NEWS,
                      onChange: (text) {
                        _cubit.getNews(index: _cubit.tabIndex, keyword: text);
                      },
                    ),
                  ),
                ],
              ),
            ],
          );
  }

  Widget _tabBar() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.h),
      height: 24.h + 20.h * 2,
      width: double.infinity,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        itemCount: _cubit.categories.length + 1,
        separatorBuilder: (context, index) => SizedBox(
          width: 8.h,
        ),
        itemBuilder: (context, index) {
          CategoryData? tab;
          if (index > 0) {
            tab = _cubit.categories[index - 1];
          }
          return InkWell(
            onTap: () {
              _cubit.changeTabIndex(
                  index: index, keyword: _textEditingController.text);
            },
            child: _cubit.categories.isNotEmpty
                ? Container(
                    //height: 24.h,
                    padding: EdgeInsets.fromLTRB(12.w, 2.h, 12.w, 0.h),
                    decoration: BoxDecoration(
                        color: index == _cubit.tabIndex
                            ? R.color.lightBlue
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(24)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          index == 0 ? R.string.all.tr() : tab?.name ?? "",
                          style: Theme.of(context).textTheme.medium500.copyWith(
                              color: index == _cubit.tabIndex
                                  ? R.color.white
                                  : R.color.shadesGray,
                              fontSize: 10.sp,
                              height: 1.0),
                        ),
                        Visibility(
                          visible: index == _cubit.tabIndex,
                          child: Padding(
                            padding: EdgeInsets.only(left: 10.w, bottom: 2.h),
                            child: Image.asset(
                              R.drawable.ic_check_true,
                              color: R.color.white,
                              width: 11.h,
                              height: 11.h,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                : SizedBox(),
          );
        },
      ),
    );
  }

  Widget _newsItems(state) {
    double itemWidth = (MediaQuery.of(context).size.width - 48.w) / 2;
    double imageWidth = itemWidth - 8.w;
    final data = _cubit.getData(_cubit.tabIndex);

    if (_cubit.isSearching && (data ?? []).isEmpty) {
      return Expanded(
        child: Align(
          alignment: Alignment.center,
          child: Text(R.string.no_result.tr()),
        ),
      );
    }

    return Expanded(
      child: SafeArea(
        top: false,
        right: false,
        left: false,
        child: (data?.isEmpty ?? true)
            ? Center(
                child: Visibility(
                    visible: state is! NewsListLoadingState,
                    child: Text(R.string.no_result.tr())),
              )
            : Scrollbar(
                child: SmartRefresher(
                  onRefresh: () {
                    _cubit.getNews(
                        index: _cubit.tabIndex,
                        isRefresh: true,
                        keyword: _textEditingController.text);
                    _refreshController.refreshCompleted();
                  },
                  onLoading: () {
                    _cubit.getNews(
                        index: _cubit.tabIndex,
                        isRefresh: false,
                        keyword: _textEditingController.text);
                    _refreshController.loadComplete();
                  },
                  controller: _refreshController,
                  enablePullUp: (_cubit.getData(_cubit.tabIndex) ?? []).length >= 20,
                  child: GridView.count(
                    crossAxisCount: 2,
                    padding:
                        EdgeInsets.only(left: 20.w, right: 20.w, bottom: 16.h),
                    childAspectRatio:
                        itemWidth / (imageWidth * 3 / 4 + 4.w + 18.h + 51),
                    mainAxisSpacing: 12.h,
                    crossAxisSpacing: 8.w,
                    children: List.generate(data?.length ?? 0, (index) {
                      final item = data![index];
                      return GestureDetector(
                          onTap: () {
                            NavigationUtils.rootNavigatePage(
                                context,
                                NewsDetailsPage(
                                  newsId: item.id,
                                ));
                          },
                          child: Container(
                            padding: EdgeInsets.all(4.w),
                            decoration: BoxDecoration(
                                color: R.color.white,
                                borderRadius: BorderRadius.circular(8.h)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8.h),
                                  child: CachedNetworkImage(
                                    height: imageWidth * 3 / 4,
                                    width: imageWidth,
                                    imageUrl: item.imageUrl ?? "",
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) => Image.asset(
                                      R.drawable.ic_default_item,
                                      fit: BoxFit.cover,
                                      height: imageWidth * 3 / 4,
                                      width: imageWidth,
                                    ),
                                    errorWidget: (context, url, error) => Image.asset(
                                      R.drawable.ic_default_item,
                                      fit: BoxFit.cover,
                                      height: imageWidth * 3 / 4,
                                      width: imageWidth,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 8.h,
                                ),
                                Text(
                                  item.title ?? "",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .medium500
                                      .copyWith(
                                          fontSize: 12,
                                          color: R.color.neutral1,
                                          height: 18 / 12),
                                ),
                                const Spacer(),
                                Text(
                                  DateUtil.parseDateToString(
                                      DateTime.fromMillisecondsSinceEpoch(
                                              item.createdDate ?? 0)
                                          .toLocal(),
                                      Const.DATE_FORMAT),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          color: R.color.secondaryColor,
                                          fontSize: 10,
                                          height: 15 / 10),
                                )
                              ],
                            ),
                          ));
                    }),
                  ),
                ),
              ),
      ),
    );
  }
}
