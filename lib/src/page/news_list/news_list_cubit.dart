import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/news_list/news_list_state.dart';
import 'package:imi/src/utils/enum.dart';

class NewsListCubit extends Cubit<NewsListState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  NewsListCubit({required this.appRepository, required this.graphqlRepository})
      : super(NewsListLoadingState()) {
    init();
  }

  Map<CategoryData, List<NewsData>?> dataByCategory = {};
  Map<CategoryData, String?> nextTokens = {};
  String? nextToken;
  List<NewsData>? allData;
  List<CategoryData> categories = [];

  int tabIndex = 0;

  bool isSearching = false;

  String _lastKeyword = "";

  void getNews(
      {bool isRefresh = false, required int index, String? keyword}) async {
    if (isRefresh) emit(NewsListLoadingState());
    allData ??= [];
    dataByCategory.forEach((key, value) {
      dataByCategory[key] ??= [];
    });
    if (isRefresh || _lastKeyword != (keyword ?? "")) {
      nextToken = null;
      nextTokens.forEach((key, value) {
        nextTokens[key] = null;
      });
      allData = [];
      dataByCategory.forEach((key, value) {
        dataByCategory[key] = [];
      });
    }
    _lastKeyword = keyword ?? "";
    final res = await graphqlRepository.getNews(
        typeCategory: NewsCategory.NEWS,
        typeIds: index == 0 ? null : [categories[index - 1].id ?? 0],
        searchKey: keyword,
        nextToken: getNextoken(index));
    res.when(success: (response) {
      if (isRefresh) {
        setData(index, []);
      }
      setNextToken(index, response.nextToken);
      getData(index)?.addAll(response.data ?? []);
      emit(NewsListSuccessState());
    }, failure: (e) {
      emit(NewsListFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void changeTabIndex({required int index, required String keyword}) {
    tabIndex = index;
    getNews(index: index, isRefresh: true, keyword: keyword);
  }

  void init() async {
    final res = await appRepository.getCategories(CategoryType.NEWS.name);
    res.when(success: (response) {
      categories = response;
      emit(NewsListSuccessState());
      getNews(index: 0, isRefresh: true, keyword: "");
    }, failure: (e) {
      emit(NewsListFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  String? getNextoken(int index) {
    if (index == 0) return nextToken;
    return nextTokens[categories[index - 1]];
  }

  void setNextToken(int index, String? token) {
    if (index == 0)
      nextToken = token;
    else
      nextTokens[categories[index - 1]] = token;
  }

  List<NewsData>? getData(int index) {
    if (index == 0) return allData;
    if (!dataByCategory.containsKey(categories[index - 1])) {
      dataByCategory[categories[index - 1]] = [];
    }
    return dataByCategory[categories[index - 1]];
  }

  void setData(int index, List<NewsData>? data) {
    if (index == 0)
      allData = data;
    else
      dataByCategory[categories[index - 1]] = data;
  }

  void onSearch() {
    isSearching = true;
    emit(NewsListSuccessState());
  }
}
