import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/course_details/widget/describe_page.dart';
import 'package:imi/src/page/detail_tutorial_snow/detail_tutorial_snow_cubit.dart';
import 'package:imi/src/page/detail_tutorial_snow/detail_tutorial_snow_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailTutorialSowPage extends StatefulWidget {
  final int postId;

  DetailTutorialSowPage({required this.postId});

  @override
  State<DetailTutorialSowPage> createState() => _DetailTutorialSowPageState();
}

class _DetailTutorialSowPageState extends State<DetailTutorialSowPage> {
  late DetailTutorialSowCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    super.initState();
    _cubit =
        DetailTutorialSowCubit(widget.postId, repository, graphqlRepository);
    _cubit.getNewsDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<DetailTutorialSowCubit, DetailTutorialSowState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return Scaffold(
              appBar: appBar(context, "",
                  backgroundColor: R.color.white,
                  titleColor: R.color.white,
                  centerTitle: true,
                  iconColor: R.color.black,),
              body: StackLoadingView(
                visibleLoading: state is DetailTutorialSowLoading,
                child: Scrollbar(
                  child: Scrollbar(
                    child: ListView(
                      shrinkWrap: true,
                      padding: EdgeInsets.symmetric(horizontal: 20.h),
                      children: [
                        Text(_cubit.newsDetail?.title ?? "",
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                  fontSize: 16.sp,
                                )),
                        SizedBox(height: 15.h),
                        HtmlWidget(
                          _cubit.newsDetail?.content ?? "",
                          textStyle: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 12.sp),
                          onTapUrl: (url) async {
                            if (await canLaunch(url)) {
                              await launch(
                                url,
                              );
                            } else {
                              throw 'Could not launch $url';
                            }
                            return launch(
                              url,
                            );
                          },
                          factoryBuilder: () => MyWidgetFactory(),
                          enableCaching: true,
                        ),
                        SizedBox(height: 20.h),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
