import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/news_details_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/detail_tutorial_snow/detail_tutorial_snow_state.dart';

class DetailTutorialSowCubit extends Cubit<DetailTutorialSowState> {
  final int postId;
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  NewsDetailsData? newsDetail;

  DetailTutorialSowCubit(this.postId, this.repository, this.graphqlRepository)
      : super(DetailTutorialSowInitial());

  void getNewsDetail({bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || isLoadMore)
        ? DetailTutorialSowInitial()
        : DetailTutorialSowLoading());
    ApiResult<NewsDetailsData> getNewsDetail =
        await graphqlRepository.getNewsDetail(postId: postId);
    getNewsDetail.when(success: (NewsDetailsData data) {
      newsDetail = data;
      emit(DetailTutorialSowSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailTutorialSowFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
