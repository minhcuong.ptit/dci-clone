import 'package:equatable/equatable.dart';

abstract class DetailTutorialSowState extends Equatable {
  @override
  List<Object> get props => [];
}

class DetailTutorialSowInitial extends DetailTutorialSowState {}
class DetailTutorialSowLoading extends DetailTutorialSowState {
  @override
  String toString() {
    return 'DetailTutorialSowLoading{}';
  }
}

class DetailTutorialSowFailure extends DetailTutorialSowState {
  final String error;

  DetailTutorialSowFailure(this.error);

  @override
  String toString() {
    return 'DetailTutorialSowFailure{error: $error}';
  }
}

class DetailTutorialSowSuccess extends DetailTutorialSowState {
  @override
  String toString() {
    return 'DetailTutorialSowSuccess{}';
  }
}