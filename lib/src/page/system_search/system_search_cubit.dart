import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/response/search_communities_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/system_search/system_search.dart';
import 'package:imi/src/page/system_search/system_search_state.dart';
import 'package:imi/src/utils/enum.dart';

class SystemSearchCubit extends Cubit<SystemSearchState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  String? nextTokenNews;
  String? nextTokenCourse;
  String? nextTokenStudyGroup;
  String? nextTokenBasicGroup;
  String? nextTokenUser;
  String? nextTokenLibrary;
  List<NewsData> listNews = [];
  NewsDataFetch? newsDataFetch;
  List<FetchCoursesData> listCourse = [];
  FetchCourse? fetchCourse;
  List<StudyGroupDataSearchCommunitiesData> listStudyGroup = [];
  StudyGroupSearchCommunities? studyGroup;
  StudyGroupSearchCommunities? basicGroup;
  StudyGroupSearchCommunities? user;
  List<StudyGroupDataSearchCommunitiesData> listBasicGroup = [];
  List<StudyGroupDataSearchCommunitiesData> listUser = [];
  List<LibraryData> listLibrary = [];
  LibraryDataFetchDocuments? library;
  int tabIndex = 0;
  List<String> listCategory = [
    R.string.news.tr(),
    R.string.course.tr(),
    R.string.library.tr(),
    R.string.study_groups_appbar.tr(),
    R.string.community_group.tr(),
    R.string.user.tr(),
  ];

  SystemSearchCubit(this.repository, this.graphqlRepository)
      : super(SystemSearchInitial());

  void getData(
      {String? keyWord,
      required int index,
      bool isRefresh = false,
      bool isLoadMore = false}) async {
    emit(!isRefresh || !isLoadMore
        ? SystemSearchLoading()
        : SystemSearchInitial());
    if (isLoadMore != true) {
      if (tabIndex == 0) {
        nextTokenNews = null;
        listNews.clear();
      } else if (tabIndex == 1) {
        nextTokenCourse = null;
        listCourse.clear();
      } else if (tabIndex == 2) {
        nextTokenLibrary = null;
        listLibrary.clear();
      } else if (tabIndex == 3) {
        nextTokenStudyGroup = null;
        listStudyGroup.clear();
      } else if (tabIndex == 4) {
        nextTokenBasicGroup = null;
        listBasicGroup.clear();
      } else if (tabIndex == 5) {
        nextTokenUser = null;
        listUser.clear();
      }
    }
    List<ApiResult<dynamic>> listApi = await Future.wait([
      graphqlRepository
          .getNews(
        typeCategory: NewsCategory.NEWS,
        searchKey: keyWord,
        nextToken: nextTokenNews,
      )
          .then((value) {
        value.when(success: (dynamic data) {
          if (data is NewsDataFetch) {
            listNews.addAll(data.data ?? []);
            nextTokenNews = data.nextToken;
            newsDataFetch = data;
            emit(SystemSearchNewsSuccess());
          }
        }, failure: (e) {
          emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
        });
        return value;
      }),
      graphqlRepository
          .getCourse(
              searchKey: keyWord,
              nextToken: nextTokenCourse,
              priorityOrder: Order.DESC.name)
          .then((value) {
        value.when(success: (dynamic data) {
          if (data is FetchCourse) {
            listCourse.addAll(data.data ?? []);
            fetchCourse = data;
            nextTokenCourse = data.nextToken;
            emit(SystemSearchCourseSuccess());
          }
        }, failure: (e) {
          emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
        });
        return value;
      }),
      graphqlRepository
          .getLibrary(
        nextToken: nextTokenLibrary,
        keyWord: keyWord,
        globalSearch: true,
      )
          .then((value) {
        value.when(success: (dynamic data) {
          if (data is LibraryDataFetchDocuments) {
            listLibrary.addAll(data.data ?? []);
            library = data;
            nextTokenLibrary = data.nextToken;
            emit(SystemSearchLibrarySuccess());
          }
        }, failure: (e) {
          emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
        });
        return value;
      }),
      graphqlRepository
          .getSearchCommunities(
        nextToken: nextTokenStudyGroup,
        keyWord: keyWord,
        communityType: CommunityType.STUDY_GROUP,
      )
          .then((value) {
        value.when(success: (dynamic data) {
          if (data is StudyGroupSearchCommunities) {
            listStudyGroup.addAll(data.data ?? []);
            nextTokenStudyGroup = data.nextToken;
            studyGroup = data;
            emit(SystemSearchStudySuccess());
          }
        }, failure: (e) {
          emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
        });
        return value;
      }),
      graphqlRepository
          .getSearchCommunities(
        nextToken: nextTokenBasicGroup,
        keyWord: keyWord,
        communityType: CommunityType.BASIC_GROUP,
      )
          .then((value) {
        value.when(success: (dynamic data) {
          if (data is StudyGroupSearchCommunities) {
            listBasicGroup.addAll(data.data ?? []);
            nextTokenBasicGroup = data.nextToken;
            basicGroup = data;
            emit(SystemSearchBasicSuccess());
          }
        }, failure: (e) {
          emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
        });
        return value;
      }),
      graphqlRepository
          .getSearchCommunities(
        nextToken: nextTokenUser,
        keyWord: keyWord,
        communityType: CommunityType.INDIVIDUAL,
      )
          .then((value) {
        value.when(success: (dynamic data) {
          if (data is StudyGroupSearchCommunities) {
            listUser.addAll(data.data ?? []);
            nextTokenUser = data.nextToken;
            user = data;
            emit(SystemSearchUserSuccess());
          }
        }, failure: (e) {
          emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
        });
        return value;
      }),
    ]);
    List.generate(listApi.length, (index) {
      ApiResult task = listApi[index];
      task.when(success: (data) {
        emit(SystemSearchSuccess());
      }, failure: (e) {
        emit(SystemSearchFailure(NetworkExceptions.getErrorMessage(e)));
      });
    });
  }

  void changeTabIndex({required int index, required String keyword}) {
    emit(SystemSearchLoading());
    tabIndex = index;
    getData(index: index, keyWord: keyword);
  }

  void replaceText(String replaceKeyword) {
    emit(SystemSearchLoading());
    replaceKeyword.replaceAll("\\", " ");
    emit(SystemSearchInitial());
  }
}
