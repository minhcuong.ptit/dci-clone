import 'dart:async';
import 'dart:ui';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/club_info/club_information/club_information_page.dart';
import 'package:imi/src/page/course_details/course_details.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_page.dart';
import 'package:imi/src/page/news_details/news_details.dart';
import 'package:imi/src/page/profile/view_profile/view_profile.dart';
import 'package:imi/src/page/system_search/system_search.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/num_ext.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/search/search_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SystemSearchPage extends StatefulWidget {
  final String keyWord;

  const SystemSearchPage({Key? key, required this.keyWord}) : super(key: key);

  @override
  State<SystemSearchPage> createState() => _SystemSearchPageState();
}

class _SystemSearchPageState extends State<SystemSearchPage> {
  late SystemSearchCubit _cubit;
  final TextEditingController _searchController = TextEditingController();
  ScrollController scrollController = ScrollController();
  final RefreshController _refreshController = RefreshController();
  Timer? _debounce;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = SystemSearchCubit(repository, graphqlRepository);
    _cubit.getData(index: _cubit.tabIndex, keyWord: widget.keyWord);
    if (widget.keyWord == "\t") {
      _searchController.text = "\\";
    } else {
      _searchController.text = widget.keyWord;
    }
    scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<SystemSearchCubit, SystemSearchState>(
        listener: (BuildContext context, state) {
          if (state is! SystemSearchLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            backgroundColor: R.color.grey100,
            appBar: AppBar(
              toolbarHeight: 60.h,
              automaticallyImplyLeading: false,
              backgroundColor: R.color.primaryColor,
              leading: InkWell(
                onTap: () => NavigationUtils.pop(context),
                child: Icon(
                  CupertinoIcons.back,
                  color: R.color.white,
                ),
              ),
              title: Image.asset(
                R.drawable.ic_logo_header,
                height: 40.h,
                width: 66.w,
              ),
              centerTitle: true,
            ),
            body: StackLoadingView(
              visibleLoading: state is SystemSearchLoading,
              child: SmartRefresher(
                controller: _refreshController,
                enablePullUp: true,
                onRefresh: () {
                  _cubit.getData(
                      isRefresh: true,
                      index: _cubit.tabIndex,
                      keyWord:
                          _searchController.text.trim().replaceAll("\\", "\t"));
                },
                onLoading: () {
                  _cubit.getData(
                      isLoadMore: true,
                      index: _cubit.tabIndex,
                      keyWord:
                          _searchController.text.trim().replaceAll("\\", "\t"));
                },
                child: ListView(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.h, vertical: 10.h),
                  children: [
                    SearchWidget(
                      margin: 0,
                      autoFocus: false,
                      onSubmit: (text) {
                        _cubit.getData(
                            index: _cubit.tabIndex,
                            keyWord: text.replaceAll("\\", "\t"));
                      },
                      searchController: _searchController,
                      type: Const.SEARCH_DETAIL_SYSTEM,
                      onChange: (text) {
                        // onSearchChanged(text.replaceAll("\\", "\t"));
                      },
                    ),
                    SizedBox(height: 10.h),
                    Container(
                      height: 25.h,
                      width: double.infinity,
                      child: ListView.separated(
                          controller: scrollController,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, int index) {
                            int? total;
                            if (index == 0) {
                              total = _cubit.newsDataFetch?.total;
                            } else if (index == 1) {
                              total = _cubit.fetchCourse?.total;
                            } else if (index == 2) {
                              total = _cubit.library?.total;
                            } else if (index == 3) {
                              total = _cubit.studyGroup?.total;
                            } else if (index == 4) {
                              total = _cubit.basicGroup?.total;
                            } else if (index == 5) {
                              total = _cubit.user?.total;
                            }
                            return GestureDetector(
                              onTap: () {
                                _cubit.changeTabIndex(
                                    index: index,
                                    keyword: _searchController.text
                                        .trim()
                                        .replaceAll("\\", "\t"));
                              },
                              child: Container(
                                padding:
                                    EdgeInsets.fromLTRB(12.w, 2.h, 12.w, 0.h),
                                decoration: BoxDecoration(
                                    color: _cubit.tabIndex == index
                                        ? R.color.lightBlue
                                        : R.color.grey100,
                                    borderRadius: BorderRadius.circular(24)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      _cubit.listCategory[index],
                                      style: Theme.of(context)
                                          .textTheme
                                          .medium500
                                          .copyWith(
                                              color: _cubit.tabIndex == index
                                                  ? R.color.white
                                                  : R.color.shadesGray,
                                              fontSize: 10.sp,
                                              height: 1.0),
                                    ),
                                    SizedBox(width: 3.h),
                                    Text(
                                      total == null
                                          ? ""
                                          : "(${total.toString()})",
                                      style: Theme.of(context)
                                          .textTheme
                                          .medium500
                                          .copyWith(
                                              fontSize: 10.sp,
                                              height: 1,
                                              color: _cubit.tabIndex == index
                                                  ? R.color.white
                                                  : R.color.shadesGray),
                                    ),
                                    Visibility(
                                      visible: _cubit.tabIndex == index,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.w, bottom: 2.h),
                                        child: Image.asset(
                                          R.drawable.ic_check_true,
                                          color: R.color.white,
                                          width: 11.h,
                                          height: 11.h,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (context, index) => SizedBox(
                                width: 8.h,
                              ),
                          itemCount: _cubit.listCategory.length),
                    ),
                    (state is! SystemSearchLoading) &&
                            ((_cubit.listNews.length == 0 &&
                                    _cubit.tabIndex == 0) ||
                                (_cubit.listCourse.length == 0 &&
                                    _cubit.tabIndex == 1) ||
                                (_cubit.listLibrary.length == 0 &&
                                    _cubit.tabIndex == 2) ||
                                (_cubit.listStudyGroup.length == 0 &&
                                    _cubit.tabIndex == 3) ||
                                (_cubit.listBasicGroup.length == 0 &&
                                    _cubit.tabIndex == 4) ||
                                (_cubit.listUser.length == 0 &&
                                    _cubit.tabIndex == 5))
                        ? Visibility(
                            visible: state is SystemSearchSuccess,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.2),
                              child: Text(
                                R.string.no_search_results.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 14.sp, color: R.color.grey),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )
                        : ListView.builder(
                            padding: EdgeInsets.symmetric(vertical: 16.h),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: length(_cubit.tabIndex),
                            itemBuilder: (context, int index) {
                              return buildItem(index, context, state);
                            })
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildItem(int index, BuildContext context, state) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.h),
      child: GestureDetector(
        onTap: () {
          if (_cubit.tabIndex == 0) {
            NavigationUtils.rootNavigatePage(
                context,
                NewsDetailsPage(
                  newsId: _cubit.listNews[index].id,
                ));
          } else if (_cubit.tabIndex == 1) {
            NavigationUtils.rootNavigatePage(
                context,
                CourseDetailsPage(
                  courseId: _cubit.listCourse[index].id,
                  productId: _cubit.listCourse[index].productId??0,
                ));
          } else if (_cubit.tabIndex == 2) {
            NavigationUtils.rootNavigatePage(
                context,
                DetailDocumentPage(
                  link: _cubit.listLibrary[index].link,
                  id: _cubit.listLibrary[index].id,
                  isLocked: _cubit.listLibrary[index].isLocked,
                ));
          } else if (_cubit.tabIndex == 3) {
            NavigationUtils.rootNavigatePage(
                context,
                ViewClubPage(
                  communityId: _cubit.listStudyGroup[index].id ?? 0,
                ));
          } else if (_cubit.tabIndex == 4) {
            NavigationUtils.rootNavigatePage(
                context,
                ViewClubPage(
                    communityId: _cubit.listBasicGroup[index].id ?? 0));
          } else if (_cubit.tabIndex == 5) {
            NavigationUtils.rootNavigatePage(context,
                ViewProfilePage(communityId: _cubit.listUser[index].id));
          }
        },
        child: Row(
          children: [
            if (_cubit.tabIndex == 0) ...[
              AvatarWidget(avatar: _cubit.listNews[index].imageUrl, size: 60.h),
              SizedBox(width: 8.h),
              Expanded(
                child: Column(
                  children: [
                    Text(
                      _cubit.listNews[index].title ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      DateUtil.parseDateToString(
                          DateTime.fromMillisecondsSinceEpoch(
                              _cubit.listNews[index].createdDate ?? 0),
                          "dd/MM/yyyy"),
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12.sp, color: R.color.grey),
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              )
            ],
            if (_cubit.tabIndex == 1) ...[
              AvatarWidget(
                  avatar: _cubit.listCourse[index].avatarUrl, size: 60.h),
              SizedBox(width: 8.h),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _cubit.listCourse[index].name ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 5.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.price_change_outlined, size: 15.h),
                        SizedBox(width: 5.h),
                        Expanded(
                          child: Text(
                            "${Utils.formatMoney(_cubit.listCourse[index].currentFee)} VND",
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 12.sp,
                                    height: 16.h / 12.sp,
                                    color: R.color.grey,
                                    decoration: TextDecoration.lineThrough),
                          ),
                        ),
                        Icon(
                          CupertinoIcons.checkmark_seal,
                          size: 15.h,
                        ),
                        SizedBox(width: 5.h),
                        Text(
                          "${Utils.formatMoney(_cubit.listCourse[index].preferentialFee)} VND",
                          style:
                              Theme.of(context).textTheme.regular400.copyWith(
                                    fontSize: 12.sp,
                                    height: 16.h / 12.sp,
                                    color: R.color.red,
                                  ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
            if (_cubit.tabIndex == 2) ...[
              Stack(
                children: [
                  AvatarWidget(
                      avatar: _cubit.listLibrary[index].imageUrl, size: 60.h),
                  Visibility(
                    visible: _cubit.listLibrary[index].type ==
                        DocumentType.VIDEO.name,
                    child: Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Icon(
                        Icons.play_circle_fill_outlined,
                        color: R.color.white,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(width: 5.h),
              Expanded(
                child: Text(
                  _cubit.listLibrary[index].title ?? "",
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(width: 10.h),
              Visibility(
                visible: _cubit.listLibrary[index].isLocked == true,
                child: Icon(Icons.lock_outline, size: 20.h),
              )
            ],
            if (_cubit.tabIndex == 3) ...[
              AvatarWidget(
                  avatar: _cubit.listStudyGroup[index].avatarUrl, size: 60.h),
              SizedBox(width: 5.h),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _cubit.listStudyGroup[index].name ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 5.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        for (int i = 0;
                            i <
                                _cubit.listStudyGroup[index].groupView!
                                    .bomMembers!.length;
                            i++)
                          if (_cubit.listStudyGroup[index].groupView
                                      ?.bomMembers?[i].role ==
                                  MemberType.LEADER.name &&
                              (_cubit.listStudyGroup[index].groupView
                                      ?.bomMembers?[i].bomMembers?.isNotEmpty ??
                                  false))
                            Expanded(
                              child: Text(
                                _cubit
                                        .listStudyGroup[index]
                                        .groupView
                                        ?.bomMembers?[i]
                                        .bomMembers
                                        ?.first
                                        .fullName ??
                                    "",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp, height: 14.h / 12.sp),
                              ),
                            ),
                        Icon(CupertinoIcons.clock, size: 15.h),
                        SizedBox(width: 5.h),
                        Text(
                          DateUtil.parseDateToString(
                              DateTime.fromMillisecondsSinceEpoch(_cubit
                                      .listStudyGroup[index]
                                      .groupView
                                      ?.event
                                      ?.startTime ??
                                  0),
                              Const.TIME_FORMAT),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 12.sp, height: 14.h / 12.sp),
                        ),
                        SizedBox(width: 5.h),
                        Text(
                          (_cubit.listStudyGroup[index].groupView?.event
                                  ?.dayOfWeek?.title ??
                              ""),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 12.sp, height: 14.h / 12.sp),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
            if (_cubit.tabIndex == 4) ...[
              AvatarWidget(
                  avatar: _cubit.listBasicGroup[index].avatarUrl, size: 60.h),
              SizedBox(width: 5.h),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _cubit.listBasicGroup[index].name ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 5.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Image.asset(R.drawable.ic_card_person,
                            height: 9.h, width: 11.w, color: R.color.black),
                        SizedBox(width: 5.h),
                        Expanded(
                          child: Text(
                            "${(_cubit.listBasicGroup[index].communityInfo?.memberNo ?? 0).quantity()}" +
                                " " +
                                R.string.member_ship.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 12.sp, height: 14.h / 12.sp),
                          ),
                        ),
                        Icon(CupertinoIcons.person_2, size: 15.h),
                        SizedBox(width: 5.h),
                        Text(
                          "${(_cubit.listBasicGroup[index].communityInfo?.followerNo ?? 0).quantity()}" +
                              " " +
                              R.string.followers.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 12.sp, height: 14.h / 12.sp),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
            if (_cubit.tabIndex == 5) ...[
              AvatarWidget(
                  avatar: _cubit.listUser[index].avatarUrl, size: 60.h),
              SizedBox(width: 5.h),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _cubit.listUser[index].name ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 5.h),
                    Row(
                      children: [
                        Image.asset(R.drawable.ic_image, height: 15.h),
                        SizedBox(width: 5.h),
                        Expanded(
                          child: Text(
                            "${_cubit.listUser[index].communityInfo?.postNo}" +
                                " " +
                                R.string.post.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 12.sp, height: 14.h / 12.sp),
                          ),
                        ),
                        Icon(CupertinoIcons.person_2, size: 15.h),
                        SizedBox(width: 5.h),
                        Text(
                          "${(_cubit.listUser[index].communityInfo?.followerNo ?? 0).quantity()}" +
                              " " +
                              R.string.followers.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 12.sp, height: 14.h / 12.sp),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ],
        ),
      ),
    );
  }

  void onSearchChanged(String text) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      _cubit.getData(index: _cubit.tabIndex, keyWord: text);
    });
  }

  int? length(int index) {
    int? length;
    if (index == 0) {
      length = _cubit.listNews.length;
    } else if (index == 1) {
      length = _cubit.listCourse.length;
    } else if (index == 2) {
      length = _cubit.listLibrary.length;
    } else if (index == 3) {
      length = _cubit.listStudyGroup.length;
    } else if (index == 4) {
      length = _cubit.listBasicGroup.length;
    } else if (index == 5) {
      length = _cubit.listUser.length;
    }
    return length;
  }
}
