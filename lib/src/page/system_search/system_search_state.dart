import 'package:equatable/equatable.dart';

abstract class SystemSearchState extends Equatable {
  @override
  List<Object> get props => [];
}

class SystemSearchInitial extends SystemSearchState {}

class SystemSearchLoading extends SystemSearchState {
  @override
  String toString() => 'SystemSearchLoading';
}

class SystemSearchNewsSuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchNewsSuccess';
  }
}

class SystemSearchUserSuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchUserSuccess';
  }
}

class SystemSearchCourseSuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchCourseSuccess';
  }
}

class SystemSearchLibrarySuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchLibrarySuccess';
  }
}

class SystemSearchStudySuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchStudySuccess';
  }
}

class SystemSearchBasicSuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchBasicSuccess';
  }
}

class SystemSearchFailure extends SystemSearchState {
  final String error;

  SystemSearchFailure(this.error);

  @override
  String toString() => 'SystemSearchFailure { error: $error }';
}

class SystemSearchSuccess extends SystemSearchState {
  @override
  String toString() {
    return 'SystemSearchSuccess';
  }
}
