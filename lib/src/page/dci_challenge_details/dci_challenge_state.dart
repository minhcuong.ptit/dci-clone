import 'package:imi/src/data/network/response/detail_challenge_response.dart';
import 'package:imi/src/data/network/response/start_challenge_response.dart';

abstract class DCIChallengeState {}

class DCIChallengeLoadingState extends DCIChallengeState {}

class DCIChallengeFailureState extends DCIChallengeState {
  final e;

  DCIChallengeFailureState(this.e);
}

class DCIChallengeSuccessState extends DCIChallengeState {}

class DCIChallengeCancelSuccessState extends DCIChallengeState {}

class RestartChallengeErrorSuccess extends DCIChallengeState {}

class RestartChallengeErrorLevelSuccess extends DCIChallengeState {}

class ReStartChallengeSuccess extends DCIChallengeState {
  final StartChallengeResponse? data;

  ReStartChallengeSuccess({this.data});
  @override
  String toString() {
    return 'ReStartChallengeSuccess';
  }
}

class DCIChallengeInitial extends DCIChallengeState {}

class DCIChallengeMission extends DCIChallengeState {
  @override
  String toString() {
    return 'DCIChallengeMission';
  }
}
