import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/detail_challenge_response.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_state.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_timeline.dart';
import 'package:imi/src/utils/const.dart';

class DCIChallengeCubit extends Cubit<DCIChallengeState> {
  final AppRepository appRepository;
  final AppGraphqlRepository _graphqlRepository;
  final int _challengVersionId;
  bool isCompletedMission=false;
  DCIChallengeCubit(
      this._graphqlRepository, this._challengVersionId, this.appRepository)
      : super(DCIChallengeLoadingState());

  ChallengeVersionById? detail;

  int currentStep = 1;
  DayState currentDayState = DayState.activated;

  Future init() async {
    currentStep = 1;
    final res = await _graphqlRepository.getDetailChallenge(
        challengeVersionId: _challengVersionId);
    res.when(success: (data) {
      detail = data.fetchChallengeVersionById;
      DateTime? lastKnownDate;
      detail?.challengeHistory?.missionHistory?.forEach((element) {
        if (element?.completedDate != null) {
          if (currentStep <= (element?.dayNo ?? 1)) {
            currentStep = element?.dayNo ?? 1;
            lastKnownDate = element?.completedDate;
          }
        }
      });
      if (lastKnownDate != null &&
          !DateUtils.isSameDay(lastKnownDate, DateTime.now())) {
        currentStep++;
      }
      if ((detail?.challengeHistory?.missionHistory ?? []).isNotEmpty) {
        final index = detail!.challengeHistory!.missionHistory!
            .indexWhere((element) => element?.dayNo == currentStep);
        if (index >= 0 &&
            detail!.challengeHistory!.missionHistory![index]
                    ?.completedMissionIds?.length ==
                detail!.challengeHistory!.missionHistory![index]!.missions
                    ?.length) {
          currentDayState = DayState.done;
          // emit(DCIChallengeMission());
        } else {
          currentDayState = DayState.activated;
        }
      }
      emit(DCIChallengeSuccessState());
    }, failure: (e) {
      emit(DCIChallengeFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void cancelMission({int? challengeId, int? challengeHistoryId}) async {
    emit(DCIChallengeLoadingState());
    final res = await appRepository.cancelMission(
        challengeId: challengeId, challengeHistoryId: challengeHistoryId);
    res.when(success: (dynamic data) {
      emit(DCIChallengeCancelSuccessState());
    }, failure: (e) {
      emit(DCIChallengeFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void reStartPractice({int? challengeId}) async {
    emit(DCIChallengeLoadingState());
    final result =
        await appRepository.getStartPractice(challengeId: challengeId);
    result.when(success: (data) {
      emit(ReStartChallengeSuccess(data: data));
    }, failure: (NetworkExceptions e) {
      if (e is BadRequest &&
          e.code == ServerError.do_only_1_challenge_at_the_same_level) {
        emit(RestartChallengeErrorSuccess());
        return;
      } else if (e is BadRequest &&
          e.code == ServerError.challenge_not_match_your_level_error) {
        emit(RestartChallengeErrorLevelSuccess());
        return;
      }
      emit(DCIChallengeFailureState(""));
    });
  }

  Future reStartChallenge() async {
    emit(DCIChallengeLoadingState());
    await init();
    emit(DCIChallengeInitial());
  }
}
