import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/mission/mission_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../../../res/R.dart';

enum DayState { notActivated, activated, done }

final _icons = [
  R.drawable.challenge_day_1,
  R.drawable.challenge_day_2,
  R.drawable.challenge_day_3,
  R.drawable.challenge_day_4,
  R.drawable.challenge_day_5,
  R.drawable.challenge_day_6,
  R.drawable.challenge_day_7,
  R.drawable.challenge_day_8,
  R.drawable.challenge_day_9,
  R.drawable.challenge_day_9,
  R.drawable.challenge_day_10,
  R.drawable.challenge_day_11,
  R.drawable.challenge_day_12,
  R.drawable.challenge_day_13,
  R.drawable.challenge_day_14,
  R.drawable.challenge_day_15,
  R.drawable.challenge_day_16,
  R.drawable.challenge_day_17,
  R.drawable.challenge_day_18,
  R.drawable.challenge_day_19,
  R.drawable.challenge_day_20,
];

class DCIChallengeTimelineItem extends StatelessWidget {
  final int index;
  final int max;
  final int numberOfMissions;
  final int current;
  final VoidCallback onMission;
  // state cua ngay hien tai
  final DayState currentState;
  // state cua ngay dang ve tren man hinh
  final DayState dayState;
  const DCIChallengeTimelineItem(
      {Key? key,
      required this.index,
      required this.dayState,
      required this.currentState,
      required this.current,
      required this.numberOfMissions,
      required this.max,
      required this.onMission})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      Expanded(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            R.string.day.tr() + " ${index + 1}",
            style: Theme.of(context).textTheme.bodyRegular,
          ),
          SizedBox(
            height: 4,
          ),
          GestureDetector(
            onTap: onMission,
            child: Container(
              padding: dayState == DayState.notActivated
                  ? null
                  : EdgeInsets.only(
                      top: 4,
                      bottom: 4,
                      left: 16,
                      right: dayState == DayState.done ? 8 : 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24),
                color: dayState != DayState.notActivated ? R.color.green : null,
              ),
              child: IntrinsicWidth(
                child: Row(
                  children: [
                    Text("$numberOfMissions " + R.string.quest.tr(),
                        style: dayState != DayState.notActivated
                            ? Theme.of(context)
                                .textTheme
                                .bodyRegular
                                .copyWith(color: R.color.white, height: 16 / 14)
                            : Theme.of(context)
                                .textTheme
                                .bodyMediumText
                                .copyWith(color: R.color.secondaryDarkGrey)),
                    if (dayState == DayState.done) ...[
                      SizedBox(
                        width: 8,
                      ),
                      Image.asset(
                        R.drawable.ic_select_rounded,
                        width: 16,
                        height: 16,
                      )
                    ]
                  ],
                ),
              ),
            ),
          ),
        ],
      )),
      Image.asset(
        _getIcon(),
        width: 40.h,
        height: 40.h,
      ),
      Expanded(child: SizedBox()),
    ];
    int moreAbove = 0;
    int moreBelow = 0;
    if ((current + 1 == index || current == index) &&
        currentState == DayState.done) {
      moreBelow = 1;
      moreAbove = 2;
    }
    return SizedBox(
      height: 60,
      child: Stack(
        children: [
          Center(
            child: Column(
              children: [
                Container(
                  height: 30,
                  width: 4,
                  color: index == 0
                      ? R.color.white
                      : index > current + moreAbove
                          ? R.color.challengeBackgroundGrey
                          : R.color.lightBlue,
                ),
                Container(
                  height: 30,
                  width: 4,
                  color: index == max - 1
                      ? R.color.white
                      : index > current - 1 + moreBelow
                          ? R.color.challengeBackgroundGrey
                          : R.color.lightBlue,
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: index.isOdd ? children.reversed.toList() : children,
          ),
        ],
      ),
    );
  }

  // calculate icon
  String _getIcon() {
    if (dayState != DayState.done) return R.drawable.ic_lock_rounded;
    int realIndex = index + 1;
    if (realIndex == max) {
      return R.drawable.challenge_day_21;
    }
    int mod = (max - 1) % 20;
    int quantitiesPerDay = (max - 1) ~/ 20;
    int bound = (quantitiesPerDay + 1) * mod;
    if (realIndex > bound) {
      return _icons[(index - bound) ~/ quantitiesPerDay + mod];
    }
    return _icons[index ~/ (quantitiesPerDay + 1)];
  }
}
