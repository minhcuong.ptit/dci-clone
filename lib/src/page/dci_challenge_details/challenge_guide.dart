import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class DCIChallengeWidget extends StatefulWidget {
  final String? html;
  const DCIChallengeWidget({Key? key, this.html}) : super(key: key);

  @override
  State<DCIChallengeWidget> createState() => _DCIChallengeWidgetState();
}

class _DCIChallengeWidgetState extends State<DCIChallengeWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          R.string.implementation_guide.tr(),
          style: Theme.of(context)
              .textTheme
              .bodyMedium
              ?.copyWith(fontSize: 18.h, color: R.color.black),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: InkWell(
          onTap: () {
            NavigationUtils.pop(context);
          },
          child: Icon(
            CupertinoIcons.back,
            color: R.color.black,
          ),
        ),
        backgroundColor: R.color.white,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 10.h),
        child: HtmlWidget(widget.html ?? ""),
      ),
    );
  }
}
