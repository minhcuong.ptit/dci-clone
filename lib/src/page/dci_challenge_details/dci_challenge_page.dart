import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/dci_challenge_details/challenge_guide.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_cubit.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_state.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_timeline.dart';
import 'package:imi/src/page/mission/mission.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:developer';
import '../../widgets/receive_point_widget.dart';
import '../home_challenges/home_challenges.dart';

class DCIChallengePage extends StatefulWidget {
  final int challengeVersionId;
  final bool showPopup;

  const DCIChallengePage(
      {Key? key, required this.challengeVersionId, required this.showPopup})
      : super(key: key);

  @override
  _DCIChallengePageState createState() => _DCIChallengePageState();
}

class _DCIChallengePageState extends State<DCIChallengePage> {
  late final DCIChallengeCubit _cubit;
  late final RefreshController _refreshController;
  final DateFormat _df = DateFormat("dd/MM/yyyy");
  final DateFormat? _dfV1 = DateFormat("dd");
  bool isShowPopUp = true;
  bool isSuccessChallenge = false;
  @override
  void initState() {
    super.initState();
    _cubit = DCIChallengeCubit(
        AppGraphqlRepository(), widget.challengeVersionId, AppRepository())
      ..init();
    _refreshController = RefreshController();
    isShowPopUp = widget.showPopup;
  }

  @override
  void dispose() {
    _cubit.close();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        NavigationUtils.popByTime(context,2);
        NavigationUtils.navigatePage(context,HomeChallengesPage());
        return true;
      },
      child: Scaffold(
        backgroundColor: isShowPopUp ? R.color.grey100 : R.color.white,
        appBar: AppBar(
          title: Text(
            R.string.dci_challenge.tr().toUpperCase(),
          ),
          centerTitle: true,
          leading: InkWell(
            onTap: () {
              // if (_cubit.state is DCIChallengeSuccessState) {
              //   NavigationUtils.pop(context);
              // } else {
              //   null;
              // }
              // NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
              NavigationUtils.popByTime(context,2);
              NavigationUtils.navigatePage(context,HomeChallengesPage());
            },
            child: Icon(CupertinoIcons.back, color: R.color.white),
          ),
          backgroundColor: R.color.primaryColor,
        ),
        body: BlocConsumer<DCIChallengeCubit, DCIChallengeState>(
          bloc: _cubit,
          listener: (context, state) {
            if (state is DCIChallengeFailureState) {
              Utils.showSnackBar(context, state.e);
            } else if (state != DCIChallengeLoadingState) {
              _refreshController.refreshCompleted();
            }
            if (state is DCIChallengeCancelSuccessState) {
              NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
            }
            if (state is ReStartChallengeSuccess && state.data != null) {
              NavigationUtils.pop(context);
              NavigationUtils.navigatePage(
                  context,
                  DCIChallengePage(
                      showPopup: false,
                      challengeVersionId: state.data!.idChallengeVersion ?? 0));
            }
            if (state is DCIChallengeInitial &&
                _cubit.detail?.challengeHistory?.status ==
                    ChallengeHistoryStatus.RESOLVED.name) {
              if(appPreferences.getBool(Const.RESOLVE_CHALLENGE)!=true){
                showDialog(
                    context: context,
                    builder: (context) {
                      Future.delayed(
                        Duration(seconds: 1),
                            () {
                          Navigator.of(context).pop();
                          _cubit.isCompletedMission? showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return new ReceivePointWidget(point:50 ,typePoint:'PRACTICE',);
                              }):null;
                        },
                      );
                      return Dialog(
                        alignment: Alignment.center,
                        child: Container(
                          height: 300.h,
                          width: 315.h,
                          padding: EdgeInsets.symmetric(horizontal: 5.h),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(R.drawable.ic_background_challenge),
                            ),
                          ),
                          child: Column(
                            children: [
                              Text(
                                R.string.congratulations.tr(),
                                style: Theme.of(context).textTheme.bold700.copyWith(
                                    fontSize: 24.h, color: R.color.primaryColor),
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                R.string.you_have_completed_the_challenge_set.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                    fontSize: 16.sp,
                                    color: R.color.primaryColor),
                                textAlign: TextAlign.center,
                              ),
                              Expanded(
                                child: Text(
                                  _cubit.detail?.name ?? "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                      fontSize: 16.sp,
                                      color: R.color.primaryColor),
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 4,
                                ),
                              ),
                              Image.asset(
                                _getIcon(),
                                width: 75.h,
                                height: 75.h,
                              ),
                            ],
                          ),
                        ),
                      );
                    });
              }

            }
            if (state is RestartChallengeErrorSuccess) {
              showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return buildPopupChallengeError(context,
                        error: R.string.error_challenge.tr());
                  });
            }
            if (state is RestartChallengeErrorLevelSuccess) {
              showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return buildPopupChallengeError(context,
                        error:
                            R.string.challenge_not_match_your_current_level.tr());
                  });
            }
          },
          builder: (context, state) {
            return StackLoadingView(
              visibleLoading: state is DCIChallengeLoadingState,
              child: SmartRefresher(
                controller: _refreshController,
                enablePullUp: false,
                onRefresh: () {
                  _cubit.init();
                },
                child: Stack(
                  children: [
                    CustomScrollView(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      slivers: [
                        SliverToBoxAdapter(
                          child: SizedBox(height: 16.h),
                        ),
                        SliverToBoxAdapter(
                          child: Visibility(
                            visible: state is! DCIChallengeLoadingState,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: 16.w,
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16.h, vertical: 12.h),
                                    decoration: BoxDecoration(
                                      color: R.color.white,
                                      borderRadius: BorderRadius.circular(10.h),
                                      boxShadow: [
                                        BoxShadow(
                                          color: R.color.black.withOpacity(0.3),
                                          blurRadius: 10,
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        Text(
                                          R.string.youre_watching.tr(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .regular400
                                              .copyWith(fontSize: 12.sp),
                                        ),
                                        Text(
                                          _cubit.detail?.name?.toUpperCase() ??
                                              "",
                                          style: Theme.of(context)
                                              .textTheme
                                              .regular400
                                              .copyWith(
                                                  fontSize: 14.sp,
                                                  color: R.color.primaryColor),
                                          textAlign: TextAlign.center,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(width: 50.h),
                                InkWell(
                                  onTap: () {
                                    NavigationUtils.rootNavigatePage(
                                        context,
                                        DCIChallengeWidget(
                                          html: _cubit.detail?.description ?? "",
                                        ));
                                  },
                                  child: Container(
                                    height: 30.h,
                                    width: 30.h,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: R.color.primaryColor,
                                    ),
                                    child: Icon(
                                      CupertinoIcons.question,
                                      color: R.color.white,
                                      size: 18.h,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(child: SizedBox(height: 20.h)),
                        SliverToBoxAdapter(
                          child: Visibility(
                            visible: state is! DCIChallengeLoadingState,
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 16.w,
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.all(4.h),
                                    decoration: BoxDecoration(
                                        color:
                                            R.color.challengeBackgroundLightBlue,
                                        borderRadius: BorderRadius.circular(8.h)),
                                    height: 190.h,
                                    child: Stack(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8.h),
                                              child: CachedNetworkImage(
                                                width: double.infinity,
                                                height: 108.h,
                                                imageUrl:
                                                    _cubit.detail?.imageUrl ?? "",
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            Expanded(
                                              child: Text(
                                                _cubit.detail?.name ?? "",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bold700
                                                    .copyWith(
                                                        fontSize: 12.sp,
                                                        color: R.color.white),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ),
                                            if (_cubit.detail?.challengeHistory
                                                    ?.status ==
                                                ChallengeHistoryStatus
                                                    .INPROGRESS.name) ...[
                                              Text(
                                                R.string.have_doing.tr(args: [
                                                  "${_cubit.detail!.challengeHistory!.completedTotal! + 1}"
                                                ]),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bold700
                                                    .copyWith(
                                                        fontSize: 12.sp,
                                                        color: R.color.white),
                                              )
                                            ],
                                            if (_cubit.detail?.challengeHistory
                                                    ?.status ==
                                                ChallengeHistoryStatus
                                                    .RESOLVED.name) ...[
                                              Text(
                                                R.string.have_done.tr(args: [
                                                  "${_cubit.detail!.challengeHistory!.completedTotal!}"
                                                ]),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bold700
                                                    .copyWith(
                                                        fontSize: 12.sp,
                                                        color: R.color.white),
                                              )
                                            ]
                                          ],
                                        ),
                                        if ((_cubit.detail?.dayNo ?? 0) > 0)
                                          Positioned(
                                            right: 6,
                                            top: 6,
                                            child: (_cubit.currentDayState !=
                                                    DayState.done)
                                                ? SizedBox()
                                                : Image.asset(
                                                    _getIcon(),
                                                    width: 24.h,
                                                    height: 24.h,
                                                  ),
                                          )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(width: 20.h),
                                Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20.h),
                                    decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            color: R.color.dropShadow
                                                .withOpacity(0.14),
                                            blurRadius: 32,
                                          ),
                                        ],
                                        color: R.color.white,
                                        borderRadius: BorderRadius.circular(8.h)),
                                    height: 190.h,
                                    child: Column(
                                      children: [
                                        SizedBox(height: 4.h),
                                        Text(
                                          R.string.used_day.tr(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bold700
                                              .copyWith(
                                                  fontSize: 12.sp,
                                                  color: R.color.tertiaryOrange,
                                                  height: 18.h / 12.sp),
                                          textAlign: TextAlign.center,
                                        ),
                                        SizedBox(height: 8.h),
                                        _horizontalDayDifference(),
                                        SizedBox(
                                          height: 12.h,
                                        ),
                                        Row(
                                          children: [
                                            Text(R.string.start.tr()),
                                            const Spacer(),
                                            Text(_df.format(_cubit
                                                    .detail
                                                    ?.challengeHistory
                                                    ?.startedDate ??
                                                DateTime.now()))
                                          ],
                                        ),
                                        SizedBox(
                                          height: 2.h,
                                        ),
                                        Row(
                                          children: [
                                            Text(_cubit.detail?.challengeHistory
                                                        ?.completedDate !=
                                                    null
                                                ? R.string.end.tr()
                                                : R.string.present.tr()),
                                            const Spacer(),
                                            Text(_df.format(_cubit
                                                    .detail
                                                    ?.challengeHistory
                                                    ?.completedDate ??
                                                DateTime.now()))
                                          ],
                                        ),
                                        const Spacer(),
                                        _cubit.detail?.challengeHistory?.status ==
                                                ChallengeHistoryStatus
                                                    .INPROGRESS.name
                                            ? GestureDetector(
                                                onTap: () {
                                                  buildConfirmCancel(context);
                                                },
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  padding:
                                                      EdgeInsets.only(bottom: 4),
                                                  decoration: BoxDecoration(
                                                      color:
                                                          R.color.tertiaryOrange,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  child: Text(
                                                    R.string.cancel_challenge
                                                        .tr()
                                                        .toUpperCase(),
                                                    textAlign: TextAlign.center,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .medium500
                                                        .copyWith(
                                                            fontSize: 13,
                                                            color: R.color.white),
                                                  ),
                                                ),
                                              )
                                            : GestureDetector(
                                                onTap: () {
                                                  buildConfirmRestart(context);
                                                },
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  padding:
                                                      EdgeInsets.only(bottom: 4),
                                                  decoration: BoxDecoration(
                                                      color: R.color.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  child: Text(
                                                    R.string.redo
                                                        .tr()
                                                        .toUpperCase(),
                                                    textAlign: TextAlign.center,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .medium500
                                                        .copyWith(
                                                            fontSize: 13,
                                                            color: R.color
                                                                .primaryColor),
                                                  ),
                                                ),
                                              ),
                                        SizedBox(
                                          height: 8.h,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(child: SizedBox(height: 20.h)),
                        if ((_cubit.detail?.dayNo ?? 0) > 0)
                          SliverList(
                              delegate:
                                  SliverChildBuilderDelegate((context, index) {
                            var questState = DayState.done;
                            if (index == _cubit.currentStep - 1)
                              questState = _cubit.currentDayState;
                            if (index > _cubit.currentStep - 1)
                              questState = DayState.notActivated;
                            return DCIChallengeTimelineItem(
                              index: index,
                              max: _cubit.detail?.dayNo ?? 0,
                              dayState: questState,
                              numberOfMissions: _cubit
                                      .detail
                                      ?.challengeHistory
                                      ?.missionHistory?[index]
                                      ?.missions
                                      ?.length ??
                                  0,
                              currentState: _cubit.currentDayState,
                              current: _cubit.currentStep - 1,
                              onMission: () {
                                index > _cubit.currentStep - 1
                                    ? null
                                    : NavigationUtils.rootNavigatePage(
                                        context,
                                        MissionPage(
                                          isCompletedDay: _cubit.detail?.challengeHistory?.missionHistory?[index]?.completedDate!=null,
                                          isCompletedChallenge:  _cubit.detail?.challengeHistory?.status ==
                                              ChallengeHistoryStatus.RESOLVED.name,
                                          nameChallenge: _cubit.detail?.name,
                                          icon: _getIcon(index: index + 1),
                                          description:
                                              _cubit.detail?.description ?? "",
                                          challengeMissionHistoryId: _cubit
                                                  .detail
                                                  ?.challengeHistory
                                                  ?.missionHistory?[index]
                                                  ?.id ??
                                              0,
                                          intDay: index,
                                          challengeId: _cubit.detail?.id ?? 0,
                                          challengeHistoryId: _cubit
                                                  .detail?.challengeHistory?.id ??
                                              0,
                                        )).then((value) async {
                                          _cubit.isCompletedMission=value[1];
                                        await _cubit.reStartChallenge();
                                        if ((value[0] ?? false) &&
                                            _cubit.detail?.challengeHistory
                                                    ?.status ==
                                                ChallengeHistoryStatus
                                                    .INPROGRESS.name&&appPreferences.getBool(Const.RESOLVE_CHALLENGE_DAY)!=true
                                        &&_cubit.detail?.challengeHistory?.missionHistory?[index]?.completedDate!=null) {
                                          showDialog(
                                            useSafeArea: true,
                                            context: context,
                                            builder:
                                                (BuildContext dialogContext) {
                                              return Dialog(
                                                alignment: Alignment.center,
                                                child: Container(
                                                  height: 220.h,
                                                  width: 315.h,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: AssetImage(R.drawable
                                                          .ic_background_challenge),
                                                    ),
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        R.string.congratulations
                                                            .tr(),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .bold700
                                                            .copyWith(
                                                                fontSize: 24.h,
                                                                color: R.color
                                                                    .primaryColor),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                      Text(
                                                        R.string
                                                            .you_have_completed_the_challenge
                                                            .tr(),
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .regular400
                                                            .copyWith(
                                                                fontSize: 16.sp,
                                                                color: R.color
                                                                    .primaryColor),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                      Expanded(
                                                        child: Text(
                                                          R.string.day.tr() +
                                                              " " +
                                                              "${index + 1}",
                                                          style: Theme.of(context)
                                                              .textTheme
                                                              .regular400
                                                              .copyWith(
                                                                  fontSize: 16.sp,
                                                                  color: R.color
                                                                      .primaryColor),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                      Image.asset(
                                                        _getIcon(),
                                                        width: 75.h,
                                                        height: 75.h,
                                                      ),
                                                      SizedBox(height: 10.h)
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        }
                                      });
                              },
                            );
                          }, childCount: _cubit.detail?.dayNo ?? 0))
                      ],
                    ),
                    Visibility(
                      visible: isShowPopUp && state is! DCIChallengeLoadingState,
                      child: Positioned(
                        left: 24.h,
                        right: 24.h,
                        top: MediaQuery.of(context).size.height * 0.25,
                        child: Stack(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.h),
                                border:
                                    Border.all(width: 1, color: R.color.grey100),
                                color: R.color.white,
                                image: DecorationImage(
                                    image: AssetImage(
                                        R.drawable.ic_background_challenge)),
                              ),
                              height: 280.h,
                              width: 315.w,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    R.string.well_come_practice.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(
                                            fontSize: 24.sp,
                                            color: R.color.green),
                                  ),
                                  SizedBox(height: 20.h),
                                  Expanded(
                                    child: RichText(
                                      maxLines: 5,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        text: R.string.welcome_ready_for.tr(
                                            args: [
                                              appPreferences
                                                      .personProfile?.fullName ??
                                                  ""
                                            ]),
                                        style: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(
                                                fontSize: 16.sp,
                                                color: R.color.green,
                                                height: 22.h / 16.sp),
                                        children: [
                                          TextSpan(
                                              text: " " +
                                                  "${_cubit.detail?.name?.toUpperCase()}",
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Text(
                                    R.string.congratulations_dci_challenge.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 16.sp,
                                            color: R.color.green),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(height: 20.h)
                                ],
                              ),
                            ),
                            Positioned(
                              right: 5.h,
                              top: 3.h,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isShowPopUp = !isShowPopUp;
                                  });
                                },
                                child: Icon(CupertinoIcons.clear_thick_circled,
                                    color: R.color.grey, size: 20.h),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget buildPopupChallengeError(BuildContext context, {String? error}) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            R.string.notifications.tr(),
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
          ),
          Text(
            error ?? "",
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 11.sp),
          ),
          SizedBox(height: 20.h),
          ButtonWidget(
              padding: EdgeInsets.symmetric(vertical: 6.h),
              backgroundColor: R.color.primaryColor,
              textSize: 12.sp,
              title: R.string.understanded.tr(),
              onPressed: () {
                NavigationUtils.pop(context);
              })
        ],
      ),
    );
  }

  Future<dynamic> buildConfirmCancel(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 20.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  R.string.confirm.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bold700
                      .copyWith(fontSize: 16.sp),
                ),
                Text(
                  R.string.confirm_cancel_challenge.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 11.sp),
                ),
                SizedBox(height: 10.h),
                ButtonWidget(
                  title: R.string.yes.tr(),
                  onPressed: () {
                    NavigationUtils.pop(context);
                    _cubit.cancelMission(
                      challengeId: _cubit.detail?.id ?? 0,
                      challengeHistoryId:
                          _cubit.detail?.challengeHistory?.id ?? 0,
                    );
                  },
                  padding: EdgeInsets.symmetric(vertical: 16.h),
                  backgroundColor: R.color.orange,
                  textSize: 14.sp,
                ),
                SizedBox(height: 10.h),
                ButtonWidget(
                  title: R.string.no.tr(),
                  onPressed: () {
                    NavigationUtils.pop(context);
                  },
                  padding: EdgeInsets.symmetric(vertical: 16.h),
                  borderColor: R.color.primaryColor,
                  backgroundColor: R.color.white,
                  textSize: 14.sp,
                  textColor: R.color.primaryColor,
                ),
              ],
            ),
          );
        });
  }

  Future<dynamic> buildConfirmRestart(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 20.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  R.string.confirm.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bold700
                      .copyWith(fontSize: 16.sp),
                ),
                Text(
                  R.string.challenge_again.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 11.sp),
                ),
                SizedBox(height: 10.h),
                ButtonWidget(
                  title: R.string.yes.tr(),
                  onPressed: () {
                    NavigationUtils.pop(context);
                    _cubit.reStartPractice(challengeId: _cubit.detail?.id ?? 0);
                  },
                  padding: EdgeInsets.symmetric(vertical: 16.h),
                  backgroundColor: R.color.primaryColor,
                  textSize: 14.sp,
                ),
                SizedBox(height: 10.h),
                ButtonWidget(
                  title: R.string.no.tr(),
                  onPressed: () {
                    NavigationUtils.pop(context);
                  },
                  padding: EdgeInsets.symmetric(vertical: 16.h),
                  borderColor: R.color.primaryColor,
                  backgroundColor: R.color.white,
                  textSize: 14.sp,
                  textColor: R.color.primaryColor,
                ),
              ],
            ),
          );
        });
  }

  Widget _horizontalDayDifference() {
    final temp =
        _cubit.detail?.challengeHistory?.completedDate ?? DateTime.now();
    final dif = DateTime(temp.year, temp.month, temp.day)
        .difference(
            _cubit.detail?.challengeHistory?.startedDate ?? DateTime.now())
        .inDays;
    int count = dif + 1;
    int day = dif + 2;
    return SizedBox(
      height: 39,
      width: 23 * count.toString().length + 2 * (count.toString().length - 1),
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Container(
              width: 23,
              height: 39,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: R.color.lightBlue,
              ),
              child: Text(
                DateUtils.isSameDay(
                        _cubit.detail?.challengeHistory?.startedDate, temp)
                    ? count.toString().characters.elementAt(index)
                    : day.toString().characters.elementAt(index),
                style: Theme.of(context).textTheme.medium500.copyWith(
                    fontSize: 26, height: 37 / 26, color: R.color.white),
                textAlign: TextAlign.center,
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(
              width: 2,
            );
          },
          itemCount: count.toString().length),
    );
  }

  String _getIcon({int? index}) {
    int realIndex = index ?? _cubit.currentStep;
    int max = _cubit.detail?.dayNo ?? 1;
    if (realIndex == _cubit.detail?.dayNo) {
      return R.drawable.challenge_day_21;
    }
    int mod = (max - 1) % 20;
    int quantitiesPerDay = (max - 1) ~/ 20;
    int bound = (quantitiesPerDay + 1) * mod;
    if (realIndex > bound) {
      return _icons[(_cubit.currentStep - 1 - bound) ~/ quantitiesPerDay + mod];
    }
    return _icons[(_cubit.currentStep - 1) ~/ (quantitiesPerDay + 1)];
  }
}

final _icons = [
  R.drawable.challenge_day_1,
  R.drawable.challenge_day_2,
  R.drawable.challenge_day_3,
  R.drawable.challenge_day_4,
  R.drawable.challenge_day_5,
  R.drawable.challenge_day_6,
  R.drawable.challenge_day_7,
  R.drawable.challenge_day_8,
  R.drawable.challenge_day_9,
  R.drawable.challenge_day_9,
  R.drawable.challenge_day_10,
  R.drawable.challenge_day_11,
  R.drawable.challenge_day_12,
  R.drawable.challenge_day_13,
  R.drawable.challenge_day_14,
  R.drawable.challenge_day_15,
  R.drawable.challenge_day_16,
  R.drawable.challenge_day_17,
  R.drawable.challenge_day_18,
  R.drawable.challenge_day_19,
  R.drawable.challenge_day_20,
];
