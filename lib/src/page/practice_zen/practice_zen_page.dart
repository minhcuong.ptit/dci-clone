import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/meditation_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/create_zen/create_zen_page.dart';
import 'package:imi/src/page/practice_zen/practice_zen.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PracticeZenPage extends StatefulWidget {
  const PracticeZenPage({Key? key}) : super(key: key);

  @override
  State<PracticeZenPage> createState() => _PracticeZenPageState();
}

class _PracticeZenPageState extends State<PracticeZenPage> {
  late PracticeZenCubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = PracticeZenCubit(repository, graphqlRepository);
    _cubit.getListMeditation();
  }
  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar:
          appBar(context, R.string.practice_of_meditation.tr().toUpperCase(),
              backgroundColor: R.color.primaryColor,
              titleColor: R.color.white,
              centerTitle: true,
              iconColor: R.color.white,
              leadingWidget: InkWell(
                onTap: () =>
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH),
                child: Icon(
                  CupertinoIcons.left_chevron,
                  color: R.color.white,
                ),
              )),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<PracticeZenCubit, PracticeZenState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! PracticeZenLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, PracticeZenState state) {
    return StackLoadingView(
      visibleLoading: state is PracticeZenLoading,
      child: SmartRefresher(
        controller: _refreshController,
        enablePullUp: _cubit.nextToken != null,
        onRefresh: () => _cubit.getListMeditation(isRefresh: true),
        onLoading: () => _cubit.getListMeditation(isLoadMore: true),
        child: ListView(
          padding: EdgeInsets.all(20.h),
          children: [
            Text(
              R.string.choose_type_of_meditation.tr(),
              style: Theme.of(context)
                  .textTheme
                  .medium500
                  .copyWith(fontSize: 16.sp, color: R.color.primaryColor),
            ),
            SizedBox(height: 30.h),
            GridView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.zero,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 16.h,
                  mainAxisSpacing: 16.h,
                  childAspectRatio: 11 / 12),
              itemCount: _cubit.listMeditation.length,
              itemBuilder: (BuildContext context, int index) {
                FetchMeditationsData data = _cubit.listMeditation[index];
                return GestureDetector(
                  onTap: () {
                    if (data.isLocked == false) {
                      appPreferences.removeData(Const.TIMER_START_MUSIC);
                      appPreferences.removeData(Const.TIMER_START_NO_MUSIC);
                      NavigationUtils.navigatePage(
                          context,
                          CreateZenPage(
                            meditationId: data.id ?? 0,
                          ));
                    } else {
                      Utils.showCommonBottomSheet(
                          context: context,
                          title: R.string.notifications.tr(),
                          formattedDetails: R.string
                              .you_dci_program_is_not_suitable_for_meditation
                              .tr(args: [data.title ?? ""]),
                          buttons: [
                            ButtonWidget(
                                backgroundColor: R.color.primaryColor,
                                borderColor: R.color.primaryColor,
                                padding: EdgeInsets.symmetric(vertical: 6.h),
                                title: R.string.understood.tr(),
                                textStyle: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 12.sp, color: R.color.white),
                                onPressed: () {
                                  NavigationUtils.pop(context);
                                }),
                          ]);
                    }
                  },
                  child: buildItemZen(context,
                      image: data.imageUrl,
                      title: data.title,
                      isLock: data.isLocked),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildItemZen(
    BuildContext context, {
    String? image,
    String? title,
    bool? isLock,
  }) {
    return Container(
      decoration: BoxDecoration(
          color: R.color.white, borderRadius: BorderRadius.circular(10.h)),
      padding: EdgeInsets.all(4.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.h),
                child: CachedNetworkImage(
                    width: double.infinity,
                    height: 108.h,
                    fit: BoxFit.cover,
                    imageUrl: image ?? ""),
              ),
              Visibility(
                visible: isLock ?? false,
                child: Positioned(
                    top: 5.h,
                    right: 5.h,
                    child: Container(
                      width: 25.h,
                      height: 25.h,
                      child: Icon(
                        CupertinoIcons.lock,
                        size: 15.h,
                        color: R.color.blue,
                      ),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: R.color.white),
                    )),
              ),
            ],
          ),
          SizedBox(height: 8.h),
          Text(
            title ?? "",
            style: Theme.of(context).textTheme.regular400.copyWith(
                fontWeight: FontWeight.normal,
                color: R.color.black,
                fontSize: 14,
                height: 21 / 14),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
