abstract class PracticeZenState {
  PracticeZenState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class PracticeZenInitial extends PracticeZenState {
  @override
  String toString() => 'PracticeZenInitial';
}

class PracticeZenSuccess extends PracticeZenState {
  @override
  String toString() => 'PracticeZenSuccess';
}

class PracticeZenLoading extends PracticeZenState {
  @override
  String toString() => 'PracticeZenLoading';
}

class PracticeZenFailure extends PracticeZenState {
  final String error;

  PracticeZenFailure(this.error);

  @override
  String toString() => 'PracticeZenFailure { error: $error }';
}
