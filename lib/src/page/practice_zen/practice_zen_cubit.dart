import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/meditation_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/practice_zen/practice_zen_state.dart';

class PracticeZenCubit extends Cubit<PracticeZenState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  List<FetchMeditationsData> listMeditation = [];
  String? nextToken;

  PracticeZenCubit(this.appRepository, this.graphqlRepository)
      : super(PracticeZenInitial());

  void getListMeditation({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? PracticeZenLoading() : PracticeZenInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listMeditation.clear();
    }
    ApiResult<FetchMeditation> getMeditation =
        await graphqlRepository.getMeditation(nextToken: nextToken);
    getMeditation.when(success: (FetchMeditation data) async {
      if (data.data != null) {
        listMeditation.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(PracticeZenSuccess());
      }
    }, failure: (NetworkExceptions error) async {
      emit(PracticeZenFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
