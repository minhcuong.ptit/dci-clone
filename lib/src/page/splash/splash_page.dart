import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Material(
          child: Center(
              child: Image.asset(
            R.drawable.bg_splash,
            height: double.infinity,
            fit: BoxFit.cover,
          )),
          color: R.color.primaryColor,
        ));
  }
}
