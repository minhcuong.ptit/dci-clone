import 'package:equatable/equatable.dart';

abstract class SettingVirtualAssistantState extends Equatable {
  @override
  List<Object> get props => [];
}

class SettingVirtualAssistantInitial extends SettingVirtualAssistantState {}

class SettingVirtualAssistantLoading extends SettingVirtualAssistantState {
  @override
  String toString() => 'SettingVirtualAssistantLoading';
}

class SettingVirtualAssistantSuccess extends SettingVirtualAssistantState {
  @override
  String toString() {
    return 'SettingVirtualAssistantSuccess';
  }
}

class SettingVirtualAssistantFailure extends SettingVirtualAssistantState {
  final String error;

  SettingVirtualAssistantFailure(this.error);

  @override
  String toString() => 'SettingVirtualAssistantFailure { error: $error }';
}
