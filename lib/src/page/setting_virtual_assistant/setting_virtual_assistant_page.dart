import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/setting_virtual_assistant/setting_virtual_assistant.dart';
import 'package:imi/src/page/setting_virtual_assistant/widget/time_meditaion.dart';
import 'package:imi/src/page/setting_virtual_assistant/widget/time_plan_seed.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SettingVirtualAssistantPage extends StatefulWidget {
  const SettingVirtualAssistantPage({Key? key}) : super(key: key);

  @override
  State<SettingVirtualAssistantPage> createState() =>
      _SettingVirtualAssistantPageState();
}

class _SettingVirtualAssistantPageState
    extends State<SettingVirtualAssistantPage> {
  late SettingVirtualAssistantCubit _cubit;

  RefreshController _controller = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = SettingVirtualAssistantCubit(repository, graphqlRepository);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.time_setting.tr().toUpperCase(),
        backgroundColor: R.color.primaryColor,
        titleColor: R.color.white,
        centerTitle: true,
        iconColor: R.color.white,
      ),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<SettingVirtualAssistantCubit,
            SettingVirtualAssistantState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, SettingVirtualAssistantState state) {
    return StackLoadingView(
      visibleLoading: state is SettingVirtualAssistantLoading,
      child: ListView(
        padding: EdgeInsets.all(20.h),
        children: [
          Text(
            R.string.setting_time_every_day.tr(),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.regular400.copyWith(
                  fontSize: 16.sp,
                  color: R.color.black,
                  height: 20 / 16,
                  fontStyle: FontStyle.italic,
                ),
          ),
          SizedBox(height: 30.h),
          buildCreateZen(
            callback: () {
              NavigationUtils.navigatePage(context, TimeMeditation(index: 0))
                  .then((value) => _cubit.refresh());
            },
            title: R.string.meditation_time.tr(),
            icon: R.drawable.ic_zen,
            time: DateUtil.parseDateToString(
                    DateTime.fromMillisecondsSinceEpoch(_cubit.saveTimeZen,
                        isUtc: true),
                    Const.TIME_FORMAT) +
                " " +
                R.string.every_day.tr() +
                "${_cubit.setCountTime > 0 ? " (+${_cubit.setCountTime + 1})" : ""}",
            cupertinoSwitch: CupertinoSwitch(
              value: _cubit.timeMeditation
                  ? _cubit.timeMeditation
                  : _cubit.isSetTimeMeditation,
              onChanged: (bool value) {
                setState(() {
                  _cubit.setTime(0);
                  appPreferences.setData(
                      Const.TIMER_MEDITATION, _cubit.isSetTimeMeditation);
                  Future.delayed(Duration(seconds: 1), () {
                    _cubit.practiceTimeSetting();
                  });
                });
              },
            ),
          ),
          SizedBox(height: 20.h),
          buildCreateZen(
            callback: () {
              NavigationUtils.navigatePage(
                  context,
                  TimePlanSeedWidget(
                    index: 1,
                  )).then((value) => _cubit.refresh());
            },
            title: R.string.time_to_write_the_seed.tr(),
            icon: R.drawable.ic_plan_tree,
            time: DateUtil.parseDateToString(
                    DateTime.fromMillisecondsSinceEpoch(_cubit.saveTimePlan,
                        isUtc: true),
                    Const.TIME_FORMAT) +
                " " +
                R.string.every_day.tr(),
            cupertinoSwitch: CupertinoSwitch(
              value: _cubit.timePlanSeed
                  ? _cubit.timePlanSeed
                  : _cubit.isSetTimePlanSeed,
              onChanged: (bool value) {
                setState(() {
                  _cubit.setTime(1);
                  appPreferences.setData(
                      Const.TIMER_PLAN_SEED, _cubit.isSetTimePlanSeed);
                  Future.delayed(Duration(seconds: 1), () {
                    _cubit.practiceTimeSetting();
                  });
                });
              },
            ),
          ),
          SizedBox(height: 20.h),
          buildCreateZen(
            callback: () {
              NavigationUtils.navigatePage(
                  context,
                  TimePlanSeedWidget(
                    index: 2,
                  )).then((value) => _cubit.refresh());
            },
            title: R.string.meditation_cafe_time.tr(),
            icon: R.drawable.ic_meditation_cafe,
            time: DateUtil.parseDateToString(
                    DateTime.fromMillisecondsSinceEpoch(_cubit.saveTimeCafe,
                        isUtc: true),
                    Const.TIME_FORMAT) +
                " " +
                R.string.every_day.tr(),
            cupertinoSwitch: CupertinoSwitch(
              value: _cubit.timeMeditationCafe
                  ? _cubit.timeMeditationCafe
                  : _cubit.isSetTimeMeditationCafe,
              onChanged: (bool value) {
                setState(() {
                  _cubit.setTime(2);
                  appPreferences.setData(Const.TIMER_MEDITATION_CAFE,
                      _cubit.isSetTimeMeditationCafe);
                  Future.delayed(Duration(seconds: 1), () {
                    _cubit.practiceTimeSetting();
                  });
                });
              },
            ),
          )
        ],
      ),
    );
  }

  Widget buildCreateZen(
      {String? title,
      String? icon,
      String? time,
      VoidCallback? callback,
      Widget? cupertinoSwitch}) {
    return InkWell(
      onTap: callback,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.h, vertical: 10.h),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40.h),
            color: R.color.lightBlue),
        child: Row(
          children: [
            Image.asset(icon ?? "", height: 24.h, width: 25.h),
            SizedBox(width: 16.w),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title ?? "",
                    style: Theme.of(context).textTheme.medium500.copyWith(
                        fontSize: 16.sp, color: R.color.white, height: 20 / 16),
                  ),
                  Text(
                    time ?? "",
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 14.sp, color: R.color.white, height: 17 / 14),
                  ),
                ],
              ),
            ),
            cupertinoSwitch ?? SizedBox()
          ],
        ),
      ),
    );
  }
}
