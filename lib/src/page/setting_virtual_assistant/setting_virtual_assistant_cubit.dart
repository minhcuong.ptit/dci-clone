import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/practice_time_setting_request.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/setting_virtual_assistant/setting_virtual_assistant_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';

class SettingVirtualAssistantCubit extends Cubit<SettingVirtualAssistantState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool isSetTimeMeditation = false;
  bool isSetTimePlanSeed = false;
  bool isSetTimeMeditationCafe = false;
  bool isInit=false;
  List<int> timeLocal=[appPreferences.getInt(Const.SET_TIME_MEDITATION1) ?? 0
    ,appPreferences.getInt(Const.SET_TIME_MEDITATION2) ?? 0,appPreferences.getInt(Const.SET_TIME_MEDITATION3) ?? 0,
    appPreferences.getInt(Const.SET_TIME_MEDITATION4) ?? 0,appPreferences.getInt(Const.SET_TIME_MEDITATION5) ?? 0,
    appPreferences.getInt(Const.SET_TIME_MEDITATION6) ?? 0,appPreferences.getInt(Const.SET_TIME_MEDITATION7) ?? 0];
  List<Duration> initaialTimer=[];
  bool get timeMeditation =>
      appPreferences.getBool(Const.TIMER_MEDITATION) ?? isSetTimeMeditation;

  bool get timePlanSeed =>
      appPreferences.getBool(Const.TIMER_PLAN_SEED) ?? isSetTimePlanSeed;

  bool get timeMeditationCafe =>
      appPreferences.getBool(Const.TIMER_MEDITATION_CAFE) ??
          isSetTimeMeditationCafe;

  int get timeCafe => appPreferences.getInt(Const.SET_TIME_CAFE) ?? 0;

  int get timePlan => appPreferences.getInt(Const.TIME_PLAN_SEED) ?? 0;
  int timeUTC = 25200000;

  int get timeZen => appPreferences.getInt(Const.SET_TIME_MEDITATION) ?? 0;

  int get timeZen1 => appPreferences.getInt(Const.SET_TIME_MEDITATION1) ?? 0;

  int get timeZen2 => appPreferences.getInt(Const.SET_TIME_MEDITATION2) ?? 0;

  int get timeZen3 => appPreferences.getInt(Const.SET_TIME_MEDITATION3) ?? 0;

  int get timeZen4 => appPreferences.getInt(Const.SET_TIME_MEDITATION4) ?? 0;

  int get timeZen5 => appPreferences.getInt(Const.SET_TIME_MEDITATION5) ?? 0;

  int get timeZen6 => appPreferences.getInt(Const.SET_TIME_MEDITATION6) ?? 0;

  int get timeZen7 => appPreferences.getInt(Const.SET_TIME_MEDITATION7) ?? 0;

  int get saveTimeCafe => appPreferences.getInt(Const.SAVE_SET_TIME_CAFE) ?? 0;

  int get saveTimePlan => appPreferences.getInt(Const.SAVE_TIME_PLAN_SEED) ?? 0;

  int get saveTimeZen =>
      appPreferences.getInt(Const.SAVE_SET_TIME_MEDITATION) ?? 0;

  int get setCountTime => appPreferences.getInt(Const.COUNT_TIME) ?? 0;

  List<int> countTime = [];

  SettingVirtualAssistantCubit(this.repository, this.graphqlRepository)
      : super(SettingVirtualAssistantInitial());

  void setTime(int index) {
    emit(SettingVirtualAssistantLoading());
    switch (index) {
      case 0:
        this.isSetTimeMeditation = !isSetTimeMeditation;
        break;
      case 1:
        this.isSetTimePlanSeed = !isSetTimePlanSeed;
        break;
      case 2:
        this.isSetTimeMeditationCafe = !isSetTimeMeditationCafe;
        break;
    }
    emit(SettingVirtualAssistantInitial());
  }

  void refresh() {
    emit(SettingVirtualAssistantLoading());
    emit(SettingVirtualAssistantInitial());
  }

  void practiceTimeSetting() async {
    emit(SettingVirtualAssistantLoading());
    ApiResult<dynamic> result = await repository.getPracticeTimeSetting(
        PracticeTimeSettingRequest(
            notification: RequestNotification(
                reminder: Reminder(
                    meditation: ReminderMeditation(times: [
                      (timeZen == 0 ? 0 : (timeZen - timeUTC)),
                      (timeZen1 == 0 ? 0 : (timeZen1 - timeUTC)),
                      (timeZen2 == 0 ? 0 : (timeZen2 - timeUTC)),
                      (timeZen3 == 0 ? 0 : (timeZen3 - timeUTC)),
                      (timeZen4 == 0 ? 0 : (timeZen4 - timeUTC)),
                      (timeZen5 == 0 ? 0 : (timeZen5 - timeUTC)),
                      (timeZen6 == 0 ? 0 : (timeZen6 - timeUTC)),
                      (timeZen7 == 0 ? 0 : (timeZen7 - timeUTC))
                    ], enable: timeMeditation),
                    planSeed: ReminderPlanSeed(
                        time: timePlan - timeUTC, enable: timePlanSeed),
                    meditationCoffee: ReminderMeditationCoffee(
                        time: timeCafe - timeUTC,
                        enable: timeMeditationCafe)))));
    result.when(success: (data) {
      emit(SettingVirtualAssistantSuccess());
    }, failure: (e) {
      emit(
          SettingVirtualAssistantFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void increaseTime(int index) {
    emit(SettingVirtualAssistantLoading());
    countTime.add(index);
    emit(SettingVirtualAssistantInitial());
  }

  void decreaseTime(int index) {
    emit(SettingVirtualAssistantLoading());
    countTime.removeAt(index);
    initaialTimer.removeAt(index);
    timeLocal.removeAt(index);
    emit(SettingVirtualAssistantInitial());
  }
  void init (){
    for(int i=0;i<setCountTime;i++){
      countTime.add(i);
      initaialTimer.add(Duration(
          microseconds: (timeLocal[i] * 1000).toInt()));
    }
    isInit=true;
  }
}
