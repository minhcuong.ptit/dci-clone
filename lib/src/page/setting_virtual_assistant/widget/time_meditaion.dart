
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/setting_virtual_assistant/setting_virtual_assistant_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

class TimeMeditation extends StatefulWidget {
  final int index;

  TimeMeditation({required this.index});

  @override
  State<TimeMeditation> createState() => _TimeMeditationState();
}

class _TimeMeditationState extends State<TimeMeditation> {
  Duration initialTimer = new Duration();
  Duration initialTimer1 = new Duration();
  Duration initialTimer2 = new Duration();
  Duration initialTimer3 = new Duration();
  Duration initialTimer4 = new Duration();
  Duration initialTimer5 = new Duration();
  Duration initialTimer6 = new Duration();
  Duration initialTimer7 = new Duration();
  late SettingVirtualAssistantCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = SettingVirtualAssistantCubit(repository, graphqlRepository);
    _cubit.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.time_setting.tr().toUpperCase(),
        backgroundColor: R.color.primaryColor,
        titleColor: R.color.white,
        centerTitle: true,
        iconColor: R.color.white,
      ),
      body: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.h),
      child: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              buildText(
                  context, "${R.string.hours_of_meditation_time.tr()} ${1}"),
              InkWell(
                onTap: () {
                  setState(() {
                    if (_cubit.countTime.length < 7) {
                      _cubit.countTime.add(1);
                      _cubit.initaialTimer.add(new Duration());
                      // bool delete = false;
                      // for (int i = 0; i < _cubit.countTime.length; i++) {
                      //   if (!_cubit.countTime.contains(i)) {
                      //     delete = true;
                      //     _cubit.countTime.insert(i, i);
                      //     return;
                      //   }
                      // }
                      // if (!delete) {
                      //   _cubit.increaseTime(_cubit.countTime.length);
                      // }
                    }
                  });
                },
                child: Icon(
                  CupertinoIcons.add_circled,
                  size: 24.h,
                  color: R.color.primaryColor,
                ),
              )
            ],
          ),
          SizedBox(height: 20.h),
          Container(
            height: 180.h,
            child: CupertinoTimerPicker(
                minuteInterval: 1,
                secondInterval: 1,
                initialTimerDuration: Duration(microseconds: (_cubit.timeZen * 1000).toInt()),
                mode: CupertinoTimerPickerMode.hm,
                onTimerDurationChanged: (Duration changedTimer) {
                  setState(() {
                    initialTimer = changedTimer;
                  });
                }),
          ),

          ListView.builder(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              itemCount: _cubit.countTime.length,
              itemBuilder: ((context, index) {
                return Column(
                  children: [
                    buildRowNumberMeditation(
                      context,
                      "${R.string.hours_of_meditation_time.tr()} ${index+2}",
                          () {
                            setState(() {
                              if (_cubit.countTime.length >= 1) {
                                _cubit.decreaseTime(index);
                              }
                            });
                          }),
                    SizedBox(height: 20.h),
                    buildTimer(
                        _cubit.initaialTimer[index] ,
                        (Duration changedTimer) {
                      setState(() {
                        _cubit.initaialTimer[index] = changedTimer;
                      });
                    },),
                  ],
                );
              })),
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 14.h),
              textSize: 14.sp,
              title: R.string.save.tr(),
              onPressed:
                  // (initialTimer.inHours != 0 ||
                  //         initialTimer.inMinutes % 60 != 0)
                  //     ?
                  () {
                if (widget.index == 0) {
                  NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  appPreferences.setData(
                      Const.SAVE_SET_TIME_MEDITATION,
                      (initialTimer.inHours != 0 ||
                              initialTimer.inMinutes % 60 != 0)
                          ? initialTimer.inMilliseconds
                          : _cubit.timeZen);
                  appPreferences.setData(
                      Const.SET_TIME_MEDITATION,
                      (initialTimer.inHours != 0 ||
                              initialTimer.inMinutes % 60 != 0)
                          ? initialTimer.inMilliseconds
                          : _cubit.timeZen);
                  _cubit.initaialTimer.length>=1?appPreferences.setData(
                      Const.SET_TIME_MEDITATION1,
                      (_cubit.initaialTimer[0].inHours != 0 ||
                          _cubit. initaialTimer[0].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[0].inMilliseconds
                          : _cubit.timeZen1):null;
                  _cubit.initaialTimer.length>=2?appPreferences.setData(
                      Const.SET_TIME_MEDITATION2,
                      (_cubit.initaialTimer[1].inHours != 0 ||
                          _cubit.initaialTimer[1].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[1].inMilliseconds
                          : _cubit.timeZen2):null;
                  _cubit.initaialTimer.length>=3? appPreferences.setData(
                      Const.SET_TIME_MEDITATION3,
                      (_cubit.initaialTimer[2].inHours != 0 ||
                          _cubit.initaialTimer[2].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[2].inMilliseconds
                          : _cubit.timeZen3):null;
                  _cubit.initaialTimer.length>=4?appPreferences.setData(
                      Const.SET_TIME_MEDITATION4,
                      (_cubit.initaialTimer[3].inHours != 0 ||
                          _cubit.initaialTimer[3].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[3].inMilliseconds
                          : _cubit.timeZen4):null;
                  _cubit.initaialTimer.length>=5?appPreferences.setData(
                      Const.SET_TIME_MEDITATION5,
                      (_cubit.initaialTimer[4].inHours != 0 ||
                          _cubit.initaialTimer[4].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[4].inMilliseconds
                          : _cubit.timeZen5):null;
                  _cubit.initaialTimer.length>=6?appPreferences.setData(
                      Const.SET_TIME_MEDITATION6,
                      (_cubit.initaialTimer[5].inHours != 0 ||
                          _cubit.initaialTimer[5].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[5].inMilliseconds
                          : _cubit.timeZen6):null;
                  _cubit.initaialTimer.length>=7? appPreferences.setData(
                      Const.SET_TIME_MEDITATION7,
                      (_cubit.initaialTimer[6].inHours != 0 ||
                          _cubit.initaialTimer[6].inMinutes % 60 != 0)
                          ? _cubit.initaialTimer[6].inMilliseconds
                          : _cubit.timeZen7):null;
                  appPreferences.setData(
                      Const.COUNT_TIME, _cubit.countTime.length);
                  _cubit.practiceTimeSetting();
                }
              }
          )
        ],
      ),
    );
  }

  Widget buildRowNumberMeditation(
      BuildContext context, String title, VoidCallback callback) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        buildText(context, title),
        InkWell(
          onTap: callback,
          child: Icon(
            CupertinoIcons.minus_circle,
            size: 24.h,
            color: R.color.primaryColor,
          ),
        )
      ],
    );
  }

  Widget buildTimer(Duration time, Function(Duration) onTimerDurationChanged) {
    return Container(
      height: 180.h,
      child: CupertinoTimerPicker(
          key: UniqueKey(),
          minuteInterval: 1,
          secondInterval: 1,
          initialTimerDuration: time,
          mode: CupertinoTimerPickerMode.hm,
          onTimerDurationChanged: onTimerDurationChanged),
    );
  }

  Widget buildButtonWidget(BuildContext context, VoidCallback callback) {
    return ButtonWidget(
        backgroundColor: R.color.primaryColor,
        padding: EdgeInsets.symmetric(vertical: 14.h),
        textSize: 14.sp,
        title: R.string.save.tr(),
        onPressed: callback);
  }
//Todo: Change All Helper Methods to Widgets
// Todo: Delete ShinkWraps in ListView
  Widget buildText(BuildContext context, String title) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .bold700
          .copyWith(fontSize: 16.sp, color: R.color.black, height: 24 / 16),
    );
  }
}
