import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/setting_virtual_assistant/setting_virtual_assistant_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

class TimePlanSeedWidget extends StatefulWidget {
  final int index;

  TimePlanSeedWidget({required this.index});

  @override
  State<TimePlanSeedWidget> createState() => _TimePlanSeedWidgetState();
}

class _TimePlanSeedWidgetState extends State<TimePlanSeedWidget> {
  Duration initialTimer = new Duration();
  bool isMeditation = true;
  late SettingVirtualAssistantCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = SettingVirtualAssistantCubit(repository, graphqlRepository);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.time_setting.tr().toUpperCase(),
        backgroundColor: R.color.primaryColor,
        titleColor: R.color.white,
        centerTitle: true,
        iconColor: R.color.white,
      ),
      body: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.h),
      child: ListView(
        children: [
          Visibility(
            visible: widget.index == 1,
            child: buildText(context, R.string.time_to_write_the_seed.tr()),
          ),
          Visibility(
            visible: widget.index == 2,
            child: buildText(context, R.string.meditation_cafe_time.tr()),
          ),
          SizedBox(height: 20.h),
          Container(
            height: 180.h,
            child: CupertinoTimerPicker(
                minuteInterval: 1,
                secondInterval: 1,
                initialTimerDuration:widget.index==1?Duration(microseconds: (_cubit.timePlan * 1000).toInt())
                :Duration(microseconds: (_cubit.timeCafe * 1000).toInt()),
                mode: CupertinoTimerPickerMode.hm,
                onTimerDurationChanged: (Duration changedTimer) {
                  setState(() {
                    initialTimer = changedTimer;
                  });
                }),
          ),
          SizedBox(height: 20.h),
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 14.h),
              textSize: 14.sp,
              title: R.string.save.tr(),
              onPressed: (initialTimer.inHours != 0 ||
                      initialTimer.inMinutes % 60 != 0)
                  ? () {
                      if (widget.index == 0) {

                      } else if (widget.index == 1) {
                        NavigationUtils.pop(context,
                            result: Const.ACTION_REFRESH);
                        appPreferences.setData(Const.SAVE_TIME_PLAN_SEED,
                            initialTimer.inMilliseconds);
                        appPreferences.setData(
                            Const.TIME_PLAN_SEED, initialTimer.inMilliseconds);
                         _cubit.practiceTimeSetting();
                      } else if (widget.index == 2) {
                        NavigationUtils.pop(context,
                            result: Const.ACTION_REFRESH);
                        appPreferences.setData(Const.SAVE_SET_TIME_CAFE,
                            initialTimer.inMilliseconds);
                        appPreferences.setData(
                            Const.SET_TIME_CAFE, initialTimer.inMilliseconds);
                        _cubit.practiceTimeSetting();
                      }
                    }
                  : initialTimer.inSeconds % 60 == 0
                      ? null
                      : null)
        ],
      ),
    );
  }

  Widget buildText(BuildContext context, String title) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .bold700
          .copyWith(fontSize: 16.sp, color: R.color.black, height: 24 / 16),
    );
  }
}
