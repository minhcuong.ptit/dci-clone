import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/detail_cart/detail_cart.dart';
import 'package:imi/src/page/history_point/history_point.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/receive_point_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/history_point_response.dart';
import '../get_point_instruction/get_point_instruction_page.dart';
import '../main/main_page.dart';

class HistoryPointPage extends StatefulWidget {
  final bool? isNotification;


  HistoryPointPage({this.isNotification});

  @override
  State<HistoryPointPage> createState() => _HistoryPointPageState();
}

class _HistoryPointPageState extends State<HistoryPointPage>
    with TickerProviderStateMixin {
  late HistoryPointCubit _cubit;
  late TabController _tabController;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HistoryPointCubit(repository, graphqlRepository);
    _tabController = new TabController(vsync: this, length: 2);
    _cubit.getListHistoryPoint();
    _cubit.getListHistoryPointOut();
    _cubit.getProfile();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<HistoryPointCubit, HistoryPointState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! HistoryPointLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, HistoryPointState state) {
    return Scaffold(
      appBar: appBar(context, "",
          backgroundColor: R.color.primaryColor,
          leadingWidget: GestureDetector(
              onTap: () async {
                widget.isNotification == true
                    ? NavigationUtils.pushAndRemoveUtilPage(context, MainPage())
                    : NavigationUtils.pop(context);
              },
              child: Icon(Icons.arrow_back_ios,color: R.color.white,))),
      body: StackLoadingView(
        visibleLoading: state is HistoryPointLoading,
        child: SmartRefresher(
          enablePullUp: true,
          controller: _refreshController,
          onRefresh: () {
            if (_tabController.index == 0) {
              _cubit.getListHistoryPoint(isRefresh: true);
            } else {
              _cubit.getListHistoryPointOut(isRefresh: true);
            }
          },
          onLoading: () {
            if (_tabController.index == 0) {
              _cubit.getListHistoryPoint(isLoadMore: true);
            } else {
              _cubit.getListHistoryPointOut(isLoadMore: true);
            }
          },
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
                  color: R.color.primaryColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(R.drawable.ic_diamond_point,
                              height: 28.h, width: 28.h),
                          SizedBox(width: 5.w),
                          RichText(
                            text: TextSpan(
                              text: "${Utils.formatMoney(_cubit.point)}",
                              style: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 28.sp, color: R.color.white),
                              children: <TextSpan>[
                                TextSpan(
                                    text: ' ${R.string.point_dci.tr()}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 16.sp,
                                            color: R.color.white)),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 30.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          buildRowPoint(
                              callback: () {
                                NavigationUtils.navigatePage(
                                    context, const GetPointInstructionPage());
                              },
                              icon: R.drawable.ic_diamond_history,
                              title: R.string.get_points_dci.tr()),
                          SizedBox(width: 10.w),
                          buildRowPoint(
                              callback: () {
                                NavigationUtils.rootNavigatePage(
                                        context,const DetailCartPage())
                                    .then((value) => _cubit.point);
                              },
                              icon: R.drawable.ic_voucher,
                              title: R.string.use_point_dci.tr()),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              _sliverSpace(12),
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Text(
                    R.string.history_points_dci.tr(),
                    style: Theme.of(context).textTheme.medium500.copyWith(
                          fontSize: 14.sp,
                          color: R.color.black,
                        ),
                  ),
                ),
              ),
              if (_cubit.listHistoryPoint.isEmpty &&
                  state is! HistoryPointLoading) ...[
                SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 40.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          R.string.you_do_not_have_any_dci_point.tr(),
                          style:
                              Theme.of(context).textTheme.regular400.copyWith(
                                    fontSize: 14.sp,
                                    color: R.color.black,
                                  ),
                        ),
                        InkWell(
                          onTap: () {
                            NavigationUtils.navigatePage(
                                context, const GetPointInstructionPage());
                          },
                          child: Text(
                            R.string.get_points_dci.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 13.sp,
                                    color: R.color.primaryColor,
                                    decoration: TextDecoration.underline),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
              if (_cubit.listHistoryPoint.isNotEmpty) ...[
                _sliverSpace(16),
                _tabBar(),
                _tabView(state)
              ]
            ],
          ),
        ),
      ),
    );
  }

  Widget _tabBar() {
    final textTheme =
        Theme.of(context).textTheme.medium500.copyWith(fontSize: 16.sp);
    return SliverPersistentHeader(
      pinned: true,
      floating: true,
      delegate: StickyTabBarDelegate(
        child: TabBar(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          indicatorColor: R.color.primaryColor,
          controller: _tabController,
          labelColor: R.color.primaryColor,
          labelPadding: EdgeInsets.only(bottom: 5.h),
          unselectedLabelColor: R.color.secondaryDarkGrey,
          labelStyle: textTheme,
          onTap: (_) {
            if (_tabController.previousIndex != _tabController.index)
              setState(() {});
          },
          indicatorSize: TabBarIndicatorSize.label,
          indicatorPadding: EdgeInsets.symmetric(horizontal: 15.w),
          tabs: [
            Text(R.string.point_received.tr()),
            Text(R.string.points_used.tr()),
          ],
        ),
      ),
    );
  }

  Widget _tabView(HistoryPointState state) {
    switch (_tabController.index) {
      case 0:
        return SliverToBoxAdapter(
          child: ListView.separated(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 5.h),
            physics: const NeverScrollableScrollPhysics(),
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                height: 1,
                color: R.color.secondaryDarkGrey,
                margin: EdgeInsets.symmetric(vertical: 5.h),
              );
            },
            itemCount: _cubit.listHistoryPoint.length,
            itemBuilder: (BuildContext context, int index) {
              CurrentUserData data = _cubit.listHistoryPoint[index];
              return buildHistory(
                  title: data.actionName,
                  date: data.createdDate,
                  point: data.point);
            },
          ),
        );
      case 1:
        return SliverToBoxAdapter(
            child: _cubit.listHistoryPointOut.isEmpty
                ? Visibility(
                    visible: state is! HistoryPointLoading,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 40.h),
                      child: Text(
                        R.string.no_history_of_points_used.tr(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.regular400.copyWith(
                              fontSize: 14.sp,
                              color: R.color.black,
                            ),
                      ),
                    ))
                : ListView.separated(
                    shrinkWrap: true,
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 5.h),
                    physics: const NeverScrollableScrollPhysics(),
                    separatorBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 1,
                        color: R.color.secondaryDarkGrey,
                        margin: EdgeInsets.symmetric(vertical: 5.h),
                      );
                    },
                    itemCount: _cubit.listHistoryPointOut.length,
                    itemBuilder: (BuildContext context, int index) {
                      CurrentUserData data = _cubit.listHistoryPointOut[index];
                      return buildHistory(
                          title: data.actionName,
                          date: data.createdDate,
                          point: data.point);
                    },
                  ));
      default:
        return SliverGrid.count(
          crossAxisCount: 2,
        );
    }
  }

  Widget buildHistory({
    String? title,
    int? date,
    int? point,
  }) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title ?? "",
                style: Theme.of(context).textTheme.bold700.copyWith(
                      fontSize: 14.sp,
                      color: R.color.black,
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              Row(
                children: [
                  Text(
                    DateUtil.parseDateToString(
                      DateTime.fromMillisecondsSinceEpoch(date ?? 0,
                          isUtc: false),
                      "dd-MM-yyyy",
                    ),
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 13.sp,
                        height: 24.h / 13.sp,
                        color: R.color.darkGrey),
                  ),
                  SizedBox(width: 20.w),
                  Text(
                    DateUtil.parseDateToString(
                        DateTime.fromMillisecondsSinceEpoch(date ?? 0,
                            isUtc: false),
                        "HH:mm"),
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 13.sp,
                        height: 24.h / 13.sp,
                        color: R.color.darkGrey),
                  ),
                ],
              ),
            ],
          ),
        ),
        Text(
          _tabController.index == 0 ? "+$point" : "$point",
          style: Theme.of(context).textTheme.bold700.copyWith(
                fontSize: 14.sp,
                color: _tabController.index == 0
                    ? R.color.primaryColor
                    : R.color.orange,
              ),
        ),
      ],
    );
  }

  Widget buildRowPoint({String? icon, String? title, VoidCallback? callback}) {
    return InkWell(
      highlightColor: R.color.white,
      onTap: callback,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 15.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(48), color: R.color.white),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              icon ?? "",
              height: 24.h,
              width: 24.h,
            ),
            SizedBox(width: 5.w),
            Text(
              title ?? "",
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontSize: 13.sp,
                  fontWeight: FontWeight.w500,
                  color: R.color.primaryColor,
                  height: 18.h / 13.sp),
            )
          ],
        ),
      ),
    );
  }

  Widget _sliverSpace(double height) {
    return SliverToBoxAdapter(
      child: SizedBox(height: height.h),
    );
  }
}

class StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar child;

  StickyTabBarDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: R.color.white,
      child: this.child,
    );
  }

  @override
  double get maxExtent => this.child.preferredSize.height;

  @override
  double get minExtent => this.child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
