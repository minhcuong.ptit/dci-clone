abstract class HistoryPointState {}

class HistoryPointInitial extends HistoryPointState {}

class HistoryPointLoading extends HistoryPointState {
  @override
  String toString() {
    return 'HistoryPointLoading{}';
  }
}

class HistoryPointFailure extends HistoryPointState {
  final String error;

  HistoryPointFailure(this.error);

  @override
  String toString() {
    return 'HistoryPointFailure{error: $error}';
  }
}

class HistoryPointSuccess extends HistoryPointState {
  @override
  String toString() {
    return 'HistoryPointSuccess{}';
  }
}

class ListHistoryPointEmpty extends HistoryPointState {
  @override
  String toString() {
    return 'ListHistoryPointEmpty{}';
  }
}

class ListHistoryPointOutEmpty extends HistoryPointState {
  @override
  String toString() {
    return 'ListHistoryPointOutEmpty{}';
  }
}
