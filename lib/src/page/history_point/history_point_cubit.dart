import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/page/history_point/history_point_state.dart';
import 'package:imi/src/utils/enum.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/history_point_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';

class HistoryPointCubit extends Cubit<HistoryPointState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  int get point => appPreferences.getInt(Const.POINT) ?? 0;
  String? nextToken;
  List<CurrentUserData> listHistoryPoint = [];
  List<CurrentUserData> listHistoryPointOut = [];
  int get communityId => appPreferences.getInt(Const.COMMUNITY_ID) ?? 0;

  HistoryPointCubit(this.repository, this.graphqlRepository)
      : super(HistoryPointInitial());

  void getListHistoryPoint({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh || !isLoadMore
        ? HistoryPointLoading()
        : HistoryPointInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listHistoryPoint.clear();
    }
    ApiResult<FetchPointHistory> getLibrary =
        await graphqlRepository.getHistoryPoint(
            nextToken: nextToken, actionTypePoint: ActionTypePoint.IN);
    getLibrary.when(success: (FetchPointHistory data) async {
      if (data.data?.length != 0) {
        listHistoryPoint.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(HistoryPointSuccess());
      } else {
        emit(ListHistoryPointEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HistoryPointFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListHistoryPointOut({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh || !isLoadMore
        ? HistoryPointLoading()
        : HistoryPointInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listHistoryPointOut.clear();
    }
    ApiResult<FetchPointHistory> getLibrary =
    await graphqlRepository.getHistoryPoint(
        nextToken: nextToken, actionTypePoint: ActionTypePoint.OUT);
    getLibrary.when(success: (FetchPointHistory data) async {
      if (data.data?.length != 0) {
        listHistoryPointOut.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(HistoryPointSuccess());
      } else {
        emit(ListHistoryPointOutEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HistoryPointFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
  void getProfile() async {
    emit(HistoryPointLoading());
    ApiResult<CommunityDataV2Response> getProfileTask =
    await graphqlRepository.getCommunityV2(communityId);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      appPreferences.saveCommunityV2(data.communityV2);
      emit(HistoryPointInitial());
    }, failure: (NetworkExceptions error) {
      emit(HistoryPointFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
