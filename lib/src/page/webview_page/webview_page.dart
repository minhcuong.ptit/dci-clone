import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

class WebviewPage extends StatefulWidget {
  final String title;
  final String? url;
  final String? html;

  WebviewPage({
    Key? key,
    required this.title,
    this.url,
    this.html,
  }) : super(key: key);

  @override
  State<WebviewPage> createState() => _WebviewPageState();
}

class _WebviewPageState extends State<WebviewPage> {
  InAppWebViewController? webViewController;

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  void initWebView() async {
    if (Platform.isAndroid) {
      await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    }
  }

  @override
  void initState() {
    initWebView();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    initWebView();
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
          backgroundColor: R.color.primaryColor,
          body: SafeArea(
            top: true,
            bottom: false,
            child: Container(
                color: R.color.primaryColor,
                height: double.infinity,
                child: Column(children: [
                  appBar(context, widget.title),
                  Expanded(child: contentWidget())
                ])),
          )),
    );
  }

  Widget contentWidget() {
    String? newUrl;
    if (!Utils.isEmpty(widget.url)) {
      newUrl = widget.url!;
      if (newUrl.toLowerCase().contains("pdf")) {
        if (Platform.isAndroid) {
          newUrl =
              "http://drive.google.com/viewerng/viewer?embedded=true&url=${widget.url}";
        }
      }
    }
    if (!Utils.isEmpty(widget.html)) {
      newUrl = Uri.dataFromString('<html><body>html</body></html>',
              mimeType: 'text/html')
          .toString();
    }
    if (Utils.isEmpty(newUrl)) {
      return Container(
        color: R.color.grey,
      );
    } else {
      return InAppWebView(
        initialData: InAppWebViewInitialData(data: widget.html!),
        initialOptions: options,
        onWebViewCreated: (controller) {
          webViewController = controller;
        },
        onLoadStart: (controller, url) {
          logger.i(url);
        },
        androidOnPermissionRequest: (controller, origin, resources) async {
          return PermissionRequestResponse(
              resources: resources,
              action: PermissionRequestResponseAction.GRANT);
        },
        shouldOverrideUrlLoading: (controller, navigationAction) async {
          var uri = navigationAction.request.url!;
          String path = uri.path;
          logger.i(path);

          if (![
            "http",
            "https",
            "file",
            "chrome",
            "data",
            "javascript",
            "about"
          ].contains(uri.scheme)) {
            Utils.launchURL(path);
            return NavigationActionPolicy.CANCEL;
          }
          return NavigationActionPolicy.ALLOW;
        },
        onLoadStop: (controller, url) async {
          logger.i(url);
          // https://debit-sandbox.faspay.co.id/pws/100003/0830000010100000/7ea636786fcd713515e505c72b391d1699d74c44/eyJ0cnhfaWQiOiIzNDM0NTgx
          if (url.toString().contains("pws") == true) {
            NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
          }
        },
        onLoadError: (controller, url, code, message) {
          logger.i(url, message);
        },
        onProgressChanged: (controller, progress) {
          if (progress < 1) {}
        },
        onUpdateVisitedHistory: (controller, url, androidIsReload) {
          logger.i(url);
        },
        onConsoleMessage: (controller, consoleMessage) {
          logger.i(consoleMessage);
        },
      );
    }
  }
}
