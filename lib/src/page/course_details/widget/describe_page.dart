import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';
import 'package:imi/src/page/course_details/course_details_cubit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:url_launcher/url_launcher.dart';

class DescriptionPage extends StatefulWidget {
  final int? courseId;

  const DescriptionPage({Key? key, this.courseId}) : super(key: key);

  @override
  State<DescriptionPage> createState() => _DescriptionPageState();
}

class _DescriptionPageState extends State<DescriptionPage> {
  late CourseDetailsCubit _cubit;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _cubit = BlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 20.h),
      physics: NeverScrollableScrollPhysics(),
      child: Column(
        children: [
          HtmlWidget(
            _cubit.courseDetailData?.description ?? "",
            factoryBuilder: () => MyWidgetFactory(),
            enableCaching: true,
            textStyle: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 12.sp),
            onTapUrl: (url) async {
              if (await canLaunch(url)) {
                await launch(
                  url,
                );
              } else {
                throw 'Could not launch $url';
              }
              return launch(
                url,
              );
            },
          ),
        ],
      ),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {}
