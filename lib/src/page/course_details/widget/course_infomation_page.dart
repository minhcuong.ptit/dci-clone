import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/course_details/course_details.dart';
import 'package:imi/src/utils/custom_theme.dart';

class CourseInfoPage extends StatefulWidget {
  final int? courseId;
  final bool? visible;

  const CourseInfoPage({Key? key, this.courseId, this.visible})
      : super(key: key);

  @override
  State<CourseInfoPage> createState() => _CourseInfoPageState();
}

class _CourseInfoPageState extends State<CourseInfoPage> {
  late CourseDetailsCubit _cubit;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _cubit = BlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 20.h),
        physics: NeverScrollableScrollPhysics(),
        child: Visibility(
          visible: widget.visible ?? false,
          child: Column(
            children: [
              buildTitle(context,
                  title: R.string.course_code.tr(args: [":"]),
                  description: _cubit.courseDetailData?.code ?? ""),
              _cubit.courseDetailData?.studyTime == ""
                  ? SizedBox()
                  : buildTitle(context,
                      title: R.string.study_time.tr(args: [":"]),
                      description: _cubit.courseDetailData?.studyTime ?? ""),
              buildTitle(context,
                  title: R.string.lecturers.tr(args: [":"]),
                  description: _cubit.courseDetailData?.teacher ?? ""),
              buildTitle(context,
                  title: R.string.study_form.tr(args: [":"]),
                  description: _cubit.courseDetailData?.studyForm ?? ""),
            ],
          ),
        ));
  }

  Widget buildTitle(BuildContext context,
      {String? title, String? description}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 2,
          child: Text(
            title ?? "",
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 12.sp),
            maxLines: 1,overflow: TextOverflow.ellipsis,
          ),
        ),
        Expanded(
            flex: 3,
            child: Text(
             description?? "",
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12.sp, color: R.color.primaryColor),
              maxLines: 2,
            )),
      ],
    );
  }
}
