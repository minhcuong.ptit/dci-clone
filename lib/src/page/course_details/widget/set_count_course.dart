import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';

class SetCountCourseWidget extends StatelessWidget {
  final VoidCallback decreaseCount;
  final VoidCallback increaseCount;
  final Function(String)? onChanged;
  final String? title;
  final bool checkColor;
  final TextEditingController? controller;

  // final VoidCallback? onChange;

  SetCountCourseWidget({
    required this.decreaseCount,
    this.onChanged,
    required this.increaseCount,
    this.title,
    required this.checkColor,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24.h,
      alignment: Alignment.center,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(2),
        border: Border.all(
          width: 1,
          color: R.color.lightGray,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
              width: 24.w,
              height: 24.h,
              child: InkWell(
                onTap: decreaseCount,
                child: Icon(
                  Icons.remove,
                  size: 20.h,
                  color: checkColor ? R.color.primaryColor : R.color.lightGray,
                ),
              )),
          Container(
            height: 24.h,
            width: 1,
            color: R.color.lightGray,
          ),
          Container(
            width: 40.w,
            //height: 24.h,
            alignment: Alignment.center,
            child: TextField(
              textAlign: TextAlign.center,
              textAlignVertical: TextAlignVertical.center,
              controller: controller,
              onChanged: onChanged,
              maxLines: 1,
              keyboardType: TextInputType.number,
              cursorColor: R.color.primaryColor,
              inputFormatters: [LengthLimitingTextInputFormatter(3)],
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsetsDirectional.fromSTEB(0, 8, 0, 8),
                border: InputBorder.none,
                hintText: title,
                hintStyle: Theme.of(context).textTheme.regular400.copyWith(
                    fontSize: 14,
                    height: 24 / 14,
                    color: R.color.primaryColor),
              ),
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontSize: 14,
                  height: 24 / 14,
                  color: R.color.primaryColor),
            ),
          ),
          Container(
            height: 24.h,
            width: 1,
            color: R.color.lightGray,
          ),
          SizedBox(
              width: 24.w,
              height: 24.h,
              child: InkWell(
                onTap: increaseCount,
                child: Icon(
                  Icons.add,
                  size: 20.h,
                  color: R.color.primaryColor,
                ),
              )),
        ],
      ),
    );
  }
}
