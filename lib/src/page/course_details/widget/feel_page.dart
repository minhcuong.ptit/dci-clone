import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/course_details/course_details_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';

class FeelPage extends StatefulWidget {
  final int? courseId;

  const FeelPage({Key? key, this.courseId}) : super(key: key);

  @override
  State<FeelPage> createState() => _FeelPageState();
}

class _FeelPageState extends State<FeelPage> {
  late CourseDetailsCubit _cubit;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _cubit = BlocProvider.of(context);
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: _cubit.courseDetailData?.testimonials?.length == 0
            ? Center(
                child: Text(
                R.string.no_comments_yet.tr(args: ["!"]),
                style: Theme.of(context).textTheme.regular400,
              ))
            : Column(
                children: [
                  CarouselSlider(
                    options: CarouselOptions(
                      height: 317.h,
                      initialPage: 0,
                      enlargeCenterPage: true,
                      autoPlay: false,
                      reverse: false,
                      enableInfiniteScroll: true,
                      autoPlayInterval: Duration(seconds: 2),
                      autoPlayAnimationDuration: Duration(milliseconds: 2000),
                      onPageChanged: (index, reason) {
                        setState(() {
                          _current = index;
                        });
                      },
                    ),
                    items: List.generate(
                        _cubit.courseDetailData?.testimonials?.length ?? 0,
                        (index) {
                      return Stack(
                        children: [
                          Container(
                            width: 245.h,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12.h),
                              border: Border.all(color: R.color.grey),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8.h, horizontal: 12.h),
                                  child: Image.asset(
                                    R.drawable.ic_characters,
                                    height: 24.h,
                                    width: 28.w,
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 18.h),
                                    child: Text(
                                      _cubit
                                              .courseDetailData
                                              ?.testimonials?[index]
                                              .description ??
                                          "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .regular400
                                          .copyWith(fontSize: 12.sp),
                                      maxLines: 5,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ),
                                Image.asset(
                                  R.drawable.background_course,
                                )
                              ],
                            ),
                          ),
                          Positioned(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ClipRRect(
                                  child: CachedNetworkImage(
                                    fit: BoxFit.cover,
                                    height: 70.h,
                                    width: 70.h,
                                    imageUrl: _cubit.courseDetailData
                                            ?.testimonials?[index].avatarUrl ??
                                        "",
                                  ),
                                  borderRadius: BorderRadius.circular(200.h),
                                ),
                                Text(
                                  _cubit.courseDetailData?.testimonials?[index]
                                          .fullName ??
                                      "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .medium500
                                      .copyWith(
                                          fontSize: 12.sp,
                                          color: R.color.white,
                                          height: 18 / 12),
                                ),
                                Text(
                                  _cubit.courseDetailData?.testimonials?[index]
                                          .title ??
                                      "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .medium500
                                      .copyWith(
                                          fontSize: 12.sp,
                                          color: R.color.white,
                                          height: 18 / 12),
                                ),
                              ],
                            ),
                            left: 0.h,
                            right: 0.h,
                            bottom: 8.h,
                          )
                        ],
                      );
                    }),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:
                        map<Widget>(_cubit.courseDetailData?.testimonials ?? [],
                            (index, url) {
                      return Container(
                        width: 10.0,
                        height: 10.0,
                        margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 2.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color:
                              _current == index ? R.color.blue : R.color.grey,
                        ),
                      );
                    }),
                  ),
                ],
              ));
  }
}
