import 'package:equatable/equatable.dart';

abstract class CourseDetailsState {}

class CourseDetailsInitial extends CourseDetailsState {}

class CourseDetailsTextChangeInitial extends CourseDetailsState {}

class CourseDetailsNumberChangeInitial extends CourseDetailsState {}

class CourseDetailsLoading extends CourseDetailsState {
  @override
  String toString() {
    return 'CourseDetailsLoading{}';
  }
}

class CourseDetailsFailure extends CourseDetailsState {
  final String error;

  CourseDetailsFailure(this.error);

  @override
  String toString() {
    return 'CourseDetailsFailure{error: $error}';
  }
}

class CourseDetailsSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'CourseDetailsSuccess{}';
  }
}

class ListCourseSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'ListCourseSuccess{}';
  }
}

class ListCourseEmpty extends CourseDetailsState {
  @override
  String toString() {
    return 'ListCourseEmpty{}';
  }
}

class CourseDetailErrorSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'CourseDetailErrorSuccess{}';
  }
}

class OrderCourseSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'OrderCourseSuccess{}';
  }
}

class BuyNowSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'BuyNowSuccess{}';
  }
}

class GetOrderCourseSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'GetOrderCourseSuccess{}';
  }
}

class GetPaymentSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'GetPaymentSuccess{}';
  }
}
class ValidateError extends CourseDetailsState {
  @override
  String toString() {
    return 'ValidateError {}';
  }
}

class GetProvinceSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'GetProvinceSuccess {}';
  }
}

class SetProvinceSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'SetProvinceSuccess {}';
  }
}

class EmptyProvince extends CourseDetailsState {
  @override
  String toString() {
    return 'EmptyProvince {}';
  }
}

class ListCourseSimilarSuccess extends CourseDetailsState {
  @override
  String toString() {
    return 'ListCourseSimilarSuccess{}';
  }
}

class ListCourseSimilarEmpty extends CourseDetailsState {
  @override
  String toString() {
    return 'ListCourseSimilarEmpty{}';
  }
}