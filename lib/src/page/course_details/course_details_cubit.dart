import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/course_detail_response.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/deatil_cart_response.dart';
import 'package:imi/src/data/network/response/detail_order_product.dart';
import 'package:imi/src/data/network/response/order_course_response.dart';
import 'package:imi/src/data/network/response/payment_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/course_details/course_details.dart';
import 'package:imi/src/page/course_details/course_details_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../res/R.dart';
import '../../data/network/request/event_free_request.dart';
import '../../data/network/response/city_response.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/utils.dart';

class CourseDetailsCubit extends Cubit<CourseDetailsState> with Validators {
  final int courseId;
  final int productId;
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  CourseDetailData? courseDetailData;
  List<FetchCoursesData> listCourse = [];
  List<FetchCoursesData> listCourseSimilar = [];
  String? nextToken;
  String? nextTokenSimilar;
  FetchItemByProductIdV2? fetchItemByProductIdV2;
  int count = 1;
  bool isCheckNote = false;
  int lengthText = 0;
  DetailCartData? cartData;
  bool? hideButtonCart;
  List<FetchPaymentTypes> listPayment = [];
  String? errorName;
  String? errorEmail;
  String? errorPhoneNumber;

  List<CityData> listProvince = [];
  CityData? selectedProvince;
  String? errorProvince;

  String? get name => appPreferences.getString(Const.FULL_NAME);

  String? get phone => appPreferences.getString(Const.PHONE);

  String? get email => appPreferences.getString(Const.EMAIL);

  String? get province => appPreferences.getString(Const.PROVINCE);

  int? get provinceId => appPreferences.getInt(Const.PROVINCE_ID);

  String? codeInvalid;
  String? cityName;

  int? cityId;

  CourseDetailsCubit(
      this.repository, this.graphqlRepository, this.courseId, this.productId)
      : super(CourseDetailsInitial());

  void getCourseDetail({bool isRefresh = false}) async {
    emit((isRefresh) ? CourseDetailsInitial() : CourseDetailsLoading());
    ApiResult<CourseDetailData> getCourseDetail =
        await graphqlRepository.getCourseDetail(courseId: courseId);
    getCourseDetail.when(success: (CourseDetailData data) {
      courseDetailData = data;
      emit(CourseDetailsSuccess());
      getListCourseSimilar(isRefresh: true);
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest && error.code == ServerError.course_is_not_open) {
        emit(CourseDetailErrorSuccess());
        return;
      }
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListCourse({bool isRefresh = false}) async {
    emit(!isRefresh ? CourseDetailsLoading() : CourseDetailsInitial());
    if (isRefresh) {
      nextToken = null;
      listCourse.clear();
    }
    ApiResult<FetchCourse> getLibrary = await graphqlRepository.getCourse(
        nextToken: nextToken,
        limit: 4,
        priorityOrder: Order.DESC.name,
        courseId: courseId,
        isHot: true);
    getLibrary.when(success: (FetchCourse data) async {
      if (data.data != null) {
        listCourse.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(ListCourseSuccess());
      } else {
        emit(ListCourseEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListCourseSimilar({bool isRefresh = false}) async {
    emit(!isRefresh ? CourseDetailsLoading() : CourseDetailsInitial());
    if (isRefresh) {
      nextTokenSimilar = null;
      listCourseSimilar.clear();
    }
    ApiResult<FetchCourse> getLibrary = await graphqlRepository.getCourse(
        nextToken: nextTokenSimilar,
        limit: 4,
        priorityOrder: Order.DESC.name,
        courseId: courseId,
        topicIds: courseDetailData?.productTopicIds);
    getLibrary.when(success: (FetchCourse data) async {
      if (data.data != null) {
        listCourseSimilar.addAll(data.data ?? []);
        nextTokenSimilar = data.nextToken;
        emit(ListCourseSimilarSuccess());
      } else {
        emit(ListCourseSimilarEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void increaseCount(int index) {
    emit(CourseDetailsLoading());
    this.count = index + 1;
    emit(CourseDetailsSuccess());
  }

  void decreaseCount(int index) {
    emit(CourseDetailsLoading());
    this.count = index - 1;
    emit(CourseDetailsSuccess());
  }

  void getItemOrderCourse() async {
    emit(CourseDetailsLoading());
    ApiResult<FetchItemByProductIdV2> result =
        await graphqlRepository.getFetchDetailOrder(productId: productId);
    result.when(success: (FetchItemByProductIdV2 data) async {
      fetchItemByProductIdV2 = data;
      emit(GetOrderCourseSuccess());
      getProvince();
    }, failure: (NetworkExceptions error) async {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void orderCourse(
    int? itemId,
    String note,
    String number,
    String? userName,
    String? phone,
    String? email,
    bool type,
  ) async {
    errorName = checkName(userName);
    errorPhoneNumber = checkPhoneNumber(phone);
    errorEmail = checkEmail(email);
    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorPhoneNumber)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (Utils.isEmpty(selectedProvince) &&
        fetchItemByProductIdV2?.userInformation?.provinceId == null &&
        province == null) {
      errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      emit(EmptyProvince());
      return;
    } else {
      errorProvince = null;
    }
    int quantity = fetchItemByProductIdV2?.quantity ?? 0;
    emit(CourseDetailsLoading());
    ApiResult<dynamic> result = await repository.orderCourse(EventFreeRequest(
        quantity: number != "" ? int.parse(number ?? "") : count,
        itemId: itemId,
        productType: "EVENT",
        userName: userName ?? name,
        phone: phone ?? phone,
        email: email ?? email,
        note: note,
        provinceId: selectedProvince?.cityId ??
            (fetchItemByProductIdV2?.userInformation?.provinceId != null
                ? cityId
                : provinceId)));
    result.when(success: (data) {
      if (type == true) {
        emit(OrderCourseSuccess());
      } else if (type == false) {
        emit(BuyNowSuccess());
      }
    }, failure: (e) {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getDetailCart() async {
    emit(CourseDetailsLoading());
    ApiResult<DetailCartData> result = await graphqlRepository.getDetailCart();
    result.when(success: (DetailCartData data) {
      cartData = data;
      emit(CourseDetailsInitial());
    }, failure: (e) {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getPayment({bool isRefresh = false}) async {
    emit((isRefresh) ? CourseDetailsInitial() : CourseDetailsLoading());
    final res = await graphqlRepository.getPayment();
    res.when(success: (List<FetchPaymentTypes> data) {
      listPayment = data ?? [];
      for (int i = 0; i < listPayment.length; i++) {
        if (listPayment[i].status == CommonStatus.ACTIVE.name) {
          hideButtonCart = true;
        }
      }
      emit(GetPaymentSuccess());
    }, failure: (e) {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void refreshPage() {
    emit(CourseDetailsLoading());
    getCourseDetail();
    getDetailCart();
    emit(CourseDetailsInitial());
  }

  void changeText(int index) {
    this.lengthText = index;
    emit(CourseDetailsTextChangeInitial());
  }

  void validateEmail(String? email) {
    errorEmail = checkEmail(email?.trim());
    emit(ValidateError());
  }

  void validateName(String? name) {
    errorName = checkName(name?.trim());
    emit(ValidateError());
  }

  void validatePhone(String? phone) {
    if (Utils.isEmpty(phone)) {
      errorPhoneNumber = R.string.please_enter_phone_number.tr();
    } else {
      errorPhoneNumber = checkPhoneNumber(phone);
    }
    emit(ValidateError());
  }

  void getProvince() async {
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListProvince();
    apiResult.when(success: (List<CityData> data) async {
      listProvince = data;
      listProvince.forEach((element) {
        if (element.cityId ==
            fetchItemByProductIdV2?.userInformation?.provinceId) {
          cityName = element.cityName;
          cityId = element.cityId;
        }
      });
      emit(GetProvinceSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CourseDetailsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectProvince(CityData? city) {
    if (selectedProvince != city) {
      emit(CourseDetailsLoading());
      this.selectedProvince = city;
      if (Utils.isEmpty(selectedProvince) &&
          Utils.isEmpty(province) &&
          fetchItemByProductIdV2?.userInformation?.provinceId == null) {
        errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      } else {
        errorProvince = null;
      }
      emit(SetProvinceSuccess());
    }
  }
}
