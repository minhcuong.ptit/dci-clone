import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/course_details/course_details_cubit.dart';
import 'package:imi/src/page/course_details/course_details_state.dart';
import 'package:imi/src/page/course_details/widget/course_infomation_page.dart';
import 'package:imi/src/page/course_details/widget/describe_page.dart';
import 'package:imi/src/page/course_details/widget/feel_page.dart';
import 'package:imi/src/page/course_details/widget/set_count_course.dart';
import 'package:imi/src/page/detail_cart/detail_cart_page.dart';
import 'package:imi/src/page/list_course/list_course_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../widgets/banner_product.dart';
import '../../widgets/dropdown_column_widget.dart';
import '../../widgets/search_list_data_popup.dart';
import '../detail_event/widget/event_highlight.dart';
import '../detail_event/widget/infor_event_order.dart';

class CourseDetailsPage extends StatefulWidget {
  final int? courseId;
  final int? productId;

  const CourseDetailsPage({Key? key, this.courseId, this.productId})
      : super(key: key);

  @override
  State<CourseDetailsPage> createState() => _CourseDetailsPageState();
}

class _CourseDetailsPageState extends State<CourseDetailsPage>
    with TickerProviderStateMixin {
  late CourseDetailsCubit _cubit;
  late TabController _tabController;
  final RefreshController _refreshController = RefreshController();
  TextEditingController _editingController = TextEditingController();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  FocusNode _focusNode = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();
  int length = 0;

  @override
  void initState() {
    // TODO: implement initState
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    super.initState();
    _cubit = CourseDetailsCubit(repository, graphqlRepository,
        widget.courseId ?? 0, widget.productId ?? 0);
    _tabController = new TabController(vsync: this, length: 3);
    _cubit.getCourseDetail();
    _cubit.getListCourse();
    _cubit.getItemOrderCourse();
    _cubit.getDetailCart();
    _cubit.getPayment();
    _nameController.text = _cubit.name ?? "";
    _phoneController.text = _cubit.phone ?? "";
    _emailController.text = _cubit.email ?? "";
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _focusNode.dispose();
    _phoneController.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _numberController.dispose();
    _emailFocus.dispose();
    _phoneFocus.dispose();
    _nameFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<CourseDetailsCubit, CourseDetailsState>(
        listener: (BuildContext context, state) {
          if (state is! CourseDetailsLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
            _cubit.lengthText = _editingController.text.trim().length;
          }
          if (state is OrderCourseSuccess) {
            NavigationUtils.pop(context);
            _cubit.count = 1;
            _cubit.refreshPage();
            showDialog(
                barrierColor: R.color.white.withOpacity(0),
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return AlertDialog(
                    backgroundColor: R.color.darkGrey,
                    contentPadding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    content: Container(
                      height: 80.h,
                      width: 80.w,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            CupertinoIcons.check_mark_circled_solid,
                            size: 21.h,
                            color: R.color.white,
                          ),
                          SizedBox(height: 10.h),
                          Text(
                            R.string.added_to_cart.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 11.sp, color: R.color.white),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                  );
                });
            Future.delayed(Duration(seconds: 2), () {
              NavigationUtils.pop(context);
            });
          }
          if (state is GetOrderCourseSuccess) {
            _editingController.text = _cubit.fetchItemByProductIdV2?.note ?? "";
          }
          if (state is BuyNowSuccess) {
            NavigationUtils.pop(context);
            _cubit.count = 1;
            NavigationUtils.navigatePage(context, DetailCartPage())
                .then((value) => _cubit.refreshPage());
          }
          if (state is GetOrderCourseSuccess) {
            _editingController.text = _cubit.fetchItemByProductIdV2?.note ?? "";
            _nameController.text =
                _cubit.fetchItemByProductIdV2?.userInformation?.userName ??
                    _cubit.name ??
                    "";
            _phoneController.text =
                _cubit.fetchItemByProductIdV2?.userInformation?.phone ??
                    _cubit.phone ??
                    "";
            _emailController.text =
                _cubit.fetchItemByProductIdV2?.userInformation?.email ??
                    _cubit.email ??
                    "";
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            backgroundColor: R.color.grey100,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              elevation: 0,
              leading: InkWell(
                onTap: () {
                  NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                },
                child: Icon(
                  CupertinoIcons.back,
                  color: R.color.black,
                ),
              ),
              backgroundColor: R.color.grey100,
              actions: [
                InkWell(
                  onTap: () {
                    NavigationUtils.rootNavigatePage(context, DetailCartPage())
                        .then((value) => _cubit.refreshPage());
                  },
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 16.h, right: 20.h),
                        child: Icon(
                          CupertinoIcons.shopping_cart,
                          size: 22.h,
                          color: R.color.black,
                        ),
                      ),
                      if (_cubit.cartData?.fetchMyOrder?.total != null)
                        Visibility(
                          visible: _cubit.cartData!.fetchMyOrder!.total! > 0,
                          child: Positioned(
                            left: _cubit.cartData!.fetchMyOrder!.total! > 9
                                ? 18
                                : 16,
                            top: 13,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      _cubit.cartData!.fetchMyOrder!.total! > 9
                                          ? 2
                                          : 4,
                                  vertical: 0.5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.h),
                                  color: R.color.red),
                              child: Text(
                                _cubit.cartData!.fetchMyOrder!.total! > 100
                                    ? "${99}+"
                                    : "${_cubit.cartData?.fetchMyOrder?.total}",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 9.sp,
                                        color: R.color.white,
                                        height: 14.h / 11.sp),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        )
                    ],
                  ),
                )
              ],
            ),
            body: StackLoadingView(
              visibleLoading: state is CourseDetailsLoading,
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () {
                  _cubit.getCourseDetail(isRefresh: true);
                  _cubit.getListCourse(isRefresh: true);
                  _numberController.clear();
                  _cubit.count = 1;
                  _cubit.getPayment(isRefresh: true);
                  _nameController.text = _cubit.fetchItemByProductIdV2
                              ?.userInformation?.userName !=
                          null
                      ? _cubit.fetchItemByProductIdV2?.userInformation
                              ?.userName ??
                          ""
                      : _cubit.name ?? "";
                  _phoneController.text = _cubit
                              .fetchItemByProductIdV2?.userInformation?.phone !=
                          null
                      ? _cubit.fetchItemByProductIdV2?.userInformation?.phone ??
                          ""
                      : _cubit.phone ?? "";
                  _emailController.text = _cubit
                              .fetchItemByProductIdV2?.userInformation?.email !=
                          null
                      ? _cubit.fetchItemByProductIdV2?.userInformation?.email ??
                          ""
                      : _cubit.email ?? "";
                  _numberController.clear();
                  _editingController.text =
                      _cubit.fetchItemByProductIdV2?.note != null
                          ? _cubit.fetchItemByProductIdV2?.note ?? ""
                          : "";
                  _cubit.selectedProvince?.cityName = (_cubit
                              .fetchItemByProductIdV2
                              ?.userInformation
                              ?.provinceId !=
                          null
                      ? _cubit.cityName
                      : _cubit.province);
                  _cubit.validateName(_nameController.text);
                  _cubit.validateEmail(_emailController.text);
                  _cubit.validatePhone(_phoneController.text);
                },
                child: CustomScrollView(
                  shrinkWrap: true,
                  slivers: [
                    state is CourseDetailErrorSuccess
                        ? SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.only(top: 30.h, bottom: 10.h),
                              child: Text(
                                R.string.this_content_is_no_longer_active.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 14.sp),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )
                        : SliverToBoxAdapter(
                            child: Visibility(
                              visible: state is! CourseDetailsLoading,
                              child: BannerProduct(
                                imageUrl:
                                    _cubit.courseDetailData?.avatarUrl ?? "",
                                isHot: _cubit.courseDetailData?.isHot == true &&
                                    state is! CourseDetailsLoading,
                                hasStartDate:
                                    _cubit.courseDetailData?.startDate != null,
                                startDate:
                                    _cubit.courseDetailData?.startDate ?? 0,
                              ),
                            ),
                          ),
                    _sliverSpace(8.h),
                    state is CourseDetailErrorSuccess
                        ? _sliverSpace(0)
                        : SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.h),
                              child: Text(
                                _cubit.courseDetailData?.name ?? "",
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(fontSize: 16.sp),
                              ),
                            ),
                          ),
                    _sliverSpace(4.h),
                    state is CourseDetailErrorSuccess
                        ? _sliverSpace(4.h)
                        : SliverToBoxAdapter(
                            child: Visibility(
                            visible: state is! CourseDetailsLoading,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.h),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${Utils.formatMoney(_cubit.courseDetailData?.currentFee ?? 0)} VND",
                                        style: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(
                                            fontSize: _cubit.courseDetailData
                                                ?.preferentialFee ==
                                                _cubit
                                                    .courseDetailData?.currentFee
                                                ? 16.sp
                                                : 12.sp,
                                            fontWeight: _cubit.courseDetailData
                                                ?.preferentialFee ==
                                                _cubit
                                                    .courseDetailData?.currentFee
                                                ? FontWeight.w700
                                                : FontWeight.w400,
                                            decoration: _cubit.courseDetailData
                                                ?.preferentialFee ==
                                                _cubit
                                                    .courseDetailData?.currentFee
                                                ? TextDecoration.none
                                                : TextDecoration.lineThrough),
                                      ),
                                      SizedBox(width: 8.h),
                                      Visibility(
                                        visible:
                                        _cubit.courseDetailData?.preferentialFee !=
                                            _cubit.courseDetailData?.currentFee,
                                        child: Text(
                                            "${Utils.formatMoney(_cubit.courseDetailData?.preferentialFee ?? 0)} VND",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bold700
                                                .copyWith(
                                                fontSize: 16.sp,
                                                color: R.color.black,
                                                height: 24.h / 16.sp)),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                      child: SizedBox(
                                    width: 10,
                                  )),
                                  Visibility(
                                    visible: state is! CourseDetailsLoading &&
                                        _cubit.hideButtonCart == true,
                                    child: InkWell(
                                      onTap: () {
                                        showMaterialModalBottomSheet(
                                          context: context,
                                          backgroundColor: Colors.transparent,
                                          builder: (context) =>
                                              BlocProvider.value(
                                            child:
                                                buildAddToCart(context, state),
                                            value: _cubit,
                                          ),
                                        );
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(bottom: 6),
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            15.w, 4.h, 15.w, 4.h),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(48.h),
                                          color: R.color.primaryColor,
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              CupertinoIcons.shopping_cart,
                                              color: R.color.white,
                                              size: 13.h,
                                            ),
                                            SizedBox(width: 4.w),
                                            Padding(
                                              padding: EdgeInsetsDirectional
                                                  .fromSTEB(0.w, 0.h, 0.w, 4.h),
                                              child: Text(
                                                  R.string.add_to_cart.tr(),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .regular400
                                                      .copyWith(
                                                          fontSize: 10.sp,
                                                          color:
                                                              R.color.white)),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )),
                    _sliverSpace(4.h),
                    state is CourseDetailErrorSuccess
                        ? _sliverSpace(0)
                        : SliverPersistentHeader(
                            pinned: true,
                            floating: true,
                            delegate: StickyTabBarDelegate(
                              state is! CourseDetailsLoading,
                              child: TabBar(
                                indicatorPadding:
                                    EdgeInsets.symmetric(horizontal: 5.h),
                                padding: EdgeInsets.symmetric(horizontal: 60.h),
                                isScrollable: true,
                                labelStyle: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                      fontSize: 14.sp,
                                    ),
                                labelColor: R.color.darkerBlue,
                                unselectedLabelColor: R.color.grey,
                                indicatorColor: R.color.lightBlue,
                                controller: _tabController,
                                onTap: (index) {
                                  if (_tabController.previousIndex !=
                                      _tabController.index) {
                                     setState(() {});
                                    //logger.e('index $index');
                                  }
                                },
                                indicatorSize: TabBarIndicatorSize.label,
                                tabs: [
                                  Tab(
                                    text: R.string.course_information.tr(),
                                  ),
                                  Tab(
                                    text: R.string.describe.tr(),
                                  ),
                                  Tab(
                                    text: R.string.feeling.tr(),
                                  ),
                                ],
                              ),
                            ),
                          ),
                    _sliverSpace(10),
                    state is CourseDetailErrorSuccess
                        ? _sliverSpace(0)
                        : tabView(state),
                    if (_cubit.listCourseSimilar.length != 0 &&
                        _cubit.courseDetailData?.productTopicIds != null &&
                        state is! CourseDetailsLoading) ...[
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 1,
                                color: R.color.grey300,
                              ),
                              SizedBox(height: 10.h),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    R.string.similar_course.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(
                                            fontSize: 16.sp,
                                            color: R.color.black),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      NavigationUtils.navigatePage(
                                          context,
                                          ListCoursePage(
                                            selected: 5,
                                            topicSearch: _cubit.courseDetailData
                                                ?.productTopicIds,
                                          ));
                                    },
                                    child: Text(R.string.read_more.tr(),
                                        style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                          color: R.color.readMore,
                                        )),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      buildSimilarCourse(context),
                    ],
                    if (state is! CourseDetailsLoading) ...[
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (_cubit.listCourseSimilar.length != 0 &&
                                  _cubit.courseDetailData?.productTopicIds ==
                                      null) ...[
                                Container(
                                  height: 1,
                                  color: R.color.grey300,
                                ),
                                SizedBox(height: 10.h),
                              ],
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    R.string.featured_course.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(
                                            fontSize: 16.sp,
                                            color: R.color.black),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      NavigationUtils.navigatePage(
                                          context, ListCoursePage(selected: 4));
                                    },
                                    child: Text(R.string.read_more.tr(),
                                        style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                          color: R.color.readMore,
                                        )),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      buildFeaturedCourse(context),
                      _sliverSpace(20)
                    ]
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildAddToCart(BuildContext context, CourseDetailsState state) {
    return BlocBuilder<CourseDetailsCubit, CourseDetailsState>(
      buildWhen: (previous, next) =>
          next is CourseDetailsTextChangeInitial ||
          next is CourseDetailsNumberChangeInitial ||
          next is SetProvinceSuccess ||
          next is GetProvinceSuccess ||
          next is EmptyProvince ||
          next is ValidateError,
      builder: (context, state) {
        final _cubit = BlocProvider.of<CourseDetailsCubit>(context);
        return Container(
          margin: EdgeInsets.only(top: 100),
          child: SingleChildScrollView(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: EdgeInsets.all(20.h),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.h),
                  topRight: Radius.circular(24.h),
                ),
                color: R.color.white,
              ),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(4.h),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              height: 95.h,
                              width: 118.w,
                              imageUrl:
                                  _cubit.courseDetailData?.avatarUrl ?? "",
                              placeholder: (_, __) {
                                return Text(
                                  R.string.loading.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 14.sp),
                                );
                              },
                            ),
                          ),
                          SizedBox(width: 8.w),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10.h),
                                Text(
                                  _cubit.courseDetailData?.name ?? "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(
                                          fontSize: 14.sp, height: 24 / 14),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Visibility(
                                      visible: _cubit.courseDetailData?.currentFee!=_cubit.courseDetailData?.preferentialFee,
                                      child: Expanded(
                                        child: Text(
                                          "${Utils.formatMoney(_cubit.courseDetailData?.currentFee ?? 0)} VND",
                                          style: Theme.of(context)
                                              .textTheme
                                              .regular400
                                              .copyWith(
                                                  fontSize: 12.sp,
                                                  decoration:
                                                      TextDecoration.lineThrough,
                                                  height: 18 / 12),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 8.h),
                                    Text(
                                      "${Utils.formatMoney(_cubit.courseDetailData?.preferentialFee ?? 0)} VND",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bold700
                                          .copyWith(
                                              fontSize: 16.sp,
                                              color: R.color.red,
                                              height: 24 / 16),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8.h),
                      _cubit.courseDetailData?.startDate == null
                          ? SizedBox()
                          : Container(
                              margin: EdgeInsets.symmetric(horizontal: 30.w),
                              padding: EdgeInsets.symmetric(
                                  vertical: 4.h, horizontal: 12.h),
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: R.color.shadowColor,
                                      blurRadius: 15,
                                      offset: Offset(0.0, 5))
                                ],
                                borderRadius: BorderRadius.circular(24.h),
                                color: R.color.white,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    CupertinoIcons.clock,
                                    size: 20.h,
                                    color: R.color.green,
                                  ),
                                  SizedBox(width: 4.h),
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.fromSTEB(
                                          0, 0, 0, 3),
                                      child: Text(
                                        R.string.opening_ceremony_on
                                                .tr()
                                                .toUpperCase() +
                                            " " +
                                            DateUtil.parseDateToString(
                                                DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                        _cubit.courseDetailData
                                                                ?.startDate ??
                                                            0,
                                                        isUtc: false),
                                                "HH:mm dd/MM/yyyy"),
                                        style: Theme.of(context)
                                            .textTheme
                                            .medium500,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                      SizedBox(height: 15.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            R.string.amount.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(fontSize: 12.sp, color: R.color.gray),
                          ),
                          StatefulBuilder(
                            builder: (BuildContext context,
                                void Function(void Function()) setState) {
                              return SetCountCourseWidget(
                                onChanged: (text) {
                                  setState(() {
                                    _cubit.count = 0;
                                  });
                                },
                                controller: _numberController,
                                decreaseCount: () {
                                  setState(() {
                                    if (_cubit.count > 1) {
                                      _cubit.decreaseCount(_cubit.count);
                                    }
                                  });
                                },
                                increaseCount: () {
                                  setState(() {
                                    _cubit.increaseCount(_cubit.count);
                                  });
                                },
                                title: "${_cubit.count}",
                                checkColor: _cubit.count > 1,
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Center(
                        child: Text(
                          R.string.contact_info.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 14.sp,
                                  height: 21.h / 14.sp,
                                  color: R.color.grey),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.first_and_last_name.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterName(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.phone_number.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterPhone(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.email.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterEmail(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.area.tr()),
                      _buildDropdownProvince(context, state),
                      SizedBox(height: 10.h),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            R.string.notes_for_dci.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 12.sp,
                                    color: R.color.gray,
                                    height: 18 / 12),
                          ),
                          SizedBox(width: 4.w),
                          Tooltip(
                              message: R
                                  .string.if_you_are_note_studying_in_person
                                  .tr(),
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                    fontSize: 11.sp,
                                    color: R.color.primaryColor,
                                  ),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 12.w, vertical: 6.h),
                              margin: EdgeInsetsDirectional.fromSTEB(
                                  100.w, 0, 30.w, 0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.h),
                                  color: R.color.white),
                              preferBelow: false,
                              showDuration: Duration(hours: 1),
                              triggerMode: TooltipTriggerMode.tap,
                              child: Icon(CupertinoIcons.info_circle,
                                  size: 16.h, color: R.color.primaryColor)),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Container(
                          height: 200.h,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.h),
                              border:
                                  Border.all(width: 1, color: R.color.gray)),
                          child:
                              SingleChildScrollView(child: buildNote(context))),
                      SizedBox(height: 4.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: "${_cubit.lengthText}",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, color: R.color.gray),
                              children: <TextSpan>[
                                TextSpan(
                                    text: ' / 500',
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: R.color.gray)),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ButtonWidget(
                              backgroundColor: R.color.orange,
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.w, 4.h, 5.w, 10.h),
                              title: R.string.add_and_view_cart.tr(),
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 10.sp,
                                      color: R.color.white,
                                      height: 20.h / 10.sp),
                              onPressed: () {
                                if (_numberController.text != "0" &&
                                    _numberController.text != "00" &&
                                    _numberController.text != "000") {
                                  _cubit.orderCourse(
                                      widget.productId,
                                      _editingController.text.trim(),
                                      _numberController.text.trim(),
                                      _nameController.text.trim(),
                                      _phoneController.text.trim(),
                                      _emailController.text.trim(),
                                      false);
                                  //NavigationUtils.pop(context);
                                } else {
                                  NavigationUtils.pop(context);
                                  Utils.showToast(
                                      context, R.string.invalid_quantity.tr());
                                }
                              }),
                          SizedBox(width: 10.w),
                          ButtonWidget(
                              backgroundColor: R.color.primaryColor,
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.w, 4.h, 5.w, 10.h),
                              title: R.string.add_to_cart.tr(),
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 10.sp,
                                      color: R.color.white,
                                      height: 20.h / 10.sp),
                              onPressed: () {
                                if (_numberController.text != "0" &&
                                    _numberController.text != "00" &&
                                    _numberController.text != "000") {
                                  _cubit.orderCourse(
                                      widget.productId,
                                      _editingController.text,
                                      _numberController.text,
                                      _nameController.text.trim(),
                                      _phoneController.text.trim(),
                                      _emailController.text.trim(),
                                      true);
                                } else {
                                  Utils.showToast(
                                      context, R.string.invalid_quantity.tr());
                                }
                                //NavigationUtils.pop(context);
                              }),
                        ],
                      ),
                    ],
                  ),
                  Positioned(
                      top: 0.h,
                      right: 0.w,
                      child: InkWell(
                        onTap: () {
                          NavigationUtils.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 24.h,
                        ),
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildFeaturedCourse(BuildContext context) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: List.generate(
            _cubit.listCourse.length,
            (index) {
              return Container(
                margin: EdgeInsets.symmetric(vertical: 6),
                child: EventHighlight(
                  callback: () {
                    // NavigationUtils.pop(context);
                    NavigationUtils.navigatePage(
                        context,
                        CourseDetailsPage(
                          courseId: _cubit.listCourse[index].id,
                          productId: _cubit.listCourse[index].productId ?? 0,
                        ));
                  },
                  imageUrl: _cubit.listCourse[index].avatarUrl ?? "",
                  title: _cubit.listCourse[index].name ?? "",
                  currentFee: _cubit.listCourse[index].currentFee ?? 0,
                  preferentialFee:
                      _cubit.listCourse[index].preferentialFee ?? 0,
                  isHot: _cubit.listCourse[index].isHot ?? false,
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildSimilarCourse(BuildContext context) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: List.generate(
            _cubit.listCourseSimilar.length,
            (index) {
              return Container(
                margin: EdgeInsets.symmetric(vertical: 6),
                child: EventHighlight(
                  callback: () {
                    // NavigationUtils.pop(context);
                    NavigationUtils.navigatePage(
                        context,
                        CourseDetailsPage(
                          courseId: _cubit.listCourseSimilar[index].id,
                          productId:
                              _cubit.listCourseSimilar[index].productId ?? 0,
                        ));
                  },
                  imageUrl: _cubit.listCourseSimilar[index].avatarUrl ?? "",
                  title: _cubit.listCourseSimilar[index].name ?? "",
                  currentFee: _cubit.listCourseSimilar[index].currentFee ?? 0,
                  preferentialFee:
                      _cubit.listCourseSimilar[index].preferentialFee ?? 0,
                  isHot: _cubit.listCourseSimilar[index].isHot ?? false,
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _sliverSpace(double height) {
    return SliverToBoxAdapter(
      child: SizedBox(height: height.h),
    );
  }

  Widget tabView(CourseDetailsState state) {
    switch (_tabController.index) {
      case 0:
        return SliverToBoxAdapter(
          child: CourseInfoPage(
              courseId: widget.courseId,
              visible: state is! CourseDetailsLoading),
        );
      case 1:
        return SliverToBoxAdapter(
            child: DescriptionPage(
          courseId: widget.courseId,
        ));
      default:
        return SliverToBoxAdapter(
            child: FeelPage(
          courseId: widget.courseId,
        ));
    }
  }

  Widget buildNote(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 6.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _editingController,
        focusNode: _focusNode,
        keyboardType: TextInputType.multiline,
        textInputAction: TextInputAction.newline,
        inputFormatters: [LengthLimitingTextInputFormatter(500)],
        maxLines: 500,
        minLines: 1,
        underLineColor: R.color.white,
        hintText: R.string.if_you_are_note_studying_in_person.tr(),
        onChanged: (value) {
          setState(() {
            _cubit.changeText(_editingController.text.trim().length);
          });
        },
      ),
    );
  }

  Widget _buildEnterName(BuildContext context, CourseDetailsState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _nameController,
        focusNode: _nameFocus,
        inputFormatters: [LengthLimitingTextInputFormatter(100)],
        isRequired: true,
        borderSize: BorderSide.none,
        hintText: R.string.enter_your_name.tr(),
        errorText: _cubit.errorName,
        onChanged: _cubit.validateName,
        border: 0,
        onSubmitted: state is CourseDetailsLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _nameFocus, _phoneFocus),
      ),
    );
  }

  Widget _buildEnterPhone(BuildContext context, CourseDetailsState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _phoneController,
        focusNode: _phoneFocus,
        keyboardType: TextInputType.number,
        inputFormatters: [LengthLimitingTextInputFormatter(10)],
        isRequired: true,
        borderSize: BorderSide.none,
        hintText: R.string.text_field_hint.tr(),
        errorText: _cubit.errorPhoneNumber,
        onChanged: _cubit.validatePhone,
        border: 0,
        onSubmitted: state is CourseDetailsLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _phoneFocus, _emailFocus),
      ),
    );
  }

  Widget _buildEnterEmail(BuildContext context, CourseDetailsState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _emailController,
        focusNode: _emailFocus,
        keyboardType: TextInputType.emailAddress,
        isRequired: true,
        maxLines: 1,
        inputFormatters: [LengthLimitingTextInputFormatter(100)],
        hintText: R.string.enter_your_email.tr(),
        borderSize: BorderSide.none,
        errorText: _cubit.errorEmail,
        onChanged: _cubit.validateEmail,
        onTap: () {},
        onSubmitted: state is CourseDetailsLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _emailFocus, _focusNode),
      ),
    );
  }

  Widget _buildDropdownProvince(
      BuildContext context, CourseDetailsState state) {
    List<String> listProvince =
        _cubit.listProvince.map((e) => e.cityName ?? "").toList();
    return Container(
      padding: EdgeInsets.only(left: 10.w, top: 2.h),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: DropdownColumnWidget(
        onTap: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listProvince,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.province.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectProvince(_cubit.listProvince[index]);
                  },
                );
              });
        },
        hintText: R.string.province.tr(),
        errorText: _cubit.errorProvince,
        currentValue: _cubit.selectedProvince?.cityName ??
            (_cubit.fetchItemByProductIdV2?.userInformation?.provinceId != null
                ? _cubit.cityName
                : _cubit.province),
        listData: listProvince,
        selectedValue: (String? value) {},
      ),
    );
  }
}

class StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar child;
  final bool? visible;

  StickyTabBarDelegate(this.visible, {required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Visibility(
      visible: visible ?? false,
      child: Container(
        color: R.color.grey100,
        child: this.child,
      ),
    );
  }

  @override
  double get maxExtent => this.child.preferredSize.height;

  @override
  double get minExtent => this.child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
