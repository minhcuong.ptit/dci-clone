import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PostState {
  PostState() : super();
}

class InitialPostState extends PostState {
  @override
  String toString() {
    return 'InitialPostState{}';
  }
}

class PostMetadataRefreshedState extends PostState {
  final String link;

  PostMetadataRefreshedState(this.link);

  @override
  String toString() {
    return 'PostMetadataRefreshedState{}';
  }

  @override
  List<Object> get props => [link];
}

class PostFailure extends PostState {
  final String? error;

  PostFailure(this.error);

  @override
  String toString() {
    return 'PostFailure{error: $error}';
  }
}

class PostLoading extends PostState {
  @override
  String toString() {
    return 'PostLoading{}';
  }
}

class PostSuccess extends PostState {
  @override
  String toString() {
    return 'PostSuccess{}';
  }
}

class PostGetCommunitySuccess extends PostState {
  @override
  String toString() {
    return 'PostGetCommunitySuccess{}';
  }
}
