import 'dart:io';

import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/src/dci_application.dart';
import 'package:metadata_fetch/metadata_fetch.dart';

import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/post_request.dart';
import 'package:imi/src/data/network/response/image_seed_data.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/response/upload_list_url_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/post/post_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:video_compress/video_compress.dart';

import '../../data/network/response/community_data.dart';
import '../../utils/enum.dart';

class PostCubit extends Cubit<PostState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  final PostType type;
  final int? targetCommunityId;

  List<CommunityData> communities = [];
  CommunityData? selectedCommunity;
  PostData? postData;
  bool isEditPost = false;
  String? _content;
  // media
  List<String?> urlFiles = [];
  List<CompressableFile> files = [];
  ImageSeedData? image;
  // link
  String? errorLink;
  Metadata? urlMetadata;
  String? url;

  PostCubit(this.repository, this.graphqlRepository, this.type,
      this.targetCommunityId, this.image)
      : super(InitialPostState()) {
    VideoCompress.deleteAllCache();
    _loadPostingCommunities();
  }

  bool canPost(String content) {
    if (selectedCommunity == null) {
      return false;
    }
    if (content.trim().isEmpty ||
        content.trim().length > Const.POST_MAXIMUM_CHARACTER) {
      return false;
    }
    switch (type) {
      case PostType.MEDIA:
        return true;
      case PostType.FREETEXT:
        return true;
      case PostType.LINK:
        return url != null;
    }
  }

  bool hasDraft(List<String> fieldContents) {
    if (fieldContents.any((element) => element.isNotEmpty)) {
      return true;
    }
    switch (type) {
      case PostType.MEDIA:
        return files.isNotEmpty;
      case PostType.FREETEXT:
        return _content?.isNotEmpty ?? false;
      case PostType.LINK:
        return url != null;
    }
  }

  void submitLink(String text) async {
    String newUrl = text.trim();
    if (newUrl != url) {
      if (!newUrl.startsWith('http')) {
        newUrl = "https://${newUrl}";
      }
      urlMetadata = await MetadataFetch.extract(newUrl);
      if (urlMetadata == null) {
        url = null;
        errorLink = R.string.post_link_invalid.tr();
      } else {
        url = newUrl;
        errorLink = null;
      }
      bool isLoading = state is PostLoading;
      emit(PostMetadataRefreshedState(newUrl));
      if (isLoading) {
        // try to maintain existing loading state
        emit(PostLoading());
      }
    } else {
      // do nothing if url does not change
    }
  }

  void selectCommunity(CommunityData community) {
    this.selectedCommunity = community;
    emit(InitialPostState());
  }

  void submitContent(String text) {
    _content = text;
    emit(InitialPostState());
  }

  void removeImageEvent(CompressableFile image) {
    if (state is PostLoading) return;
    files.remove(image);
    emit(InitialPostState());
  }

  void removeUrlImageEvent(String url) {
    urlFiles.remove(url);
    emit(InitialPostState());
  }

  void _compressVideo(XFile file) async {
    MediaInfo? mediaInfo = await VideoCompress.compressVideo(file.path,
        quality: VideoQuality.MediumQuality,
        deleteOrigin: false,
        includeAudio: true);
    if (mediaInfo?.file?.path != null) {
      CompressableFile compressableFile = CompressableFile(
          file: XFile(mediaInfo!.file!.path), isCompressed: true);
      int index = files.indexWhere((element) => element.file.path == file.path);
      if (index >= 0) {
        files[index] = compressableFile;
      }
    }
  }

  void pickImageEvent(bool isCamera, bool isImageType) async {
    emit(PostLoading());
    final picker = ImagePicker();
    try {
      XFile? file = isImageType
          ? await picker.pickImage(
              source: isCamera ? ImageSource.camera : ImageSource.gallery)
          : await picker.pickVideo(
              source: isCamera ? ImageSource.camera : ImageSource.gallery);
      if (file != null) {
        int bytes = await file.length();
        if (isImageType && bytes > Const.IMAGE_SIZE) {
          emit(PostFailure(R.string.upload_image_invalid.tr()));
        } else if (!isImageType && bytes > Const.VIDEO_SIZE) {
          emit(PostFailure(R.string.upload_video_invalid.tr()));
        } else {
          files.add(CompressableFile(file: file, isCompressed: isImageType));
          if (!isImageType) {
            _compressVideo(file);
          }
          emit(InitialPostState());
        }
      } else
        emit(InitialPostState());
    } catch (e) {
      emit(PostFailure(R.string.error.tr()));
    }
  }

  void preparePost(String text) async {
    if (state is PostLoading) {
      return;
    }
    if (selectedCommunity == null) {
      return;
    }
    emit(PostLoading());
    if (type == PostType.MEDIA && files.isNotEmpty) {
      ApiResult<List<UploadListUrlResponse>> listUrlResult = await repository
          .getListUploadUrl(files.map((e) => e.file.name).toList());
      listUrlResult.when(success: (List<UploadListUrlResponse> response) {
        _createPost(text, response);
      }, failure: (NetworkExceptions error) {
        emit(PostFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } else {
      _createPost(text, []);
    }
  }

  void _createPost(String text, List<UploadListUrlResponse> response) async {
    DciApplication.isPostInCommunity=false;
    List<String> images = [];
    emit(PostLoading());
    if (image != null) {
      images.add(image?.key ?? "");
    } else {
      urlFiles.forEach((element) {
        var key = Utils.getKeyPostData(element ?? '');
        if (key.isNotEmpty) {
          images.add(key);
        }
      });
    }

    // upload all files via keys
    if (response.isNotEmpty) {
      for (int i = 0; i < response.length; ++i) {
        String? key = response[i].key;
        String? url = response[i].url;
        if (key != null && url != null) {
          while (!files[i].isCompressed) {
            await Future.delayed(const Duration(milliseconds: 200));
          }
          await repository.uploadImage(url, File(files[i].file.path));
          images.add(key);
        }
      }
    }
    PostMetadata? metadata;
    switch (type) {
      case PostType.MEDIA:
        metadata = PostMetadataMedia();
        break;
      case PostType.FREETEXT:
        metadata = PostMetadataFreeText();
        break;
      case PostType.LINK:
        final linkUri = Uri.parse(this.url!);
        metadata = PostMetadataLink(
            url: this.url,
            rootUrl: linkUri.host,
            title: urlMetadata?.title,
            description: urlMetadata?.description,
            image: urlMetadata?.image);
        break;
    }
    if (isEditPost) {
      ApiResult<dynamic> editTask = await repository.editPost(
          PostRequest(
              communityId: selectedCommunity?.id,
              text: text,
              metadata: metadata,
              backgroundImages: images),
          postData?.id ?? '');
      editTask.when(success: (dynamic data) async {
        DciApplication.isPostInCommunity=true;
        emit(PostSuccess());
      }, failure: (NetworkExceptions error) async {
        emit(PostFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } else {
      ApiResult<dynamic> createTask = await repository.createPost(PostRequest(
          communityId: selectedCommunity?.id,
          text: text,
          metadata: metadata,
          backgroundImages: images));
      createTask.when(success: (dynamic data) async {
        DciApplication.isPostInCommunity=true;
        emit(PostSuccess());
      }, failure: (NetworkExceptions error) async {
        emit(PostFailure(NetworkExceptions.getErrorMessage(error)));
      });
    }
  }

  void _loadPostingCommunities() async {
    ApiResult<List<CommunityData>> apiResult =
        await graphqlRepository.getPostingCommunities();
    apiResult.when(success: (data) async {
      emit(PostLoading());
      if (targetCommunityId != null) {
        selectedCommunity =
            data.firstWhereOrNull((e) => e.id == targetCommunityId);
      }
      // simple sort and filter
      List<CommunityData> individuals = data
          .where((e) => CommunityType.INDIVIDUAL == e.type)
          .sorted((a, b) => (a.name ?? "").compareTo(b.name ?? ""));
      List<CommunityData> others = data
          .whereNot((e) => CommunityType.INDIVIDUAL == e.type)
          .sorted((a, b) => (a.name ?? "").compareTo(b.name ?? ""));
      communities = individuals + others;
      if (selectedCommunity == null && communities.isNotEmpty) {
        selectedCommunity = communities.first;
      }
      emit(InitialPostState());
    }, failure: (NetworkExceptions error) async {
      emit(PostFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void removeImageShare() {
    image = null;
    emit(InitialPostState());
  }
}

class CompressableFile {
  final XFile file;
  bool isCompressed;
  CompressableFile({
    required this.file,
    this.isCompressed = true,
  });
}
