import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/post/post.dart';
import 'package:imi/src/page/post/post_checkin/post_checkin.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/pending_action.dart';
import 'package:latlong2/latlong.dart';

import '../../../data/network/response/address_search_info.dart';
import '../../../utils/enum.dart';
import '../../../utils/utils.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/text_field_widget.dart';

enum PostCheckinPageViewType {
  LIST,
  MAP,
}

extension PostCheckinPageViewTypeExt on PostCheckinPageViewType {
  String get invertedIcon {
    switch (this) {
      case PostCheckinPageViewType.LIST:
        return R.drawable.ic_map;
      case PostCheckinPageViewType.MAP:
        return R.drawable.ic_list;
    }
  }

  Color get backgroundColor {
    switch (this) {
      case PostCheckinPageViewType.LIST:
        return R.color.baseWhite;
      case PostCheckinPageViewType.MAP:
        return Colors.white;
    }
  }
}

class CheckinMapPayload {
  final String? fullAddress;
  final LatLng? currentLocation;

  CheckinMapPayload(this.fullAddress, this.currentLocation);
}

class PostCheckinPage extends StatefulWidget {
  final CheckinMapPayload? payload;
  final int? targetCommunityId;

  // if existing payload exists, we are updating
  bool get _isUpdate => payload != null;

  const PostCheckinPage({Key? key, this.payload, this.targetCommunityId})
      : super(key: key);

  @override
  _PostCheckinPageState createState() => _PostCheckinPageState();
}

class _PostCheckinPageState extends State<PostCheckinPage> {
  TextEditingController searchController = TextEditingController();
  late PostCheckinCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = PostCheckinCubit(
        payload: widget.payload,
        type: widget._isUpdate
            ? PostCheckinPageViewType.MAP
            : PostCheckinPageViewType.LIST);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.baseWhite,
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<PostCheckinCubit, PostCheckinState>(
          listener: (context, state) {},
          builder: (context, state) {
            return Scaffold(
              appBar: appBar(
                  context,
                  widget._isUpdate
                      ? R.string.checkin_pick_on_map.tr()
                      : R.string.create_post.tr(),
                  leadingWidget: GestureDetector(
                    onTap: () => NavigationUtils.pop(context),
                    child: Container(
                        padding: EdgeInsets.only(
                            left: 16.w, right: 16.w, top: 12.h, bottom: 12.h),
                        child: Icon(
                          Icons.close,
                          color: R.color.black,
                          size: 18.w,
                        )),
                  ),
                  backgroundColor: R.color.white,
                  rightWidget: widget._isUpdate
                      ? null
                      : Center(
                          child: GestureDetector(
                            onTap: () {
                              if (!widget._isUpdate) {
                                _cubit.setViewType(
                                    _cubit.type == PostCheckinPageViewType.LIST
                                        ? PostCheckinPageViewType.MAP
                                        : PostCheckinPageViewType.LIST);
                              }
                            },
                            child: Row(children: [
                              Padding(
                                  padding: EdgeInsets.all(6.w),
                                  child: Image.asset(
                                    _cubit.type.invertedIcon,
                                    width: 18.w,
                                  )),
                              SizedBox(width: 16.w),
                            ]),
                          ),
                        )),
              backgroundColor: _cubit.type.backgroundColor,
              body: GestureDetector(
                onTap: () => Utils.hideKeyboard(context),
                child: buildPage(context, state),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, PostCheckinState state) {
    return KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
      return GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: _cubit.type == PostCheckinPageViewType.LIST
            ? _buildListView(context, state)
            : _buildMapView(context, state),
      );
    });
  }

  Widget _buildListView(BuildContext context, PostCheckinState state) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24.w),
          child: Column(
            children: [
              SizedBox(height: 12.h),
              TextFieldWidget(
                autoFocus: false,
                controller: searchController,
                hintText: R.string.checkin_search_location.tr(),
                fillColor: Colors.transparent,
                textInputAction: TextInputAction.search,
                suffixIcon: Container(
                    padding: EdgeInsets.all(12.w),
                    child: Image.asset(
                      R.drawable.ic_search,
                      height: 13.h,
                      width: 13.h,
                    )),
                onChanged: (text) {},
                onSubmitted: (text) {
                  _cubit.search(text?.trim() ?? "");
                },
              ),
              SizedBox(height: 8.h),
              Expanded(
                child: _cubit.suggestions.isEmpty &&
                        searchController.text.trim().isNotEmpty
                    ? Center(
                        child: Text(
                          R.string.no_result.tr(),
                          textAlign: TextAlign.center,
                        ),
                      )
                    : ListView.separated(
                        itemCount: _cubit.suggestions.length,
                        separatorBuilder: (context, index) =>
                            Container(color: R.color.lighterGray, height: 1.h),
                        itemBuilder: (BuildContext context, int index) {
                          return _buildSearchRow(
                              context, _cubit.suggestions[index]);
                        },
                      ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: state is PostCheckinLoading,
          child: PendingAction(),
        ),
      ],
    );
  }

  Widget _buildMapView(BuildContext context, PostCheckinState state) {
    return Stack(
      children: [
        Column(
          children: [
            Container(
              margin: EdgeInsets.only(
                  left: 24.w, right: 24.w, top: 4.h, bottom: 8.h),
              child: TextFieldWidget(
                autoFocus: false,
                controller: searchController,
                hintText: R.string.search.tr(),
                fillColor: Colors.transparent,
                textInputAction: TextInputAction.search,
                icon: Container(
                    padding: EdgeInsets.all(14.w),
                    child: Image.asset(
                      R.drawable.ic_search,
                      height: 13.h,
                      width: 13.h,
                    )),
                onChanged: (text) {},
                onSubmitted: (text) {
                  if (_cubit.type == PostCheckinPageViewType.LIST) {
                    _cubit.search(text?.trim() ?? "");
                  }
                },
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                child: FlutterMap(
                  mapController: _cubit.mapController,
                  options: MapOptions(
                      zoom: 13.0,
                      onTap: (tapPosition, point) {
                        _cubit.parseGeoPoint(point);
                      }),
                  layers: [
                    TileLayerOptions(
                      urlTemplate:
                          "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c'],
                    ),
                    MarkerLayerOptions(
                      markers: [
                        if (_cubit.payload?.currentLocation != null)
                          Marker(
                            width: 240.w,
                            height: 76.h,
                            anchorPos: AnchorPos.align(AnchorAlign.top),
                            point: _cubit.payload!.currentLocation!,
                            builder: (ctx) => Container(
                              child: Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: R.color.shadowColor,
                                              blurRadius: 3.0,
                                              offset: Offset(0.0, 5))
                                        ],
                                        color: R.color.white,
                                        borderRadius:
                                            BorderRadius.circular(12.h)),
                                    width: 240.w,
                                    height: 44.h,
                                    padding:
                                        EdgeInsets.symmetric(vertical: 4.h),
                                    child: Row(
                                      children: [
                                        SizedBox(width: 16.w),
                                        Image.asset(
                                          R.drawable.ic_map,
                                          width: 16.w,
                                        ),
                                        SizedBox(width: 8.w),
                                        SizedBox(
                                          width: 192.w,
                                          child: Text(
                                            _cubit.payload?.fullAddress ?? "",
                                            maxLines: 2,
                                            style: Theme.of(context)
                                                .textTheme
                                                .labelSmallText
                                                .copyWith(
                                                    color: R.color.shadesGray),
                                          ),
                                        ),
                                        SizedBox(width: 8.w),
                                      ],
                                    ),
                                  ),
                                  Image.asset(
                                    R.drawable.ic_map_marker_tooltip,
                                    height: 6.h,
                                  ),
                                  SizedBox(height: 2.h),
                                  Image.asset(
                                    R.drawable.ic_map_marker,
                                    height: 24.h,
                                  )
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Positioned(
          bottom: 108.h,
          right: 24.w,
          child: InkWell(
              onTap: () => _cubit.moveToCurrentPosition(),
              child: Image.asset(
                R.drawable.ic_current_location,
                width: 40.w,
                height: 40.w,
              )),
        ),
        Positioned(
          left: 0,
          bottom: 24.h,
          right: 0,
          child: _buildContinueButton(context),
        ),
      ],
    );
  }

  Widget _buildContinueButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      child: ButtonWidget(
          title: R.string.checkin_select_location.tr(),
          backgroundColor: _cubit.payload != null
              ? R.color.primaryColor
              : R.color.lighterGray,
          uppercaseTitle: false,
          onPressed: () {
            CheckinMapPayload? payload = _cubit.payload;
            if (payload != null) {
              _returnPickerResult(payload);
            }
          }),
    );
  }

  Widget _buildSearchRow(BuildContext context, AddressSearchInfo info) {
    String fullAddressWithoutName = [
      info.address?.street,
      info.address?.city,
      info.address?.state,
      info.address?.country
    ].whereNotNull().whereNot((e) => e.isEmpty).join(", ");
    String fullAddress = [info.address?.name, fullAddressWithoutName]
        .whereNotNull()
        .whereNot((e) => e.isEmpty)
        .join(", ");
    return GestureDetector(
      onTap: () {
        CheckinMapPayload payload = CheckinMapPayload(
            fullAddress, LatLng(info.latitude ?? 0, info.longitude ?? 0));
        _returnPickerResult(payload);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 12.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 4.h),
              child: Image.asset(
                R.drawable.ic_checkin_location,
                height: 24.h,
              ),
            ),
            SizedBox(width: 8.w),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    info.address?.name ?? "",
                    style: Theme.of(context)
                        .textTheme
                        .labelLargeText
                        .copyWith(fontSize: 14.sp),
                  ),
                  Text(
                    fullAddressWithoutName,
                    style: Theme.of(context)
                        .textTheme
                        .bodySmallText
                        .copyWith(color: R.color.shadesGray),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _returnPickerResult(CheckinMapPayload payload) {
    if (widget._isUpdate) {
      NavigationUtils.pop(context, result: payload);
    } else {
      NavigationUtils.pop(context);
      NavigationUtils.navigatePage(
          context,
          PostPage(
            type: PostType.FREETEXT,
            targetCommunityId: widget.targetCommunityId,
          ));
    }
  }
}
