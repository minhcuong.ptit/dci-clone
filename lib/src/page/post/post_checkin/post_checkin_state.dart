import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PostCheckinState extends Equatable {
  PostCheckinState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialPostCheckinState extends PostCheckinState {
  @override
  String toString() {
    return 'InitialPostCheckinState{}';
  }
}

class PostCheckinFailure extends PostCheckinState {
  final String? error;

  PostCheckinFailure(this.error);

  @override
  String toString() {
    return 'PostCheckinFailure{error: $error}';
  }
}

class PostCheckinLoading extends PostCheckinState {
  @override
  String toString() {
    return 'PostCheckinLoading{}';
  }
}

class PostCheckinSuccess extends PostCheckinState {
  @override
  String toString() {
    return 'PostCheckinSuccess{}';
  }
}
