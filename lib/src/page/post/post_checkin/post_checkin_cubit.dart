import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:imi/src/page/post/post_checkin/post_checkin.dart';
import 'package:latlong2/latlong.dart';

import '../../../data/network/response/address_search_info.dart';

class PostCheckinCubit extends Cubit<PostCheckinState> {
  CheckinMapPayload? payload;
  List<AddressSearchInfo> suggestions = [];
  PostCheckinPageViewType type;

  MapController mapController = MapController();

  PostCheckinCubit({this.payload, required this.type})
      : super(InitialPostCheckinState()) {
    mapController.onReady.then((value) => moveToInitPosition());
  }

  void moveToInitPosition() async {
    if (payload?.currentLocation != null) {
      mapController.move(payload!.currentLocation!, 13);
    } else {
      moveToCurrentPosition();
    }
  }

  void moveToCurrentPosition() async {
    Position? position = await Geolocator.getLastKnownPosition();
    if (position != null) {
      mapController.move(LatLng(position.latitude, position.longitude), 13);
    }
  }

  void setViewType(PostCheckinPageViewType type) {
    emit(PostCheckinLoading());
    this.type = type;
    emit(InitialPostCheckinState());
  }

  void search(String keyword) async {
    if (keyword.isEmpty) {
      return;
    }
    emit(PostCheckinLoading());
    LatLng? point;
    Position? position = await Geolocator.getLastKnownPosition();
    if (position != null) {
      point = LatLng(position.latitude, position.longitude);
    }
    try {
      suggestions = await _fetchAddresses(keyword, currentLocation: point);
    } catch (error) {
      suggestions = [];
    }
    emit(InitialPostCheckinState());
  }

  void parseGeoPoint(LatLng point) async {
    payload = null;
    mapController.move(point, 13);
    emit(PostCheckinLoading());
    List<Placemark> placemarks =
        await placemarkFromCoordinates(point.latitude, point.longitude);
    if (placemarks.isNotEmpty) {
      Placemark placemark = placemarks.first;
      payload = CheckinMapPayload(
          [
            placemark.name,
            placemark.street,
            placemark.locality,
            placemark.administrativeArea
          ].whereNotNull().whereNot((element) => element.isEmpty).join(", "),
          point);
    }
    emit(InitialPostCheckinState());
  }

  Future<List<AddressSearchInfo>> _fetchAddresses(String searchText,
      {int limitInformation = 10, LatLng? currentLocation}) async {
    Map<String, dynamic> queryParameters = {
      "q": searchText,
      "limit": limitInformation == 0 ? "" : "$limitInformation",
    };
    if (currentLocation != null) {
      queryParameters["lat"] = currentLocation.latitude;
      queryParameters["lon"] = currentLocation.longitude;
    }
    Response response = await Dio().get(
      "https://photon.komoot.io/api/",
      queryParameters: queryParameters,
    );
    final json = response.data;
    return (json["features"] as List)
        .map((d) => AddressSearchInfo.fromPhotonAPI(d))
        .toList();
  }
}
