import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/image_seed_data.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/post/post_cubit.dart';
import 'package:imi/src/page/post/post_state.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/discard_popup.dart';
import 'package:imi/src/widgets/pending_action.dart';
import 'package:imi/src/widgets/simple_choice_popup.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../data/network/request/post_request.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/dci_event_handle.dart';
import '../../utils/enum.dart';
import '../../widgets/dropdown_column_widget.dart';
import '../../widgets/link_preview_widget.dart';
import '../../widgets/text_field_widget.dart';
import '../club_info/club_information/club_information.dart';
import '../video_player/video_play_widget.dart';

class PostPage extends StatefulWidget {
  final PostType type;
  final int? targetCommunityId;
  final PostData? postData;
  final ImageSeedData? imageData;

  const PostPage(
      {Key? key,
      required this.type,
      this.targetCommunityId,
      this.postData,
      this.imageData})
      : super(key: key);

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  TextEditingController _linkController = TextEditingController();
  TextEditingController _controller = TextEditingController();
  late PostCubit _cubit;

  int? get _communityId => appPreferences.getInt(Const.COMMUNITY_ID);
  late MainCubit _mainCubit;
  late final FocusNode _detailNode;
  late final FocusNode _linkNode;

  @override
  void initState() {
    super.initState();
    _mainCubit = GetIt.I();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = PostCubit(repository, graphqlRepository, widget.type,
        widget.targetCommunityId, widget.imageData);
    _detailNode = FocusNode();
    _linkNode = FocusNode();
    if (widget.type == PostType.LINK) {
      _linkNode.requestFocus();
    } else {
      _detailNode.requestFocus();
    }
    initData();
  }

  @override
  void dispose() {
    _linkController.dispose();
    _controller.dispose();
    _cubit.close();
    _linkNode.dispose();
    _detailNode.dispose();
    super.dispose();
  }

  void initData() {
    _cubit.postData = widget.postData;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_cubit.postData != null &&
          (_cubit.postData?.id?.isNotEmpty ?? false)) {
        _cubit.isEditPost = true;
        PostMetadata? metadata = widget.postData?.metadata;
        _controller.text = widget.postData?.text ?? '';
        switch (widget.type) {
          case PostType.MEDIA:
            _cubit.urlFiles = _cubit.postData?.backgroundImages ?? [];
            break;
          case PostType.FREETEXT:
            break;
          case PostType.LINK:
            if (metadata is PostMetadataLink) {
              _linkController.text = metadata.url ?? '';
              _cubit.submitLink(_linkController.text);
            }
            break;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _onPop(context);

        return false;
      },
      child: Scaffold(
        backgroundColor: R.color.baseWhite,
        body: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<PostCubit, PostState>(
            listener: (context, state) {
              if (state is PostFailure)
                Utils.showErrorSnackBar(context, state.error);
              if (state is PostSuccess) {
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                if (widget.targetCommunityId != null &&
                    _cubit.selectedCommunity?.postingTitle != "Tường của tôi") {
                  NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  NavigationUtils.navigatePage(
                      context,
                      ViewClubPage(
                        communityId: widget.targetCommunityId ?? 0,
                        checkGroup: true,
                      ));
                } else if (widget.targetCommunityId != null &&
                    _cubit.selectedCommunity?.postingTitle == "Tường của tôi") {
                  NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  NavigationUtils.navigatePage(
                      context,
                      ViewProfilePage(
                        communityId: widget.targetCommunityId ?? 0,
                        profileUser: true,
                      ));
                } else if (widget.targetCommunityId == null) {
                  _mainCubit.selectTab(1);
                }
                if (!_cubit.isEditPost)
                Utils.showToast(context, R.string.your_post_was_shared.tr());
                DCIEventHandler().send(PostEventEvent()
                  ..postEventType = PostEventType.CREATE_NEW_PAGE);
              }
              if (state is PostSuccess && widget.imageData != null) {
                NavigationUtils.popToFirst(context);
                _mainCubit.selectTab(1);
              }
            },
            builder: (context, state) {
              return Scaffold(
                appBar: appBar(context, R.string.create_post.tr(),
                    textStyle: Theme.of(context)
                        .textTheme
                        .bodyBold
                        .apply(color: R.color.white)
                        .copyWith(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w700,
                            height: 1.7),
                    leadingWidget: GestureDetector(
                      onTap: () {
                        _onPop(context);
                      },
                      child: Container(
                          child: Icon(
                        CupertinoIcons.left_chevron,
                        color: R.color.white,
                      )),
                    ),
                    backgroundColor: R.color.blue,
                    rightWidget: Center(
                      child: InkWell(
                        onTap: _cubit.canPost(_controller.text)
                            ? () {
                                Utils.hideKeyboard(context);
                                if (state != PostLoading) {
                                  _cubit.preparePost(_controller.text.trim());
                                }
                              }
                            : null,
                        child: Row(children: [
                          Padding(
                              padding: EdgeInsets.only(bottom: 0.h),
                              child: Icon(
                                Icons.check,
                                color: _cubit.canPost(_controller.text)
                                    ? R.color.white
                                    : R.color.white.withOpacity(0.4),
                                size: 22.w,
                              )),
                          SizedBox(width: 6.w),
                          Text(
                            R.string.post_publish.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .buttonSmall
                                .copyWith(
                                    color: _cubit.canPost(_controller.text)
                                        ? R.color.white
                                        : R.color.white.withOpacity(0.4),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w700),
                          ),
                          SizedBox(width: 16.w),
                        ]),
                      ),
                    )),
                backgroundColor: R.color.baseWhite,
                body: GestureDetector(
                  onTap: () => Utils.hideKeyboard(context),
                  child: buildPage(context, state),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onPop(BuildContext context) {
    if (_cubit.hasDraft([_linkController.text, _controller.text.trim()])) {
      showBarModalBottomSheet(
          context: context,
          backgroundColor: Colors.transparent,
          expand: false,
          builder: (context) => SingleChildScrollView(
                  child: DiscardPopup(
                title: R.string.discard_post.tr(),
                description: R.string.discard_post_confirm.tr(),
                onConfirm: () {
                  Utils.hideKeyboard(context);
                  NavigationUtils.pop(context);
                },
              )));
    } else {
      Utils.hideKeyboard(context);
      NavigationUtils.pop(context);
    }
  }

  Widget buildPage(BuildContext context, PostState state) {
    return Stack(
      children: [
        ListView(
          shrinkWrap: true,
          children: [
            ..._buildMetadata(context),
            ..._buildCommonField(context),
          ],
        ),
        Visibility(
          visible: state is PostLoading,
          child: PendingAction(),
        ),
      ],
    );
  }

  List<Widget> _buildCommonField(BuildContext context) {
    return [
      Container(
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView(
          shrinkWrap: true,
          children: [
            Row(
              children: [
                Text(
                  R.string.post_content.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bodySmallText
                      .copyWith(fontSize: 12.sp, color: R.color.shadesGray),
                ),
                Text(
                  "*",
                  style: Theme.of(context)
                      .textTheme
                      .bodySmallText
                      .copyWith(fontSize: 14.sp, color: R.color.red),
                ),
              ],
            ),
            SizedBox(height: 4.h),
            _contentText(),
            SizedBox(height: 4.h),
            RichText(
                textAlign: TextAlign.end,
                text: TextSpan(
                    text: "${_controller.text.trim().length}",
                    style: Theme.of(context).textTheme.bodySmallText.copyWith(
                        fontSize: 10.sp,
                        color: _controller.text.trim().length <=
                                Const.POST_MAXIMUM_CHARACTER
                            ? R.color.black
                            : R.color.red),
                    children: [
                      TextSpan(
                        text: "/${Const.POST_MAXIMUM_CHARACTER}",
                        style: Theme.of(context)
                            .textTheme
                            .bodySmallText
                            .copyWith(
                                fontSize: 10.sp, color: R.color.shadesGray),
                      )
                    ])),
            SizedBox(height: 24.h),
            Text(
              R.string.post_share_to.tr(),
              style: Theme.of(context)
                  .textTheme
                  .bodySmallText
                  .copyWith(fontSize: 10.sp, color: R.color.shadesGray),
            ),
            _cubit.isEditPost
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 8.h,
                      ),
                      Text(
                        (_cubit.postData?.communityV2?.id ?? 0) ==
                                (_communityId ?? 0)
                            ? R.string.public.tr()
                            : _cubit.postData?.communityV2?.name ?? '',
                        style: Theme.of(context)
                            .textTheme
                            .labelLargeText
                            .copyWith(
                                fontWeight: FontWeight.w500, fontSize: 13.sp),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      Container(
                        height: 1.h,
                        width: double.infinity,
                        color: R.color.grey300,
                      )
                    ],
                  )
                : _buildDropdownShareTarget(context),
          ],
        ),
      )
    ];
  }

  List<Widget> _buildMetadata(BuildContext context) {
    switch (widget.type) {
      case PostType.MEDIA:
        return _buildMetadataMedia(context);
      case PostType.FREETEXT:
        return _buildMetadataFreeText(context);
      case PostType.LINK:
        return _buildMetadataLink(context);
    }
  }

  List<Widget> _buildMetadataMedia(BuildContext context) {
    return [
      Container(
        margin: EdgeInsets.only(top: 16.h),
        height: 100.h,
        child: ListView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 16.w),
          shrinkWrap: true,
          children: [
            if (widget.imageData != null) ...[
              _cubit.image == null
                  ? SizedBox()
                  : Container(
                      margin: EdgeInsets.only(right: 8.w),
                      height: 100.h,
                      width: 100.h,
                      child: Stack(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.circular(4.w),
                              child: CachedNetworkImage(
                                width: 100.h,
                                height: 100.h,
                                imageUrl: widget.imageData?.url ?? '',
                                fit: BoxFit.cover,
                              )),
                          Positioned(
                            top: 0.h,
                            right: 0.h,
                            child: InkWell(
                              onTap: () {
                                _cubit.removeImageShare();
                              },
                              child: Image.asset(
                                R.drawable.ic_close_transparent,
                                width: 28.h,
                                height: 28.h,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
            ],
            ..._cubit.urlFiles
                .map((e) => Container(
                      margin: EdgeInsets.only(right: 8.w),
                      height: 100.h,
                      width: 100.h,
                      child: Stack(
                        children: [
                          ((e ?? '').contains('.jpg?') ||
                                  (e ?? '').contains('.png?'))
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(4.w),
                                  child: CachedNetworkImage(
                                    width: 100.h,
                                    height: 100.h,
                                    imageUrl: e ?? '',
                                    fit: BoxFit.cover,
                                  ))
                              : VideoPlayWidget(
                                  url: e ?? '',
                                  showControls: false,
                                ),
                          Positioned(
                            top: 0.h,
                            right: 0.h,
                            child: InkWell(
                              onTap: () => _cubit.removeUrlImageEvent(e ?? ''),
                              child: Image.asset(
                                R.drawable.ic_close_transparent,
                                width: 28.h,
                                height: 28.h,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
                .toList(),
            ..._cubit.files
                .map((e) => Container(
                      margin: EdgeInsets.only(right: 8.w),
                      height: 100.h,
                      width: 100.h,
                      child: Stack(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.circular(4.h),
                              child: Utils.isImage(e.file.path)
                                  ? Image.file(
                                      File(e.file.path),
                                      fit: BoxFit.cover,
                                      width: 100.h,
                                      height: 100.h,
                                    )
                                  : VideoPlayWidget(
                                      xFile: e.file,
                                      showControls: false,
                                    )),
                          Positioned(
                            top: 0.h,
                            right: 0.h,
                            child: InkWell(
                              onTap: () => _cubit.removeImageEvent(e),
                              child: Image.asset(
                                R.drawable.ic_close_primary,
                                width: 28.h,
                                height: 28.h,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
                .toList(),
            if ((_cubit.files.length + _cubit.urlFiles.length) <
                Const.POST_MAXIMUM_MEDIA)
              InkWell(
                  onTap: () {
                    _postMedia(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.w),
                        border: Border.all(
                          width: 1.h,
                          color: R.color.black,
                        )),
                    height: 100.h,
                    width: 100.h,
                    child: Icon(
                      Icons.add,
                      color: R.color.black,
                      size: 20.h,
                    ),
                  ))
          ],
        ),
      ),
    ];
  }

  List<Widget> _buildMetadataFreeText(BuildContext context) {
    return [
      Container(
        margin: EdgeInsets.only(top: 16.h),
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            SizedBox(height: 4.h),
          ],
        ),
      ),
    ];
  }

  List<Widget> _buildMetadataLink(BuildContext context) {
    return [
      Container(
        margin: EdgeInsets.only(top: 16.h),
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            Text(
              R.string.post_link.tr(),
              style: Theme.of(context)
                  .textTheme
                  .bodySmallText
                  .copyWith(fontSize: 10.sp, color: R.color.shadesGray),
            ),
            SizedBox(height: 4.h),
            FocusScope(
              onFocusChange: (value) {
                if (!value) {
                  _cubit.submitLink(_linkController.text);
                }
              },
              child: TextFieldWidget(
                focusNode: _linkNode,
                controller: _linkController,
                hintText: R.string.post_link_example.tr(),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.url,
                fillColor: Colors.transparent,
                onChanged: (value) {},
                onSubmitted: (value) {},
                errorText: _cubit.errorLink,
                customInputStyle: Theme.of(context).textTheme.bodyRegular,
              ),
            ),
            SizedBox(height: 16.h),
            LinkPreviewWidget(
              image: _cubit.urlMetadata?.image,
              title: _cubit.urlMetadata?.title,
              description: _cubit.urlMetadata?.description,
              url: _cubit.url,
            ),
          ],
        ),
      ),
    ];
  }

  Widget _buildDropdownShareTarget(BuildContext context) {
    List<String> listProvince =
        _cubit.communities.map((e) => e.postingTitle ?? "").toList();
    return Padding(
      padding: EdgeInsets.only(top: 8.h),
      child: DropdownColumnWidget(
        isRequired: false,
        onTap: () {
          showBarModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            expand: false,
            builder: (context) => SingleChildScrollView(
              child: SimpleChoicePopup(
                choices: _cubit.communities
                    .map((e) => SimpleChoice(
                        icon: e.type?.postingIcon,
                        title: e.postingTitle ?? "",
                        checked: e == _cubit.selectedCommunity,
                        onChosen: () {
                          _cubit.selectCommunity(e);
                        }))
                    .toList(),
              ),
            ),
          );
        },
        hintText: "",
        labelText: "",
        currentValue: _cubit.selectedCommunity?.postingTitle ?? "",
        listData: listProvince,
        selectedValue: (text) {},
      ),
    );
  }

  void _postMedia(BuildContext context) {
    showBarModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      expand: false,
      builder: (context) => SingleChildScrollView(
        child: SimpleChoicePopup(
          choices: [
            SimpleChoice(
                icon: R.drawable.ic_image,
                title: R.string.select_image.tr(),
                onChosen: () => _addMedia(context, true)),
            SimpleChoice(
                icon: R.drawable.ic_video,
                title: R.string.select_video.tr(),
                onChosen: () => _addMedia(context, false)),
          ],
        ),
      ),
    );
  }

  void _addMedia(BuildContext context, bool isImageType) {
    showBarModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      expand: false,
      builder: (context) => SingleChildScrollView(
        child: SimpleChoicePopup(
          choices: [
            // if (isImageType || !Platform.isIOS)
            SimpleChoice(
                icon: R.drawable.ic_camera,
                title: R.string.profile_camera.tr(),
                onChosen: () => _cubit.pickImageEvent(true, isImageType)),
            SimpleChoice(
                icon: R.drawable.ic_gallery,
                title: R.string.profile_gallery.tr(),
                onChosen: () => _cubit.pickImageEvent(false, isImageType)),
          ],
        ),
      ),
    );
  }

  Widget _contentText() {
    final maxLines = 7;
    return (widget.type != PostType.FREETEXT)
        ? TextFieldWidget(
            focusNode: _detailNode,
            controller: _controller,
            hintText: R.string.post_content_hint.tr(),
            textInputAction: TextInputAction.newline,
            maxLines: 5,
            minLines: 1,
            fillColor: Colors.transparent,
            keyboardType: TextInputType.multiline,
            onChanged: (value) {
              _cubit.submitContent(value ?? "");
            },
            onSubmitted: (value) {},
            customInputStyle: Theme.of(context)
                .textTheme
                .bodyRegular
                .copyWith(fontSize: 13.sp),
          )
        : Container(
            decoration: BoxDecoration(
              border: Border.all(color: R.color.grey, width: 0.5.w),
              borderRadius: BorderRadius.circular(0.h),
            ),
            margin: EdgeInsets.only(top: 12.h),
            padding: EdgeInsets.symmetric(horizontal: 8.w),
            child: TextField(
                cursorWidth: 1,
                focusNode: _detailNode,
                textInputAction: TextInputAction.newline,
                controller: _controller,
                maxLines: maxLines,
                decoration: InputDecoration(
                  hintText: R.string.post_content_hint.tr(),
                  fillColor: R.color.white,
                  border: InputBorder.none,
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyRegular
                      .copyWith(color: R.color.grey, fontSize: 12.sp),
                ),
                onChanged: (value) {
                  _cubit.submitContent(value);
                },
                style: Theme.of(context).textTheme.bodyRegular.copyWith(
                      color: R.color.black,
                    )),
          );
  }
}
