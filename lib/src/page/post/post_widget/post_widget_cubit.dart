import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/reaction_request.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/utils/const.dart';
import 'cubit.dart';

class PostWidgetCubit extends Cubit<PostWidgetState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool checkUnavailable = false;

  PostWidgetCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(PostWidgetInitial());

  Future reaction(int postId, bool like) async {
    emit(PostWidgetLikePost());
    ApiResult<dynamic> result = await repository.reaction(ReactionRequest(
        postId: postId, action: like ? Const.LIKE : Const.DISLIKE));
    result.when(success: (dynamic response) async {
      emit(PostWidgetReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(PostWidgetFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future bookmark(int postId, bool like) async {
    emit(PostWidgetLikePost());
    ApiResult<dynamic> result = await repository.bookmark(ReactionRequest(
        postId: postId, action: like ? Const.BOOKMARK : Const.REMOVE));
    result.when(success: (dynamic response) async {
      emit(PostWidgetReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(PostWidgetFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
