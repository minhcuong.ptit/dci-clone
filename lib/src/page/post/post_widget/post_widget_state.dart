import 'package:equatable/equatable.dart';

abstract class PostWidgetState extends Equatable {
  PostWidgetState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class PostWidgetInitial extends PostWidgetState {
  @override
  String toString() => 'CommunityInitial';
}

class PostWidgetLikePost extends PostWidgetState {
  @override
  String toString() => 'PostWidgetLoading';
}

class PostWidgetLoading extends PostWidgetState {
  @override
  String toString() => 'PostWidgetLoading';
}

class PostWidgetFailure extends PostWidgetState {
  final String error;

  PostWidgetFailure(this.error);

  @override
  String toString() => 'CommunityFailure { error: $error }';
}

class PostWidgetReactionSuccess extends PostWidgetState {
  @override
  String toString() => 'PostWidgetReactionSuccess {}';
}
