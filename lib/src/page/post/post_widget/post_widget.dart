import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/request/post_request.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/link_preview_widget.dart';
import 'package:imi/src/widgets/post_slider_widget.dart';
import 'package:imi/src/widgets/text_field_widget.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../data/network/repository/app_graphql_repository.dart';
import '../../../data/network/repository/app_repository.dart';
import '../../likes/likes.dart';
import '../../../utils/const.dart';
import '../../../utils/time_ago.dart';
import '../../../widgets/avatar_widget.dart';
import '../../../widgets/custom_expandable_text.dart';
import '../../../widgets/dci_popup.dart';
import '../../../widgets/detail_post_popup.dart';
import 'cubit.dart';

class PostWidget extends StatefulWidget {
  final PostData data;
  final bool isLoggedIn;
  final bool isShowMore;
  final VoidCallback posDetailCallback;
  final Function(bool bookmark) onBookmarkSuccess;
  final VoidCallback viewProfileCallback;
  final VoidCallback? viewClubCallback;
  final VoidCallback likeCallback;
  final VoidCallback shareCallback;
  final VoidCallback moreCallback;
  final VoidCallback commentCallback;
  final VoidCallback showLikeCallback;
  final VoidCallback reportCallback;
  final VoidCallback reportPost;
  final bool? isShowComment;
  final bool? isShowMenu;
  final bool? isFromGroupPage;
  final bool isInPostDetails;

  PostWidget({
    Key? key,
    required this.data,
    this.isLoggedIn = true,
    this.isShowMore = true,
    required this.onBookmarkSuccess,
    required this.viewProfileCallback,
    this.viewClubCallback,
    required this.likeCallback,
    required this.shareCallback,
    required this.moreCallback,
    required this.showLikeCallback,
    required this.posDetailCallback,
    required this.commentCallback,
    required this.reportCallback,
    this.isShowComment,
    this.isFromGroupPage,
    this.isShowMenu,
    this.isInPostDetails = false,
    required this.reportPost,
  }) : super(key: key);

  @override
  State<PostWidget> createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget>
    with AutomaticKeepAliveClientMixin {
  final TextEditingController _controllerComment = TextEditingController();
  bool _showIndividual = true;
  late PostWidgetCubit _cubit;
  late PostWidgetState _currentState;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _currentState = PostWidgetInitial();
    _cubit = PostWidgetCubit(
      repository: repository,
      graphqlRepository: graphqlRepository
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _showIndividual = ((widget.data.communityV2?.type) == Const.INDIVIDUAL) ||
        (widget.isFromGroupPage ?? false);
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<PostWidgetCubit, PostWidgetState>(
        listener: (context, state) {
          if (state is PostWidgetReactionSuccess) {
            // widget.onLikeSuccess(
            //     _cubit.post?.isLike ?? false, _cubit.post?.likeNumber ?? 0);
          }
        },
        builder: (context, state) {
          _currentState = state;
          return _buildPage(context, state);
        },
      ),
    );
  }

  Widget _buildPage(context, state) {
    return GestureDetector(
      onTap: () {
        Utils.hideKeyboard(context);
        FocusScope.of(context).unfocus();
        widget.posDetailCallback();
      },
      child: Container(
        color: R.color.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: R.color.white,
              padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 12.h),
              child: Row(
                children: [
                  Container(
                    child: _avatar(),
                    padding: EdgeInsets.only(
                        top: 15.h, right: _showIndividual ? 8.w : 16.w),
                  ),
                  Expanded(
                    child: _headerTitle(),
                  ),
                  Visibility(
                    visible: (widget.isShowMenu ?? true),
                    child: IconButton(
                        onPressed: () {
                          openPostPopup(context);
                        },
                        icon: Icon(Icons.more_horiz_outlined,
                            color: R.color.darkGray)),
                  )
                ],
              ),
            ),
            GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: widget.posDetailCallback,
                child: contentPostWidget(context)),
            Container(
              color: R.color.white,
              padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 16.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: widget.likeCallback,
                        child: Image.asset(
                          widget.data.isLike == true
                              ? R.drawable.ic_like_on
                              : R.drawable.ic_like_off,
                          height: 24.h,
                          width: 24.w,
                        ),
                      ),
                      SizedBox(
                        width: 8.w,
                      ),
                      InkWell(
                        onTap: () {
                          NavigationUtils.rootNavigatePage(
                              context,
                              LikesPage(
                                  postId: int.parse(widget.data.id ?? "0"),
                                  likeCount: widget.data.likeNumber ?? 0));
                        },
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 3.h),
                          child: Text(
                              Utils().formatNumberLike(
                                  widget.data.likeNumber ?? 0),
                              style: Theme.of(context)
                                  .textTheme
                                  .labelSmallText
                                  .copyWith(
                                      color: R.color.textFieldTitle,
                                      fontSize: 16.sp)),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 16.w,
                  ),
                  GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: widget.commentCallback,
                      child: Row(
                        children: [
                          Image.asset(
                            R.drawable.ic_message_off,
                            height: 22.h,
                            width: 24.w,
                          ),
                          SizedBox(width: 8.w),
                          Padding(
                            padding: EdgeInsets.only(bottom: 3.h),
                            child: Text(
                                Utils()
                                    .formatNumberLike(
                                        widget.data.commentNumber ?? 0)
                                    .toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmallText
                                    .copyWith(
                                        color: R.color.textFieldTitle,
                                        fontSize: 16.sp)),
                          ),
                        ],
                      )),
                ],
              ),
            ),
            _comment()
          ],
        ),
      ),
    );
  }

  Widget _avatar() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: widget.viewProfileCallback,
      child: _showIndividual
          ? AvatarWidget(
              radius: 16.h,
              avatar: widget.data.avatar,
              size: 32.h,
            )
          : Stack(
              clipBehavior: Clip.none,
              alignment: Alignment.centerRight,
              children: [
                AvatarWidget(
                  radius: 16.h,
                  avatar: widget.data.communityV2?.avatarUrl,
                  size: 32.h,
                ),
                Positioned(
                  left: 15.w,
                  child: Container(
                    decoration: BoxDecoration(
                        color: R.color.white,
                        borderRadius: BorderRadius.circular(15)),
                    child: Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: AvatarWidget(
                        radius: 12.h,
                        avatar: widget.data.avatar,
                        size: 24.h,
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Widget _headerTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 8.h,
        ),
        InkWell(
          onTap: (widget.data.communityV2?.type) == Const.INDIVIDUAL
              ? widget.viewProfileCallback
              : widget.viewClubCallback,
          child: Row(
            children: [
              Visibility(
                visible: !_showIndividual,
                child: Flexible(
                  child: Text(
                    (widget.data.communityV2?.name ?? ""),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .bodySmallBold
                        .copyWith(fontWeight: FontWeight.w700, fontSize: 11.sp),
                  ),
                ),
              ),
              Visibility(
                visible: _showIndividual,
                child: Flexible(
                  child: Text(
                    (widget.data.fullName ?? ""),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .bodySmallBold
                        .copyWith(fontWeight: FontWeight.w700, fontSize: 11.sp),
                  ),
                ),
              ),
              SizedBox(
                width: 8.w,
              ),
            ],
          ),
        ),
        Row(
          children: [
            Flexible(
              child: Visibility(
                visible: !_showIndividual,
                child: Padding(
                  padding: EdgeInsets.only(right: 8.w),
                  child: GestureDetector(
                    onTap: widget.viewProfileCallback,
                    child: Text(
                      (widget.data.fullName ?? ""),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .labelSmallText
                          .copyWith(
                              fontSize: 8.sp,
                              fontWeight: FontWeight.w500,
                              color: R.color.black),
                    ),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: (widget.data.communityV2?.type) == Const.INDIVIDUAL,
              child: Padding(
                padding: EdgeInsets.only(top: 2.h, right: 8.w),
                child: Image.asset(
                  R.drawable.ic_global,
                  width: 10.w,
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: 2.h, right: 6.w),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2.w),
                    color: R.color.shadesGray.withOpacity(0.8)),
                width: 4.w,
                height: 4.w),
            Text(
              widget.data.createdDate == null
                  ? ""
                  : TimeAgo.timeAgoSinceDate(
                      DateTime.fromMillisecondsSinceEpoch(
                              widget.data.createdDate!,
                              isUtc: true)
                          .toUtc()),
              style: Theme.of(context).textTheme.labelSmallText.copyWith(
                  fontSize: 8.sp,
                  fontWeight: FontWeight.w400,
                  color: R.color.shadesGray),
            )
          ],
        ),
      ],
    );
  }

  Widget contentPostWidget(context) {
    Widget _textWidget = CustomExpandableText(
      text: widget.data.text ?? "",
      maxLines: 4,
      textStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(fontSize: 11.sp, fontWeight: FontWeight.w400),
    );
    if (Utils.isEmpty(widget.data.backgroundImage) &&
        Utils.isEmpty(widget.data.backgroundImages) &&
        (Utils.isEmpty(widget.data.backgroundColor) ||
            Utils.parseStringToColor(widget.data.backgroundColor) ==
                R.color.white)) {
      PostMetadata? metadata = widget.data.metadata;
      return Padding(
        padding: EdgeInsets.only(left: 16.w, right: 16.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _textWidget,
            if (metadata is PostMetadataLink) ...[
              SizedBox(height: 8.h),
              LinkPreviewWidget(
                image: metadata.image,
                title: metadata.title,
                description: metadata.description,
                url: metadata.url,
              ),
            ],
          ],
        ),
      );
    } else if (!Utils.isEmpty(widget.data.backgroundImages) ||
        !Utils.isEmpty(widget.data.backgroundImage)) {
      List<String?> listImage =
          widget.data.backgroundImages ?? [widget.data.backgroundImage];
      //listImage = listImage.take(4).toList();
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: _textWidget,
          ),
          SizedBox(height: 8.h),
          PostSliderWidget(
            images: listImage,
            isInPostDetails: widget.isInPostDetails,
            onPressImage: widget.posDetailCallback,
          ),
        ],
      );
    } else {
      return Container(
        constraints: BoxConstraints(
            minHeight: (ScreenUtil().screenWidth - 16.h * 2) * 10 / 16),
        color: Utils.parseStringToColor(widget.data.backgroundColor),
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 16.w, right: 16.w),
        child: CustomExpandableText(
          text: widget.data.text ?? "",
          maxLines: 4,
        ),
      );
    }
  }

  Widget actionPostWidget(
      IconData icon, bool isSelected, VoidCallback callback) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        width: 30.h,
        height: 30.h,
        margin: EdgeInsets.only(left: 10.h),
        alignment: Alignment.center,
        // padding: EdgeInsets.all(14.h),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: R.color.shadowColor,
                  blurRadius: 20.0,
                  offset: Offset(5, 10))
            ],
            color: R.color.white),
        child: Icon(
          icon,
          size: 15.h,
          color: isSelected ? R.color.primaryColor : R.color.black,
        ),
      ),
    );
  }

  Widget _comment() {
    return Visibility(
      visible: (widget.isShowComment ?? false),
      child: Container(
        height: 44.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.h),
            border: Border.all(color: R.color.grey),
            color: Colors.white),
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        child: TextFieldWidget(
          border: 1.w,
          height: 36.h,
          autoFocus: false,
          borderSize: BorderSide.none,
          controller: _controllerComment,
          isRequired: true,
          inputFormatters: [
            LengthLimitingTextInputFormatter(255),
          ],
          keyboardType: TextInputType.streetAddress,
          hintText: R.string.comment_here.tr(),
          // errorText: _cubit.errorAddress,
          //onChanged: _cubit.validateHeadquarterAddress,
          onSubmitted: (_) {},
        ),
      ),
    );
  }

  void showMessageDialog(BuildContext context) {
    showBarModalBottomSheet(
        expand: false,
        context: context,
        builder: (_) {
          return buildShowMoreBottomSheet(context);
        });
  }

  Widget buildShowMoreBottomSheet(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 30.h, vertical: 35.h),
      child: Column(
        children: [
          buildRowBottom(
              image: R.drawable.ic_report,
              title: R.string.report_to_club.tr(),
              callback: () {
                NavigationUtils.pop(context);
                widget.reportCallback();
                showDialog<void>(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return CupertinoAlertDialog(
                      title: Text(R.string.notification.tr()),
                      content: Text(R.string.report_thank_you.tr()),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text(R.string.ok.tr()),
                          onPressed: () {
                            NavigationUtils.pop(context);
                          },
                        ),
                      ],
                    );
                  },
                );
              }),
          SizedBox(height: 12.h),
          Divider(height: 1, color: R.color.gray),
          SizedBox(height: 12.h),
        ],
      ),
    );
  }

  Widget buildRowBottom(
      {String? image, String? title, VoidCallback? callback}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: callback,
      child: Row(
        children: [
          Image.asset(image ?? "", height: 18.h, color: R.color.memberTitle),
          SizedBox(width: 15.h),
          Text(
            title ?? "",
            style: TextStyle(
              fontSize: 14.sp,
              fontWeight: FontWeight.w400,
              color: R.color.black,
            ),
          )
        ],
      ),
    );
  }

  void openPostPopup(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => SingleChildScrollView(
                child: DCIPopup(
              child: DetailPostPopup(
                onReportPost: widget.reportPost,
                post: widget.data,
                onDelete: () {},
                onEdit: () {},
                showEdit: false,
                showDelete: false,
                onBookmark: () async {
                  if (_currentState is! PostWidgetLikePost) {
                    bool bookmarked =
                        widget.data.isBookmark == true ? false : true;
                    widget.data.isBookmark = true;
                    await _cubit.bookmark(
                        int.parse(widget.data.id ?? '0'), bookmarked);
                    widget.onBookmarkSuccess(bookmarked);
                  }
                },
              ),
            )));
  }

  @override
  bool get wantKeepAlive => true;
}
