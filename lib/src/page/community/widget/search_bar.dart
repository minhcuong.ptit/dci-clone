import 'package:async/async.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';

enum SearchBarType {
  ICON_LEFT,
  ICON_RIGHT,
}

class SearchBar extends StatefulWidget {
  final SearchBarType? searchBarType;
  final String? title;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onTapSearch;
  final Color? color;
  const SearchBar(
      {Key? key,
      required this.onChanged,
      this.title,
      this.searchBarType,
      required this.onTapSearch,
      this.color})
      : super(key: key);
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final TextEditingController _searchController = TextEditingController();
  CancelableOperation<void>? cancellableOperation;
  final _delayTime = Duration(milliseconds: 600);
  @override
  Widget build(BuildContext context) {
    return (widget.searchBarType == SearchBarType.ICON_RIGHT)
        ? buildSearchRight(context)
        : buildSearchLeft(context);
  }

  @override
  void dispose() {
    cancellableOperation?.cancel();
    super.dispose();
  }

  void _start() {
    cancellableOperation = CancelableOperation.fromFuture(
      Future.delayed(_delayTime),
      onCancel: () {
        print('Canceled');
      },
    );
  }

  void _onItemChanged(String value) {
    cancellableOperation?.cancel();
    _start();
    cancellableOperation?.value.whenComplete(() {
      widget.onChanged(value);
    });
  }

  Widget buildSearchLeft(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          // boxShadow: [
          //   BoxShadow(
          //       color: Colors.grey,
          //       spreadRadius: 5,
          //       blurRadius: 2,
          //       offset: Offset(0, 1) // changes position of shadow
          //       ),
          // ],
          borderRadius: BorderRadius.circular(30.h),
          color: widget.color ?? R.color.disableGray.withOpacity(0.1)),
      child: Padding(
        padding: EdgeInsetsDirectional.fromSTEB(10, 0, 5, 3),
        child: TextField(
            controller: _searchController,
            maxLines: 1,
            decoration: InputDecoration(
              prefixIcon: Padding(
                padding:  EdgeInsets.only(top: 4.h),
                child: IconButton(
                  color: R.color.black.withOpacity(0.6),
                  icon: Icon(
                   CupertinoIcons.search,
                       size: 21.h,
                  ),
                  onPressed: () {
                    widget.onTapSearch(_searchController.text.tr());
                  },
                ),
              ),
              hintText: widget.title ?? R.string.search.tr(),
              fillColor: R.color.white,
              border: InputBorder.none,
              hintStyle: Theme.of(context).textTheme.bodyRegular.copyWith(
                    color: R.color.grey,
                  ),
            ),
            onChanged: _onItemChanged,
            style: Theme.of(context).textTheme.bodyRegular.copyWith(
                  color: R.color.black,
                )),
      ),
    );
  }

  Widget buildSearchRight(BuildContext context) {
    return TextField(
        controller: _searchController,
        maxLines: 1,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            color: R.color.black.withOpacity(0.6),
            icon: Icon(
              Icons.search,
            ),
            onPressed: () {
              widget.onTapSearch(_searchController.text.tr());
            },
          ),
          hintText: widget.title ?? R.string.search.tr(),
          fillColor: R.color.white,
          border: InputBorder.none,
          hintStyle: Theme.of(context).textTheme.bodyRegular.copyWith(
                color: R.color.grey,
              ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: R.color.grey300),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: R.color.grey300),
          ),
        ),
        onChanged: _onItemChanged,
        style: Theme.of(context).textTheme.bodyRegular.copyWith(
              color: R.color.black,
            ));
  }
}
