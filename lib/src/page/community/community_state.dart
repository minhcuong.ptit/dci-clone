import 'package:equatable/equatable.dart';

abstract class CommunityState extends Equatable {
  CommunityState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class CommunityInitial extends CommunityState {
  @override
  String toString() => 'CommunityInitial';
}

class CommunityLikePost extends CommunityState {
  @override
  String toString() => 'CommunityLoading';
}

class CommunityLoading extends CommunityState {
  @override
  String toString() => 'CommunityLoading';
}

class ActionFollowLoading extends CommunityState {
  @override
  String toString() => 'ActionFollowLoading';
}

class CommunityFailure extends CommunityState {
  final String error;

  CommunityFailure(this.error);

  @override
  String toString() => 'CommunityFailure { error: $error }';
}

class LogoutSuccess extends CommunityState {
  final String? message;

  LogoutSuccess({this.message});

  @override
  String toString() => 'LogoutSuccess { message: $message }';
}

class GetProfileSuccess extends CommunityState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCategorySuccess extends CommunityState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends CommunityState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class GetListCommunityTimeLineSuccess extends CommunityState {
  @override
  String toString() {
    return 'GetListCommunityTimeLineSuccess';
  }
}

class UnreadCountSuccess extends CommunityState {
  @override
  String toString() {
    return 'UnreadCountSuccess';
  }
}

class ChooseCategorySuccess extends CommunityState {
  @override
  String toString() {
    return 'ChooseCategorySuccess';
  }
}

class ChooseCommunitySuccess extends CommunityState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

class DeleteConversationSuccess extends CommunityState {
  @override
  String toString() => 'DeleteConversationSuccess {}';
}

class GetListConversationSuccess extends CommunityState {
  final String? message;

  GetListConversationSuccess({this.message});

  @override
  String toString() => 'GetListConversationSuccess { message: $message }';
}

class ReactionSuccess extends CommunityState {
  @override
  String toString() => 'ReactionSuccess {}';
}

class FetchReportReasonsSuccess extends CommunityState {
  @override
  String toString() {
    return 'FetchReportReasonsSuccess';
  }
}

class ReportSuccess extends CommunityState {
  @override
  String toString() {
    return 'ReportSuccess';
  }
}

class ReportPostSuccess extends CommunityState {
  @override
  String toString() {
    return 'ReportPostSuccess';
  }
}
