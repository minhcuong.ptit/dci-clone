import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/reaction_request.dart';
import 'package:imi/src/data/network/request/report_request.dart';
import 'package:imi/src/data/network/request/submit_favorite_request.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/my_profile_response.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/response/report_reasons_response.dart';
import 'package:imi/src/data/network/response/user_data.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';

import '../../data/network/request/follow_request.dart';
import '../../data/network/response/home_response.dart';
import '../../data/network/response/list_content_response.dart';
import '../../data/network/response/user_view.dart';
import 'community.dart';

class CommunityCubit extends Cubit<CommunityState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  String? get userId => appPreferences.getString(Const.ID);

  int? get communityId => appPreferences.getInt(Const.COMMUNITY_ID);

  String? get fullName => appPreferences.getString(Const.FULL_NAME);

  String get firstName => (fullName?.split(" ") ?? ["There"]).first;

  String? get province => appPreferences.getString(Const.PROVINCE);

  String? get avatar => appPreferences.getString(Const.AVATAR);

  String? get cover => appPreferences.getString(Const.COVER);

  int? get followers => appPreferences.getInt(Const.FOLLOWERS) ?? 0;

  int get followings => appPreferences.getInt(Const.FOLLOWINGS) ?? 0;

  int get point => appPreferences.getInt(Const.POINT) ?? 0;

  bool get isLoggedIn => appPreferences.isLoggedIn;

  List<int> get listFavoriteCategory => appPreferences
      .getListString(Const.FAVORITE_CATEGORY)
      .map((e) => int.parse(e))
      .toList();

  List<int> get listFavoriteCommunity => appPreferences
      .getListString(Const.FAVORITE_COMMUNITY)
      .map((e) => int.parse(e))
      .toList();

  List<CategoryData> listCategory = [];
  List<CategoryData>? listSelectedCategory;
  List<CommunityData> listRecommendClub = [];
  List<CommunityData> listRecommendUser = [];
  List<PostData> listPostTimeline = [];
  List<PostData> listPostFollowingOrigin = [];
  List<PostData> listPostFollowing = [];
  int posTab = 0;
  FollowingTab posTabFollowing = FollowingTab.ALL;
  String? nextTokenTimeline;
  String? nextTokenFollowing;
  int unreadNotification = 0;
  bool haveNotification = true;
  bool haveMessage = true;
  bool isFirstLoadTimeLine = false;
  List<FetchReportReason> listReportPost = [];

  CommunityCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(CommunityInitial());

  Future<void> getData({bool isRefresh = false}) async {
    if (isLoggedIn) {
      getProfile(isRefresh: isRefresh);
    } else {
      getCommunityTimeline(isRefresh: isRefresh);
      getCommunityFollowing(isRefresh: isRefresh);
    }
  }

  Future<void> getProfile({bool isRefresh = false}) async {
    emit((isRefresh || !isFirstLoadTimeLine)
        ? CommunityLoading()
        : CommunityInitial());
    ApiResult<MyProfileResponse> getProfileTask =
        await graphqlRepository.getMyProfile();
    getProfileTask.when(success: (MyProfileResponse response) async {
      UserData? user = response.me;
      appPreferences.saveData(user);
      CommunityData? myCommunity = response.myCommunity;
      appPreferences.saveCommunity(myCommunity);
      getCommunityTimeline(isRefresh: true);
      // getCommunityFollowing(isRefresh: isRefresh);
      isFirstLoadTimeLine ? emit(GetProfileSuccess()) : null;
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void unreadCount() async {
    emit(CommunityLoading());
    ApiResult<int> unread = await graphqlRepository.getNotificationCount(false);
    unread.when(success: (int response) async {
      unreadNotification = response;
      emit(UnreadCountSuccess());
    }, failure: (NetworkExceptions error) async {
      unreadNotification = 0;
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getReportPost() async {
    isFirstLoadTimeLine ? emit(CommunityLoading()) : null;
    ApiResult<ReportReasonsData> reportPost = await graphqlRepository
        .getReportReasons(reasonType: ReportReasonType.POST.name);
    reportPost.when(success: (ReportReasonsData data) {
      listReportPost = data.fetchReportReasons!;
      isFirstLoadTimeLine ? emit(FetchReportReasonsSuccess()) : null;
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void report({int? reasonId, String? postId}) async {
    emit(CommunityLoading());
    ApiResult<dynamic> result = await repository.reportPost(
        ReportRequest(
            reasonId: reasonId, postType: ReportReasonType.INDIVIDUAL.name),
        postId: postId);
    result.when(success: (dynamic data) {
      emit(ReportSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void reportGroup({int? reasonId, String? postId}) async {
    emit(CommunityLoading());
    ApiResult<dynamic> result = await repository.reportPost(
        ReportRequest(
            reasonId: reasonId, postType: ReportReasonType.GROUP.name),
        postId: postId);
    result.when(success: (dynamic data) {
      emit(ReportPostSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getCommunityTimeline(
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || !isLoadMore) ? CommunityLoading() : CommunityInitial());
    if (isLoadMore != true) {
      nextTokenTimeline = null;
      listPostTimeline.clear();
    }
    ApiResult<HomeResponse> homeTask = await graphqlRepository.getHome(
        myPost: true,
        nextToken: nextTokenTimeline,
        listCategory: listFavoriteCategory);
    homeTask.when(success: (HomeResponse data) async {
      nextTokenTimeline = data.posts?.nextToken;
      listCategory = data.categories ?? [];
      if (listSelectedCategory == null) {
        listSelectedCategory = listCategory
            .where((element) => listFavoriteCategory.contains(element.id))
            .toList();
      }
      if (listRecommendUser.isEmpty) {
        listRecommendUser = listRecommendClub
            .where((element) => listFavoriteCommunity.contains(element.id))
            .toList();
      }
      listPostTimeline.addAll(data.posts?.data ?? []);
      isFirstLoadTimeLine = true;
      isFirstLoadTimeLine ? emit(GetListCommunityTimeLineSuccess()) : null;
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshPostTimeline() {
    emit(CommunityLoading());
    getCommunityTimeline();
    emit(GetListCommunityTimeLineSuccess());
  }

  void getCommunityFollowing(
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit(isRefresh || !isLoadMore ? CommunityLoading() : CommunityInitial());
    if (isLoadMore != true) {
      nextTokenFollowing = null;
      listPostFollowing.clear();
    }
    List<String> listCommunityTypeIn = getListCommunityTypeIn();
    List<ApiResult<dynamic>> apiResults = await Future.wait([
      graphqlRepository.getListPost(nextTokenFollowing,
          listCommunityTypeIn: listCommunityTypeIn,
          isBookmarked: posTabFollowing == FollowingTab.BOOKMARK),
      graphqlRepository.getRecommendCommunity(
        [CommunityType.BASIC_GROUP.name, CommunityType.STUDY_GROUP.name],
        listFavoriteCategory,
      ),
      graphqlRepository.getRecommendCommunity(
          [CommunityType.INDIVIDUAL.name], listFavoriteCategory),
    ]);
    for (int i = 0; i < apiResults.length; i++) {
      ApiResult apiResult = apiResults[i];
      apiResult.when(success: (dynamic data) async {
        if (data is PostPageData) {
          if(nextTokenFollowing!=data.nextToken){
            nextTokenFollowing = data.nextToken;
          }
          if(!listPostFollowing.contains(data.data??[])){
            listPostFollowing.addAll(data.data ?? []);
          }
          if (posTabFollowing == FollowingTab.ALL) {
            listPostFollowingOrigin = listPostFollowing;
          }
        }
        if (data is ListContentResponse) {
          if (i == 1) {
            listRecommendClub = data.communities?.data ?? [];
          } else {
            listRecommendUser = data.communities?.data ?? [];
          }
        }
        if (i == apiResults.length - 1) {
          emit(GetListCommunitySuccess());
        }
      }, failure: (NetworkExceptions error) async {
        emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
      });
    }
  }

  void selectTab(int index) {
    emit(CommunityLoading());
    posTab = index;
    emit(ChooseCategorySuccess());
  }

  void selectTabFollowing(FollowingTab tab) async {
    emit(CommunityLoading());
    posTabFollowing = tab;
    nextTokenFollowing = null;
    List<String> listCommunityTypeIn = getListCommunityTypeIn();
    ApiResult<PostPageData?> communityTask =
        await graphqlRepository.getListPost(nextTokenFollowing,
            listCommunityTypeIn: listCommunityTypeIn,
            isBookmarked: posTabFollowing == FollowingTab.BOOKMARK);
    communityTask.when(success: (PostPageData? data) async {
      nextTokenFollowing = data?.nextToken;
      listPostFollowing.clear();
      listPostFollowing.addAll(data?.data ?? []);
      if (posTabFollowing == FollowingTab.ALL) {
        listPostFollowingOrigin = listPostFollowing;
      }
      emit(GetListCommunitySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectCategory(CategoryData selectedCategory) {
    emit(CommunityLoading());
    int index = (listSelectedCategory ?? [])
        .indexWhere((element) => element.id == selectedCategory.id);
    if (index >= 0) {
      listSelectedCategory?.removeAt(index);
    } else {
      listSelectedCategory?.add(selectedCategory);
    }
    emit(ChooseCategorySuccess());
  }

  void selectCommunity(CommunityData selectedCommunity) {
    emit(CommunityLoading());
    int index = listRecommendUser
        .indexWhere((element) => element.id == selectedCommunity.id);
    if (index >= 0) {
      listRecommendUser.removeAt(index);
    } else {
      listRecommendUser.add(selectedCommunity);
    }
    emit(ChooseCommunitySuccess());
  }

  void applyFavoriteCategory({bool isCancel = false}) {
    emit(CommunityLoading());
    if (isCancel)
      listSelectedCategory = listCategory
          .where((element) => listFavoriteCategory.contains(element.id))
          .toList();
    appPreferences.setData(Const.FAVORITE_CATEGORY,
        (listSelectedCategory ?? []).map((e) => e.id).toList());
    submitFavorite();
    emit(ChooseCommunitySuccess());
  }

  void applyFavoriteCommunity({bool isCancel = false}) {
    emit(CommunityLoading());
    if (isCancel)
      listRecommendUser = listRecommendClub
          .where((element) => listFavoriteCommunity.contains(element.id))
          .toList();
    appPreferences.setData(
        Const.FAVORITE_COMMUNITY, listRecommendUser.map((e) => e.id).toList());
    submitFavorite();
    emit(ChooseCommunitySuccess());
  }

  void refreshCommunity() {
    emit(CommunityLoading());
    emit(CommunityInitial());
  }

  void submitFavorite() async {
    emit(CommunityLoading());
    try {
      List<int> listSelectedCategoryId =
          (listSelectedCategory ?? []).map((e) => e.id!).toList();
      List<int> listSelectedCommunityId =
          listRecommendUser.map((e) => e.id!).toList();
      SubmitFavoriteRequest request = SubmitFavoriteRequest(
        categories: listSelectedCategoryId,
        communities: listSelectedCommunityId,
      );
      ApiResult<dynamic> submitFavoriteTask =
          await repository.submitFavorite(request);
      submitFavoriteTask.when(success: (dynamic data) async {
        emit(CommunityInitial());
        getCommunityTimeline(isRefresh: true);
      }, failure: (NetworkExceptions error) async {
        emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } catch (e) {
      logger.e(e);
      emit(CommunityInitial());
    }
  }

  Future reaction(int postId, bool like) async {
    emit(CommunityLikePost());
    ApiResult<dynamic> result = await repository.reaction(ReactionRequest(
        postId: postId, action: like ? Const.LIKE : Const.DISLIKE));
    result.when(success: (dynamic response) async {
      emit(ReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          getCommunityFollowing();
          getCommunityTimeline();
        }
        return;
      }
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future bookmark(int postId, bool like) async {
    emit(CommunityLikePost());
    ApiResult<dynamic> result = await repository.bookmark(ReactionRequest(
        postId: postId, action: like ? Const.BOOKMARK : Const.REMOVE));
    result.when(success: (dynamic response) async {
      emit(ReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          getCommunityFollowing();
          getCommunityTimeline();
        }
        return;
      }
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void follow(CommunityData? communityData) async {
    emit(ActionFollowLoading());
    bool isFollowing = communityData?.userView?.isFollowing == true;
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityData?.id,
        action: isFollowing ? Const.UNFOLLOW : Const.FOLLOW));
    result.when(success: (dynamic response) async {
      if (listPostTimeline.isEmpty) {
        getCommunityTimeline(isRefresh: true);
      }
      try {
        listRecommendUser
            .firstWhere((element) => element.id == communityData?.id)
            .userView = UserView(isFollowing: !isFollowing);
      } catch (e) {}
      try {
        listRecommendClub
            .firstWhere((element) => element.id == communityData?.id)
            .userView = UserView(isFollowing: !isFollowing);
      } catch (e) {}
      nextTokenFollowing = null;
      List<String> listCommunityTypeIn = getListCommunityTypeIn();
      ApiResult<PostPageData?> CommunityTask =
          await graphqlRepository.getListPost(nextTokenFollowing,
              listCommunityTypeIn: listCommunityTypeIn,
              isBookmarked: posTabFollowing == FollowingTab.BOOKMARK);
      CommunityTask.when(success: (PostPageData? data) async {
        nextTokenFollowing = data?.nextToken;
        listPostFollowing.addAll(data?.data ?? []);
        if (posTabFollowing == FollowingTab.ALL) {
          listPostFollowingOrigin = listPostFollowing;
        }
        emit(GetListCommunitySuccess());
      }, failure: (NetworkExceptions error) async {
        emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
      });
      emit(ReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void followUser(CommunityData? communityData) async {
    emit(ActionFollowLoading());
    bool isFollowing = communityData?.userView?.isFollowing == true;
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityData?.id,
        action: isFollowing ? Const.UNFOLLOW : Const.FOLLOW));
    result.when(success: (dynamic response) async {
      if (listPostTimeline.isEmpty) {
        getCommunityTimeline(isRefresh: true);
      }
      try {
        listRecommendUser
            .firstWhere((element) => element.id == communityData?.id)
            .userView = UserView(isFollowing: !isFollowing);
      } catch (e) {}
      try {
        listRecommendClub
            .firstWhere((element) => element.id == communityData?.id)
            .userView = UserView(isFollowing: !isFollowing);
      } catch (e) {}
      emit(GetListCommunitySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  List<String> getListCommunityTypeIn() {
    List<String> listCommunityTypeIn = [];
    if (posTabFollowing == FollowingTab.INDIVIDUAL) {
      listCommunityTypeIn = [Const.INDIVIDUAL];
    } else if (posTabFollowing == FollowingTab.GROUP) {
      listCommunityTypeIn = [
        CommunityType.STUDY_GROUP.name,
        CommunityType.BASIC_GROUP.name
      ];
    }
    return listCommunityTypeIn;
  }
}
