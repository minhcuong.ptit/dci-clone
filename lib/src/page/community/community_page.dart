import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/page/comment/comment_page.dart';
import 'package:imi/src/page/likes/likes.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/community_widget.dart';
import 'package:imi/src/page/post/post_widget/post_widget.dart';
import 'package:imi/src/widgets/custom_search_widget.dart';
import 'package:imi/src/widgets/popup_report_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../dci_application.dart';
import '../../utils/dci_event_handle.dart';
import '../../widgets/custom_tabbar_controller.dart';
import '../../widgets/refresh_loadmore.dart';
import '../auth_user/login/login_page.dart';
import '../club_info/club_information/club_information.dart';
import '../pin_code/forgot_pin/forgot_pin_page.dart';
import 'community.dart';

class CommunityPage extends StatefulWidget {
  const CommunityPage({Key? key}) : super(key: key);

  @override
  _CommunityPageState createState() => _CommunityPageState();
}

class _CommunityPageState extends State<CommunityPage>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  late CommunityCubit _cubit;
  late MainCubit _mainCubit;
  late AuthenticationCubit _authCubit;
  late TabController _tabController;
  RefreshController _timelineRefreshController = RefreshController();
  RefreshController _followingRefreshController = RefreshController();
  TextEditingController _controller = TextEditingController();
  ScrollController _controllerTimeline = ScrollController();
  final ScrollController _controllerFollowing = ScrollController();
  late CommunityState _currentState;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = CommunityCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _currentState = CommunityInitial();
    _tabController = TabController(vsync: this, length: 2);
    _mainCubit = GetIt.I();
    _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    // _cubit.getCommunityTimeline();
    _cubit.getData();
    DCIEventHandler().subscribe(_onPostListener);

    _controllerTimeline = ScrollController()
      ..onBottomReach(() {
        // your code goes here
        // _cubit.getCommunityTimeline(isLoadMore: true);
      }, sensitivity: 200.0, throttleDuration: Duration(milliseconds: 500));
    _cubit.getReportPost();
  }

  @override
  void dispose() {
    DCIEventHandler().unsubscribe(_onPostListener);
    _timelineRefreshController.dispose();
    _followingRefreshController.dispose();
    _controller.dispose();
    _controllerTimeline.dispose();
    _controllerFollowing.dispose();
    super.dispose();
  }

  void _onPostListener(PostEventEvent event) {
    if (mounted && (event.postEventType == PostEventType.CREATE_NEW_PAGE)) {
      _cubit.getData(isRefresh: true);
      _controllerTimeline.animateTo(0,
          duration: Duration(milliseconds: 200), curve: Curves.linear);
      _controllerFollowing.animateTo(0,
          duration: Duration(milliseconds: 200), curve: Curves.linear);
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: BlocProvider(
            create: (context) => _cubit,
            child: BlocConsumer<CommunityCubit, CommunityState>(
              listener: (context, state) {
                _currentState = state;
                if (state is LogoutSuccess) {
                  AuthenticationCubit authCubit =
                      BlocProvider.of<AuthenticationCubit>(context);
                  authCubit.logout();
                }
                if (state is CommunityFailure) {
                  Utils.showErrorSnackBar(context, state.error);
                }
                if (state is CommunityLoading) {
                  _timelineRefreshController.loadComplete();
                  _timelineRefreshController.refreshCompleted();
                  _followingRefreshController.loadComplete();
                  _followingRefreshController.refreshCompleted();
                }
                if (state is ReportSuccess) {
                  Utils.showToast(context, R.string.report_user_success.tr());
                  // _cubit.refreshPostTimeline();
                }
                if (state is ReportPostSuccess) {
                  Utils.showToast(context, R.string.report_post_success.tr());
                  // _cubit.refreshPostTimeline();
                }
              },
              builder: (
                BuildContext context,
                CommunityState state,
              ) {
                _currentState = state;
                return buildPage(context, state);
              },
            ),
          ),
        ));
  }

  Widget buildPage(BuildContext context, CommunityState state) {
    return StackLoadingView(
      visibleLoading: (state is CommunityLoading),
      child: Container(
        width: double.infinity,
        color: R.color.white,
        child: Stack(
          children: [
            Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 130.h,
                      width: double.infinity,
                      color: R.color.primaryColor,
                    ),
                    Positioned(
                      top: 0.h,
                      right: 0.h,
                      child: Image.asset(
                        R.drawable.background_library,
                        height: 110.h,
                      ),
                    ),
                    Positioned(
                      left: 20.h,
                      bottom: 24.h,
                      child: Text(
                        R.string.dci_community.tr().toUpperCase(),
                        style: Theme.of(context)
                            .textTheme
                            .medium500
                            .copyWith(fontSize: 20.sp, color: R.color.white),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 0.h),
                  child: CustomSearchWidget(
                    onSubmitted: (String value) {},
                    controller: _controller,
                    onSearchChange: () {},
                    onChanged: (String value) {},
                    shadow: [],
                    color: R.color.backgroundGray,
                  ),
                ),
                buildTabBar(),
                buildTabBarView(context, state),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTabBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
      child: CustomTabBarController(
        onTabChange: (index) {
          _cubit.selectTab(index);
          if (index == 1 && _cubit.listPostFollowing.length == 0&&!_tabController.indexIsChanging) {
            _cubit.getCommunityFollowing(isRefresh: true);
          }
        },
        tabController: _tabController,
        tabBarItems: [
          TabBarItem(title: R.string.timeline.tr()),
          TabBarItem(title: R.string.following.tr()),
        ],
        physics: NeverScrollableScrollPhysics(),
      ),
    );
  }

  Widget buildTabBarView(BuildContext context, CommunityState state) {
    return Expanded(
      child: TabBarView(
        controller: _tabController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          buildTimelineTab(context, state),
          buildFollowingTab(context, state)
        ],
      ),
    );
  }

  Widget buildTimelineTab(BuildContext context, CommunityState state) {
    return RefreshConfiguration(
      footerBuilder: () => ClassicFooter(loadingIcon: CircularProgressIndicator()),
      child: RefreshLoadmore(
        onRefresh: () async {
          _cubit.getCommunityTimeline(isRefresh: true);
        },
        onLoadmore: () async {
          if (state is GetListCommunityTimeLineSuccess &&
              _cubit.nextTokenTimeline != null) {
            _cubit.getCommunityTimeline(isLoadMore: true);
          }
        },
        isLastPage: false,
        child: state is CommunityLoading
            ? SizedBox()
            : _cubit.isFirstLoadTimeLine == true
                ? _cubit.listPostTimeline.length == 0
                    ? DciApplication.isPostInCommunity == false
                        ? headerNoneFollow(context, state)
                        : Center(
                            child: CircularProgressIndicator(),
                          )
                    : ListView.separated(
                         padding: EdgeInsets.zero,
                        // padding: EdgeInsets.only(bottom: 30.h),
                        shrinkWrap: true,
                        controller: _controllerTimeline,
                        itemCount: _cubit.listPostTimeline.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            Container(
                          height: 8.h,
                          color: R.color.grey300,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          PostData data = _cubit.listPostTimeline[index];
                          return postWidget(data);
                        },
                      )
                : SizedBox(),
      ),
    );
  }

  Widget buildFollowingTab(BuildContext context, CommunityState state) {
    return RefreshLoadmore(
      // enablePullUp: true,
      // controller: _followingRefreshController,
      isLastPage: false,
      onRefresh: ()async {
        _cubit.getCommunityFollowing(isRefresh: true);
      },
      onLoadmore:() async {
        if(state is GetListCommunitySuccess &&_cubit.nextTokenFollowing != null){
          _cubit.getCommunityFollowing(isLoadMore: true);
        }
            },
      child: state is GetListCommunitySuccess &&
              Utils.isEmpty(_cubit.listPostFollowingOrigin)
          ? contentForNoneFollowing(context, state)
          : contentForContainFollowing(state),
    );
  }

  Widget buildFollowingTabBar() {
    List<FollowingTab> listTab = FollowingTab.values;
    return Container(
      height: 24.h + 16.h * 2,
      padding: EdgeInsets.symmetric(vertical: 16.h),
      alignment: Alignment.center,
      width: double.infinity,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        itemCount: listTab.length,
        separatorBuilder: (context, index) => SizedBox(
          width: 8.h,
        ),
        itemBuilder: (context, index) {
          FollowingTab tab = listTab[index];
          return InkWell(
            onTap: () {
              _cubit.selectTabFollowing(tab);
            },
            child: Container(
              //height: 24.h,
              padding: EdgeInsets.fromLTRB(12.w, 2.h, 12.w, 0.h),
              decoration: BoxDecoration(
                  color: tab == _cubit.posTabFollowing
                      ? R.color.lightBlue
                      : Colors.transparent,
                  borderRadius: BorderRadius.circular(24)),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    tab.title,
                    style: Theme.of(context).textTheme.bodySmallText.copyWith(
                        color: tab == _cubit.posTabFollowing
                            ? R.color.white
                            : R.color.shadesGray,
                        height: 1.0),
                  ),
                  Visibility(
                    visible: tab == _cubit.posTabFollowing,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.w, bottom: 2.h),
                      child: Image.asset(
                        R.drawable.ic_check_true,
                        color: R.color.white,
                        width: 11.h,
                        height: 11.h,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget contentForNoneFollowing(BuildContext context, CommunityState state) {
    return ListView(
      padding: EdgeInsets.only(bottom: 50.h),
      shrinkWrap: true,
      children: [
        buildFollowingTabBar(),
        state is ActionFollowLoading
            ? headerLoadingFeed()
            : headerNoneFollow(context, state),
        sectionClubWidget(),
        sectionUserSuggestWidget(),
      ],
    );
  }

  Widget headerNoneFollow(BuildContext context, CommunityState state) {
    return Container(
      height: 200.h,
      color: R.color.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 16.h,
          ),
          Image.asset(
            R.drawable.ic_person_none,
            height: 80.h,
            width: 80.h,
          ),
          SizedBox(
            height: 16.h,
          ),
          Text(
            _cubit.posTabFollowing.name == 'BOOKMARK' &&
                    Utils.isEmpty(_cubit.listPostFollowingOrigin)
                ? R.string.save_category_have_no_post.tr()
                : _cubit.posTab == 0
                    ? R.string.please_create_post.tr()
                    : R.string.you_not_follow.tr(),
            style: Theme.of(context)
                .textTheme
                .labelSmallText
                .copyWith(color: R.color.lightShadesGray),
          ),
        ],
      ),
    );
  }

  Widget headerLoadingFeed() {
    return Container(
      height: 200.h,
      color: R.color.white,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 16.h,
          ),
          Image.asset(
            R.drawable.ic_refreshing,
            height: 80.h,
            width: 80.h,
          ),
          SizedBox(
            height: 16.h,
          ),
          Text(
            R.string.wait_a_moment.tr(),
            style: Theme.of(context)
                .textTheme
                .labelSmallText
                .copyWith(color: R.color.lightShadesGray, fontSize: 12.sp),
          ),
        ],
      ),
    );
  }

  Widget contentForContainFollowing(CommunityState state) {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        controller: _controllerFollowing,
        padding: EdgeInsets.only(bottom: 50.h),
        itemCount: _cubit.listPostFollowing.length + 2,
        separatorBuilder: (context, index) {
          if (index > 2 && state is! CommunityLoading) {
            return Container(
              height: 8.h,
              color: R.color.greyF4,
            );
          } else {
            return SizedBox();
          }
        },
        itemBuilder: (context, index) {
          if (state is CommunityLoading && index != 0) {
            return SizedBox.shrink();
          }
          if (index == 0) {
            return buildFollowingTabBar();
          } else if (index == 1) {
            if (_cubit.listPostFollowing.isEmpty)
              return Center(
                child: Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.2),
                  child: Visibility(
                    visible: _cubit.state is GetListCommunitySuccess,
                    child: Text(
                      R.string.no_data_to_show.tr() + "!",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp),
                    ),
                  ),
                ),
              );
            PostData data = _cubit.listPostFollowing[index - 1];
            return postWidget(data);
          } else if (index == 2) {
            return sectionClubWidget();
          } else {
            if (index - 2 > _cubit.listPostFollowing.length - 1)
              return Center(
                child: Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.2),
                  child: Visibility(
                    visible: _cubit.state is GetListCommunitySuccess,
                    child: Text(
                      R.string.no_data_to_show.tr() + "!",
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 14.sp),
                    ),
                  ),
                ),
              );
            PostData data = _cubit.listPostFollowing[index - 2];
            return postWidget(data);
          }
        },
      ),
    );
  }

  Widget sectionClubWidget() {
    return Visibility(
      visible: !Utils.isEmpty(_cubit.listRecommendClub),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 8.h,
            color: R.color.grey300,
            padding: EdgeInsets.symmetric(vertical: 8.h),
          ),
          rowTitle(R.string.groups_suggestion.tr(), () {
            if (checkShowLoginDialog(context)) {}
          }),
          communityWidget(),
          SizedBox(height: 16.h),
          Container(
            padding: EdgeInsets.symmetric(vertical: 8.h),
            height: 8.h,
            color: R.color.grey300,
          )
        ],
      ),
    );
  }

  Widget sectionUserSuggestWidget() {
    int length = _cubit.listRecommendUser.length;
    return Visibility(
      visible: !Utils.isEmpty(_cubit.listRecommendUser),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          rowTitle(R.string.suggestion_dcier.tr(), () {
            if (checkShowLoginDialog(context)) {}
          }),
          ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 16.h),
              itemCount: length,
              separatorBuilder: (context, index) => SizedBox(
                    height: 25.h,
                  ),
              itemBuilder: (context, index) {
                CommunityData data = _cubit.listRecommendUser[index];
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                        onTap: () {
                          NavigationUtils.navigatePage(
                              context,
                              ViewProfilePage(
                                communityId: data.id,
                              )).then((value) {
                            _cubit.getCommunityFollowing(isRefresh: true);
                          });
                        },
                        child: AvatarWidget(
                            avatar: data.avatarUrl ?? "", size: 40.h)),
                    SizedBox(width: 8.w),
                    Expanded(
                        child: GestureDetector(
                      onTap: () {
                        NavigationUtils.navigatePage(
                            context,
                            ViewProfilePage(
                              communityId: data.id,
                            )).then((value) {
                          _cubit.getCommunityFollowing(isRefresh: true);
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Text(
                          //   data.ownerProfile?.ktaMembership?.primaryClub
                          //           ?.name ??
                          //       "",
                          //   style: Theme.of(context)
                          //       .textTheme
                          //       .labelMiniText
                          //       .copyWith(color: R.color.shadesGray),
                          // ),
                          // SizedBox(
                          //   height: 4.h,
                          // ),
                          Text(data.name ?? "",
                              style: Theme.of(context).textTheme.bodySmallBold),
                          Row(
                            children: [
                              Image.asset(
                                R.drawable.ic_group_person,
                                color: R.color.black,
                                height: 12.h,
                              ),
                              SizedBox(
                                width: 6.w,
                              ),
                              Text(
                                '${Utils().formatNumberLike(data.communityInfo?.followerNo ?? 0)} ${R.string.follows.tr()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmallText
                                    .copyWith(color: R.color.shadesGray),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                    ButtonWidget(
                      title: data.userView?.isFollowing == false
                          ? R.string.follow.tr()
                          : R.string.following.tr(),
                      onPressed: () => followCommunity(data),
                      uppercaseTitle: false,
                      textSize: 12.sp,
                      radius: 8.r,
                      textColor: R.color.orange,
                      backgroundColor: R.color.white,
                      borderColor: R.color.orange,
                      textStyle: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(color: R.color.orange, height: 1.2),
                      height: 33.h,
                      width: 93.w,
                    ),
                    // InkWell(
                    //   onTap: () => followCommunity(data),
                    //   child: Container(
                    //     decoration: BoxDecoration(
                    //         borderRadius:
                    //             BorderRadius.all(Radius.circular(8.w)),
                    //         border: Border.all(
                    //             color: R.color.follow, width: 1.w)),
                    //     padding: EdgeInsets.symmetric(
                    //         horizontal: 23.w, vertical: 13.h),
                    //     child: Text(
                    //       data.userView?.isFollowing == false
                    //           ? R.string.follow.tr()
                    //           : R.string.following.tr(),
                    //       style: Theme.of(context)
                    //           .textTheme
                    //           .labelLargeText
                    //           .copyWith(color: R.color.follow, height: 1.2.h),
                    //     ),
                    //   ),
                    // ),
                  ],
                );
              }),
        ],
      ),
    );
  }

  Widget rowTitle(String title, VoidCallback? onTapAll) {
    return Container(
      padding: EdgeInsets.all(16.h),
      child: Row(
        children: [
          Expanded(
              child: Text(
            title,
            style: Theme.of(context).textTheme.bodyBold,
          )),
          Visibility(
              visible: false,
              child: GestureDetector(
                onTap: onTapAll,
                child: Text(
                  R.string.view_all.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .buttonLink
                      .copyWith(color: R.color.primaryColor),
                ),
              ))
        ],
      ),
    );
  }

  void gotoClubDetail(communityId) {
    NavigationUtils.navigatePage(
        context,
        ViewClubPage(
          communityId: communityId,
        )).then((value) {
      // _cubit.getListFollow(
      //     isRefresh: true, type: widget.type);
    });
  }

  Widget communityWidget() {
    int length = _cubit.listRecommendClub.length;
    return Container(
      height: 168.h,
      width: double.infinity,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 16.h),
        itemCount: length,
        separatorBuilder: (context, index) => SizedBox(
          width: 16.h,
        ),
        itemBuilder: (context, index) {
          CommunityData data = _cubit.listRecommendClub[index];
          bool isSelected = data.userView?.isFollowing == true;
          return Container(
            height: 168.h,
            width: 148.h,
            child: CommunityWidget(
              data: data,
              onTap: () {
                gotoClubDetail(data.id);
              },
              onTapFollow: () {
                followCommunity(data);
              },
              isSelected: isSelected,
              studyGroup: data.type == CommunityType.STUDY_GROUP,
            ),
          );
        },
      ),
    );
  }

  void followCommunity(CommunityData data) {
    if (checkShowLoginDialog(context)) {
      _cubit.followUser(data);
    }
  }

  void gotoPostDetail(PostData data) {
    if (checkShowLoginDialog(context)) {
      DciApplication.pauseVideoInDetail = true;
      NavigationUtils.rootNavigatePage(
          context,
          CommentPage(
            post: data,
            onCommentSuccess: (countComment) {
              data.commentNumber = countComment;
              _cubit.refreshCommunity();
            },
            onLikeSuccess: (like, likeNumber) {
              data.isLike = like;
              data.likeNumber = likeNumber;
              _cubit.refreshCommunity();
            },
            onRemovePostSuccess: () {
              _cubit.getCommunityTimeline(isRefresh: true);
              _cubit.getCommunityFollowing(isRefresh: true);
            },
            onBookmarkSuccess: (bool bookmark) {
              data.isBookmark = bookmark;
            },
          ));
    }
  }

  Widget postWidget(PostData data) {
    return PostWidget(
      reportPost: () {
        showBarModalBottomSheet(
            context: context,
            builder: (context) {
              return SingleChildScrollView(
                padding: EdgeInsets.all(20.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      R.string.article_report.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 16.sp),
                    ),
                    Text(
                      R.string.reason_report.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 11.sp),
                    ),
                    SizedBox(height: 10.h),
                    ListView.separated(
                      shrinkWrap: true,
                      itemCount: _cubit.listReportPost.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          onTap: () {
                            NavigationUtils.pop(context);
                            showBarModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return PopupWidget(
                                    title: R.string.article_report.tr(),
                                    reasons: R.string.post_deletion_confirmation
                                        .tr(),
                                    onReport: () {
                                      NavigationUtils.pop(context);
                                      if (data.type ==
                                          ReportReasonType.INDIVIDUAL.name) {
                                        _cubit.report(
                                            reasonId:
                                                _cubit.listReportPost[index].id,
                                            postId: data.id);
                                      } else if (data.type ==
                                          ReportReasonType.GROUP.name) {
                                        _cubit.reportGroup(
                                            reasonId:
                                                _cubit.listReportPost[index].id,
                                            postId: data.id);
                                      }
                                    },
                                  );
                                  ;
                                });
                          },
                          child: Text(
                            _cubit.listReportPost[index].reason ?? "",
                            style: Theme.of(context)
                                .textTheme
                                .bold700
                                .copyWith(fontSize: 13.sp),
                          ),
                        );
                        // }
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider();
                      },
                    ),
                  ],
                ),
              );
            });
      },
      isInPostDetails: false,
      data: data,
      isLoggedIn: _cubit.isLoggedIn,
      isShowComment: false,
      viewProfileCallback: () {
        if (checkShowLoginDialog(context) && data.communityId != null) {
          NavigationUtils.rootNavigatePage(
              context,
              ViewProfilePage(
                communityId: data.communityId,
              )).then((value) {
            if (value == Const.ACTION_CHANGE_TAB_MEMBERSHIP) {
              _mainCubit.selectTab(Const.SCREEN_MEMBERSHIP);
            }
          });
        }
      },
      viewClubCallback: () {
        if (checkShowLoginDialog(context) && data.communityV2?.id != null) {
          NavigationUtils.navigatePage(
              context,
              ViewClubPage(
                communityId: data.communityV2?.id ?? 0,
              )).then((value) {});
        }
      },
      posDetailCallback: () {
        gotoPostDetail(data);
      },
      likeCallback: () async {
        if (!(_currentState is CommunityLikePost)) {
          bool like = data.isLike == true ? false : true;
          await _cubit.reaction(int.parse(data.id!), like);
          data.isLike = like;
          data.likeNumber = like == true
              ? ((data.likeNumber ?? 0) + 1)
              : ((data.likeNumber ?? 0) - 1);
          _cubit.refreshCommunity();
        }
      },
      showLikeCallback: () {
        if (checkShowLoginDialog(context)) {
          NavigationUtils.rootNavigatePage(
              context,
              LikesPage(
                  postId: int.parse(data.id ?? "0"),
                  likeCount: data.likeNumber ?? 0));
        }
      },
      shareCallback: () {
        if (checkShowLoginDialog(context)) {}
      },
      onBookmarkSuccess: (bool bookmark) async {
        data.isBookmark = bookmark;
        _cubit.refreshCommunity();
      },
      moreCallback: () {
        if (checkShowLoginDialog(context)) {}
      },
      commentCallback: () {
        gotoPostDetail(data);
      },
      reportCallback: () {
        // _cubit.listPost.removeAt(index - 1);
        // setState(() {});
      },
    );
  }

  bool checkShowLoginDialog(BuildContext context) {
    bool isLoggedIn = _cubit.isLoggedIn;
    if (!isLoggedIn) {
      showLoginDialog();
    }
    return isLoggedIn;
  }

  // void buildConfirmLogin(BuildContext context, VoidCallback loginCallback) {
  //   showDialog<void>(
  //     context: context,
  //     barrierDismissible: false,
  //     builder: (context) {
  //       return CupertinoAlertDialog(
  //         title: Text(R.string.confirm.tr()),
  //         content: Text(R.string.you_need_to_login.tr()),
  //         actions: <Widget>[
  //           CupertinoDialogAction(
  //             child: Text(R.string.ok.tr()),
  //             onPressed: () {
  //               NavigationUtils.pop(context);
  //               loginCallback();
  //             },
  //           ),
  //           CupertinoDialogAction(
  //             child: Text(R.string.cancel.tr()),
  //             onPressed: () {
  //               NavigationUtils.pop(context);
  //             },
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }

  void showMessageDialog(BuildContext context) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(R.string.notification.tr()),
          content: Text(R.string.coming_soon.tr()),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(R.string.ok.tr()),
              onPressed: () {
                NavigationUtils.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  void showLoginDialog() {
    showBarModalBottomSheet(
        context: context,
        useRootNavigator: true,
        backgroundColor: Colors.transparent,
        expand: false,
        builder: (context) => BlocProvider<AuthenticationCubit>.value(
            value: _authCubit,
            child: SingleChildScrollView(
                child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 24.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    R.string.sign_in.tr(),
                    style: Theme.of(context).textTheme.title2,
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Text(
                    R.string.you_need_sign_in.tr(),
                    style: Theme.of(context)
                        .textTheme
                        .bodySmallText
                        .copyWith(color: R.color.shadesGray),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Container(
                    height: 1,
                    color: R.color.lightestGray,
                  ),
                  SizedBox(
                    height: 24.h,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: ButtonWidget(
                        height: 32.h,
                        textSize: 12.sp,
                        title: R.string.sign_up.tr(),
                        borderColor: R.color.black,
                        backgroundColor: R.color.white,
                        textColor: R.color.black,
                        onPressed: () {
                          // _mainCubit.refreshAuth();
                          // Navigate sign up
                          NavigationUtils.pop(context);
                          NavigationUtils.rootNavigatePage(
                              context,
                              BlocProvider.value(
                                value: _authCubit,
                                child: ForgotPinPage(
                                  type: ForgotPinType.SIGN_UP,
                                ),
                              ));
                        },
                      )),
                      SizedBox(
                        width: 16.h,
                      ),
                      Expanded(
                          child: ButtonWidget(
                        height: 32.h,
                        textSize: 12.sp,
                        title: R.string.sign_in.tr(),
                        onPressed: () {
                          // _mainCubit.refreshAuth();
                          NavigationUtils.pop(context);
                          showBarModalBottomSheet(
                              context: context,
                              backgroundColor: Colors.transparent,
                              expand: false,
                              useRootNavigator: true,
                              builder: (context) =>
                                  BlocProvider<AuthenticationCubit>.value(
                                      value: _authCubit,
                                      child: SingleChildScrollView(
                                          child: LoginPage(
                                        onRefresh: () {
                                          _mainCubit.refreshAuth();
                                        },
                                      ))));
                        },
                      )),
                    ],
                  )
                ],
              ),
            ))));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

// void login() {
//   Utils.hideKeyboard(context);
//   _cubit.verifyOtp(_phoneController.text.trim(), _pinController.text.trim());
// }
}

extension BottomReachExtension on ScrollController {
  void onBottomReach(VoidCallback callback,
      {double sensitivity = 200.0, required Duration throttleDuration}) {
    final duration = throttleDuration ?? Duration(milliseconds: 200);
    Timer? timer;

    addListener(() {
      if (timer != null) {
        return;
      }

      // I used the timer to destroy the timer
      timer = Timer(duration, () => timer = null);

      // see Esteban Díaz answer
      final maxScroll = position.maxScrollExtent;
      final currentScroll = position.pixels;
      if (maxScroll - currentScroll <= sensitivity) {
        callback();
      }
    });
  }
}
