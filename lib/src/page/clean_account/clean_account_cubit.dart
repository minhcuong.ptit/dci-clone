import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/logout_request.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/clean_account/clean_account_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';

import '../../data/preferences/app_preferences.dart';

class CleanAccountCubit extends Cubit<CleanAccountState> with Validators {
  final AppGraphqlRepository graphqlRepository;
  final AppRepository repository;

  String get phone => appPreferences.getString(Const.PHONE) ?? "";

  String get pinCode => appPreferences.getString(Const.PIN_CODE) ?? "";
  bool isTitleAppbar = false;
  String? oldPasswordError;
  bool isClean = false;

  CleanAccountCubit(this.graphqlRepository, this.repository)
      : super(CleanAccountInitial());

  void setTitleAppbar() {
    emit(CleanAccountLoading());
    isTitleAppbar = true;
    emit(CleanAccountInitial());
  }

  void validateOldPin(String oldPinCode) {
    emit(CleanAccountInitial());
    oldPasswordError = null;
    oldPasswordError = checkPin(oldPinCode);
    emit(ChangePasswordValidation());
  }

  void confirmPass(String oldPin) {
    if (state is CleanAccountLoading) {
      return;
    }
    emit(CleanAccountLoading());
    oldPasswordError = checkPin(oldPin);
    if (!Utils.isEmpty(oldPasswordError)) {
      emit(ChangePasswordValidation());
      return;
    }
    if (oldPin != pinCode) {
      oldPasswordError = R.string.set_pin_not_match.tr();
    }
    emit(CleanAccountInitial());
  }

  void cleanAccount() async {
    emit(CleanAccountLoading());
    final res = await repository.cleanAccount();
    res.when(success: (dynamic data) {
      if (data == "") {
        emit(CleanAccountSuccess());
      } else {
        emit(CleanFailure());
      }
    }, failure: (e) {
      emit(CleanAccountFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void logout() async {
    emit(CleanAccountLoading());
    ApiResult<dynamic> registerResult = await repository.logout(LogoutRequest(
        refreshToken: appPreferences.loginResponse?.refreshToken));
    registerResult.when(success: (dynamic data) {
      appPreferences.clearData();
      emit(LogoutSuccess());
    }, failure: (NetworkExceptions error) {
      appPreferences.clearData();
      emit(LogoutSuccess());
      emit(CleanAccountFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
