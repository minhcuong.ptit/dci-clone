import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/auth_user/login/login.dart';
import 'package:imi/src/page/authentication/authentication_cubit.dart';
import 'package:imi/src/page/clean_account/clean_account_cubit.dart';
import 'package:imi/src/page/clean_account/clean_account_state.dart';
import 'package:imi/src/page/clean_account/widget/clean_account_failure.dart';
import 'package:imi/src/page/clean_account/widget/clean_account_success.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/more/setting_account/setting_account.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

class ConfirmCleanAccount extends StatefulWidget {
  const ConfirmCleanAccount({Key? key}) : super(key: key);

  @override
  State<ConfirmCleanAccount> createState() => _ConfirmCleanAccountState();
}

class _ConfirmCleanAccountState extends State<ConfirmCleanAccount> {
  late CleanAccountCubit _cubit;
  late MainCubit _mainCubit;
  late AuthenticationCubit _authCubit;
  TextEditingController _oldPinController = TextEditingController();

  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = CleanAccountCubit(graphqlRepository, repository);
    _mainCubit = GetIt.I();
    _authCubit = BlocProvider.of<AuthenticationCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.confirm_delete_account.tr().toUpperCase(),
        backgroundColor: R.color.white,
        titleColor: R.color.black,
        centerTitle: true,
        iconColor: R.color.black,
      ),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<CleanAccountCubit, CleanAccountState>(
          listener: (context, state) {
            if (state is CleanAccountSuccess) {
              NavigationUtils.navigatePage(context,
                  CleanAccountSuccessWidget(() {
                _cubit.logout();
                NavigationUtils.popToFirst(context);
                _authCubit.logout();
                _mainCubit.refreshAuth();
              }));
            } else if(state is CleanAccountFailure) {
              NavigationUtils.navigatePage(context,
                  CleanAccountFailureWidget(callbackFailure: () {
                NavigationUtils.popToFirst(context);
              }));
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, CleanAccountState state) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            R.string.confirm_this_is_your_account.tr(),
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
          ),
          SizedBox(height: 12.h),
          buildTextDetail(
              R.string.before_permanently_deleting_your_account.tr()),
          buildEnterOldPassword(context),
          SizedBox(height: 40.h),
          ButtonWidget(
              backgroundColor: R.color.orange,
              padding: EdgeInsetsDirectional.fromSTEB(12.w, 8.h, 12.w, 12.h),
              title: R.string.next.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 16.sp, color: R.color.white),
              onPressed: () {
                _cubit.confirmPass(_oldPinController.text);
                if (_cubit.oldPasswordError == null) {
                  showDialog<void>(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.h))),
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        buttonPadding: EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        title: Image.asset(
                          R.drawable.ic_delete,
                          color: R.color.orange,
                          height: 44.h,
                        ),
                        content: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                R.string.confirm_permanent_account_deletion
                                    .tr(),
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 20,
                                        color: R.color.black,
                                        height: 32 / 20),
                              ),
                              Text(
                                R.string
                                    .you_are_about_to_permanently_delete_your_account
                                    .tr(),
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 16,
                                        color: R.color.gray,
                                        height: 24 / 16),
                              ),
                              SizedBox()
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ButtonWidget(
                                borderColor: R.color.primaryColor,
                                  backgroundColor: R.color.white,
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      46, 4, 46, 10),
                                  title: R.string.cancel.tr(),
                                  textStyle: Theme.of(context)
                                      .textTheme
                                      .medium500
                                      .copyWith(
                                          fontSize: 12,
                                          color: R.color.primaryColor,
                                          height: 24 / 12),
                                  onPressed: () {
                                    NavigationUtils.pop(context);
                                    NavigationUtils.pop(context);
                                    NavigationUtils.pop(context);
                                  }),
                              SizedBox(width: 10.w),
                               ButtonWidget(
                                  backgroundColor: R.color.orange,
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      8, 4, 8, 10),
                                  title: R.string.clean_account.tr(),
                                  textStyle: Theme.of(context)
                                      .textTheme
                                      .medium500
                                      .copyWith(
                                          fontSize: 12,
                                          color: R.color.white,
                                          height: 24 / 12),
                                  onPressed: () {
                                    _cubit.cleanAccount();
                                  }),
                            ],
                          )
                        ],
                      );
                    },
                  );
                }
              }),
        ],
      ),
    );
  }

  Widget buildTextDetail(String title) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .regular400
          .copyWith(fontSize: 14.sp, height: 20 / 14),
    );
  }

  Widget buildEnterOldPassword(BuildContext context) {
    return TextFieldWidget(
        autoFocus: true,
        isPassword: true,
        controller: _oldPinController,
        hintText: R.string.please_enter_new_password.tr(),
        textInputAction: TextInputAction.go,
        inputFormatters: [
          LengthLimitingTextInputFormatter(20),
        ],
        keyboardType: TextInputType.text,
        onChanged: (_) {
          _cubit.validateOldPin(_oldPinController.text);
        },
        errorText: _cubit.oldPasswordError,
        customInputStyle: Theme.of(context).textTheme.bodyBold);
  }
}
