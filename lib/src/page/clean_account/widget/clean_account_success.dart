import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

class CleanAccountSuccessWidget extends StatelessWidget {
  final VoidCallback callbackSuccess;


  CleanAccountSuccessWidget(this.callbackSuccess);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.clean_account.tr().toUpperCase(),
        backgroundColor: R.color.white,
        titleColor: R.color.black,
        centerTitle: true,
        iconColor: R.color.black,
      ),
      body: ListView(
        padding: EdgeInsets.all(20.h),
        children: [
          Text(
            R.string.your_account_has_been_deleted.tr(),
            textAlign: TextAlign.center,
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
          ),
          SizedBox(height: 100.h),
          Text(
            R.string.thank_you_for_participating_in_dci.tr(),
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 14.sp, height: 20 / 14),
          ),
          SizedBox(height: 100.h),
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 12.h),
              title: R.string.understanded.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 16.sp, color: R.color.white),
              onPressed: callbackSuccess),
        ],
      ),
    );
  }
}
