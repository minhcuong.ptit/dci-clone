import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

class CleanAccountFailureWidget extends StatelessWidget {
  final VoidCallback callbackFailure;

  CleanAccountFailureWidget({required this.callbackFailure});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.clean_account.tr().toUpperCase(),
        backgroundColor: R.color.white,
        titleColor: R.color.black,
        centerTitle: true,
        iconColor: R.color.black,
      ),
      body: ListView(
        padding: EdgeInsets.all(20.h),
        children: [
          Text(
            R.string.my_account_cannot_be_deleted.tr(),
            textAlign: TextAlign.center,
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
          ),
          SizedBox(height: 100.h),
          Text(
            R.string.you_have_not_been_able_to_delete_your_account.tr(),
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 14.sp, height: 20 / 14),
          ),
          SizedBox(height: 100.h),
          ButtonWidget(
              backgroundColor: R.color.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 12.h),
              title: R.string.understanded.tr(),
              textStyle: Theme.of(context)
                  .textTheme
                  .bold700
                  .copyWith(fontSize: 16.sp, color: R.color.white),
              onPressed: callbackFailure),
        ],
      ),
    );
  }
}
