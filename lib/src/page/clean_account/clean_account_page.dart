import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/clean_account/clean_account.dart';
import 'package:imi/src/page/clean_account/clean_account_cubit.dart';
import 'package:imi/src/page/clean_account/widget/confirm_clean_account.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

class CleanAccountPage extends StatefulWidget {
  const CleanAccountPage({Key? key}) : super(key: key);

  @override
  State<CleanAccountPage> createState() => _CleanAccountPageState();
}

class _CleanAccountPageState extends State<CleanAccountPage> {
  late CleanAccountCubit _cubit;
  late MainCubit _mainCubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _mainCubit = GetIt.I();
    _cubit = CleanAccountCubit(graphqlRepository, repository);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<CleanAccountCubit, CleanAccountState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, CleanAccountState state) {
    List<String> b =
        _cubit.phone.split(('${_cubit.phone[0]}${_cubit.phone[1]}'));
    List<String> title = [
      R.string.you_cannot_log_back_into_dci_vn.tr(),
      R.string.you_will_not_be_able_to_retrieve_the_content.tr(),
      R.string.your_profile_page_will_be_deleted.tr(),
      R.string.your_post_will_be_deleted.tr(),
      R.string.other_users_wont_find_you.tr(),
      R.string.you_can_create_a_new_account.tr()
    ];
    return Scaffold(
      appBar: appBar(
        context,
        R.string.clean_account.tr().toUpperCase(),
        backgroundColor: R.color.white,
        titleColor: R.color.black,
        centerTitle: true,
        iconColor: R.color.black,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${R.string.your_account.tr()} (+${_cubit.phone[0]}${_cubit.phone[1]}) ${b[1]}",
              style:
                  Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
            ),
            SizedBox(height: 12.h),
            buildTextDetail(
                R.string.your_account_will_be_permanently_deleted.tr()),
            Container(
              height: 300.h,
              child: ListView.builder(
                itemCount: 6,
                itemBuilder: (BuildContext context, int index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${index + 1}. ",
                        style: Theme.of(context)
                            .textTheme
                            .regular400
                            .copyWith(fontSize: 14.sp, height: 20 / 14),
                      ),
                      Expanded(
                        child: Text(
                          title[index],
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 14.sp, height: 20 / 14),
                          maxLines: 2,
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
            ButtonWidget(
                backgroundColor: R.color.orange,
                padding: EdgeInsetsDirectional.fromSTEB(12.w, 8.h, 12.w, 12.h),
                title: R.string.next.tr(),
                textStyle: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(fontSize: 16.sp, color: R.color.white),
                onPressed: () {
                  NavigationUtils.navigatePage(
                      context,
                      BlocProvider.value(
                          value: _mainCubit, child: ConfirmCleanAccount()));
                }),
            SizedBox(height: 12.h),
            ButtonWidget(
                backgroundColor: R.color.primaryColor,
                padding: EdgeInsetsDirectional.fromSTEB(12.w, 8.h, 12.w, 12.h),
                title: R.string.cancel.tr(),
                textStyle: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(fontSize: 16.sp, color: R.color.white),
                onPressed: () {
                  NavigationUtils.pop(context);
                }),
          ],
        ),
      ),
    );
  }

  Widget buildTextDetail(String title) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .regular400
          .copyWith(fontSize: 14.sp, height: 20 / 14),
    );
  }
}
