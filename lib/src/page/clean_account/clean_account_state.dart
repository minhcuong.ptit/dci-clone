abstract class CleanAccountState {
  CleanAccountState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class CleanAccountInitial extends CleanAccountState {
  @override
  String toString() => 'CleanAccountInitial';
}

class CleanAccountSuccess extends CleanAccountState {
  @override
  String toString() => 'CleanAccountSuccess';
}

class CleanAccountLoading extends CleanAccountState {
  @override
  String toString() => 'CleanAccountLoading';
}

class CleanAccountFailure extends CleanAccountState {
  final String error;

  CleanAccountFailure(this.error);

  @override
  String toString() => 'CleanAccountFailure { error: $error }';
}

class ChangePasswordValidation extends CleanAccountState {
  @override
  String toString() => 'ChangePasswordValidation';
}

class LogoutSuccess extends CleanAccountState {
  @override
  String toString() => 'LogoutSuccess';
}
class CleanFailure extends CleanAccountState {
  @override
  String toString() => 'CleanFailure';
}
class CleanSuccess extends CleanAccountState {
  @override
  String toString() => 'CleanSuccess';
}