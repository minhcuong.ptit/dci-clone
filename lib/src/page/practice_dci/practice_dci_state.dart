abstract class PracticeDCIState {
  PracticeDCIState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class PracticeDCIInitial extends PracticeDCIState {
  @override
  String toString() => 'PracticeDCIInitial';
}

class PracticeDCISuccess extends PracticeDCIState {
  @override
  String toString() => 'PracticeDCISuccess';
}

class PracticeDCILoading extends PracticeDCIState {
  @override
  String toString() => 'PracticeDCILoading';
}

class PracticeDCIFailure extends PracticeDCIState {
  final String error;

  PracticeDCIFailure(this.error);

  @override
  String toString() => 'PracticeDCIFailure { error: $error }';
}
