import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/news_details_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/practice_dci/practice_dci_state.dart';
import 'package:imi/src/utils/enum.dart';

class PracticeDCICubit extends Cubit<PracticeDCIState> {
  final AppRepository appRepository;
  final AppGraphqlRepository graphqlRepository;
  List<NewsData> listTutorial = [];
  NewsDetailsData? newsDetail;
  int? count;

  PracticeDCICubit(this.appRepository, this.graphqlRepository)
      : super(PracticeDCIInitial());

  void getNews({bool isRefresh = false, bool isLoadMore = false}) async {
    emit(isRefresh ? PracticeDCIInitial() : PracticeDCILoading());
    ApiResult<NewsDataFetch> getNewsDetail = await graphqlRepository.getNews(
        typeCategory: NewsCategory.SELF_PRACTICE, limit: 1);
    getNewsDetail.when(success: (NewsDataFetch data) {
      listTutorial = data.data ?? [];
      emit(PracticeDCISuccess());
      getDetail(data.data?.first.id ?? 0);
    }, failure: (NetworkExceptions error) async {
      emit(PracticeDCIFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getDetail(int postId) async {
    emit(PracticeDCILoading());
    ApiResult<NewsDetailsData> getNewsDetail =
        await graphqlRepository.getNewsDetail(postId: postId);
    getNewsDetail.when(success: (NewsDetailsData data) {
      newsDetail = data;
      emit(PracticeDCISuccess());
    }, failure: (NetworkExceptions error) async {
      emit(PracticeDCIFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getCount() async {
    emit(PracticeDCILoading());
    ApiResult<dynamic> getNewsDetail = await graphqlRepository.countChallenge();
    getNewsDetail.when(success: (dynamic data) {
      count = data;
      emit(PracticeDCISuccess());
    }, failure: (NetworkExceptions error) async {
      emit(PracticeDCIFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
