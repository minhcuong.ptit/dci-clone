import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/home_challenges/home_challenges_page.dart';
import 'package:imi/src/page/practice_dci/practice_dci.dart';
import 'package:imi/src/page/practice_dci/practice_dci_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../course_details/widget/describe_page.dart';
import '../setting_virtual_assistant/setting_virtual_assistant_page.dart';

class PracticeDCIPage extends StatefulWidget {
  const PracticeDCIPage({Key? key}) : super(key: key);

  @override
  State<PracticeDCIPage> createState() => _PracticeDCIPageState();
}

class _PracticeDCIPageState extends State<PracticeDCIPage> {
  late PracticeDCICubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = PracticeDCICubit(repository, graphqlRepository);
    _cubit.getNews();
    _cubit.getCount();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar: appBar(
        context,
        R.string.practice_dci.tr().toUpperCase(),
        backgroundColor: R.color.primaryColor,
        titleColor: R.color.white,
        centerTitle: true,
        iconColor: R.color.white,
      ),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<PracticeDCICubit, PracticeDCIState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! PracticeDCILoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, PracticeDCIState state) {
    return StackLoadingView(
      visibleLoading: state is PracticeDCILoading,
      child: Scrollbar(
        child: SmartRefresher(
          controller: _refreshController,
          onRefresh: () => _cubit.getNews(isRefresh: true),
          onLoading: () => _cubit.getNews(isLoadMore: true),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
            children: [
              buildPractice(
                  title: R.string
                      .set_practice_hours_according_to_the_virtual_assistant
                      .tr(),
                  callback: () {
                    NavigationUtils.navigatePage(
                        context, SettingVirtualAssistantPage());
                  }),
              SizedBox(height: 20.h),
              buildPractice(
                  title: R.string.there_are_set_of_dci_practice
                      .tr(args: ["${_cubit.count ?? 0}"]),
                  callback: () {
                    NavigationUtils.navigatePage(context, HomeChallengesPage());
                  }),
              SizedBox(height: 20.h),
              _cubit.listTutorial.length != 0
                  ? Text(
                      _cubit.newsDetail?.title ?? "",
                      style: Theme.of(context).textTheme.bold700.copyWith(
                          fontSize: 16.sp,
                          color: R.color.black,
                          height: 24.h / 16.h),
                    )
                  : SizedBox(),
              SizedBox(height: 12.h),
              _cubit.listTutorial.length != 0
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: RichText(
                            text: TextSpan(
                              text: DateUtil.parseDateToString(
                                      DateTime.fromMillisecondsSinceEpoch(
                                          _cubit.newsDetail?.createdDate ?? 0),
                                      Const.DATE_FORMAT) +
                                  " ",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, color: R.color.grey),
                              children: [
                                TextSpan(
                                  text: R.string.written_by.tr(args: [" "]),
                                ),
                                TextSpan(
                                    text: _cubit.newsDetail?.author ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(
                                            color: R.color.black
                                                .withOpacity(0.6))),
                              ],
                            ),
                          ),
                        ),
                        Text(
                          R.string.view
                              .tr(args: [": ${_cubit.newsDetail?.views}"]),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 12.sp,
                                  color: R.color.grey,
                                  height: 14.h / 12.sp),
                        ),
                      ],
                    )
                  : SizedBox(),
              SizedBox(height: 12.h),
              _cubit.listTutorial.length != 0
                  ? HtmlWidget(
                      _cubit.newsDetail?.content ?? "",
                      textStyle: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12.sp),
                      onTapUrl: (url) async {
                        if (await canLaunch(url)) {
                          await launch(
                            url,
                          );
                        } else {
                          throw 'Could not launch $url';
                        }
                        return launch(
                          url,
                        );
                      },
                      factoryBuilder: () => MyWidgetFactory(),
                      enableCaching: true,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPractice({VoidCallback? callback, String? title}) {
    return InkWell(
      onTap: callback,
      child: Container(
        height: 72.h,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(
          horizontal: 18.w,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(40.h),
            color: R.color.lightBlue),
        child: Text(
          title ?? "",
          style: Theme.of(context).textTheme.medium500.copyWith(
              fontSize: 16.sp, color: R.color.white, height: 20.h / 16.h),
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
