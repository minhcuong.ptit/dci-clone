import 'package:flutter/material.dart';

@immutable
abstract class FavoriteState {
  FavoriteState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialFavoriteState extends FavoriteState {}

class FavoriteLoading extends FavoriteState {
  @override
  String toString() => 'BodyParameterLoading';
}

class GetListCategorySuccess extends FavoriteState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends FavoriteState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class FavoriteSuccess extends FavoriteState {
  @override
  String toString() {
    return 'FavoriteSuccess';
  }
}

class ChooseCommunitySuccess extends FavoriteState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

class FavoriteFailure extends FavoriteState {
  final String error;

  FavoriteFailure(this.error);

  @override
  String toString() => 'BodyParameterFailure { error: $error }';
}

class SubmitFavoriteSuccess extends FavoriteState {
  @override
  String toString() {
    return 'SubmitFavoriteSuccess{}';
  }
}

class ChooseFavoriteSuccess extends FavoriteState {
  @override
  String toString() {
    return 'ChooseFavoriteSuccess';
  }
}
