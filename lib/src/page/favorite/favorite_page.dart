import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/choose_category/choose_category.dart';
import 'package:imi/src/page/choose_community/choose_community.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/community_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/category_widget.dart';
import '../choose_category/widget/category_header.dart';
import 'favorite.dart';

class FavoritePage extends StatefulWidget {
  final VoidCallback onRefresh;
  final bool checkPage;

  const FavoritePage(
      {Key? key, required this.onRefresh, required this.checkPage})
      : super(key: key);

  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  late FavoriteCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  var _columnNumber = 2;
  var _rowNumber = 5;

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = FavoriteCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.getListContent();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(true),
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<FavoriteCubit, FavoriteState>(
          listener: (context, state) {
            if (state is! FavoriteLoading) {
              _refreshController.refreshCompleted();
            }
            if (state is! SubmitFavoriteSuccess) {
              widget.onRefresh();
            }
            if (state is FavoriteFailure)
              Utils.showErrorSnackBar(context, state.error);
            // if (state is ChooseFavoriteSuccess) {
            //   _cubit.getListContent();
            // }
          },
          builder: (context, state) {
            return _buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, FavoriteState state) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: R.color.white,
      body: StackLoadingView(
        visibleLoading: state is FavoriteLoading,
        child: Column(
          children: [
            Stack(
              children: [
                CategoryHeader(),
                widget.checkPage == true
                    ? Positioned(
                        top: 48.h,
                        left: 20.w,
                        child: InkWell(
                            onTap: () => NavigationUtils.pop(context),
                            child: Icon(CupertinoIcons.left_chevron,
                                color: R.color.white)))
                    : SizedBox()
              ],
            ),
            Expanded(
              child: SmartRefresher(
                  controller: _refreshController,
                  onRefresh: () => _cubit.getListContent(isRefresh: true),
                  child: _buildListItem()),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(24.w, 8.h, 24.w, 0.h),
            child: Text(
              R.string.choose_your_favorite_hint.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .labelSmallText
                  .apply(color: R.color.grey)
                  .copyWith(fontSize: 14.sp, height: 24 / 16),
            ),
          ),
          SizedBox(height: 16.h),
          _categoryWidget(),
          SizedBox(height: 40.h),
          Text(
            R.string.groups_for_you.tr(),
            style: Theme.of(context).textTheme.headline3,
          ),
          Text(
            R.string.choose_the_best_community.tr(),
            style: Theme.of(context)
                .textTheme
                .bodySmallText
                .copyWith(color: R.color.grey),
          ),
          SizedBox(height: 24.h),
          Row(
            children: [
              Text(R.string.your_group.tr(),
                  style: Theme.of(context).textTheme.bodySmallText.copyWith(
                      color: R.color.black,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500)),
              Spacer(),
              InkWell(
                onTap: () async {
                  _cubit.applyFavoriteCategory();
                  var result = await Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => ChooseCommunityPage(
                              listCommunity: _cubit.listCommunity,
                              listSelectedCommunity:
                                  _cubit.listSelectedCommunity,
                            )),
                  );
                  if (result != null) {
                    _cubit.updateCommunity(result);
                  }
                },
                child: Text(R.string.view_all.tr(),
                    style: Theme.of(context).textTheme.bodySmallText.copyWith(
                        color: R.color.readMore,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500)),
              )
            ],
          ),
          SizedBox(height: 16.h),
          communityWidget(),
          SizedBox(height: 36.h),
          Center(
            child: SizedBox(
              width: 250.w,
              child: ButtonWidget(
                  textSize: 16.sp,
                  height: 48.h,
                  uppercaseTitle: true,
                  padding: EdgeInsets.only(top: 5.h),
                  backgroundColor: R.color.primaryColor,
                  title: R.string.save.tr(),
                  onPressed: _cubit.listSelectedCategory.length < 3
                      ? null
                      : () {
                          appPreferences.setData(
                              Const.FAVORITE_CATEGORY,
                              _cubit.listSelectedCategory
                                  .map((e) => e.id)
                                  .toList());
                          appPreferences.setData(
                              Const.FAVORITE_COMMUNITY,
                              _cubit.listSelectedCommunity
                                  .map((e) => e.id)
                                  .toList());
                          _cubit.submitFavorite();
                          if (widget.checkPage == true) {
                            NavigationUtils.pop(context);
                          }
                          //
                          // BlocProvider.of<AuthenticationCubit>(context)
                          //     .categoryToHome();
                        }),
            ),
          ),
          SizedBox(
            height: 120.h,
          )
        ],
      ),
    );
  }

  Widget _categoryWidget() {
    if (_cubit.listCategory.isEmpty) {
      return SizedBox();
    }
    var listWidget = <Widget>[];
    for (var i = 0; i < _rowNumber; i++) {
      listWidget.add(Padding(
        padding: EdgeInsets.only(top: 16.h),
        child: _buildRow(context, i),
      ));
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: listWidget,
      ),
    );
  }

  Widget _buildRow(BuildContext context, int line) {
    var list = <Widget>[];
    for (var i = _columnNumber * line; i < _cubit.listCategory.length; i++) {
      list.add(Expanded(child: _buildItemOfRow(context, i)));
      if (list.length >= _columnNumber) break;
    }
    while (list.length < _columnNumber) {
      list.add(Expanded(child: Container()));
    }
    list.insert(
        1,
        SizedBox(
          width: 16.w,
        ));
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: list);
  }

  Widget _buildItemOfRow(BuildContext context, index) {
    CategoryData e = _cubit.listCategory[index];
    bool isSelected = _cubit.listSelectedCategory
            .indexWhere((element) => element.id == e.id) >=
        0;
    return CategoryWidget(
      data: e,
      //isLoadMore: _isLoadMore,
      isSelected: isSelected,
      onTap: () async {
        _cubit.applyFavoriteCategory();
        _cubit.selectCategory(e);

        //   var result = await Navigator.of(context).push(
        //     MaterialPageRoute(
        //         builder: (context) => ChooseCategoryPage(
        //               listCategory: _cubit.listCategory,
        //               listSelectedCategory: _cubit.listSelectedCategory,
        //             )),
        //   );
        //   if (result != null) {
        //     _cubit.updateCategory(result);
        //
        // }
      },
      shadow: BoxShadow(
        color: R.color.shadowColor,
        spreadRadius: 0,
        blurRadius: 3.h,
        offset: Offset(0, 3), // changes position of shadow
      ),
    );
  }

  Widget communityWidget() {
    final maxWidthOfCell = (ScreenUtil().screenWidth - 12.w * 2 - 16.w) / 2.2;
    return Container(
      height: 170.h,
      child: ListView.separated(
        primary: false,
        shrinkWrap: true,
        separatorBuilder: (context, index) {
          return SizedBox(
            width: 12.w,
          );
        },
        scrollDirection: Axis.horizontal,
        physics: ClampingScrollPhysics(),
        itemCount: _cubit.listCommunity.length,
        itemBuilder: (context, index) {
          CommunityData data = _cubit.listCommunity[index];
          bool isSelected = _cubit.listSelectedCommunity
                  .indexWhere((element) => element.id == data.id) >=
              0;
          return CommunityWidget(
            width: maxWidthOfCell,
            data: data,
            onTap: data.type != CommunityType.STUDY_GROUP
                ? () {
                    _cubit.follow(data);
                  }
                : () {},
            isSelected: isSelected,
            isShowStar: true,
            studyGroup: data.type == CommunityType.STUDY_GROUP,
          );
        },
      ),
    );
  }
}
