import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/list_content_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/network/request/submit_favorite_request.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import '../../utils/logger.dart';
import 'favorite.dart';

class FavoriteCubit extends Cubit<FavoriteState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CategoryData> listCategory = [];
  List<CategoryData> listSelectedCategory = [];
  List<CommunityData> listCommunity = [];
  List<CommunityData> listSelectedCommunity = [];
  List<int> get listFavoriteCategory => appPreferences
      .getListString(Const.SAVE_CATEGORY)
      .map((e) => int.parse(e))
      .toList();
  final favoriteCategory = appPreferences
      .getListString(Const.FAVORITE_CATEGORY)
      .map((e) => int.parse(e))
      .toList();
  FavoriteCubit({required this.repository, required this.graphqlRepository})
      : super(InitialFavoriteState());


  void getListContent({bool isRefresh = false}) async {
    emit(isRefresh ? InitialFavoriteState() : FavoriteLoading());
    ApiResult<ListContentResponse> contentTask =
        await graphqlRepository.getListContent(listFavoriteCategory);
    contentTask.when(success: (ListContentResponse data) async {
      listCommunity = data.communities?.data ?? [];
      listCategory = data.categories ?? [];
      emit(GetListCommunitySuccess());
      listSelectedCategory = listCategory
          .where((item) => appPreferences
              .getListString(Const.FAVORITE_CATEGORY)
              .contains(item.id.toString()))
          .toList();
    }, failure: (NetworkExceptions error) async {
      emit(FavoriteFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectCategory(CategoryData selectedCategory) {
    emit(FavoriteLoading());
    int index = listSelectedCategory
        .indexWhere((element) => element.id == selectedCategory.id);
    if (index >= 0) {
      listSelectedCategory.removeAt(index);
    } else {
      listSelectedCategory.add(selectedCategory);
    }
    emit(ChooseFavoriteSuccess());
  }

  void updateCategory(List<CategoryData> selectedCategory) {
    listSelectedCategory = selectedCategory;
    List<CategoryData> selectedCategoryUncheck = [];
    listCategory.forEach((item) {
      int index =
          listSelectedCategory.indexWhere((element) => element.id == item.id);
      if (index < 0) {
        selectedCategoryUncheck.add(item);
      }
    });
    listCategory.clear();
    listCategory.addAll(listSelectedCategory);
    listCategory.addAll(selectedCategoryUncheck);
    emit(FavoriteSuccess());
  }

  void applyFavoriteCategory({bool isCancel = false}) {
    emit(FavoriteLoading());
    if (isCancel)
      listSelectedCategory = listCategory
          .where((element) => listFavoriteCategory.contains(element.id))
          .toList();
    appPreferences.setData(
        Const.SAVE_CATEGORY, (listSelectedCategory).map((e) => e.id).toList());
    emit(FavoriteSuccess());
  }

  int mySortComparison(CategoryData a, CategoryData b) {
    return (a.isSelected == b.isSelected) ? -1 : 1;
  }

  void updateCommunity(List<CommunityData> selectedCommunity) {
    listSelectedCommunity = selectedCommunity;
    emit(FavoriteSuccess());
  }

  // void selectCommunity(CommunityData selectedCommunity) {
  //   emit(FavoriteLoading());
  //   int index = listSelectedCommunity
  //       .indexWhere((element) => element.id == selectedCommunity.id);
  //   if (index >= 0) {
  //     listSelectedCommunity.removeAt(index);
  //   } else {
  //     listSelectedCommunity.add(selectedCommunity);
  //   }
  //   emit(FavoriteSuccess());
  // }

  void submitFavorite() async {
    emit(FavoriteLoading());
    try {
      List<int> listSelectedCategoryId =
          listSelectedCategory.map((e) => e.id!).toList();
      List<int> listSelectedCommunityId =
          listSelectedCommunity.map((e) => e.id!).toList();
      SubmitFavoriteRequest request = SubmitFavoriteRequest(
        categories: listSelectedCategoryId,
        communities: listSelectedCommunityId,
      );
      ApiResult<dynamic> submitFavoriteTask =
          await repository.submitFavorite(request);
      submitFavoriteTask.when(success: (dynamic data) async {
        emit(SubmitFavoriteSuccess());
        //getHomeTimeline(isRefresh: true);
      }, failure: (NetworkExceptions error) async {
        emit(FavoriteFailure(NetworkExceptions.getErrorMessage(error)));
      });
    } catch (e) {
      logger.e(e);
      //emit(HomeInitial());
    }
  }

  void follow(CommunityData? communityData) async {
    int index = listSelectedCommunity
        .indexWhere((element) => element.id == communityData?.id);
    if (index >= 0) {
      listSelectedCommunity.removeAt(index);
    } else {
      listSelectedCommunity.add(communityData!);
    }
    emit(FavoriteLoading());
    bool isFollowing = communityData?.userView?.isFollowing == true;
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityData?.id,
        action: isFollowing ? Const.UNFOLLOW : Const.FOLLOW));
    result.when(success: (dynamic response) async {
      emit(GetListCommunitySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(FavoriteFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
