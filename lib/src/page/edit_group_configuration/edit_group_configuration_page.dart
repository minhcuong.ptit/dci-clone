import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/request/admin_update_community_request.dart';
import '../../utils/const.dart';
import '../../utils/date_util.dart';
import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/text_field_widget.dart';
import 'edit_group_configuration.dart';

class EditGroupConfigurationPage extends StatefulWidget {
  final String typeOfGroup;
  final int communityId;

  const EditGroupConfigurationPage(
      {Key? key, required this.typeOfGroup, required this.communityId})
      : super(key: key);

  @override
  State<EditGroupConfigurationPage> createState() =>
      _EditGroupConfigurationPageState();
}

class _EditGroupConfigurationPageState
    extends State<EditGroupConfigurationPage> {
  DateFormat dateFormat = DateFormat("dd-MM-yyyy");
  DateFormat timeFormat = DateFormat("kk:mm");
  OverlayEntry? entry;
  final layerLink = LayerLink();
  late EditGroupConfigurationCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = EditGroupConfigurationCubit(repository, graphqlRepository);
    _cubit.getGroupInfo(widget.communityId);
  }

  @override
  void dispose() {
    _cubit.groupNameController.dispose();
    _cubit.descriptionController.dispose();
    super.dispose();
  }

  void showDropDownMenu() {
    final dropDown = Overlay.of(context)!;
    final renderBox = context.findRenderObject() as RenderBox;
    final size = renderBox.size;
    final offset = renderBox.globalToLocal(Offset.zero);
    entry = OverlayEntry(
      builder: (BuildContext context) => Positioned(
        width: size.width,
        child: CompositedTransformFollower(
          link: layerLink,
          showWhenUnlinked: false,
          offset: Offset(-30, offset.dy + 35),
          child: _buildDropDown(),
        ),
      ),
    );
    dropDown.insert(entry!);
  }

  void hideDropDown() {
    entry?.remove();
    entry = null;
  }

  Widget _buildDropDown() {
    return Material(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 24),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 1),
                spreadRadius: 1,
                color: Colors.black.withOpacity(0.08),
              ),
            ],
            border: Border.all(width: 0.05, color: R.color.grey),
            color: R.color.white,
            borderRadius: BorderRadius.circular(4)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _cubit.listCategory
              .asMap()
              .entries
              .map(
                (e) => ValueListenableBuilder(
                    valueListenable: _cubit.listCheckBoxCategory,
                    builder: (context, List<bool> value, _) {
                      return GestureDetector(
                        onTap: () {
                          _cubit.listCheckBoxCategory.value[e.key] =
                              !_cubit.listCheckBoxCategory.value[e.key];
                          if (_cubit.listCheckBoxCategory.value[e.key]) {
                            _cubit.pickedCategory.value.add(Category(
                                id: _cubit.listCategory[e.key].id!,
                                category: _cubit.listCategory[e.key].name!,
                                index: e.key));
                          } else {
                            _cubit.pickedCategory.value.removeWhere(
                                (element) => element.category == e.value.name);
                          }
                          _cubit.pickedCategoryNotifier.value =
                              List.from(_cubit.pickedCategory.value);
                          hideDropDown();
                        },
                        child: Container(
                            padding: EdgeInsets.only(
                                top: 5.h, bottom: 5.h, left: 15.w),
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              children: [
                                value[e.key]
                                    ? Icon(
                                        Icons.check_box,
                                        size: 20.h,
                                        color: R.color.primaryColor,
                                      )
                                    : Icon(
                                        Icons.check_box_outline_blank,
                                        size: 20.h,
                                        color: R.color.grey,
                                      ),
                                SizedBox(width: 8.h),
                                Text(e.value.name.toString(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodySmallText
                                        .copyWith(
                                            color: value[e.key]
                                                ? R.color.primaryColor
                                                : R.color.grey)),
                              ],
                            )),
                      );
                    }),
              )
              .toList(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) => _cubit,
        child: Scaffold(
          appBar: appBar(
            context,
            R.string.edit_group_configuration.tr(),
            centerTitle: true,
            leadingWidget: InkWell(
              onTap: () {
                hideDropDown();
                NavigationUtils.pop(context);
              },
              child: Icon(CupertinoIcons.back),
            ),
          ),
          body: GestureDetector(
            onTap: () {
              hideDropDown();
            },
            child: BlocConsumer<EditGroupConfigurationCubit,
                    EditGroupConfigurationState>(
                listener: ((context, state) {
                  if (state is SelectedStartDateSuccess) {
                    _cubit.dateStart = DateUtil.parseDateToString(
                        _cubit.startDate, Const.DATE_FORMAT_REQUEST);
                  }
                  if (state is SelectedEndDateSuccess) {
                    _cubit.dateEnd = DateUtil.parseDateToString(
                        _cubit.endDate, Const.DATE_FORMAT_REQUEST);
                  }
                  if (state is SelectedStartTimeSuccess) {
                    _cubit.timeStart = DateUtil.parseDateToString(
                        _cubit.startTime, Const.TIME_FORMAT);
                  }
                  if (state is SelectedEndTimeSuccess) {
                    _cubit.timeEnd = DateUtil.parseDateToString(
                        _cubit.endTime, Const.TIME_FORMAT);
                  }
                }),
                builder: ((context, state) => _buildPage(context, state))),
          ),
        ));
  }

  Widget _buildPage(BuildContext context, state) {
    Size size = MediaQuery.of(context).size;
    return StackLoadingView(
      visibleLoading: state is EditGroupConfigurationLoading,
      child: state is EditGroupConfigurationLoading
          ? SizedBox.shrink()
          : Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      R.string.group_name.tr(),
                      style: Theme.of(context).textTheme.bodySmallText.copyWith(
                            color: R.color.shadesGray,
                          ),
                    ),
                    TextFieldWidget(
                        autoFocus: false,
                        controller: _cubit.groupNameController,
                        maxLines: 1,
                        underLineColor: R.color.grey300),
                    SizedBox(
                      height: 20.h,
                    ),
                    Text(
                      R.string.categories.tr(),
                      style: Theme.of(context).textTheme.bodySmallText.copyWith(
                            color: R.color.shadesGray,
                          ),
                    ),
                    _category(),
                    SizedBox(
                      height: 4.h,
                    ),
                    Divider(
                      thickness: 1,
                      height: 0,
                    ),
                    SizedBox(
                      height: 15.h,
                    ),
                    Text(
                      R.string.group_description.tr(),
                      style: Theme.of(context).textTheme.bodySmallText.copyWith(
                            color: R.color.shadesGray,
                          ),
                    ),
                    TextFieldWidget(
                        autoFocus: false,
                        controller: _cubit.descriptionController,
                        minLines: 1,
                        maxLines: 2,
                        underLineColor: R.color.grey300),
                    SizedBox(
                      height: 15.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          R.string.affiliate_link.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmallText
                              .copyWith(
                                color: R.color.shadesGray,
                              ),
                        ),
                        GestureDetector(
                            onTap: () => _cubit.addLink(),
                            child: Image.asset(
                              R.drawable.ic_add_circle,
                              width: 24.w,
                            ))
                      ],
                    ),
                    _addLink(),
                    widget.typeOfGroup == CommunityType.STUDY_GROUP.name
                        ? _studyGroup(state)
                        : SizedBox(
                            height: _cubit.link.length != 0
                                ? size.height * 0.3
                                : size.height * 0.32,
                          ),
                    SizedBox(
                      height: _cubit.link.length != 0
                          ? size.height * 0.05
                          : size.height * 0.12,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 24.w, right: 24.w, bottom: 30.h),
                      child: ButtonWidget(
                          title: R.string.save_changes.tr(),
                          height: 48.h,
                          backgroundColor: R.color.secondaryButtonColor,
                          uppercaseTitle: true,
                          textStyle:
                              Theme.of(context).textTheme.title2.copyWith(
                                    color: R.color.white,
                                  ),
                          onPressed: () {
                            _cubit.updateInfoGroup(
                              widget.communityId,
                              AdminUpdateCommunity(
                                name: _cubit.groupNameController.text,
                                description: _cubit.descriptionController.text,
                                contentPrefence: _cubit.pickedCategory.value
                                    .map((e) => e.id)
                                    .toList(),
                                memberLimit:
                                    int.parse(_cubit.maxMemberController.text),
                                socialLinks: _cubit.link
                                    .map((e) => SocialLinks(
                                        name: e.name.text, url: e.link.text))
                                    .toList(),
                                groupEvent: GroupEvent(
                                    startDate: _cubit.startDate != null
                                        ? _cubit
                                            .startDate!.millisecondsSinceEpoch
                                        : _cubit.groupInfo?.groupView?.event
                                            ?.startDate,
                                    endDate:
                                        _cubit.endDate?.millisecondsSinceEpoch ??
                                            _cubit.groupInfo?.groupView?.event
                                                ?.endDate,
                                    dayOfWeek: _cubit.isPickedDay
                                        ? _cubit.postDay
                                        : _cubit.groupInfo?.groupView?.event
                                            ?.dayOfWeek?.name
                                            .toString(),
                                    startTime: _cubit.startTime
                                            ?.millisecondsSinceEpoch ??
                                        _cubit.groupInfo?.groupView?.event
                                            ?.startTime,
                                    endTime: _cubit
                                            .endTime?.millisecondsSinceEpoch ??
                                        _cubit.groupInfo?.groupView?.event?.endTime),
                              ),
                              context,
                            );
                          }),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget _addLink() {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: _cubit.link.length,
      itemBuilder: (context, index) {
        return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(
            child: Column(
              children: [
                TextFieldWidget(
                    autoFocus: false,
                    controller: _cubit.link[index].name,
                    hintText: R.string.name.tr(),
                    maxLines: 1,
                    underLineColor: Colors.transparent),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Image.asset(
                      R.drawable.ic_link,
                      width: 20.w,
                    ),
                    SizedBox(
                      width: 3.w,
                    ),
                    Expanded(
                      child: TextFieldWidget(
                          autoFocus: false,
                          controller: _cubit.link[index].link,
                          maxLines: 1,
                          customInputStyle:
                              Theme.of(context).textTheme.bodySmallText.copyWith(
                                    color: R.color.blue,
                                  ),
                          underLineColor: Colors.transparent),
                    ),
                  ],
                ),
                Divider(
                  thickness: 1,
                  height: 0,
                ),
                SizedBox(
                  height: 10.h,
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () => _cubit.deleteLink(index),
            child: Container(
              margin: EdgeInsets.only(top: 5.h),
              child: Image.asset(
                R.drawable.ic_remove,
                width: 24.w,
              ),
            ),
          ),
        ]);
      },
    );
  }

  Widget _studyGroup(state) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            R.string.living_time.tr(),
            style: Theme.of(context).textTheme.bodySmallText.copyWith(
                  color: R.color.shadesGray,
                ),
          ),
        ),
        SizedBox(
          height: 15.h,
        ),
        _pickDate(true),
        Divider(
          thickness: 1,
        ),
        _messageError(
            isValid: _cubit.isValidDate,
            message: R.string.end_date_cannot_less_start_date.tr()),
        SizedBox(
          height: 15.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
                _cubit.isPickedDay
                    ? _cubit.dailyActivities
                    : _cubit.groupInfo?.groupView?.event?.dayOfWeek?.title
                            .toString() ??
                        '',
                style: Theme.of(context).textTheme.bodySmallText),
            GestureDetector(
                onTap: _cubit.showDay, child: Icon(Icons.arrow_drop_down_sharp))
          ],
        ),
        Divider(
          thickness: 1,
        ),
        state is ShowDayofWeek
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: _cubit.week
                    .asMap()
                    .entries
                    .map((e) => GestureDetector(
                          onTap: () {
                            _cubit.pickDailyActivities(e.key, e.value);
                          },
                          child: ValueListenableBuilder(
                            valueListenable: _cubit.dayStart,
                            builder: (context, value, _) => Container(
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: _cubit.dayStart.value == e.key
                                    ? R.color.challengeBackgroundBlue
                                    : R.color.white,
                                borderRadius: BorderRadius.circular(2),
                              ),
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 4),
                                child: Text(
                                  e.value.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodySmallText
                                      .copyWith(
                                          color: _cubit.dayStart.value == e.key
                                              ? R.color.white
                                              : R.color.black),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ))
                    .toList())
            : SizedBox(),
        SizedBox(
          height: 15.h,
        ),
        _pickDate(false),
        Divider(
          thickness: 1,
        ),
        _messageError(
            isValid: _cubit.isValidTime,
            message: R.string.end_time_cannot_less_start_time.tr()),
        SizedBox(
          height: 15.h,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            R.string.maximum_members.tr(),
            style: Theme.of(context).textTheme.bodySmallText.copyWith(
                  color: R.color.shadesGray,
                ),
          ),
        ),
        TextFieldWidget(
            keyboardType: TextInputType.number,
            controller: _cubit.maxMemberController,
            autoFocus: false,
            maxLines: 1,
            underLineColor: R.color.grey300),
      ],
    );
  }

  Widget _pickDate(bool isDate) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () => isDate
              ? _cubit.pickDob(context, 'startTime')
              : _cubit.pickTime(context, 'startTime'),
          child: Text(isDate ? _cubit.dateStart : _cubit.timeStart,
              style: Theme.of(context).textTheme.bodySmallText),
        ),
      SizedBox(
        width: 150.w,
        child: Row(
          children: [
            Image.asset(
              R.drawable.ic_arrow_right,
              width: 15,
            ),
            Expanded(
              child: GestureDetector(
                onTap: () => isDate
                    ? _cubit.pickDob(context, 'endTime')
                    : _cubit.pickTime(context, 'endTime'),
                child: Padding(
                    padding: EdgeInsets.only(left: 10.h),
                    child: Text(isDate ? _cubit.dateEnd : _cubit.timeEnd,
                        style: Theme.of(context).textTheme.bodySmallText)),
              ),
            ),
            Image.asset(
              isDate ? R.drawable.ic_calendar : R.drawable.ic_bx_time,
              width: 20,
            )
          ],
        ),
      )
      ],
    );
  }

  Widget _messageError({required bool isValid, required String message}) {
    return !isValid
        ? Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                R.drawable.ic_alert_infor,
                width: 24.w,
              ),
              Text(
                message,
                style: Theme.of(context)
                    .textTheme
                    .bodySmallText
                    .copyWith(color: R.color.red, fontSize: 12),
              ),
            ],
          )
        : SizedBox.shrink();
  }

  Widget _category() {
    return CompositedTransformTarget(
      link: layerLink,
      child: Row(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            height: 30.h,
            child: ValueListenableBuilder(
                valueListenable: _cubit.pickedCategoryNotifier,
                builder: (context, List<Category> value, _) {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: value.length,
                    itemBuilder: (context, index) {
                      return Container(
                          margin: EdgeInsets.only(left: 2.w),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: R.color.periBlue,
                              borderRadius: BorderRadius.circular(50)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 3.5),
                                  child: Text(value[index].category.toString(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodySmallText
                                          ),
                                ),
                                SizedBox(
                                  width: 5.w,
                                ),
                                GestureDetector(
                                    onTap: () {
                                      _cubit.pickedCategory.value
                                          .removeAt(index);
                                      for (int i = 0;
                                          i <
                                              _cubit.listCategoryNotifier.value
                                                  .length;
                                          i++) {
                                        _cubit.listCategoryNotifier.value[i] =
                                            false;
                                      }
                                      for (int i = 0;
                                          i <
                                              _cubit
                                                  .pickedCategory.value.length;
                                          i++) {
                                        _cubit.listCategoryNotifier.value[_cubit
                                            .pickedCategory
                                            .value[i]
                                            .index] = true;
                                      }
                                      _cubit.listCheckBoxCategory.value =
                                          List.from(_cubit
                                              .listCategoryNotifier.value);
                                      _cubit.pickedCategoryNotifier.value =
                                          List.from(
                                              _cubit.pickedCategory.value);
                                    },
                                    child: Image.asset(
                                      R.drawable.ic_cancel,
                                      width: 13.w,
                                    ))
                              ],
                            ),
                          ));
                    },
                  );
                }),
          ),
          GestureDetector(
              onTap: showDropDownMenu,
              child: Image.asset(
                R.drawable.ic_add_circle,
                width: 24.w,
              )),
        ],
      ),
    );
  }
}
