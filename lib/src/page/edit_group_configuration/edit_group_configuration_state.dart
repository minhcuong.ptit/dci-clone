import 'package:equatable/equatable.dart';

abstract class EditGroupConfigurationState extends Equatable {
  @override
  List<Object> get props => [];
}

class EditGroupConfigurationInitial extends EditGroupConfigurationState {}

class EditGroupConfigurationLoading extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'EditGroupConfigurationLoading{}';
  }
}

class EditGroupConfigurationFailure extends EditGroupConfigurationState {
  final String error;

  EditGroupConfigurationFailure(this.error);

  @override
  String toString() {
    return 'EditGroupConfigurationFailure{error: $error}';
  }
}

class EditGroupConfigurationSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'EditGroupConfigurationSuccess{}';
  }
}

class EditGroupConfigurationEmpty extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'EditGroupConfigurationEmpty{}';
  }
}


class ShowDayofWeek extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'ShowDayofWeek{}';
  }
}

class HideDayofWeek extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'HideDayofWeek{}';
  }
}

class SelectedStartDateSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'SelectedDateSuccess{}';
  }
}
class SelectedEndDateSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'SelectedDateSuccess{}';
  }
}
class SelectedStartTimeSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'SelectedStartTimeSuccess{}';
  }
}
class SelectedEndTimeSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'SelectedEndTimeSuccess{}';
  }
}

class SelectedDateFail extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'SelectedDateFail{}';
  }
}
class SelectedTimeFail extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'SelectedTimeFail{}';
  }
}

class addLinkSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'addLinkSuccess{}';
  }
}
class adminUpdateSuccess extends EditGroupConfigurationState {
  @override
  String toString() {
    return 'adminUpdateSuccess{}';
  }
}
