import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:imi/src/data/network/request/admin_update_community_request.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/category_data.dart';
import '../../data/network/response/community_data.dart';
import '../../data/network/response/list_content_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import 'edit_group_configuration_state.dart';

class EditGroupConfigurationCubit extends Cubit<EditGroupConfigurationState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  CommunityDataV2? groupInfo;
  AdminUpdateCommunity? admin;
  DateFormat dateFormat = DateFormat("dd/MM/yyyy");
  DateFormat timeFormat = DateFormat("kk:mm");
  TextEditingController groupNameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController maxMemberController = TextEditingController();
  bool isPickedDay = false;
  bool showDayOfWeek = false;
  bool isValidDate = true;
  bool isValidTime = true;
  ValueNotifier<int> dayStart = ValueNotifier(7);
  String dailyActivities = R.string.choose_weekly_schedule.tr();
  String? postDay;
  String dateStart = R.string.choose_date_start.tr();
  String dateEnd = R.string.end.tr();
  String timeStart = R.string.choose_time_start.tr();
  String timeEnd = R.string.end.tr();
  DateTime? startTime;
  DateTime? endTime;
  DateTime? startDate;
  DateTime? endDate;
  ValueNotifier<List<Category>> pickedCategoryNotifier = ValueNotifier([]);
  ValueNotifier<List<Category>> pickedCategory = ValueNotifier([]);
  ValueNotifier<List<bool>> listCheckBoxCategory =
      ValueNotifier(List.generate(10, (index) => false));
  ValueNotifier<List<bool>> listCategoryNotifier =
      ValueNotifier(List.generate(10, (index) => false));
  List<DayOfWeekTitle> week = [
    DayOfWeekTitle.T2,
    DayOfWeekTitle.T3,
    DayOfWeekTitle.T4,
    DayOfWeekTitle.T5,
    DayOfWeekTitle.T6,
    DayOfWeekTitle.T7,
    DayOfWeekTitle.CN
  ];
  List<Link> link = [];
  List<SocialLinks> socialLinks = [];

  List<int> get listFavoriteCategory => appPreferences
      .getListString(Const.SAVE_CATEGORY)
      .map((e) => int.parse(e))
      .toList();
  List<CategoryData> listCategory = [];

  EditGroupConfigurationCubit(this.repository, this.graphqlRepository)
      : super(EditGroupConfigurationInitial());

  void getMaxMember(int communityId) async {
    ApiResult<AdminUpdateCommunity> result =
        await repository.getGroupInfo(communityId);
    result.when(success: (AdminUpdateCommunity data) {
      admin = data;
      maxMemberController.text = admin?.memberLimit?.toString() ?? 0.toString();
      if (admin?.memberLimit == null) {
        maxMemberController.text = '0';
      }
      getListContent();
    }, failure: (NetworkExceptions error) async {
      emit(EditGroupConfigurationFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getGroupInfo(int communityId) async {
    emit(EditGroupConfigurationLoading());
    ApiResult<CommunityDataV2Response> result =
        await graphqlRepository.getCommunityV2(communityId );
    result.when(success: (CommunityDataV2Response data) async {
      groupInfo = data.communityV2;
      groupNameController.text = groupInfo?.name ?? '';
      descriptionController.text = groupInfo?.description ?? '';
      initDateTime();
      if (groupInfo?.socialLinks?.isEmpty ?? true) {
        link.add(
            Link(name: TextEditingController(), link: TextEditingController()));
      }
      for (int i = 0; i < groupInfo!.socialLinks!.length; i++) {
        link.add(
            Link(name: TextEditingController(), link: TextEditingController()));
        link[i].name.text = groupInfo?.socialLinks?[i]?.name ?? '';
        link[i].link.text = groupInfo?.socialLinks?[i]?.url ?? '';
      }
      dayStart.value=groupInfo?.groupView?.event?.dayOfWeek?.index??7;
      getMaxMember(communityId);
    }, failure: (NetworkExceptions error) async {
      emit(EditGroupConfigurationFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListContent({bool isRefresh = false}) async {
    ApiResult<ListContentResponse> contentTask =
        await graphqlRepository.getListContent(listFavoriteCategory);
    contentTask.when(success: (ListContentResponse data) async {
      listCategory = data.categories ?? [];
      for (int i = 0; i < listCategory.length; i++) {
        for (int j = 0; j < groupInfo!.categories!.length; j++) {
          if (listCategory[i].name == groupInfo?.categories?[j]?.name) {
            listCheckBoxCategory.value[i] = true;
            pickedCategoryNotifier.value.add(Category(
              category: groupInfo?.categories?[j]?.name ?? '',
              index: i,
              id: groupInfo?.categories?[j]?.id ?? 0,
            ));
            pickedCategory.value.add(Category(
                category: groupInfo?.categories?[j]?.name ?? '',
                index: i,
                id: groupInfo?.categories?[j]?.id ?? 0));
            break;
           } else {
            listCheckBoxCategory.value[i] = false;
          }
        }
      }
      emit(EditGroupConfigurationSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(EditGroupConfigurationFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  void updateInfoGroup(
      int communityId, AdminUpdateCommunity request, context) async {
    emit(EditGroupConfigurationLoading());
    var res = await repository.adminupdateCommunity(request, communityId);
    res.when(success: (_) async {
      adminUpdateSuccess();
      NavigationUtils.pop(context);
    }, failure: (NetworkExceptions error) async {
      Utils.showSnackBar(context, error.toString());
      emit(EditGroupConfigurationFailure(
          NetworkExceptions.getErrorMessage(error)));
    });
  }

  initDate(int timeStamp, String date, int dateFromApi) {
    if (timeStamp != 0) {
      date =
          dateFormat.format(DateTime.fromMillisecondsSinceEpoch(dateFromApi));
    }
  }

  void showDay() {
    showDayOfWeek = !showDayOfWeek;
    showDayOfWeek ? emit(ShowDayofWeek()) : emit(HideDayofWeek());
  }

  initDateTime() {
    if (groupInfo?.groupView?.event?.endDate != null) {
      dateEnd = dateFormat.format(DateTime.fromMillisecondsSinceEpoch(
          groupInfo?.groupView?.event?.endDate ?? 0));
    }
    if (groupInfo?.groupView?.event?.startDate != null) {
      dateStart = dateFormat.format(DateTime.fromMillisecondsSinceEpoch(
          groupInfo?.groupView?.event?.startDate ?? 0));
    }
    if (groupInfo?.groupView?.event?.endTime != null) {
      timeEnd = timeFormat.format(DateTime.fromMillisecondsSinceEpoch(
          groupInfo?.groupView?.event?.endTime ?? 0));
    }
    if (groupInfo?.groupView?.event?.startTime != null) {
      timeStart = timeFormat.format(DateTime.fromMillisecondsSinceEpoch(
          groupInfo?.groupView?.event?.startTime ?? 0));
    }
  }

  void pickDailyActivities(int index, DayOfWeekTitle day) {
    dayStart.value = index;
    dailyActivities = day.name;
    isPickedDay = true;
    postDay = day.getTitle;
    showDayOfWeek = false;
    emit(HideDayofWeek());
  }

  void pickDob(context, String? typeOfDate) {
    DatePicker.showDatePicker(context,
        maxTime: DateTime(2030, 1, 1),
        minTime: DateTime(1922, 1, 1),
        showTitleActions: true,
        locale: appPreferences.locale, onConfirm: (date) {
      typeOfDate == 'startTime' ? selectStartDate(date) : selectEndDate(date);
    });
  }

  void selectStartDate(DateTime date) {
    startDate = date;
    checkValidDate('start');
  }

  void selectEndDate(DateTime date) {
    endDate = date;
    checkValidDate('end');
  }

  checkDate(DateTime startDate, DateTime endDate) {
    return startDate.isBefore(endDate);
  }

  checkValidDate(String typeofDate) {
    emit(EditGroupConfigurationInitial());
    if (startDate != null && endDate != null) {
      isValidDate = checkDate(startDate!, endDate!);
    }
    isValidDate
        ? typeofDate == 'start'
            ? emit(SelectedStartDateSuccess())
            : emit(SelectedEndDateSuccess())
        : emit(SelectedDateFail());
  }

  void pickTime(context, String? typeOfDate) {
    DatePicker.showTimePicker(context,
        showSecondsColumn: false,
        showTitleActions: true,
        locale: appPreferences.locale, onConfirm: (date) {
      typeOfDate == 'startTime' ? selectStartTime(date) : selectEndTime(date);
    });
  }

  void selectStartTime(DateTime date) {
    startTime = date;
    checkValidTime('start');
  }

  void selectEndTime(DateTime date) {
    endTime = date;
    checkValidTime('end');
  }

  checkValidTime(String typeofDate) {
    emit(EditGroupConfigurationInitial());
    if (startTime != null && endTime != null) {
      isValidTime = checkDate(startTime!, endTime!);
    }
    isValidTime
        ? typeofDate == 'start'
            ? emit(SelectedStartTimeSuccess())
            : emit(SelectedEndTimeSuccess())
        : emit(SelectedTimeFail());
  }

  void addLink() {
    emit(EditGroupConfigurationLoading());
    link.add(
        Link(name: TextEditingController(), link: TextEditingController()));
    emit(addLinkSuccess());
  }

  void deleteLink(int index) {
    emit(EditGroupConfigurationLoading());
    link.removeAt(index);
    emit(addLinkSuccess());
  }
}

class Link {
  TextEditingController name;
  TextEditingController link;

  Link({required this.name, required this.link});
}

class Category {
  String category;
  int index;
  int id;

  Category({required this.category, required this.index, required this.id});
}
