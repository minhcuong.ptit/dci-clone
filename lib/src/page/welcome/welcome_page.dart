import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../utils/enum.dart';
import '../../utils/navigation_utils.dart';
import '../auth_user/login/login.dart';
import '../pin_code/forgot_pin/forgot_pin.dart';

class WelcomePage extends StatefulWidget {
  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appPreferences.setData(Const.FIRST_LAUNCH, false);
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light, child: buildPage(context)),
    );
  }

  Widget buildPage(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Container(
              color: R.color.blue,
              child: Image.asset(
                R.drawable.bg_welcome,
                height: double.infinity,
                fit: BoxFit.cover,
              )),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 170.h,
                ),
                SizedBox(
                  height: 24.h,
                ),
                Expanded(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        BlocProvider.of<AuthenticationCubit>(context)
                            .welcomeToLogin();
                      },
                      child: Container(
                        width: 251.w,
                        height: 53.h,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(color: R.color.blue, width: 1),
                          borderRadius: BorderRadius.circular(48),
                          color: R.color.blue,
                        ),
                        child: Text(
                          R.string.login.tr().toUpperCase(),
                          style: Theme.of(context)
                              .textTheme
                              .buttonRegular
                              .apply(color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 24.h,
                    ),
                    InkWell(
                      onTap: () {
                        NavigationUtils.navigatePage(
                            context,
                            BlocProvider.value(
                              value:
                                  BlocProvider.of<AuthenticationCubit>(context),
                              child: ForgotPinPage(
                                type: ForgotPinType.SIGN_UP,
                              ),
                            ));
                      },
                      child: Container(
                        width: 251.w,
                        height: 53.h,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(color: R.color.white, width: 1),
                          borderRadius: BorderRadius.circular(48),
                          color: R.color.white,
                        ),
                        child: Text(
                          R.string.sign_up_new_account.tr().toUpperCase(),
                          style: Theme.of(context)
                              .textTheme
                              .buttonRegular
                              .apply(color: R.color.blue),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 24.h,
                    ),
                  ],
                ))
              ],
            ),
          )
        ],
      ),
      color: R.color.primaryColor,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
