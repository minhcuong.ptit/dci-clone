import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/post_request.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/page/manage_reported_posts/manage_reported_posts.dart';
import 'package:imi/src/page/manage_reported_posts/widget/detail_report.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/time_ago.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/custom_expandable_text.dart';
import 'package:imi/src/widgets/link_preview_widget.dart';
import 'package:imi/src/widgets/post_slider_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ManageReportPostsPage extends StatefulWidget {
  final int? communityId;

  const ManageReportPostsPage({Key? key, this.communityId}) : super(key: key);

  @override
  State<ManageReportPostsPage> createState() => _ManageReportPostsPageState();
}

class _ManageReportPostsPageState extends State<ManageReportPostsPage> {
  late ManageReportedPostsCubit _cubit;
  bool _showIndividual = true;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ManageReportedPostsCubit(
        repository, graphqlRepository, widget.communityId);
    _cubit.getListReport();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<ManageReportedPostsCubit, ManageReportedPostsState>(
        listener: (BuildContext context, state) {
          if (state is! ManageReportedPostsLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is DeletePostSuccess) {
            _cubit.refreshPost();
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: appBar(context, R.string.reported.tr().toUpperCase(),
                centerTitle: true),
            body: StackLoadingView(
              visibleLoading: state is ManageReportedPostsLoading,
              child: state is! ManageReportForbiddenState
                  ? SmartRefresher(
                      controller: _refreshController,
                      onRefresh: () => _cubit.getListReport(isRefresh: true),
                      onLoading: () => _cubit.getListReport(isLoadMore: true),
                      child: _cubit.listPostReport.length == 0
                          ? Visibility(
                              visible: state is ManageReportedPostsSuccess,
                              child: Center(
                                child: Text(
                                  R.string.no_reports_yet.tr(),
                                  style: Theme.of(context).textTheme.regular400,
                                ),
                              ),
                            )
                          : buildPage(),
                    )
                  : Center(
                      child: Text(
                        R.string.something_happen.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .title
                            .copyWith(color: R.color.black, fontSize: 14),
                      ),
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget buildPage() {
    return ListView.separated(
        itemBuilder: (context, int index) {
          PostData data = _cubit.listPostReport[index];
          _showIndividual = ((data.communityV2?.type) == Const.INDIVIDUAL);
          // ||
          // (widget.isFromGroupPage ?? false);
          return Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: R.color.white,
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.h, vertical: 12.h),
                  child: Row(
                    children: [
                      Container(
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {},
                          child: _showIndividual
                              ? AvatarWidget(
                                  radius: 16.h,
                                  avatar: data.avatar,
                                  size: 32.h,
                                )
                              : Stack(
                                  clipBehavior: Clip.none,
                                  alignment: Alignment.centerRight,
                                  children: [
                                    AvatarWidget(
                                      radius: 16.h,
                                      avatar: data.communityV2?.avatarUrl,
                                      size: 32.h,
                                    ),
                                    Positioned(
                                      left: 15.w,
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: R.color.white,
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(1.0),
                                          child: AvatarWidget(
                                            radius: 12.h,
                                            avatar: data.avatar,
                                            size: 24.h,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                        ),
                        padding: EdgeInsets.only(
                            top: 15.h, right: _showIndividual ? 8.w : 16.w),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 8.h,
                            ),
                            InkWell(
                              onTap: () {},
                              // (data.communityV2?.type) == Const.INDIVIDUAL
                              //     ? widget.viewProfileCallback
                              //     : widget.viewClubCallback,
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: !_showIndividual,
                                    child: Flexible(
                                      child: Text(
                                        (data.communityV2?.name ?? ""),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodySmallBold
                                            .copyWith(
                                                fontWeight: FontWeight.w700,
                                                fontSize: 11.sp),
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: _showIndividual,
                                    child: Flexible(
                                      child: Text(
                                        (data.fullName ?? ""),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodySmallBold
                                            .copyWith(
                                                fontWeight: FontWeight.w700,
                                                fontSize: 11.sp),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8.w,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Visibility(
                                    visible: !_showIndividual,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 8.w),
                                      child: GestureDetector(
                                        onTap: () {},
                                        //widget.viewProfileCallback,
                                        child: Text(
                                          (data.fullName ?? ""),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: Theme.of(context)
                                              .textTheme
                                              .labelSmallText
                                              .copyWith(
                                                  fontSize: 8.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: R.color.black),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: (data.communityV2?.type) ==
                                      Const.INDIVIDUAL,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(top: 2.h, right: 8.w),
                                    child: Image.asset(
                                      R.drawable.ic_global,
                                      width: 10.w,
                                    ),
                                  ),
                                ),
                                Container(
                                    margin:
                                        EdgeInsets.only(top: 2.h, right: 6.w),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(2.w),
                                        color: R.color.shadesGray
                                            .withOpacity(0.8)),
                                    width: 4.w,
                                    height: 4.w),
                                Text(
                                  data.createdDate == null
                                      ? ""
                                      : TimeAgo.timeAgoSinceDate(
                                          DateTime.fromMillisecondsSinceEpoch(
                                                  data.createdDate!,
                                                  isUtc: true)
                                              .toUtc()),
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelSmallText
                                      .copyWith(
                                          fontSize: 8.sp,
                                          fontWeight: FontWeight.w400,
                                          color: R.color.shadesGray),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          NavigationUtils.rootNavigatePage(
                              context,
                              DetailReportPage(
                                countReport: data.reportedPostInfo?.total,
                                postId: int.parse(data.id ?? ""),
                              ));
                        },
                        child: Container(
                          height: 20.h,
                          width: 20.w,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: R.color.red,
                          ),
                          child: Center(
                            child: Text(
                              "${data.reportedPostInfo?.total}",
                              style: Theme.of(context)
                                  .textTheme
                                  .medium500
                                  .copyWith(
                                      fontSize: 10.sp,
                                      height: 12.h / 10.sp,
                                      color: R.color.white),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {},
                    child: contentPostWidget(context, data)),
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 20.h, horizontal: 24.w),
                  child: Row(
                    children: [
                      Expanded(
                          child: ButtonWidget(
                              backgroundColor: R.color.white,
                              borderColor: R.color.primaryColor,
                              textColor: R.color.primaryColor,
                              padding: EdgeInsets.symmetric(vertical: 6.h),
                              uppercaseTitle: false,
                              textSize: 12.sp,
                              title: R.string.skip.tr(),
                              onPressed: () {
                                showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return buildButtonConfirm(context,
                                          title:
                                              R.string.ignore_this_report.tr(),
                                          description:
                                              R.string.false_report.tr(),
                                          cancel: R.string.cancel.tr(),
                                          confirm: R.string.confirm.tr(),
                                          cancelCallback: () {
                                        NavigationUtils.pop(context);
                                      }, confirmCallback: () {
                                        NavigationUtils.pop(context);
                                        _cubit.deletePost(
                                            postId: data.id,
                                            reportPostAction:
                                                ReportPostAction.REJECT.name);
                                      });
                                    });
                              })),
                      SizedBox(width: 20.w),
                      Expanded(
                        child: ButtonWidget(
                            backgroundColor: R.color.primaryColor,
                            padding: EdgeInsets.symmetric(vertical: 6.h),
                            uppercaseTitle: false,
                            textSize: 12.sp,
                            title: R.string.permanently_deleted.tr(),
                            onPressed: () {
                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return buildButtonConfirm(context,
                                        title:
                                            R.string.permanently_deleted.tr(),
                                        description: R
                                            .string.confirm_permanently_deleted
                                            .tr(),
                                        cancel: R.string.cancel.tr(),
                                        confirm: R.string.delete.tr(),
                                        cancelCallback: () {
                                      NavigationUtils.pop(context);
                                    }, confirmCallback: () {
                                      NavigationUtils.pop(context);
                                      _cubit.deletePost(
                                          postId: data.id,
                                          reportPostAction:
                                              ReportPostAction.APPROVE.name);
                                    });
                                  });
                            }),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        },
        separatorBuilder: (context, index) => Container(
              height: 8.h,
              color: R.color.grey300,
            ),
        itemCount: _cubit.listPostReport.length);
  }

  Widget buildButtonConfirm(BuildContext context,
      {String? title,
      String? description,
      String? cancel,
      String? confirm,
      VoidCallback? cancelCallback,
      VoidCallback? confirmCallback}) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 24.w, vertical: 40.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? "",
            style: Theme.of(context)
                .textTheme
                .bold700
                .copyWith(fontSize: 16.sp, height: 24.h / 16.sp),
          ),
          Text(
            description ?? "",
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 11.sp, color: R.color.grey),
          ),
          SizedBox(height: 40.h),
          Row(
            children: [
              Expanded(
                  child: ButtonWidget(
                      backgroundColor: R.color.white,
                      borderColor: R.color.primaryColor,
                      textColor: R.color.primaryColor,
                      padding: EdgeInsets.symmetric(vertical: 6.h),
                      uppercaseTitle: false,
                      textSize: 12.sp,
                      title: cancel ?? "",
                      onPressed: cancelCallback)),
              SizedBox(width: 20.w),
              Expanded(
                child: ButtonWidget(
                    backgroundColor: R.color.primaryColor,
                    padding: EdgeInsets.symmetric(vertical: 6.h),
                    uppercaseTitle: false,
                    textSize: 12.sp,
                    title: confirm ?? "",
                    onPressed: confirmCallback),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget contentPostWidget(context, PostData data) {
    Widget _textWidget = CustomExpandableText(
      text: data.text ?? "",
      maxLines: 4,
      textStyle: Theme.of(context)
          .textTheme
          .bodySmallText
          .copyWith(fontSize: 11.sp, fontWeight: FontWeight.w400),
    );
    if (Utils.isEmpty(data.backgroundImage) &&
        Utils.isEmpty(data.backgroundImages) &&
        (Utils.isEmpty(data.backgroundColor) ||
            Utils.parseStringToColor(data.backgroundColor) == R.color.white)) {
      PostMetadata? metadata = data.metadata;
      return Padding(
        padding: EdgeInsets.only(left: 16.w, right: 16.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _textWidget,
            if (metadata is PostMetadataLink) ...[
              SizedBox(height: 8.h),
              LinkPreviewWidget(
                image: metadata.image,
                title: metadata.title,
                description: metadata.description,
                url: metadata.url,
              ),
            ],
          ],
        ),
      );
    } else if (!Utils.isEmpty(data.backgroundImages) ||
        !Utils.isEmpty(data.backgroundImage)) {
      List<String?> listImage = data.backgroundImages ?? [data.backgroundImage];
      listImage = listImage.take(4).toList();
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w),
            child: _textWidget,
          ),
          SizedBox(height: 8.h),
          PostSliderWidget(
            images: listImage,
            isInPostDetails: true,
            onPressImage: () {},
          ),
        ],
      );
    } else {
      return Container(
        constraints: BoxConstraints(
            minHeight: (ScreenUtil().screenWidth - 16.h * 2) * 10 / 16),
        color: Utils.parseStringToColor(data.backgroundColor),
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 16.w, right: 16.w),
        child: CustomExpandableText(
          text: data.text ?? "",
          maxLines: 4,
        ),
      );
    }
  }
}
