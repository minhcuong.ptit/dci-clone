import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/detail_report_response.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/manage_reported_posts/manage_reported_posts.dart';
import 'package:imi/src/page/manage_reported_posts/manage_reported_posts_state.dart';
import 'package:imi/src/utils/enum.dart';

import '../../data/network/repository/app_graphql_repository.dart';

class ManageReportedPostsCubit extends Cubit<ManageReportedPostsState> {
  ManageReportedPostsCubit(
      this.repository, this.graphqlRepository, this.communityId)
      : super(ManageReportedPostsInitial());
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<PostData> listPostReport = [];
  String? nextToken;
  int? communityId;

  void getListReport({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit((isRefresh)
        ? ManageReportedPostsInitial()
        : ManageReportedPostsLoading());
    if (isLoadMore != true) {
      nextToken = null;
      listPostReport = [];
    }
    ApiResult<dynamic> apiResult = await graphqlRepository.getListPost(
        nextToken,
        postStatus: PostStatus.REPORTED.name,
        communityId: communityId);
    apiResult.when(success: (dynamic data) {
      if (data is PostPageData) {
        nextToken = data.nextToken;
        listPostReport = data.data ?? [];
      }
      emit(ManageReportedPostsSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest && error.code != null)
        emit(ManageReportForbiddenState());
      else
        emit(ManageReportedPostsFailure(
            NetworkExceptions.getErrorMessage(error)));
    });
  }

  void deletePost({String? postId, reportPostAction}) async {
    emit(ManageReportedPostsLoading());
    ApiResult<dynamic> apiResult = await repository.deletePost(
        reportPostAction: reportPostAction, postId: postId);
    apiResult.when(success: (dynamic data) {
      emit(DeletePostSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(
          ManageReportedPostsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshPost() {
    emit(ManageReportedPostsLoading());
    getListReport();
    emit(ManageReportedPostsInitial());
  }
}
