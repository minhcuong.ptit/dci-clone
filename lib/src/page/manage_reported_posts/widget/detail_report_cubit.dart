import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/detail_report_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/manage_reported_posts/widget/detail_report.dart';
import 'package:imi/src/page/manage_reported_posts/widget/detail_report_state.dart';

class DetailReportCubit extends Cubit<DetailReportState> {
  DetailReportCubit(this.repository, this.graphqlRepository, this.postId)
      : super(DetailReportInitial());
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<ReportedPostData> listDetailReport = [];
  String? nextToken;
  final int postId;

  void getDetailReport(
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || isLoadMore)
        ? DetailReportInitial()
        : DetailReportLoading());
    if (isRefresh) {
      nextToken = null;
      listDetailReport = [];
    }
    ApiResult<dynamic> apiResult = await graphqlRepository.getDetailReport(
        nextToken: nextToken, postId: postId);
    apiResult.when(success: (dynamic data) {
      if (data is FetchReportedPostInfo) {
        nextToken = data.nextToken;
        listDetailReport = data.data ?? [];
      }
      emit(DetailReportSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailReportFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
