import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/page/manage_reported_posts/widget/detail_report.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DetailReportPage extends StatefulWidget {
  final int? countReport;
  final int postId;

  const DetailReportPage({Key? key, required this.postId, this.countReport})
      : super(key: key);

  @override
  State<DetailReportPage> createState() => _DetailReportPageState();
}

class _DetailReportPageState extends State<DetailReportPage> {
  late DetailReportCubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = DetailReportCubit(repository, graphqlRepository, widget.postId);
    _cubit.getDetailReport();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: BlocConsumer<DetailReportCubit, DetailReportState>(
        listener: (BuildContext context, state) {
          if (state is! DetailReportLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: appBar(context,
                R.string.total_report.tr(args: ["${widget.countReport}"])),
            body: StackLoadingView(
              visibleLoading: state is DetailReportLoading,
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () => _cubit.getDetailReport(isRefresh: true),
                onLoading: () => _cubit.getDetailReport(isLoadMore: true),
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 16.h),
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(width: 40.h),
                        Expanded(
                          flex: 3,
                          child: Text(
                            R.string.reporter.tr(),
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                fontSize: 12.sp, height: 20.h / 12.sp),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(
                            R.string.reason.tr(),
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                fontSize: 12.sp, height: 20.h / 12.sp),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 15.h),
                    ListView.separated(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: _cubit.listDetailReport.length,
                      itemBuilder: (context, int index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              child: CachedNetworkImage(
                                fit: BoxFit.cover,
                                height: 40.h,
                                width: 40.h,
                                imageUrl:
                                    _cubit.listDetailReport[index].avatarUrl ??
                                        "",
                              ),
                              borderRadius: BorderRadius.circular(200.h),
                            ),
                            SizedBox(width: 8.h),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    _cubit.listDetailReport[index].fullName ??
                                        "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(
                                            fontSize: 11.sp,
                                            height: 20.h / 11.sp),
                                  ),
                                  Text(
                                    DateUtil.parseDateToString(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            _cubit.listDetailReport[index]
                                                    .createdDate ??
                                                0),
                                        Const.DATE_FORMAT_POINT_2),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 10.sp,
                                            height: 16.h / 10.sp),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              _cubit.listDetailReport[index].reason?.reason ??
                                  "",
                              style: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 11.sp, height: 20.h / 11.sp),
                            )
                          ],
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider();
                      },
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
