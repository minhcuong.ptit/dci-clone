import 'package:equatable/equatable.dart';

abstract class DetailReportState extends Equatable {
  @override
  List<Object> get props => [];
}

class DetailReportInitial extends DetailReportState {}

class DetailReportLoading extends DetailReportState {
  @override
  String toString() {
    return 'DetailReportLoading{}';
  }
}

class DetailReportFailure extends DetailReportState {
  final String error;

  DetailReportFailure(this.error);

  @override
  String toString() {
    return 'DetailReportFailure{error: $error}';
  }
}

class DetailReportSuccess extends DetailReportState {
  @override
  String toString() {
    return 'DetailReportSuccess{}';
  }
}
