import 'package:equatable/equatable.dart';

abstract class ManageReportedPostsState extends Equatable {
  @override
  List<Object> get props => [];
}

class ManageReportedPostsInitial extends ManageReportedPostsState {}

class ManageReportedPostsLoading extends ManageReportedPostsState {
  @override
  String toString() {
    return 'ManageReportedPostsLoading{}';
  }
}

class ManageReportForbiddenState extends ManageReportedPostsState {}

class ManageReportedPostsFailure extends ManageReportedPostsState {
  final String error;

  ManageReportedPostsFailure(this.error);

  @override
  String toString() {
    return 'ManageReportedPostsFailure{error: $error}';
  }
}

class ManageReportedPostsSuccess extends ManageReportedPostsState {
  @override
  String toString() {
    return 'ManageReportedPostsSuccess{}';
  }
}

class DeletePostSuccess extends ManageReportedPostsState {
  @override
  String toString() {
    return 'DeletePostSuccess{}';
  }
}
