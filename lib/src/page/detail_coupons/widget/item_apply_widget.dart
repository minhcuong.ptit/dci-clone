import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';
import '../../../utils/utils.dart';

class ItemApplyWidget extends StatelessWidget {
  final VoidCallback callback;
  final String imageUrl;
  final String title;
  final int price;
  final bool isHot;
  final int quality;

  ItemApplyWidget(
      {required this.callback,
      required this.imageUrl,
      required this.title,
      required this.price,
      required this.isHot,
      required this.quality});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
          onTap: callback,
          child: Container(
            height: 86.h,
            padding: EdgeInsets.symmetric(horizontal: 4.h),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.h), color: R.color.white),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(4.h),
                  child: CachedNetworkImage(
                    width: 120.h,
                    height: 86.h,
                    fit: BoxFit.cover,
                    imageUrl: imageUrl,
                    placeholder: (context, url) => Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: R.color.grey,
                        ),
                        borderRadius: BorderRadius.circular(4.h),
                      ),
                      child: Stack(
                        children: [
                          Image.asset(
                            R.drawable.ic_default_item,
                            fit: BoxFit.cover,
                            width: 120.h,
                            height: 86.h,
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            child: Text(
                              R.string.loading.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(fontSize: 14.sp),
                            ),
                          )
                        ],
                      ),
                    ),
                    errorWidget: (context, url, error) => Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: R.color.grey,
                          ),
                          borderRadius: BorderRadius.circular(4.h)),
                      child: Stack(
                        children: [
                          Image.asset(
                            R.drawable.ic_default_item,
                            fit: BoxFit.cover,
                            width: 120.h,
                            height: 86.h,
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            child: Text(
                              R.string.loading.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(fontSize: 14.sp),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 8.w),
                Expanded(
                  child: Container(
                    height: 86.h,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: Theme.of(context).textTheme.bold700.copyWith(
                              fontSize: 14.sp,
                              color: R.color.dark_blue,
                              height: 18.h / 14.sp),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5.h),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                "${R.string.price.tr()} ",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp,
                                        color: R.color.black,
                                        height: 16.h / 12.sp),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(width: 8.w),

                              Text(
                                "${Utils.formatMoney(price)} VND",
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        fontSize: 12.sp,
                                        color: R.color.orange,
                                        height: 16.h / 12.sp),
                              ),

                          ],
                        ),
                        SizedBox(height: 5.h),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                "${R.string.amount.tr()}: ",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp,
                                        color: R.color.black,
                                        height: 16.h / 12.sp),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(width: 8.w),
                            Text(
                              "${quality}",
                              style: Theme.of(context)
                                  .textTheme
                                  .medium500
                                  .copyWith(
                                      fontSize: 12.sp,
                                      color: R.color.dark_blue,
                                      height: 16.h / 12.sp),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Visibility(
          visible: isHot == true,
          child: Positioned(
              top: 8.h,
              left: -3.h,
              child: Image.asset(R.drawable.ic_hot, height: 20.h)),
        ),
      ],
    );
  }
}
