import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/detail_coupons/detail_coupons.dart';
import 'package:imi/src/page/detail_coupons/widget/item_apply_widget.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/buy_more_response.dart';
import '../../data/network/response/course_by_more_response.dart';
import '../../data/network/response/event_by_more_response.dart';
import '../../utils/date_util.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/utils.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/custom_appbar.dart';
import '../../widgets/custom_expandable_text.dart';
import '../../widgets/dropdown_column_widget.dart';
import '../../widgets/search_list_data_popup.dart';
import '../../widgets/text_field_widget.dart';
import '../course_details/course_details.dart';
import '../detail_event/detail_event.dart';
import '../detail_event/widget/infor_event_order.dart';

class DetailCouponsPage extends StatefulWidget {
  final int couponId;
  final int? orderId;
  bool? checkErrorCoupons;

  DetailCouponsPage(this.couponId, this.orderId, {this.checkErrorCoupons});

  @override
  State<DetailCouponsPage> createState() => _DetailCouponsPageState();
}

class _DetailCouponsPageState extends State<DetailCouponsPage> {
  late DetailCouponsCubit _cubit;
  RefreshController _refreshController = RefreshController();

  TextEditingController _noteController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  FocusNode _focusNode = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = DetailCouponsCubit(repository, graphqlRepository);
    _cubit.getDetailCoupons(widget.couponId);
    _cubit.getLisBuyMore(widget.couponId);
    _cubit.getProvince();
    _nameController.text = _cubit.name ?? "";
    _phoneController.text = _cubit.phone ?? "";
    _emailController.text = _cubit.email ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white,
      appBar: appBar(context, R.string.discount_code_details.tr(),
          centerTitle: true,
          backgroundColor: R.color.primaryColor,
          titleColor: R.color.white,
          iconColor: R.color.white),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<DetailCouponsCubit, DetailCouponsState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! DetailCouponsLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
            if (state is OrderNowSuccess) {
              showDialog(
                  barrierColor: R.color.white.withOpacity(0),
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return buildAlertDialogBuyNow(context);
                  });
              Future.delayed(Duration(seconds: 3), () {
                NavigationUtils.pop(context);
              });
            }
            if (state is ApplyCouponFailure) {
              Utils.showToast(context, _cubit.errorText,setTime: true);
            }

            if (state is BuyMoreSuccess) {
              _cubit.applyCoupon(
                  discountCode: _cubit.couponDetail?.code ?? "",
                  orderId: widget.orderId ?? 0,
                  couponId: widget.couponId);
              NavigationUtils.pop(context);
            }
            if (state is ApplyCouponSuccess) {
              NavigationUtils.pop(context, result: _cubit.codeCoupons);
              NavigationUtils.pop(context, result: _cubit.codeCoupons);
            }
            if (widget.checkErrorCoupons == true) {
              Utils.showToast(context,
                  R.string.coupon_is_unavailable_for_this_item_error.tr(),setTime: true);
              widget.checkErrorCoupons = false;
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, DetailCouponsState state) {
    return StackLoadingView(
      visibleLoading: state is DetailCouponsLoading,
      child: SmartRefresher(
        controller: _refreshController,
        enablePullUp: true,
        onRefresh: () {
          _cubit.getDetailCoupons(widget.couponId);
          _cubit.getLisBuyMore(widget.couponId, isRefresh: true);
        },
        child: ListView(
          children: [
            Visibility(
                visible: state is! DetailCouponsLoading,
                child: buildBanner(context)),
            SizedBox(height: 12.h),
            Visibility(
              visible: state is! DetailCouponsLoading,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: R.color.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 1,
                      blurRadius: 8,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      R.string.describe.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 14.sp, height: 24.h / 14.sp),
                    ),
                    SizedBox(height: 5),
                    CustomExpandableText(
                        maxLines: 4,
                        text: _cubit.couponDetail?.description ?? ""),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20.h),
            if (_cubit.courses.length != 0 &&
                state is! DetailCouponsLoading) ...[
              buildSeeMore(R.string.course_need_to_buy_more.tr()),
              SizedBox(height: 12.h),
              buildCourseByMore(),
              SizedBox(height: 12),
            ],
            if (_cubit.events.length != 0 &&
                state is! DetailCouponsLoading) ...[
              buildSeeMore(R.string.event_need_to_buy_more.tr()),
              SizedBox(height: 12.h),
              buildEventByMore(),
            ],
            SizedBox(height: 30.h),
            Visibility(
                visible: state is! DetailCouponsLoading &&
                    (_cubit.events.length != 0 ||
                        _cubit.courses.length != 0),
                child: buildTotalPrice(
                    title: R.string.total_by_more.tr(),
                    price: "${_cubit.totalBuy()}")),
            SizedBox(height: 6.h),
            Visibility(
              visible: state is! DetailCouponsLoading,
              child: buildTotalPrice(
                  title: R.string.total_discount.tr(),
                  price: "${_cubit.couponDetail?.totalDiscount ?? 0}"),
            ),
            Visibility(
              visible: state is! DetailCouponsLoading,
              child: Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: ButtonWidget(
                    backgroundColor: R.color.primaryColor,
                    padding:
                        EdgeInsetsDirectional.fromSTEB(5.w, 4.h, 5.w, 10.h),
                    width: 120,
                    uppercaseTitle: false,
                    title: (_cubit.events.length != 0 ||
                            _cubit.courses.length != 0)
                        ? R.string.buy_now.tr()
                        : R.string.apply_code.tr(),
                    textStyle: Theme.of(context).textTheme.medium500.copyWith(
                        fontSize: 13.sp,
                        color: R.color.white,
                        height: 24.h / 13.sp),
                    onPressed: () {
                      if (_cubit.events.length != 0 ||
                          _cubit.courses.length != 0) {
                        showMaterialModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.transparent,
                          builder: (context) => BlocProvider.value(
                            child: buildAddToCart(context, state),
                            value: _cubit,
                          ),
                        );
                      } else {
                        _cubit.applyCoupon(
                            discountCode: _cubit.couponDetail?.code ?? "",
                            orderId: widget.orderId ?? 0,
                            couponId: widget.couponId);
                      }
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTotalPrice({String? title, String? price}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Text(
              "${title}: ",
              style: Theme.of(context).textTheme.bold700.copyWith(
                  fontSize: 14.sp, color: R.color.black, height: 24.h / 14.sp),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Text(
            "${Utils.formatMoney(price)} VND",
            style: Theme.of(context).textTheme.medium500.copyWith(
                fontSize: 12.sp, color: R.color.orange, height: 16.h / 12.sp),
          ),
        ],
      ),
    );
  }

  Widget buildEventByMore() {
    return ListView.separated(
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: 20),
      shrinkWrap: true,
      separatorBuilder: (BuildContext context, int index) {
        return Container(height: 15.h);
      },
      itemCount: _cubit.events.length,
      itemBuilder: (BuildContext context, int index) {
        BuyMoreEvents data = _cubit.events[index];
        return ItemApplyWidget(
          callback: () {
            NavigationUtils.navigatePage(
                context,
                EventDetailPage(
                  eventId: data.id ?? 0,
                  productId: data.productId ?? 0,
                ));
          },
          imageUrl: data.avatarUrl ?? "",
          title: data.name ?? "",
          price: data.preferentialFee == 0
              ? data.currentFee ?? 0
              : data.preferentialFee ?? 0,
          isHot: data.isHot ?? false,
          quality: data.quantity ?? 0,
        );
      },
    );
  }

  Widget buildCourseByMore() {
    return ListView.separated(
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: 20),
      shrinkWrap: true,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 8),
        );
      },
      itemCount: _cubit.courses.length,
      itemBuilder: (BuildContext context, int index) {
        BuyMoreCourses data = _cubit.courses[index];
        return ItemApplyWidget(
          callback: () {
            NavigationUtils.navigatePage(
                context,
                CourseDetailsPage(
                  courseId: data.id,
                  productId: data.productId ?? 0,
                ));
          },
          imageUrl: data.avatarUrl ?? "",
          title: data.name ?? "",
          price: data.preferentialFee == 0
              ? data.currentFee ?? 0
              : data.preferentialFee ?? 0,
          isHot: data.isHot ?? false,
          quality: data.quantity ?? 0,
        );
      },
    );
  }

  Widget buildBanner(BuildContext context) {
    return Container(
      height: 220,
      child: Stack(
        children: [
          Image.asset(
            R.drawable.ic_banner_coupons,
            height: 200,
            width: double.infinity,
          ),
          Positioned(
            left: 20,
            top: 50,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "${_cubit.couponDetail?.code?.toUpperCase() ?? ""} ${R.string.reduce.tr() ?? ""}",
                  style: Theme.of(context).textTheme.bold700.copyWith(
                      fontSize: 14.sp,
                      color: R.color.white,
                      height: 26.h / 14.sp),
                ),
                Text(
                  "${_cubit.couponDetail?.discountType == DiscountType.PRICE.name ? "${Utils.formatMoney(_cubit.couponDetail?.discount ?? 0)} VND" : " ${_cubit.couponDetail?.discount ?? 0}%"}",
                  style: Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: 22.sp,
                        color: R.color.yellow,
                        height: 28.h / 22.sp,
                      ),
                ),
                Visibility(
                  visible: _cubit.couponDetail?.expiredDate != null,
                  child: Text(
                    "${R.string.expiry.tr()}: ${DateUtil.parseDateToString(DateTime.fromMillisecondsSinceEpoch(_cubit.couponDetail?.expiredDate ?? 0, isUtc: false), "dd/MM/yyyy")}",
                    style: Theme.of(context).textTheme.medium500.copyWith(
                        fontSize: 11.sp,
                        color: R.color.white,
                        height: 16.h / 11.sp),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 20,
            right: 20,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: R.color.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 8,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    R.drawable.ic_discount_code,
                    height: 30.h,
                  ),
                  SizedBox(width: 8.w),
                  Text(
                    R.string.apply_for_orders_from_vnd.tr(args: [
                      "${Utils.formatMoney(_cubit.couponDetail?.minPrice ?? 0)}"
                    ]),
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12.sp,
                        color: R.color.black,
                        height: 18.h / 12.sp),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildSeeMore(String title) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 16.sp, color: R.color.black),
      ),
    );
  }

  Widget buildContent(String content) {
    return Text(
      content,
      style: Theme.of(context)
          .textTheme
          .regular400
          .copyWith(fontSize: 12, color: R.color.black, height: 18 / 12),
    );
  }

  Widget buildTitle(String title) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .medium500
          .copyWith(fontSize: 14, color: R.color.primaryColor, height: 24 / 14),
    );
  }

  Widget buildAlertDialogBuyNow(BuildContext context) {
    return AlertDialog(
      backgroundColor: R.color.darkGrey,
      contentPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      content: Container(
        height: 80.h,
        width: 80.w,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              CupertinoIcons.check_mark_circled_solid,
              size: 21.h,
              color: R.color.white,
            ),
            SizedBox(height: 10.h),
            Text(
              R.string.added_to_cart.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 11.sp, color: R.color.white),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  Widget buildAddToCart(BuildContext context, DetailCouponsState state) {
    return BlocBuilder<DetailCouponsCubit, DetailCouponsState>(
      buildWhen: (previous, next) =>
          next is DetailTextChangeInitial ||
          next is DetailNumberChangeInitial ||
          next is GetListProvinceSuccess ||
          next is ProvinceSuccess ||
          next is ListEmptyProvince,
      builder: (context, state) {
        final _cubit = BlocProvider.of<DetailCouponsCubit>(context);
        return Container(
          margin: EdgeInsets.only(top: 100),
          child: SingleChildScrollView(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: EdgeInsets.all(20.h),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.h),
                  topRight: Radius.circular(24.h),
                ),
                color: R.color.white,
              ),
              child: Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10.h),
                      Center(
                        child: Text(
                          R.string.contact_info.tr().toUpperCase(),
                          style: Theme.of(context).textTheme.bold700.copyWith(
                              fontSize: 16,
                              height: 21 / 14,
                              color: R.color.black),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.first_and_last_name.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterName(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.phone_number.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterPhone(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.email.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterEmail(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.area.tr()),
                      _buildDropdownProvince(context, state),
                      SizedBox(height: 10.h),
                      Text(
                        R.string.notes_for_dci.tr(),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 12.sp,
                            color: R.color.gray,
                            height: 18 / 12),
                      ),
                      SizedBox(height: 2.h),
                      Container(
                          height: 200.h,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.h),
                              border:
                                  Border.all(width: 1, color: R.color.gray)),
                          child:
                              SingleChildScrollView(child: buildNote(context))),
                      SizedBox(height: 4.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: "${_cubit.lengthText}",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, color: R.color.gray),
                              children: <TextSpan>[
                                TextSpan(
                                    text: ' / 500',
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: R.color.gray)),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4.h),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 80.w),
                        child: ButtonWidget(
                            backgroundColor: R.color.primaryColor,
                            padding: EdgeInsetsDirectional.fromSTEB(
                                5.w, 4.h, 5.w, 10.h),
                            uppercaseTitle: false,
                            title: R.string.buy_now.tr(),
                            textStyle: Theme.of(context)
                                .textTheme
                                .bold700
                                .copyWith(
                                    fontSize: 12.sp,
                                    color: R.color.white,
                                    height: 22 / 12),
                            onPressed: () {
                              _cubit.createByMore(
                                userName: _nameController.text.trim(),
                                phone: _phoneController.text.trim(),
                                email: _emailController.text.trim(),
                                note: _noteController.text.trim(),
                              );
                            }),
                      ),
                    ],
                  ),
                  Positioned(
                      top: 0.h,
                      right: 0.w,
                      child: InkWell(
                        onTap: () {
                          NavigationUtils.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 24.h,
                        ),
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildNote(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 6.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _noteController,
        focusNode: _focusNode,
        keyboardType: TextInputType.multiline,
        textInputAction: TextInputAction.newline,
        inputFormatters: [LengthLimitingTextInputFormatter(500)],
        maxLines: 500,
        minLines: 1,
        underLineColor: R.color.white,
        hintText: R.string.please_enter_your_answer_here.tr(),
        onChanged: (value) {
          setState(() {
            _cubit.changeText(_noteController.text.trim().length);
          });
        },
      ),
    );
  }

  Widget _buildEnterName(BuildContext context, DetailCouponsState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _nameController,
        focusNode: _nameFocus,
        inputFormatters: [LengthLimitingTextInputFormatter(100)],
        isRequired: true,
        borderSize: BorderSide.none,
        hintText: R.string.enter_your_name.tr(),
        errorText: _cubit.errorName,
        onChanged: _cubit.validateName,
        border: 0,
        onSubmitted: state is EventDetailLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _nameFocus, _phoneFocus),
      ),
    );
  }

  Widget _buildEnterPhone(BuildContext context, DetailCouponsState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _phoneController,
        focusNode: _phoneFocus,
        keyboardType: TextInputType.number,
        inputFormatters: [LengthLimitingTextInputFormatter(10)],
        isRequired: true,
        borderSize: BorderSide.none,
        hintText: R.string.text_field_hint.tr(),
        errorText: _cubit.errorPhoneNumber,
        onChanged: _cubit.validatePhone,
        border: 0,
        onSubmitted: state is EventDetailLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _phoneFocus, _emailFocus),
      ),
    );
  }

  Widget _buildEnterEmail(BuildContext context, DetailCouponsState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _emailController,
        focusNode: _emailFocus,
        keyboardType: TextInputType.emailAddress,
        isRequired: true,
        maxLines: 1,
        inputFormatters: [LengthLimitingTextInputFormatter(100)],
        hintText: R.string.enter_your_email.tr(),
        borderSize: BorderSide.none,
        errorText: _cubit.errorEmail,
        onChanged: _cubit.validateEmail,
        onTap: () {},
        onSubmitted: state is EventDetailLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _emailFocus, _focusNode),
      ),
    );
  }

  Widget _buildDropdownProvince(
      BuildContext context, DetailCouponsState state) {
    List<String> listProvince =
        _cubit.listProvince.map((e) => e.cityName ?? "").toList();
    return Container(
      padding: EdgeInsets.only(left: 10.w, top: 2.h),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: DropdownColumnWidget(
        onTap: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listProvince,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.province.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectProvince(_cubit.listProvince[index]);
                  },
                );
              });
        },
        hintText: R.string.province.tr(),
        errorText: _cubit.errorProvince,
        currentValue: _cubit.selectedProvince?.cityName ??
            (_cubit.fetchItemByProductIdV2?.userInformation?.provinceId != null
                ? _cubit.cityName
                : _cubit.province),
        listData: listProvince,
        selectedValue: (String? value) {},
      ),
    );
  }
}
