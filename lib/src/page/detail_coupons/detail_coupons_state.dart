abstract class DetailCouponsState {}

class DetailCouponsInitial extends DetailCouponsState {}

class DetailCouponsLoading extends DetailCouponsState {
  @override
  String toString() {
    return 'DetailCouponsLoading{}';
  }
}

class DetailCouponsFailure extends DetailCouponsState {
  final String error;

  DetailCouponsFailure(this.error);

  @override
  String toString() {
    return 'DetailCouponsFailure{error: $error}';
  }
}

class DetailCouponsSuccess extends DetailCouponsState {
  @override
  String toString() {
    return 'DetailCouponsSuccess{}';
  }
}

class EmptyListCourse extends DetailCouponsState {
  @override
  String toString() {
    return 'EmptyListCourse{}';
  }
}

class EmptyListEventSimilar extends DetailCouponsState {
  @override
  String toString() {
    return 'EmptyListEventSimilar{}';
  }
}

class OrderNowSuccess extends DetailCouponsState {
  @override
  String toString() {
    return 'OrderNowSuccess{}';
  }
}

class ApplyCouponSuccess extends DetailCouponsState {
  @override
  String toString() {
    return 'ApplyCouponSuccess{}';
  }
}

class ApplyCouponFailure extends DetailCouponsState {
  @override
  String toString() {
    return 'ApplyCouponFailure{}';
  }
}

class ValidateError extends DetailCouponsState {
  @override
  String toString() {
    return 'ValidateError {}';
  }
}

class GetListProvinceSuccess extends DetailCouponsState {
  @override
  String toString() {
    return 'GetListProvinceSuccess {}';
  }
}

class ProvinceSuccess extends DetailCouponsState {
  @override
  String toString() {
    return 'ProvinceSuccess {}';
  }
}

class OrderValidatePhone extends DetailCouponsState {
  @override
  String toString() {
    return 'OrderValidatePhone {}';
  }
}
class OrderValidateEmail extends DetailCouponsState {
  @override
  String toString() {
    return 'OrderValidateEmail {}';
  }
}
class OrderValidate extends DetailCouponsState {
  @override
  String toString() {
    return 'OrderValidatePhone {}';
  }
}

class ListEmptyProvince extends DetailCouponsState {
  @override
  String toString() {
    return 'ListEmptyProvince {}';
  }
}

class DetailTextChangeInitial extends DetailCouponsState {}

class DetailNumberChangeInitial extends DetailCouponsState {}

class BuyMoreSuccess extends DetailCouponsState {
  @override
  String toString() {
    return 'BuyMoreSuccess {}';
  }
}