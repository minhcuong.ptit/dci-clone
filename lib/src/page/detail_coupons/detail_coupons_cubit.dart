import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/request/buy_more_request.dart';
import 'package:imi/src/page/detail_coupons/detail_coupons_state.dart';
import 'package:imi/src/utils/validators.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/request/apply_coupon_request.dart';
import '../../data/network/request/event_free_request.dart';
import '../../data/network/response/buy_more_response.dart';
import '../../data/network/response/city_response.dart';
import '../../data/network/response/detail_coupons_response.dart';
import '../../data/network/response/detail_order_product.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import '../../utils/utils.dart';

class DetailCouponsCubit extends Cubit<DetailCouponsState> with Validators {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  CouponDetail? couponDetail;

  List<BuyMoreCourses> courses = [];
  List<BuyMoreEvents> events = [];
  String? nextToken;
  String? nextTokenEventSimilar;
  String? nextTokenCourseSimilar;
  String? errorText;

  String? errorName;
  String? errorEmail;
  String? errorPhoneNumber;

  List<CityData> listProvince = [];
  CityData? selectedProvince;
  String? errorProvince;

  String? get name => appPreferences.getString(Const.FULL_NAME);

  String? get phone => appPreferences.getString(Const.PHONE);

  String? get email => appPreferences.getString(Const.EMAIL);

  String? get province => appPreferences.getString(Const.PROVINCE);

  int? get provinceId => appPreferences.getInt(Const.PROVINCE_ID);

  String? codeCoupons;
  int? couponId;
  FetchItemByProductIdV2? fetchItemByProductIdV2;
  String? cityName;
  int? cityId;
  int lengthText = 0;

  DetailCouponsCubit(this.repository, this.graphqlRepository)
      : super(DetailCouponsInitial());

  void getDetailCoupons(int couponId,{bool isRefresh = false}) async {
    emit(!isRefresh ? DetailCouponsLoading() : DetailCouponsInitial());
    if (isRefresh) {
      couponDetail = null;
    }
    ApiResult<CouponDetail> apiResult =
        await graphqlRepository.getDetailCoupons(couponId: couponId);
    apiResult.when(success: (CouponDetail data) async {
      couponDetail = data;
      emit(DetailCouponsSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailCouponsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getLisBuyMore(int couponId, {bool isRefresh = false}) async {
    emit(!isRefresh ? DetailCouponsLoading() : DetailCouponsInitial());
    if (isRefresh) {
      nextTokenCourseSimilar = null;
      events.clear();
      courses.clear();
    }
    ApiResult<FetchProductBuyMore> getLibrary =
        await graphqlRepository.getBuyMore(couponId: couponId);
    getLibrary.when(success: (FetchProductBuyMore data) async {
      if (data.courses != null && data.events != null) {
        courses = data.courses ?? [];
        events = data.events ?? [];
        emit(DetailCouponsSuccess());
      } else {
        emit(EmptyListCourse());
      }
    }, failure: (NetworkExceptions error) async {
      emit(DetailCouponsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void buyNow({int? itemId, String? productType}) async {
    emit(DetailCouponsLoading());
    ApiResult<dynamic> result = await repository.orderCourse(EventFreeRequest(
        quantity: 1,
        itemId: itemId,
        productType: productType,
        userName: name,
        phone: phone,
        email: email,
        provinceId: provinceId));
    result.when(success: (data) {
      emit(OrderNowSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailCouponsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  num totalEvent() {
    double total = 0;
    for (int i = 0; i < events.length; i++) {
      int preferentialFee = events[i].preferentialFee ?? 0;
      total = total + preferentialFee * events[i].quantity!;
    }
    return total;
  }

  num totalCount() {
    double total = 0;
    for (int i = 0; i < courses.length; i++) {
      int preferentialFee = courses[i].preferentialFee ?? 0;
      total = total + preferentialFee * courses[i].quantity!;
    }
    return total;
  }

  num totalBuy() {
    num totalBuyMore = totalEvent() + totalCount();
    return totalBuyMore;
  }

  void applyCoupon(
      {required String discountCode,
      required int orderId,
      required int couponId}) async {
    this.codeCoupons = discountCode;
    this.couponId = couponId;
    emit(DetailCouponsLoading());
    final result = await repository.applyCoupon(
        ApplyCouponRequest(couponCode: discountCode),
        orderId: orderId);
    result.when(success: (data) {
      emit(ApplyCouponSuccess());
    }, failure: (NetworkExceptions e) async {
      if (e is BadRequest) {
        if (e.code == ServerError.coupon_is_already_applied_error) {
          errorText = R.string.coupon_is_already_applied_error.tr();
        } else if (e.code == ServerError.coupon_is_expired_error) {
          errorText = R.string.coupon_is_expired_error.tr();
        } else if (e.code == ServerError.not_found) {
          errorText = R.string.not_found.tr();
        } else if (e.code == ServerError.coupon_is_invalid_error) {
          errorText = R.string.coupon_is_invalid_error.tr();
        } else if (e.code == ServerError.coupon_can_not_combine_error) {
          errorText = R.string.coupon_can_not_combine_error.tr();
        } else if (e.code ==
            ServerError.coupon_is_unavailable_for_all_item_order_error) {
          errorText =
              R.string.coupon_is_unavaliable_for_all_item_order_error.tr();
        } else if (e.code ==
            ServerError.coupon_level_is_unavailable_for_this_item_error) {
          errorText =
              R.string.coupon_level_is_unavailable_for_this_item_error.tr();
        } else if (e.code ==
            ServerError.coupon_is_unavailable_for_this_item_error) {
          errorText = R.string.coupon_is_unavailable_for_this_item_error.tr();
        } else if (e.code == ServerError.coupon_invalid_min_price_error) {
          errorText = R.string.coupon_invalid_min_price_error.tr();
        } else if (e.code == ServerError.coupon_reach_maximum_usage_error) {
          errorText = R.string.coupon_reach_maximum_usage_error.tr();
        } else if (e.code == ServerError.only_one_combo_coupon_error) {
          errorText = R.string.only_one_combo_coupon_error.tr();
        }
        emit(ApplyCouponFailure());
        return;
      }
      emit(DetailCouponsFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void changeText(int index) {
    this.lengthText = index;
    emit(DetailTextChangeInitial());
  }

  void validateEmail(String? email) {
    errorEmail = checkEmail(email?.trim());
    emit(ValidateError());
  }

  void validateName(String? name) {
    errorName = checkName(name?.trim());
    emit(ValidateError());
  }

  void validatePhone(String? phone) {
    if (Utils.isEmpty(phone)) {
      errorPhoneNumber = R.string.please_enter_phone_number.tr();
    } else {
      errorPhoneNumber = checkPhoneNumber(phone);
    }
    emit(ValidateError());
  }

  void getProvince() async {
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListProvince();
    apiResult.when(success: (List<CityData> data) async {
      listProvince = data;
      listProvince.forEach((element) {
        if (element.cityId ==
            fetchItemByProductIdV2?.userInformation?.provinceId) {
          cityName = element.cityName;
          cityId = element.cityId;
        }
      });
      emit(GetListProvinceSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailCouponsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectProvince(CityData? city) {
    if (selectedProvince != city) {
      emit(DetailCouponsLoading());
      this.selectedProvince = city;
      if (Utils.isEmpty(selectedProvince) &&
          Utils.isEmpty(province) &&
          fetchItemByProductIdV2?.userInformation?.provinceId == null) {
        errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      } else {
        errorProvince = null;
      }
      emit(ProvinceSuccess());
    }
  }

  void createByMore({
    String? userName,
    String? phone,
    String? email,
    String? note,
  }) async {
    errorName = checkName(userName);
    errorPhoneNumber = checkPhoneNumber(phone);
    errorEmail = checkEmail(email);
    List<int> productId = [];
    List<int> quantityProduct = [];

    for (int j = 0; j < courses.length; j++) {
      productId.add(courses[j].productId ?? 0);
      quantityProduct.add(courses[j].quantity ?? 0);
    }
    for (int i = 0; i < events.length; i++) {
      productId.add(events[i].productId ?? 0);
      quantityProduct.add(events[i].quantity ?? 0);
    }

    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorPhoneNumber)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (Utils.isEmpty(selectedProvince) &&
        fetchItemByProductIdV2?.userInformation?.provinceId == null &&
        province == null) {
      errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      emit(ListEmptyProvince());
      return;
    } else {
      errorProvince = null;
    }
    emit(DetailCouponsLoading());
    final res = await repository.buyMore(BuyMoreRequest(
      products: productId
          .asMap()
          .entries
          .map((e) => BuyMoreProducts(
              itemId: e.value, quantity: quantityProduct[e.key]))
          .toList(),
      userName: userName ?? name,
      phone: phone ?? phone,
      email: email ?? email,
      note: note,
      provinceId: selectedProvince?.cityId ??
          (fetchItemByProductIdV2?.userInformation?.provinceId != null
              ? cityId
              : provinceId),
    ));
    res.when(success: (data) {
      emit(BuyMoreSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code ==
            ServerError.event_free_duplicate_email_and_phone_error) {
          emit(OrderValidate());
        }
        if (error.code == ServerError.event_free_duplicate_phone_error) {
          emit(OrderValidatePhone());
        }
        if (error.code == ServerError.event_free_duplicate_email_error) {
          emit(OrderValidateEmail());
        }

        return;
      }
      emit(DetailCouponsFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
