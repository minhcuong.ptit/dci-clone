import 'package:equatable/equatable.dart';

abstract class DetailZenState extends Equatable {
  @override
  List<Object> get props => [];
}

class DetailZenInitial extends DetailZenState {}
class DetailZenLoading extends DetailZenState {
  @override
  String toString() {
    return 'DetailZenLoading{}';
  }
}

class DetailZenFailure extends DetailZenState {
  final String error;

  DetailZenFailure(this.error);

  @override
  String toString() {
    return 'DetailZenFailure{error: $error}';
  }
}

class DetailZenSuccess extends DetailZenState {
  @override
  String toString() {
    return 'DetailZenSuccess{}';
  }
}