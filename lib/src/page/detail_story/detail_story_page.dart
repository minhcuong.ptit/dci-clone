import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/src/page/detail_story/detail_story_cubit.dart';
import 'package:imi/src/page/detail_story/detail_story_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/stack_loading_view.dart';
import '../course_details/widget/describe_page.dart';

class DetailStoryPage extends StatefulWidget {
  final int? storyId;

  DetailStoryPage(this.storyId);

  @override
  State<DetailStoryPage> createState() => _DetailStoryPageState();
}

class _DetailStoryPageState extends State<DetailStoryPage> {
  late DetailStoryCubit _cubit;

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    // TODO: implement initState
    super.initState();
    _cubit =
        DetailStoryCubit(widget.storyId ?? 0, repository, appGraphqlRepository);
    _cubit.getStoryDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white,
      body: BlocProvider(
        create: (BuildContext context) => _cubit,
        child: BlocConsumer<DetailStoryCubit, DetailStoryState>(
          listener: (BuildContext context, state) {},
          builder: (BuildContext context, state) {
            return StackLoadingView(
              visibleLoading: state is DetailStoryLoading,
              child: ListView(
                children: [
                  buildImage(state),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: Text(
                      _cubit.storyDetail?.title ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 16.sp),
                    ),
                  ),
                  SizedBox(height: 15.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    child: HtmlWidget(
                      _cubit.storyDetail?.content ?? "",
                      textStyle: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12.sp),
                      onTapUrl: (url) async {
                        if (await canLaunch(url)) {
                          return await launch(
                            url,
                          );
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      factoryBuilder: () => MyWidgetFactory(),
                      enableCaching: true,
                    ),
                  ),
                  SizedBox(height: 15.h),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget buildImage(DetailStoryState state) {
    return Stack(
      children: [
        Visibility(
          child: Container(
            height: 200,
            width: double.infinity,
            child: CachedNetworkImage(
              imageUrl: _cubit.storyDetail?.imageUrl ?? "",
              fit: BoxFit.fill,
              placeholder: (context, url) => Container(
                height: 200,
                width: double.infinity,
                child: Image.asset(R.drawable.ic_default_banner,
                    fit: BoxFit.cover, width: double.infinity),
              ),
              errorWidget: (context, url, error) => Container(
                height: 200,
                width: double.infinity,
                child: Image.asset(R.drawable.ic_default_banner,
                    fit: BoxFit.cover, width: double.infinity),
              ),
            ),
          ),
          visible: state is DetailStorySuccess,
        ),
        Positioned(
            top: 20,
            right: 30,
            child: buildButtonClose(() {
              NavigationUtils.pop(context);
            }))
      ],
    );
  }

  Widget buildButtonClose(VoidCallback callback) {
    return InkWell(
      highlightColor: R.color.white,
      onTap: callback,
      child: Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: R.color.darkGrey.withOpacity(0.8)),
          padding: EdgeInsets.all(4),
          child: Icon(
            Icons.clear,
            size: 14.h,
            color: R.color.white,
          )),
    );
  }
}
