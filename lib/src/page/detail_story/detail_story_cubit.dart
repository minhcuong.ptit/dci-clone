import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/page/detail_story/detail_story_state.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/news_details_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';

class DetailStoryCubit extends Cubit<DetailStoryState> {
  final int postId;
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  NewsDetailsData? storyDetail;

  DetailStoryCubit(this.postId, this.repository, this.graphqlRepository)
      : super(DetailStoryInitial());

  void getStoryDetail({bool isRefresh = false}) async {
    emit(DetailStoryLoading());
    ApiResult<NewsDetailsData> getNewsDetail =
        await graphqlRepository.getNewsDetail(postId: postId);
    getNewsDetail.when(success: (NewsDetailsData data) {
      storyDetail = data;
      emit(DetailStorySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailStoryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
