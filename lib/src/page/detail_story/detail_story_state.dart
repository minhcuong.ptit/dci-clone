abstract class DetailStoryState {}

class DetailStoryInitial extends DetailStoryState {}

class DetailStoryLoading extends DetailStoryState {
  @override
  String toString() {
    return 'DetailStoryLoading{}';
  }
}

class DetailStoryFailure extends DetailStoryState {
  final String error;

  DetailStoryFailure(this.error);

  @override
  String toString() {
    return 'DetailStoryFailure{error: $error}';
  }
}

class DetailStorySuccess extends DetailStoryState {
  @override
  String toString() {
    return 'DetailStorySuccess{}';
  }
}
