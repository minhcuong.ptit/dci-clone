import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ViewProfileState extends Equatable {
  ViewProfileState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialViewProfileState extends ViewProfileState {
  @override
  String toString() {
    return 'InitialViewProfileState{}';
  }
}

class ViewProfileFailure extends ViewProfileState {
  final String error;

  ViewProfileFailure(this.error);

  @override
  String toString() {
    return 'ViewProfileFailure{error: $error}';
  }
}

class ViewProfileLoading extends ViewProfileState {
  @override
  String toString() {
    return 'ViewProfileLoading{}';
  }
}

class ViewProfileLikePost extends ViewProfileState {
  @override
  String toString() {
    return 'ViewProfileLikePost{}';
  }
}

class ViewProfileSuccess extends ViewProfileState {
  @override
  String toString() {
    return 'ViewProfileSuccess{}';
  }
}

class ReasonsReportUserSuccess extends ViewProfileState {
  @override
  String toString() {
    return 'ReasonsReportUserSuccess{}';
  }
}

class ReasonsReportPostSuccess extends ViewProfileState {
  @override
  String toString() {
    return 'ReasonsReportPostSuccess{}';
  }
}

class ReportPostSuccess extends ViewProfileState {
  @override
  String toString() {
    return 'ReportPostSuccess{}';
  }
}

class ReportSuccess extends ViewProfileState {
  @override
  String toString() {
    return 'ReportSuccess{}';
  }
}

class GetMyCommunitySuccess extends ViewProfileState {
  @override
  String toString() {
    return 'GetMyCommunitySuccess{}';
  }
}

class GetMyPostSuccess extends ViewProfileState {
  @override
  String toString() {
    return 'GetMyPostSuccess{}';
  }
}

class PickImageSuccess extends ViewProfileState {
  @override
  String toString() => 'PickImageSuccess';
}

class UpdateImageSuccess extends ViewProfileState {
  @override
  String toString() => 'UpdateImageSuccess';
}

class ReactionSuccess extends ViewProfileState {
  @override
  String toString() => 'ReactionSuccess';
}

class BookmarkSuccess extends ViewProfileState {
  @override
  String toString() => 'BookmarkSuccess';
}

class GetListCategorySuccess extends ViewProfileState {
  @override
  String toString() => 'GetListCategorySuccess';
}

class BlockUserSuccess extends ViewProfileState {
  @override
  String toString() => 'BlockUserSuccess';
}

class BlockedSuccess extends ViewProfileState {
  @override
  String toString() => 'BlockedSuccess';
}
