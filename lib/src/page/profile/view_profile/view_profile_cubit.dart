import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/request/reaction_request.dart';
import 'package:imi/src/data/network/request/report_request.dart';
import 'package:imi/src/data/network/request/update_profile_request_v2.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/list_content_response.dart';
import 'package:imi/src/data/network/response/media_response.dart';
import 'package:imi/src/data/network/response/my_profile_response.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/response/report_reasons_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/profile/view_profile/view_profile.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../../data/network/response/upload_list_url_response.dart';

class ViewProfileCubit extends Cubit<ViewProfileState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityData> clubs = [];
  XFile? fileAvatar;
  XFile? fileCover;
  List<FetchReportReason> listReasonsReportUser = [];
  List<FetchReportReason> listReasonsReportPosts = [];

  // List<String> get listCategory =>
  //     appPreferences.getListString(Const.FAVORITE_CATEGORY);

  bool isOwner(int? profileCommunityId) =>
      profileCommunityId == null ||
      (profileCommunityId) == appPreferences.getInt(Const.COMMUNITY_ID);

  int? get myCommunityId => appPreferences.getInt(Const.COMMUNITY_ID);

  String? get userId => appPreferences.getString(Const.OWNER_ID);

  List<PostData> listPost = [];
  String? nextPostsToken;
  bool _isLoadingPosts = false;
  bool isBlockUser = true;

  bool get isLoadingPosts => _isLoadingPosts;

  List<MediaData> listMedia = [];
  String? nextMediaToken;
  bool _isLoadingMedia = false;

  bool get isLoadingMedia => _isLoadingMedia;
  List<CommunityData> listSelectedCommunity = [];
  List<CategoryData> listCategory = [];
  List<CategoryData> listSelectedCategory = [];
  CommunityDataV2? data;
  late Box<CommunityDataV2> dataLocal;
  late Box<CategoryData> categories;
  Uint8List? imageBytes;
  int? communityId;
  bool navigateToFollowPage=false;

  List<int> get listFavoriteCategory => appPreferences
      .getListString(Const.FAVORITE_CATEGORY)
      .map((e) => int.parse(e))
      .toList();

  ViewProfileCubit(
      {required this.repository,
      required this.graphqlRepository,
      required this.communityId})
      : super(InitialViewProfileState());

  bool checkInfoUser() {
    if (appPreferences.getInt(Const.COMMUNITY_ID) == null ||
        appPreferences.getString(Const.OWNER_ID) == null ||
        appPreferences.getString(Const.AVATAR) == null ||
        appPreferences.getInt(Const.POSTS) == null ||
        appPreferences.getInt(Const.FOLLOWERS) == null ||
        appPreferences.getInt(Const.FOLLOWINGS) == null ||
        appPreferences.getString(Const.LEADER) == null ||
        appPreferences.getBool(Const.DCI_LEVEL) == null ||
        appPreferences.getListString(Const.FAVORITE_CATEGORY) == [] ||
        appPreferences.getString(Const.COMMUNITY_V2) == null) {
      return true;
    }
    return false;
  }

  Future getMyCommunity({bool isRefresh = false}) async {
    emit(isRefresh ? InitialViewProfileState() : ViewProfileLoading());
    ApiResult<MyProfileResponse> getProfileTask =
        await graphqlRepository.getMyProfile();
    getProfileTask.when(success: (MyProfileResponse data) async {
      appPreferences.saveCommunity(data.myCommunity);
      appPreferences.saveData(data.me);
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getCommunity({int? communityId, bool isRefresh = false}) async {
    dataLocal = Hive.box('communityDataV2');
    emit(isRefresh ? InitialViewProfileState() : ViewProfileLoading());
    ApiResult<CommunityDataV2Response> getProfileTask = await graphqlRepository
        .getCommunityV2(communityId ?? myCommunityId ?? 0);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      this.data = data.communityV2;
      dataLocal.put('data', data.communityV2!);
      if (communityId == null || isOwner(communityId))
        appPreferences.saveCommunityV2(data.communityV2);
      navigateToFollowPage=true;
      emit(GetMyCommunitySuccess());
      getReasonsReportUser();
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest &&
          error.code == ServerError.user_is_blocked_error) {
        emit(BlockedSuccess());
        return;
      }
      emit(ViewProfileFailure(""));
    });
  }

  Future<void> getListPost({int? communityId, bool isRefresh = false}) async {
    emit(ViewProfileLoading());
    if (!isRefresh && _isLoadingPosts) return;
    _isLoadingPosts = true;
    if (isRefresh) {
      nextPostsToken = null;
    }
    ApiResult<PostPageData?> getListPostTask = await graphqlRepository
        .getListPost(nextPostsToken, communityId: communityId ?? myCommunityId);
    _isLoadingPosts = false;

    getListPostTask.when(success: (PostPageData? data) {
      if (isRefresh) {
        listPost = data?.data ?? [];
      } else {
        listPost.addAll(data?.data ?? []);
      }
      nextPostsToken = data?.nextToken;
      emit(GetMyPostSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future<void> getMedia({int? communityId, bool isRefresh = false}) async {
    if (_isLoadingMedia) return;
    _isLoadingMedia = true;
    if (isRefresh) {
      nextMediaToken = null;
    }
    ApiResult<CommunityMedia?> getListMediaTask =
        await graphqlRepository.getMedia(nextMediaToken,
            communityId: communityId ?? myCommunityId ?? 0);
    _isLoadingMedia = false;

    getListMediaTask.when(success: (CommunityMedia? data) {
      if (isRefresh) {
        listMedia.clear();
        listMedia = data?.data ?? [];
      } else {
        listMedia.addAll(data?.data ?? []);
      }
      nextMediaToken = data?.nextToken;
      emit(GetMyPostSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void reaction(int postId, bool like) async {
    emit(ViewProfileLikePost());
    ApiResult<dynamic> result = await repository.reaction(ReactionRequest(
        postId: postId, action: like ? Const.LIKE : Const.DISLIKE));
    result.when(success: (dynamic response) async {
      final post = listPost.firstWhere((e) => e.id == postId.toString());
      post.isLike = like;
      if (like) {
        post.likeNumber = (post.likeNumber ?? 0) + 1;
      } else {
        post.likeNumber = (post.likeNumber ?? 1) - 1;
      }
      emit(ReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == "unavailable_post") {
          getListPost(communityId: communityId ?? myCommunityId,isRefresh: true);
          getCommunity(communityId: communityId ?? myCommunityId,isRefresh: true);
        }
        return;
      }
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshProfile() {
    emit(ViewProfileLoading());
    emit(ReactionSuccess());
  }

  void getReasonsReportUser() async {
    emit(ViewProfileLoading());
    ApiResult<ReportReasonsData> reportPost = await graphqlRepository
        .getReportReasons(reasonType: ReportReasonType.INDIVIDUAL.name);
    reportPost.when(success: (ReportReasonsData data) {
      listReasonsReportUser = data.fetchReportReasons!;
      emit(ReasonsReportUserSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getReasonsReportPost() async {
    emit(ViewProfileLoading());
    ApiResult<ReportReasonsData> reportPost = await graphqlRepository
        .getReportReasons(reasonType: ReportReasonType.POST.name);
    reportPost.when(success: (ReportReasonsData data) {
      listReasonsReportPosts = data.fetchReportReasons!;
      emit(ReasonsReportPostSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void reportPost({int? reasonId, String? postId}) async {
    emit(ViewProfileLoading());
    ApiResult<dynamic> result = await repository.reportPost(
        ReportRequest(
            reasonId: reasonId, postType: ReportReasonType.INDIVIDUAL.name),
        postId: postId);
    result.when(success: (dynamic data) {
      emit(ReportPostSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void reportUser({int? reasonId, String? reportUser}) async {
    emit(ViewProfileLoading());
    ApiResult<dynamic> result = await repository
        .reportUser(ReportRequest(reasonId: reasonId), userUuid: reportUser);
    result.when(success: (dynamic data) {
      emit(ReportSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void follow(int? communityId, bool isFollow) async {
    emit(ViewProfileLoading());
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityId,
        action: isFollow ? Const.FOLLOW : Const.UNFOLLOW));
    result.when(success: (dynamic response) async {
      data?.userView?.isFollowing = isFollow;
      if (isFollow)
        data?.communityInfo?.followerNo =
            (data?.communityInfo?.followerNo ?? 0) + 1;
      else
        data?.communityInfo?.followerNo =
            (data?.communityInfo?.followerNo ?? 0) - 1;
      emit(ReactionSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListContent({bool isRefresh = false}) async {
    emit(isRefresh ? InitialViewProfileState() : ViewProfileLoading());
    ApiResult<ListContentResponse> contentTask =
        await graphqlRepository.getListContent(listFavoriteCategory);
    contentTask.when(success: (ListContentResponse data) async {
      listCategory = data.categories ?? [];
      emit(GetListCategorySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void updateCategory(List<CategoryData> selectedCategory) {
    listSelectedCategory = selectedCategory;
    List<CategoryData> selectedCategoryUncheck = [];
    listCategory.forEach((item) {
      int index =
          listSelectedCategory.indexWhere((element) => element.id == item.id);
      if (index < 0) {
        selectedCategoryUncheck.add(item);
      }
    });
    listCategory.clear();
    listCategory.addAll(listSelectedCategory);
    listCategory.addAll(selectedCategoryUncheck);
    emit(GetListCategorySuccess());
  }

  void selectCategory(CategoryData selectedCategory) {
    emit(ViewProfileLoading());
    int index = listSelectedCategory
        .indexWhere((element) => element.id == selectedCategory.id);
    if (index >= 0) {
      listSelectedCategory.removeAt(index);
    } else {
      listSelectedCategory.add(selectedCategory);
    }
    emit(GetListCategorySuccess());
  }

  void refreshCategory() {
    emit(ViewProfileLoading());
    getCommunity();
    emit(GetListCategorySuccess());
  }

  void blockUser({String? userUuid, String? action}) async {
    emit(ViewProfileLoading());
    ApiResult<dynamic> blockUer =
        await repository.blockUser(userUuid: userUuid, action: action);
    blockUer.when(success: (dynamic data) {
      emit(BlockUserSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void pickImage({required bool isCamera, required bool isChangeAvatar}) async {
    final picker = ImagePicker();
    final file = await picker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery);
    if (file != null) {
      emit(ViewProfileLoading());
      ApiResult<List<UploadListUrlResponse>> listUrlResult =
          await repository.getListUploadUrl([file.name]);
      listUrlResult.when(success: (data) async {
        if (data.isNotEmpty) {
          await repository.uploadImage(data.first.url ?? "", File(file.path));
          isChangeAvatar
              ? await repository.updateProfile(
                  UpdateProfileRequestV2(avatarUrl: data.first.key))
              : await repository.updateProfile(
                  UpdateProfileRequestV2(imageUrl: data.first.key));
          getCommunity();
        }
      }, failure: (e) {
        emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(e)));
      });
    }
  }

  Future<Uint8List> generateThumbnail(String? url) async {
    String? fileName = await VideoThumbnail.thumbnailFile(
      video: url ?? '',
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG,
      maxHeight: 350,
      quality: 75,
    );
    final file = File(fileName!);
    return file.readAsBytesSync();
  }
}
