import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/authentication/authentication.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/follow/follow_page.dart';
import 'package:imi/src/page/home_sow/home_sow.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/messages/messages_page.dart';
import 'package:imi/src/page/profile/view_profile/view_profile.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_details.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_page.dart';
import 'package:imi/src/page/video_player/video_play_widget.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/community_info_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/page/post/post_widget/post_widget.dart';
import 'package:imi/src/widgets/popup_report_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../../data/network/response/post_data.dart';
import '../../../widgets/simple_choice_popup.dart';
import '../../club_info/club_information/club_information.dart';
import '../../comment/comment_page.dart';
import '../edit_profile/edit_profile.dart';

class ViewProfilePage extends StatefulWidget {
  final int? communityId;
  final bool? profileUser;

  const ViewProfilePage({Key? key, required this.communityId, this.profileUser})
      : super(key: key);

  @override
  _ViewProfilePageState createState() => _ViewProfilePageState();
}

class _ViewProfilePageState extends State<ViewProfilePage>
    with TickerProviderStateMixin {
  final RefreshController _refreshController = RefreshController();
  late ViewProfileCubit _cubit;

  late TabController _tabController;
  late MainCubit _mainCubit;
  late AuthenticationCubit _authCubit;

  @override
  void initState() {
    super.initState();
    try {
      _mainCubit = BlocProvider.of<MainCubit>(context);
      _authCubit = BlocProvider.of<AuthenticationCubit>(context);
    } catch (e) {}
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ViewProfileCubit(
        repository: repository,
        graphqlRepository: graphqlRepository,
        communityId: widget.communityId ?? 0);

    if (widget.communityId == null) {
      _cubit.getMyCommunity().then((value) {
        _cubit.getCommunity(communityId: widget.communityId);
        _cubit.getListPost(communityId: widget.communityId);
        _cubit.getMedia(communityId: widget.communityId);
      });
    } else {
      _cubit.getCommunity(communityId: widget.communityId);
      _cubit.getListPost(communityId: widget.communityId);
      _cubit.getMedia(communityId: widget.communityId);
    }

    _tabController = new TabController(vsync: this, length: 3);
    _cubit.getReasonsReportPost();
    if (widget.profileUser == true) {
      _tabController.index = 1;
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<ViewProfileCubit, ViewProfileState>(
        listener: (context, state) {
          if (state is! ViewProfileLoading) {
            _refreshController.refreshCompleted();
          }
          if (state is ViewProfileFailure)
            Utils.showErrorSnackBar(context, state.error);
          if (state is ReportSuccess) {
            Utils.showToast(context, R.string.report_user_success.tr());
          }
          if (state is ReportPostSuccess) {
            Utils.showToast(context, R.string.report_user_success.tr());
          }
          if (state is BlockedSuccess) {
            _cubit.isBlockUser = !_cubit.isBlockUser;
          }
        },
        builder: (context, state) {
          return Scaffold(
            backgroundColor: R.color.white,
            floatingActionButton: Visibility(
              visible: _tabController.index != 0 &&
                  appPreferences.hasDCILevel &&
                  _cubit.data?.ownerProfile?.userUuid == _cubit.userId,
              child: GestureDetector(
                onTap: () {
                  NavigationUtils.openCreatePostPopup(context,
                          targetCommunityId: _cubit.myCommunityId)
                      .then((value) {
                    if (value == Const.ACTION_REFRESH) {}
                  });
                  //NavigationUtils.navigatePage(context, MeditationDetailReportPage());
                },
                child: Container(
                  width: 42.w,
                  height: 42.w,
                  margin: EdgeInsets.only(bottom: 16.h),
                  decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.add_rounded,
                    color: R.color.white,
                  ),
                ),
              ),
            ),
            appBar: appBar(context, R.string.individual.tr().toUpperCase(),
                leadingWidget: IconButton(
                  onPressed: () {
                    NavigationUtils.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_ios),
                ),
                rightWidget: InkWell(
                  onTap: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return SingleChildScrollView(
                            padding: EdgeInsets.all(20.h),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // buildRow(context,
                                //     icon: CupertinoIcons.link,
                                //     title: R.string.copy_your_profile_path.tr(),
                                //     callback: () {
                                //   Clipboard.setData(ClipboardData(
                                //           text: _cubit.data?.dynamicLink))
                                //       .whenComplete(
                                //           () => NavigationUtils.pop(context));
                                // }),
                                if (widget.communityId != null &&
                                    widget.communityId !=
                                        appPreferences.personProfile
                                            ?.communityV2?.id) ...[
                                  buildRow(context,
                                      icon: Icons.warning_amber_outlined,
                                      title: R.string.user_reports.tr(),
                                      callback: () {
                                    NavigationUtils.pop(context);
                                    buildShowReport(context);
                                  }),
                                  Divider(),
                                  buildRow(context,
                                      icon: Icons.block,
                                      title: R.string.user_block.tr(),
                                      callback: () {
                                    NavigationUtils.pop(context);
                                    buildConfirmBlock(context);
                                  }),
                                  Divider()
                                ]
                              ],
                            ),
                          );
                        });
                  },
                  child: (_cubit.isBlockUser == false)
                      ? SizedBox()
                      : Visibility(
                          visible: (widget.communityId != null &&
                              widget.communityId !=
                                  appPreferences
                                      .personProfile?.communityV2?.id),
                          child: Padding(
                            padding: EdgeInsets.all(8.h),
                            child: Icon(CupertinoIcons.ellipsis_vertical),
                          ),
                        ),
                ),
                centerTitle: true),
            body: buildPage(context, state),
          );
        },
      ),
    );
  }

  Future<dynamic> buildConfirmBlock(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 30.h, horizontal: 24.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  R.string.user_block.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bold700
                      .copyWith(fontSize: 16.sp),
                ),
                Text(
                  R.string.confirm_block_user.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 11.sp),
                ),
                Container(
                  color: R.color.grey200,
                  height: 1,
                  margin: EdgeInsets.symmetric(vertical: 20.h),
                ),
                Row(
                  children: [
                    Expanded(
                      child: ButtonWidget(
                          title: R.string.cancel.tr(),
                          textColor: R.color.primaryColor,
                          backgroundColor: R.color.white,
                          borderColor: R.color.primaryColor,
                          textSize: 12.sp,
                          uppercaseTitle: false,
                          padding: EdgeInsets.symmetric(vertical: 6.h),
                          onPressed: () {
                            NavigationUtils.pop(context);
                          }),
                    ),
                    SizedBox(width: 20.w),
                    Expanded(
                      child: ButtonWidget(
                        title: R.string.blocked.tr(),
                        onPressed: () {
                          NavigationUtils.popToFirst(context);
                          Utils.showToast(context, R.string.block_success.tr());
                          _cubit.blockUser(
                              userUuid: _cubit.data?.ownerProfile?.userUuid,
                              action: Const.BLOCK);
                        },
                        padding: EdgeInsets.symmetric(vertical: 6.h),
                        textSize: 12.sp,
                        backgroundColor: R.color.primaryColor,
                        uppercaseTitle: false,
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  Future<dynamic> buildShowReport(BuildContext context) {
    return showModalBottomSheet(
        isScrollControlled: false,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 20.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  R.string.user_reports.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bold700
                      .copyWith(fontSize: 16.sp),
                ),
                Text(
                  R.string.reason_report.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 11.sp),
                ),
                SizedBox(height: 10.h),
                ListView.separated(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: _cubit.listReasonsReportUser.length,
                  itemBuilder: (context, int index) {
                    return GestureDetector(
                      onTap: () {
                        NavigationUtils.pop(context);
                        showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return PopupWidget(
                                title: R.string.user_reports.tr(),
                                onReport: () {
                                  NavigationUtils.pop(context);
                                  _cubit.reportUser(
                                      reportUser:
                                          _cubit.data?.ownerProfile?.userUuid ??
                                              "",
                                      reasonId: _cubit
                                          .listReasonsReportUser[index].id);
                                },
                                reasons: R.string.confirm_report.tr(),
                              );
                            });
                      },
                      child: Text(
                        _cubit.listReasonsReportUser[index].reason ?? "",
                        style: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 13.sp),
                      ),
                    );
                    // }
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider();
                  },
                ),
              ],
            ),
          );
        });
  }

  Widget buildRow(BuildContext context,
      {required IconData icon, String? title, VoidCallback? callback}) {
    return InkWell(
      onTap: callback,
      child: Row(
        children: [
          Icon(
            icon,
            size: 20.h,
          ),
          SizedBox(width: 8.h),
          Expanded(
            child: Text(
              title ?? "",
              style: Theme.of(context)
                  .textTheme
                  .medium500
                  .copyWith(height: 22 / 12),
            ),
          )
        ],
      ),
    );
  }

  void _onRefresh() {
    _cubit.getCommunity(communityId: widget.communityId, isRefresh: true);

    if (_tabController.index == 1) {
      _cubit.getListPost(communityId: widget.communityId, isRefresh: true);
    } else if (_tabController.index == 2) {
      _cubit.getMedia(isRefresh: true, communityId: widget.communityId);
    }
  }

  Widget buildPage(BuildContext context, ViewProfileState state) {
    return GestureDetector(
      onTap: () => Utils.hideKeyboard(context),
      child: StackLoadingView(
        visibleLoading: state is ViewProfileLoading,
        child: _cubit.isBlockUser == false
            ? Container(
                alignment: Alignment.topCenter,
                padding: EdgeInsets.symmetric(
                  vertical: 20.h,
                ),
                child: Text(
                  R.string.this_user_does_not_exist.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 14.sp, color: R.color.grey),
                  textAlign: TextAlign.center,
                ),
              )
            : SmartRefresher(
                controller: _refreshController,
                enablePullUp: _tabController.index != 0,
                primary: true,
                onLoading: () {
                  if (_tabController.index == 1) {
                    _cubit
                        .getListPost(communityId: widget.communityId)
                        .whenComplete(() => _refreshController.loadComplete());
                  } else if (_tabController.index == 2) {
                    _cubit
                        .getMedia(communityId: widget.communityId)
                        .whenComplete(() => _refreshController.loadComplete());
                  }
                },
                onRefresh: () {
                  // _cubit.getMyCommunity(isRefresh: true);
                  _onRefresh();
                },
                child: CustomScrollView(
                  slivers: [
                    _avatarAndBackground(),
                    _sliverSpace(16),
                    SliverToBoxAdapter(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          _cubit.data?.name ?? "",
                          textAlign: TextAlign.center,
                          maxLines: 100,
                          style: Theme.of(context)
                              .textTheme
                              .bold700
                              .copyWith(fontSize: 18.sp, height: 22.h / 18.sp),
                        ),
                      ),
                    ),
                    _sliverSpace(16),
                    SliverToBoxAdapter(
                      child: CommunityInfoWidget(
                        details: [
                          _cubit.data?.communityInfo?.followingNo ?? 0,
                          _cubit.data?.communityInfo?.followerNo ?? 0,
                          _cubit.data?.communityInfo?.postNo ?? 0,
                        ],
                        titles: [
                          R.string.following.tr(),
                          R.string.followers.tr(),
                          R.string.post.tr(),
                        ],
                        callbacks: [
                          () {
                            _cubit.navigateToFollowPage
                                ? NavigationUtils.navigatePage(
                                        context,
                                        FollowPage(
                                            type: FollowType.FOLLOWING,
                                            communityId: widget.communityId ??
                                                _cubit.myCommunityId ??
                                                0,
                                            numberNo: _cubit.data?.communityInfo
                                                    ?.followingNo ??
                                                0))
                                    .then((value) {
                                    _cubit.getCommunity(
                                        communityId: widget.communityId);
                                  })
                                : null;
                          },
                          () {
                            _cubit.navigateToFollowPage
                                ? NavigationUtils.navigatePage(
                                        context,
                                        FollowPage(
                                            type: FollowType.FOLLOWER,
                                            communityId: widget.communityId ??
                                                _cubit.myCommunityId ??
                                                0,
                                            numberNo: _cubit.data?.communityInfo
                                                    ?.followerNo ??
                                                0))
                                    .then((value) {
                                    _cubit.getCommunity(
                                        communityId: widget.communityId);
                                  })
                                : null;
                          },
                          () {
                            if (_tabController.index != 1) {
                              _tabController.animateTo(1);
                              setState(() {});
                            }
                          },
                        ],
                      ),
                    ),
                    _sliverSpace(16),
                    _actions(),
                    _sliverSpace(16),
                    _tabBar(),
                    _sliverSpace(16),
                    _tabView(state),
                  ],
                )),
      ),
    );
  }

  Widget _sliverSpace(double height) {
    return SliverToBoxAdapter(
      child: SizedBox(height: height.h),
    );
  }

  Widget _avatarAndBackground() {
    return SliverToBoxAdapter(
      child: SizedBox(
        height: 181.h,
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 136.h,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: (_cubit.data?.imageUrl == null
                              ? AssetImage(R.drawable.bg_group)
                              : NetworkImage(_cubit.data?.imageUrl ?? ""))
                          as ImageProvider)),
            ),
            if (_cubit.data?.ownerProfile?.userUuid == _cubit.userId) ...[
              _changeAvatar(
                top: 10.h,
                right: 20.w,
                isAvatar: false,
              ),
            ],
            Positioned(
              top: 91.h,
              left: MediaQuery.of(context).size.width / 2 - 45.h,
              child: AvatarWidget(
                size: 90.h,
                avatar: _cubit.data?.avatarUrl,
              ),
            ),
            if (widget.communityId == null ||
                widget.communityId == appPreferences.getInt(Const.COMMUNITY_ID))
              Positioned(
                child: GestureDetector(
                  onTap: () => _pickAvatar(true),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      border: Border.all(
                        color: R.color.white,
                        width: 2,
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: CircleAvatar(
                      backgroundColor: R.color.grey300,
                      radius: 12,
                      child: Image.asset(
                        R.drawable.ic_camera,
                        height: 16.h,
                        width: 16.w,
                      ),
                    ),
                  ),
                ),
                top: 149.h,
                left: MediaQuery.of(context).size.width / 2 + 21.h,
              )
          ],
        ),
      ),
    );
  }

  Widget _changeAvatar(
      {required double top, required double right, required bool isAvatar}) {
    return Positioned(
      top: top,
      right: right,
      child: GestureDetector(
        onTap: () => _pickAvatar(isAvatar),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: R.color.white,
              width: 2,
            ),
            shape: BoxShape.circle,
          ),
          child: CircleAvatar(
            backgroundColor: R.color.grey300,
            radius: 12,
            child: Image.asset(
              isAvatar ? R.drawable.ic_camera : R.drawable.ic_edit,
              height: isAvatar ? 16.h : 22.h,
              width: isAvatar ? 16.w : 22.w,
            ),
          ),
        ),
      ),
    );
  }

  void _pickAvatar(bool isAvatar) {
    showBarModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      expand: false,
      builder: (context) => SingleChildScrollView(
        child: SimpleChoicePopup(
          choices: [
            SimpleChoice(
                icon: R.drawable.ic_camera,
                title: R.string.profile_camera.tr(),
                onChosen: () =>
                    _cubit.pickImage(isCamera: true, isChangeAvatar: isAvatar)),
            SimpleChoice(
                icon: R.drawable.ic_gallery,
                title: R.string.profile_gallery.tr(),
                onChosen: () => _cubit.pickImage(
                    isCamera: false, isChangeAvatar: isAvatar)),
          ],
        ),
      ),
    );
  }

  Widget _actions() {
    if (_cubit.isOwner(widget.communityId))
      return SliverToBoxAdapter(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.w),
          child: ButtonWidget(
              textSize: 12,
              width: MediaQuery.of(context).size.width - 32.w,
              title: R.string.edit_information.tr(),
              uppercaseTitle: false,
              backgroundColor: R.color.primaryColor,
              radius: 48.r,
              height: 32.h,
              onPressed: () {
                NavigationUtils.navigatePage(context, EditProfilePage())
                    .then((value) {
                  if (value ?? false) {
                    _onRefresh();
                  }
                });
              }),
        ),
      );
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: ButtonWidget(
                  title: (_cubit.data?.userView?.isFollowing ?? false)
                      ? R.string.following.tr()
                      : R.string.follow.tr(),
                  uppercaseTitle: false,
                  backgroundColor: R.color.lightBlue,
                  width: 150.w,
                  radius: 48,
                  textSize: 12.sp,
                  height: 32.h,
                  onPressed: () {
                    _cubit.follow(widget.communityId,
                        !(_cubit.data?.userView?.isFollowing ?? false));
                  }),
            ),
            SizedBox(width: 10.h),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  _cubit.navigateToFollowPage
                      ? NavigationUtils.navigatePage(
                          context,
                          ChatPage(
                            chatType: ChatType.private,
                            userUID: _cubit.data?.ownerProfile?.userUuid,
                            channelName: _cubit.data?.name,
                          ),
                        )
                      : null;
                },
                child: Container(
                  height: 32.h,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 0.h),
                  decoration: BoxDecoration(
                      color: R.color.white,
                      border: Border.all(color: R.color.lightBlue, width: 1),
                      borderRadius: BorderRadius.circular(48.h)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        R.string.messaging.tr(),
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 12.sp,
                            color: R.color.lightBlue,
                            height: 1),
                      ),
                      SizedBox(width: 8.w),
                      Image.asset(
                        R.drawable.ic_message_off,
                        height: 22.h,
                        color: R.color.lightBlue,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _tabBar() {
    final _textTheme = Theme.of(context)
        .textTheme
        .bold700
        .copyWith(fontSize: 12, color: R.color.gray, height: 20 / 12);
    return SliverPersistentHeader(
      pinned: true,
      floating: true,
      delegate: StickyTabBarDelegate(
          child: TabBar(
        indicatorColor: R.color.lightBlue,
        controller: _tabController,
        onTap: (_) {
          if (_tabController.previousIndex != _tabController.index)
            setState(() {});
        },
        indicatorSize: TabBarIndicatorSize.label,
        tabs: [
          Tab(
            child: Text(R.string.profile.tr().toUpperCase(),
                style: _tabController.index != 0
                    ? _textTheme
                    : _textTheme.copyWith(color: R.color.lightBlue)),
          ),
          Tab(
            child: Text(R.string.post.tr().toUpperCase(),
                style: _tabController.index != 1
                    ? _textTheme
                    : _textTheme.copyWith(color: R.color.lightBlue)),
          ),
          Tab(
            child: Text(R.string.media.tr().toUpperCase(),
                textAlign: TextAlign.center,
                style: _tabController.index != 2
                    ? _textTheme
                    : _textTheme.copyWith(color: R.color.lightBlue)),
          ),
        ],
      )),
    );
  }

  void detailPage(PostData post) {
    if (post.communityId != null) {
      NavigationUtils.rootNavigatePage(
          context,
          CommentPage(
            postId: int.parse(post.id ?? '0'),
            onCommentSuccess: (countComment) {
              post.commentNumber = countComment;
              _cubit.refreshProfile();
            },
            onLikeSuccess: (like, likeNumber) {
              post.isLike = like;
              post.likeNumber = likeNumber;
              _cubit.refreshProfile();
            },
            onRemovePostSuccess: () {
              _cubit.getListPost(
                  communityId: widget.communityId, isRefresh: true);
              _cubit.refreshCategory();
            },
            onBookmarkSuccess: (bool bookmark) {
              post.isBookmark = bookmark;
            },
          ));
    }
  }

  Widget _tabView(ViewProfileState state) {
    switch (_tabController.index) {
      case 0:
        return SliverToBoxAdapter(
          child: ViewProfileDetails(
            communityId: widget.communityId,
          ),
        );
      case 1:
        if (_cubit.listPost.isEmpty && state is! ViewProfileLoading) {
          return SliverToBoxAdapter(
              child: Padding(
            padding: EdgeInsets.only(top: 16.0.h),
            child: Align(
                alignment: Alignment.center,
                child: Text(R.string.no_data_to_show.tr())),
          ));
        }
        return SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          if (index >= _cubit.listPost.length) return null;
          final post = _cubit.listPost[index];
          return Column(
            children: [
              PostWidget(
                reportPost: () {
                  showBarModalBottomSheet(
                      context: context,
                      builder: (_) {
                        return SingleChildScrollView(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20.h, vertical: 20.h),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                R.string.article_report.tr(),
                                style: Theme.of(_)
                                    .textTheme
                                    .bold700
                                    .copyWith(fontSize: 16.sp),
                              ),
                              Text(
                                R.string.reason_report.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 11.sp),
                              ),
                              SizedBox(height: 10.h),
                              ListView.separated(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: _cubit.listReasonsReportPosts.length,
                                itemBuilder: (context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      NavigationUtils.pop(context);
                                      showModalBottomSheet(
                                          context: context,
                                          builder: (context) {
                                            return PopupWidget(
                                              title:
                                                  R.string.article_report.tr(),
                                              reasons: R.string
                                                  .post_deletion_confirmation
                                                  .tr(),
                                              onReport: () {
                                                NavigationUtils.pop(context);
                                                _cubit.reportPost(
                                                    postId: post.id,
                                                    reasonId: _cubit
                                                        .listReasonsReportPosts[
                                                            index]
                                                        .id);
                                              },
                                            );
                                          });
                                    },
                                    child: Text(
                                      _cubit.listReasonsReportPosts[index]
                                              .reason ??
                                          "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bold700
                                          .copyWith(fontSize: 13.sp),
                                    ),
                                  );
                                  // }
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return Divider();
                                },
                              ),
                            ],
                          ),
                        );
                      });
                },
                data: post,
                viewProfileCallback: () {},
                likeCallback: () {
                  if (!(state is ViewProfileLikePost)) {
                    if (post.isLike ?? false) {
                      _cubit.reaction(int.parse(post.id ?? "0"), false);
                    } else {
                      _cubit.reaction(int.parse(post.id ?? "0"), true);
                    }
                  }
                },
                shareCallback: () {},
                moreCallback: () {},
                showLikeCallback: () {},
                posDetailCallback: () {
                  detailPage(post);
                },
                commentCallback: () {
                  detailPage(post);
                },
                viewClubCallback: () {
                  if (post.communityV2?.id != null) {
                    NavigationUtils.navigatePage(
                        context,
                        ViewClubPage(
                          communityId: post.communityV2?.id ?? 0,
                        )).then((value) {});
                  }
                },
                reportCallback: () {},
                onBookmarkSuccess: (bool bookmark) {
                  setState(() {
                    post.isBookmark = bookmark;
                  });
                },
              ),
              Container(
                height: 8.h,
                color: R.color.greyF4,
              )
            ],
          );
        }));
      default:
        if (_cubit.listMedia.isEmpty) {
          return SliverToBoxAdapter(
              child: Padding(
            padding: EdgeInsets.only(top: 16.0.h),
            child: Align(
                alignment: Alignment.center,
                child: Text(R.string.no_data_to_show.tr())),
          ));
        } else {
          final res = <Widget>[];
          for (int i = 0; i < _cubit.listMedia.length; i++) {
            final e = _cubit.listMedia[i];
            (e.s3Key!.contains('.jpg?') || e.s3Key!.contains('.png?'))
                ? ""
                : _cubit.generateThumbnail(e.s3Key);
            res.add(InkWell(
              onTap: () {
                if (e.postId != null) {
                  NavigationUtils.rootNavigatePage(
                      context,
                      CommentPage(
                        postId: e.postId,
                        onCommentSuccess: (countComment) {},
                        onLikeSuccess: (like, likeNumber) {},
                        onRemovePostSuccess: () {
                          _cubit.getListPost(
                              communityId: widget.communityId, isRefresh: true);
                        },
                        onBookmarkSuccess: (bool bookmark) {},
                      ));
                }
              },
              child: Container(
                width: MediaQuery.of(context).size.width / 3,
                height: MediaQuery.of(context).size.height / 3,
                padding: EdgeInsets.all(8.h),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: (e.s3Key!.contains('.jpg?') ||
                          e.s3Key!.contains('.png?'))
                      ? e.s3Key == null || state is ViewProfileLoading
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 20.h,
                                  width: 20.w,
                                  child: CupertinoActivityIndicator(
                                    animating: true,
                                    color: R.color.primaryColor,
                                  ),
                                ),
                              ],
                            )
                          : CachedNetworkImage(
                              memCacheHeight: 180,
                              memCacheWidth: 130,
                              imageUrl: e.s3Key ?? "",
                              fit: BoxFit.cover,
                            )
                      : Stack(
                          children: [
                            FutureBuilder<Uint8List>(
                              future: _cubit.generateThumbnail(e.s3Key),
                              builder: (context, snapshot) => snapshot.data !=
                                      null
                                  ? Image.memory(
                                      snapshot.data!,
                                      height: 350,
                                      width: MediaQuery.of(context).size.width,
                                    )
                                  : Center(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            height: 40.h,
                                            width: 40.w,
                                            child: CupertinoActivityIndicator(
                                              animating: true,
                                              color: R.color.primaryColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                            ),
                            Visibility(
                              visible: state is! ViewProfileLoading,
                              child: Positioned(
                                  top: 40.h,
                                  left: 38.w,
                                  child: Icon(
                                    CupertinoIcons.play_circle,
                                    color: R.color.white,
                                    size: 40.h,
                                  )),
                            )
                          ],
                        ),
                ),
              ),
            ));
          }
          return SliverGrid.count(
            crossAxisCount: 3,
            children: res,
          );
        }
    }
  }
}

class StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar child;

  StickyTabBarDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: this.child,
    );
  }

  @override
  double get maxExtent => this.child.preferredSize.height;

  @override
  double get minExtent => this.child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
