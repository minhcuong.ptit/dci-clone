import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/page/choose_category/choose_category_page.dart';
import 'package:imi/src/page/club_info/club_information/club_information_page.dart';
import 'package:imi/src/page/favorite/favorite.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/category_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../res/R.dart';
import '../../../data/preferences/app_preferences.dart';
import '../../../utils/const.dart';
import '../../../widgets/custom_expandable_text.dart';

class ViewProfileDetails extends StatefulWidget {
  final int? communityId;

  const ViewProfileDetails({Key? key, this.communityId}) : super(key: key);

  @override
  State<ViewProfileDetails> createState() => _ViewProfileDetailsState();
}

class _ViewProfileDetailsState extends State<ViewProfileDetails> {
  bool isExpandedJoinedGroups = false;
  late ViewProfileCubit _cubit;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _cubit = BlocProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0.w),
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (_cubit.data?.ownerProfile?.intro != "") ...[
              Text(
                R.string.introduction.tr(),
                style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 12,
                      color: R.color.textGray,
                    ),
              ),
              SizedBox(
                height: 8.h,
              ),
              CustomExpandableText(
                maxLines: 3,
                text:_cubit.data?.ownerProfile?.intro ??
                    R.string.have_not_updated_yet.tr(),
                textStyle: Theme.of(context)
                    .textTheme
                    .regular400
                    .copyWith(fontSize: 14, color: R.color.textBlack),
              ),
              SizedBox(
                height: 16.h,
              ),
            ],
            if (_cubit.data?.communityInfo?.communityRole?[0] ==
                MemberType.LEADER) ...[
              if (_cubit.data?.ownerProfile?.workingUnit != null ||
                  _cubit.data?.ownerProfile?.currentPosition != null ||
                  _cubit.data?.ownerProfile?.numberOfLeadGroup != null ||
                  _cubit.data?.ownerProfile?.achievement != null) ...[
                Text(
                  R.string.info_leader.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12, color: R.color.textGray),
                ),
              ],
              if (_cubit.data?.ownerProfile?.workingUnit != "") ...[
                SizedBox(height: 8.h),
                buildTextInfoLeader(context, false,
                    title: R.string.work_unit.tr(args: [":"]),
                    description: _cubit.data?.ownerProfile?.workingUnit ?? ""),
              ],
              if (_cubit.data?.ownerProfile?.currentPosition != "") ...[
                SizedBox(height: 8.h),
                buildTextInfoLeader(context, false,
                    title: R.string.position_leader.tr(args: [":"]),
                    description:
                        _cubit.data?.ownerProfile?.currentPosition ?? ""),
              ],
              if (_cubit.data?.ownerProfile?.numberOfLeadGroup != null) ...[
                SizedBox(height: 8.h),
                buildTextInfoLeader(context, true,
                    title: R.string.group_number.tr(args: [":"]),
                    description: _cubit.data?.ownerProfile?.numberOfLeadGroup
                            .toString() ??
                        ""),
                SizedBox(
                  height: 16.h,
                ),
              ],
              if (_cubit.data?.ownerProfile?.achievement != null) ...[
                Text(
                  R.string.achievements_of_the_members.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12, color: R.color.textGray),
                ),
                SizedBox(height: 8.h),
                Text(
                  _cubit.data?.ownerProfile?.achievement ?? "",
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 14, color: R.color.textBlack),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 16.h,
                ),
              ],
              if (_cubit.data?.ownerProfile?.leaderType != null) ...[
                Text(
                  R.string.leader_level.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12, color: R.color.textGray),
                ),
                SizedBox(height: 8.h),
                Text(
                  LeaderType.values
                      .firstWhere((element) =>
                          element.name == _cubit.data?.ownerProfile?.leaderType)
                      .title(),
                  style: Theme.of(context)
                      .textTheme
                      .labelTag
                      .copyWith(fontSize: 14, color: R.color.textBlack),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 16.h,
                ),
              ],
              Text(
                R.string.learn_information.tr(),
                style: Theme.of(context)
                    .textTheme
                    .regular400
                    .copyWith(fontSize: 12, color: R.color.textGray),
              ),
              SizedBox(height: 8.h),
              ...List.generate(
                  _cubit.data?.communityInfo?.communityLevel?.length ?? 0,
                  (index) {
                return Padding(
                  padding: EdgeInsets.only(bottom: 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          _cubit.data?.communityInfo?.communityLevel?[index]
                                  ?.levelName ??
                              "",
                          style: Theme.of(context)
                              .textTheme
                              .labelTag
                              .copyWith(fontSize: 14, color: R.color.textBlack),
                        ),
                      ),
                      SizedBox(width: 8.h),
                      Container(
                        height: 15.h,
                        width: 1.h,
                        color: R.color.grey400,
                      ),
                      SizedBox(width: 8.h),
                      Text(
                        _cubit.data?.communityInfo?.communityLevel?[index]?.year
                                .toString() ??
                            "",
                        style: Theme.of(context)
                            .textTheme
                            .labelTag
                            .copyWith(fontSize: 14, color: R.color.textBlack),
                      ),
                    ],
                  ),
                );
              })
            ],
            Text(
    _cubit.data?.ownerProfile?.countryName== "Việt Nam"||appPreferences.getString(Const.COUNTRY_NAME) == "Việt Nam"
                  ? R.string.address.tr()
                  : R.string.location.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12, color: R.color.textGray),
            ),
            SizedBox(
              height: 8.h,
            ),
            Text(
              (_cubit.data?.ownerProfile?.countryName == "Việt Nam"||appPreferences.getString(Const.COUNTRY_NAME) == "Việt Nam"
                      ?appPreferences.getString(Const.PROVINCE)?? _cubit.data?.ownerProfile?.provinceName
                      :appPreferences.getString(Const.COUNTRY_NAME)?? _cubit.data?.ownerProfile?.countryName) ??
                  R.string.have_not_updated_yet.tr(),
              style: Theme.of(context).textTheme.regular400.copyWith(
                    fontSize: 14,
                    color: R.color.textBlack,
                  ),
            ),
            SizedBox(
              height: 16.h,
            ),
            Text(
              R.string.joined_group.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 12, color: R.color.textGray),
            ),
            SizedBox(
              height: 8.h,
            ),
            _joinedGroup(),
            SizedBox(
              height: 16.h,
            ),
            Row(
              children: [
                Text(
                  R.string.favorite_contents.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12, color: R.color.textGray),
                ),
                const Spacer(),
                Visibility(
                  visible: _cubit.data?.ownerProfile?.userUuid == _cubit.userId,
                  child: GestureDetector(
                    onTap: () {
                      NavigationUtils.navigatePage(
                          context,
                          FavoritePage(
                              checkPage: true,
                              onRefresh: () {
                                _cubit.getCommunity(
                                    communityId: widget.communityId);
                              }));
                    },
                    child: Text(
                      R.string.modify.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12, color: R.color.darkerBlue),
                    ),
                  ),
                ),
              ],
            ),
            _favoriteCategories(),
            SizedBox(
              height: 16.h,
            ),
            // if (_cubit.data?.communityInfo?.communityRole?[0] ==
            //     MemberType.LEADER) ...[
            //   Visibility(
            //     visible: widget.communityId == null,
            //     child: GestureDetector(
            //       onTap: () {
            //         NavigationUtils.rootNavigatePage(
            //             context,
            //             ChooseCategoryPage(
            //               listCategory: _cubit.listCategory,
            //               listSelectedCategory: _cubit.listSelectedCategory,
            //             )).then((value) => _cubit.refreshCategory());
            //       },
            //       child: Text(
            //         R.string.change.tr(),
            //         style: Theme.of(context)
            //             .textTheme
            //             .regular400
            //             .copyWith(fontSize: 12, color: R.color.blue),
            //       ),
            //     ),
            //   ),
            // ],
            SizedBox(
              height: 50.h,
            )
          ],
        ),
      ),
    );
  }

  Widget buildTextInfoLeader(
    BuildContext context,
    bool showTextGroup, {
    String? title,
    String? description,
  }) {
    return Row(
      children: [
        Text(
          title ?? "",
          style: Theme.of(context)
              .textTheme
              .regular400
              .copyWith(fontSize: 14, color: R.color.textBlack),
        ),
        SizedBox(width: 5.h),
        Text(
          description ?? "",
          style: Theme.of(context)
              .textTheme
              .regular400
              .copyWith(fontSize: 14, color: R.color.textBlack),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        SizedBox(width: 5.h),
        Expanded(
          child: Visibility(
            visible: showTextGroup,
            child: Text(
              R.string.group.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 14, color: R.color.textBlack),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        )
      ],
    );
  }

  Widget _joinedGroup() {
    if ((_cubit.data?.userView?.joinedGroups ?? []).isEmpty)
      return Text(
        R.string.no_joined_group.tr(),
        style: Theme.of(context)
            .textTheme
            .medium500
            .copyWith(fontSize: 14, color: R.color.textBlack),
      );

    int originSize = _cubit.data?.userView?.joinedGroups?.length ?? 0;
    int size = originSize;
    if (size > 3 && !isExpandedJoinedGroups) {
      size = 3;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...List<Widget>.generate(
          size,
          (index) {
            return GestureDetector(
              onTap: () {
                NavigationUtils.navigatePage(
                    context,
                    ViewClubPage(
                        communityId: _cubit.data?.userView?.joinedGroups?[index]
                                .group?.id ??
                            0));
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 6.0.h),
                child: Row(
                  children: [
                    Image.asset(
                      R.drawable.ic_joined_group,
                      width: 24.h,
                      height: 24.h,
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Flexible(
                      child: Text(
                        _cubit.data?.userView?.joinedGroups?[index].group
                                ?.name?.trimLeft() ??
                            "",
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 14,
                            color: R.color.textBlack,
                            height: 20 / 14),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 8.w),
                      color: R.color.lightGray,
                      width: 1,
                      height: 12,
                    ),
                    Flexible(
                      child: Text(
                        _cubit.data?.userView?.joinedGroups?[index].userRoles
                                ?.first ??
                            GroupMemberRole.MEMBER.name.tr(),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 14,
                            color: R.color.textBlack,
                            height: 20 / 14),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
        if (originSize > 3)
          GestureDetector(
            onTap: () {
              setState(() {
                isExpandedJoinedGroups = !isExpandedJoinedGroups;
              });
            },
            child: Text(
              isExpandedJoinedGroups
                  ? R.string.less.tr()
                  : "..." + R.string.more.tr(),
              style: Theme.of(context)
                  .textTheme
                  .medium500
                  .copyWith(fontSize: 12, color: R.color.darkerBlue),
            ),
          )
      ],
    );
  }

  Widget _favoriteCategories() {
    if ((_cubit.data?.categories ?? []).isEmpty) {
      return Text(
        R.string.have_not_updated_yet.tr(),
        style: Theme.of(context).textTheme.medium500.copyWith(fontSize: 14),
      );
    }
    return Wrap(
      children: List.generate(
          _cubit.data?.categories?.length ?? 0,
          (index) => Container(
                width: ScreenUtil.defaultSize.width.h / 2,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.h),
                  child: CategoryWidget(
                      shadow: BoxShadow(
                        color: R.color.shadowColor,
                        spreadRadius: 0,
                        blurRadius: 3.h,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                      data: _cubit.data?.categories?[index] ?? CategoryData()),
                ),
              )),
    );
  }
}
