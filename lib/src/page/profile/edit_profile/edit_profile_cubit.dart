import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/Level.dart';
import 'package:imi/src/data/network/response/city_response.dart';
import 'package:imi/src/data/network/response/country.dart';
import 'package:imi/src/data/network/response/level_data.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/dci_event_handle.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/utils/validators.dart';
import '../../../data/network/request/profile_request.dart';
import '../../../data/network/response/person_profile.dart';
import '../../../utils/date_util.dart';
import '../../../utils/logger.dart';
import 'edit_profile.dart';

class EditProfileCubit extends Cubit<EditProfileState> with Validators {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  String? errorEmail;
  String? errorDob;
  String? errorCountry;
  String? errorProvince;
  String? errorDistrict;
  String? errorName;
  DateTime? selectedDob;
  CityData? selectedProvince;
  Country? selectedCountry;
  CityData? selectedDistrict;
  String? province;
  String? country;
  List<LevelData> levels = [];
  List<Country> listCountry = [];
  List<CityData> listProvince = [];
  List<CityData> listDistrict = [];
  bool hasChosenJoinedDCI = false;
  List<Level> joinedInformation = [];
  String countryCode = Const.DEFAULT_COUNTRY_CODE;
  PersonProfile? personProfile;
  bool get isLoggedIn => appPreferences.isLoggedIn;
  final communityId = appPreferences.getInt(Const.COMMUNITY_ID);

  EditProfileCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(InitialEditProfileState()) {
    getProfile();
    getLevels();
    getCountries();
    getProvince();
  }

  void getProfile() async {
    ApiResult<PersonProfile> apiResult =
        await graphqlRepository.getMyPersonalProfile();
    apiResult.when(success: (PersonProfile data) {
      appPreferences.setPersonProfile(data);
      personProfile = data;
      selectedAddress();
      var communityRoles =
          personProfile?.communityV2?.communityInfo?.communityRole;
      personProfile?.communityV2?.communityInfo?.communityLevel
          ?.forEach((element) {
        joinedInformation.add(Level(
          level: element?.level,
          year: element?.year ?? 0,
          communityId: communityId,
          levelName: element?.levelName,
        ));
      });
      try {
        final temp =
            joinedInformation.firstWhere((element) => element.level == 1);
        joinedInformation.removeWhere((element) => element.level == temp.level);
        joinedInformation = [temp, ...joinedInformation];
      } catch (_) {}
      checkJoinedDCI((communityRoles ?? []).contains(MemberType.MEMBER));

      emit(GetProfileSuccess());
    }, failure: (NetworkExceptions error) {
      emit(EditProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void validateEmail(String? email) {
    errorEmail = checkEmail(email?.trim());
    emit(ValidateError());
  }

  void validateName(String? name) {
    errorName = checkName(name?.trim());
    emit(ValidateError());
  }

  void selectDob(DateTime date) {
    selectedDob = date;
    errorDob = null;
    emit(SelectedDateSuccess());
  }

  void getLevels() async {
    ApiResult<List<LevelData>> apiResult = await repository.getLevels();
    apiResult.when(success: (List<LevelData> data) async {
      levels = data;
      emit(GetLevelsSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(EditProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getCountries() async {
    emit(EditProfileLoading());
    ApiResult<List<Country>> apiResult = await repository.getCountries();
    apiResult.when(success: (List<Country> data) async {
      listCountry = data;
      selectedAddress();
      emit(GetProvinceSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(EditProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getProvince() async {
    emit(EditProfileLoading());
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListProvince();
    apiResult.when(success: (List<CityData> data) async {
      listProvince = data;
      selectedAddress();
      emit(GetProvinceSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(EditProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectedAddress() {
    if (selectedCountry == null) {
      try {
        selectedCountry = listCountry
            .firstWhere((element) => element.id == personProfile?.countryId);
      } catch (e) {
        logger.e(e);
      }
    }
    if (selectedProvince == null) {
      try {
        selectedProvince = listProvince.firstWhere(
            (element) => element.cityId == personProfile?.provinceId);
        getListDistrict(selectedProvince?.cityId ?? 0);
      } catch (e) {
        logger.e(e);
      }
    }
  }

  Future getListDistrict(int cityId) async {
    emit(GetListDistrict());
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListDistrict(cityId);
    apiResult.when(success: (List<CityData> data) async {
      listDistrict = data;
      try {
        selectedDistrict = listDistrict.firstWhere(
            (element) => element.cityId == personProfile?.districtId);
      } catch (e) {
        logger.e(e);
      }
      emit(GetListDistrictSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(EditProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshEditProfile() {
    emit(InitialEditProfileState());
  }

  void selectCountry(Country? country) {
    if (selectedCountry != country) {
      emit(EditProfileLoading());
      this.selectedCountry = country;
      if (Utils.isEmpty(selectedCountry)) {
        errorCountry = R.string.text_empty.tr(args: [R.string.country.tr()]);
      } else {
        errorCountry = null;
      }
      errorProvince == null;
      errorDistrict = null;
      selectedProvince = null;
      if ((country?.name ?? '').toLowerCase() !=
          R.string.vietnam.tr().toLowerCase()) {
        selectedProvince = null;
        selectedDistrict = null;
      }
      emit(InitialEditProfileState());
    }
  }

  void selectProvince(CityData? city) {
    if (selectedProvince != city) {
      emit(EditProfileLoading());
      this.selectedProvince = city;
      if (Utils.isEmpty(selectedProvince)) {
        errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      } else {
        errorProvince = null;
      }
      selectedDistrict = null;
      emit(InitialEditProfileState());
      getListDistrict(selectedProvince?.cityId ?? 0);
    }
  }

  void selectDistrict(CityData? city) {
    emit(EditProfileLoading());
    this.selectedDistrict = city;
    if (Utils.isEmpty(selectedDistrict)) {
      errorDistrict = R.string.text_empty.tr(args: [R.string.district.tr()]);
    } else {
      errorDistrict = null;
    }
    emit(InitialEditProfileState());
  }

  void checkJoinedDCI(bool joined) {
    hasChosenJoinedDCI = joined;
    if (hasChosenJoinedDCI) {
      if (joinedInformation.isEmpty) {
        joinedInformation.add(Level(
            levelName: levels.isEmpty
                ? "Chương trình DCI level 1"
                : levels.firstWhere((element) => element.id == 1).name,
            year: 2014,
            level: 1,
            communityId: communityId));
      }
    }
    emit(SelectJoinedDCIState());
  }

  void changJoinedInformation() {
    emit(SelectJoinedDCIState());
  }

  void deleteJoinedInformation(int index) {
    joinedInformation.removeAt(index);
    emit(SelectJoinedDCIState());
  }

  void addJoinedInformation() {
    var level = Level(
        levelName: levels.isEmpty
            ? "Chương trình DCI level 1"
            : levels.firstWhere((element) => element.id == 1).name,
        year: 2014,
        communityId: communityId);
    joinedInformation.add(level);
    emit(SelectJoinedDCIState());
  }

  void prepareUpdateProfile(
      String name, String email, String info, String location, String workingAt,String position) async {
    emit(EditProfileLoading());
    errorName = checkName(name);
    errorEmail = checkEmail(email);
    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (Utils.isEmpty(selectedDob) && (personProfile?.birthday ?? '').isEmpty) {
      errorDob = R.string.please_enter_your_dob.tr();
      emit(ValidateError());
      return;
    } else {
      errorDob = null;
    }
    // if (Utils.isEmpty(selectedCountry)) {
    //   errorCountry = R.string.text_empty.tr(args: [R.string.country.tr()]);
    //   emit(ValidateError());
    //   return;
    // } else {
    //   errorCountry = null;
    // }
    if (selectedCountry?.id == 1) {
      if (Utils.isEmpty(selectedProvince)) {
        errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
        emit(ValidateError());
        return;
      } else {
        errorProvince = null;
      }
      if (Utils.isEmpty(selectedDistrict)) {
        errorDistrict =
            R.string.text_empty.tr(args: [R.string.district.tr()]);
        emit(ValidateError());
        return;
      } else {
        errorDistrict = null;
      }
    } else {
      errorProvince = null;
      errorDistrict = null;
    }
    String? uuid = appPreferences.getString(Const.ID);
    String birthday = selectedDob != null
        ? selectedDob!.toIso8601String()
        : (personProfile?.birthday ?? '');
    ProfileRequest _profileRequest = ProfileRequest()
      ..fullName = name
      ..email = email
      ..intro = info
      ..location = location
      ..workingUnit =workingAt
      ..currentPosition = position
      ..birthday = birthday
      ..countryId = 1
      ..provinceId = selectedProvince?.cityId
      ..districtId = selectedDistrict?.cityId
      ..levels = joinedInformation
      ..memberType = hasChosenJoinedDCI
          ? MemberType.MEMBER.name
          : MemberType.NORMAL_USER.name;
    profileUpdate(uuid, _profileRequest);
  }

  void profileUpdate(String? userUuid, ProfileRequest profileRequest) async {
    if (userUuid == null) {
      emit(InitialEditProfileState());
      return;
    }
    emit(EditProfileLoading());
    ApiResult<dynamic> apiResult =
        await repository.profileUpdate(userUuid, profileRequest);
    apiResult.when(success: (data) async {
      graphqlRepository
          .getCommunityV2(appPreferences.getInt(Const.COMMUNITY_ID) ?? 0)
          .then((value) {
        value.when(
            success: (data) {
              appPreferences.saveCommunityV2(data.communityV2);
              DCIEventHandler().send(UpdateProfileEvent());
            },
            failure: (_) {});
      });
      emit(EditProfileSuccess());
    }, failure: (error) async {
      emit(EditProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
