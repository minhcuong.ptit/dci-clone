import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/dropdown_column_widget.dart';
import 'package:imi/src/widgets/search_list_data_popup.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

import '../../../utils/dci_event_handle.dart';
import '../../../utils/enum.dart';
import '../../../utils/navigation_utils.dart';
import 'edit_profile.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage();

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  late EditProfileCubit _cubit;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _introductionController = TextEditingController();
  final TextEditingController _workingAtController = TextEditingController();
  final TextEditingController _positionController = TextEditingController();
  FocusNode _emailFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();
  final FocusNode _introductionFocus = FocusNode();
  final FocusNode _workingAtFocus = FocusNode();
  final FocusNode _positionFocus = FocusNode();
  final ScrollController _scrollController = ScrollController();
  bool _isChangeProfile = false;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = EditProfileCubit(
        repository: repository, graphqlRepository: graphqlRepository);
  }

  @override
  void dispose() {
    _cubit.close();
    _emailController.dispose();
    _dobController.dispose();
    _nameController.dispose();
    _addressController.dispose();
    _introductionController.dispose();
    _emailFocus.dispose();
    _nameFocus.dispose();
    _addressFocus.dispose();
    _workingAtFocus.dispose();
    _positionFocus.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        NavigationUtils.pop(context, result: _isChangeProfile);
        return true;
      },
      child: Scaffold(
        backgroundColor: R.color.white,
        appBar: appBar(
          context,
          R.string.profile_information.tr(),
          iconColor: R.color.black,
          backgroundColor: R.color.white,
          centerTitle: true,
          leadingWidget: IconButton(
              onPressed: () {
                NavigationUtils.pop(context, result: _isChangeProfile);
              },
              icon: Icon(Icons.arrow_back_ios)),
        ),
        body: BlocProvider(
          create: (context) => _cubit,
          child: BlocConsumer<EditProfileCubit, EditProfileState>(
            listener: (context, state) {
              if (state is EditProfileFailure)
                Utils.showErrorSnackBar(context, state.error);
              if (state is SelectedDateSuccess)
                _dobController.text = DateUtil.parseDateToString(
                    _cubit.selectedDob, Const.DATE_FORMAT);
              if (state is EditProfileSuccess) {
                _isChangeProfile = true;
                Utils.showToast(context, R.string.update_profile_success.tr());
                DCIEventHandler().send(UpdateProfileEvent());
                NavigationUtils.pop(context, result: _isChangeProfile);
              }
              if (state is GetProfileSuccess) {
                if (_introductionController.text.isEmpty) {
                  _introductionController.text =
                      _cubit.personProfile?.intro ?? '';
                  _nameController.text = _cubit.personProfile?.fullName ?? '';
                  _dobController.text = DateUtil.parseDateToString(
                      DateTime.parse(_cubit.personProfile?.birthday ?? ''),
                      Const.DATE_FORMAT);
                  _emailController.text = _cubit.personProfile?.email ?? '';
                  _addressController.text =
                      _cubit.personProfile?.communityV2?.location ?? '';
                  _introductionController.text =
                      _cubit.personProfile?.intro ?? '';
                  _workingAtController.text =
                      _cubit.personProfile?.workingUnit ?? "";
                  _positionController.text = _cubit.personProfile?.currentPosition ?? "";
                }
              }
            },
            builder: (context, state) {
              return buildPage(context, state);
            },
          ),
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, EditProfileState state) {
    return GestureDetector(
      onTap: () => Utils.hideKeyboard(context),
      child: KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
        return Stack(
          children: [
            StackLoadingView(
              visibleLoading: state is EditProfileLoading,
              child: Padding(
                padding: EdgeInsets.all(20.h),
                child: ListView(
                  controller: _scrollController,
                  children: [
                    _buildEnterName(context, state),
                    _buildEnterEmail(context, state),
                    _buildEnterDob(context, state),
                    _buildDropdownCountry(context, state),
                    _buildDropdownProvince(context, state),
                    _buildDropdownDistrict(context, state),
                    _buildEnterAddress(context, state),
                    if(_cubit.personProfile?.communityV2?.communityInfo?.communityRole?[0] ==
                        MemberType.LEADER)...[
                    _buildEnterWorkingAt(context, state),
                    _buildPosition(context, state)],
                    _buildEnterIntroduction(context, state),
                    _checkboxDCI(),
                    _listJoinedInformation(),
                    SizedBox(height: 140.h),
                  ],
                ),
              ),
            ),
            Positioned(
                left: 0,
                bottom: 0,
                right: 0,
                child: Visibility(
                  visible: !isKeyboardVisible,
                  child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(
                          horizontal: 30.h, vertical: 40.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25.h),
                          topRight: Radius.circular(25.h),
                        ),
                        color: R.color.white,
                      ),
                      child: SizedBox(
                        child: ButtonWidget(
                          title: R.string.save_changes.tr(),
                          padding: EdgeInsets.symmetric(horizontal: 24.w),
                          height: 56.h,
                          backgroundColor: R.color.orange,
                          textSize: 16.sp,
                          textColor: R.color.white,
                          onPressed: state is EditProfileLoading
                              ? null
                              : () => updateProfile(),
                        ),
                      )),
                )),
          ],
        );
      }),
    );
  }

  Widget _buildEnterName(BuildContext context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 0.h),
      child: TextFieldWidget(
        autoFocus: false,
        // isEnable: !(state is EditProfileLoading),
        controller: _nameController,
        focusNode: _nameFocus,
        inputFormatters: [
          //FilteringTextInputFormatter.allow(Validators.nameRegex),
          LengthLimitingTextInputFormatter(255)
        ],
        isRequired: true,
        labelText: R.string.full_name.tr(),
        hintText: R.string.enter_your_name.tr(),
        errorText: _cubit.errorName,
        labelStyle: labelStyle(),
        onChanged: _cubit.validateName,
        onSubmitted: state is EditProfileLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _nameFocus, _emailFocus),
      ),
    );
  }

  Widget _buildEnterEmail(BuildContext context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 15.h),
      child: TextFieldWidget(
        autoFocus: false,
        // isEnable: !(state is EditProfileLoading),
        controller: _emailController,
        focusNode: _emailFocus,
        keyboardType: TextInputType.emailAddress,
        isRequired: true,
        maxLines: 1,
        inputFormatters: [LengthLimitingTextInputFormatter(255)],
        labelText: R.string.email.tr(),
        hintText: R.string.enter_your_email.tr(),
        labelStyle: labelStyle(),
        errorText: _cubit.errorEmail,
        onChanged: _cubit.validateEmail,
        onTap: () {},
        onSubmitted: state is EditProfileLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _emailFocus, _addressFocus),
      ),
    );
  }

  Widget _buildEnterDob(BuildContext context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 15.h),
      child: TextFieldWidget(
          autoFocus: false,
          controller: _dobController,
          readOnly: true,
          isRequired: true,
          // will disable paste operation
          onTap: pickDob,
          hintText: R.string.hint_dob.tr(),
          textInputAction: TextInputAction.next,
          keyboardType: TextInputType.text,
          labelText: R.string.dob.tr(),
          labelStyle: labelStyle(),
          errorText: _cubit.errorDob,
          suffixIcon: GestureDetector(
            onTap: pickDob,
            child: Icon(
              Icons.calendar_today_outlined,
              color: R.color.textGreyColor,
              size: 25.h,
            ),
          )),
    );
  }

  void pickDob() {
    DatePicker.showDatePicker(context,
        maxTime: DateTime.now(),
        minTime: DateTime(1922, 1, 1),
        showTitleActions: true,
        locale: appPreferences.locale,
        currentTime: _cubit.selectedDob, onConfirm: (date) {
      _cubit.selectDob(date);
    });
  }

  Widget _buildDropdownProvince(BuildContext context, EditProfileState state) {
    List<String> listProvince =
        _cubit.listProvince.map((e) => e.cityName ?? "").toList();
    return Padding(
      padding: EdgeInsets.only(top: 15.h),
      child: DropdownColumnWidget(
        // isRequired: true,
        onTap: () {
          if ((_cubit.selectedCountry?.id ?? 0) > 1) return;
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listProvince,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.province.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectProvince(_cubit.listProvince[index]);
                  },
                );
              });
        },
        hintText: R.string.province.tr(),
        errorText: _cubit.errorProvince,
        labelText: R.string.province.tr(),
        currentValue: _cubit.selectedProvince?.cityName ?? "Hà Nội",
        listData: listProvince,
        selectedValue: (String? value) {},
      ),
    );
  }

  Widget _buildDropdownCountry(BuildContext context, EditProfileState state) {
    List<String> listCountry =
        _cubit.listCountry.map((e) => e.name ?? "").toList();
    return Padding(
      padding: EdgeInsets.only(top: 15.h),
      child: DropdownColumnWidget(
        //isRequired: true,
        onTap: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listCountry,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.country.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectCountry(_cubit.listCountry[index]);
                  },
                );
              });
        },
        hintText: R.string.country.tr(),
        //errorText: _cubit.errorCountry,
        labelText: R.string.country.tr(),
        currentValue: _cubit.selectedCountry?.name ?? R.string.vietnam.tr(),
        listData: listCountry,
        selectedValue: (String? value) {},
      ),
    );
  }

  Widget _buildDropdownDistrict(BuildContext context, EditProfileState state) {
    List<String> list =
        _cubit.listDistrict.map((e) => e.cityName ?? "").toList();
    return Padding(
      padding: EdgeInsets.only(top: 15.h),
      child: DropdownColumnWidget(
        // isRequired: true,
        onTap: () {
          if ((_cubit.selectedCountry?.id ?? 0) > 1) return;
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: list,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.district.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectDistrict(_cubit.listDistrict[index]);
                  },
                );
              });
        },
        isEnabled: true,
        hintText: R.string.district.tr(),
        labelText: R.string.district.tr(),
        errorText: _cubit.errorDistrict,
        currentValue: _cubit.selectedDistrict?.cityName,
        listData: list,
        selectedValue: (String? value) {},
      ),
    );
  }

  Widget _buildEnterAddress(context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 0.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _addressController,
        focusNode: _addressFocus,
        keyboardType: TextInputType.multiline,
        maxLength: 500,
        inputFormatters: [
          //FilteringTextInputFormatter.allow(Validators.nameRegex),
          LengthLimitingTextInputFormatter(500)
        ],
        buildCounter: (context,
            {required int currentLength,
            required bool isFocused,
            required int? maxLength}) {
          return Text("$currentLength/$maxLength");
        },
        maxLines: 5,
        minLines: 1,
        isRequired: true,
        labelText: R.string.address.tr(),
        hintText: R.string.address.tr(),
        labelStyle: labelStyle(),
        onSubmitted: state is EditProfileLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _addressFocus, _workingAtFocus),
      ),
    );
  }

  Widget _buildEnterWorkingAt(context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 0.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _workingAtController,
        focusNode: _workingAtFocus,
        keyboardType: TextInputType.multiline,
        maxLength: 250,
        inputFormatters: [LengthLimitingTextInputFormatter(250)],
        buildCounter: (context,
            {required int currentLength,
            required bool isFocused,
            required int? maxLength}) {
          return Text("$currentLength/$maxLength");
        },
        maxLines: 5,
        minLines: 1,
        isRequired: true,
        labelText: R.string.work_unit.tr(args: [""]),
        hintText: R.string.work_unit.tr(args: [""]),
        labelStyle: labelStyle(),
        onSubmitted: state is EditProfileLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _workingAtFocus, _positionFocus),
      ),
    );
  }

  Widget _buildPosition(context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 0.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _positionController,
        focusNode: _positionFocus,
        keyboardType: TextInputType.multiline,
        maxLength: 250,
        inputFormatters: [LengthLimitingTextInputFormatter(250)],
        buildCounter: (context,
            {required int currentLength,
            required bool isFocused,
            required int? maxLength}) {
          return Text("$currentLength/$maxLength");
        },
        maxLines: 5,
        minLines: 1,
        isRequired: true,
        labelText: R.string.position_leader.tr(args: [""]),
        hintText: R.string.position_leader.tr(args: [""]),
        labelStyle: labelStyle(),
        onSubmitted: state is EditProfileLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _positionFocus, _introductionFocus),
      ),
    );
  }

  Widget _buildEnterIntroduction(context, EditProfileState state) {
    return Padding(
      padding: EdgeInsets.only(top: 0.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _introductionController,
        focusNode: _introductionFocus,
        keyboardType: TextInputType.multiline,
        maxLength: 500,
        inputFormatters: [LengthLimitingTextInputFormatter(500)],
        buildCounter: (context,
            {required int currentLength,
            required bool isFocused,
            required int? maxLength}) {
          return Text("$currentLength/$maxLength");
        },
        maxLines: 5,
        minLines: 1,
        isRequired: true,
        labelText: R.string.short_introduction.tr(),
        hintText: R.string.short_introduction.tr(),
        labelStyle: labelStyle(),
        onSubmitted: state is EditProfileLoading
            ? null
            : (_) => Utils.hideKeyboard(context),
      ),
    );
  }

  Widget _checkboxDCI() {
    return Row(
      children: [
        Checkbox(
          value: _cubit.hasChosenJoinedDCI,
          checkColor: R.color.white,
          activeColor: R.color.blue,
          onChanged: (value) {
            _cubit.checkJoinedDCI(true);
          },
        ),
        Text(
          R.string.joined_dci.tr(),
          style:
              Theme.of(context).textTheme.regular400.copyWith(fontSize: 12.sp),
        ),
        Spacer(),
        Checkbox(
          value: !_cubit.hasChosenJoinedDCI,
          checkColor: R.color.white,
          activeColor: R.color.blue,
          onChanged: (value) {
            _cubit.checkJoinedDCI(false);
          },
        ),
        Text(
          R.string.not_joined_dci.tr(),
          style:
              Theme.of(context).textTheme.regular400.copyWith(fontSize: 12.sp),
        ),
      ],
    );
  }

  Widget _listJoinedInformation() {
    if (_cubit.hasChosenJoinedDCI == false) return SizedBox.shrink();
    List<Widget> children = [];
    List<String> listLevel = _cubit.levels.map((e) => e.name ?? "").toList();
    final now = DateTime.now();
    for (int i = 0; i < _cubit.joinedInformation.length; i++) {
      children.add(Stack(
        children: [
          Column(
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 7,
                    child: DropdownColumnWidget(
                      showBorder: false,
                      onTap: () {
                        if (i == 0) return;
                        showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            backgroundColor: Colors.transparent,
                            builder: (context) {
                              return SearchListDataPopup(
                                listData: listLevel,
                                hintText: R.string.search.tr() +
                                    " " +
                                    R.string.program.tr().toLowerCase(),
                                onSelect: (int index) {
                                  _cubit.joinedInformation[i].level =
                                      _cubit.levels[index].id;
                                  _cubit.joinedInformation[i].levelName =
                                      _cubit.levels[index].name;
                                  _cubit.changJoinedInformation();
                                },
                              );
                            });
                      },
                      isEnabled: true,
                      hintText: R.string.program.tr(),
                      labelText: R.string.program.tr(),
                      currentValue: _cubit.joinedInformation[i].levelName,
                      listData:
                          List.generate(12, (index) => (index + 1).toString()),
                      selectedValue: (_) {},
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: DropdownColumnWidget(
                      showBorder: false,
                      // isRequired: true,
                      onTap: () {
                        showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            backgroundColor: Colors.transparent,
                            builder: (context) {
                              return SearchListDataPopup(
                                listData: List.generate(now.year - 2010 + 1,
                                    (index) => (index + 2010).toString()),
                                hintText: R.string.search.tr() +
                                    " " +
                                    R.string.year.tr().toLowerCase(),
                                onSelect: (int index) {
                                  _cubit.joinedInformation[i].year =
                                      2010 + index;
                                  _cubit.changJoinedInformation();
                                },
                              );
                            });
                      },
                      isEnabled: true,
                      hintText: R.string.year.tr(),
                      labelText: R.string.year.tr(),
                      currentValue: _cubit.joinedInformation[i].year.toString(),
                      listData: List.generate(now.year - 2010 + 1,
                          (index) => (index + 2010).toString()),
                      selectedValue: (String? value) {},
                    ),
                  )
                ],
              ),
              Container(
                height: 1,
                color: R.color.borderColor,
                padding: EdgeInsets.only(bottom: 7.h),
              )
            ],
          ),
          if (i != 0)
            Positioned(
                right: 0,
                top: 0,
                child: GestureDetector(
                  onTap: () {
                    _cubit.deleteJoinedInformation(i);
                  },
                  child: Image.asset(
                    R.drawable.ic_delete_rounded,
                    width: 16.w,
                  ),
                ))
        ],
      ));
    }
    return Column(
      children: [
        ...children,
        if (_cubit.joinedInformation.length < 12)
          InkWell(
            onTap: () {
              _cubit.addJoinedInformation();
              _scrollController.animateTo(
                  _scrollController.position.maxScrollExtent,
                  duration: Duration(milliseconds: 200),
                  curve: Curves.easeIn);
            },
            child: Row(
              children: [
                Image.asset(
                  R.drawable.ic_add_circle,
                  width: 24.w,
                ),
                Text(
                  R.string.add_the_program_learned.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bold700
                      .copyWith(fontSize: 12.sp),
                )
              ],
            ),
          )
      ],
    );
  }

  TextStyle labelStyle() {
    return Theme.of(context)
        .textTheme
        .regular400
        .copyWith(fontSize: 14.sp, color: R.color.gray);
  }

  void updateProfile() {
    _cubit.prepareUpdateProfile(
        _nameController.text.trim(),
        _emailController.text.trim(),
        _introductionController.text.trim(),
        _addressController.text.trim(),
        _workingAtController.text.trim(),
        _positionController.text.trim());
  }
}
