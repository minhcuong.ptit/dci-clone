import 'package:meta/meta.dart';

@immutable
abstract class EditProfileState {
  EditProfileState([List props = const []]) : super();
}

class InitialEditProfileState extends EditProfileState {
  @override
  String toString() {
    return 'InitialEditProfileState{}';
  }
}

class EditProfileFailure extends EditProfileState {
  final String? error;

  EditProfileFailure(this.error);

  @override
  String toString() {
    return 'EditProfileFailure{error: $error}';
  }
}

class EditProfileLoading extends EditProfileState {
  @override
  String toString() {
    return 'EditProfileLoading{}';
  }
}

class GetProfileSuccess extends EditProfileState {
  @override
  String toString() {
    return 'GetProfileSuccess{}';
  }
}

class EditProfileSuccess extends EditProfileState {
  @override
  String toString() {
    return 'EditProfileSuccess{}';
  }
}

class UpdateImageSuccess extends EditProfileState {
  @override
  String toString() => 'UpdateImageSuccess';
}

class PickImageSuccess extends EditProfileState {
  @override
  String toString() => 'PickImageSuccess';
}

class SelectedDateSuccess extends EditProfileState {
  @override
  String toString() {
    return 'SelectedDateSuccess{}';
  }
}

class ValidateError extends EditProfileState {
  @override
  String toString() {
    return 'ValidateError {}';
  }
}

class GetProvinceSuccess extends EditProfileState {
  @override
  String toString() => 'GetProvinceSuccess';
}

class SelectJoinedDCIState extends EditProfileState {}

class GetListDistrict extends EditProfileState {
  @override
  String toString() {
    return 'GetListDistrict{}';
  }
}
class GetListDistrictSuccess extends EditProfileState {
  @override
  String toString() {
    return 'GetListDistrictSuccess{}';
  }
}
class GetLevelsSuccess extends EditProfileState {
  @override
  String toString() => 'GetLevelsSuccess';
}