import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/widgets/video_widget.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

import '../../../res/R.dart';
import '../../widgets/custom_appbar.dart';

class VideoPlayPage extends StatefulWidget {
  final String url;

  const VideoPlayPage({Key? key, required this.url}) : super(key: key);
  @override
  _VideoPlayPageState createState() => _VideoPlayPageState();
}

class _VideoPlayPageState extends State<VideoPlayPage> {
  late VideoPlayerController _controller;
  Orientation? _orientation;
  @override
  void initState() {
    super.initState();
    _enableRotation();
    _controller = VideoPlayerController.network(widget.url)
      ..initialize().then((_) {
        setState(() {});
      });
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: _orientation == Orientation.landscape,
        backgroundColor: R.color.gray,
        appBar: appBar(context, '',
            backgroundColor: Colors.transparent, iconColor: R.color.white),
        body: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            _orientation = orientation;
            return SafeArea(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                child: VideoWidget(
                  url: widget.url,
                  isFromPage: true,
                ),
              ),
            );
          },
        ));
  }

  @override
  void dispose() {
    super.dispose();
    _disableRotation();
    _controller.dispose();
  }

  void _enableRotation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  void _disableRotation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }
}
