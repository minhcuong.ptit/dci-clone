import 'dart:async';
import 'dart:io';
import 'dart:developer';
import 'dart:typed_data';
import 'package:chewie/chewie.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/src/dci_application.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:video_player/video_player.dart';
import 'package:visibility_detector/visibility_detector.dart';
import '../../../res/R.dart';
import '../../utils/utils.dart';

class VideoPlayWidget extends StatefulWidget {
  const VideoPlayWidget(
      {Key? key,
      this.url = '',
      this.xFile,
      this.showControls,
      this.showLoading})
      : super(key: key);

  final String? url;
  final XFile? xFile;
  final bool? showControls;
  final bool? showLoading;

  @override
  State<StatefulWidget> createState() {
    return _VideoPlayWidgetState();
  }
}

class _VideoPlayWidgetState extends State<VideoPlayWidget>
    with AutomaticKeepAliveClientMixin {
  late VideoPlayerController _videoPlayerController1;
  ChewieController? _chewieController;
  bool _isPlaying = false;
   Uint8List? imageBytes;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _videoPlayerController1.dispose();
    _chewieController?.dispose();
    Utils.portraitModeOnly();
    super.dispose();
  }

  Future<void> initializePlayer() async {
    if (_chewieController == null) {
      if (widget.url?.isNotEmpty ?? false) {
        _videoPlayerController1 =
            VideoPlayerController.network(widget.url ?? '');
      } else {
        _videoPlayerController1 =
            VideoPlayerController.file(File(widget.xFile?.path ?? ''));
      }
      await _videoPlayerController1.initialize();
      _createChewieController();
    }
  }
  void _createChewieController() {
    _chewieController = ChewieController(
      aspectRatio: _videoPlayerController1.value.aspectRatio,
      videoPlayerController: _videoPlayerController1,
      autoPlay: false,
      looping: false,
      showOptions: false,
      showControls: widget.showControls ?? true,
      allowPlaybackSpeedChanging: false,
      allowMuting: false,
      deviceOrientationsAfterFullScreen:[
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown],
      autoInitialize: true,
      allowedScreenSleep: true,
        customControls: CupertinoControls(
          backgroundColor: Colors.white54,
          iconColor: R.color.primaryColor,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    if(DciApplication.pauseVideoInDetail){
      _chewieController?.pause();
    }
    super.build(context);
    return
      VisibilityDetector(
      key: Key(widget.url.toString()),
      onVisibilityChanged: (visibilityInfo) {
        var visiblePercentage = visibilityInfo.visibleFraction * 100;
        if(visiblePercentage<5){
          DciApplication.videoPlayerController1?.pause();
          _chewieController?.pause();
        }
      },
      child:
      Column(
        children: <Widget>[
          Expanded(
            child: FutureBuilder(
              future: initializePlayer(),
              builder: (context, snapshot) => Center(

                child: _chewieController != null &&
                        _chewieController!
                            .videoPlayerController.value.isInitialized
                    ? Chewie(
                        controller: _chewieController!,
                      ) :  widget.showLoading == true
                        ? SizedBox()
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                              SizedBox(height: 20),
                              Text(R.string.loading.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelSmallText
                                      .copyWith(fontSize: 17.sp)),
                            ],
                          ),
              ),
            ),
          ),
        ],
      )
    );
  }

  @override
  bool get wantKeepAlive => true;
}
