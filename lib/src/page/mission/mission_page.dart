import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/home_sow/home_sow_page.dart';
import 'package:imi/src/page/mission/mission.dart';
import 'package:imi/src/page/practice_zen/practice_zen_page.dart';
import 'package:imi/src/page/start_practice/widget/challenge_guide_widget.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_switch_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../utils/const.dart';
import 'dialog_confirm.dart';

class MissionPage extends StatefulWidget {
  final String? nameChallenge;
  final int? challengeMissionHistoryId;
  final int intDay;
  final int? challengeId;
  final int? challengeHistoryId;
  final String? icon;
  final String? description;
  final bool? isCompletedChallenge;
  final bool? isCompletedDay;
  const MissionPage(
      {Key? key,
      this.challengeMissionHistoryId,
      this.nameChallenge,
      required this.intDay,
      this.challengeId,
      this.challengeHistoryId,
      this.icon,
      this.isCompletedChallenge,
      this.isCompletedDay,
      this.description})
      : super(key: key);

  @override
  State<MissionPage> createState() => _MissionPageState();
}

class _MissionPageState extends State<MissionPage> {
  late MissionCubit _cubit;
  TextEditingController _textEditingController = TextEditingController();
  RefreshController _refreshController = RefreshController();
  bool hideNote = false;
  var textLength = 0;
  bool get showDialogConfirm => ((_cubit.fetchMissionOfDay?.completedDate?.day != null &&
      _cubit.fetchMissionOfDay!.completedDate!.day <
          DateTime.now().day));

  // String? get note => appPreferences.getString("NOTE_CHALLENGE");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = MissionCubit(
        repository, graphqlRepository, widget.challengeMissionHistoryId);
    _cubit.getMission();
    appPreferences.setData(Const.RESOLVE_CHALLENGE,widget.isCompletedChallenge);
    appPreferences.setData(Const.RESOLVE_CHALLENGE_DAY,widget.isCompletedDay);
  }
  @override
  void dispose() {
    _cubit.close();
    _refreshController.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<MissionCubit, MissionState>(
        listener: (BuildContext context, state) {
          if (state is! MissionLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is SaveMissionSuccess) {
                bool isSuccess = _cubit.listMission.fold<bool>(
                true,
                (previousValue, element) =>
                    previousValue && (element.completed ?? false));
            NavigationUtils.pop(context, result: [isSuccess,state.results]);
          }
          if (state is MissionSuccess) {
            _textEditingController.text = _cubit.missionData?.note ?? "";
          }
        },
        builder: (BuildContext context, state) {
          return WillPopScope(
            onWillPop: () async {
              if (showDialogConfirm) {
                NavigationUtils.pop(context);
              } else {
                !_cubit.checkChangeStatus(_textEditingController.text)? showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return DialogConfirm(
                        confirm: (){
                          NavigationUtils.pop(context);
                          _cubit.saveMission(
                              context: context,
                              challengeId: widget.challengeId,
                              challengeHistoryId:
                              widget.challengeHistoryId,
                              challengeMissionHistoryId: widget
                                  .challengeMissionHistoryId,
                              note: _textEditingController.text
                                  .trim());
                        },
                        cancel: (){
                          NavigationUtils.pop(context);
                          NavigationUtils.pop(context);
                        },
                      );
                    }):  NavigationUtils.pop(context);;
              }
              return true;
            },
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: R.color.primaryColor,
                title: Text(R.string.quest.tr().toUpperCase()),
                centerTitle: true,
                automaticallyImplyLeading: false,
                leading: InkWell(
                    onTap: () {
                      if (showDialogConfirm) {
                        NavigationUtils.pop(context);
                      } else {
                        !_cubit.checkChangeStatus(_textEditingController.text)?    showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return DialogConfirm(
                                confirm: (){
                                  NavigationUtils.pop(context);
                                  _cubit.saveMission(
                                    context: context,
                                      challengeId: widget.challengeId,
                                      challengeHistoryId:
                                      widget.challengeHistoryId,
                                      challengeMissionHistoryId: widget
                                          .challengeMissionHistoryId,
                                      note: _textEditingController.text
                                          .trim());
                                },
                                cancel: (){
                                  NavigationUtils.pop(context);
                                  NavigationUtils.pop(context);
                                },
                              );;
                            }):NavigationUtils.pop(context);
                      }
                    },
                    child: Icon(
                      CupertinoIcons.back,
                      color: R.color.white,
                    )),
              ),
              body: StackLoadingView(
                visibleLoading: state is MissionLoading,
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  padding: EdgeInsets.all(20.h),
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.h, vertical: 12.h),
                            decoration: BoxDecoration(
                              color: R.color.white,
                              borderRadius: BorderRadius.circular(10.h),
                              boxShadow: [
                                BoxShadow(
                                  color: R.color.black.withOpacity(0.3),
                                  blurRadius: 10,
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Text(
                                  R.string.you_are_taking.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 12.sp),
                                ),
                                Text(
                                  widget.nameChallenge?.toUpperCase() ?? "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 14.sp, color: R.color.blue),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: 50.h),
                        GestureDetector(
                          onTap: () {
                            NavigationUtils.rootNavigatePage(
                                context,
                                ChallengeGuideWidget(
                                  description: widget.description,
                                ));
                          },
                          child: Container(
                            height: 30.h,
                            width: 30.h,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: R.color.primaryColor,
                            ),
                            child: Icon(
                              CupertinoIcons.question,
                              color: R.color.white,
                              size: 18.h,
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 10.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          widget.icon ?? "",
                          height: 50.h,
                          width: 50.h,
                        ),
                        SizedBox(width: 5.h),
                        Expanded(
                          child: Text(
                            R.string.day.tr() +
                                " " +
                                "${widget.intDay + 1}: " +
                                R.string.please_complete_the_following_tasks
                                    .tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 14.sp, height: 18.h / 14.sp),
                          ),
                        ),
                        SizedBox(width: 8.h),
                        Tooltip(
                          message:
                              R.string.tasks_can_be_performed_in_any_order.tr(),
                          triggerMode: TooltipTriggerMode.tap,
                          verticalOffset: 15.h,
                          margin: EdgeInsets.only(
                              left: context.width * 0.45,
                              right: 10.h),
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.h, vertical: 12.sp),
                          preferBelow: true,
                          textStyle:
                              Theme.of(context).textTheme.regular400.copyWith(
                                    fontSize: 11.sp,
                                    height: 16.h / 11.sp,
                                  ),
                          decoration: BoxDecoration(
                              color: R.color.white,
                              borderRadius: BorderRadius.circular(10.h),
                              boxShadow: [
                                BoxShadow(
                                  color: R.color.black.withOpacity(0.3),
                                  blurRadius: 10,
                                ),
                              ]),
                          child: Image.asset(
                            R.drawable.ic_report_v3,
                            height: 20.h,
                            width: 20.h,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 25.h),
                    buildListView(),
                    SizedBox(height: 15.h),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.w),
                      width: 100.w,
                      height: 260.h,
                      decoration: BoxDecoration(
                          border: Border.all(color: R.color.grey, width: 1),
                          borderRadius: BorderRadius.circular(5.h)),
                      child: SizedBox(
                        height: 280.h,
                        child: TextField(
                          // enabled: widget.enable,
                          autofocus: false,
                          controller: _textEditingController,
                          // maxLength:2000,
                          maxLines: 1000,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(2000)
                          ],
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(top: 2.h),
                            border: InputBorder.none,
                            hintStyle: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 12.sp,
                                    height: 20.h / 12.sp,
                                    color: R.color.textGray),
                            hintText: R.string.note_mission.tr(),
                          ),
                          onChanged: (text) {
                            setState((){
                              textLength=text.length;
                            });
                          },
                          onSubmitted: (text) {},
                          // buildCounter: _buildCounter,
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 12.sp,
                                  height: 20.h / 12.sp,
                                  color: R.color.textGray),
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "${textLength==0?_textEditingController.text.length:textLength}" + "/" + "2000",
                        textAlign: TextAlign.end,
                      ),
                    ),
                    SizedBox(height: 10.h),
                    Visibility(
                      visible: state is! MissionLoading,
                      child: ButtonWidget(
                        title: R.string.save.tr(),
                        textSize: 14.sp,
                        onPressed: (_cubit.fetchMissionOfDay?.completedDate
                                        ?.day !=
                                    null &&
                                !DateUtils.isSameDay(
                                    _cubit.fetchMissionOfDay!.completedDate!,
                                    DateTime.now()))
                            ? null
                            : () {
                                _cubit.saveMission(
                                  context: context,
                                    challengeId: widget.challengeId,
                                    challengeHistoryId:
                                        widget.challengeHistoryId,
                                    challengeMissionHistoryId:
                                        widget.challengeMissionHistoryId,
                                    note: _textEditingController.text);
                              },
                        padding: EdgeInsets.symmetric(vertical: 8.h),
                        backgroundColor: R.color.primaryColor,
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildListView() {
    return ListView.builder(
        shrinkWrap: true,
        physics:const NeverScrollableScrollPhysics(),
        itemCount: _cubit.listMission.length,
        itemBuilder: (context, int index) {
          return buildMission(
            context,
            selectedMission: () {
              if (_cubit.listMission[index].mission?.type?.name ==
                  MissionType.MEDITATION.name) {
                NavigationUtils.rootNavigatePage(context, PracticeZenPage());
              } else if (_cubit.listMission[index].mission?.type?.name ==
                  MissionType.PLAN_SEED.name) {
                NavigationUtils.rootNavigatePage(context, HomeSowPage());
              }
            },
            steps: "${index + 1}",
            mission: _cubit.listMission[index].mission?.type?.title(),
            description: _cubit.listMission[index].mission?.name ?? "",
            value: _cubit.listMission[index].completed,
            onChanged: (bool value) {
              setState(() {
                _cubit.listMission[index].completed = value;
                _cubit.updateMission(
                    index: index,
                    id: _cubit.listMission[index].mission?.id,
                    value: value);
              });
            },
            hideChallenge: _cubit.listMission[index].mission?.type?.name ==
                    MissionType.PRACTICE.name
                ? false
                : true,
            image: _cubit.listMission[index].mission?.type?.name ==
                    MissionType.MEDITATION.name
                ? R.drawable.ic_zen
                : _cubit.listMission[index].mission?.type?.name ==
                        MissionType.PLAN_SEED.name
                    ? R.drawable.ic_plan_tree
                    : "",
            challenge: _cubit.listMission[index].mission?.type?.name ==
                    MissionType.MEDITATION.name
                ? R.string.meditation.tr()
                : _cubit.listMission[index].mission?.type?.name ==
                        MissionType.PLAN_SEED.name
                    ? R.string.plan_seed.tr()
                    : "",
          );
        });
  }

  Widget buildMission(
    BuildContext context, {
    String? steps,
    String? mission,
    String? description,
    required ValueChanged<bool> onChanged,
    bool? value,
    String? image,
    String? challenge,
    bool? hideChallenge,
    required VoidCallback selectedMission,
  }) {
    return Padding(
      padding: EdgeInsets.only(left: 20.h, right: 10.h),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 25.h,
            width: 25.h,
            decoration:
                BoxDecoration(shape: BoxShape.circle, color: R.color.blue),
            child: Text(
              steps ?? "",
              style: Theme.of(context).textTheme.bold700.copyWith(
                  fontSize: 12.sp, color: R.color.white, height: 20.h / 12.sp),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(width: 10.h),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  mission ?? "",
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 14.sp, height: 18.h / 14.sp),
                ),
                Text(
                  description ?? "",
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp),
                  maxLines: 10,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 10.h),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CustomSwitch(
                      value: value ?? false,
                      onChanged: onChanged,
                    ),
                    SizedBox(width: 5.h),
                    Expanded(
                      child: value == true
                          ? Text(
                              R.string.completed.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .medium500
                                  .copyWith(
                                      fontSize: 12.sp, height: 16.h / 12.sp),
                            )
                          : Text(
                              R.string.wait_for_completion.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .medium500
                                  .copyWith(
                                      fontSize: 12.sp, height: 16.h / 12.sp),
                            ),
                    ),
                    Visibility(
                      visible: hideChallenge ?? false,
                      child: GestureDetector(
                        onTap: selectedMission,
                        child: Container(
                          width: 110.w,
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.h, vertical: 5.h),
                          decoration: BoxDecoration(
                              color: R.color.blue,
                              borderRadius: BorderRadius.circular(20.h)),
                          child: Row(
                            children: [
                              Image.asset(
                                image ?? "",
                                height: 15.h,
                                width: 15.w,
                              ),
                              SizedBox(width: 5.h),
                              Text(
                                challenge ?? "",
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        fontSize: 14.sp,
                                        color: R.color.white,
                                        height: 18.h / 14.sp),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 5.h)
              ],
            ),
          )
        ],
      ),
    );
  }
}
