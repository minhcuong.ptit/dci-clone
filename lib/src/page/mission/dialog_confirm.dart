import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../res/R.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/button_widget.dart';

class DialogConfirm extends StatelessWidget {
  final VoidCallback? confirm;
  final VoidCallback? cancel;

  const DialogConfirm({Key? key, this.confirm, this.cancel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(
          horizontal: 24.h, vertical: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            R.string.confirm.tr(),
            style: Theme.of(context)
                .textTheme
                .bold700
                .copyWith(fontSize: 16.sp),
          ),
          SizedBox(height: 8.h),
          Text(
            R.string.save_change.tr(),
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 11.sp),
          ),
          SizedBox(height: 15.h),
          ButtonWidget(
            title: R.string.yes.tr(),
            onPressed: confirm,
            backgroundColor: R.color.orange,
            textSize: 14.sp,
            padding: EdgeInsets.symmetric(vertical: 16.h),
          ),
          SizedBox(height: 8.h),
          ButtonWidget(
            title: R.string.no.tr(),
            onPressed:cancel,
            backgroundColor: R.color.white,
            textColor: R.color.primaryColor,
            textSize: 14.sp,
            padding: EdgeInsets.symmetric(vertical: 16.h),
            borderColor: R.color.primaryColor,
          ),
        ],
      ),
    );
  }
}
