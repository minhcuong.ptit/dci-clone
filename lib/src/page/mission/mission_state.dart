import 'package:equatable/equatable.dart';

abstract class MissionState extends Equatable {
  @override
  List<Object> get props => [];
}

class MissionInitial extends MissionState {}

class MissionLoading extends MissionState {
  @override
  String toString() {
    return 'MissionLoading{}';
  }
}

class MissionFailure extends MissionState {
  final String error;

  MissionFailure(this.error);

  @override
  String toString() {
    return 'MissionFailure{error: $error}';
  }
}

class MissionSuccess extends MissionState {
  @override
  String toString() {
    return 'MissionSuccess{}';
  }
}

class SaveMissionSuccess extends MissionState {
  final bool results;
  SaveMissionSuccess(this.results);
  @override
  String toString() {
    return 'SaveMissionSuccess{}';
  }
}
