import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/mission_request.dart';
import 'package:imi/src/data/network/response/mission_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/mission/mission.dart';
import 'package:imi/src/page/mission/mission_state.dart';

import '../../utils/navigation_utils.dart';

class MissionCubit extends Cubit<MissionState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  MissionCubit(
      this.repository, this.graphqlRepository, this.challengeMissionHistoryId)
      : super(MissionInitial());

  List<FetchMissionOfDay> listMission = [];
  FetchMissionOfDay? fetchMissionOfDay;
  MissionData? missionData;
  List<MissionRequestMissions> listRequest = [];
  int? challengeMissionHistoryId;
  List<bool> missionStatus = [];
  String note = '';

  void saveMission(
      {context,
      int? challengeId,
      int? challengeHistoryId,
      int? challengeMissionHistoryId,
      String? note,
      bool? isCompletedChallenge}) async {
    emit(MissionLoading());
    ApiResult<dynamic> apiResult = await repository.getMission(
        MissionRequest(
          challengeMissionHistoryId: challengeMissionHistoryId,
          missions: listRequest,
          note: note,
        ),
        challengeId: challengeId,
        challengeHistoryId: challengeHistoryId);
    apiResult.when(success: (data) {
      //appPreferences.setData("NOTE_CHALLENGE", note);
      emit(SaveMissionSuccess(data ?? false));
      // NavigationUtils.pop(context, result: data??false);
    }, failure: (e) async {
      emit(MissionFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getMission({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? MissionLoading() : MissionInitial());
    final res = await graphqlRepository.getMission(
        challengeMissionHistoryId: challengeMissionHistoryId);
    res.when(success: (MissionData data) {
      listMission = data.missions ?? [];
      missionData = data;
      listRequest = [];
      note = data.note ?? '';
      for (int i = 0; i < listMission.length; i++) {
        // if (listMission[i].completed == true) {
        //   listRequest.add(MissionRequestMissions(
        //     missionId: listMission[i].mission?.id ?? 0,
        //     completed: listMission[i].completed ?? false,
        //   ));
        // }
        missionStatus.add(listMission[i].completed ?? false);
        listRequest.add(MissionRequestMissions(
          missionId: listMission[i].mission?.id ?? 0,
          completed: listMission[i].completed ?? false,
        ));
        fetchMissionOfDay = data.missions?[i];
      }
      emit(MissionSuccess());
    }, failure: (e) async {
      emit(MissionFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void updateMission({int? id, bool? value, int? index}) {
    emit(MissionLoading());
    listRequest[index ?? 0] = MissionRequestMissions(
      missionId: id ?? 0,
      completed: value ?? false,
    );

    // if (value == true) {
    //   listRequest.add(MissionRequestMissions(
    //     missionId: id ?? 0,
    //     completed: value ?? false,
    //   ));
    // } else {
    //   listRequest.removeWhere((element) => element.missionId == id);
    // }
    emit(MissionInitial());
  }

  //check user change status completed-incompleted, change note.
  bool checkChangeStatus(String noteField) {
    for (int i = 0; i < missionStatus.length; i++) {
      if (missionStatus[i] != listRequest[i].completed) {
        return false;
      }
    }
    if (note != noteField) {
      return false;
    }
    return true;
  }
}
