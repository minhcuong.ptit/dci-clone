import 'dart:math';
import 'dart:developer' as dev;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/deatil_cart_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/course_details/widget/set_count_course.dart';
import 'package:imi/src/page/detail_cart/detail_cart_cubit.dart';
import 'package:imi/src/page/detail_cart/detail_cart_state.dart';
import 'package:imi/src/page/detail_order/detail_order_page.dart';
import 'package:imi/src/page/home_event/home_event_page.dart';
import 'package:imi/src/page/list_course/list_course_page.dart';
import 'package:imi/src/page/list_discount/list_discount_page.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/confirm_popup_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/edit_oder_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:imi/src/widgets/upper_case_text_formatter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DetailCartPage extends StatefulWidget {
  const DetailCartPage({Key? key}) : super(key: key);

  @override
  State<DetailCartPage> createState() => _DetailCartPageState();
}

class _DetailCartPageState extends State<DetailCartPage> {
  late DetailCartCubit _cubit;
  TextEditingController _discountController = TextEditingController();
  TextEditingController _noteController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();
  List<TextEditingController>? _countController;
  final RefreshController _refreshController = RefreshController();
  FocusNode _focusNode = FocusNode();
  final FocusNode _discountFocus = FocusNode();
  bool checkNode = false;
  bool isEmptyCart = false;
  int currentIndex = 0;
  late MainCubit _mainCubit;
  String errorName = '';
  String errorPhone = '';
  String errorEmail = '';

  String? get pref => appPreferences.getString(Const.ZERO_ITEM);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _mainCubit = GetIt.I();
    _cubit = DetailCartCubit(repository, graphqlRepository);
    _cubit.getDetailCart();
    _cubit.getPointDci();
    _cubit.getProvince();
    _cubit.getPayment();
    _cubit.getPointActionOut();

    appPreferences.removeData(Const.ZERO_ITEM);
  }

  @override
  void dispose() {
    _discountController.dispose();
    _refreshController.dispose();
    _focusNode.dispose();
    _countController?.forEach((element) {
      element.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<DetailCartCubit, DetailCartState>(
        // buildWhen: (a, b) =>
        //     a != b &&
        //     (b is GetListOrderSuccess ||
        //         b is DeleteOrderSuccess ||
        //         b is GetItemEmpty ||
        //         b is SwitchPaymentSuccess ||
        //         b is SwitchMethodPaymentSuccess),
        listener: (BuildContext context, state) {
          if (state is! DetailCartLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          if (state is ApplyCouponSuccess) {
            _cubit.getDetailCart();
            _discountController.clear();
          }
          if (state is DeleteOrderSuccess) {
            _cubit.refreshPage();
          }
          if (state is DeleteCouponSuccess) {
            _cubit.refreshPage();
          }
          if (state is UpdateOrderItemSuccess) {
            NavigationUtils.pop(context);
            _cubit.refreshPage();
          }
          if (state is CreateOrderSuccess && state.data != null) {
            NavigationUtils.navigatePage(
                context,
                DetailOrderPage(
                  orderHistoryId: state.data?.id ?? 0,
                  paymentType: _cubit.listPayment[_cubit.switchIndex ?? 0]
                      .childs?[_cubit.switchPay ?? 0].type,
                )).then((value) => _cubit.refreshPage());
          }
          if (state is CodeDiscountInvalid) {
            _cubit.refreshPage();
          }
          if (state is UpdateQuantitySuccess) {
            _cubit.refreshPage();
            appPreferences.removeData(Const.ZERO_ITEM);
          }
          // if (state is DetailCartSuccess && _cubit.listItem.length == 0) {
          //   setState(() {
          //     isEmptyCart = !isEmptyCart;
          //   });
          // }
          if (state is DetailCartFailure) {
            showDialog(
                barrierColor: R.color.white.withOpacity(0),
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return Dialog(
                    backgroundColor: R.color.gray,
                    insetPadding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      width: 30.w,
                      height: 80.h,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            CupertinoIcons.check_mark_circled_solid,
                            size: 21.h,
                            color: R.color.white,
                          ),
                          SizedBox(height: 10.h),
                          Text(
                            R.string.product.tr()[0].toUpperCase() +
                                R.string.product.tr().substring(1) +
                                ' ' +
                                state.error +
                                ' ' +
                                R.string.does_not_exist.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 11.sp, color: R.color.white),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                  );
                });
            Future.delayed(const Duration(seconds: 2), () {
              NavigationUtils.pop(context);
            });
          }

          if (state is ErrorMethodPay) {
            // Utils.showToast(context, _cubit.errorPay);
          }
        },
        builder: (context, state) {
          return WillPopScope(
            onWillPop: () {
              return Future(() => true).whenComplete(() {
                if (_cubit.listItem.isEmpty) {
                  appPreferences.removeData(Const.COUNT_CART);
                }
                NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
              });
            },
            child: Scaffold(
              backgroundColor: R.color.grey100,
              appBar: appBar(
                context,
                R.string.cart.tr(),
                centerTitle: true,
                leadingWidget: InkWell(
                  highlightColor: R.color.white,
                  onTap: () {
                    if (_cubit.listItem.isEmpty) {
                      appPreferences.removeData(Const.COUNT_CART);
                    }
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  },
                  child: const Icon(CupertinoIcons.back),
                ),
              ),
              body: buildListItem(state, context),
            ),
          );
        },
      ),
    );
  }

  Widget buildNoItem(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(
          left: 23.h,
          right: 23.h,
          top: 60.h,
        ),
        child: Column(
          children: [
            Image.asset(
              R.drawable.ic_empty_cart,
              height: 120.h,
              width: 120.w,
            ),
            SizedBox(height: 20.h),
            Text(
              R.string.empty_cart.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 14.sp, color: R.color.black),
              textAlign: TextAlign.center,
            ),
            Text(
              R.string.add_dci_cart.tr(),
              style: Theme.of(context)
                  .textTheme
                  .regular400
                  .copyWith(fontSize: 13.sp, color: R.color.gray),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 60.h),
            ButtonWidget(
                title: R.string.buy_more_events.tr(),
                uppercaseTitle: false,
                backgroundColor: R.color.secondaryButtonColor,
                padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 10.w),
                textSize: 16.sp,
                onPressed: () {
                  NavigationUtils.pop(context);
                  NavigationUtils.rootNavigatePage(context, HomeEventPage());
                }),
            SizedBox(height: 15.h),
            ButtonWidget(
                title: R.string.buy_more_courses.tr(),
                uppercaseTitle: false,
                backgroundColor: R.color.secondaryButtonColor,
                padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 10.w),
                textSize: 16.sp,
                onPressed: () {
                  NavigationUtils.pop(context);
                  NavigationUtils.rootNavigatePage(context, ListCoursePage());
                }),
            SizedBox(height: 15.h),
            ButtonWidget(
                title: R.string.home_page.tr(),
                uppercaseTitle: false,
                backgroundColor: R.color.white,
                borderColor: R.color.secondaryButtonColor,
                padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 10.w),
                textSize: 16.sp,
                textColor: R.color.secondaryButtonColor,
                onPressed: () {
                  _mainCubit.eventRefreshCount();
                  NavigationUtils.popToFirst(context);
                  _mainCubit.selectTab(0);
                }),
          ],
        ),
      ),
    );
  }

  bool get textEmpty => _discountController.text.isEmpty;

  Widget buildListItem(DetailCartState state, BuildContext context) {
    return StackLoadingView(
      visibleLoading: state is DetailCartLoading,
      child: SmartRefresher(
        controller: _refreshController,
        onRefresh: () => _cubit.getPointDci(),
        onLoading: () => _cubit.getPointActionOut(),
        child: (_cubit.listItem.isEmpty)
            ? Visibility(
                visible: state is GetItemEmpty,
                child: buildNoItem(context),
              )
            : ListView(
                children: [
                  Visibility(
                    visible: state is! DetailCartLoading,
                    child: ListView.builder(
                      itemCount: _cubit.listItem.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, int index) {
                        Items data = _cubit.listItem[index];
                        _cubit.changePosition = index;
                        return buildProduct(data, context, index, state);
                      },
                    ),
                  ),
                  Visibility(
                    visible: state is! DetailCartLoading,
                    child: Container(
                      color: R.color.white,
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.h, vertical: 10.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(
                                      R.drawable.ic_discount_code,
                                      height: 20)),
                              SizedBox(width: 5),
                              Text(
                                R.string.discount_code.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(fontSize: 13, height: 16 / 12),
                              ),
                              Spacer(),
                              GestureDetector(
                                onTap: () {
                                  NavigationUtils.navigatePage(
                                          context,
                                          ListDiscountPage(_cubit
                                              .cartData?.fetchMyOrder?.orderId))
                                      .then((value) {
                                    _cubit.refreshPage();
                                  });
                                },
                                child: Text(R.string.read_more.tr(),
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: R.color.primaryColor,
                                    )),
                              )
                            ],
                          ),
                          SizedBox(height: 15.h),
                          Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: 40.h,
                                  child: TextField(
                                    controller: _discountController,
                                    style: Theme.of(context)
                                        .textTheme
                                        .medium500
                                        .copyWith(fontSize: 12.sp),
                                    inputFormatters: [
                                      UpperCaseTextFormatter(),
                                    ],
                                    onChanged: _cubit.validateInputChange,
                                    focusNode: _discountFocus,
                                    decoration: InputDecoration(
                                      //errorText: _cubit.errorText,
                                      hintText:
                                          R.string.enter_discount_code.tr(),
                                      hintStyle: Theme.of(context)
                                          .textTheme
                                          .medium500
                                          .copyWith(
                                              fontSize: 14.sp,
                                              color: R.color.grey,
                                              height: 20.h / 14.sp),
                                      border: InputBorder.none,
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 8.h),
                                      suffix: InkWell(
                                        onTap: () {
                                          _discountController.clear();
                                          setState(() {
                                            textEmpty;
                                          });
                                        },
                                        child: Icon(CupertinoIcons.clear,
                                            size: 16.h),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          borderSide:
                                              BorderSide(color: R.color.grey)),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          borderSide:
                                              BorderSide(color: R.color.grey)),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 10.w),
                              ButtonWidget(
                                title: R.string.apply_code.tr(),
                                onPressed: textEmpty
                                    ? null
                                    : () {
                                        _cubit.applyCoupon(
                                            discountCode:
                                                _discountController.text,
                                            orderId: _cubit.cartData
                                                    ?.fetchMyOrder?.orderId ??
                                                0);
                                      },
                                textSize: 13.sp,
                                uppercaseTitle: false,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10.h, horizontal: 20.h),
                                backgroundColor: R.color.primaryColor,
                              )
                            ],
                          ),
                          _discountController.text.trim().isEmpty
                              ? const SizedBox.shrink()
                              : Visibility(
                                  visible: state is ApplyCouponErrorSuccess,
                                  child: Text(
                                    _cubit.errorText ?? "",
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 11.sp,
                                            color: R.color.red),
                                  ),
                                ),
                          SizedBox(height: 5.h),
                        ],
                      ),
                    ),
                  ),
                  _cubit.listCoupons.isEmpty
                      ? const SizedBox.shrink()
                      : buildListCoupon(context),
                  SizedBox(height: 5.h),
                  Visibility(
                    visible: state is! DetailCartLoading,
                    child: Container(
                      color: R.color.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.h, vertical: 5.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            R.string.detail_payment.tr(),
                            style: Theme.of(context).textTheme.bodyMediumText,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: buildTextWidget(context,
                                    title: R.string.provisional.tr(),
                                    fontSize: 13.sp,
                                    color: R.color.black),
                              ),
                              buildTextWidget(context,
                                  title:
                                      "${Utils.formatMoney(_cubit.totalPrice)} VND",
                                  fontSize: 14.sp,
                                  color: R.color.primaryColor)
                            ],
                          ),
                          Visibility(
                            visible: _cubit.getTotalSingleDiscount() != 0,
                            child: Row(
                              children: [
                                Expanded(
                                  child: buildTextWidget(context,
                                      title: R.string.total_single_discount.tr(),
                                      fontSize: 13.sp,
                                      color: R.color.black),
                                ),
                                buildTextWidget(context,
                                    title:
                                        "- ${Utils.formatMoney(_cubit.getTotalSingleDiscount())} VND",
                                    fontSize: 14.sp,
                                    color: R.color.primaryColor)
                              ],
                            ),
                          ),
                          Visibility(
                            visible: _cubit.cartData?.fetchMyOrder
                                    ?.totalComboCouponDiscount !=
                                0,
                            child: Row(
                              children: [
                                Expanded(
                                  child: buildTextWidget(context,
                                      title: R.string.total_combo_discount.tr(),
                                      fontSize: 13.sp,
                                      color: R.color.black),
                                ),
                                buildTextWidget(context,
                                    title:
                                        "- ${Utils.formatMoney(_cubit.cartData?.fetchMyOrder?.totalComboCouponDiscount)} VND",
                                    fontSize: 14.sp,
                                    color: R.color.primaryColor)
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    '${R.string.use.tr()} ${R.string.point_dci.tr()}',
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 13.sp,
                                            color: R.color.black,
                                            height: 18 / 12),
                                  ),
                                  SizedBox(width: 4.w),
                                  Tooltip(
                                      message: R.string.tooltip_dci_order.tr(),
                                      textStyle: Theme.of(context)
                                          .textTheme
                                          .regular400
                                          .copyWith(
                                            fontSize: 11.sp,
                                            color: R.color.primaryColor,
                                          ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 12.w, vertical: 6.h),
                                      margin: EdgeInsetsDirectional.fromSTEB(
                                          100.w, 0, 20.w, 0),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(15.h),
                                          color: R.color.white),
                                      preferBelow: false,
                                      showDuration: const Duration(hours: 1),
                                      triggerMode: TooltipTriggerMode.tap,
                                      child: Icon(CupertinoIcons.info_circle,
                                          size: 16.h,
                                          color: R.color.primaryColor)),
                                  SizedBox(width: 4.w),
                                  SizedBox(
                                      width: 40,
                                      height: 30,
                                      child: FittedBox(
                                        fit: BoxFit.cover,
                                        child: Switch(
                                            value: _cubit.switchPoint,
                                            onChanged: (value) {
                                              setState(() {
                                                // _cubit.togglePayment(_cubit.cartData
                                                //     ?.fetchMyOrder?.orderId ??
                                                //     0);
                                                _cubit.switchPoint =
                                                    !_cubit.switchPoint;
                                              });
                                            }),
                                      ))
                                ],
                              ),
                              buildTextWidget(context,
                                  title:
                                      "- ${_cubit.switchPoint == true ? Utils.formatMoney(min(_cubit.maxPoint * _cubit.exchangeRate, _cubit.totalPrice)) : 0} VND",
                                  fontSize: 14.sp,
                                  color: R.color.primaryColor)
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: buildTextWidget(context,
                                    title: R.string.total_payment.tr(),
                                    fontSize: 16.sp,
                                    color: R.color.black,
                                    fontWeight: FontWeight.w700),
                              ),
                              buildTextWidget(context,
                                  title:
                                      "${Utils.formatMoney(max(_cubit.totalPrice - _cubit.totalDiscount - _cubit.usePoint(), 0))} VND",
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w700,
                                  color: R.color.red)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 5.h),
                  buildPayment(state, context),
                  SizedBox(height: 20.h),
                  Visibility(
                    visible: state is! DetailCartLoading &&
                        _cubit.hideButtonPayment == true,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () {
                              setState(() {
                                _cubit.checkProvision = !_cubit.checkProvision;
                              });
                            },
                            child: _cubit.checkProvision
                                ? const Icon(Icons.check_box_outline_blank)
                                : Icon(
                                    Icons.check_box_rounded,
                                    color: R.color.primaryColor,
                                  ),
                          ),
                          SizedBox(width: 5.h),
                          Expanded(
                            child: RichText(
                              textAlign: TextAlign.start,
                              text: TextSpan(
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 14.sp),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            "${R.string.registration_agree.tr()} ",
                                        style: TextStyle(
                                            color: R.color.textTitleGray)),
                                    TextSpan(
                                        text: R.string.registration_term.tr(),
                                        style: TextStyle(
                                            color: R.color.primaryColor),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            Utils.launchURL(
                                                Utils.getTypeUrlLauncher(
                                                    Const.TERM_URL,
                                                    Const.LAUNCH_TYPE_WEB));
                                          }),
                                    TextSpan(
                                        text:
                                            " ${R.string.registration_and.tr()} ",
                                        style: TextStyle(
                                            color: R.color.textTitleGray)),
                                    TextSpan(
                                        text:
                                            R.string.registration_privacy.tr(),
                                        style: TextStyle(
                                            color: R.color.primaryColor),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            Utils.launchURL(
                                                Utils.getTypeUrlLauncher(
                                                    Const.PRIVACY_URL,
                                                    Const.LAUNCH_TYPE_WEB));
                                          }),
                                    TextSpan(
                                        text: " ${R.string.from_dci.tr()}",
                                        style: TextStyle(
                                            color: R.color.textTitleGray)),
                                  ]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15.h),
                  Visibility(
                    visible: state is! DetailCartLoading &&
                        _cubit.hideButtonPayment == true,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.sp),
                      child: ButtonWidget(
                        title: R.string.create_order_and_pay.tr().toUpperCase(),
                        onPressed: _cubit.checkProvision ||
                                (_cubit.switchIndex == null ||
                                    _cubit.switchPay == null) ||
                                state is CreateOrderLoading
                            ? null
                            : () {
                                _cubit.checkPaymentValid();
                                if (pref == "0") {
                                  Utils.showToast(
                                      context, R.string.invalid_courses.tr());
                                } else if (pref != "0") {
                                  _cubit.indexNotValid != _cubit.switchPay
                                      ? _cubit.createOder(
                                          orderId: _cubit.cartData?.fetchMyOrder
                                                  ?.orderId ??
                                              0,
                                          usePoint: _cubit.switchPoint,
                                          paymentType: PaymentType.values
                                              .firstWhereOrNull((element) =>
                                                  element.name ==
                                                  _cubit
                                                      .listPayment[
                                                          _cubit.switchIndex ??
                                                              0]
                                                      .childs?[
                                                          _cubit.switchPay ?? 0]
                                                      .type))
                                      : null;
                                }
                              },
                        showCircleIndicator: state is CreateOrderLoading,
                        backgroundColor: R.color.primaryColor,
                        textSize: 12.sp,
                        padding: EdgeInsets.symmetric(vertical: 9.h),
                      ),
                    ),
                  ),
                  SizedBox(height: 50.h)
                ],
              ),
      ),
    );
  }

  Widget buildProduct(
      Items data, BuildContext context, int index, DetailCartState state) {
    return BlocBuilder<DetailCartCubit, DetailCartState>(
      buildWhen: (previous, next) =>
          previous != next && next is GetListOrderSuccess,
      builder: (BuildContext context, state) {
        return Container(
          color: R.color.white,
          padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 12.h),
          margin: EdgeInsets.only(bottom: 5.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.productType == ProductType.COURSE.name
                    ? data.course?.name ?? ''
                    : data.event?.name ?? '',
                style: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(fontSize: 14.sp, color: R.color.dark_blue),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 5.h),
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12.h),
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: data.productType == ProductType.COURSE.name
                          ? data.course?.avatarUrl ?? ""
                          : data.event?.avatarUrl ?? '',
                      height: 86.h,
                      width: 117.w,
                    ),
                  ),
                  SizedBox(width: 5.h),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        buildTextWidget(context,
                            title: R.string.price.tr(),
                            fontSize: 11.sp,
                            color: R.color.grey),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 5.h),
                          child: buildTextWidget(context,
                              title: "${R.string.amount.tr()}:",
                              fontSize: 11.sp,
                              color: R.color.grey),
                        ),
                        buildTextWidget(context,
                            title: R.string.discount_total.tr(),
                            color: R.color.grey,
                            fontSize: 11.sp),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      buildTextWidget(context,
                          title:
                              "${Utils.formatMoney(data.productType == ProductType.COURSE.name ? data.course?.preferentialFee ?? 0 : data.event?.preferentialFee ?? 0)} VND",
                          color: R.color.red,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w500),
                      SetCountCourseWidget(
                        controller: _countController?[index],
                        decreaseCount: () {
                          if (data.quantity == 1) {
                            return;
                          } else {
                            _cubit.updateOrderQuantity(index,
                                orderId: _cubit.cartData?.fetchMyOrder?.orderId,
                                orderDetailId: _cubit.listItem[index].id,
                                quantity: data.quantity ?? 0,
                                count: false);
                          }
                        },
                        increaseCount: () {
                          _cubit.updateOrderQuantity(index,
                              orderId: _cubit.cartData?.fetchMyOrder?.orderId,
                              orderDetailId: _cubit.listItem[index].id,
                              quantity: data.quantity ?? 0,
                              count: true);
                        },
                        title: data.quantity.toString(),
                        checkColor: data.quantity! > 1,
                        onChanged: (text) {
                          if (int.parse(text) < 1) {
                            Utils.showToast(
                                context, R.string.invalid_courses.tr());
                            appPreferences.setData(Const.ZERO_ITEM, text);
                          } else {
                            _cubit.onChangedQuantity(index,
                                quantity: int.parse(text),
                                orderDetailId: _cubit.listItem[index].id,
                                orderId:
                                    _cubit.cartData?.fetchMyOrder?.orderId);
                          }
                        },
                      ),
                      _cubit.listCoupons.isEmpty
                          ? Text("0 VND",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp,
                                      color: R.color.red,
                                      fontWeight: FontWeight.w500))
                          : buildTextWidget(context,
                              title:
                                  "-${Utils.formatMoney(max(_cubit.discount(index), 0))} VND",
                              color: R.color.red,
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500),
                    ],
                  )
                ],
              ),
              const Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      buildEditItems(context, data, index);
                    },
                    child: Image.asset(
                      R.drawable.ic_edit,
                      height: 24.h,
                      width: 24.w,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showBarModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return ConfirmPopup(
                              description: R.string.delete_item.tr(),
                              onConfirmYes: () {
                                NavigationUtils.pop(context);
                                _cubit.deleteOrder(
                                    orderId:
                                        _cubit.cartData?.fetchMyOrder?.orderId,
                                    orderDetailId: _cubit.listItem[index].id);
                              },
                              onConfirmNo: () {
                                NavigationUtils.pop(context);
                              },
                            );
                          });
                    },
                    child: Image.asset(
                      R.drawable.ic_delete_post,
                      height: 24.h,
                      width: 24.w,
                      color: R.color.red,
                    ),
                  ),
                  SizedBox(width: 73.w),
                  Expanded(
                    child: buildTextWidget(context,
                        title: "${R.string.provisional.tr()}:",
                        fontSize: 12.sp,
                        color: R.color.black,
                        fontWeight: FontWeight.w500),
                  ),
                  buildTextWidget(context,
                      title:
                          "${Utils.formatMoney(max((data.productType == ProductType.COURSE.name ? data.course?.preferentialFee ?? 0 : data.event?.preferentialFee ?? 0) * data.quantity! - _cubit.discount(index), 0))} VND",
                      color: R.color.red,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w500),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget buildPayment(DetailCartState state, BuildContext context) {
    return Visibility(
      visible: state is! DetailCartLoading && _cubit.hideButtonPayment == true,
      child: Container(
        color: R.color.white,
        padding: EdgeInsets.symmetric(horizontal: 16.sp),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              R.string.payment_methods.tr(),
              style: Theme.of(context)
                  .textTheme
                  .medium500
                  .copyWith(fontSize: 13.sp),
            ),
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: _cubit.listPayment.length,
                itemBuilder: (context, int index) {
                  return Visibility(
                    visible: _cubit.listPayment[index].status ==
                        CommonStatus.ACTIVE.name,
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ValueListenableBuilder(
                              builder: (context, List<bool> value, _) => Radio(
                                  value: _cubit.listSwitch.value[index],
                                  groupValue: true,
                                  onChanged: (value) {
                                    _cubit.switchPayment(index);
                                  }),
                              valueListenable: _cubit.listSwitch,
                            ),
                            if (_cubit.listPayment[index].type ==
                                PaymentType.ALEPAY_GATEWAY.name) ...[
                              Image.asset(
                                R.drawable.ic_alepay,
                                height: 40,
                              ),
                            ],
                            SizedBox(width: 4.w),
                            InkWell(
                              onTap: () {
                                _cubit.switchPayment(index);
                              },
                              child: Text(
                                PaymentType.values
                                    .firstWhereOrNull((element) =>
                                        element.name ==
                                        _cubit.listPayment[index].type)!
                                    .title(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp,
                                        height: 20.h / 12.sp,
                                        color: R.color.textBlack),
                              ),
                            )
                          ],
                        ),
                        Visibility(
                          visible: _cubit.listSwitch.value[index] == true &&
                              _cubit.listPayment[index].childs?.length != 0,
                          child: Container(
                            color: R.color.lightestGray,
                            margin: EdgeInsets.only(left: 20, bottom: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 15),
                                  child: Text(
                                    R.string.choose_a_payment_method.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: R.color.grey),
                                  ),
                                ),
                                ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: _cubit
                                        .listPayment[index].childs?.length,
                                    itemBuilder: (context, int index1) {
                                      return Column(
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              ValueListenableBuilder(
                                                builder: (context,
                                                        List<bool> value, _) =>
                                                    Radio(
                                                        value: _cubit
                                                            .listSwitchChild
                                                            .value[index1],
                                                        groupValue: true,
                                                        onChanged: (value) {
                                                          _cubit
                                                              .switchPaymentChild(
                                                                  index1);
                                                        }),
                                                valueListenable:
                                                    _cubit.listSwitchChild,
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  _cubit.switchPaymentChild(
                                                      index1);
                                                },
                                                child: Text(
                                                  PaymentType.values
                                                      .firstWhereOrNull(
                                                          (element) =>
                                                              element.name ==
                                                              _cubit
                                                                  .listPayment[
                                                                      index]
                                                                  .childs?[
                                                                      index1]
                                                                  .type)!
                                                      .title(),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .regular400
                                                      .copyWith(
                                                          fontSize: 12.sp,
                                                          height: 20.h / 12.sp,
                                                          color: R
                                                              .color.textBlack),
                                                ),
                                              )
                                            ],
                                          ),
                                          Visibility(
                                            visible:
                                                _cubit.switchPay == index1 &&
                                                    _cubit.notContainPayment,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20),
                                              child: Text(
                                                R.string
                                                    .payment_method_not_supported
                                                    .tr(),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .regular400
                                                    .copyWith(
                                                        color:
                                                            R.color.brightRed,
                                                        fontSize: 11.sp),
                                              ),
                                            ),
                                          )
                                        ],
                                      );
                                    }),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  Widget buildListCoupon(BuildContext context) {
    return Visibility(
      visible: _cubit.state is! DetailCartLoading,
      child: Container(
        margin: EdgeInsets.only(top: 5.h),
        color: R.color.white,
        padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 5.h),
        child: Column(
          children: List.generate(
              _cubit.listCoupons.length,
              (index) => Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              _cubit.listCoupons[index].coupon?.code ?? "",
                              style: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 13.sp,
                                      color: R.color.dark_blue),
                            ),
                            Visibility(
                              visible:
                                  _cubit.listCoupons[index].isValid == false,
                              child: Text(
                                _cubit.codeInvalid ??
                                    R.string.coupon_is_invalid_error.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 11.sp, color: R.color.red),
                              ),
                            )
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          _cubit.deleteCoupon(
                              orderId:
                                  _cubit.cartData?.fetchMyOrder?.orderId ?? 0,
                              couponId:
                                  _cubit.listCoupons[index].coupon?.id ?? 0);
                        },
                        child: Text(
                          R.string.delete.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 13.sp, color: R.color.grey),
                        ),
                      )
                    ],
                  )),
        ),
      ),
    );
  }

  Future buildEditItems(BuildContext context, Items data, int index) {
    appPreferences.setData('ProvinceId', _cubit.cityId[index]);
    appPreferences.setData('Province', _cubit.cityName[index] ?? 'Hà Nội');
    return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(24), topLeft: Radius.circular(24)),
        ),
        isScrollControlled: true,
        context: context,
        isDismissible: false,
        builder: (context) {
          _cubit.countItem?[index] != null;
          return StatefulBuilder(builder:
              (BuildContext context, void Function(void Function()) setState) {
            return EditOrderWidget(
              lengthText: _cubit.lengthText[index],
              imageUrl: data.productType == ProductType.COURSE.name
                  ? data.course?.avatarUrl ?? ""
                  : data.event?.avatarUrl ?? "",
              nameCourse: data.productType == ProductType.COURSE.name
                  ? data.course?.name ?? ""
                  : data.event?.name ?? "",
              quantityEditingController: _cubit.quantityController![index],
              currentFee: data.productType == ProductType.COURSE.name
                  ? data.course?.currentFee ?? 0
                  : data.event?.currentFee ?? 0,
              preferentialFee: data.productType == ProductType.COURSE.name
                  ? data.course?.preferentialFee ?? 0
                  : data.event?.preferentialFee ?? 0,
              quantity: "${data.quantity}",
              checkColor: _cubit.countItem![index] > 1,
              onCurrentDecrease: () {
                setState(() {
                  _cubit.updateQuantity(
                      index: index,
                      quantity: _cubit.countItem ?? [],
                      disChange: false);
                });
              },
              textEditingController: _cubit.noteItemController?[index],
              nameEditingController: _cubit.nameController?[index],
              phoneEditingController: _cubit.phoneController?[index],
              emailEditingController: _cubit.emailController?[index],
              errorName: errorName,
              errorPhone: errorPhone,
              errorEmail: errorEmail,
              province: _cubit.listProvince,
              provinceId: _cubit.cityId[index],
              currentProvince: appPreferences.getString('Province'),
              //_cubit.textControllers[_cubit.listItem[index].note],
              focusNode: _focusNode,
              isEvent:
                  data.productType == ProductType.COURSE.name ? false : true,
              onCurrentIncrease: () {
                setState(() {
                  _cubit.updateQuantity(
                      index: index,
                      quantity: _cubit.countItem ?? [],
                      disChange: true);
                });
              },
              save: () {
                _cubit.phoneController?[index].text != '' &&
                        _cubit.emailController?[index].text != '' &&
                        _cubit.nameController?[index].text != ''
                    ? _cubit.updateOrderItem(
                        index, _cubit.quantityController![index].text,
                        orderId: _cubit.cartData?.fetchMyOrder?.orderId ?? 0,
                        orderDetailId: _cubit.listItem[index].id,
                        itemId: _cubit.listItem[index].id,
                        note: _cubit.noteItemController?[index].text,
                        phone: _cubit.phoneController?[index].text,
                        email: _cubit.emailController?[index].text,
                        userName: _cubit.nameController?[index].text,
                        provinceId: appPreferences.getInt('ProvinceId'))
                    : null;
              },
              onChanged: (value) {
                setState(
                  () {
                    _cubit.changeText(
                        _cubit.noteItemController![index].text.length, index);
                  },
                );
              },
              onPopUp: () {
                appPreferences.removeData('ProvinceId');
                appPreferences.removeData('Province');
                NavigationUtils.pop(context);
                _cubit.clearQuantity(index);
              },
            );
          });
        }).then((value) => _cubit.clearQuantity(index));
  }

  Widget buildTextWidget(BuildContext context,
      {String? title, double? fontSize, Color? color, FontWeight? fontWeight}) {
    return Text(
      title ?? "",
      style: Theme.of(context)
          .textTheme
          .regular400
          .copyWith(fontSize: fontSize, color: color, fontWeight: fontWeight),
    );
  }
}
