import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/response/create_order_response.dart';

abstract class DetailCartState extends Equatable {
  const DetailCartState();

  @override
  List<Object> get props => [];
}

class DetailCartInitial extends DetailCartState {}

class DetailCartLoading extends DetailCartState {
  @override
  String toString() {
    return 'DetailCartLoading{}';
  }
}

class CreateOrderLoading extends DetailCartState {
  @override
  String toString() {
    return 'DetailCartLoading{}';
  }
}

class DetailCartFailure extends DetailCartState {
  final String error;

  DetailCartFailure(this.error);

  @override
  String toString() {
    return 'DetailCartFailure{error: $error}';
  }
}

class GetProvinceDetailCartSuccess extends DetailCartState {
  @override
  String toString() {
    return 'GetProvinceDetailCartSuccess{}';
  }
}

class DetailCartSuccess extends DetailCartState {
  @override
  String toString() {
    return 'DetailCartSuccess{}';
  }
}

class DeleteOrderSuccess extends DetailCartState {
  @override
  String toString() {
    return 'DeleteOrderSuccess{}';
  }
}

class ApplyCouponSuccess extends DetailCartState {
  @override
  String toString() {
    return 'ApplyCouponSuccess{}';
  }
}

class ApplyCouponErrorSuccess extends DetailCartState {
  @override
  String toString() {
    return 'ApplyCouponErrorSuccess{}';
  }
}

class UpdateQuantitySuccess extends DetailCartState {
  @override
  String toString() {
    return 'UpdateQuantitySuccess{}';
  }
}

class DeleteCouponSuccess extends DetailCartState {
  @override
  String toString() {
    return 'DeleteCouponSuccess{}';
  }
}

class CreateOrderSuccess extends DetailCartState {
  final CreateOrderResponse? data;

  CreateOrderSuccess({this.data});

  @override
  String toString() {
    return 'CreateOrderSuccess';
  }
}

class UpdateOrderItemSuccess extends DetailCartState {
  @override
  String toString() {
    return 'UpdateOrderItemSuccess{}';
  }
}

class ChangePaymentSuccess extends DetailCartState {
  @override
  String toString() {
    return 'ChangePaymentSuccess{}';
  }
}

class CodeDiscountInvalid extends DetailCartState {
  @override
  String toString() {
    return 'CodeDiscountInvalid{}';
  }
}

class OptionPaymentSuccess extends DetailCartState {
  @override
  String toString() {
    return 'OptionPaymentSuccess{}';
  }
}

class CheckPaymentConfig extends DetailCartState {
  @override
  String toString() {
    return 'CheckPaymentConfig{}';
  }
}

class GetProvinceCartSuccess extends DetailCartState {
  @override
  String toString() {
    return 'GetProvinceSuccess {}';
  }
}

class SetProvinceCartSuccess extends DetailCartState {
  @override
  String toString() {
    return 'SetProvinceSuccess {}';
  }
}

class EventDetailCartTextChangeInitial extends DetailCartState {}

class EventDetailCartNumberChangeInitial extends DetailCartState {}

class TogglePaymentSuccess extends DetailCartState {
  @override
  String toString() {
    return 'TogglePaymentSuccess{}';
  }
}

class GetItemEmpty extends DetailCartState {
  @override
  String toString() {
    return 'GetItemEmpty {}';
  }
}

class GetListOrderSuccess extends DetailCartState {
  @override
  String toString() {
    return 'GetListOrderSuccess {}';
  }
}

class GetPointSuccess extends DetailCartState {
  @override
  String toString() {
    return 'GetPointSuccess {}';
  }
}

class ErrorMethodPay extends DetailCartState {
  @override
  String toString() {
    return 'ErrorMethodPay {}';
  }
}

class SwitchPaymentSuccess extends DetailCartState {
  @override
  String toString() {
    return 'SwitchPaymentSuccess {}';
  }
}

class SwitchMethodPaymentSuccess extends DetailCartState {
  @override
  String toString() {
    return 'SwitchMethodPaymentSuccess {}';
  }
}
