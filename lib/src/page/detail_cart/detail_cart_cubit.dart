import 'dart:async';
import 'dart:developer' as dev;
import 'dart:math';
import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/apply_coupon_request.dart';
import 'package:imi/src/data/network/request/create_order_request.dart';
import 'package:imi/src/data/network/request/update_order_items_request.dart';
import 'package:imi/src/data/network/response/deatil_cart_response.dart';
import 'package:imi/src/data/network/response/payment_response.dart';
import 'package:imi/src/data/network/response/point_action_out.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/detail_cart/detail_cart_state.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:pubnub/core.dart';

import '../../data/network/response/city_response.dart';
import '../../data/network/response/community_data.dart';
import '../../data/network/response/detail_order_response.dart';
import '../../data/network/response/exchangeRate.dart';
import '../../data/network/service/app_client.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/app_config.dart';
import '../../utils/const.dart';

enum DiscountType { PERCENTAGE, PRICE }

class DetailCartCubit extends Cubit<DetailCartState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  DetailCartCubit(this.repository, this.graphqlRepository)
      : super(DetailCartInitial());
  DetailCartData? cartData;
  String? nextToken;
  String? errorText;
  String? note;
  String? codeInvalid;
  int couponId = 0;
  List<int> lengthText = [];
  int? quantityEdit;
  int? quantityItem;
  Timer? _debounce;
  List<String>? listNote;
  List<TextEditingController>? noteItemController = [];
  List<TextEditingController>? nameController = [];
  List<TextEditingController>? phoneController = [];
  List<TextEditingController>? emailController = [];
  List<TextEditingController>? quantityController = [];
  List<TextEditingController>? provinceController = [];
  List<FetchPaymentTypes> listPayment = [];
  List<Items> listItem = [];
  List<Coupons> listCoupons = [];
  List<int>? countItem = [];
  bool? hideButtonPayment;
  bool? hidePaymentOnline;
  bool? hidePaymentDCI;
  bool checkProvision = true;
  ValueNotifier<List<bool>> isSwitchPayment =
      ValueNotifier([false, false, false]);
  ValueNotifier<List<bool>> isSwitchPaymentChild =
      ValueNotifier([false, false, false]);
  ValueNotifier<List<bool>> listSwitch = ValueNotifier([false, false, false]);
  ValueNotifier<List<bool>> listSwitchChild =
      ValueNotifier([false, false, false]);
  int groupValue = 0;
  int? switchIndex;
  int? switchPay;
  bool switchPoint = false;
  int exchangeRate = 1;
  String errorName = '';
  String errorPhone = '';
  String errorEmail = '';
  int pointReceived = 0;
  String typeReceivePoint = 'EVENT';
  bool isContainEvent = false;
  bool isContainCourse = false;
  List<String> cityName = [];
  List<int> cityId = [];
  List<CityData> listProvince = [];
  num totalPrice = 0;
  num totalPayment = 0;
  num totalDiscount = 0;
  int maxPoint = 0;
  bool isActiveUsePoint = true;
  int maxPointConfig = 0;
  List<bool> configAlePay = [];
  int indexNotValid = -1;
  bool notContainPayment = false;

  String? get codeCoupons => appPreferences.getString(Const.COUPONS);
  String? errorPay;
  int? changePosition;

  void getDetailCart({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? DetailCartLoading() : DetailCartInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listItem.clear();
    }
    ApiResult<DetailCartData> result = await graphqlRepository.getDetailCart();
    result.when(success: (DetailCartData data) {
      if (data.fetchMyOrder?.items?.length != null) {
        listItem = data.fetchMyOrder?.items ?? [];
        listCoupons = data.fetchMyOrder?.coupons ?? [];
        cartData = data;
        data.fetchMyOrder?.coupons?.forEach((element) {
          couponId = element.coupon?.id ?? 0;
        });
        data.fetchMyOrder?.items?.forEach((element) {
          note = element.note;
          quantityItem = element.quantity ?? 0;
        });
        for (int i = 0; i < listItem.length; i++) {
          lengthText.add(listItem[i].note?.length ?? 0);
          noteItemController
              ?.add(TextEditingController(text: listItem[i].note));
          nameController?.add(TextEditingController(
              text: listItem[i].userInformation?.userName));
          phoneController?.add(
              TextEditingController(text: listItem[i].userInformation?.phone));
          emailController?.add(
              TextEditingController(text: listItem[i].userInformation?.email));
          countItem?.add(listItem[i].quantity ?? 0);
          quantityController?.add(
              TextEditingController(text: listItem[i].quantity.toString()));
        }
        exchangeRate = data.fetchMyOrder?.pointExchangeRate ?? 1;
        totalPrice = getTotalPrice();
        totalDiscount = getTotalDiscount();
        totalPayment = getTotalPayment();
        maxPoint = getMaxUsePoint();

        emit(GetListOrderSuccess());
      } else {
        emit(GetItemEmpty());
      }
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getProvince() async {
    // emit(DetailCartLoading());
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListProvince();
    apiResult.when(success: (List<CityData> data) async {
      listProvince = data;
      cityName.clear();
      cityId.clear();
      for (int i = 0; i < listItem.length; i++) {
        listProvince.forEach((element) {
          if (listItem[i].userInformation?.provinceId == element.cityId) {
            cityName.add(element.cityName ?? '');
            cityId.add(element.cityId ?? 0);
          }
        });
      }
      // emit(DetailCartLoading());
    }, failure: (NetworkExceptions error) async {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void deleteOrder({int? orderId, int? orderDetailId}) async {
    emit(DetailCartLoading());
    ApiResult<dynamic> result = await repository.deleteOrderItem(
        orderId: orderId, orderDetailId: orderDetailId);
    result.when(success: (dynamic data) {
      emit(DeleteOrderSuccess());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void applyCoupon({required String discountCode, required int orderId}) async {
    emit(DetailCartLoading());
    final result = await repository.applyCoupon(
        ApplyCouponRequest(couponCode: discountCode),
        orderId: orderId);
    result.when(success: (data) {
      emit(ApplyCouponSuccess());
    }, failure: (NetworkExceptions e) {
      if (e is BadRequest) {
        if (e.code == ServerError.coupon_is_already_applied_error) {
          errorText = R.string.coupon_is_already_applied_error.tr();
        } else if (e.code == ServerError.coupon_is_expired_error) {
          errorText = R.string.coupon_is_expired_error.tr();
        } else if (e.code == ServerError.not_found) {
          errorText = R.string.not_found.tr();
        } else if (e.code == ServerError.coupon_is_invalid_error) {
          errorText = R.string.coupon_is_invalid_error.tr();
        } else if (e.code == ServerError.coupon_can_not_combine_error) {
          errorText = R.string.coupon_can_not_combine_error.tr();
        } else if (e.code ==
            ServerError.coupon_is_unavailable_for_all_item_order_error) {
          errorText =
              R.string.coupon_is_unavaliable_for_all_item_order_error.tr();
        } else if (e.code ==
            ServerError.coupon_level_is_unavailable_for_this_item_error) {
          errorText =
              R.string.coupon_level_is_unavailable_for_this_item_error.tr();
        } else if (e.code ==
            ServerError.coupon_is_unavailable_for_this_item_error) {
          errorText = R.string.coupon_is_unavailable_for_this_item_error.tr();
        } else if (e.code == ServerError.coupon_invalid_min_price_error) {
          errorText = R.string.coupon_invalid_min_price_error.tr();
        } else if (e.code == ServerError.coupon_reach_maximum_usage_error) {
          errorText = R.string.coupon_reach_maximum_usage_error.tr();
        }
        emit(ApplyCouponErrorSuccess());
        return;
      }
      emit(DetailCartFailure(""));
    });
  }

  void refreshPage() {
    emit(DetailCartLoading());
    getDetailCart();
    emit(DetailCartInitial());
  }

  num discount(int index) {
    final item = listItem[index];
    int discount = 0;
    item.validCouponIds?.forEach((couponId) {
      final coupon =
          listCoupons.firstWhere((element) => element.coupon?.id == couponId);
      if (coupon.coupon?.discountType == DiscountType.PERCENTAGE.name) {
        discount += ((item.productType == ProductType.COURSE.name
                    ? item.course?.preferentialFee ?? 0
                    : item.event?.preferentialFee ?? 0) *
                (coupon.coupon?.discount ?? 0) ~/
                100) *
            item.quantity!;
      } else if (coupon.coupon?.discountType == DiscountType.PRICE.name) {
        discount += coupon.coupon?.discount ?? 0;
      }
    });
    // item.item?.currentFee = max((item.item?.currentFee ?? 0) - discount, 0);
    // return max((item.item?.currentFee ?? 0) - discount, 0);
    return discount;
  }

  num getTotalDiscount() {
    double totalDiscount = 0;
    num discountCombo = cartData?.fetchMyOrder?.totalComboCouponDiscount ?? 0;
    for (int i = 0; i < listItem.length; i++) {
      totalDiscount = totalDiscount + discount(i);
    }
    return totalDiscount + discountCombo;
  }
  num getTotalSingleDiscount() {
    double totalDiscount = 0;
    for (int i = 0; i < listItem.length; i++) {
      totalDiscount = totalDiscount + discount(i);
    }
    return totalDiscount ;
  }

  num getTotalPayment() {
    return totalPrice - totalDiscount;
  }

  int usePoint() {
    if (switchPoint) {
      return exchangeRate * maxPoint;
    }
    return 0;
  }

  int getMaxUsePoint() {
    if (isActiveUsePoint) {
      return min(
          maxPointConfig * exchangeRate > totalPayment
              ? totalPayment.toInt() % exchangeRate == 0
                  ? totalPayment.toInt() ~/ exchangeRate
                  : totalPayment.toInt() ~/ exchangeRate + 1
              : maxPointConfig,
          appPreferences.getInt(Const.POINT) ?? 0.toInt());
    }
    return min(
        totalPayment.toInt() % exchangeRate == 0
            ? totalPayment.toInt() ~/ exchangeRate
            : totalPayment.toInt() ~/ exchangeRate + 1,
        appPreferences.getInt(Const.POINT) ?? 0.toInt());
  }

  num getTotalPrice() {
    double total = 0;
    for (int i = 0; i < listItem.length; i++) {
      int preferentialFee = listItem[i].productType == ProductType.COURSE.name
          ? listItem[i].course?.preferentialFee ?? 0
          : listItem[i].event?.preferentialFee ?? 0;
      total = total + preferentialFee * listItem[i].quantity!;
    }
    return total;
  }

  void updateOrderQuantity(int index,
      {int? orderId,
      int? orderDetailId,
      required int quantity,
      bool? count}) async {
    //emit(DetailCartLoading());
    quantityItem = listItem[index].quantity ?? 0;
    quantityEdit = quantityItem;
    if (count == true) {
      quantityItem = quantity + 1;
    } else if (count == false) {
      if (quantity <= 1) {
        quantity = 1;
      } else {
        quantityItem = quantity - 1;
      }
    }
    ApiResult<dynamic> res = await repository.updateOrderQuantity(
        orderId: orderId, orderDetailId: orderDetailId, quantity: quantityItem);
    res.when(success: (data) {
      quantityController?[index].text = quantityItem.toString();
      emit(UpdateQuantitySuccess());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void editQuantity(
    int index, {
    int? orderId,
    int? orderDetailId,
    required int quantity,
  }) async {
    quantityEdit = listItem[index].quantity ?? 0;
    quantityEdit = quantity;
    ApiResult<dynamic> res = await repository.updateOrderQuantity(
        orderId: orderId, orderDetailId: orderDetailId, quantity: quantityEdit);
    res.when(success: (data) {
      emit(UpdateQuantitySuccess());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void deleteCoupon({int? orderId, int? couponId}) async {
    emit(DetailCartLoading());
    final res =
        await repository.deleteCoupon(orderId: orderId, couponId: couponId);
    res.when(success: (data) {
      emit(DeleteCouponSuccess());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void createOder(
      {required int orderId,
      required bool usePoint,
      PaymentType? paymentType}) async {
    emit(CreateOrderLoading());
    final res = await repository.createOder(CreateOrderRequest(
        orderId: orderId, paymentType: paymentType, usePoint: usePoint));
    res.when(success: (data) {
      // getDetailOrderCourse(data.id);
      emit(CreateOrderSuccess(data: data));
    }, failure: (e) {
      if (e is BadRequest) {
        if (e.code == ServerError.product_is_not_available_error) {
          final res = dio
              .post(AppConfig.environment.apiEndpoint + 'v1/order-history',
                  data: CreateOrderRequest(
                      orderId: orderId, paymentType: PaymentType.SUPPORT))
              .catchError((error) {
            emit(DetailCartFailure(error.response.data['data']));
          });
          return;
        }
        if (e.code == ServerError.coupon_is_invalid_error) {
          codeInvalid = R.string.coupon_is_invalid_error.tr();
        } else if (e.code == ServerError.coupon_invalid_min_price_error) {
          codeInvalid = R.string.coupon_is_invalid_error.tr();
        } else if (e.code == ServerError.not_support_payment_method_error) {
          errorPay = R.string.this_payment_method_is_temporarily.tr();
          emit(ErrorMethodPay());
        }
        emit(CodeDiscountInvalid());
        return;
      }
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  String? validateText(String value) {
    if (value.isEmpty) {
      return null;
    }
    return null;
  }

  void validateInputChange(String? text) {
    emit(DetailCartLoading());
    errorText = validateText(text!);
    emit(DetailCartSuccess());
  }

  void updateOrderItem(
    int index,
    String quantity, {
    int? itemId,
    String? note,
    String? userName,
    String? phone,
    String? email,
    int? orderId,
    int? provinceId,
    int? orderDetailId,
  }) async {
    emit(DetailCartLoading());
    final res = await repository.updateOrderItem(
      UpdateOrderItemRequest(
          phone: phone,
          email: email,
          userName: userName,
          itemId: itemId,
          provinceId: provinceId,
          quantity: quantity != "" ? int.parse(quantity) : countItem?[index],
          note: note),
      orderId: orderId,
      orderDetailId: orderDetailId,
    );
    res.when(success: (data) {
      countItem![index] = int.parse(quantityController![index].text);
      emit(UpdateOrderItemSuccess());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void updateQuantity(
      {required int index, required List<int> quantity, bool? disChange}) {
    if (disChange == true) {
      this.countItem?[index] = quantity[index] + 1;
      quantityController![index].text = countItem![index].toString();
    } else if (disChange == false) {
      if (this.countItem?[index] == 1) {
        countItem?[index] = 1;
      } else {
        this.countItem?[index] = quantity[index] - 1;
        quantityController![index].text = countItem![index].toString();
      }
    }
  }

  void changeText(int length, int index) {
    lengthText[index] = length;
    emit(DetailCartInitial());
  }

  void onChangedQuantity(
    int index, {
    int? orderId,
    int? orderDetailId,
    required int quantity,
  }) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      editQuantity(index,
          quantity: quantity, orderDetailId: orderDetailId, orderId: orderId);
    });
  }

  void getPointDci() async {
    emit(DetailCartLoading());
    ApiResult<CommunityDataV2Response> getProfileTask = await graphqlRepository
        .getCommunityV2(appPreferences.personProfile?.communityV2?.id ?? 0);
    getProfileTask.when(success: (CommunityDataV2Response data) async {
      appPreferences.saveCommunityV2(data.communityV2);
    }, failure: (NetworkExceptions error) async {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getPayment() async {
    emit(DetailCartLoading());
    final res = await graphqlRepository.getPayment();
    res.when(success: (List<FetchPaymentTypes> data) {
      listPayment = data;
      for (int i = 0; i < listPayment.length; i++) {
        if (listPayment[i].status == CommonStatus.ACTIVE.name) {
          hideButtonPayment = true;
        }
      }
      // emit(OptionPaymentSuccess());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void clearQuantity(int index) {
    emit(DetailCartLoading());
    this.countItem?[index] = listItem[index].quantity ?? 0;
    this.noteItemController?[index].text = listItem[index].note ?? "";
    this.nameController?[index].text =
        listItem[index].userInformation?.userName ?? "";
    this.phoneController?[index].text =
        listItem[index].userInformation?.phone ?? "";
    this.emailController?[index].text =
        listItem[index].userInformation?.email ?? "";
    this.quantityController?[index].text = listItem[index].quantity.toString();
    emit(DetailCartInitial());
  }

  void switchPayment(int index) {
    emit(DetailCartLoading());
    this.isSwitchPayment.value[index] = true;
    notContainPayment = false;
    switchIndex = index;
    for (int i = 0; i < isSwitchPayment.value.length; i++) {
      if (index != i) {
        isSwitchPayment.value[i] = false;
      }
    }
    listSwitch.value = List.from(isSwitchPayment.value);
    emit(SwitchPaymentSuccess());
  }

  void switchPaymentChild(int index) {
    emit(DetailCartLoading());
    this.isSwitchPaymentChild.value[index] = true;
    notContainPayment = false;
    switchPay = index;
    for (int i = 0; i < isSwitchPaymentChild.value.length; i++) {
      if (index != i) {
        isSwitchPaymentChild.value[i] = false;
      }
    }
    listSwitchChild.value = List.from(isSwitchPaymentChild.value);
    emit(SwitchMethodPaymentSuccess());
  }

  void getPointActionOut() async {
    emit(DetailCartLoading());
    ApiResult<PointActionOutData> getPointActionOut =
        await graphqlRepository.getPointActionOut();
    getPointActionOut.when(success: (PointActionOutData data) async {
      isActiveUsePoint = true;
      maxPointConfig = data.fetchPointActionOut
              ?.where((element) => element?.action == "PAY_FOR_ORDER")
              .first
              ?.maximumPoint ??
          0;
      if (data.fetchPointActionOut
              ?.where((element) => element?.action == "PAY_FOR_ORDER")
              .first
              ?.status ==
          'DISABLED') {
        isActiveUsePoint = false;
      }
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getDetailOrderCourse(
    int? orderHistoryId,
  ) async {
    ApiResult<FetchOrderHistoryByIdV2> result = await graphqlRepository
        .getDetailOrderCourse(orderHistoryId: orderHistoryId);
    result.when(success: (FetchOrderHistoryByIdV2 data) async {
      int pointExchange = data.orderHistoryDTO?.pointExchangeRate ?? 0;
      int usedPoint = data.orderHistoryDTO?.usedPointAmount ?? 0;
      appPreferences.setData(
          Const.POINT,
          appPreferences.getInt(Const.POINT) ??
              0.toInt() - pointExchange * usedPoint);
    }, failure: (NetworkExceptions error) async {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void checkPaymentValid() async {
    emit(DetailCartInitial());
    final res = await graphqlRepository.getPayment();
    res.when(success: (List<FetchPaymentTypes> data) {
      for (int i = 0; i < listPayment.length; i++) {
        if (data[i].childs?.indexWhere((element) =>
                element.type ==
                listPayment[switchIndex ?? 0].childs?[switchPay ?? 0].type) ==
            -1) {
          notContainPayment = true;
          indexNotValid = switchPay ?? -1;
          break;
        }
      }
      emit(CheckPaymentConfig());
    }, failure: (e) {
      emit(DetailCartFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }
}
