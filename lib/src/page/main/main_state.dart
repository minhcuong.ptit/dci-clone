import 'package:equatable/equatable.dart';

abstract class MainState extends Equatable {
  MainState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class MainInitial extends MainState {
  @override
  String toString() => 'MainInitial';
}

class MainLoading extends MainState {
  @override
  String toString() => 'MainLoading';
}

class MainFailure extends MainState {
  final String error;

  MainFailure(this.error);

  @override
  String toString() => 'MainFailure { error: $error }';
}

class MainPageChangeState extends MainState {
  final int position;

  MainPageChangeState(this.position);

  @override
  String toString() => 'MainPageState';

  @override
  List<Object> get props => [position];
}

class MainRefreshAuthState extends MainState {
  @override
  String toString() {
    return 'MainRefreshAuthState{}';
  }
}

class MainRefreshAllTabState extends MainState {
  @override
  String toString() {
    return 'MainRefreshAllTabState{}';
  }
}
