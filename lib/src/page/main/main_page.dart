import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/community/community.dart';
import 'package:imi/src/page/library/library.dart';
import 'package:imi/src/page/library/library_page.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/page/more/setting/setting_cubit.dart';
import 'package:imi/src/page/more/setting/setting_page.dart';
import 'package:imi/src/page/pin_code/change_password/change_password_cubit.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/dci_event_handle.dart';
import '../../app.dart';
import '../../utils/const.dart';
import '../../utils/navigation_utils.dart';
import '../favorite/favorite.dart';
import '../home_tab/home_tab.dart';
import '../meditation_detail_report/meditation_detail_report.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin, RouteAware {
  late final TabController _tabController;
  late MainCubit _mainCubit;
  late DCIEventHandler _eventHandler;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    _eventHandler = DCIEventHandler()
      ..subscribe<UpdateProfileEvent>((p0) {
        if (mounted) setState(() {});
      });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _mainCubit = GetIt.I();
    _mainCubit.initCubit();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return _mainCubit.pop();
      },
      child: DefaultTabController(
          length: 4,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: R.color.lightestGray,
            body: BlocConsumer<MainCubit, MainState>(
              bloc: _mainCubit,
              listener: (context, state) {
                if (state is MainPageChangeState) {
                  _tabController.animateTo(state.position);
                }
              },
              listenWhen: (pre, next) {
                return next is MainPageChangeState;
              },
              builder: (context, state) {
                int index = 0;
                if (state is MainPageChangeState) {
                  index = state.position;
                }
                return Column(
                  children: [
                    Expanded(
                      child: TabBarView(
                          controller: _tabController,
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            HomeTabPage(),
                            _mainCubit.listFavoriteCategory.isNotEmpty
                                ? CommunityPage()
                                : FavoritePage(
                                    checkPage: false,
                                    onRefresh: () {
                                      setState(() {
                                        _mainCubit.getListCategory();
                                      });
                                    },
                                  ),
                            BlocProvider<LibraryCubit>(
                              create: (_) => LibraryCubit(
                                  AppRepository(), AppGraphqlRepository()),
                              child: LibraryPage(),
                            ),
                            BlocProvider<SettingCubit>(
                                create: (_) => SettingCubit(
                                    appRepository: AppRepository(),
                                    graphqlRepository: AppGraphqlRepository()),
                                child: SettingPage()),
                          ]),
                    ),
                    Container(
                      width: context.width,
                      height: 74.h,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Divider(
                            color: R.color.btnDisable,
                            height: 0.2.h,
                          ),
                          Expanded(
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: R.color.white,
                                // border: Border.all(
                                //     color: R.color.btnDisable, width: 0.5.w),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  _navigationBarItem(
                                      onTap: () {
                                        _mainCubit.selectTab(0);
                                         },
                                      enable: index == 0,
                                      icon: R.drawable.ic_home,
                                      title: R.string.home.tr(),
                                      context: context),
                                  _navigationBarItem(
                                      onTap: () {
                                       _mainCubit.selectTab(1);
                                      },
                                      enable: index == 1,
                                      icon: R.drawable.ic_group,
                                      title: R.string.community.tr(),
                                      context: context),
                                  Visibility(
                                    visible: index != 0 &&
                                        _mainCubit
                                            .listFavoriteCategory.isNotEmpty &&
                                        appPreferences.hasDCILevel,
                                    child: GestureDetector(
                                      onTap: () {
                                        NavigationUtils.openCreatePostPopup(
                                                context)
                                            .then((value) {
                                          if (value == Const.ACTION_REFRESH) {}
                                        });
                                        //NavigationUtils.navigatePage(context, MeditationDetailReportPage());
                                      },
                                      child: Container(
                                        width: 42.w,
                                        height: 42.w,
                                        margin: EdgeInsets.only(bottom: 16.h),
                                        decoration: BoxDecoration(
                                          color: R.color.primaryColor,
                                          shape: BoxShape.circle,
                                        ),
                                        child: Icon(
                                          Icons.add_rounded,
                                          color: R.color.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  _navigationBarItem(
                                      onTap: () {
                                        _mainCubit.selectTab(2);
                                      },
                                      enable: index == 2,
                                      icon: R.drawable.ic_learn,
                                      title: R.string.library.tr(),
                                      context: context),
                                  _navigationBarItem(
                                      onTap: () {
                                        _mainCubit.selectTab(3);
                                      },
                                      enable: index == 3,
                                      icon: R.drawable.ic_user,
                                      title: R.string.account.tr(),
                                      context: context),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          )),
    );
  }

  Widget _navigationBarItem(
      {title,
      icon,
      required VoidCallback onTap,
      required bool enable,
      required BuildContext context}) {
    return Expanded(
      flex: 1,
      child: InkWell(
        onTap: onTap,
        child: IntrinsicHeight(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                icon,
                width: 24.w,
                color: enable ? R.color.primaryColor : R.color.lightGray,
              ),
              FittedBox(
                  child: Text(
                title,
                maxLines: 1,
                style: Theme.of(context)
                    .textTheme
                    .labelSmallText
                    .copyWith(fontSize: 12.sp, fontWeight: FontWeight.w400),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
