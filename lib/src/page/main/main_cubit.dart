import 'dart:async';
import 'dart:developer';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/response/deatil_cart_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/dci_application.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:injectable/injectable.dart';
import '../../data/network/response/community_data.dart';
import '../../data/network/response/my_profile_response.dart';
import '../../data/network/response/user_data.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import 'main.dart';

@LazySingleton()
class MainCubit extends Cubit<MainState> {
  late AppGraphqlRepository graphqlRepository;
  late FirebaseMessaging _firebaseMessaging;
  int countUnRead = 0;
  int prePosition = -1;
  int currentPosition = 0;
  Timer? _forceLogoutDebounce;
  DetailCartData? cartData;
  int countItem = 0;
  int countCart = 0;
  bool getInfoCommunitySuccess=false;

  bool get isLoggedIn => appPreferences.isLoggedIn;

  late List<int> listFavoriteCategory;

  MainCubit() : super(MainInitial()) {
    graphqlRepository = AppGraphqlRepository();
    Future.delayed(Duration(seconds: 1), () {
      _firebaseMessaging = FirebaseMessaging.instance;
      _firebaseMessaging.getToken().then((token) {
        logger.d(token);
        appPreferences.setData(Const.HEADER_KEY_DEVICE_TOKEN, token);
      });
    });
    initCubit();
  }

  initCubit() {
    getListCategory();
    getProfile();
    currentPosition = 0;
    getDetailCart();
  }

  getListCategory() {
    listFavoriteCategory = appPreferences
        .getListString(Const.FAVORITE_CATEGORY)
        .map((e) => int.parse(e))
        .toList();
  }

  void selectTab(int position) {
    if (currentPosition == position) return;
    prePosition = currentPosition;
    if(prePosition==1){
      DciApplication.videoPlayerController1?.pause();
    }
    currentPosition = position;
    emit(MainPageChangeState(currentPosition));
  }

  bool pop() {
    if (prePosition < 0) return true;
    selectTab(prePosition);
    prePosition = -1;
    return false;
  }

  void refreshAllTab() {
    emit(MainLoading());
    emit(MainRefreshAllTabState());
  }
  void eventRefreshCount() {
    emit(MainLoading());
    this.countCart = 1;
    emit(MainRefreshAllTabState());
  }

  void refreshAuth() {
    emit(MainRefreshAuthState());
  }

  void refreshMain() {
    emit(MainLoading());
    emit(MainInitial());
  }

  void forceLogOut() {
    if (_forceLogoutDebounce?.isActive ?? false) {
      _forceLogoutDebounce?.cancel();
    }
    _forceLogoutDebounce = Timer(const Duration(milliseconds: 500), () {
      appPreferences.clearData();
    });
  }

  void getProfile({bool isRefresh = false}) async {
    if (listFavoriteCategory.isEmpty) {
      int? mCommunityId = appPreferences.getInt(Const.COMMUNITY_ID);
      if (mCommunityId == null) {
        ApiResult<MyProfileResponse> getProfileTask =
            await graphqlRepository.getMyProfile();
        getProfileTask.when(success: (MyProfileResponse response) async {
          UserData? user = response.me;
          appPreferences.saveData(user);
          CommunityData? myCommunity = response.myCommunity;
          appPreferences.saveCommunity(myCommunity);
          getCommunity(myCommunity?.id ?? 0);
          //emit(GetProfileSuccess());
        }, failure: (NetworkExceptions error) async {
          //emit(CommunityFailure(NetworkExceptions.getErrorMessage(error)));
        });
      } else {
        getCommunity(mCommunityId);
      }
    }
  }

  Future<void> getCommunity(int communityId) async {
    ApiResult<CommunityDataV2Response> getCommunityTask =
        await graphqlRepository.getCommunityV2(communityId);
    getCommunityTask.when(success: (CommunityDataV2Response data) async {
      appPreferences.saveCommunityV2(data.communityV2);
      getInfoCommunitySuccess=true;
      getListCategory();
      refreshMain();
    }, failure: (NetworkExceptions error) async {
      getInfoCommunitySuccess=true;
      //emit(ViewProfileFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  Future<void> getDetailCart() async {
    emit(MainLoading());
    ApiResult<DetailCartData> result = await graphqlRepository.getDetailCart();
    result.when(success: (DetailCartData data) {
      cartData = data;
      if (data.fetchMyOrder?.total != null) {
        countItem = data.fetchMyOrder?.total ?? 0;
      }
      // emit(MainInitial());
    }, failure: (e) {
      emit(MainFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }
  // NotificationData? _handleParseNotification(RemoteMessage? message) {
  //   dynamic data = message?.data;
  //   if (data == null) return null;
  //   try {
  //     NotificationData data = NotificationData.fromJson(json.decode(message!.data['payload']));
  //     return data;
  //   } catch (e) {
  //     logger.e(e.toString());
  //   }
  //   return null;
  // }

  // void handleClickNotification(NotificationData? data) {
  //   if (data == null) return;
  // }

  @override
  Future<void> close() {
    _forceLogoutDebounce?.cancel();
    return super.close();
  }
}
