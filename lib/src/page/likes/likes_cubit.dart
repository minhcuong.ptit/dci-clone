import 'package:bloc/bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/likes/likes_state.dart';
import 'package:imi/src/utils/const.dart';

import '../../data/network/response/community_data.dart';
import '../../data/network/response/list_community_like_post_response.dart';

class LikesCubit extends Cubit<LikesState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityDataV2> likes = [];
  String? nextToken;

  LikesCubit({required this.repository, required this.graphqlRepository})
      : super(LikesInitial());

  void getLikes({
    bool isRefresh = false,
    bool isLoadMore = false,
    int postId = 0,
  }) async {
    emit(!isRefresh ? LikesLoading() : LikesInitial());
    if (isLoadMore != true) {
      nextToken = null;
      likes.clear();
    }

    ApiResult<ListCommunityLikePostResponse> followResult =
        await graphqlRepository.getLikes(
            postId, Const.NETWORK_DEFAULT_LIMIT, nextToken);
    followResult.when(success: (ListCommunityLikePostResponse response) async {
      if (response.dciCommunities?.data?.isNotEmpty ?? false) {
        likes.addAll(response.dciCommunities!.data!);
        nextToken = response.nextToken;
        emit(GetLikesSuccess());
      } else {
        emit(GetLikesEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(LikesFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void follow(int? communityId, bool isFollow) async {
    emit(LikesLoading());
    ApiResult<dynamic> result = await repository.follow(FollowRequest(
        communityId: communityId,
        action: isFollow ? Const.FOLLOW : Const.UNFOLLOW));
    result.when(success: (dynamic response) async {
      emit(FollowSuccess("success"));
    }, failure: (NetworkExceptions error) async {
      emit(LikesFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
