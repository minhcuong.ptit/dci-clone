import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/follow_response.dart';
import 'package:imi/src/page/profile/view_profile/view_profile.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/pending_action.dart';
import 'package:imi/src/widgets/user_list_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../data/network/response/community_data.dart';
import 'likes_cubit.dart';
import 'likes_state.dart';

class LikesPage extends StatefulWidget {
  final int postId;
  final int likeCount;

  const LikesPage({Key? key, required this.postId, required this.likeCount})
      : super(key: key);

  @override
  _LikesPageState createState() => _LikesPageState();
}

class _LikesPageState extends State<LikesPage> {
  late LikesCubit _cubit;

  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = LikesCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.getLikes(postId: widget.postId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
          context, R.string.the_person_who_expressed_his_feelings.tr(),
          textStyle: Theme.of(context)
              .textTheme
              .bodyBold
              .apply(color: R.color.black)
              .copyWith(fontSize: 14.sp, fontWeight: FontWeight.w500),
          centerTitle: false),
      body: BlocProvider(
        create: (BuildContext context) => _cubit,
        child: BlocConsumer<LikesCubit, LikesState>(
          listener: (context, state) {
            if (state is LikesFailure)
              Utils.showErrorSnackBar(context, state.error);
            if (!(state is LikesLoading)) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
            if (state is GetLikesSuccess) {}
            if (state is GetLikesEmpty) {}
          },
          builder: (BuildContext context, LikesState state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, LikesState state) {
    return Container(
      height: double.infinity,
      color: R.color.white,
      child: Stack(
        children: [
          SmartRefresher(
            enablePullUp: _cubit.nextToken != null,
            controller: _refreshController,
            onRefresh: () =>
                _cubit.getLikes(postId: widget.postId, isRefresh: true),
            onLoading: () =>
                _cubit.getLikes(postId: widget.postId, isLoadMore: true),
            child: _cubit.likes.length == 0
                ? Visibility(
                    visible: state is GetLikesEmpty,
                    child: Center(
                        child: Text(
                      R.string.no_one_like.tr(),
                    )),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 24.w, bottom: 12.h),
                        child: Text(
                          '${R.string.all.tr()} ${Utils().formatNumberLike(widget.likeCount)}',
                          style: Theme.of(context)
                              .textTheme
                              .labelSmallText
                              .copyWith(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                color: R.color.black,
                              ),
                        ),
                      ),
                      Expanded(
                        child: ListView.separated(
                          shrinkWrap: true,
                          itemCount: _cubit.likes.length,
                          separatorBuilder: (context, index) => Container(
                              color: R.color.primaryColor, height: 0.h),
                          itemBuilder: (BuildContext context, int index) {
                            CommunityDataV2 data = _cubit.likes[index];
                            return UserListWidget(
                              data: data.convertToV2(),
                              userListType: UserListType.like,
                              followCallback: () {
                                follow(data);
                              },
                              profileCallback: () {
                                NavigationUtils.navigatePage(
                                    context,
                                    ViewProfilePage(
                                      communityId: data.id,
                                    ));
                              },
                               checkStudyGroup: data.type == CommunityType.STUDY_GROUP,
                              checkMember: data.groupView?.myRoles !=null,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
          ),
          Visibility(
            visible: state is LikesLoading,
            child: PendingAction(),
          ),
        ],
      ),
    );
  }

  void follow(CommunityDataV2 data) {
    _cubit.follow(
        data.id!, data.userView?.isFollowing == true ? false : true);
    data.userView?.following =
        data.userView?.isFollowing == true ? false : true;
  }
}
