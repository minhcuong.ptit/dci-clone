
import 'package:equatable/equatable.dart';

abstract class LikesState extends Equatable {
  @override
  List<Object> get props => [];
}

class LikesInitial extends LikesState {}

class LikesFailure extends LikesState {
  final String error;

  LikesFailure(this.error);

  @override
  String toString() => 'LikesFailure { error: $error }';
}

class FollowSuccess extends LikesState {
  final String message;

  FollowSuccess(this.message);

  @override
  String toString() => 'LikesSuccess { message: $message }';
}

class LikesLoading extends LikesState {
  @override
  String toString() => 'LikesLoading';
}

class GetLikesEmpty extends LikesState {
  @override
  String toString() {
    return 'GetLikesEmpty{}';
  }
}

class GetLikesSuccess extends LikesState {
  @override
  String toString() => 'GetLikesSuccess';
}
