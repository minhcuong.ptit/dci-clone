import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ChooseCommunityState extends Equatable {
  ChooseCommunityState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialChooseCommunityState extends ChooseCommunityState {}

class ChooseCommunityLoading extends ChooseCommunityState {
  @override
  String toString() => 'BodyParameterLoading';
}

class GetListCategorySuccess extends ChooseCommunityState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends ChooseCommunityState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class ChooseCommunitySuccess extends ChooseCommunityState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

