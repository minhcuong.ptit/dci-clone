import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../data/network/response/community_data.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/community_widget.dart';
import 'choose_community.dart';

class ChooseCommunityPage extends StatefulWidget {
  final List<CommunityData> listCommunity;
  final List<CommunityData> listSelectedCommunity;

  const ChooseCommunityPage(
      {Key? key,
      required this.listCommunity,
      required this.listSelectedCommunity})
      : super(key: key);
  @override
  _ChooseCommunityPageState createState() => _ChooseCommunityPageState();
}

class _ChooseCommunityPageState extends State<ChooseCommunityPage> {
  late ChooseCommunityCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ChooseCommunityCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.listCommunity = widget.listCommunity;
    _cubit.listSelectedCommunity = widget.listSelectedCommunity;
    _cubit.getListContent();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<ChooseCommunityCubit, ChooseCommunityState>(
          listener: (context, state) {
            if (state is! ChooseCommunityLoading) {
              _refreshController.refreshCompleted();
            }
          },
          builder: (context, state) {
            return _buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, ChooseCommunityState state) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: R.color.white,
        centerTitle: true,
        elevation: 0,
        toolbarHeight: 70.h,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: (Navigator.of(context).canPop() == true
            ? IconButton(
                onPressed: () {
                  if (Navigator.canPop(context)) {
                    Navigator.of(context).pop();
                  }
                },
                icon: Icon(
                  CupertinoIcons.left_chevron,
                  color: R.color.black,
                ))
            : null),
        iconTheme: IconThemeData(color: R.color.black),
        title: Column(
          children: [
            Text(
              R.string.groups_for_you.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodyBold
                  .apply(color: R.color.black)
                  .copyWith(fontSize: 17.sp, fontWeight: FontWeight.w700),
            ),
            Text(
              R.string.choose_the_best_community.tr(),
              style: Theme.of(context)
                  .textTheme
                  .bodySmallText
                  .copyWith(color: R.color.grey),
            ),
          ],
        ),
      ),
      backgroundColor: R.color.white,
      body: _buildListItem(),
    );
  }

  Widget _buildListItem() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 16.h),
            _communityWidget(),
            SizedBox(height: 24.h),
            Center(
              child: SizedBox(
                width: 250.w,
                child: ButtonWidget(
                    textSize: 16.sp,
                    height: 48.h,
                    uppercaseTitle: false,
                    title: R.string.save.tr(),
                    onPressed: () {
                      if (Navigator.canPop(context)) {
                        NavigationUtils.pop(context,
                            result: _cubit.listSelectedCommunity);
                      }
                    }),
              ),
            ),
            SizedBox(
              height: 70.h,
            )
          ],
        ),
      ),
    );
  }

  Widget _communityWidget() {
    if (_cubit.listCommunity.isEmpty) {
      return SizedBox();
    }
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      primary: false,
      itemCount: _cubit.listCommunity.length,
      padding: EdgeInsets.zero,
      itemBuilder: (BuildContext context, int index) {
        CommunityData data = _cubit.listCommunity[index];
        bool isSelected = _cubit.listSelectedCommunity
                .indexWhere((element) => element.id == data.id) >=
            0;
        return CommunityWidget(
          isFromListing: true,
          data: data,
          onTap: () {
            _cubit.selectCommunity(data);
          },
          isSelected: isSelected,
          isShowStar: true,
          studyGroup: data.type == CommunityType.STUDY_GROUP,
        );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 10.h,
          mainAxisSpacing: 10.h,
          crossAxisCount: 2,
          childAspectRatio: 148.w / 168.h),
    );
  }
}
