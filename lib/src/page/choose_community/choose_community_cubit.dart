import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/network/response/community_data.dart';
import 'choose_community.dart';

class ChooseCommunityCubit extends Cubit<ChooseCommunityState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityData> listCommunity = [];
  List<CommunityData> listSelectedCommunity = [];

  ChooseCommunityCubit(
      {required this.repository, required this.graphqlRepository})
      : super(InitialChooseCommunityState());

  void getListContent() async {
    emit(GetListCommunitySuccess());
  }

  void selectCommunity(CommunityData selectedCommunity) {
    emit(ChooseCommunityLoading());
    int index = listSelectedCommunity
        .indexWhere((element) => element.id == selectedCommunity.id);
    if (index >= 0) {
      listSelectedCommunity.removeAt(index);
    } else {
      listSelectedCommunity.add(selectedCommunity);
    }
    emit(ChooseCommunitySuccess());
  }
}
