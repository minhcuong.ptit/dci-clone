import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/challeng_response.dart';
import 'package:imi/src/data/network/response/detail_challenge_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/home_challenges/home_challenges_state.dart';
import 'package:imi/src/utils/enum.dart';

class HomeChallengesCubit extends Cubit<HomeChallengesState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<ChallengeVersionById> listChallenges = [];
  String? nextToken;
  bool isFilter = false;
  int? isSelect;

  HomeChallengesCubit(this.repository, this.graphqlRepository)
      : super(HomeChallengesStateInitial());

  void getListChallenges({
    List<String>? status,
    bool? todo,
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh
        ? HomeChallengesStateLoading()
        : HomeChallengesStateInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listChallenges.clear();
    }
    ApiResult<FetchChallenge> getMeditation = await graphqlRepository
        .getListChallenges(nextToken: nextToken, status: status, todo: todo);
    getMeditation.when(success: (FetchChallenge data) async {
      if (data.data?.length != 0) {
        listChallenges.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(HomeChallengesStateSuccess());
      } else {
        emit(GetListChallengeEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(
          HomeChallengesStateFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void onFilter() {
    emit(HomeChallengesStateLoading());
    isFilter = true;
    emit(HomeChallengesStateInitial());
  }

  void refreshHomeChallenge() {
    emit(HomeChallengesStateLoading());
    getListChallenges();
    emit(HomeChallengesStateInitial());
  }
}
