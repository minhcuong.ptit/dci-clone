

abstract class HomeChallengesState {
  HomeChallengesState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class HomeChallengesStateInitial extends HomeChallengesState {
  @override
  String toString() => 'HomeChallengesStateInitial';
}

class HomeChallengesStateSuccess extends HomeChallengesState {
  @override
  String toString() => 'HomeChallengesStateSuccess';
}

class HomeChallengesStateLoading extends HomeChallengesState {
  @override
  String toString() => 'HomeChallengesStateLoading';
}

class HomeChallengesStateFailure extends HomeChallengesState {
  final String error;

  HomeChallengesStateFailure(this.error);

  @override
  String toString() => 'HomeChallengesStateFailure { error: $error }';
}

class GetListCleanSeedEmpty extends HomeChallengesState {
  @override
  String toString() {
    return 'GetListCleanSeedEmpty{}';
  }
}

class GetListSowSeedEmpty extends HomeChallengesState {
  @override
  String toString() {
    return 'GetListSowSeedEmpty{}';
  }
}

class GetListChallengeEmpty extends HomeChallengesState {
  @override
  String toString() {
    return 'GetListChallengeEmpty{}';
  }
}
