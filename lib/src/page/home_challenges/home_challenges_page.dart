import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/challeng_response.dart';
import 'package:imi/src/data/network/response/detail_challenge_response.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_page.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_timeline.dart';
import 'package:imi/src/page/home_challenges/home_challenges.dart';
import 'package:imi/src/page/start_practice/start_practice_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
class HomeChallengesPage extends StatefulWidget {
  const HomeChallengesPage({Key? key}) : super(key: key);

  @override
  State<HomeChallengesPage> createState() => _HomeChallengesPageState();
}

class _HomeChallengesPageState extends State<HomeChallengesPage> {
  late HomeChallengesCubit _cubit;
  RefreshController _refreshController = RefreshController();
  List<String> listTitle = [
    R.string.challenge_in_progress.tr(),
    R.string.challenge_done.tr(),
    R.string.unrealized_challenge.tr()
  ];
  List<String> listEnum = ["INPROGRESS", "RESOLVED", "TODO"];
  List<String> listSaveTitle = [];
  String? setTodo;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HomeChallengesCubit(repository, graphqlRepository);
    _cubit.getListChallenges();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
        context,
        R.string.dci_challenge.tr().toUpperCase(),
        backgroundColor: R.color.primaryColor,
        titleColor: R.color.white,
        centerTitle: true,
        iconColor: R.color.white,
      ),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<HomeChallengesCubit, HomeChallengesState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! HomeChallengesStateLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, HomeChallengesState state) {
    return StackLoadingView(
      visibleLoading: state is HomeChallengesStateLoading,
      child: Scrollbar(
        child: SmartRefresher(
          controller: _refreshController,
          enablePullUp: _cubit.nextToken != null,
          onRefresh: () {
            _cubit.getListChallenges(
                isRefresh: true,
                status: listSaveTitle
                            .where((element) => element == "TODO")
                            .toString() ==
                        "(TODO)"
                    ? listSaveTitle
                        .skipWhile((value) => value == "TODO")
                        .toList()
                    : listSaveTitle,
                todo: (listSaveTitle
                            .where((element) => element == "TODO")
                            .toString() ==
                        "(TODO)"
                    ? true
                    : null));
          },
          onLoading: () {
            _cubit.getListChallenges(
                isLoadMore: true,
                status: listSaveTitle
                            .where((element) => element == "TODO")
                            .toString() ==
                        "(TODO)"
                    ? listSaveTitle
                        .skipWhile((value) => value == "TODO")
                        .toList()
                    : listSaveTitle,
                todo: (listSaveTitle
                            .where((element) => element == "TODO")
                            .toString() ==
                        "(TODO)"
                    ? true
                    : null));
          },
          child: ListView(
            padding: EdgeInsets.all(20.h),
            children: [
              Text(
                R.string.well_come_practice.tr(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bold700.copyWith(
                    color: R.color.primaryColor,
                    fontSize: 20.sp,
                    height: 20 / 20),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 8.h),
              Text(
                R.string.choose_a_challenge_to_start_with.tr(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.regular400.copyWith(
                    color: R.color.primaryColor,
                    fontSize: 16.sp,
                    height: 20 / 16),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 8.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  dropdownFilter(),
                ],
              ),
              SizedBox(height: 12.h),
              _cubit.listChallenges.length == 0
                  ? Visibility(
                      visible: state is GetListChallengeEmpty,
                      child: Column(
                        children: [
                          SizedBox(height: 100.h),
                          Text(
                            R.string.there_are_currently_no_challenge.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(fontSize: 14.sp, color: R.color.gray),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ))
                  : GridView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.zero,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 16.h,
                          mainAxisSpacing: 16.h,
                          childAspectRatio: 9 / 10),
                      itemCount: _cubit.listChallenges.length,
                      itemBuilder: (BuildContext context, int index) {
                        ChallengeVersionById data =
                            _cubit.listChallenges[index];
                        return GestureDetector(
                          onTap: () {
                            if ((data.challengeHistory?.isLocked == false) &&
                                (data.challengeHistory?.id != null)) {
                              NavigationUtils.rootNavigatePage(
                                      context,
                                      DCIChallengePage(
                                          showPopup: false,
                                          challengeVersionId:
                                              data.challengeVersionId ?? 0))
                                  .then(
                                      (value) => _cubit.refreshHomeChallenge());
                            } else if ((data.challengeHistory?.isLocked ==
                                    false) &&
                                (data.challengeHistory?.id == null)) {
                              NavigationUtils.rootNavigatePage(
                                  context,
                                  StartPracticePage(
                                    challengeId: _cubit.listChallenges[index]
                                        .challengeVersionId,
                                  ));
                            } else if (data.challengeHistory?.isLocked ==
                                true) {
                              Utils.showCommonBottomSheet(
                                  context: context,
                                  title: R.string.notifications.tr(),
                                  formattedDetails: R
                                      .string.your_dci_program_is_not_suitable
                                      .tr(args: ["${data.name}"]),
                                  buttons: [
                                    ButtonWidget(
                                        backgroundColor: R.color.primaryColor,
                                        borderColor: R.color.primaryColor,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 6.h),
                                        title: R.string.understood.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          NavigationUtils.pop(context);
                                        }),
                                  ]);
                            } else {}
                          },
                          child: buildItemChallenges(context,
                              image: data.imageUrl,
                              title: data.name,
                              isLock: data.challengeHistory?.isLocked,
                              status: data.challengeHistory?.status,
                              completedTotal:
                                  data.challengeHistory?.completedTotal,
                              data: data),
                        );
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dropdownFilter() {
    return DropdownButtonHideUnderline(
      child: listEnum.length != 0
          ? DropdownButton2(
              dropdownPadding: EdgeInsets.zero,
              dropdownDecoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(8)),
              customButton: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.h),
                    color:
                        _cubit.isFilter ? R.color.primaryColor : R.color.white,
                    boxShadow: [
                      BoxShadow(
                        color: R.color.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 10,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ]),
                padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 20.h),
                child: Image.asset(
                  R.drawable.ic_filter_list,
                  height: 20.h,
                  color: _cubit.isFilter ? R.color.white : R.color.primaryColor,
                ),
              ),
              items: [
                DropdownMenuItem<Widget>(
                    enabled: false,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () {
                                  listSaveTitle.clear();
                                  _cubit.getListChallenges();
                                  NavigationUtils.pop(context);
                                },
                                child: Text(
                                  R.string.clear_filter.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 12.sp,
                                          color: R.color.primaryColor),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                              SizedBox(width: 20.w),
                              InkWell(
                                onTap: () {
                                  _cubit.onFilter();
                                  _cubit.getListChallenges(
                                      status: listSaveTitle
                                                  .where((element) =>
                                                      element == "TODO")
                                                  .toString() ==
                                              "(TODO)"
                                          ? listSaveTitle
                                              .skipWhile(
                                                  (value) => value == "TODO")
                                              .toList()
                                          : listSaveTitle,
                                      todo: (listSaveTitle
                                                  .where((element) =>
                                                      element == "TODO")
                                                  .toString() ==
                                              "(TODO)"
                                          ? true
                                          : null));
                                  NavigationUtils.pop(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(48.h),
                                      color: R.color.primaryColor),
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      15.h, 2.h, 15.h, 4.h),
                                  child: Text(
                                    R.string.filter.tr(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: R.color.white),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(height: 1.h, color: R.color.grey),
                      ],
                    )),
                ...listEnum.map((item) {
                  return DropdownMenuItem<String>(
                    value: item,
                    enabled: false,
                    child: StatefulBuilder(
                      builder: (context, menuSetState) {
                        final _isSelected = listSaveTitle.contains(item);
                        return InkWell(
                          onTap: () {
                            _isSelected
                                ? listSaveTitle.remove(item)
                                : listSaveTitle.add(item);
                            setState(() {});
                            menuSetState(() {});
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.h, vertical: 10.h),
                            child: Row(
                              children: [
                                _isSelected
                                    ? Image.asset(
                                        R.drawable.ic_check_box,
                                        height: 16.h,
                                      )
                                    : Image.asset(
                                        R.drawable.ic_un_ckeck_box,
                                        height: 16.h,
                                      ),
                                SizedBox(width: 8.w),
                                Expanded(
                                  child: Text(
                                    item == "INPROGRESS"
                                        ? R.string.challenge_in_progress.tr()
                                        : (item == "RESOLVED"
                                            ? R.string.challenge_done.tr()
                                            : (item == "TODO"
                                                ? R.string.unrealized_challenge
                                                    .tr()
                                                : "")),
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: _isSelected
                                                ? R.color.primaryColor
                                                : R.color.grey),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }),
              ],
              value: listSaveTitle.isEmpty ? null : listSaveTitle.last,
              onChanged: (value) {},
              dropdownWidth: 250.w,
              dropdownMaxHeight: 250.h,
              enableFeedback: true,
              itemPadding: EdgeInsets.zero,
              selectedItemBuilder: (context) {
                return listEnum.map(
                  (item) {
                    return Container(
                      alignment: AlignmentDirectional.center,
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        listSaveTitle.join(', '),
                        style: TextStyle(
                          fontSize: 14,
                          overflow: TextOverflow.ellipsis,
                        ),
                        maxLines: 1,
                      ),
                    );
                  },
                ).toList();
              },
            )
          : SizedBox(),
    );
  }

  Widget buildTitleFilter(String title, Color color) {
    return Text(
      title,
      style: Theme.of(context)
          .textTheme
          .regular400
          .copyWith(fontSize: 12.sp, color: color),
      overflow: TextOverflow.ellipsis,
      maxLines: 1,
    );
  }

  Widget buildItemChallenges(BuildContext context,
      {String? image,
      String? title,
      bool? isLock,
      String? status,
      int? completedTotal,
      ChallengeVersionById? data}) {
    int? completed = completedTotal ?? 0;
    int totalProgress = completed + 1;
    String? icon = (status == ChallengeHistoryStatus.INPROGRESS.name)
        ? _getCurrentIcon(data!)
        : null;
    return Container(
      decoration: BoxDecoration(
          color: status == ChallengeHistoryStatus.INPROGRESS.name
              ? R.color.lightBlue
              : R.color.lightestGray,
          borderRadius: BorderRadius.circular(10.h)),
      padding: EdgeInsets.all(4.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10.h),
                child: CachedNetworkImage(
                    width: double.infinity,
                    height: 108.h,
                    fit: BoxFit.cover,
                    imageUrl: image ?? ""),
              ),
              Visibility(
                visible: isLock ?? false,
                child: Positioned(
                    top: 5.h,
                    right: 5.h,
                    child: Container(
                      width: 25.h,
                      height: 25.h,
                      child: Icon(
                        CupertinoIcons.lock,
                        size: 15.h,
                        color: R.color.blue,
                      ),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: R.color.white),
                    )),
              ),
              if (status == ChallengeHistoryStatus.INPROGRESS.name) ...[
                Positioned(
                    top: 5.h,
                    right: 5.h,
                    child: Visibility(
                      visible: icon != null,
                      child: Container(
                        child: Image.asset(
                          icon ?? "",
                          height: 25.h,
                          width: 25.h,
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: R.color.white),
                      ),
                    ))
              ],
              if (status == ChallengeHistoryStatus.RESOLVED.name) ...[
                Positioned(
                    top: 5.h,
                    right: 5.h,
                    child: Container(
                      child: Image.asset(
                        R.drawable.challenge_day_21,
                        height: 25.h,
                        width: 25.h,
                      ),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: R.color.white),
                    ))
              ],
            ],
          ),
          SizedBox(height: 8.h),
          Expanded(
            child: Text(
              title ?? "",
              style: Theme.of(context).textTheme.regular400.copyWith(
                  color: status == ChallengeHistoryStatus.INPROGRESS.name
                      ? R.color.white
                      : R.color.black,
                  fontSize: 12.sp,
                  height: 16 / 12),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(height: 2.h),
          if (status == ChallengeHistoryStatus.RESOLVED.name) ...[
            Text(
              R.string.executed_time.tr(args: ["${completedTotal}"]),
              style: Theme.of(context).textTheme.regular400.copyWith(
                    color: R.color.grey,
                    fontSize: 12.sp,
                    height: 16 / 12,
                  ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
          if (status == ChallengeHistoryStatus.INPROGRESS.name) ...[
            Text(
              "${R.string.ongoing_time.tr()}" " ${totalProgress}",
              style: Theme.of(context).textTheme.regular400.copyWith(
                    color: status == ChallengeHistoryStatus.INPROGRESS.name
                        ? R.color.white
                        : R.color.grey,
                    fontSize: 12.sp,
                    height: 16 / 12,
                  ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
          if (status == null) ...[
            Text(
              R.string.unfulfilled.tr(),
              style: Theme.of(context).textTheme.regular400.copyWith(
                    color: R.color.grey,
                    fontSize: 12.sp,
                    height: 16 / 12,
                  ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
          SizedBox(height: 2.h),
        ],
      ),
    );
  }

  String? _getCurrentIcon(ChallengeVersionById detail) {
    int currentStep = 0;
    DateTime? lastKnownDate;
    detail.challengeHistory?.missionHistory?.forEach((element) {
      if (element?.completedDate != null) {
        if (currentStep <= (element?.dayNo ?? 1)) {
          currentStep = element?.dayNo ?? 1;
          lastKnownDate = element?.completedDate;
        }
      }
    });
    if (lastKnownDate != null &&
        !DateUtils.isSameDay(lastKnownDate, DateTime.now())) {
      currentStep++;
    }
    if ((detail.challengeHistory?.missionHistory ?? []).isNotEmpty) {
      final index = detail.challengeHistory!.missionHistory!
          .indexWhere((element) => element?.dayNo == currentStep);
      if (index >= 0 &&
          detail.challengeHistory!.missionHistory![index]?.completedMissionIds
                  ?.length ==
              detail
                  .challengeHistory!.missionHistory![index]!.missions?.length) {
        // emit(DCIChallengeMission());
      } else {
        currentStep--;
      }
    }
    if (currentStep <= 0) return null;
    int realIndex = currentStep;
    int max = detail.dayNo ?? 1;
    if (realIndex == detail.dayNo) {
      return R.drawable.challenge_day_21;
    }
    int mod = (max - 1) % 20;
    int quantitiesPerDay = (max - 1) ~/ 20;
    int bound = (quantitiesPerDay + 1) * mod;
    if (realIndex > bound) {
      return _icons[(currentStep - 1 - bound) ~/ quantitiesPerDay + mod];
    }
    return _icons[(currentStep - 1) ~/ (quantitiesPerDay + 1)];
  }
}

final _icons = [
  R.drawable.challenge_day_1,
  R.drawable.challenge_day_2,
  R.drawable.challenge_day_3,
  R.drawable.challenge_day_4,
  R.drawable.challenge_day_5,
  R.drawable.challenge_day_6,
  R.drawable.challenge_day_7,
  R.drawable.challenge_day_8,
  R.drawable.challenge_day_9,
  R.drawable.challenge_day_9,
  R.drawable.challenge_day_10,
  R.drawable.challenge_day_11,
  R.drawable.challenge_day_12,
  R.drawable.challenge_day_13,
  R.drawable.challenge_day_14,
  R.drawable.challenge_day_15,
  R.drawable.challenge_day_16,
  R.drawable.challenge_day_17,
  R.drawable.challenge_day_18,
  R.drawable.challenge_day_19,
  R.drawable.challenge_day_20,
];
