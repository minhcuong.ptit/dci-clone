import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/create_zen_response.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class CompleteZenningPage extends StatelessWidget {
  final CreateZenResponse data;
  const CompleteZenningPage({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.zenBackgroundBlack,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20.0.h),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context);
                  },
                  child: Image.asset(
                    R.drawable.ic_cancel,
                    color: R.color.white,
                    width: 24.h,
                    height: 24.h,
                  ),
                ),
              ),
              SizedBox(
                height: 24.h,
              ),
              Image.asset(
                R.drawable.ic_zen,
                color: R.color.white,
                width: 40.h,
                height: 40.h,
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                R.string.zen_streak.tr(args: [data.dayStreak.toString()]),
                style:
                    Theme.of(context).textTheme.title.copyWith(fontSize: 16.sp),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 8.h,
              ),
              _daysOfWeek(),
              SizedBox(
                height: 8.h,
              ),
              Text(
                R.string.zen_duration_description
                    .tr(args: [(data.duration ~/ 60).toString()]),
                style: Theme.of(context).textTheme.medium500.copyWith(
                    fontSize: 12, height: 18 / 12, color: R.color.grey),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _daysOfWeek() {
    List<String> days = [
      R.string.mon.tr(),
      R.string.tue.tr(),
      R.string.wed.tr(),
      R.string.thu.tr(),
      R.string.fri.tr(),
      R.string.sat.tr(),
      R.string.sun.tr(),
    ];
    return SizedBox(
      width: 216.w,
      height: 24,
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Container(
              width: 24,
              height: 24,
              padding: EdgeInsets.only(bottom: 1.5),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: data.dayOfWeeks.contains(index + 1)
                      ? R.color.primaryColor
                      : null,
                  borderRadius: BorderRadius.circular(2)),
              child: Text(
                days[index],
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodySmallText
                    .copyWith(color: R.color.white),
              ),
            );
          },
          separatorBuilder: (_, __) => SizedBox(
                width: 8,
              ),
          itemCount: 7),
    );
  }
}
