import 'dart:async';
import 'dart:developer';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/request/create_zen_request.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/zen/zen_page.dart';
import 'package:imi/src/page/zen/zen_state.dart';
import 'package:just_audio/just_audio.dart';

class ZenCubit extends Cubit<ZenState> {
  bool _isPlaying = true;

  bool get isPlaying => _isPlaying;

  Timer? timer;
  int? total;
  bool isPreparing = true;
  bool isEnd = false;

  int _stopTick = 0;
  int _endRepeatCount = 0;
  int _startRepeatCount = 0;
  int _startedTime = 0;
  ZenOption? option;

  final AudioPlayer backgroundPlayer = AudioPlayer();
  final AudioPlayer startPlayer = AudioPlayer();
  final AudioPlayer endPlayer = AudioPlayer();
  final AudioPlayer intervalPlayer = AudioPlayer();

  StreamSubscription<bool>? _subscription;

  AppRepository _repository = AppRepository();

  late int meditationId;
  bool loadSourceSuccess = false;

  ZenCubit(this.total, this.option, this.meditationId)
      : super(ZenInitialState());

  int get tick => (timer?.tick ?? 0) - _stopTick;


  void init() async {
    emit(ZenLoadingState());
    if (option?.backgroundMusic != null) {
      if (option!.backgroundMusic!.contains("https://s3")) {
        await backgroundPlayer.setUrl(option!.backgroundMusic!);
      } else if (option!.backgroundMusic!.startsWith("assets/")) {
        await backgroundPlayer.setAsset(option!.backgroundMusic!,
            preload: true);
      } else {
        await backgroundPlayer.setFilePath(option!.backgroundMusic!,
            preload: true);
      }
      backgroundPlayer.setLoopMode(LoopMode.one);
    }
    if (option?.startOption?.musicPath != null) {
      if (option!.startOption!.musicPath!.contains("https://s3")) {
        await startPlayer.setUrl(option!.startOption!.musicPath!,
            preload: true);
      } else {
        await startPlayer.setAsset(option!.startOption!.musicPath!,
            preload: true);
      }
      startPlayer.playerStateStream.listen((state) {
        if (state.processingState == ProcessingState.completed) {
          _startRepeatCount++;
          if (_startRepeatCount < option!.startOption!.repeatTimes) {
            startPlayer.seek(Duration(seconds: 0));
            startPlayer.play();
          }
        }
      });
    }

    if (option?.endOption?.musicPath != null) {
      await endPlayer.setAsset(option!.endOption!.musicPath!, preload: true);
      endPlayer.playerStateStream.listen((state) {
        if (state.processingState == ProcessingState.completed) {
          _endRepeatCount++;
          if (_endRepeatCount < option!.endOption!.repeatTimes) {
            endPlayer.seek(Duration(seconds: 0));
            endPlayer.play();
          } else {
            _createZen();
          }
        }
      });
    }
    if (option?.intervalOption?.musicPath != null) {
      await intervalPlayer.setAsset(option!.intervalOption!.musicPath!);
    }

    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_isPlaying) {
        emit(ZenTimerChangeState());
        if (tick >= (option?.delay ?? 0)) {
          isPreparing = false;
          _stopTick = 0;
          this.timer?.cancel();
          startZen();
        }
      } else {
        _stopTick++;
      }
    });
  }



  void countTime() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_isPlaying) {
        emit(ZenTimerChangeState());
        if (tick >= (option?.delay ?? 0)) {
          isPreparing = false;
          _stopTick = 0;
          this.timer?.cancel();
          startZen();
        }
      } else {
        _stopTick++;
      }
    });
  }

  void cancelSubscription() {
    _subscription?.pause();
  }

  void startZen() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) async {
      if (_isPlaying) {
        if (tick <= 1) {
          _startedTime = DateTime.now().millisecondsSinceEpoch;
          if (option?.startOption?.musicPath != null) {
            startPlayer.play();
          }
          if (option?.backgroundMusic != null) {
            backgroundPlayer.play();
          }
        } else if (total != null && tick >= total!) {
          if (option?.endOption?.musicPath != null) {
            endPlayer.play();
          } else {
            stop(false);
            _createZen();
          }
          isEnd = true;
        } else if (option?.intervalOption?.intervalDuration.inSeconds != null &&
            tick > 0 &&
            tick % option!.intervalOption!.intervalDuration.inSeconds == 0) {
          intervalPlayer.seek(const Duration(seconds: 0));
          intervalPlayer.play();
        }
        emit(ZenTimerChangeState());
      } else {
        _stopTick++;
      }
    });
  }

  void pause() {
    _isPlaying = false;
    backgroundPlayer.pause();
    startPlayer.pause();
    intervalPlayer.pause();
    emit(ZenTimerChangeState());
  }

  void play() {
    if (!_isPlaying) {
      _isPlaying = true;
      if (!isPreparing) {
        backgroundPlayer.play();
        if (_startRepeatCount < (option?.startOption?.repeatTimes ?? -1))
          startPlayer.play();
      }
      emit(ZenTimerChangeState());
    }
  }

  void stop(bool manual) async {
    if (isPreparing) {
      emit(ZenExitBeforeZenState());
      return;
    }
    emit(ZenLoadingState());
    timer?.cancel();
    _isPlaying = false;
    backgroundPlayer.pause();
    startPlayer.pause();
    intervalPlayer.pause();
    if (manual) {
      endPlayer.pause();
      _createZen();
    }
  }

  void _createZen() async {
    final res = await _repository.createZen(CreateZenRequest(
        meditationId: meditationId,
        startedDate: _startedTime,
        completedDate: DateTime.now().millisecondsSinceEpoch,
        duration: tick));
    res.when(success: (data) {
      emit(ZenEndSuccessState(data));
    }, failure: (e) {
      emit(ZenFailureState(NetworkExceptions.getErrorMessage(e)));
    });
  }

  Future<void> close() async {
    timer?.cancel();
    backgroundPlayer.dispose();
    startPlayer.dispose();
    endPlayer.dispose();
    intervalPlayer.dispose();
    _subscription?.cancel();
    super.close();
  }
}
