import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/zen/complete_zenning.dart';
import 'package:imi/src/page/zen/countdown_painter.dart';
import 'package:imi/src/page/zen/zen_cubit.dart';
import 'package:imi/src/page/zen/zen_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:wakelock/wakelock.dart';

class ZenOption {
  int? delay;
  ZenRepeatOption? startOption;
  ZenRepeatOption? endOption;
  ZenIntervalOption? intervalOption;
  final String? backgroundMusic;

  ZenOption(
      {this.delay,
      this.startOption,
      this.endOption,
      this.intervalOption,
      required this.backgroundMusic});
}

class ZenIntervalOption {
  final String? musicPath;
  final Duration intervalDuration;

  ZenIntervalOption({required this.musicPath, required this.intervalDuration});
}

class ZenRepeatOption {
  final String? musicPath;
  final int repeatTimes;

  ZenRepeatOption({required this.musicPath, required this.repeatTimes});
}

class ZenPage extends StatefulWidget {
  final Duration? totalTime;
  final int meditationId;
  final ZenOption? option;
  final int index;

  const ZenPage(
      {Key? key,
      required this.totalTime,
      required this.option,
      required this.meditationId,
      required this.index
      })
      : super(key: key);

  @override
  _ZenPageState createState() => _ZenPageState();
}

class _ZenPageState extends State<ZenPage> with WidgetsBindingObserver {
  late final ZenCubit _cubit;
  late ui.Image zenImage;

  void initState() {
    super.initState();
    _cubit = ZenCubit(
        widget.totalTime?.inSeconds, widget.option, widget.meditationId);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _cubit.init();
    });
    WidgetsBinding.instance.addObserver(this);
  }

  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == ui.AppLifecycleState.inactive||state==ui.AppLifecycleState.detached||state==ui.AppLifecycleState.paused) {
      Wakelock.disable();
      _cubit.cancelSubscription();
      _cubit.pause();
    }
    if(state==ui.AppLifecycleState.resumed){
      Wakelock.enable();
    }
  }

  Future _loadImage(String imagePath) async {
    ByteData bd = await rootBundle.load(imagePath);
    final Uint8List bytes = Uint8List.view(bd.buffer);
    final ui.Codec codec = await ui.instantiateImageCodec(bytes);
    final ui.Image image = (await codec.getNextFrame()).image;
    zenImage = image;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.zenBackgroundBlack,
      body: WillPopScope(
        onWillPop: () async {
          if (_cubit.isPreparing || _cubit.state is ZenLoadingState)
            return true;
          return await Utils.showConfirmationBottomSheet(
              context: context,
              title: R.string.confirm.tr(),
              formatedDetails: R.string.exit_zen_detail.tr(),
              confirmationDescription: R.string.yes.tr(),
              cancelText: R.string.no.tr(),
              onConfirm: () {
                _cubit.stop(true);
                return false;
              }).then((value) {
            if (value is bool) return value;
            return false;
          });
        },
        child: FutureBuilder(
          future: _loadImage(R.drawable.ic_zen),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return SizedBox();
            return BlocConsumer<ZenCubit, ZenState>(
              bloc: _cubit,
              listener: (context, state) {
                if (state is ZenExitBeforeZenState) {
                  NavigationUtils.pop(context);
                } else if (state is ZenEndSuccessState) {
                  NavigationUtils.replacePage(
                      context, CompleteZenningPage(data: state.zenResponse));
                } else if (state is ZenFailureState) {
                  Utils.showSnackBar(context, state.error);
                  NavigationUtils.pop(context);
                }
              },
              builder: (context, state) => StackLoadingView(
                visibleLoading: state is ZenLoadingState,
                child: SafeArea(
                  child: Column(
                    children: [
                      Center(
                        child: Text(
                          R.string.zen.tr(args: [""]),
                          style: Theme.of(context).textTheme.title,
                        ),
                      ),
                      Expanded(
                          child: Padding(
                              padding: EdgeInsets.all(36.w),
                              child: LayoutBuilder(
                                builder: (context, constraints) {
                                  double radius =
                                      min(300.w, constraints.maxWidth);
                                  return Center(
                                    child: SizedBox(
                                      width: radius,
                                      height: radius,
                                      child: widget.totalTime != null ||
                                              _cubit.isPreparing
                                          ? _hasLimitedTime(radius, context)
                                          : _hasUnlimitedTime(radius, context),
                                    ),
                                  );
                                },
                              ))),
                      _controlButtons(),
                      SizedBox(
                        height: 36.h,
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Column _hasUnlimitedTime(double radius, BuildContext context) {
    return Column(
      children: [
        Image.asset(
          R.drawable.ic_infinity,
          width: radius - 36.w,
          height: radius - 72.h,
          color: R.color.white,
        ),
        Text(
          (_cubit.isPreparing
                  ? R.string.preparing.tr()
                  : R.string.zenning_unlimit.tr()) +
              ": " +
              _remainTime,
          style: Theme.of(context)
              .textTheme
              .medium500
              .copyWith(fontSize: 16, color: R.color.white),
        ),
      ],
    );
  }

  Stack _hasLimitedTime(double radius, BuildContext context) {
    return Stack(
      children: [
        CustomPaint(
          painter: CountDownBackgroundPainter(),
          size: Size(radius, radius),
        ),
        if ((widget.option?.delay ?? 0) > 0 || !_cubit.isPreparing)
          CustomPaint(
            painter: CountDownPainter(
                max: _cubit.isPreparing
                    ? widget.option?.delay ?? 0
                    : (widget.totalTime ?? Duration(seconds: 3000)).inSeconds,
                current: _cubit.tick,
                image: zenImage),
            size: Size(radius, radius),
          ),
        CustomPaint(
          painter: CountDownForegroundPainter(),
          size: Size(radius, radius),
        ),
        Center(
          child: IntrinsicHeight(
            child: Column(
              children: [
                Text(
                  _remainTime,
                  style: Theme.of(context).textTheme.title,
                ),
                Text(
                  _cubit.isPreparing && widget.option != null
                      ? R.string.preparing.tr()
                      : _cubit.isEnd
                          ? R.string.completed.tr()
                          : R.string.zenning_limit.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 14, color: R.color.white),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  String get _remainTime {
    int tick = _cubit.tick;
    int seconds = tick % 60;
    tick ~/= 60;
    String secStr = "${seconds > 9 ? '' : '0'}$seconds";
    int minutes = tick % 60;
    tick ~/= 60;
    String minStr = "${minutes > 9 ? '' : '0'}$minutes";
    int hours = tick;
    String hoursStr = "${hours > 9 ? '' : '0'}$hours";
    return "$hoursStr:$minStr:$secStr";
  }

  Widget _controlButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        if (_cubit.isPlaying)
          GestureDetector(
            onTap: () {
              _cubit.pause();
            },
            child: Container(
              width: 77.h,
              height: 77.h,
              decoration: BoxDecoration(
                  color: R.color.primaryLightGrey,
                  borderRadius: BorderRadius.circular(100)),
              child: Icon(
                Icons.pause,
                size: 36.h,
              ),
            ),
          ),
        if (!_cubit.isPlaying) ...[
          if (!_cubit.isEnd)
            GestureDetector(
              onTap: _cubit.play,
              child: Container(
                width: 77.h,
                height: 77.h,
                decoration: BoxDecoration(
                    color: R.color.primaryLightGrey,
                    borderRadius: BorderRadius.circular(100)),
                child: Icon(
                  Icons.play_arrow,
                  size: 36.h,
                ),
              ),
            ),
          GestureDetector(
            onTap: () {
              _cubit.stop(true);
            },
            child: Container(
              width: 77.h,
              height: 77.h,
              decoration: BoxDecoration(
                  color: R.color.primaryLightGrey,
                  borderRadius: BorderRadius.circular(100)),
              child: Icon(
                Icons.stop,
                size: 36.h,
              ),
            ),
          ),
        ]
      ],
    );
  }
}
