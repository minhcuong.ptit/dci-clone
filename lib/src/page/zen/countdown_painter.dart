import 'dart:math';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:imi/res/R.dart';

class CountDownPainter extends CustomPainter {
  final int max;
  final int current;
  final ui.Image image;

  CountDownPainter(
      {required this.max, required this.current, required this.image});

  @override
  void paint(Canvas canvas, Size size) {
    final centerOffset = Offset(size.width / 2, size.height / 2);
    final innerRadius = size.width / 2 - 40;
    final currentAngle = current * pi * 2 / max;
    canvas.drawArc(
        Rect.fromCenter(
            center: Offset(size.width / 2, 20), width: 40, height: 40),
        pi / 2,
        pi,
        true,
        Paint()..color = R.color.primaryColor);

    final path = ui.Path();
    path.moveTo(size.width / 2, 0);
    path.addArc(
      Rect.fromCenter(
          center: centerOffset, width: size.width, height: size.height),
      -pi / 2,
      currentAngle,
    );
    canvas.drawArc(
        Rect.fromCenter(
            center: centerOffset, width: size.width, height: size.height),
        -pi / 2,
        currentAngle,
        true,
        Paint()
          ..color = R.color.primaryColor
          ..strokeWidth = 40);
    canvas.drawCircle(
        Offset(size.width / 2 + (innerRadius + 20) * sin(currentAngle),
            size.height / 2 - (innerRadius + 20) * cos(currentAngle)),
        20,
        Paint()..color = R.color.primaryColor);
    canvas.drawCircle(
        Offset(size.width / 2 + (innerRadius + 20) * sin(currentAngle),
            size.height / 2 - (innerRadius + 20) * cos(currentAngle)),
        14,
        Paint()..color = R.color.black);
    paintImage(
        canvas: canvas,
        rect: Rect.fromCircle(
            center: Offset(
                size.width / 2 + (innerRadius + 20) * sin(currentAngle),
                size.height / 2 - (innerRadius + 20) * cos(currentAngle)),
            radius: 10),
        image: image);
    // path.lineTo(
    //   size.width / 2 + innerRadius * sin(currentAngle),
    //   size.height / 2 - innerRadius * cos(currentAngle),
    // );
    // path.addArc(
    //   Rect.fromCenter(
    //       center: centerOffset,
    //       width: innerRadius * 2,
    //       height: innerRadius * 2),
    //   -pi / 2,
    //   currentAngle,
    // );
    // path.close();
    // canvas.drawPath(path, Paint()..color = R.color.primaryColor);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    if (oldDelegate is CountDownPainter) {
      return oldDelegate.current / oldDelegate.max != current / max;
    }
    return false;
  }
}

class CountDownBackgroundPainter extends CustomPainter {
  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    final centerOffset = Offset(size.width / 2, size.height / 2);
    canvas.drawCircle(centerOffset, size.width / 2,
        Paint()..color = R.color.zenCircleBackgroundBlack.withOpacity(0.47));
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class CountDownForegroundPainter extends CustomPainter {
  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    final centerOffset = Offset(size.width / 2, size.height / 2);

    canvas.drawCircle(centerOffset, size.width / 2 - 40,
        Paint()..color = R.color.zenCircleBackgroundBlack);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
