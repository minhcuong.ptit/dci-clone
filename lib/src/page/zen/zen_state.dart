import 'package:imi/src/data/network/response/create_zen_response.dart';

abstract class ZenState {
  const ZenState();
}

class ZenInitialState extends ZenState {}

class ZenLoadingState extends ZenState {}

class ZenExitBeforeZenState extends ZenState {}

class ZenTimerChangeState extends ZenState {}

class ZenEndSuccessState extends ZenState {
  final CreateZenResponse zenResponse;

  ZenEndSuccessState(this.zenResponse);
}

class ZenFailureState extends ZenState {
  final String error;

  ZenFailureState(this.error);
}
