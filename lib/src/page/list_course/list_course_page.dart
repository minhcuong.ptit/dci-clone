import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/course_details/course_details.dart';
import 'package:imi/src/page/list_course/list_course.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../data/network/response/product_topic_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../widgets/item_course.dart';
import '../../widgets/search/search_widget.dart';

class ListCoursePage extends StatefulWidget {
  final int? couponId;
  final String? codeCoupons;
  late int? selected;
  final List<int>? topicSearch;

  ListCoursePage(
      {this.couponId, this.codeCoupons, this.selected, this.topicSearch});

  @override
  State<ListCoursePage> createState() => _ListCoursePageState();
}

class _ListCoursePageState extends State<ListCoursePage> {
  late ListCourseCubit _cubit;
  RefreshController _refreshController = RefreshController();
  RefreshController _refreshTopicController = RefreshController();
  TextEditingController _searchController = TextEditingController();
  TextEditingController _searchTopicController = TextEditingController();
  bool searchingTopic = false;
  List<ProductTopicDataFetchProductTopicsData?> productTopic1 = [];

  bool hideAppBar = false;
  String _selectedMenu = R.string.sort_by_popularity.tr();
  late double _appBarHeight;
  AppGraphqlRepository graphqlRepository = AppGraphqlRepository();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    _cubit = ListCourseCubit(repository, graphqlRepository,widget.topicSearch??[]);
    _cubit.getProductTopic();
    // productTopic1 = _cubit.productTopic;
    _appBarHeight = AppBar().preferredSize.height;
    if (widget.selected == 4) {
      _cubit.changePopMenuTitle(4);
      _cubit.radioChoice.value[0] = false;
      _cubit.radioChoice.value[4] = true;
      _cubit.searchCourse(keyWord: _searchController.text.trim(), isHot: true);
    } else if (widget.selected == 5) {
      _cubit.isSelect=5;
      _cubit.changePopMenuTitle(5);
      _cubit.radioChoice.value[0] = false;
      _cubit.radioChoice.value[5] = true;
      _cubit.searchCourse(
          keyWord: _searchController.text.trim(), topicIds: widget.topicSearch);
    } else {
      _cubit.searchCourse(priorityOrder: Order.DESC.name);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _searchTopicController.dispose();
    _searchController.dispose();
    _refreshController.dispose();
    _cubit.close();
    super.dispose();
  }

  get() async {
    _cubit.getProductTopic(searchKey: _searchTopicController.text.trim());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (BuildContext context) => _cubit,
        child: BlocConsumer<ListCourseCubit, ListCourseState>(
          listener: (BuildContext context, state) {
            if (state is ListCourseFailure) {
              Utils.showErrorSnackBar(context, state.error);
            }
            if (state is! ListCourseLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (BuildContext context, ListCourseState state) {
            return GestureDetector(
                onTap: () => Utils.hideKeyboard(context),
                child: buildPage(context, state));
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, ListCourseState state) {
    return Scaffold(
        backgroundColor: R.color.lightestGray,
        body: StackLoadingView(
          visibleLoading: state is ListCourseLoading,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: MediaQuery.of(context).padding.top,
                width: double.infinity,
                color: R.color.primaryColor,
              ),
              _appBar(),
              Padding(
                padding: widget.codeCoupons == null
                    ? EdgeInsets.symmetric(horizontal: 16.w, vertical: 12.h)
                    : EdgeInsets.only(left: 16.w, right: 16.w, top: 12.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        _cubit.selectedMenu,
                        style: Theme.of(context).textTheme.medium500.copyWith(
                            fontSize: 16.sp,
                            color: R.color.neutral1,
                            height: 20 / 16),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    SizedBox(width: 10.w),
                    buildPopupMenu(state),
                  ],
                ),
              ),
              if (widget.codeCoupons != null) ...[
                Padding(
                  padding: EdgeInsets.only(left: 16.w, bottom: 10.h),
                  child: Text(
                    "${R.string.discount_code.tr()}" " ${widget.codeCoupons}",
                    textAlign: TextAlign.start,
                    style: Theme.of(context)
                        .textTheme
                        .regular400
                        .copyWith(fontSize: 10.sp, color: R.color.gray),
                  ),
                )
              ],
              Expanded(
                child: RefreshConfiguration(
                  enableLoadingWhenNoData: false,
                  enableScrollWhenRefreshCompleted: false,
                  child: SmartRefresher(
                    enablePullUp: _cubit.nextToken != null &&
                        state is SearchListCourseSuccess,
                    controller: _refreshController,
                    onRefresh: () {
                      if (_cubit.isSelect == 0 &&
                          widget.selected != 4 &&
                          widget.selected != 5) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priorityOrder: Order.DESC.name,
                            isRefresh: true);
                      } else if (_cubit.isSelect == 1) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            isRefresh: true);
                      } else if (_cubit.isSelect == 2) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priceOrder: Order.ASC.name,
                            isRefresh: true);
                      } else if (_cubit.isSelect == 3) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priceOrder: Order.DESC.name,
                            isRefresh: true);
                      } else if (_cubit.isSelect == 4 || widget.selected == 4) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            isHot: true,
                            isRefresh: true);
                      } else if (_cubit.isSelect == 5 || widget.selected == 5) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            topicIds: widget.selected == 5
                                ? widget.topicSearch
                                : _cubit.topicSearch,
                            isRefresh: true);
                      }
                    },
                    onLoading: () {
                      if (_cubit.isSelect == 0 &&
                          widget.selected != 4 &&
                          widget.selected != 5) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priorityOrder: Order.DESC.name,
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 1) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 2) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priceOrder: Order.ASC.name,
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 3) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priceOrder: Order.DESC.name,
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 4 || widget.selected == 4) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            isHot: true,
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 5 || widget.selected == 5) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            topicIds: widget.selected == 5
                                ? widget.topicSearch
                                : _cubit.topicSearch,
                            isLoadMore: true);
                      }
                    },
                    child: buildSearchListCourse(context, state),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget buildSearchListCourse(BuildContext context, ListCourseState state) {
    return state is ListCourseLoading
        ? const SizedBox.shrink()
        : _cubit.listSearchCourse.length == 0 && state is ListSearchCourseEmpty
            ? Center(
                child: Text(
                  R.string.no_result_search.tr(),
                  style: Theme.of(context).textTheme.bodyRegular,
                ),
              )
            :      GridView.builder(
        primary: true,
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        physics:const BouncingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 16.h,
            crossAxisSpacing: 12.h,
            childAspectRatio: 17 / 20),
        itemCount: _cubit.listSearchCourse.length,
        itemBuilder: (BuildContext ctx, index) {
          FetchCoursesData data = _cubit.listSearchCourse[index];
          return GestureDetector(
            onTap: () {
              NavigationUtils.rootNavigatePage(
                  context,
                  CourseDetailsPage(
                    courseId: data.id,
                    productId: data.productId ?? 0,
                  ));
            },
            child: ItemCourse(
                url: data.avatarUrl ?? '',
                name: data.name ?? '',
                price: "${data.currentFee ?? 0}",
                discount: "${data.preferentialFee ?? 0}",
                isHot: data.isHot ?? false),
          );
        });
  }

  Widget buildPopupMenu(ListCourseState state) {
    return PopupMenuButton<PopMenu>(
        offset: Offset(0, 32.h),
        constraints: BoxConstraints(minWidth: context.width * 0.55),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: R.color.hintGray),
          borderRadius: BorderRadius.circular(10),
        ),
        onSelected: (PopMenu item) {
          setState(() {
            // _selectedMenu = item.title();
            // _cubit.onOrder();
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.h),
              color: _cubit.isOrder ? R.color.primaryColor : R.color.white),
          padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 20.h),
          child: Image.asset(
            R.drawable.ic_filter_list,
            height: 20.h,
            color: _cubit.isOrder ? R.color.white : R.color.primaryColor,
          ),
        ),
        itemBuilder: (BuildContext context) => List.generate(
              6,
              (index) {
                return PopupMenuItem<PopMenu>(
                  enabled: false,
                  value: PopMenu.values[index],
                  child: StatefulBuilder(
                    builder:
                        (BuildContext context, StateSetter setStateRadio) =>
                            ValueListenableBuilder(
                      valueListenable: _cubit.radioChoice,
                      builder: (context, List<bool> value, _) => Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              _cubit.selectFilter(index);
                            },
                            child: Row(
                              children: [
                                Radio(
                                  value: _cubit.radioChoice.value[index],
                                  groupValue: true,
                                  onChanged: (value) {
                                    setStateRadio(() {
                                      _cubit.selectFilter(index);
                                    });
                                  },
                                ),
                                Text(
                                  _cubit.title[index],
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 14.sp,
                                          color: _cubit.radioChoice.value[index]
                                              ? R.color.primaryColor
                                              : R.color.black),
                                ),
                                Spacer(),
                                index == 5
                                    ? Expanded(
                                        child: _cubit.isSelect == 5
                                            ? Icon(Icons.arrow_drop_up_rounded)
                                            : Icon(
                                                Icons.arrow_drop_down_rounded))
                                    : const SizedBox.shrink()
                              ],
                            ),
                          ),
                          _cubit.isSelect == 5 && index == 5
                              ? Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, bottom: 10),
                                    child: Text(
                                      R.string.choose_suggested_topic.tr(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelLargeText
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 5 && index == 5
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                    width: context.width * 0.7,
                                    child: TextField(
                                      maxLines: 1,
                                      controller: _searchTopicController,
                                      style: Theme.of(context)
                                          .textTheme
                                          .regular400,
                                      decoration: InputDecoration(
                                        hintText: R.string.search.tr(),
                                        hintStyle: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(
                                                color: R.color.disableGray),
                                        border: InputBorder.none,
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: R.color.disableGray),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 1,
                                              color: R.color.disableGray),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setStateRadio(() {
                                              get();
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                            });
                                          },
                                          icon: Icon(Icons.search),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 5 && index == 5
                              ? ValueListenableBuilder(
                                  valueListenable: _cubit.loadingSearch,
                                  builder: (context, value, _) => _cubit
                                          .loadingSearch.value
                                      ? CircularProgressIndicator()
                                      : ConstrainedBox(
                                          constraints: BoxConstraints(
                                              maxWidth: context.width * 0.7,
                                              maxHeight: 140,
                                              minHeight: 50),
                                          child: Scrollbar(
                                            child: NotificationListener<ScrollUpdateNotification>(
                                              onNotification: (notification) {
                                                //How many pixels scrolled from pervious frame
                                                if(notification.metrics.atEdge){
                                                  if(notification.metrics.pixels!=0&&state is GetTopicSuccess){
                                                    //Loadmore
                                                  }
                                                }
                                                   return true;
                                              },
                                              child: SingleChildScrollView(

                                                physics:
                                                    const BouncingScrollPhysics(),
                                                child: _cubit.productTopic.isEmpty
                                                    ? Center(
                                                        child: Padding(
                                                        padding:
                                                            const EdgeInsets.only(
                                                                top: 5),
                                                        child: Text(
                                                          R.string.not_exist.tr(),
                                                          style: Theme.of(context)
                                                              .textTheme
                                                              .regular400,
                                                        ),
                                                      ))
                                                    : Wrap(
                                                        children:
                                                            _cubit.productTopic
                                                                .asMap()
                                                                .entries
                                                                .map((e) =>
                                                                    ChoiceChip(
                                                                      avatar: _cubit
                                                                              .choiceChip[e.key]
                                                                          ? Icon(
                                                                              Icons.check,
                                                                              color:
                                                                                  R.color.zaffre,
                                                                            )
                                                                          : null,
                                                                      backgroundColor: R
                                                                          .color
                                                                          .white,
                                                                      selectedColor: R
                                                                          .color
                                                                          .white,
                                                                      label:
                                                                          Padding(
                                                                        padding: EdgeInsets.only(
                                                                            bottom:
                                                                                1),
                                                                        child:
                                                                            Text(
                                                                          e.value?.topicName ??
                                                                              '',
                                                                          style: Theme.of(context)
                                                                              .textTheme
                                                                              .subTitleRegular
                                                                              .copyWith(color: _cubit.choiceChip[e.key] ? R.color.zaffre : R.color.grey),
                                                                        ),
                                                                      ),
                                                                      selected: _cubit
                                                                              .choiceChip[
                                                                          e.key],
                                                                      shape: StadiumBorder(
                                                                          side: BorderSide(
                                                                              width:
                                                                                  1,
                                                                              color: _cubit.choiceChip[e.key]
                                                                                  ? R.color.zaffre
                                                                                  : R.color.grey)),
                                                                      onSelected:
                                                                          (bool
                                                                              selected) {
                                                                        setStateRadio(
                                                                            () {
                                                                          _cubit.choiceChip[e.key] =
                                                                              selected;
                                                                        });
                                                                      },
                                                                    ))
                                                                .toList(),
                                                spacing: 10,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                )
                              : const SizedBox.shrink(),
                          index == 5
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      ButtonWidget(
                                        backgroundColor: R.color.brightRed,
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            12.w, 8.h, 12.w, 8.h),
                                        title: R.string.clear_filter.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          _cubit.changePopMenuTitle(0);
                                          _cubit.selectFilter(0);
                                          _cubit.clearFilter();
                                          _searchTopicController.clear();
                                          _searchController.clear();
                                          _cubit.searchCourse(
                                              priorityOrder: Order.DESC.name,
                                              isRefresh: true);
                                          _cubit.getProductTopic();
                                          NavigationUtils.pop(context);
                                        },
                                      ),
                                      const SizedBox(
                                        width: 15,
                                      ),
                                      ButtonWidget(
                                        backgroundColor: R.color.primaryColor,
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            12.w, 8.h, 12.w, 8.h),
                                        title: R.string.apply_code.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          _cubit.changePopMenuTitle(
                                              _cubit.isSelect);
                                          _cubit.isSelect == 5
                                              ? _cubit.getSearchTopic()
                                              : null;
                                          widget.selected=0;
                                          _cubit.searchCourse(
                                              keyWord:
                                                  _searchController.text.trim(),
                                              priorityOrder:
                                                  _cubit.isSelect == 0
                                                      ? Order.DESC.name
                                                      : null,
                                              isHot: _cubit.isSelect == 4
                                                  ? true
                                                  : null,
                                              priceOrder: _cubit.isSelect == 2
                                                  ? Order.ASC.name
                                                  : _cubit.isSelect == 3
                                                      ? Order.DESC.name
                                                      : null,
                                              topicIds: _cubit.isSelect == 5
                                                  ? _cubit.topicSearch
                                                  : [],
                                              isRefresh: true);
                                          NavigationUtils.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                )
                              : const SizedBox.shrink()
                        ],
                      ),
                    ),
                  ),
                );
              },
            ));
  }

  Widget _appBar() {
    return _cubit.isSearch
        ? Container(
            height: _appBarHeight,
            width: double.infinity,
            decoration: BoxDecoration(color: R.color.primaryColor, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.h), blurRadius: 1.h, color: R.color.gray)
            ]),
            child: Row(
              children: [
                SizedBox(
                  width: 16.w,
                ),
                GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: R.color.white,
                    size: 20.h,
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Text(
                      R.string.course.tr().toUpperCase(),
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 20.sp, color: R.color.white),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _cubit.checkSearch();
                  },
                  child: Image.asset(R.drawable.ic_search,
                      width: 20.h, height: 20.h, color: R.color.white),
                ),
                SizedBox(width: 16.w),
              ],
            ),
          )
        : Stack(
            children: [
              Container(
                width: double.infinity,
                height: _appBarHeight,
                decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 2.h),
                          blurRadius: 1.h,
                          color: R.color.gray)
                    ]),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.h, top: 5.h),
                    child: GestureDetector(
                      onTap: () {
                        NavigationUtils.pop(context);
                      },
                      child: Icon(CupertinoIcons.back, color: R.color.white),
                    ),
                  ),
                  Expanded(
                    child: SearchWidget(
                      onSubmit: (text) {
                        _cubit.searchCourse(
                            keyWord: _searchController.text.trim(),
                            priorityOrder:
                                _cubit.isSelect == 0 ? Order.DESC.name : null,
                            isHot: widget.selected == 4
                                ? true
                                : (_cubit.isSelect == 4 ? true : null),
                            priceOrder: _cubit.isSelect == 2
                                ? Order.ASC.name
                                : Order.DESC.name,
                            topicIds: widget.selected == 5
                                ? widget.topicSearch
                                : (_cubit.isSelect == 5
                                    ? _cubit.topicSearch
                                    : []),
                            isRefresh: true);
                      },
                      searchController: _searchController,
                      type: Const.COURSE,
                      onChange: (String) {},
                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
