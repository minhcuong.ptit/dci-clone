
abstract class ListCourseState {}

class ListCourseInitial extends ListCourseState {}

class ListCourseLoading extends ListCourseState {
  @override
  String toString() {
    return 'ListCourseLoading{}';
  }
}

class getPopupMenuTitle extends ListCourseState {
  @override
  String toString() {
    return 'getPopupMenuTitle{}';
  }
}

class ListTopicLoading extends ListCourseState {
  @override
  String toString() {
    return 'ListTopicLoading{}';
  }
}

class ListCourseFailure extends ListCourseState {
  final String error;

  ListCourseFailure(this.error);

  @override
  String toString() {
    return 'ListCourseFailure{error: $error}';
  }
}

class ListCourseSuccess extends ListCourseState {
  @override
  String toString() {
    return 'ListCourseSuccess{}';
  }
}

class ListTopicSuccess extends ListCourseState {
  @override
  String toString() {
    return 'ListTopicSuccess{}';
  }
}

class SearchListCourseSuccess extends ListCourseState {
  @override
  String toString() {
    return 'SearchListCourseSuccess{}';
  }
}

class ListSearchCourseEmpty extends ListCourseState {
  @override
  String toString() {
    return 'ListSearchCourseEmpty{}';
  }
}

class GetTopicSuccess extends ListCourseState {
  @override
  String toString() {
    return 'GetTopicSuccess{}';
  }
}