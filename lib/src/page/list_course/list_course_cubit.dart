import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/database/app_database.dart';
import 'package:imi/src/data/database/sqflite/DBHelper.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/list_course/list_course_state.dart';

import '../../../res/R.dart';
import '../../data/network/response/course_topics_response.dart';
import '../../data/network/response/product_topic_response.dart';

class ListCourseCubit extends Cubit<ListCourseState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool isSearch = true;
  List<FetchCoursesData> listCourse = [];
  List<FetchCoursesData> listSearchCourse = [];
  List<CourseTopicsDataFetchCourseTopicsData?> productTopic = [];
  List<CourseTopicsDataFetchCourseTopicsData?> productTopicTemp = [];
  ValueNotifier<List<bool>> radioChoiceTemp =
      ValueNotifier([true, false, false, false, false, false]);
  ValueNotifier<List<bool>> radioChoice =
      ValueNotifier([true, false, false, false, false, false]);
  ValueNotifier<bool> loadingSearch = ValueNotifier(false);
  String selectedMenu = R.string.sort_by_popularity.tr();
  late List<bool> choiceChip;
  List<int> topicSearch = [];
  List<String> title = [
    R.string.sort_by_popularity.tr(),
    R.string.lastest.tr(),
    R.string.low_to_high.tr(),
    R.string.high_to_low.tr(),
    R.string.outstanding.tr(),
    R.string.similar.tr()
  ];
  String? nextToken;
  String? nextTokenSearch;
  bool hasMorePage = true;
  bool isOrder = false;
  int isSelect = 0;
  List<int> topicSearchSimilar = [];

  ListCourseCubit(this.repository, this.graphqlRepository,this.topicSearchSimilar)
      : super(ListCourseInitial());

  void checkSearch() {
    emit(ListCourseLoading());
    this.isSearch = !isSearch;
    emit(ListCourseInitial());
  }


  void searchCourse(
      {bool isRefresh = false,
      bool isLoadMore = false,
      String? type,
      String? keyWord,
      bool? isHot,
      String? priorityOrder,
      List<int>? topicIds,
      String? priceOrder,
      int?couponId}) async {
    emit((isRefresh||!isLoadMore) ? ListCourseLoading() : ListCourseInitial());
    // emit(isLoadMore ? ListCourseInitial() : ListCourseLoading());
    if (isRefresh) {
      nextToken = null;
      listSearchCourse.clear();
    }
    ApiResult<FetchCourse> getSearchCourse = await graphqlRepository.getCourse(
        isHot: isHot,
        nextToken: nextToken,
        searchKey: keyWord != "\\" ? keyWord : keyWord?.replaceAll("\\", "\t"),
        priorityOrder: priorityOrder,
        topicIds: topicIds,
        priceOrder: priceOrder,
    couponId:couponId );
    getSearchCourse.when(success: (FetchCourse data) async {
      if (data.data?.length != 0) {
        listSearchCourse.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(SearchListCourseSuccess());
      } else {
        emit(ListSearchCourseEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(ListCourseFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getProductTopic({
    String? searchKey,
  }) async {
    loadingSearch.value = true;
    ApiResult<CourseTopicsData> getSearchCourse =
        await graphqlRepository.getCourseTopics(searchKey: searchKey);
    getSearchCourse.when(success: (CourseTopicsData data) async {
      topicSearch.clear();
      productTopic.clear();
      productTopicTemp.clear();
      productTopic.addAll(data.fetchCourseTopics?.data ?? []);
      productTopicTemp.addAll(data.fetchCourseTopics?.data ?? []);
      choiceChip = List.generate(productTopic.length, (index) => false);
      loadingSearch.value = false;
      for (int i = 0; i < productTopic.length; i++) {
        for (int j = 0; j < topicSearchSimilar.length; j++) {
          if (productTopic[i]?.id == topicSearchSimilar[j]) {
            choiceChip.add(choiceChip[i] = true);
          }
        }
      }
      emit(GetTopicSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(ListCourseFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void onOrder() {
    // emit(ListCourseLoading());
    // isOrder = true;
    // emit(ListCourseSuccess());
  }

  void selectFilter(int index) {
    emit(ListCourseLoading());
    isSelect = index;
    // selectedMenu=title[index];
    for (int i = 0; i < radioChoiceTemp.value.length; i++) {
      if (index == i) {
        radioChoiceTemp.value[i] = true;
        continue;
      }
      radioChoiceTemp.value[i] = false;
    }
    radioChoice.value = List.from(radioChoiceTemp.value);
    emit(ListCourseInitial());
  }

  void changePopMenuTitle(int index) {
    emit(getPopupMenuTitle());
    selectedMenu=title[index];
    emit(ListCourseInitial());
  }

  void getSearchTopic() {
    topicSearch.clear();
    for (int i = 0; i < productTopic.length; i++) {
      if (choiceChip[i]) {
        topicSearch.add(productTopic[i]?.id ?? 0);
      }
    }
  }

  void clearFilter() {
    topicSearchSimilar.clear();
    choiceChip = choiceChip.map((e) => e = false).toList();
  }
}
