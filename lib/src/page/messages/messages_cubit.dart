import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/messages/messages_state.dart';
import 'package:pubnub/pubnub.dart';

import '../../data/network/response/list_joined_channel_response.dart';
import '../../data/network/response/user_channel_group_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import 'package:imi/src/utils/app_config.dart' as config;

class MessagesCubit extends Cubit<MessagesState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;

  String? get userId => appPreferences.getString(Const.ID);

  String? nextToken;
  List<JoinedChannelsData> listJoinedChannels = [];
  late final PubNub _pubNub;
  late final ChannelGroupDx? _chatChannel;
  int count = 0;
  bool checkLoadMore = false;

  MessagesCubit(this.repository, this.graphqlRepository)
      : super(MessagesInitial());

  void getListJoinedChannel(
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit(isRefresh || !isLoadMore ? MessagesLoading() : MessagesInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listJoinedChannels.clear();
    }
    ApiResult<FetchChatChannels> result =
        await graphqlRepository.getListChatChannel(nextToken: nextToken);
    result.when(success: (FetchChatChannels data) async {
      if (data.data != null) {
        listJoinedChannels.addAll(data.data ?? []);
        nextToken = data.nextToken;
        for (int i = 0; i < listJoinedChannels.length; i++) {
          getMessage(
                  channels: listJoinedChannels[i].channelPubnubId ?? "",
                  timeToken: Timetoken(BigInt.from(
                      listJoinedChannels[i].lastReadTimeToken ?? 0)),
                  data: listJoinedChannels[i]);
        }
      }
      emit(MessagesSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(MessagesFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void initChannel(List<UserChannelGroups> channelId) {
    _pubNub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: config.AppConfig.environment.pubNubSubscribeKey,
            publishKey: config.AppConfig.environment.pubNubPublishKey,
            userId: UserId(appPreferences.getString(Const.ID) ?? "")));
    _chatChannel = _pubNub.channelGroups;
    for (int i = 0; i < channelId.length; i++) {
      _pubNub
          .subscribe(channelGroups: {channelId[i].channelGroupsPubnubId ?? ""})
          .messages
          .listen((event) async {
            if (_chatChannel != null) {
              nextToken = null;
              await Future.delayed(Duration(milliseconds: 200));
              updateListJoinedChannel(event.channel, event.publishedAt);
              checkLoadMore = true;
            }
          });
    }
  }

  void updateListJoinedChannel(String channelId, Timetoken timeToken) async {
    ApiResult<FetchChatChannels> result =
        await graphqlRepository.getListChatChannel();
    result.when(success: (FetchChatChannels data) async {
      listJoinedChannels = data.data ?? [];
      for (int i = 0; i < listJoinedChannels.length; i++) {
        getMessage(
            channels: listJoinedChannels[i].channelPubnubId ?? "",
            timeToken: Timetoken(
                BigInt.from(listJoinedChannels[i].lastReadTimeToken ?? 0)),
            data: listJoinedChannels[i]);
      }
      emit(NewMessagesSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(MessagesFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  getMessage(
      {String? channels,
      String subKey = Const.SUBSCRIBE_KEY_STG,
      Timetoken? timeToken,
      JoinedChannelsData? data}) async {
    emit(MessagesLoading());
    await Dio()
        .get(
            "https://ps.pndsn.com/v3/history/sub-key/${config.AppConfig.environment == config.Environment.PRODUCTION ? Const.SUBSCRIBE_KEY_PROD : Const.SUBSCRIBE_KEY_STG}/message-counts/$channels"
            "?channelsTimetoken=$timeToken")
        .then((value) {
      data?.unReadCount = jsonDecode(value.data)['channels']['${channels}'];
    });
    emit(NewMessagesLoading());
  }
}
