abstract class MessagesState {}

class MessagesInitial extends MessagesState {
  @override
  String toString() => 'MessagesInitial';
}

class MessagesSuccess extends MessagesState {
  @override
  String toString() => 'MessagesSuccess';
}

class MessagesLoading extends MessagesState {
  @override
  String toString() => 'MessagesLoading';
}

class MessagesFailure extends MessagesState {
  final String error;

  MessagesFailure(this.error);

  @override
  String toString() => 'MessagesFailure { error: $error }';
}

class NewMessagesSuccess extends MessagesState {
  @override
  String toString() => 'NewMessagesSuccess';
}

class NewMessagesLoading extends MessagesState {
  @override
  String toString() => 'NewMessagesLoading';
}
