import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/list_joined_channel_response.dart';
import 'package:imi/src/data/network/response/user_channel_group_response.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/messages/messages.dart';
import 'package:imi/src/page/search_user_chat/search_user_chat_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/time_ago.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import 'package:imi/src/widgets/custom_appbar_messages_widget.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pubnub/pubnub.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../utils/const.dart';

class MessagesPage extends StatefulWidget {
  final List<UserChannelGroups> channelGroupsPubnubId;

  MessagesPage({required this.channelGroupsPubnubId});

  @override
  State<MessagesPage> createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  late MessagesCubit _cubit;
  late MainCubit _mainCubit;
  RefreshController _refreshController = RefreshController();
  bool onPop = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _mainCubit = GetIt.I();
    _cubit = MessagesCubit(repository, graphqlRepository);
    _cubit.getListJoinedChannel();
      _cubit.initChannel(widget.channelGroupsPubnubId);

  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: R.color.primaryColor,
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: SafeArea(
          top: true,
          child: WillPopScope(
            onWillPop: () async {
              NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
              return true;
            },
            child: BlocProvider(
              create: (context) => _cubit,
              child: BlocConsumer<MessagesCubit, MessagesState>(
                listener: (context, state) {
                  if (state is! MessagesLoading) {
                    _refreshController.refreshCompleted();
                    _refreshController.loadComplete();
                  }
                  // TODO: implement listener
                },
                builder: (context, state) {
                  return SafeArea(
                      top: true,
                      child: Scaffold(
                          appBar: appBarMessages(
                            context,
                            onSelectedIcon: () {
                              NavigationUtils.navigatePage(context, MainPage());
                              _mainCubit.selectTab(0);
                            },
                            rightWidget: SizedBox(),
                          ),
                          body: buildPage(context, state)));
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, MessagesState state) {
    return StackLoadingView(
      visibleLoading: state is MessagesLoading,
      child: SmartRefresher(
        controller: _refreshController,
        enablePullUp: true,
        onRefresh: () {
          _cubit.getListJoinedChannel(isRefresh: true);
          _cubit.checkLoadMore = false;
        },
        onLoading: _cubit.checkLoadMore == true
            ? null
            : () {
                _cubit.getListJoinedChannel(isLoadMore: true);
              },
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
          children: [
            InkWell(
              onTap: () {
                NavigationUtils.navigatePage(context, SearchUserChatPage());
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.h),
                    color: R.color.grey100),
                padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 20.w),
                child: Row(
                  children: [
                    Icon(CupertinoIcons.search,
                        size: 20.h, color: R.color.black),
                    SizedBox(width: 10.w),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 2.5),
                      child: Text(
                        R.string.search.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .regular400
                            .copyWith(fontSize: 13, color: R.color.grey),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 20.h),
            ListView.separated(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: NeverScrollableScrollPhysics(),
              itemCount: _cubit.listJoinedChannels.length,
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(height: 15.h);
              },
              itemBuilder: (BuildContext context, int index) {
                JoinedChannelsData data = _cubit.listJoinedChannels[index];
                return InkWell(
                    highlightColor: R.color.white,
                    onTap: () {
                      NavigationUtils.navigatePage(
                          context,
                          ChatPage(
                            chatType: data.userUuid != null
                                ? ChatType.private
                                : ChatType.group,
                            channelId: data.channelPubnubId,
                            channelName: data.communityName,
                            userUID: data.userUuid,
                            communityId: data.communityId,
                            checkPageHomeChat: true,
                          )).then((value) {
                        _cubit.getListJoinedChannel();
                        _cubit.checkLoadMore = true;
                      });
                    },
                    child: buildMessages(context, data));
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMessages(BuildContext context, JoinedChannelsData data) {
    return Row(
      children: [
        buildAvatarWidget(data),
        SizedBox(width: 10.w),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildNameUser(context, data),
              SizedBox(height: 4.h),
              // buildContentMessages(context, data)
            ],
          ),
        )
      ],
    );
  }

  // Widget buildContentMessages(BuildContext context, JoinedChannelsData data) {
  //   String? contentMessages = data.lastMessage?.message?['content'];
  //   List<dynamic>? imageUrl = data.lastMessage?.message?['images'];
  //   String? contentImage = R.string.sent_image.tr();
  //   List<dynamic>? file = data.lastMessage?.message?['files'];
  //   String? contentFile = R.string.sent_file.tr();
  //   String? sendBy = data.lastMessage?.message?['sendBy'];
  //   String? communityName = data.communityName ?? "";
  //   if (_cubit.userId == sendBy) {
  //     if (contentMessages != "" && contentMessages != null) {
  //       contentMessages = "${R.string.by_send.tr()}: " + "${contentMessages}";
  //     } else if (imageUrl?.length != 0) {
  //       contentImage = "${R.string.by_send.tr()} " + contentImage;
  //     } else if (file?.length != 0) {
  //       contentFile = "${R.string.by_send.tr()} " + contentFile;
  //     }
  //   } else {
  //     if (contentMessages != "" && contentMessages != null) {
  //       contentMessages = contentMessages;
  //     } else if (imageUrl?.length != 0) {
  //       contentImage = "${communityName} " + contentImage;
  //     } else if (file?.length != 0) {
  //       contentFile = "${communityName} " + contentFile;
  //     }
  //   }
  //   return Row(
  //     children: [
  //       Expanded(
  //         child: Text(
  //           data.lastMessage?.message != null
  //               ? ((contentMessages != "" && contentMessages != null)
  //                   ? contentMessages
  //                   : (imageUrl?.length != 0)
  //                       ? contentImage
  //                       : (file?.length != 0)
  //                           ? contentFile
  //                           : R.string.send_a_message_to_start_a_conversation
  //                               .tr())
  //               : R.string.send_a_message_to_start_a_conversation.tr(),
  //           style: Theme.of(context).textTheme.bold700.copyWith(
  //               fontSize: 12.sp,
  //               fontWeight:
  //                   data.lastMessage?.isRead == true || _cubit.userId == sendBy
  //                       ? FontWeight.w400
  //                       : FontWeight.w700,
  //               height: 16.h / 12.sp),
  //           maxLines: 1,
  //           overflow: TextOverflow.ellipsis,
  //         ),
  //       ),
  //       SizedBox(width: 5.w),
  //       Visibility(
  //         visible: data.unReadCount != 0,
  //         child: Container(
  //           height: 16.h,
  //           width: 18.w,
  //           decoration: BoxDecoration(
  //               borderRadius: BorderRadius.circular(100),
  //               color: R.color.orange),
  //           alignment: Alignment.center,
  //           child: Text(
  //             "${data.unReadCount! > 9 ? "9+" : data.unReadCount}",
  //             style: Theme.of(context)
  //                 .textTheme
  //                 .regular400
  //                 .copyWith(fontSize: 10.sp, color: R.color.white, height: 1),
  //           ),
  //         ),
  //       )
  //     ],
  //   );
  // }

  Widget buildNameUser(BuildContext context, JoinedChannelsData data) {
    return Row(
      children: [
        Expanded(
          child: Text(
            data.communityName ?? "",
            style: Theme.of(context).textTheme.regular400.copyWith(
                fontSize: 14.sp,
                fontWeight: FontWeight.w400,
                height: 20.h / 14.sp),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        SizedBox(width: 5.w),
        Visibility(
          visible: data.unReadCount != 0,
          child: data.unReadCount != null
              ? Container(
                  height: 16.h,
                  width: 18.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: R.color.orange),
                  alignment: Alignment.center,
                  child: Text(
                    "${data.unReadCount == null ? "" : data.unReadCount! > 9 ? "9+" : data.unReadCount}",
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 10.sp, color: R.color.white, height: 1),
                  ),
                )
              : SizedBox.shrink(),
        ),
        SizedBox(width: 5.w),
        data.lastMessage?.timeToken != null
            ? Text(
                TimeAgo.timeAgoSinceDateChat(
                  Timetoken(BigInt.from(data.lastMessage?.timeToken ?? 0))
                      .toDateTime()
                      .toLocal(),
                ),
                textAlign: TextAlign.end,
                style: Theme.of(context)
                    .textTheme
                    .labelMarco
                    .copyWith(color: R.color.darkGray))
            : SizedBox.shrink(),
      ],
    );
  }

  Widget buildAvatarWidget(JoinedChannelsData data) {
    return AvatarWidget(
        size: 56.h,
        avatar: data.communityImage != null
            ? data.communityImage
            : R.drawable.ic_person_dci);
  }
}
