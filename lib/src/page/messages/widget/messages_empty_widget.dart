import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../../../widgets/custom_appbar_messages_widget.dart';

class MessagesEmptyWidget extends StatefulWidget {
  const MessagesEmptyWidget({Key? key}) : super(key: key);

  @override
  State<MessagesEmptyWidget> createState() => _MessagesEmptyWidgetState();
}

class _MessagesEmptyWidgetState extends State<MessagesEmptyWidget> {
  late MainCubit _mainCubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _mainCubit = GetIt.I();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: R.color.primaryColor,
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: SafeArea(
          top: true,
          child: Scaffold(
            appBar: appBarMessages(
              context,
              rightWidget: InkWell(
                onTap: () {},
                child: Image.asset(
                  R.drawable.ic_headphones,
                  height: 22.h,
                  width: 22.h,
                ),
              ),
              onSelectedIcon: () {
                NavigationUtils.navigatePage(context, MainPage());
                _mainCubit.selectTab(0);
              },
            ),
            body: Center(
              child: Column(
                children: [
                  SizedBox(height: 100.h),
                  Image.asset(
                    R.drawable.ic_messages_empty,
                    height: 140.h,
                    width: 140.w,
                  ),
                  SizedBox(height: 10.h),
                  Text(
                    R.string.you_do_not_have_a_message.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.regular400,
                  ),
                  Text(
                    R.string.start_message_now.tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.regular400,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
