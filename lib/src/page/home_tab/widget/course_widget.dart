import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/course_details/course_details_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../../../../res/R.dart';

class CourseWidget extends StatefulWidget {
  @override
  _CourseWidgetState createState() => _CourseWidgetState();
}

class _CourseWidgetState extends State<CourseWidget> {
  var imageUrl =
      'https://margaretcruises.com/wp-content/uploads/2022/03/Rectangle-39456.png';

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      precacheImage(NetworkImage(imageUrl), context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _headerMaxHeight =
        (MediaQuery.of(context).size.width - 48.w) * (169.h / 312.w);
    return Padding(
      padding: EdgeInsets.fromLTRB(24.w, 24.h, 24.w, 0.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(R.string.course.tr().toUpperCase(),
                  style: Theme.of(context)
                      .textTheme
                      .labelSmallText
                      .apply(color: R.color.black)
                      .copyWith(fontSize: 14.sp, fontWeight: FontWeight.w500)),
              Spacer(),
              GestureDetector(
                onTap: () {
                  NavigationUtils.rootNavigatePage(
                      context, CourseDetailsPage());
                },
                child: Text(R.string.read_more.tr(),
                    style: Theme.of(context)
                        .textTheme
                        .labelSmallText
                        .apply(color: R.color.readMore)
                        .copyWith(fontSize: 14.sp)),
              ),
            ],
          ),
          SizedBox(
            height: 24.h,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(8.w),
            child: Container(
              height: _headerMaxHeight,
              child: Center(
                  child: Image.network(
                imageUrl,
                fit: BoxFit.cover,
                width: double.infinity,
              )),
            ),
          ),
          SizedBox(
            height: 16.h,
          ),
          Text('Năng đoạn kim cương 1'.toUpperCase(),
              style: Theme.of(context)
                  .textTheme
                  .labelSmallText
                  .apply(color: R.color.black)
                  .copyWith(fontSize: 14.sp, fontWeight: FontWeight.w700)),
          Text('Thầy Jone',
              style: Theme.of(context)
                  .textTheme
                  .labelSmallText
                  .apply(color: R.color.contentGrey)
                  .copyWith(fontSize: 14.sp, fontWeight: FontWeight.w400)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(R.drawable.ic_people, width: 16.w, height: 16.w),
              SizedBox(
                width: 8.w,
              ),
              Text('400 ${R.string.student.tr()}',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmallText
                      .apply(color: R.color.contentGrey)
                      .copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          height: 1.67)),
              SizedBox(
                width: 24.w,
              ),
              Image.asset(R.drawable.ic_time, width: 16.w, height: 16.w),
              SizedBox(
                width: 8.w,
              ),
              Text('3 ${R.string.hour.tr()}',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmallText
                      .apply(color: R.color.contentGrey)
                      .copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                          height: 1.67)),
            ],
          )
        ],
      ),
    );
  }
}
