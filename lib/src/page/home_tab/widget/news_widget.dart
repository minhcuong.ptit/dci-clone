import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../res/R.dart';

class NewsWidget extends StatefulWidget {
  final String title;

  const NewsWidget({Key? key, required this.title}) : super(key: key);
  @override
  _NewsWidgetState createState() => _NewsWidgetState();
}

class _NewsWidgetState extends State<NewsWidget> {
  var imageUrls = [
    'https://margaretcruises.com/wp-content/uploads/2022/03/Rectangle-39457.png',
    'https://margaretcruises.com/wp-content/uploads/2022/03/Rectangle-39458.png'
  ];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      imageUrls.forEach((element) {
        precacheImage(NetworkImage(element), context);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _maxHeight =
        (MediaQuery.of(context).size.width / 2 - 48.w) * (103.h / 148.w);
    return Padding(
      padding: EdgeInsets.fromLTRB(24.w, 24.h, 24.w, 0),
      child: Column(
        children: [
          Row(
            children: [
              Text(widget.title.toUpperCase(),
                  style: TextStyle(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w500,
                    color: R.color.black,
                  )),
              Spacer(),
              Text(R.string.read_more.tr(),
                  style: TextStyle(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w500,
                    color: R.color.readMore,
                  ))
            ],
          ),
          SizedBox(
            height: 24.h,
          ),
          Row(
            children: [
              Expanded(child: _widgetItem(_maxHeight, imageUrls[0])),
              SizedBox(
                width: 16.w,
              ),
              Expanded(child: _widgetItem(_maxHeight, imageUrls[1])),
            ],
          ),
        ],
      ),
    );
  }

  Widget _widgetItem(maxHeight, url) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8.w),
          child: Container(
            height: maxHeight,
            child: Image.network(
              url,
              fit: BoxFit.cover,
              height: maxHeight,
              width: double.infinity,
            ),
          ),
        ),
        SizedBox(
          height: 16.h,
        ),
        Text(
          '3 cách đạt được tự do tài chính giúp giới abc',
          style: TextStyle(
            fontSize: 14.sp,
            fontWeight: FontWeight.w400,
            color: R.color.newsTitle,
          ),
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
        Text('3 ${R.string.hour.tr()}',
            style: TextStyle(
                fontSize: 12.sp,
                fontWeight: FontWeight.w400,
                color: R.color.contentGrey,
                height: 1.67.w)),
      ],
    );
  }
}
