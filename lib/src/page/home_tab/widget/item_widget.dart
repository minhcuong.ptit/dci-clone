import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/time_ago.dart';

import '../../../../res/R.dart';
import '../../../utils/const.dart';

class ItemWidget extends StatelessWidget {
  final VoidCallback callback;
  final String imageUrl;
  final String title;
  final int dateTime;
  final bool? news;

  ItemWidget(
      {required this.callback,
      required this.imageUrl,
      required this.title,
      required this.dateTime,
      this.news});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        width: 150.h,
        decoration: BoxDecoration(
            color: R.color.white, borderRadius: BorderRadius.circular(10.h)),
        margin: EdgeInsets.only(right: 16.h),
        padding: EdgeInsets.all(4.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.h),
              child: CachedNetworkImage(
                width: double.infinity,
                height: 108.h,
                fit: BoxFit.fill,
                imageUrl: imageUrl,
                placeholder: (context, url) => Image.asset(
                  R.drawable.ic_default_item,
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: 108.h,
                ),
                errorWidget: (context, url, error) => Image.asset(
                  R.drawable.ic_default_item,
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: 108.h,
                ),
              ),
            ),
            SizedBox(height: 7.h),
            Expanded(
              child: Text(
                title,
                style: Theme.of(context).textTheme.regular400.copyWith(
                    color: R.color.black,
                    fontSize: 14.sp,
                    height: 21.h / 14.sp),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(height: 8.h),
            Text(
              news == true
                  ? TimeAgo.timeAgoSinceDate(
                      DateTime.fromMillisecondsSinceEpoch(dateTime,
                          isUtc: false))
                  : DateUtil.parseDateToString(
                      DateTime.fromMillisecondsSinceEpoch(dateTime,
                          isUtc: false),
                      Const.DATE_TIME_FORMAT),
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontWeight: FontWeight.normal,
                  color: news == true ? R.color.gray : R.color.black,
                  fontSize: 12.sp,
                  height: 20.h / 12.sp),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(height: 4.h),
          ],
        ),
      ),
    );
  }
}
