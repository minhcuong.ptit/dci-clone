import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/data/network/response/banner_response.dart';

import '../../../../res/R.dart';
import '../../../widgets/page_view_indicator/page_view_indicator.dart';

class BannerSliderWidget extends StatefulWidget {
  BannerData? bannerData;

  BannerSliderWidget(this.bannerData);

  @override
  State<StatefulWidget> createState() {
    return _BannerSliderWidgetState();
  }
}

class _BannerSliderWidgetState extends State<BannerSliderWidget> {
  ValueNotifier<int> _pageIndexNotifier = ValueNotifier<int>(0);
  final List<String> images = [
    'https://margaretcruises.com/wp-content/uploads/2022/03/unsplash_krV5aS4jDjA.png',
    'https://images.unsplash.com/photo-1586871608370-4adee64d1794?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2862&q=80',
    'https://images.unsplash.com/photo-1586901533048-0e856dff2c0d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
    'https://images.unsplash.com/photo-1586902279476-3244d8d18285?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80',
    'https://images.unsplash.com/photo-1586943101559-4cdcf86a6f87?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1556&q=80',
    'https://images.unsplash.com/photo-1586951144438-26d4e072b891?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
    'https://images.unsplash.com/photo-1586953983027-d7508a64f4bb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
  ];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.bannerData?.fetchBanners?.forEach((imageUrl) {
        precacheImage(NetworkImage(imageUrl.imageUrl ?? ""), context);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(24.w, 24.h, 24.w, 0),
        child: Stack(
          alignment: FractionalOffset.bottomCenter,
          children: [
            CarouselSlider.builder(
              itemCount: widget.bannerData?.fetchBanners?.length,
              options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                viewportFraction: 1.0,
                aspectRatio: 2.14,
                initialPage: 0,
                autoPlayInterval: Duration(seconds: 4),
                autoPlayAnimationDuration: Duration(milliseconds: 400),
                onPageChanged: (index, reason) {
                  _pageIndexNotifier.value = index;
                },
              ),
              itemBuilder: (context, index, realIdx) {
                return ClipRRect(
                  borderRadius: BorderRadius.circular(8.w),
                  child: Container(
                    child: Center(
                        child: CachedNetworkImage(
                            imageUrl: widget.bannerData?.fetchBanners?[index]
                                    .imageUrl ??
                                "",
                            fit: BoxFit.cover,
                            width: double.infinity)),
                  ),
                );
              },
            ),
            _pageViewIndicator()
          ],
        ));
  }

  @override
  void dispose() {
    _pageIndexNotifier.dispose();
    super.dispose();
  }

  PageViewIndicator _pageViewIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: _pageIndexNotifier,
      length: widget.bannerData?.fetchBanners?.length ?? 0,
      normalBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController!,
          curve: Curves.ease,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3.w),
          child: Container(
            width: 6.w,
            height: 6.w,
            color: R.color.navy,
          ),
        ),
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController!,
          curve: Curves.ease,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3.w),
          child: Container(
            width: 6.w,
            height: 6.w,
            color: R.color.indicator,
          ),
        ),
      ),
    );
  }
}
