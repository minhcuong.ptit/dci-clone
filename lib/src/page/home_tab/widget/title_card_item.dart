
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../res/R.dart';

class TitleCardItem extends StatelessWidget {
  final VoidCallback navigatePage;
  final String title;
  const TitleCardItem({Key? key,required this.navigatePage,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 24.h),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
              color: R.color.black,
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: navigatePage,
            child: Text(
              R.string.read_more.tr(),
              style: TextStyle(
                fontSize: 14.sp,
                fontWeight: FontWeight.w500,
                color: R.color.readMore,
              ),
            ),
          )
        ],
      ),
    );
  }
}
