import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:imi/src/page/detail_cart/detail_cart_page.dart';
import 'package:imi/src/page/pin_code/change_password/change_password_cubit.dart';
import 'package:imi/src/page/profile/view_profile/view_profile_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/avatar_widget.dart';
import '../../../../res/R.dart';

class HomeTabSliverHeaderDelegate extends SliverPersistentHeaderDelegate {
  final BuildContext parentContext;
  final _bannerOpacityTween = Tween<double>(begin: 1.0, end: 0);
  final _appBarColorTween =
      ColorTween(begin: Colors.transparent, end: R.color.blue);
  final _actionBtnBgTween =
      ColorTween(begin: Colors.white.withOpacity(0.3), end: Colors.white);
  final _actionBtnColorTween =
      ColorTween(begin: Colors.white, end: Colors.white);
  final _bgColorTween =
      ColorTween(begin: Colors.white.withOpacity(0.3), end: Colors.transparent);
  final _titleTextColorTween =
      ColorTween(begin: Colors.transparent, end: Colors.white);

  double _headerMinHeight = 90.h;
  double _headerMaxHeight = 115.h;
  late double _devicePaddingTop;
  bool needCartNumberAnimated;

  double get maxShrinkOffset => maxExtent - minExtent;
  final Function? onDismiss;
  final VoidCallback onNotification;
  final VoidCallback onDetailCart;
  final int unreadCount;
  final int totalItem;

  HomeTabSliverHeaderDelegate(
      {required this.parentContext,
      required this.needCartNumberAnimated,
      required this.onNotification,
      required this.onDetailCart,
      required this.unreadCount,
      required this.totalItem,
      this.onDismiss}) {
    //_headerMaxHeight = 120.h;
    //(MediaQuery.of(parentContext).size.width) * (146.h / 312.w);
    _devicePaddingTop = MediaQuery.of(parentContext).viewPadding.top;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      needCartNumberAnimated = false;
    });
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final scrollProgress = shrinkOffset / maxShrinkOffset;
    return ConstrainedBox(
      constraints: BoxConstraints(minHeight: maxExtent),
      child: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          Positioned(
            top: 0.h - shrinkOffset / 3,
            left: 0.w,
            right: 0.w,
            child: Opacity(
              opacity: _bannerOpacityTween.transform(scrollProgress) >= 0
                  ? _bannerOpacityTween.transform(scrollProgress)
                  : 0.0,
              child: Image.asset(
                R.drawable.bg_home_header,
                width: MediaQuery.of(context).size.width,
                height: maxExtent,
                fit: BoxFit.cover,
              ),
            ),
          ),
          _buildAppBar(context, scrollProgress),
        ],
      ),
    );
  }

  @override
  double get maxExtent => _headerMaxHeight + _devicePaddingTop;

  @override
  double get minExtent => _headerMinHeight + _devicePaddingTop;

  @override
  OverScrollHeaderStretchConfiguration get stretchConfiguration =>
      OverScrollHeaderStretchConfiguration();

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    if (oldDelegate is HomeTabSliverHeaderDelegate) {
      return parentContext != oldDelegate.parentContext;
    }
    return this != oldDelegate;
  }

  Widget _buildAppBar(BuildContext context, double scrollProgress) => Container(
        height: minExtent,
        color: _appBarColorTween.lerp(scrollProgress > 1 ? 1 : scrollProgress),
        padding: EdgeInsets.only(
          top: _devicePaddingTop,
          left: 12.w,
          right: 12.w,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                NavigationUtils.navigatePage(
                    context, ViewProfilePage(communityId: null));
              },
              child: AvatarWidget(
                  avatar: appPreferences.personProfile?.avatarUrl, size: 32.h),
            ),
            Expanded(
              child: Visibility(
                visible: scrollProgress > 0,
                child: Image.asset(
                  R.drawable.ic_logo_header,
                  color: _titleTextColorTween.lerp(scrollProgress),
                  colorBlendMode: BlendMode.modulate,
                  width: 66.w,
                  height: 41.h,
                ),
              ),
            ),
            Row(
              children: [
                InkWell(
                  onTap: onDetailCart,
                  child: SizedBox(
                    height: 26,
                    width: 24,
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        LayoutBuilder(
                          builder: (BuildContext context,
                              BoxConstraints constraints) {
                            return Icon(
                              CupertinoIcons.shopping_cart,
                              color: R.color.white,
                              size: 22.h,
                            );
                          },
                        ),
                        Positioned(
                          right: -8,
                          top: -5,
                          child: Visibility(
                            visible: totalItem > 0,
                            child: Container(
                              padding: EdgeInsets.only(
                                  right: totalItem > 9 ? 2 : 5,
                                  left: totalItem > 9 ? 2 : 5,
                                  bottom: 1.5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: R.color.tertiaryOrange),
                              child: Text(
                                totalItem > 99 ? "99" : "$totalItem",
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 7, color: R.color.white),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: onNotification,
                  child: SizedBox(
                    height: 30,
                    width: 40,
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Center(
                          child: LayoutBuilder(
                            builder: (context, constraints) => Image.asset(
                              R.drawable.ic_notification,
                              height: 24,
                              width: 24,
                              color: R.color.white,
                            ),
                          ),
                        ),
                        Positioned(
                          right: 3,
                          top: -3,
                          child: Visibility(
                            visible: unreadCount > 0,
                            child: Container(
                              padding: EdgeInsets.only(
                                  right: totalItem > 9 ? 2 : 5,
                                  left: totalItem > 9 ? 2 : 5,
                                  bottom: 1.5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: R.color.tertiaryOrange),
                              child: Text(
                                unreadCount > 99 ? "99" : "$unreadCount",
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        color: R.color.white, fontSize: 7),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
}
