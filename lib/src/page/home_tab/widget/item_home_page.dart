import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';
import '../../../utils/const.dart';
import '../../../utils/date_util.dart';
import '../../../utils/time_ago.dart';

class CardItem extends StatelessWidget {
  final String? image;
  final String? title;
  final int? time;
  final bool? typeVideo;
  final bool? isVideo;
  final bool? isLockVideo;
  final bool? isLockEbook;
  final bool? isEbook;
  final int? dateTime;
  final bool? news;
  final bool? containDate;

  CardItem(
      {Key? key,
      this.image,
      required this.title,
      required this.containDate,
      this.dateTime,
      this.isEbook,
      this.isLockEbook,
      this.isLockVideo,
      this.isVideo,
      this.news,
      this.time,
      this.typeVideo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150.h,
      decoration: BoxDecoration(
          color: R.color.white, borderRadius: BorderRadius.circular(10.h)),
      margin: EdgeInsets.only(right: 16.h),
      padding: EdgeInsets.all(4.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(10.h),
                  child: CachedNetworkImage(
                    width: double.infinity,
                    height: 108.h,
                    fit: BoxFit.cover,
                    imageUrl: image ?? "",
                    placeholder: (context, url) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: 108.h,
                    ),
                    errorWidget: (context, url, error) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: 108.h,
                    ),
                  )),
              Visibility(
                visible: isVideo ?? false,
                child: Positioned(
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Icon(
                    CupertinoIcons.play_circle_fill,
                    size: 30.h,
                    color: R.color.white,
                  ),
                ),
              ),
              Visibility(
                visible: (isEbook ?? false) && (isLockEbook ?? false),
                child: Positioned(
                    top: 5.h,
                    right: 5.h,
                    child: Container(
                      width: 25.h,
                      height: 25.h,
                      child: Icon(
                        CupertinoIcons.lock,
                        size: 15.h,
                        color: R.color.blue,
                      ),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: R.color.white),
                    )),
              ),
              Visibility(
                visible: (typeVideo ?? false) && (isLockVideo ?? false),
                child: Positioned(
                    top: 5.h,
                    right: 5.h,
                    child: Container(
                      width: 25.h,
                      height: 25.h,
                      child: Icon(
                        CupertinoIcons.lock,
                        size: 15.h,
                        color: R.color.blue,
                      ),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: R.color.white),
                    )),
              )
            ],
          ),
          SizedBox(height: 8.h),
          Expanded(
            child: Text(
              title ?? "",
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontWeight: FontWeight.normal,
                  color: R.color.black,
                  fontSize: 14,
                  height: 21 / 14),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          containDate == true
              ? Text(
                  news == true
                      ? TimeAgo.timeAgoSinceDate(
                          DateTime.fromMillisecondsSinceEpoch(dateTime ?? 0,
                              isUtc: false))
                      : DateUtil.parseDateToString(
                          DateTime.fromMillisecondsSinceEpoch(dateTime ?? 0,
                              isUtc: false),
                          Const.DATE_TIME_FORMAT),
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontWeight: FontWeight.normal,
                      color: news == true ? R.color.gray : R.color.black,
                      fontSize: 12.sp,
                      height: 20.h / 12.sp),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                )
              : const SizedBox.shrink(),
          containDate == true ? SizedBox(height: 4.h) : const SizedBox.shrink(),
        ],
      ),
    );
    ;
  }
}
