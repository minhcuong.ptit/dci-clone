import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/home_sow/home_sow_page.dart';
import 'package:imi/src/page/home_zen/home_zen.dart';
import 'package:imi/src/page/practice_dci/practice_dci_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../../../../res/R.dart';
import '../../../data/network/response/course_response.dart';
import '../../home_study_group/home_study_group.dart';

class CategoryWidget extends StatefulWidget {
  final List<FetchCoursesData> listCourse;
  CategoryWidget(this.listCourse);

  @override
  CategoryWidgetState createState() => CategoryWidgetState();
}

class CategoryWidgetState extends State<CategoryWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildContent();
  }

  Widget _buildContent() {
    var listWidget = <Widget>[];
    listWidget.add(_buildRow(context));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(16, 24, 16, 0),
          color: R.color.lightestGray,
          child: Column(
            children: listWidget,
          ),
        ),
      ],
    );
  }

  Widget _buildRow(BuildContext context) {
    var _columnNumber = 4;
    var list = <Widget>[];
    String logo = '';
    String title = '';
    Function? onTap;
    for (var i = 0; i < 4; i++) {
      switch (i) {
        case 0:
          logo = R.drawable.ic_study_groups;
          title = R.string.study_groups.tr();
          onTap = () {
            NavigationUtils.navigatePage(context, HomeStudyGroupPage(listCourse: widget.listCourse??[],));
          };
          break;
        case 1:
          logo = R.drawable.ic_self_practice;
          title = R.string.self_practice.tr();
          onTap = () {
            NavigationUtils.navigatePage(context, PracticeDCIPage());
          };
          break;
        case 2:
          logo = R.drawable.ic_book_of_three_times;
          title = R.string.book_of_three_times.tr();
          onTap = () {
            NavigationUtils.rootNavigatePage(context, HomeSowPage());
          };
          break;
        case 3:
          logo = R.drawable.ic_coffee_meditation;
          title = R.string.coffee_meditation.tr();
          onTap = () {
            NavigationUtils.rootNavigatePage(context, HomeZenPage());
          };
          break;
      }
      list.add(Expanded(child: _buildItemOfRow(context, logo, title, onTap)));
      if (list.length >= _columnNumber) break;
    }
    while (list.length < _columnNumber) {
      list.add(Expanded(child: Container()));
    }
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: list);
  }

  Widget _buildItemOfRow(
      BuildContext context, String logo, String title, Function? onTap) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Column(
        children: [
          Container(
            width: 45.w,
            height: 45.w,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(33.w),
                color: R.color.categoryBackground),
            child: Image.asset(
              logo,
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
            child: Text(
              title.toUpperCase(),
              style: Theme.of(context)
                  .textTheme
                  .labelSmallText
                  .copyWith(fontSize: 11),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
