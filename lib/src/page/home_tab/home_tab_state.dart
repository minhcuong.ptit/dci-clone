import 'package:equatable/equatable.dart';

abstract class HomeTabState {}

class HomeTabInitial extends HomeTabState {
  @override
  String toString() => 'HomeTabInitial';
}

class HomeTabSuccess extends HomeTabState {
  @override
  String toString() => 'HomeTabSuccess';
}

class HomeTabLoading extends HomeTabState {
  @override
  String toString() => 'HomeTabLoading';
}

class HomeTabFailure extends HomeTabState {
  final String error;

  HomeTabFailure(this.error);

  @override
  String toString() => 'HomeTabFailure { error: $error }';
}

class HomeTabUnreadNotificationChange extends HomeTabState {
  final int unreadCount;

  HomeTabUnreadNotificationChange(this.unreadCount);
}

class HomeTabCartSuccess extends HomeTabState {
  @override
  String toString() => 'HomeTabCartSuccess';
}

class GetListChannelSuccess extends HomeTabState {
  @override
  String toString() => 'GetListChannelSuccess';
}
