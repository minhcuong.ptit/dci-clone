import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/response/banner_response.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/deatil_cart_response.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/response/un_read_count_response.dart';
import 'package:imi/src/data/network/response/user_channel_group_response.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:pubnub/pubnub.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/event_response.dart';
import '../../data/network/response/fetch_event_data.dart';
import '../../data/network/response/person_profile.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import '../../utils/enum.dart';
import 'home_tab_state.dart';
import 'package:imi/src/utils/app_config.dart' as config;

class HomeTabCubit extends Cubit<HomeTabState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  PersonProfile? personProfile;
  bool isCheckedJoinDCI = true;
  List<LibraryData> listLibrary = [];
  List<NewsData> listNews = [];
  List<NewsData> listSuccessStory = [];
  List<FetchCoursesData> listCourse = [];
  List<FetchEventsData> listEvent = [];

  //BannerData? listBanner;
  List<BannerDataFetchBanners> listBanner = [];
  String? _nextToken;

  List<UserChannelGroups> userChannelId = [];
  DetailCartData? detailCartData;

  String? get userId => appPreferences.getString(Const.ID);

  late final PubNub pubNub;
  late final ChannelGroupDx? chatChannel;

  int? get countCart => appPreferences.getInt(Const.COUNT_CART);

  int? get countMessage => appPreferences.getInt(Const.COUNT_MESSAGE);

  HomeTabCubit({
    required this.repository,
    required this.graphqlRepository,
  }) : super(HomeTabInitial()) {}

  void getProfile({bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || isLoadMore) ? HomeTabInitial() : HomeTabLoading());
    ApiResult<PersonProfile> apiResult =
        await graphqlRepository.getMyPersonalProfile();
    apiResult.when(success: (PersonProfile data) {
      appPreferences.setPersonProfile(data);
      personProfile = data;
      var communityRoles =
          personProfile?.communityV2?.communityInfo?.communityRole;
      MemberType? memberType =
          (communityRoles?.isNotEmpty ?? false) ? communityRoles![0] : null;
     bool isMember= communityRoles?.where((element) => element.name== MemberType.NEW_USER.name||element.name==MemberType.NORMAL_USER.name).toList().isEmpty??false;
    if(isMember){
      appPreferences.setData(Const.MEMBER, 'MEMBER');
    }else{
      appPreferences.setData(Const.MEMBER, 'NOT_LEARN_DCI');
    }
      isCheckedJoinDCI = (memberType == MemberType.MEMBER) ||
          (memberType == MemberType.NORMAL_USER) ||
          (memberType == MemberType.LEADER);

      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getUnreadNotificationCount() async {
    final res = await graphqlRepository.getUnreadNotificationCount();
    res.when(
        success: (data) {
          emit(HomeTabUnreadNotificationChange(data));
        },
        failure: (_) {});
  }

  void getDetailCart() async {
    ApiResult<DetailCartData> result = await graphqlRepository.getDetailCart();
    result.when(
        success: (DetailCartData data) {
          detailCartData = data;
          appPreferences.setData(Const.COUNT_CART, data.fetchMyOrder?.total);
          emit(HomeTabCartSuccess());
        },
        failure: (e) {});
  }

  void refreshCountCart() {
    emit(HomeTabLoading());
    getDetailCart();
    emit(HomeTabCartSuccess());
  }

  @override
  Future<void> close() {
    return super.close();
  }

  void getListJoinedChannel() async {
    emit(HomeTabLoading());
    ApiResult<List<UserChannelGroups>> result =
        await graphqlRepository.getUserChannelGroup();
    result.when(success: (List<UserChannelGroups> data) async {
      userChannelId = data;
      emit(GetListChannelSuccess());
      //initChannel(data);
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  // void initChannel(List<UserChannelGroups> channelId) {
  //   pubNub = PubNub(
  //       defaultKeyset: Keyset(
  //           subscribeKey:
  //           config.AppConfig.environment.pubNubSubscribeKey,
  //           publishKey:
  //           config.AppConfig.environment.pubNubPublishKey,
  //           userId: UserId(appPreferences.getString(Const.ID) ?? "")));
  //   chatChannel = pubNub.channelGroups;
  //   for (int i = 0; i < channelId.length; i++) {
  //     pubNub
  //         .subscribe(channelGroups: {channelId[i].channelGroupsPubnubId ?? ""})
  //         .messages
  //         .listen((event) async {
  //           if (chatChannel != null) {
  //             getReadTotalCount();
  //           }
  //         });
  //   }
  // }

  void getReadTotalCount() async {
    ApiResult<TotalCount> result =
        await graphqlRepository.unreadCount(userId ?? "");
    result.when(success: (TotalCount data) async {
      appPreferences.setData(Const.COUNT_MESSAGE, data.count);
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListBanner({bool isRefresh = false}) async {
    emit(!isRefresh ? HomeTabLoading() : HomeTabInitial());
    if (isRefresh) {
      _nextToken = null;
    }
    ApiResult<BannerData> result = await graphqlRepository.getBanner();
    result.when(success: (BannerData data) async {
      listBanner = data.fetchBanners ?? [];
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListNews({bool isRefresh = false}) async {
    emit(!isRefresh ? HomeTabLoading() : HomeTabInitial());
    if (isRefresh) {
      _nextToken = null;
    }
    ApiResult<NewsDataFetch> result = await graphqlRepository.getNews(
        typeCategory: NewsCategory.NEWS, limit: Const.LIMIT);
    result.when(success: (NewsDataFetch data) async {
      listNews = data.data ?? [];
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListLibrary({bool isRefresh = false}) async {
    emit(!isRefresh ? HomeTabLoading() : HomeTabInitial());
    if (isRefresh) {
      _nextToken = null;
    }
    final result = await graphqlRepository.getLibraryPage(isPaging: false);
    result.when(success: (LibraryDataFetchDocuments data) async {
      listLibrary =data.data?.where((element) => element.privacy==DocumentPrivacyScope.PUBLIC.name).toList()??[];
      listLibrary.sort((a, b) => b.priority!.compareTo(a.priority??0.toInt()));
      if(listLibrary.length>5){
        listLibrary=listLibrary.take(5).toList();
      }
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListCourse({bool isRefresh = false}) async {
    emit(!isRefresh ? HomeTabLoading() : HomeTabInitial());
    if (isRefresh) {
      _nextToken = null;
    }
    ApiResult<FetchCourse> result = await graphqlRepository.getCourse(
        nextToken: _nextToken,
        priorityOrder: Order.DESC.name,
        limit: Const.LIMIT);
    result.when(success: (FetchCourse data) async {
      listCourse = data.data ?? [];
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListEvent({bool isRefresh = false}) async {
    emit(!isRefresh ? HomeTabLoading() : HomeTabInitial());
    if (isRefresh) {
      _nextToken = null;
    }
    ApiResult<FetchEvents> result = await graphqlRepository.getEvent(
        nextToken: _nextToken, limit: Const.LIMIT);
    result.when(success: (FetchEvents data) async {
      listEvent = data.data ?? [];
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListSuccessStory({bool isRefresh = false}) async {
    emit(!isRefresh ? HomeTabLoading() : HomeTabInitial());
    if (isRefresh) {
      _nextToken = null;
    }
    ApiResult<NewsDataFetch> result = await graphqlRepository.getNews(
        typeCategory: NewsCategory.SUCCESS_GOAL, limit: Const.LIMIT);
    result.when(success: (NewsDataFetch data) async {
      listSuccessStory = data.data ?? [];
      emit(HomeTabSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeTabFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

}
