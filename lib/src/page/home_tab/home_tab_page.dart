import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/course_details/course_details.dart';
import 'package:imi/src/page/detail_cart/detail_cart_page.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_page.dart';
import 'package:imi/src/page/home_tab/widget/banner_slider_widget.dart';
import 'package:imi/src/page/home_tab/widget/category_widget.dart';
import 'package:imi/src/page/home_tab/widget/home_tab_sliver_header_delegate.dart';
import 'package:imi/src/page/home_tab/widget/item_home_page.dart';
import 'package:imi/src/page/home_tab/widget/item_widget.dart';
import 'package:imi/src/page/home_tab/widget/title_card_item.dart';
import 'package:imi/src/page/list_course/list_course_page.dart';
import 'package:imi/src/page/main/main_cubit.dart';
import 'package:imi/src/page/messages/messages_page.dart';
import 'package:imi/src/page/messages/widget/messages_empty_widget.dart';
import 'package:imi/src/page/news_details/news_details_page.dart';
import 'package:imi/src/page/news_list/news_list_page.dart';
import 'package:imi/src/page/notification/notification.dart';
import 'package:imi/src/page/system_search/system_search.dart';
import 'package:imi/src/page/webview_page/webview_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/time_ago.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/page_view_indicator/page_view_indicator.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../app.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../utils/date_util.dart';
import '../../utils/dci_event_handle.dart';
import '../../utils/navigation_utils.dart';
import '../../widgets/search/search_widget.dart';
import '../detail_event/detail_event.dart';
import '../detail_story/detail_story_page.dart';
import '../home_event/home_event_page.dart';
import '../home_success_story/home_story.dart';
import '../profile/edit_profile/edit_profile.dart';
import 'home_tab_cubit.dart';
import 'home_tab_state.dart';

class HomeTabPage extends StatefulWidget {
  const HomeTabPage({Key? key}) : super(key: key);

  @override
  _HomeTabPageState createState() => _HomeTabPageState();
}

class _HomeTabPageState extends State<HomeTabPage>
    with
        SingleTickerProviderStateMixin,
        RouteAware,
        AutomaticKeepAliveClientMixin {
  late HomeTabCubit _cubit;
  late MainCubit _mainCubit;
  final TextEditingController _controller = TextEditingController();
  final RefreshController _refreshController = RefreshController();
  final ScrollController _scrollController = ScrollController();
  int _currentIndex = 0;
  bool checkPage = false;

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _mainCubit = GetIt.I<MainCubit>();
    _cubit = HomeTabCubit(
        repository: repository, graphqlRepository: graphqlRepository);
    _cubit.getProfile();
    _cubit.getListBanner();
    _cubit.getListNews();
    _cubit.getListLibrary();
    _cubit.getListEvent();
    _cubit.getListCourse();
    _cubit.getListSuccessStory();
    _cubit.getUnreadNotificationCount();
    _cubit.getDetailCart();
    _cubit.getListJoinedChannel();
    _cubit.getReadTotalCount();
    DCIEventHandler().subscribe(_onPostListener);
    DCIEventHandler().subscribe(_onReceiveNotification);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      _cubit.getUnreadNotificationCount();
      _cubit.getReadTotalCount();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    _cubit.close();
    _refreshController.dispose();
    _controller.dispose();
    _scrollController.dispose();
    DCIEventHandler().unsubscribe(_onPostListener);
    DCIEventHandler().unsubscribe(_onReceiveNotification);
    super.dispose();
  }

  void _onPostListener(UpdateProfileEvent event) {
    if (mounted) {
      _cubit.getProfile();
    }
  }

  void _onReceiveNotification(NewNotificationEvent event) {
    if (mounted) {
      _cubit.getUnreadNotificationCount();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<HomeTabCubit, HomeTabState>(
          listener: (context, state) {
            if (state is! HomeTabLoading) {
              _refreshController.refreshCompleted();
            }
            if (_mainCubit.countCart == 1) {
              _cubit.refreshCountCart();
              _mainCubit.countCart = 0;
              return;
            }
          },
          builder: (context, state) {
            _refreshController.refreshCompleted();
            return _buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, HomeTabState state) {
    return Material(
      color: R.color.lightestGray,
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Stack(
          children: [
            StackLoadingView(
              visibleLoading: state is HomeTabLoading,
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () {
                  _cubit.getProfile(isRefresh: true);
                  _cubit.getListBanner(isRefresh: true);
                  _cubit.getListNews(isRefresh: true);
                  _cubit.getListLibrary(isRefresh: true);
                  _cubit.getListEvent(isRefresh: true);
                  _cubit.getListCourse(isRefresh: true);
                  _cubit.getListSuccessStory(isRefresh: true);
                },
                onLoading: () {},
                child: CustomScrollView(
                  controller: _scrollController,
                  slivers: [
                    BlocConsumer<HomeTabCubit, HomeTabState>(
                      buildWhen: (previous, current) =>
                          current is HomeTabUnreadNotificationChange,
                      listener: (previous, current) {
                        if (current is HomeTabUnreadNotificationChange) {
                          _scrollController.animateTo(
                              _scrollController.offset + 0.1,
                              duration: const Duration(milliseconds: 10),
                              curve: Curves.linear);
                        }
                        if (current is HomeTabCartSuccess) {
                          _scrollController.animateTo(
                              _scrollController.offset + 0.1,
                              duration: const Duration(milliseconds: 10),
                              curve: Curves.linear);
                        }
                        if (checkPage == true) {
                          if (current is GetListChannelSuccess) {
                            if (_cubit.userChannelId.length != 0) {
                              NavigationUtils.rootNavigatePage(
                                  context,
                                  MessagesPage(
                                    channelGroupsPubnubId: _cubit.userChannelId,
                                  )).then((value) => _cubit.getReadTotalCount());
                            } else {
                              NavigationUtils.rootNavigatePage(
                                  context, MessagesEmptyWidget());
                            }
                          }
                        }
                      },
                      builder: (context, state) {
                        return SliverPersistentHeader(
                          pinned: true,
                          delegate: HomeTabSliverHeaderDelegate(
                              onDetailCart: () {
                                NavigationUtils.rootNavigatePage(
                                        context, DetailCartPage())
                                    .whenComplete(
                                        () => _cubit.refreshCountCart());
                              },
                              totalItem:
                                  _cubit.detailCartData?.fetchMyOrder?.total !=
                                          null
                                      ? _cubit.countCart ?? 0
                                      : 0,
                              parentContext: context,
                              unreadCount:
                                  state is HomeTabUnreadNotificationChange
                                      ? state.unreadCount
                                      : 0,
                              needCartNumberAnimated: false,
                              onDismiss: () {
                                if (Navigator.canPop(context)) {
                                  Navigator.pop(context);
                                }
                              },
                              onNotification: () {
                                NavigationUtils.navigatePage(
                                        context, NotificationPage())
                                    .whenComplete(() =>
                                        _cubit.getUnreadNotificationCount());
                              }),
                        );
                      },
                    ),
                    _buildListItem()
                  ],
                ),
              ),
            ),
            Positioned(
                left: 0,
                right: 0,
                top: 117.h,
                child: Visibility(
                  visible: !_cubit.isCheckedJoinDCI,
                  child: GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      NavigationUtils.navigatePage(context, EditProfilePage());
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.w),
                        color: R.color.blueF6,
                      ),
                      margin: EdgeInsets.symmetric(horizontal: 24.w),
                      height: 48.h,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 16.w,
                          ),
                          Icon(
                            Icons.info,
                            color: R.color.white,
                            size: 20.h,
                          ),
                          SizedBox(
                            width: 10.w,
                          ),
                          Text(
                            R.string.please_update_your_infomation_aobout_dci
                                .tr(),
                            style: Theme.of(context)
                                .textTheme
                                .labelSmallText
                                .copyWith(
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w600,
                                  color: R.color.white,
                                ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _buildListItem() => SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SearchWidget(
                        autoFocus: false,
                        onSubmit: (text) {
                          if (text.isEmpty) {
                            null;
                          } else
                            NavigationUtils.rootNavigatePage(
                                context,
                                SystemSearchPage(
                                    keyWord: text.replaceAll("\\", "\t")));
                        },
                        searchController: _controller,
                        type: Const.SEARCH_SYSTEM,
                        onChange: (text) {
                          //_cubit.replaceText(text.replaceAll('\\', ''));
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _cubit.getListJoinedChannel();
                        checkPage = true;
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 9.h, horizontal: 8.w),
                        margin: EdgeInsets.only(right: 8.w, top: 8.h),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: R.color.white),
                        child: Stack(
                          children: [
                            Image.asset(
                              R.drawable.ic_message_off,
                              color: R.color.primaryColor,
                              height: 28.h,
                            ),
                            _cubit.countMessage == 0 ||
                                    _cubit.countMessage == null
                                ? const SizedBox.shrink()
                                : Positioned(
                                    right: 1,
                                    top: 1,
                                    child: Visibility(
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            right: 5, left: 5, bottom: 1.5),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: R.color.tertiaryOrange),
                                        child: Text(
                                          "${_cubit.countMessage}",
                                          textAlign: TextAlign.center,
                                          style: Theme.of(context)
                                              .textTheme
                                              .medium500
                                              .copyWith(
                                                  color: R.color.white,
                                                  fontSize: 7),
                                        ),
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                CategoryWidget(_cubit.listCourse),
                SizedBox(height: 10.h),
                CarouselSlider(
                  options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 2.14,
                    viewportFraction: 1.0,
                    initialPage: 0,
                    onPageChanged: (index, reason) {
                      setState(
                        () {
                          _currentIndex = index;
                        },
                      );
                    },
                  ),
                  items: List.generate(
                      _cubit.listBanner.length,
                      (index) => Stack(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  if (_cubit.listBanner[index].type ==
                                      BannerType.COURSE.name) {
                                    NavigationUtils.rootNavigatePage(
                                            context,
                                            CourseDetailsPage(
                                              courseId: _cubit
                                                  .listBanner[index].link?.id,
                                              productId: _cubit
                                                  .listBanner[index]
                                                  .link
                                                  ?.productId,
                                            ))
                                        .then((value) =>
                                            _cubit.refreshCountCart());
                                  } else if (_cubit.listBanner[index].type ==
                                      BannerType.NEWS.name) {
                                    NavigationUtils.rootNavigatePage(
                                        context,
                                        NewsDetailsPage(
                                          newsId:
                                              _cubit.listBanner[index].link?.id,
                                        ));
                                  } else if (_cubit.listBanner[index].type ==
                                      BannerType.LIBRARY.name) {
                                    NavigationUtils.rootNavigatePage(
                                        context,
                                        DetailDocumentPage(
                                          id: _cubit.listBanner[index].link?.id,
                                        ));
                                  } else if (_cubit.listBanner[index].type ==
                                      BannerType.EXTERNAL_LINK.name) {
                                    Utils.launchURL(Utils.getTypeUrlLauncher(
                                        _cubit.listBanner[index].externalLink ??
                                            "",
                                        Const.LAUNCH_TYPE_WEB));
                                  } else if (_cubit.listBanner[index].type ==
                                      BannerType.NOT_LINK.name) {
                                    setState(() {});
                                  }
                                },
                                child: Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 20.h),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8.w),
                                    child: Container(
                                      child: Center(
                                        child: CachedNetworkImage(
                                          imageUrl: _cubit
                                                  .listBanner[index].imageUrl ??
                                              "",
                                          fit: BoxFit.fill,
                                          width: double.infinity,
                                          placeholder: (context, url) =>
                                              Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(8.w),
                                                border: Border.all(
                                                    width: 1,
                                                    color: R.color.grey)),
                                            child: Image.asset(
                                                R.drawable.ic_default_banner,
                                                fit: BoxFit.cover,
                                                width: double.infinity),
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(8.w),
                                                border: Border.all(
                                                    width: 1,
                                                    color: R.color.grey)),
                                            child: Image.asset(
                                                R.drawable.ic_default_banner,
                                                fit: BoxFit.cover,
                                                width: double.infinity),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 5.h,
                                left: 0.h,
                                right: 0.h,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: _cubit.listBanner.map((urlOfItem) {
                                    int index =
                                        _cubit.listBanner.indexOf(urlOfItem);
                                    return Container(
                                      width: 10.0,
                                      height: 10.0,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 2.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: _currentIndex == index
                                            ? R.color.primaryColor
                                            : R.color.grey,
                                      ),
                                    );
                                  }).toList(),
                                ),
                              )
                            ],
                          )),
                ),
                SizedBox(height: 20.h),
                TitleCardItem(
                  navigatePage:(){
                    NavigationUtils.navigatePage(context, NewsListPage());
                  } ,
                  title: R.string.news.tr().toUpperCase(),
                ),
                SizedBox(height: 24.h),
                Container(
                  height: 200.h,
                  padding: EdgeInsets.only(
                    left: 24.h,
                  ),
                  child: ListView.builder(
                      itemCount: _cubit.listNews.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          onTap: () {
                            NavigationUtils.navigatePage(
                                context,
                                NewsDetailsPage(
                                  newsId: _cubit.listNews[index].id,
                                ));
                          },
                          child: CardItem(
                            image: _cubit.listNews[index].imageUrl ?? "",
                            title: _cubit.listNews[index].title ?? "",
                            dateTime: _cubit.listNews[index].createdDate ?? 0,
                            news: true,
                            containDate: true,
                          ),
                        );
                      }),
                ),
                SizedBox(height: 20.h),
                TitleCardItem(
                  navigatePage:(){
                    _mainCubit.selectTab(2);
                  } ,
                  title: R.string.library.tr().toUpperCase(),
                ),
                SizedBox(height: 16.h),
                Container(
                  height: 180.h,
                  padding: EdgeInsets.only(left: 24.h),
                  child: ListView.builder(
                      itemCount: _cubit.listLibrary.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, int index) {
                        LibraryData data = _cubit.listLibrary[index];
                        return GestureDetector(
                          onTap: () {
                            NavigationUtils.navigatePage(
                              context,
                              DetailDocumentPage(
                                id: data.id,
                                link: data.link,
                                isLocked: data.isLocked,
                              ),
                            );
                          },
                          child: CardItem(
                            containDate: false,
                            isVideo: data.type == DocumentType.VIDEO.name,
                            isEbook: data.type == DocumentType.E_BOOK.name,
                            isLockEbook: data.isLocked,
                            isLockVideo: data.isLocked,
                            typeVideo: data.type == DocumentType.VIDEO.name,
                            image: data.imageUrl,
                            title: data.title,
                            time: data.modifiedDate,
                          ),
                        );
                      }),
                ),
                SizedBox(height: 20.h),
                TitleCardItem(
                  navigatePage:(){
                    NavigationUtils.navigatePage(context, HomeEventPage())
                        .then((value) => _cubit.refreshCountCart());
                  } ,
                  title: R.string.event.tr().toUpperCase(),
                ),
                SizedBox(height: 16.h),
                Container(
                  height: 200.h,
                  padding: EdgeInsets.only(
                    left: 24.h,
                  ),
                  child: ListView.builder(
                      itemCount: _cubit.listEvent.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          onTap: () {
                            NavigationUtils.navigatePage(
                                context,
                                EventDetailPage(
                                  eventId: _cubit.listEvent[index].id ?? 0,
                                  productId:
                                      _cubit.listEvent[index].productId ?? 0,
                                )).then((value) => _cubit.refreshCountCart());
                          },
                          child: CardItem(
                              image: _cubit.listEvent[index].avatarUrl ?? "",
                              title: _cubit.listEvent[index].name ?? "",
                              dateTime: _cubit.listEvent[index].startDate ?? 0,
                              containDate: true),
                        );
                      }),
                ),
                SizedBox(height: 24.h),
                TitleCardItem(
                  navigatePage:(){
                    NavigationUtils.rootNavigatePage(
                        context, ListCoursePage())
                        .then((value) => _cubit.refreshCountCart());
                  } ,
                  title: R.string.course.tr().toUpperCase(),
                ),
                SizedBox(height: 15.h),
                Container(
                  height: 180.h,
                  padding: EdgeInsets.only(left: 24.h),
                  child: ListView.builder(
                      itemCount: _cubit.listCourse.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, int index) {
                        FetchCoursesData data = _cubit.listCourse[index];
                        return GestureDetector(
                          onTap: () {
                            NavigationUtils.navigatePage(
                                context,
                                CourseDetailsPage(
                                  courseId: data.id,
                                  productId: data.productId ?? 0,
                                )).then((value) => _cubit.refreshCountCart());
                          },
                          child: CardItem(
                              image: data.avatarUrl,
                              title: data.name,
                              containDate: false),
                        );
                      }),
                ),
                SizedBox(height: 24.h),
                TitleCardItem(
                  navigatePage:(){
                    NavigationUtils.navigatePage(context, HomeStoryPage());
                  } ,
                  title: R.string.success_stories.tr().toUpperCase(),
                ),
                SizedBox(height: 24.h),
                Container(
                  height: 180.h,
                  padding: EdgeInsets.only(
                    left: 24.h,
                  ),
                  child: ListView.builder(
                      itemCount: _cubit.listSuccessStory.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          onTap: () {
                            NavigationUtils.navigatePage(
                                context,
                                DetailStoryPage(
                                   _cubit.listSuccessStory[index].id,
                                ));
                          },
                          child: CardItem(
                            image: _cubit.listSuccessStory[index].imageUrl ?? "",
                            title: _cubit.listSuccessStory[index].title ?? "",
                            containDate: false,
                          ),
                        );
                      }),
                ),
                SizedBox(
                  height: 50.h,
                )
              ],
            );
          },
          childCount: 1,
        ),
      );

  @override
  bool get wantKeepAlive => true;
}
