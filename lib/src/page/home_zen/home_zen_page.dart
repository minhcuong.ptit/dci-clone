import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/histories_meditation_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/page/detail_tutorial_snow/detail_tutorial_snow_page.dart';
import 'package:imi/src/page/detail_zen/detail_zen.dart';
import 'package:imi/src/page/home_zen/home_zen.dart';
import 'package:imi/src/page/home_zen/home_zen_cubit.dart';
import 'package:imi/src/page/meditation_detail_report/meditation_detail_report_page.dart';
import 'package:imi/src/page/practice_zen/practice_zen_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/time_ago.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeZenPage extends StatefulWidget {
  const HomeZenPage({Key? key}) : super(key: key);

  @override
  State<HomeZenPage> createState() => _HomeZenPageState();
}

class _HomeZenPageState extends State<HomeZenPage> {
  late HomeZenCubit _cubit;
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = HomeZenCubit(repository, graphqlRepository);
    _cubit.getListTutorial();
    _cubit.getListHistoriesMeditation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.lightestGray,
      appBar: appBar(context, R.string.meditation.tr().toUpperCase(),
          backgroundColor: R.color.primaryColor,
          titleColor: R.color.white,
          centerTitle: true,
          iconColor: R.color.white,
          rightWidget: Padding(
            padding: EdgeInsets.only(right: 28.w),
            child: GestureDetector(
              onTap: () {
                NavigationUtils.navigatePage(
                    context, MeditationDetailReportPage());
              },
              child: Image.asset(
                R.drawable.ic_bar_chart,
                height: 23.h,
                width: 23.h,
              ),
            ),
          )),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<HomeZenCubit, HomeZenState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! HomeZenLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, HomeZenState state) {
    return StackLoadingView(
      visibleLoading: state is HomeZenLoading,
      child: SmartRefresher(
        controller: _refreshController,
        enablePullUp: _cubit.nextToken != null,
        onRefresh: () {
          _cubit.getListTutorial(isRefresh: true);
          _cubit.getListHistoriesMeditation(isRefresh: true);
        },
        onLoading: () {
          _cubit.getListHistoriesMeditation(isLoadMore: true);
        },
        child: ListView(
          children: [
            buildCreateZen(context),
            SizedBox(height: 30.h),
            buildSlideTutorial(),
            SizedBox(height: 30.h),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20.h),
              height: 1,
              width: double.infinity,
              color: R.color.grey300,
            ),
            SizedBox(height: 15.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.h),
              child: Text(
                R.string.your_latest_activity.tr(),
                style: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(fontSize: 16.sp),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Visibility(
              visible: _cubit.meditationData.length == 0,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                child: Text(
                  R.string.you_currently_have_no_activity.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 12.sp, color: R.color.grey),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Visibility(
              visible: state is! HistoryEmpty,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                itemCount: _cubit.meditationData.length,
                itemBuilder: (BuildContext context, int index) {
                  FetchMeditationHistoriesData data =
                      _cubit.meditationData[index];
                  return buildHistoryZen(
                      title: data.meditation?.title,
                      start: data.createdDate,
                      longTime: data.duration);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildHistoryZen({String? title, int? start, int? longTime}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.h),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(R.drawable.ic_diamond, height: 32.h, width: 32.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title ?? "",
                  style: Theme.of(context)
                      .textTheme
                      .medium500
                      .copyWith(fontSize: 12.sp, height: 20 / 14),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(R.drawable.ic_bx_time, height: 16.h),
                    SizedBox(width: 4.w),
                    Expanded(
                      child: Row(
                        children: [
                          Text(
                            DateUtils.isSameDay(
                                        DateTime.fromMillisecondsSinceEpoch(
                                                start ?? 0)
                                            .toLocal(),
                                        DateTime.now()) ==
                                    true
                                ? R.string.to_day.tr()
                                : DateFormat("dd-MM-yyyy").format(
                                    DateTime.fromMillisecondsSinceEpoch(
                                        start ?? 0)),
                            style: Theme.of(context)
                                .textTheme
                                .tooltip
                                .copyWith(fontSize: 12.sp, height: 16 / 12),
                          ),
                          Expanded(
                            child: Text(
                              " - ${R.string.time.tr().toLowerCase()} " +
                                  "${Duration(seconds: longTime ?? 0).inHours % 3600 == 0 ? "" : "${Duration(seconds: longTime ?? 0).inHours % 3600} " + R.string.hour.tr().toLowerCase()} " +
                                  "${Duration(seconds: longTime ?? 0).inMinutes % 60 == 0 ? "" : "${Duration(seconds: longTime ?? 0).inMinutes % 60} " + R.string.minutes.tr().toLowerCase()} " +
                                  "${Duration(seconds: longTime ?? 0).inSeconds % 60 == 0 ? "" : "${Duration(seconds: longTime ?? 0).inSeconds % 60} " + R.string.seconds.tr().toLowerCase()}",
                              style: Theme.of(context)
                                  .textTheme
                                  .tooltip
                                  .copyWith(fontSize: 12.sp, height: 16 / 12),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildSlideTutorial() {
    return Container(
      height: 150.h,
      padding: EdgeInsets.only(left: 20.h),
      child: ListView.builder(
          itemCount: _cubit.listTutorial.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, int index) {
            NewsData data = _cubit.listTutorial[index];
            return GestureDetector(
              onTap: () {
                NavigationUtils.navigatePage(
                    context,
                    DetailZenPage(
                      postId: _cubit.listTutorial[index].id ?? 0,
                    ));
              },
              child: Container(
                width: 150.h,
                decoration: BoxDecoration(
                    color: R.color.white,
                    borderRadius: BorderRadius.circular(8.h)),
                margin: EdgeInsets.only(right: 8.h),
                padding: EdgeInsets.all(4.h),
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.h),
                      child: CachedNetworkImage(
                        width: double.infinity,
                        height: 108.h,
                        fit: BoxFit.fill,
                        imageUrl: data.imageUrl ?? "",
                        placeholder: (_, __) {
                          return Text(
                            R.string.loading.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(fontSize: 14.sp),
                          );
                        },
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 60.h,
                        width: 144.w,
                        decoration: BoxDecoration(
                          color: R.color.white,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(8.h),
                              bottomRight: Radius.circular(8.h)),
                        ),
                        child: Text(
                          data.title ?? "",
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                              color: R.color.black,
                              fontSize: 14.sp,
                              height: 24.h / 14.sp),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }

  Widget buildCreateZen(BuildContext context) {
    return GestureDetector(
      onTap: () {
        NavigationUtils.navigatePage(context, PracticeZenPage())
            .then((value) => _cubit.refreshHistory());
      },
      child: Container(
        height: 48.h,
        width: double.infinity,
        child: Stack(
          children: [
            Container(
              height: 24,
              width: double.infinity,
              color: R.color.primaryColor,
            ),
            Center(
              child: Container(
                alignment: Alignment.center,
                height: 48.h,
                margin: EdgeInsets.symmetric(horizontal: 20.w),
                //width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(24.h),
                    color: R.color.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.add,
                        size: 24.h, color: R.color.primaryColor),
                    SizedBox(width: 10.h),
                    Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 8),
                      child: Text(
                        R.string.create_meditation.tr(),
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 16.sp, color: R.color.darkerBlue),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
