import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/histories_meditation_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/home_zen/home_zen_state.dart';
import 'package:imi/src/utils/enum.dart';

class HomeZenCubit extends Cubit<HomeZenState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<NewsData> listTutorial = [];
  List<FetchMeditationHistoriesData> meditationData = [];
  String? nextToken;
  String? _nextToken;

  HomeZenCubit(this.repository, this.graphqlRepository)
      : super(HomeZenInitial());

  void getListTutorial({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? HomeZenLoading() : HomeZenInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listTutorial.clear();
    }
    ApiResult<NewsDataFetch> getLibrary = await graphqlRepository.getNews(
        typeCategory: NewsCategory.MEDITATION_GUIDE);
    getLibrary.when(success: (NewsDataFetch data) async {
      if (data.data?.length != 0) {
        listTutorial = data.data ?? [];
        nextToken = data.nextToken;
        emit(HomeZenSuccess());
      } else {
        emit(HistoryEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeZenFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListHistoriesMeditation({
    bool isRefresh = false,
    bool isLoadMore = false,
  }) async {
    emit(!isRefresh ? HomeZenLoading() : HomeZenInitial());
    if (isLoadMore != true) {
      nextToken = null;
      meditationData.clear();
    }
    ApiResult<FetchMeditationHistories> getListMeditation =
        await graphqlRepository.getHistoriesMeditation(nextToken: nextToken);
    getListMeditation.when(success: (FetchMeditationHistories data) async {
      if (data.data != null) {
        meditationData.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(HomeZenSuccess());
      } else {
        emit(HistoryEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeZenFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
  void refreshHistory(){
    emit(HomeZenLoading());
    getListHistoriesMeditation();
    emit(HomeZenSuccess());
  }
}
