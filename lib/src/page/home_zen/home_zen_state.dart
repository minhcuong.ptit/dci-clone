import 'package:equatable/equatable.dart';

abstract class HomeZenState {
  HomeZenState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class HomeZenInitial extends HomeZenState {
  @override
  String toString() => 'HomeZenInitial';
}
class HomeZenSuccess extends HomeZenState {
  @override
  String toString() => 'HomeZenSuccess';
}

class HomeZenLoading extends HomeZenState {
  @override
  String toString() => 'HomeZenLoading';
}

class HomeZenFailure extends HomeZenState {
  final String error;

  HomeZenFailure(this.error);

  @override
  String toString() => 'HomeZenFailure { error: $error }';
}

class HistoryEmpty extends HomeZenState {
  @override
  String toString() => 'HistoryEmpty';
}

