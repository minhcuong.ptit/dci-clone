import 'dart:developer';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/list_order_course_response.dart';
import 'package:imi/src/page/detail_order/detail_order_page.dart';
import 'package:imi/src/page/list_order/list_order.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../utils/navigation_utils.dart';

class ListOrderPage extends StatefulWidget {
  const ListOrderPage({Key? key}) : super(key: key);

  @override
  State<ListOrderPage> createState() => _ListOrderPageState();
}

class _ListOrderPageState extends State<ListOrderPage>
     {
  int currentIndex = 0;
  late ListOrderCubit _cubit;
  RefreshController _refreshController = RefreshController();
  final ScrollController _controller = ScrollController();
  List<String> titleTabBar = [
    R.string.wait_for_pay.tr(),
    R.string.paid.tr(),
    R.string.canceled.tr()
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ListOrderCubit(repository, graphqlRepository);
    _cubit.getListOrderCourse(
        status: OrderStatus.WAITING_PAYMENT, isRefresh: true);
  }

  // @override
  // // TODO: implement wantKeepAlive
  // bool get wantKeepAlive => true;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.order_list.tr(), centerTitle: true),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<ListOrderCubit, ListOrderState>(
          listener: (context, state) {
            if (state is ListOrderLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, ListOrderState state) {
    return StackLoadingView(
      visibleLoading: state is ListOrderLoading,
      child: SmartRefresher(
        controller: _refreshController,
        enablePullUp: true,
        onRefresh: () {
          _cubit.getListOrderCourse(
            isRefresh: true,
            status: _cubit.currentStatus,
          );
        },
        onLoading: () {
          _cubit.listOrder.length == 0 > 20
              ? _cubit.getListOrderCourse(
                  isLoadMore: true,
                  status: _cubit.currentStatus,
                )
              : null;
        },
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          children: [
            buildListView(
              state,
            ),
            SizedBox(height: 10.h),
            _cubit.listOrder.length == 0 && state is! ListOrderLoading
                ? buildOrderEmpty(state)
                : buildListOrder(state)
          ],
        ),
      ),
    );
  }

  Widget buildListView(ListOrderState state) {
    return Container(
      alignment: Alignment.center,
      height: 50.h,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
            children: titleTabBar
                .asMap()
                .entries
                .map(
                  (e) => GestureDetector(
                    onTap: () {
                      currentIndex = e.key;
                      _cubit.getStatus(e.key);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.h),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            titleTabBar[e.key],
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 16.sp,
                                    color: currentIndex == e.key
                                        ? R.color.dark_blue
                                        : R.color.secondaryColor),
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                          currentIndex == e.key
                              ? Container(
                                  color: R.color.primaryColor,
                                  height: 2.h,
                                  width: 55.w,
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                )
                .toList()),
      ),
    );
  }

  Widget buildListOrder(ListOrderState state) {
    return ListView.separated(
      padding: EdgeInsets.only(bottom: 30.h),
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      controller: _controller,
      itemCount: _cubit.listOrder.length,
      separatorBuilder: (BuildContext context, int index) => Container(
        margin: EdgeInsets.symmetric(vertical: 5.h),
        height: 1.h,
        color: R.color.grey,
      ),
      itemBuilder: (BuildContext context, int index) {
        FetchOrderHistoryData data = _cubit.listOrder[index];
        return buildItem(
            callback: () {
              NavigationUtils.navigatePage(
                      context,
                      DetailOrderPage(
                        isShowPopUp: currentIndex == 1 ? true : false,
                        orderHistoryId: data.orderHistory?.id ?? 0,
                        paymentType: data.orderHistory?.paymentType,
                      ))
                  .then((value) => _cubit.getListOrderCourse(
                      status: OrderStatus.values.firstWhereIndexedOrNull(
                              (index, element) =>
                                  element.name == data.lastAudit?.status) ??
                          OrderStatus.WAITING_PAYMENT));
            },
            orderCode: data.orderHistory?.code,
            numberProduct: data.totalItem,
            date: data.lastAudit?.createdDate,
            price: data.orderHistory?.price,
            status: data.lastAudit?.status);
      },
    );
  }

  Widget buildOrderEmpty(ListOrderState state) {
    return Visibility(
      visible: state is ListOrderLoading,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 100.h),
          Image.asset(
            R.drawable.ic_shopping,
            height: 54.h,
            width: 54.w,
          ),
          SizedBox(height: 10.h),
          Text(
            R.string.no_orders_yet.tr(),
            style: Theme.of(context).textTheme.regular400.copyWith(
                fontSize: 12.sp,
                fontWeight: FontWeight.w600,
                height: 24.h / 12.sp,
                color: R.color.darkGrey),
          )
        ],
      ),
    );
  }

  Widget buildItem(
      {String? orderCode,
      int? numberProduct,
      int? date,
      int? price,
      String? status,
      VoidCallback? callback}) {
    return InkWell(
      onTap: callback,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: RichText(
                  text: TextSpan(
                    text: R.string.order.tr() + " ",
                    style: Theme.of(context).textTheme.regular400.copyWith(
                          fontSize: 14.sp,
                          height: 20.h / 14.sp,
                        ),
                    children: <TextSpan>[
                      TextSpan(
                        text: orderCode,
                        style: Theme.of(context).textTheme.bold700.copyWith(
                              fontSize: 14.sp,
                              height: 24.h / 14.sp,
                            ),
                      ),
                    ],
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(width: 10.w),
              Icon(
                CupertinoIcons.right_chevron,
                size: 20.h,
              )
            ],
          ),
          SizedBox(height: 4.h),
          Row(
            children: [
              Expanded(
                child: Text(
                  "${numberProduct} ${R.string.product.tr()}",
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 13.sp,
                      height: 24.h / 13.sp,
                      color: R.color.darkGrey),
                ),
              ),
              SizedBox(width: 10.w),
              Text(
                DateUtil.parseDateToString(
                    DateTime.fromMillisecondsSinceEpoch(date ?? 0, isUtc: true),
                    Const.DATE_FORMAT_REQUEST),
                style: Theme.of(context).textTheme.regular400.copyWith(
                    fontSize: 13.sp,
                    height: 24.h / 13.sp,
                    color: R.color.darkGrey),
              ),
            ],
          ),
          SizedBox(height: 4.h),
          Row(
            children: [
              Expanded(
                child: RichText(
                  text: TextSpan(
                    text: R.string.into_money.tr() + " ",
                    style: Theme.of(context).textTheme.regular400.copyWith(
                          fontSize: 14.sp,
                          height: 20.h / 14.sp,
                        ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "${Utils.formatMoney(price ?? 0)} VND",
                        style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 14.sp,
                            height: 24.h / 14.sp,
                            color: R.color.orange),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 10.w),
              Text(
                status == OrderStatus.WAITING_PAYMENT.name
                    ? R.string.wait_for_pay.tr()
                    : status == OrderStatus.PAID.name
                        ? R.string.paid.tr()
                        : R.string.canceled.tr(),
                style: Theme.of(context).textTheme.regular400.copyWith(
                    fontSize: 13.sp,
                    height: 24.h / 13.sp,
                    color: status == OrderStatus.WAITING_PAYMENT.name
                        ? R.color.blue
                        : status == OrderStatus.PAID.name
                            ? R.color.green
                            : R.color.orange),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
