abstract class ListOrderState {}

class ListOrderInitial extends ListOrderState {}

class ListOrderLoading extends ListOrderState {
  @override
  String toString() {
    return 'ListOrderLoading{}';
  }
}

class ListOrderFailure extends ListOrderState {
  final String error;

  ListOrderFailure(this.error);

  @override
  String toString() {
    return 'ListOrderFailure{error: $error}';
  }
}

class ListOrderSuccess extends ListOrderState {
  @override
  String toString() {
    return 'ListOrderSuccess{}';
  }
}

class ListOrderEmpty extends ListOrderState {
  @override
  String toString() {
    return 'ListOrderEmpty{}';
  }
}
