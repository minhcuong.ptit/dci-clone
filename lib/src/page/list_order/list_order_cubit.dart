
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/list_order_course_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/list_order/list_order_state.dart';
import 'package:imi/src/utils/enum.dart';

class ListOrderCubit extends Cubit<ListOrderState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  String? nextToken;
  int currentIndex=0;
  OrderStatus currentStatus =OrderStatus.WAITING_PAYMENT;
  List<FetchOrderHistoryData> listOrder = [];
  ListOrderCubit(this.repository, this.graphqlRepository)
      : super(ListOrderInitial());

   void getStatus(int index) {
    if (index == 0) {
      currentStatus = OrderStatus.WAITING_PAYMENT;
      getListOrderCourse(status: OrderStatus.WAITING_PAYMENT,isRefresh: true);
    }
    if (index == 1) {
      currentStatus = OrderStatus.PAID;
      getListOrderCourse(status: OrderStatus.PAID,isRefresh: true);
    }
    if (index == 2) {
      currentStatus = OrderStatus.CANCELED;
      getListOrderCourse(status: OrderStatus.CANCELED,isRefresh: true);
    }
  }

  void getListOrderCourse(
      {bool isRefresh = false,
      bool isLoadMore = false,
      required OrderStatus status}) async {
    emit((isRefresh||isLoadMore) ? ListOrderLoading() : ListOrderInitial());
    listOrder.clear();
    if (isLoadMore != true) {
      nextToken = null;
      listOrder.clear();
    }
    ApiResult<FetchOrderHistoryV2> result = await graphqlRepository.getListOrder(
        status: status, nextToken: nextToken);
    result.when(success: (FetchOrderHistoryV2 data) async {
      if (data.data != null) {
        listOrder.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(ListOrderSuccess());
      } else {
        emit(ListOrderEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(ListOrderFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }
}
