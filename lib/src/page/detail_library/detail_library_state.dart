import 'package:equatable/equatable.dart';

abstract class DetailLibraryState extends Equatable {
  @override
  List<Object> get props => [];
}

class DetailLibraryInitial extends DetailLibraryState {}

class DetailLibraryLoading extends DetailLibraryState {
  @override
  String toString() {
    return 'DetailLibraryLoading{}';
  }
}

class DetailLibraryFailure extends DetailLibraryState {
  final String error;

  DetailLibraryFailure(this.error);

  @override
  String toString() {
    return 'DetailLibraryFailure{error: $error}';
  }
}

class DetailLibrarySuccess extends DetailLibraryState {
  @override
  String toString() {
    return 'DetailLibrarySuccess{}';
  }
}

class DetailLibraryEmpty extends DetailLibraryState {
  @override
  String toString() {
    return 'DetailLibraryEmpty{}';
  }
}
class SearchDocumentSuccess extends DetailLibraryState {
  @override
  String toString() {
    return 'SearchDocumentSuccess{}';
  }
}
