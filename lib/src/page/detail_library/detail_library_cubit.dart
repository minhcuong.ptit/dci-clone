import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:imi/src/data/database/app_database.dart';
import 'package:imi/src/data/database/entities/search_keyword_entity.dart';
import 'package:imi/src/data/database/sqflite/DBHelper.dart';
import 'package:imi/src/data/database/sqflite/search_suggest.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/detail_library/detail_library_state.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';

import '../../data/network/response/document_categories_response.dart';
import '../../utils/const.dart';

class DetailLibraryCubit extends Cubit<DetailLibraryState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  final db = DBHelper();
  String? nextToken;
  String? nextTokenSearch;
  bool hasMorePage = true;
  List<LibraryData> listLibrary = [];
  List<LibraryData> listSearchDocument = [];
  List<DocumentCategoriesDataFetchCategoriesByDocumentData?> category = [];
 List<SearchSuggest> listPreviousKeyword = [];
  late AppDatabase database;
  Timer? _debounce;
  bool showTextField=false;
  DetailLibraryCubit(this.repository, this.graphqlRepository)
      : super(DetailLibraryInitial());

  void initDatabase() async {
    database =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    await getPreviousSearch();
    //getPopularClub();
  }


  void searchSuggest({String? keyWord, String? type}) async {
    emit(DetailLibraryLoading());
    await db.save(SearchSuggest(keyWord: keyWord, type: Const.DETAIL_LIBRARY));
    emit(DetailLibraryInitial());
    //searchDocument(keyWord: keyWord, type: type);
    getPreviousSearch();
  }

  void deleteSearchSuggest(int id) async {
    emit(DetailLibraryLoading());
    await db.delete(id);
    emit(DetailLibraryInitial());
    getPreviousSearch();
  }

  void deleteAll() async {
    emit(DetailLibraryLoading());
    await db.deleteAll();
    emit(DetailLibraryInitial());
    getPreviousSearch();
  }

  void getDocument({
    bool isRefresh = false,
    bool isLoadMore = false,
    String? type,
    String? searchKey,
    List<int>? categoryIds
  }) async {
    emit((isRefresh || !isLoadMore)
        ? DetailLibraryLoading()
        : DetailLibraryInitial());
    if (isLoadMore != true) {
      nextToken = null;
      category.clear();
    }
    final getLibrary = await graphqlRepository.getDocumentByCategory(
      nextToken: nextToken,
      categoryIds: categoryIds,
      searchKey: searchKey,
      type: type,
    );
    getLibrary.when(success: (DocumentCategories data) async {
      category.addAll(data.data?.fetchCategoriesByDocument?.data ?? []);
      // noResults=category.where((element) => element?.categoryDocuments?.data?.isNotEmpty??false).toList().isEmpty;
      nextToken = data.data?.fetchCategoriesByDocument?.nextToken;
      emit(DetailLibrarySuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailLibraryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void searchDocument(
      {String? type,
      String? keyWord,
      bool isRefresh = false,
      bool isLoadMore = false}) async {
    emit((!isRefresh || !isLoadMore)
        ? DetailLibraryLoading()
        : DetailLibraryInitial());
    if (isLoadMore != true) {
      nextTokenSearch = null;
      listSearchDocument.clear();
    }
    ApiResult<LibraryDataFetchDocuments> getSearchDocument =
        await graphqlRepository.getLibrary(
      nextToken: nextTokenSearch,
      type: type,
      keyWord: keyWord,
    );
    getSearchDocument.when(success: (LibraryDataFetchDocuments data) async {
      listSearchDocument.addAll(data.data ?? []);
      nextTokenSearch = data.nextToken;
      emit(SearchDocumentSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(DetailLibraryFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void searching() {
    emit(DetailLibraryLoading());
    showTextField = !showTextField;
    emit(DetailLibraryInitial());
  }

  void addKeyword({String? type, String? keyword}) async {
    emit(DetailLibraryLoading());
    if (!Utils.isEmpty(keyword?.trim())) {
      var item =
          await database.searchDao.findKeywordByText(keyword!.trim()).first;
      if (item == null) {
        await database.searchDao
            .insertSearchKeyword(SearchKeywordEntity(0, keyword.trim()));
        // listPreviousKeyword.add(keyword.trim());
      }
    }
    emit(DetailLibraryInitial());
    searchDocument(type: type, keyWord: keyword);
  }

  void clearKeyword(String keyword) async {
    emit(DetailLibraryLoading());
    var item = await database.searchDao.findKeywordByText(keyword).first;
    if (item != null) {
      await database.searchDao.deleteKeyword(item);
    }
    emit(DetailLibraryInitial());
    getPreviousSearch();
  }

  void clearAllKeyword() async {
    emit(DetailLibraryLoading());
    await database.searchDao.deleteAllKeyword();
    listSearchDocument.clear();
    nextToken = null;
    emit(DetailLibraryInitial());
    getPreviousSearch();
  }

  void onSearchChanged(String text, String type) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      addKeyword(type: type, keyword: text);
    });
  }

  Future getPreviousSearch({bool isRefresh = false}) async {
    emit(isRefresh ? DetailLibraryInitial() : DetailLibraryLoading());
    listPreviousKeyword =
        await db.getSearchSuggests(keyword: "", type: Const.DETAIL_LIBRARY);
    emit(SearchDocumentSuccess());
  }
}
