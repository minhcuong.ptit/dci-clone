import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/page/detail_document_library/detail_document_library_page.dart';
import 'package:imi/src/page/detail_library/detail_library.dart';
import 'package:imi/src/page/library/widgets/category.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../utils/const.dart';
import '../../widgets/search/search_widget.dart';
import '../detail_category_library/detail_category_library.dart';
import '../home_tab/widget/item_home_page.dart';
import '../library/widgets/library_category_title.dart';

class DocumentCategoryPage extends StatefulWidget {
  final LibraryDocumentType type;
  final String? keyWord;
  final List<int>? categoryIds;

  DocumentCategoryPage({required this.type, this.keyWord, this.categoryIds});

  @override
  _DocumentCategoryPageState createState() => _DocumentCategoryPageState();
}

class _DocumentCategoryPageState extends State<DocumentCategoryPage> {
  late DetailLibraryCubit _cubit;
  RefreshController _refreshController = RefreshController();
  TextEditingController _searchController = TextEditingController();
  bool hideAppBar = false;
  late double _appBarHeight;
  bool isSearch=false;


  @override
  void initState() {
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository appGraphqlRepository = AppGraphqlRepository();
    _cubit = DetailLibraryCubit(repository, appGraphqlRepository);
    _cubit.getDocument(type: widget.type.name, searchKey: widget.keyWord,categoryIds: widget.categoryIds);
    _searchController.text = widget.keyWord ?? "";
    _appBarHeight = AppBar().preferredSize.height;
  }

  @override
  void dispose() {
    _refreshController.dispose();
    _searchController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => _cubit,
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: BlocConsumer<DetailLibraryCubit, DetailLibraryState>(
          listener: (BuildContext context, state) {
            if (state is! DetailLibraryLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
          },
          builder: (BuildContext context, DetailLibraryState state) {
            return Scaffold(
              backgroundColor: R.color.lightestGray,
              body: Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).padding.top,
                    width: double.infinity,
                    color: R.color.primaryColor,
                  ),
                  _appBar(),
                  Expanded(
                    child: StackLoadingView(
                      visibleLoading: state is DetailLibraryLoading,
                      child: SmartRefresher(
                        controller: _refreshController,
                        enablePullUp: _cubit.nextToken != null &&
                            state is DetailLibrarySuccess,
                        onRefresh: () {
                          _cubit.getDocument(
                              type: widget.type.name,
                              categoryIds: widget.categoryIds,
                              isRefresh: true,
                              searchKey: isSearch?_searchController.text:widget.keyWord);
                        },
                        onLoading: () {
                          _cubit.getDocument(
                              type: widget.type.name,
                              categoryIds: widget.categoryIds,
                              isLoadMore: true,
                              searchKey:isSearch?_searchController.text:widget.keyWord);
                        },
                        child: state is DetailLibraryLoading
                            ? const SizedBox.shrink()
                            : buildListDocument(context),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget buildListDocument(BuildContext context) {
    return _cubit.category.isEmpty
        ? Center(
            child: Text(
              R.string.no_result_search.tr(),
              style: Theme.of(context).textTheme.bodyRegular,
            ),
          )
        : ListView.builder(
            shrinkWrap: true,
            itemCount: _cubit.category.length,
            itemBuilder: (context, index) {
              var category = _cubit.category[index];
              return category?.categoryDocuments?.data?.length == 0
                  ? const SizedBox.shrink()
                  : Padding(
                      padding: EdgeInsets.only(left: 20, right: 10,top: index==0?20:0,bottom: index==_cubit.category.length-1?20:0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CategoryTitle(
                            title: category?.category?.name ?? '',
                            showMore: () => NavigationUtils.navigatePage(
                              context,
                              DetailCategoryLibrary(
                                searchKey: _searchController.text,
                                libraryDocumentType: widget.type,
                                categoryId: category?.category?.id??0,
                             ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          SizedBox(
                            height: 200.h,
                            child: ListView.builder(
                                physics: const BouncingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    category?.categoryDocuments?.data?.length ??
                                        0,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, int index) {
                                  var documentByCategory =
                                      category?.categoryDocuments?.data;
                                  documentByCategory?.sort((a, b) => b!.priority!.compareTo(a!.priority??0.toInt()));
                                  return GestureDetector(
                                    onTap: () {
                                      NavigationUtils.navigatePage(
                                        context,
                                        DetailDocumentPage(
                                          link:
                                              documentByCategory?[index]?.link,
                                          id: documentByCategory?[index]?.id,
                                          isLocked: documentByCategory?[index]
                                              ?.isLocked,
                                        ),
                                      );
                                    },
                                    child: CardItem(
                                        containDate: false,
                                        isVideo:documentByCategory?[index]
                                            ?.type==DocumentType.VIDEO.name,
                                        isEbook: documentByCategory?[index]
                                            ?.type==DocumentType.E_BOOK.name,
                                        isLockEbook: documentByCategory?[index]
                                            ?.isLocked,
                                        isLockVideo: documentByCategory?[index]
                                            ?.isLocked,
                                        typeVideo:documentByCategory?[index]
                                            ?.type==DocumentType.VIDEO.name,
                                        image: documentByCategory?[index]
                                            ?.imageUrl,
                                        title:
                                            documentByCategory?[index]?.title,
                                        time: documentByCategory?[index]
                                            ?.modifiedDate),
                                  );
                                }),
                          ),
                        ],
                      ),
                    );
            });
  }

  Widget _appBar() {
    return !_cubit.showTextField
        ? Container(
            height: _appBarHeight,
            width: double.infinity,
            decoration: BoxDecoration(color: R.color.primaryColor, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.h), blurRadius: 1.h, color: R.color.gray)
            ]),
            child: Row(
              children: [
                SizedBox(
                  width: 16.w,
                ),
                GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: R.color.white,
                    size: 20.h,
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Text(
                     widget.type.title,
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 16.sp, color: R.color.white),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _cubit.searching();
                  },
                  child: Image.asset(R.drawable.ic_search,
                      width: 20.h, height: 20.h, color: R.color.white),
                ),
                SizedBox(width: 16.w),
              ],
            ),
          )
        : Stack(
            children: [
              Container(
                width: double.infinity,
                height: _appBarHeight,
                decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 2.h),
                          blurRadius: 1.h,
                          color: R.color.gray)
                    ]),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.h, top: 20.h),
                    child: GestureDetector(
                      onTap: () {
                        NavigationUtils.pop(context,
                            result: Const.ACTION_REFRESH);
                      },
                      child: Icon(CupertinoIcons.back, color: R.color.white),
                    ),
                  ),
                  Expanded(
                    child: SearchWidget(
                      autoFocus: false,
                      onSubmit: (text) {
                        _cubit.getDocument(
                          categoryIds: widget.categoryIds,
                            searchKey: _searchController.text,
                            type: widget.type.name,
                            isRefresh: true);
                        isSearch=true;
                      },
                      searchController: _searchController,
                      type: 'CATEGORY_LIBRARY',
                      onChange: (String) {},
                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
