import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/page/list_discount/list_discount.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/coupons_response.dart';
import '../detail_coupons/detail_coupons_page.dart';

class ListDiscountPage extends StatefulWidget {
  final int? orderId;

  ListDiscountPage(this.orderId);

  @override
  State<ListDiscountPage> createState() => _ListDiscountPageState();
}

class _ListDiscountPageState extends State<ListDiscountPage> {
  RefreshController _refreshController = RefreshController();
  late ListDiscountCubit _cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = ListDiscountCubit(repository, graphqlRepository);
    _cubit.getListOrderCourse();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, R.string.discount_code_for_you.tr(),
          centerTitle: true,
          backgroundColor: R.color.primaryColor,
          iconColor: R.color.white,
          titleColor: R.color.white),
      body: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<ListDiscountCubit, ListDiscountState>(
          listener: (context, state) {
            // TODO: implement listener
            if (state is! ListDiscountLoading) {
              _refreshController.refreshCompleted();
              _refreshController.loadComplete();
            }
            if (state is ApplyCouponFailure) {
              NavigationUtils.navigatePage(
                  context,
                  DetailCouponsPage(_cubit.couponId ?? 0, widget.orderId,
                      checkErrorCoupons: true));
            }
            if (state is ApplyCouponSuccess) {
              NavigationUtils.pop(context, result: _cubit.codeCoupons);
            }
          },
          builder: (context, state) {
            return buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget buildPage(BuildContext context, ListDiscountState state) {
    return StackLoadingView(
      visibleLoading: state is ListDiscountLoading,
      child: RefreshConfiguration(
        enableScrollWhenRefreshCompleted: false,
        child: SmartRefresher(
          controller: _refreshController,
          enablePullUp: true,
          onRefresh: () {
            _cubit.getListOrderCourse(isRefresh: true);
          },
          onLoading: () {
            _cubit.getListOrderCourse(isLoadMore: true);
          },
          child: ListView.separated(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(vertical: 10),
            itemCount: _cubit.listCoupons.length,
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                height: 1,
                color: R.color.lightGray,
                margin: EdgeInsets.symmetric(vertical: 8),
              );
            },
            itemBuilder: (BuildContext context, int index) {
              FetchCouponsData data = _cubit.listCoupons[index];
              return Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: Image.asset(R.drawable.ic_discount_code,
                              height: 70)),
                      SizedBox(width: 10.w),
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                data.code ?? "",
                                style: Theme.of(context)
                                    .textTheme
                                    .medium500
                                    .copyWith(
                                        fontSize: 13.sp, color: R.color.black),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(width: 10.w),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ButtonWidget(
                                    title: R.string.apply_code.tr(),
                                    uppercaseTitle: false,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 8),
                                    textSize: 13,
                                    backgroundColor: data.isValid == false
                                        ? R.color.grey
                                        : R.color.primaryColor,
                                    onPressed: () {
                                      _cubit.applyCoupon(
                                          discountCode: data.code ?? "",
                                          orderId: widget.orderId ?? 0,
                                          couponId: data.id ?? 0);
                                    }),
                                InkWell(
                                  onTap: () {
                                    NavigationUtils.navigatePage(
                                        context,
                                        DetailCouponsPage(
                                            data.id ?? 0, widget.orderId));
                                  },
                                  highlightColor: R.color.white,
                                  child: Column(
                                    children: [
                                      Text(
                                        R.string.condition.tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .medium500
                                            .copyWith(
                                              fontSize: 13.sp,
                                              color: R.color.primaryColor,
                                              height: 24.h / 13.sp,
                                            ),
                                      ),
                                      Container(
                                        height: 1,
                                        width: 60,
                                        color: R.color.primaryColor,
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ));
            },
          ),
        ),
      ),
    );
  }
}
