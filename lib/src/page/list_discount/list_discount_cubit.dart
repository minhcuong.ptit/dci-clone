import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/request/apply_coupon_request.dart';
import 'package:imi/src/data/network/response/coupons_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/list_discount/list_discount_state.dart';

import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../utils/const.dart';

class ListDiscountCubit extends Cubit<ListDiscountState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  String? nextToken;
  List<FetchCouponsData> listCoupons = [];
  String? codeCoupons;
  int? couponId;
  bool disableLoadMore = false;

  ListDiscountCubit(this.repository, this.graphqlRepository)
      : super(ListDiscountInitial());

  void getListOrderCourse(
      {bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || !isLoadMore)
        ? ListDiscountLoading()
        : ListDiscountInitial());
    if (isLoadMore != true) {
      nextToken = null;
      listCoupons.clear();
    }
    ApiResult<FetchCoupons> result =
        await graphqlRepository.getListCoupons(nextToken: nextToken);
    result.when(success: (FetchCoupons data) async {
      if (data.data != null) {
        listCoupons.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(ListDiscountSuccess());
      } else {
        emit(ListDiscountEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(ListDiscountFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void applyCoupon({required String discountCode, required int orderId, required int couponId}) async {
    this.codeCoupons = discountCode;
    this.couponId =couponId;
    emit(ListDiscountLoading());
    final result = await repository.applyCoupon(
        ApplyCouponRequest(couponCode: discountCode),
        orderId: orderId);
    result.when(success: (data) {
      emit(ApplyCouponSuccess());
    },failure: (NetworkExceptions error) async {
      emit(ApplyCouponFailure());
    });
  }
}
