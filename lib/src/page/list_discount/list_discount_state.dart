abstract class ListDiscountState {}

class ListDiscountInitial extends ListDiscountState {}

class ListDiscountLoading extends ListDiscountState {
  @override
  String toString() {
    return 'ListDiscountLoading{}';
  }
}

class ListDiscountFailure extends ListDiscountState {
  final String error;

  ListDiscountFailure(this.error);

  @override
  String toString() {
    return 'ListDiscountFailure{error: $error}';
  }
}

class ListDiscountSuccess extends ListDiscountState {
  @override
  String toString() {
    return 'ListDiscountSuccess{}';
  }
}

class ListDiscountEmpty extends ListDiscountState {
  @override
  String toString() {
    return 'ListDiscountEmpty{}';
  }
}
class ApplyCouponSuccess extends ListDiscountState {
  @override
  String toString() {
    return 'ApplyCouponSuccess{}';
  }
}

class ApplyCouponFailure extends ListDiscountState {
  @override
  String toString() {
    return 'ApplyCouponFailure{}';
  }
}