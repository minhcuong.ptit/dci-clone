import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../data/network/response/community_data.dart';
import '../../widgets/custom_appbar.dart';
import 'study_group_invite.dart';

class StudyGroupInvitePage extends StatefulWidget {
  final int clubCommunityId;

  const StudyGroupInvitePage({Key? key, required this.clubCommunityId})
      : super(key: key);

  @override
  _StudyGroupInvitePageState createState() => _StudyGroupInvitePageState();
}

class _StudyGroupInvitePageState extends State<StudyGroupInvitePage> {
  late StudyGroupInviteCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  final ScrollController scrollController = ScrollController();
  TextEditingController _searchController = TextEditingController();
  Timer? _debounce;

  @override
  void initState() {
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = StudyGroupInviteCubit(
        repository: repository,
        graphqlRepository: graphqlRepository,
        clubCommunityId: widget.clubCommunityId);
    _cubit.getListContent(communityId: widget.clubCommunityId);
    scrollController.addListener(_onScroll);
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    var _positionPixels = scrollController.position;
    if (_positionPixels.atEdge &&
        _positionPixels.pixels != 0 &&
        _cubit.canRequestLoadMore) {
      _cubit.getListContent(loadMore: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: BlocProvider(
        create: (context) => _cubit,
        child: BlocConsumer<StudyGroupInviteCubit, StudyGroupInviteState>(
          listener: (context, state) {
            if (state is! StudyGroupInviteLoading) {
              _refreshController.refreshCompleted();
            }
            if (state is! SubmitStudyGroupInviteSuccess) {}
            if (state is StudyGroupInviteFailure)
              Utils.showErrorSnackBar(context, state.error);
          },
          builder: (context, state) {
            return _buildPage(context, state);
          },
        ),
      ),
    );
  }

  Widget _buildPage(BuildContext context, StudyGroupInviteState state) {
    return Scaffold(
      backgroundColor: R.color.white,
      appBar: appBar(context, R.string.invite_member.tr(),
          backgroundColor: R.color.white,
          iconColor: R.color.black,
          elevation: 0,
          centerTitle: false,
          textStyle: Theme.of(context)
              .textTheme
              .bodyBold
              .apply(color: R.color.black)
              .copyWith(fontSize: 13.sp, fontWeight: FontWeight.w700),
          rightWidget: InkWell(
            onTap: () {
              _cubit.submitStudyGroupInvite();
            },
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.w),
                  child: Container(
                    width: 22.w,
                    height: 22.h,
                    color: R.color.blueB4,
                    child: Center(
                        child: Text(
                      '${_cubit.listUserSelected.length}',
                      style: Theme.of(context)
                          .textTheme
                          .bodySmallText
                          .apply(color: R.color.white)
                          .copyWith(
                              fontSize: 10.sp,
                              fontWeight: FontWeight.w400,
                              height: 1.2),
                    )),
                  ),
                ),
                SizedBox(
                  width: 10.w,
                ),
                Text(
                  R.string.invite.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bodySmallText
                      .apply(
                          color: _cubit.listUserSelected.length == 0
                              ? R.color.lightShadesGray
                              : R.color.blueB4)
                      .copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w700,
                          height: 1.4),
                ),
                SizedBox(
                  width: 16.w,
                )
              ],
            ),
          )),
      body: StackLoadingView(
        visibleLoading: state is StudyGroupInviteLoading,
        child: Column(
          children: [
            Expanded(
              child: _buildBody(state),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody(StudyGroupInviteState state) {
    return Container(
      color: R.color.grey100.withOpacity(0.3),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 16.h,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 24.h),
            decoration: BoxDecoration(
                color: R.color.white,
                border: Border(
                    bottom: BorderSide(color: R.color.lighterGray, width: 1))),
            child: Row(
              children: [
                Expanded(
                  child: CupertinoSearchTextField(
                    decoration: BoxDecoration(color: R.color.white),
                    // autoFocus: false,
                    controller: _searchController,
                    prefixIcon: SizedBox(),
                    placeholder: R.string.search_member.tr(),
                    style: Theme.of(context).textTheme.bodyRegular.copyWith(
                          color: R.color.black,
                        ),
                    onChanged: _onSearchChanged,
                    onSubmitted: (text) {
                      _cubit.searchMember(widget.clubCommunityId, text);
                      // Utils.hideKeyboard(context);
                      FocusScope.of(context).requestFocus(new FocusNode());
                      //  Utils.hideKeyboard(context);
                    },
                  ),
                ),
                InkWell(
                    onTap: () {
                      _cubit.search(_searchController.text.trim());
                    },
                    child: Icon(CupertinoIcons.search,
                        size: 20.h, color: R.color.black))
              ],
            ),
          ),
          // Padding(
          //   padding: EdgeInsets.symmetric(horizontal: 24.w),
          //   child: SearchBar(
          //     searchBarType: SearchBarType.ICON_RIGHT,
          //     title: R.string.search_member.tr(),
          //     onChanged: _onSearchChanged,
          //     onTapSearch: (String value) {
          //       _cubit.search(value);
          //       Utils.hideKeyboard(context);
          //     },
          //   ),
          // ),
          SizedBox(
            height: 16.h,
          ),
          Expanded(
            child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () => _cubit.getListContent(isRefresh: true),
                child: Utils.isEmpty(_searchController.text)
                    ? listMember(state)
                    : listSearchMember(state)),
          ),
        ],
      ),
    );
  }

  Widget listMember(StudyGroupInviteState state) {
    List<CommunityDataV2> listMember = _cubit.listUser;
    bool emptySearch = Utils.isEmpty(listMember);
    return  ListView.separated(
            controller: scrollController,
            primary: false,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: _cubit.listUser.length + (_cubit.hasNext ? 1 : 0),
            itemBuilder: (context, index) {
              if (index < _cubit.listUser.length) {
                CommunityDataV2 data = _cubit.listUser[index];
                bool isSelected = _cubit.listUserSelected.indexWhere(
                        (element) =>
                            element.ownerProfile?.userUuid ==
                            data.ownerProfile?.userUuid) >=
                    0;
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.h),
                  child: ListTile(
                    title: Row(
                      children: [
                        Image.asset(
                          isSelected
                              ? R.drawable.ic_checkbox_on
                              : R.drawable.ic_checkbox_off,
                          width: 24.w,
                        ),
                        SizedBox(
                          width: 10.w,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(data.name ?? '',
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmallText
                                    .apply(color: R.color.black)
                                    .copyWith(
                                        fontSize: 11.sp,
                                        fontWeight: FontWeight.w700)),
                            Text(R.string.individual.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmallText
                                    .apply(color: R.color.darkGray)
                                    .copyWith(
                                        fontSize: 10.sp,
                                        fontWeight: FontWeight.w400)),
                          ],
                        ),
                      ],
                    ),
                    trailing: Image.asset(
                      R.drawable.ic_chevron_right,
                      width: 24.w,
                    ),
                    onTap: () {
                      _cubit.selectUser(data);
                    },
                  ),
                );
              }
              return Container(
                width: double.infinity,
                height: 50.h,
                color: R.color.grey200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        width: 18.w,
                        height: 18.w,
                        child: CircularProgressIndicator(
                          color: R.color.orange,
                          strokeWidth: 3.w,
                        )),
                    SizedBox(
                      width: 20.w,
                    ),
                    Text(R.string.loading.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .labelSmallText
                            .copyWith(fontSize: 17.sp)),
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 16.w),
                width: double.infinity,
                color: R.color.grey300,
                height: 0.2.h,
              );
            },
          );
  }

  Widget listSearchMember(StudyGroupInviteState state) {
    List<CommunityDataV2> listMember = _cubit.listUserNoneSearch;
    bool emptySearch = Utils.isEmpty(listMember);
    return emptySearch
        ? Visibility(
      visible: state is GetListCommunitySuccess,
      child: Center(child: Text(R.string.empty_member.tr())),
    )
        :
      ListView.separated(
      controller: scrollController,
      primary: false,
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: _cubit.listUserNoneSearch.length,
      itemBuilder: (context, index) {
        if (index < _cubit.listUserNoneSearch.length) {
          CommunityDataV2 data = _cubit.listUserNoneSearch[index];
          bool isSelected = _cubit.listUserSelected.indexWhere((element) =>
                  element.ownerProfile?.userUuid ==
                  data.ownerProfile?.userUuid) >=
              0;
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 8.h),
            child: ListTile(
              title: Row(
                children: [
                  Image.asset(
                    isSelected
                        ? R.drawable.ic_checkbox_on
                        : R.drawable.ic_checkbox_off,
                    width: 24.w,
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(data.name ?? '',
                          style: Theme.of(context)
                              .textTheme
                              .labelSmallText
                              .apply(color: R.color.black)
                              .copyWith(
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.w700)),
                      Text(R.string.individual.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .labelSmallText
                              .apply(color: R.color.darkGray)
                              .copyWith(
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w400)),
                    ],
                  ),
                ],
              ),
              trailing: Image.asset(
                R.drawable.ic_chevron_right,
                width: 24.w,
              ),
              onTap: () {
                _cubit.selectUser(data);
              },
            ),
          );
        }
        return Container(
          width: double.infinity,
          height: 50.h,
          color: R.color.grey200,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: 18.w,
                  height: 18.w,
                  child: CircularProgressIndicator(
                    color: R.color.orange,
                    strokeWidth: 3.w,
                  )),
              SizedBox(
                width: 20.w,
              ),
              Text(R.string.loading.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .labelSmallText
                      .copyWith(fontSize: 17.sp)),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 16.w),
          width: double.infinity,
          color: R.color.grey300,
          height: 0.2.h,
        );
      },
    );
  }

  void _onSearchChanged(String text) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      _cubit.searchMember(widget.clubCommunityId, text.trim());
    });
  }
}
