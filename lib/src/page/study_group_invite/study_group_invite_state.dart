import 'package:flutter/material.dart';

@immutable
abstract class StudyGroupInviteState {
  StudyGroupInviteState([List props = const []]) : super();

  @override
  List<Object> get props => [];
}

class InitialStudyGroupInviteState extends StudyGroupInviteState {}

class StudyGroupInviteLoading extends StudyGroupInviteState {
  @override
  String toString() => 'StudyGroupInviteLoading';
}

class GetListCategorySuccess extends StudyGroupInviteState {
  @override
  String toString() {
    return 'GetListDataSuccess';
  }
}

class GetListCommunitySuccess extends StudyGroupInviteState {
  @override
  String toString() {
    return 'GetListCommunitySuccess';
  }
}

class StudyGroupInviteSuccess extends StudyGroupInviteState {
  @override
  String toString() {
    return 'StudyGroupInviteSuccess';
  }
}

class ChooseCommunitySuccess extends StudyGroupInviteState {
  @override
  String toString() {
    return 'ChooseCommunitySuccess';
  }
}

class StudyGroupInviteFailure extends StudyGroupInviteState {
  final String error;

  StudyGroupInviteFailure(this.error);

  @override
  String toString() => 'BodyParameterFailure { error: $error }';
}

class SubmitStudyGroupInviteSuccess extends StudyGroupInviteState {
  @override
  String toString() {
    return 'SubmitStudyGroupInviteSuccess{}';
  }
}

class SearchMemberSuccess extends StudyGroupInviteState {
  @override
  String toString() {
    return 'SearchMemberSuccess{}';
  }
}
