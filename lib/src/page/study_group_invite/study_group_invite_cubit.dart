import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/utils/utils.dart';

import '../../data/network/request/group_member_request.dart';
import '../../data/network/response/community_data.dart';
import '../../data/network/response/list_dci_community_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/enum.dart';
import 'study_group_invite.dart';

class StudyGroupInviteCubit extends Cubit<StudyGroupInviteState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  List<CommunityDataV2> listUser = [];
  List<CommunityDataV2> listUserNoneSearch = [];
  List<CommunityDataV2> listUserSelected = [];
  AppPreferences appPreferences = AppPreferences();
  final int clubCommunityId;

  String? nextToken;
  bool hasNext = false;
  bool _isLoading = false;
  bool get canRequestLoadMore => !_isLoading && hasNext;
  String? _searchKey;
  bool _isValid = true;
  StudyGroupInviteCubit(
      {required this.clubCommunityId,
      required this.repository,
      required this.graphqlRepository})
      : super(InitialStudyGroupInviteState());

  void searchMember(int? communityId, String? keyword) async {
    emit(StudyGroupInviteLoading());
    if (Utils.isEmpty(keyword)) {
      listUserNoneSearch.clear();
      emit(SearchMemberSuccess());
    } else {
      ApiResult<ListDCICommunityResponse> apiResult =
          await graphqlRepository.getDCICommunities(CommunityType.INDIVIDUAL,
              communityId: communityId, searchKey: keyword);
      apiResult.when(success: (data) async {
        // listUserNoneSearch.addAll(data.dciCommunities?.data ?? []);
        listUserNoneSearch = data.dciCommunities?.data ?? [];
        emit(GetListCommunitySuccess());
      }, failure: (error) async {
        emit(StudyGroupInviteFailure(NetworkExceptions.getErrorMessage(error)));
      });
    }
  }

  void getListContent({
    int? communityId,
    bool isRefresh = false,
    bool loadMore = false,
  }) async {
    if (isRefresh) {
      hasNext = false;
      nextToken = null;
      listUser.clear();
    }
    emit((isRefresh || loadMore)
        ? InitialStudyGroupInviteState()
        : StudyGroupInviteLoading());
    _isLoading = true;
    ApiResult<ListDCICommunityResponse> contentTask =
        await graphqlRepository.getDCICommunities(
      CommunityType.INDIVIDUAL,
      nextToken: nextToken,
      communityId: communityId,
    );
    contentTask.when(success: (ListDCICommunityResponse data) async {
      listUser.addAll(data.dciCommunities?.data ?? []);
      /*hasNext = (data.dciCommunities?.data ?? []).length >= limit;*/
      nextToken = data.dciCommunities?.nextToken;
      _isLoading = false;
      emit(GetListCommunitySuccess());
    }, failure: (NetworkExceptions error) async {
      if (listUser.isNotEmpty) {
        hasNext = false;
      }
      _isLoading = false;
      emit(StudyGroupInviteFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void search(String str) {
    emit(StudyGroupInviteLoading());
    if (_searchKey != str) {
      _searchKey = str;
      if (str.isNotEmpty) {
        getListContent(isRefresh: true, communityId: clubCommunityId);
      } else {
        getListContent(isRefresh: true, communityId: clubCommunityId);
      }
    }
    emit(SearchMemberSuccess());
  }

  void selectUser(CommunityDataV2 selectedCommunity) {
    emit(StudyGroupInviteLoading());
    int index = listUserSelected.indexWhere((element) =>
        element.ownerProfile?.userUuid ==
        selectedCommunity.ownerProfile?.userUuid);
    if (index >= 0) {
      listUserSelected.removeAt(index);
    } else {
      listUserSelected.add(selectedCommunity);
    }
    emit(StudyGroupInviteSuccess());
  }

  void submitStudyGroupInvite() async {
    List<String> listSelectedCommunityId =
        listUserSelected.map((e) => e.ownerProfile?.userUuid ?? '').toList();
    if (listSelectedCommunityId.isNotEmpty) {
      emit(StudyGroupInviteLoading());
      final res = await repository.requestToJoinGroup(
          GroupMemberActionRequest(
              userUuid: listSelectedCommunityId, clubId: clubCommunityId),
          memberActionType: MemberActionType.INVITE);
      res.when(success: (response) {
        listUserSelected.clear();
        getListContent(isRefresh: true);
      }, failure: (error) {
        emit(StudyGroupInviteFailure(NetworkExceptions.getErrorMessage(error)));
      });
    }
  }
}
