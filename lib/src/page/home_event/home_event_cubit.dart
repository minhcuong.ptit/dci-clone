import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/page/home_event/home_event_state.dart';

import '../../../res/R.dart';
import '../../data/database/app_database.dart';
import '../../data/database/sqflite/DBHelper.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/event_response.dart';
import '../../data/network/response/event_topics_response.dart';
import '../../data/network/response/fetch_event_data.dart';
import '../../data/network/response/product_topic_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/network/service/network_exceptions.dart';

class HomeEventCubit extends Cubit<HomeEventState> {
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool isSearch = true;
  List<FetchEventsData> listEventSearch = [];
  ValueNotifier<List<bool>> radioChoiceTemp =
      ValueNotifier([true, false, false, false]);
  ValueNotifier<List<bool>> radioChoice =
      ValueNotifier([true, false, false, false]);
  ValueNotifier<bool> loadingSearch = ValueNotifier(false);
  List<bool> choiceChip = [];
  List<EventTopicsDataFetchCourseTopicsData?> productTopic = [];
  List<int> topicSearch = [];
  List<String> title = [
    R.string.lastest.tr(),
    R.string.sort_by_popularity.tr(),
    R.string.outstanding.tr(),
    R.string.similar.tr()
  ];
  String selectedMenu = R.string.lastest.tr();
  late AppDatabase database;
  final db = DBHelper();
  String? nextToken;
  String? nextTokenSearch;
  bool hasMorePage = true;
  bool isOrder = false;
  int isSelect = 0;
  List<int> topicSearchSimilar = [];

  HomeEventCubit(
      this.repository, this.graphqlRepository, this.topicSearchSimilar)
      : super(HomeEventInitial());

  void checkSearch() {
    emit(HomeEventLoading());
    this.isSearch = !isSearch;
    emit(HomeEventInitial());
  }

  void searchEvent(
      {bool isRefresh = false,
      bool isLoadMore = false,
      String? keyWord,
      String? priorityOrder,
      bool? isHot,
      List<int>? topicIds,
      String? priceOrder,
      int? couponId}) async {
    emit(isRefresh || !isLoadMore ? HomeEventLoading() : HomeEventInitial());
    if (isLoadMore != true) {
      nextTokenSearch = null;
      listEventSearch.clear();
    }
    ApiResult<FetchEvents> getSearchCourse = await graphqlRepository.getEvent(
        isHot: isHot,
        topicIds: topicIds,
        nextToken: nextTokenSearch,
        searchKey: keyWord != "\\" ? keyWord : keyWord?.replaceAll("\\", "\t"),
        priorityOrder: priorityOrder,
        priceOrder: priceOrder,
        couponId: couponId);
    getSearchCourse.when(success: (FetchEvents data) async {
      listEventSearch.addAll(data.data ?? []);
      nextTokenSearch = data.nextToken;
      emit(SearchHomeEventSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(HomeEventFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getProductTopic({
    String? searchKey,
  }) async {
    loadingSearch.value = true;
    ApiResult<EventTopicsData> getSearchCourse =
        await graphqlRepository.getEventTopics(searchKey: searchKey);
    getSearchCourse.when(success: (EventTopicsData data) async {
      topicSearch.clear();
      productTopic.clear();
      productTopic = (data.fetchEventTopics?.data ?? []);
      choiceChip = List.generate(productTopic.length, (index) => false);
      loadingSearch.value = false;
      for (int i = 0; i < productTopic.length; i++) {
        for (int j = 0; j < topicSearchSimilar.length; j++) {
          if (productTopic[i]?.id == topicSearchSimilar[j]) {
            choiceChip.add(choiceChip[i] = true);
          }
        }
      }
    }, failure: (NetworkExceptions error) async {
      emit(HomeEventFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void onOrder() {
    emit(HomeEventLoading());
    isOrder = true;
    emit(HomeEventSuccess());
  }

  void selectFilter(int index) {
    emit(HomeEventLoading());
    isSelect = index;
    for (int i = 0; i < radioChoiceTemp.value.length; i++) {
      if (index == i) {
        radioChoiceTemp.value[i] = true;
        continue;
      }
      radioChoiceTemp.value[i] = false;
    }
    radioChoice.value = List.from(radioChoiceTemp.value);
    emit(HomeEventInitial());
  }

  void getSearchTopic() {
    topicSearch.clear();
    for (int i = 0; i < productTopic.length; i++) {
      if (choiceChip[i]) {
        topicSearch.add(productTopic[i]?.id ?? 0);
      }
    }
  }

  void changePopMenuTitle(int index) {
    emit(getPopupMenu());
    selectedMenu = title[index];
    emit(HomeEventInitial());
  }

  void clearFilter() {
    topicSearchSimilar.clear();
    choiceChip = choiceChip.map((e) => e = false).toList();
  }
}
