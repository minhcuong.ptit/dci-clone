import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/page/home_event/home_event_cubit.dart';
import 'package:imi/src/page/home_event/home_event_state.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:developer';
import 'dart:ui' as ui;
import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/fetch_event_data.dart';
import '../../utils/const.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/utils.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/search/search_widget.dart';
import '../../widgets/stack_loading_view.dart';
import '../detail_event/detail_event.dart';

class HomeEventPage extends StatefulWidget {
  final int? couponId;
  final String? codeCoupons;
  late int? selected;
  final List<int>? topicSearch;

  HomeEventPage(
      {this.couponId, this.codeCoupons, this.selected, this.topicSearch});

  @override
  State<HomeEventPage> createState() => _HomeEventPageState();
}

class _HomeEventPageState extends State<HomeEventPage> {
  late HomeEventCubit _cubit;
  RefreshController _refreshController = RefreshController();
  TextEditingController _searchTopicController = TextEditingController();
  TextEditingController _searchController = TextEditingController();
  bool hideAppBar = false;
  String _selectedMenu = R.string.lastest.tr();
  late double _appBarHeight;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit =
        HomeEventCubit(repository, graphqlRepository, widget.topicSearch ?? []);
    if (widget.selected == 2) {
      _cubit.changePopMenuTitle(2);
      _cubit.radioChoice.value[0] = false;
      _cubit.radioChoice.value[2] = true;
      _cubit.searchEvent(keyWord: _searchController.text.trim(), isHot: true);
    } else if (widget.selected == 3) {
      _cubit.isSelect = 3;
      // for (int i = 0; i < _cubit.productTopic.length; i++) {
      //   for (int j = 0; j < widget.topicSearch!.length; j++) {
      //     if (_cubit.productTopic[i]?.id == widget.topicSearch?[j]) {
      //       _cubit.choiceChip.add(_cubit.choiceChip[i] = true);
      //       continue;
      //     }
      //     _cubit.choiceChip.add(_cubit.choiceChip[i] = false);
      //   }
      // }
      _cubit.changePopMenuTitle(3);
      _cubit.radioChoice.value[0] = false;
      _cubit.radioChoice.value[3] = true;
      _cubit.searchEvent(
          keyWord: _searchController.text.trim(), topicIds: widget.topicSearch);
    } else {
      _cubit.searchEvent(
        isRefresh: true,
      );
    }

    _cubit.getProductTopic();
    _appBarHeight = AppBar().preferredSize.height;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _searchTopicController.dispose();
    _searchController.dispose();
    _refreshController.dispose();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<HomeEventCubit, HomeEventState>(
        listener: (context, state) {
          if (state is HomeEventFailure) {
            Utils.showErrorSnackBar(context, state.error);
          }
          if (state is! HomeEventLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
          }
          // TODO: implement listener
        },
        builder: (context, state) {
          return buildPage(context, state);
        },
      ),
    );
  }

  Widget buildPage(BuildContext context, HomeEventState state) {
    return Scaffold(
        backgroundColor: R.color.lightestGray,
        body: StackLoadingView(
          visibleLoading: state is HomeEventLoading,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: MediaQuery.of(context).padding.top,
                width: double.infinity,
                color: R.color.primaryColor,
              ),
              _appBar(),
              Padding(
                padding: widget.codeCoupons == null
                    ? EdgeInsets.symmetric(horizontal: 16.w, vertical: 12.h)
                    : EdgeInsets.only(left: 16.w, right: 16.w, top: 12.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        _cubit.selectedMenu,
                        style: Theme.of(context).textTheme.medium500.copyWith(
                            fontSize: 16.sp,
                            color: R.color.neutral1,
                            height: 20 / 16),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    SizedBox(width: 10.w),
                    buildPopupMenu(),
                  ],
                ),
              ),
              if (widget.codeCoupons != null) ...[
                Padding(
                  padding: EdgeInsets.only(left: 16.w, bottom: 10.h),
                  child: Text(
                    "${R.string.discount_code.tr()}" " ${widget.codeCoupons}",
                    textAlign: TextAlign.start,
                    style: Theme.of(context)
                        .textTheme
                        .regular400
                        .copyWith(fontSize: 10.sp, color: R.color.gray),
                  ),
                )
              ],
              Expanded(
                child: RefreshConfiguration(
                  enableScrollWhenRefreshCompleted: false,
                  child: SmartRefresher(
                    enablePullUp: _cubit.nextTokenSearch != null &&
                        state is SearchHomeEventSuccess,
                    controller: _refreshController,
                    onRefresh: () {
                      if (_cubit.isSelect == 0 &&
                          (widget.selected != 2 && widget.selected != 3)) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            isRefresh: true);
                      } else if (_cubit.isSelect == 1) {
                        _cubit.searchEvent(
                            priorityOrder: Order.DESC.name,
                            keyWord: _searchController.text.trim(),
                            isRefresh: true);
                      } else if (_cubit.isSelect == 2 || widget.selected == 2) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            isHot: true,
                            isRefresh: true);
                      } else if (_cubit.isSelect == 3 || widget.selected == 3) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            topicIds: widget.selected == 3
                                ? widget.topicSearch
                                : _cubit.topicSearch,
                            isRefresh: true);
                      }
                    },
                    onLoading: () {
                      if (_cubit.isSelect == 0 &&
                          (widget.selected != 2 && widget.selected != 3)) {
                        _cubit.searchEvent(
                            priorityOrder: Order.DESC.name,
                            keyWord: _searchController.text.trim(),
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 1) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 2 || widget.selected == 2) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            isHot: true,
                            isLoadMore: true);
                      } else if (_cubit.isSelect == 3 || widget.selected == 3) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            topicIds: widget.selected == 3
                                ? widget.topicSearch
                                : _cubit.topicSearch,
                            isLoadMore: true);
                      }
                    },
                    child: buildSearchListCourse(context, state),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget buildSearchListCourse(BuildContext context, HomeEventState state) {
    return state is HomeEventLoading
        ? const SizedBox.shrink()
        : _cubit.listEventSearch.length == 0
            ? Center(
                child: Text(
                  R.string.no_result_search.tr(),
                  style: Theme.of(context).textTheme.bodyRegular,
                ),
              )
            : GridView.builder(
                primary: true,
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                physics: const BouncingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 16.h,
                    crossAxisSpacing: 12.h,
                    childAspectRatio: 18 / 20),
                itemCount: _cubit.listEventSearch.length,
                itemBuilder: (BuildContext ctx, index) {
                  FetchEventsData data = _cubit.listEventSearch[index];
                  return GestureDetector(
                    onTap: () {
                      NavigationUtils.rootNavigatePage(
                          context,
                          EventDetailPage(
                            eventId: data.id ?? 0,
                            productId: data.productId ?? 0,
                          ));
                    },
                    child: buildItemCourse(
                        imageUrl: data.avatarUrl,
                        title: data.name,
                        dateTime: data.startDate,
                        hot: data.isHot),
                  );
                });
  }

  Widget buildItemCourse(
      {String? imageUrl, String? title, int? dateTime, bool? hot}) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.all(4.h),
          decoration: BoxDecoration(
              color: R.color.white, borderRadius: BorderRadius.circular(8.h)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(8.h),
                  child: CachedNetworkImage(
                    height: 108.h,
                    width: double.infinity,
                    imageUrl: imageUrl ?? "",
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      height: 108.h,
                      width: double.infinity,
                    ),
                    errorWidget: (context, url, error) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      height: 108.h,
                      width: double.infinity,
                    ),
                  )),
              SizedBox(height: 4.h),
              Text(
                title ?? "",
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.medium500.copyWith(
                    fontSize: 12.sp,
                    color: R.color.neutral1,
                    height: 16.h / 12.sp),
              ),
              SizedBox(height: 6.h),
              Spacer(),
              Text(
                DateUtil.parseDateToString(
                    DateTime.fromMillisecondsSinceEpoch(dateTime ?? 0,
                        isUtc: false),
                    Const.DATE_TIME_FORMAT),
                style: Theme.of(context).textTheme.regular400.copyWith(
                    fontWeight: FontWeight.normal,
                    color: R.color.black,
                    fontSize: 12,
                    height: 20 / 12),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
        hot == true
            ? Positioned(
                top: 6,
                left: -6,
                child: Stack(
                  children: [
                    Image.asset(R.drawable.ic_hot, height: 18.h, width: 34.w),
                  ],
                ),
              )
            : SizedBox(),
      ],
    );
  }

  Widget buildPopupMenu() {
    return PopupMenuButton<PopMenu>(
        offset: Offset(0, 32.h),
        constraints:
            BoxConstraints(minWidth: MediaQuery.of(context).size.width * 0.55),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: R.color.hintGray),
          borderRadius: BorderRadius.circular(10),
        ),
        onSelected: (PopMenu item) {
          setState(() {
            _selectedMenu = item.title();
            _cubit.onOrder();
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.h),
              color: _cubit.isOrder ? R.color.primaryColor : R.color.white),
          padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 20.h),
          child: Image.asset(
            R.drawable.ic_filter_list,
            height: 20.h,
            color: _cubit.isOrder ? R.color.white : R.color.primaryColor,
          ),
        ),
        itemBuilder: (BuildContext context) => List.generate(
            4,
            (index) => PopupMenuItem<PopMenu>(
                  enabled: false,
                  value: PopMenu.values[index],
                  child: StatefulBuilder(
                    builder:
                        (BuildContext context, StateSetter setStateRadio) =>
                            ValueListenableBuilder(
                      valueListenable: _cubit.radioChoice,
                      builder: (context, List<bool> value, _) => Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              _cubit.selectFilter(index);
                            },
                            child: Row(
                              children: [
                                Radio(
                                  value: _cubit.radioChoice.value[index],
                                  groupValue: true,
                                  onChanged: (value) {
                                    setStateRadio(() {
                                      _cubit.selectFilter(index);
                                    });
                                  },
                                ),
                                Text(
                                  _cubit.title[index],
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 14.sp,
                                          color: _cubit.radioChoice.value[index]
                                              ? R.color.primaryColor
                                              : R.color.black),
                                ),
                                Spacer(),
                                index == 3
                                    ? Expanded(
                                        child: _cubit.isSelect == 3
                                            ? Icon(Icons.arrow_drop_up_rounded)
                                            : Icon(
                                                Icons.arrow_drop_down_rounded))
                                    : const SizedBox.shrink()
                              ],
                            ),
                          ),
                          _cubit.isSelect == 3 && index == 3
                              ? Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20, bottom: 10),
                                    child: Text(
                                      R.string.choose_suggested_topic.tr(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelLargeText
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 3 && index == 3
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                      width: context.width * 0.7,
                                      child: TextField(
                                        maxLines: 1,
                                        controller: _searchTopicController,
                                        style: Theme.of(context)
                                            .textTheme
                                            .regular400,
                                        decoration: InputDecoration(
                                          hintText: R.string.search.tr(),
                                          hintStyle: Theme.of(context)
                                              .textTheme
                                              .regular400
                                              .copyWith(
                                                  color: R.color.disableGray),
                                          border: InputBorder.none,
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: R.color.disableGray),
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 1,
                                                color: R.color.disableGray),
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          suffixIcon: IconButton(
                                            onPressed: () {
                                              setStateRadio(() {
                                                FocusManager
                                                    .instance.primaryFocus
                                                    ?.unfocus();
                                                _cubit.getProductTopic(
                                                    searchKey:
                                                        _searchTopicController
                                                            .text
                                                            .trim());
                                              });
                                            },
                                            icon: Icon(Icons.search),
                                          ),
                                        ),
                                      )))
                              : const SizedBox.shrink(),
                          _cubit.isSelect == 3 && index == 3
                              ? ValueListenableBuilder(
                                  valueListenable: _cubit.loadingSearch,
                                  builder: (context, value, _) =>
                                      _cubit.loadingSearch.value
                                          ? CircularProgressIndicator()
                                          : ConstrainedBox(
                                              constraints: BoxConstraints(
                                                  maxWidth: context.width * 0.7,
                                                  maxHeight: 180,
                                                  minHeight: 50),
                                              child: Scrollbar(
                                                child: SingleChildScrollView(
                                                  physics:
                                                      const BouncingScrollPhysics(),
                                                  child:
                                                      _cubit.productTopic
                                                              .isEmpty
                                                          ? Center(
                                                              child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 5),
                                                              child: Text(
                                                                R.string
                                                                    .not_exist
                                                                    .tr(),
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .regular400,
                                                              ),
                                                            ))
                                                          : Wrap(
                                                              children: _cubit
                                                                  .productTopic
                                                                  .asMap()
                                                                  .entries
                                                                  .map((e) =>
                                                                      ChoiceChip(
                                                                        avatar: _cubit.choiceChip[e.key]
                                                                            ? Icon(
                                                                                Icons.check,
                                                                                color: R.color.zaffre,
                                                                              )
                                                                            : null,
                                                                        backgroundColor: R
                                                                            .color
                                                                            .white,
                                                                        selectedColor: R
                                                                            .color
                                                                            .white,
                                                                        label:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.only(bottom: 1),
                                                                          child:
                                                                              Text(
                                                                            e.value?.topicName ??
                                                                                '',
                                                                            style:
                                                                                Theme.of(context).textTheme.subTitleRegular.copyWith(color: _cubit.choiceChip[e.key] ? R.color.zaffre : R.color.grey),
                                                                          ),
                                                                        ),
                                                                        selected:
                                                                            _cubit.choiceChip[e.key],
                                                                        shape: StadiumBorder(
                                                                            side:
                                                                                BorderSide(width: 1, color: _cubit.choiceChip[e.key] ? R.color.zaffre : R.color.grey)),
                                                                        onSelected:
                                                                            (bool
                                                                                selected) {
                                                                          setStateRadio(
                                                                              () {
                                                                            _cubit.choiceChip[e.key] =
                                                                                selected;
                                                                          });
                                                                        },
                                                                      ))
                                                                  .toList(),
                                                              spacing: 10,
                                                              alignment:
                                                                  WrapAlignment
                                                                      .start,
                                                              direction: Axis
                                                                  .horizontal,
                                                              textDirection: ui
                                                                  .TextDirection
                                                                  .ltr,
                                                            ),
                                                ),
                                              ),
                                            ),
                                )
                              : const SizedBox.shrink(),
                          //TODO: Need refactor to reuse
                          index == 3
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      ButtonWidget(
                                        backgroundColor: R.color.brightRed,
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            12.w, 8.h, 12.w, 8.h),
                                        title: R.string.clear_filter.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          _cubit.changePopMenuTitle(0);
                                          _cubit.selectFilter(0);
                                          _cubit.clearFilter();
                                          _searchTopicController.clear();
                                          _searchController.clear();
                                          _cubit.searchEvent(isRefresh: true);
                                          _cubit.getProductTopic();
                                          NavigationUtils.pop(context);
                                        },
                                      ),
                                      const SizedBox(
                                        width: 15,
                                      ),
                                      ButtonWidget(
                                        backgroundColor: R.color.primaryColor,
                                        padding: EdgeInsetsDirectional.fromSTEB(
                                            12.w, 8.h, 12.w, 8.h),
                                        title: R.string.apply_code.tr(),
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 12.sp,
                                                color: R.color.white),
                                        onPressed: () {
                                          _cubit.changePopMenuTitle(
                                              _cubit.isSelect);
                                          _cubit.isSelect == 3
                                              ? _cubit.getSearchTopic()
                                              : null;
                                          widget.selected=0;
                                          _cubit.searchEvent(
                                              keyWord:
                                                  _searchController.text.trim(),
                                              priorityOrder:
                                                  _cubit.isSelect == 1
                                                      ? Order.DESC.name
                                                      : null,
                                              isHot: _cubit.isSelect == 2
                                                  ? true
                                                  : null,
                                              topicIds: _cubit.isSelect == 3
                                                  ? _cubit.topicSearch
                                                  : [],
                                              isRefresh: true);
                                          NavigationUtils.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                )
                              : const SizedBox.shrink()
                        ],
                      ),
                    ),
                  ),
                )));
  }

//TODO: Need Refactor
  Widget _appBar() {
    return _cubit.isSearch
        ? Container(
            height: _appBarHeight,
            width: double.infinity,
            decoration: BoxDecoration(color: R.color.primaryColor, boxShadow: [
              BoxShadow(
                  offset: Offset(0, 2.h), blurRadius: 1.h, color: R.color.gray)
            ]),
            child: Row(
              children: [
                SizedBox(
                  width: 16.w,
                ),
                GestureDetector(
                  onTap: () {
                    NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: R.color.white,
                    size: 20.h,
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Text(
                      R.string.event.tr().toUpperCase(),
                      style: Theme.of(context)
                          .textTheme
                          .medium500
                          .copyWith(fontSize: 20.sp, color: R.color.white),
                    ),
                    alignment: Alignment.center,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _cubit.checkSearch();
                  },
                  child: Image.asset(R.drawable.ic_search,
                      width: 20.h, height: 20.h, color: R.color.white),
                ),
                SizedBox(width: 16.w),
              ],
            ),
          )
        : Stack(
            children: [
              Container(
                width: double.infinity,
                height: _appBarHeight,
                decoration: BoxDecoration(
                    color: R.color.primaryColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 2.h),
                          blurRadius: 1.h,
                          color: R.color.gray)
                    ]),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.h, top: 20.h),
                    child: GestureDetector(
                      onTap: () {
                        NavigationUtils.pop(context,
                            result: Const.ACTION_REFRESH);
                      },
                      child: Icon(CupertinoIcons.back, color: R.color.white),
                    ),
                  ),
                  Expanded(
                    child: SearchWidget(
                      onSubmit: (text) {
                        _cubit.searchEvent(
                            keyWord: _searchController.text.trim(),
                            priorityOrder:
                                _cubit.isSelect == 0 ? Order.DESC.name : null,
                            isHot: widget.selected == 2
                                ? true
                                : _cubit.isSelect == 2
                                    ? true
                                    : null,
                            topicIds: widget.selected == 3
                                ? widget.topicSearch
                                : (_cubit.isSelect == 3
                                    ? _cubit.topicSearch
                                    : []),
                            isRefresh: true);
                      },
                      searchController: _searchController,
                      type: Const.COURSE,
                      onChange: (String) {},
                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
