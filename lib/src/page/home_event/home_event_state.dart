
abstract class HomeEventState {}

class HomeEventInitial extends HomeEventState {}

class HomeEventLoading extends HomeEventState {
  @override
  String toString() {
    return 'HomeEventLoading{}';
  }
}

class getPopupMenu extends HomeEventState {
  @override
  String toString() {
    return 'getPopupMenu{}';
  }
}

class HomeEventFailure extends HomeEventState {
  final String error;

  HomeEventFailure(this.error);

  @override
  String toString() {
    return 'HomeEventFailure{error: $error}';
  }
}

class HomeEventSuccess extends HomeEventState {
  @override
  String toString() {
    return 'HomeEventSuccess{}';
  }
}

class SearchHomeEventSuccess extends HomeEventState {
  @override
  String toString() {
    return 'SearchHomeEventSuccess{}';
  }
}

class ListEventEmpty extends HomeEventState {
  @override
  String toString() {
    return 'ListEventEmpty{}';
  }
}
