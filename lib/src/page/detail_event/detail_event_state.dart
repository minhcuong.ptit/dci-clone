abstract class EventDetailState {}

class EventDetailInitial extends EventDetailState {}

class EventDetailTextChangeInitial extends EventDetailState {}

class EventDetailNumberChangeInitial extends EventDetailState {}

class EventDetailLoading extends EventDetailState {
  @override
  String toString() {
    return 'EventDetailLoading{}';
  }
}

class EventDetailFailure extends EventDetailState {
  final String error;

  EventDetailFailure(this.error);

  @override
  String toString() {
    return 'EventDetailFailure{error: $error}';
  }
}

class EventDetailSuccess extends EventDetailState {
  @override
  String toString() {
    return 'EventDetailSuccess{}';
  }
}

class ListEventDetailSuccess extends EventDetailState {
  @override
  String toString() {
    return 'ListEventDetailSuccess{}';
  }
}

class ListEventDetailEmpty extends EventDetailState {
  @override
  String toString() {
    return 'ListEventDetailEmpty{}';
  }
}

class EventDetailErrorSuccess extends EventDetailState {
  @override
  String toString() {
    return 'EventDetailErrorSuccess{}';
  }
}

class OrderEventDetailSuccess extends EventDetailState {
  @override
  String toString() {
    return 'OrderEventDetailSuccess{}';
  }
}

class BuyNowSuccess extends EventDetailState {
  @override
  String toString() {
    return 'BuyNowSuccess{}';
  }
}

class GetOrderEventSuccess extends EventDetailState {
  @override
  String toString() {
    return 'GetOrderEventSuccess{}';
  }
}

class GetPaymentSuccess extends EventDetailState {
  @override
  String toString() {
    return 'GetPaymentSuccess{}';
  }
}
class ValidateError extends EventDetailState {
  @override
  String toString() {
    return 'ValidateError {}';
  }
}

class GetProvinceSuccess extends EventDetailState {
  @override
  String toString() {
    return 'GetProvinceSuccess {}';
  }
}

class SetProvinceSuccess extends EventDetailState {
  @override
  String toString() {
    return 'SetProvinceSuccess {}';
  }
}

class OrderFreeSuccess extends EventDetailState {
  @override
  String toString() {
    return 'OrderFreeSuccess {}';
  }
}

class OrderEventSuccess extends EventDetailState {
  @override
  String toString() {
    return 'OrderEventSuccess {}';
  }
}

class OrderEventViewDialogSuccess extends EventDetailState {
  @override
  String toString() {
    return 'OrderEventViewSuccess {}';
  }
}
class OrderMaximum extends EventDetailState {
  @override
  String toString() {
    return 'OrderMaximum {}';
  }
}
class OrderValidatePhone extends EventDetailState {
  @override
  String toString() {
    return 'OrderValidatePhone {}';
  }
}
class OrderValidateEmail extends EventDetailState {
  @override
  String toString() {
    return 'OrderValidateEmail {}';
  }
}
class OrderValidate extends EventDetailState {
  @override
  String toString() {
    return 'OrderValidatePhone {}';
  }
}

class EmptyProvince extends EventDetailState {
  @override
  String toString() {
    return 'EmptyProvince {}';
  }
}

class ListEventSimilarEmpty extends EventDetailState {
  @override
  String toString() {
    return 'ListEventSimilarEmpty{}';
  }
}
