import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/course_details/widget/set_count_course.dart';
import 'package:imi/src/page/detail_event/detail_event.dart';
import 'package:imi/src/page/detail_event/detail_event_cubit.dart';
import 'package:imi/src/page/detail_event/widget/event_highlight.dart';
import 'package:imi/src/page/detail_event/widget/event_information.dart';
import 'package:imi/src/page/detail_event/widget/infor_event_order.dart';
import 'package:imi/src/page/detail_event/widget/order_success.dart';
import 'package:imi/src/page/home_event/home_event_page.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/dropdown_column_widget.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/response/fetch_event_data.dart';
import '../../utils/const.dart';
import '../../utils/date_util.dart';
import '../../utils/navigation_utils.dart';
import '../../utils/utils.dart';
import '../../widgets/button_widget.dart';
import '../../widgets/search_list_data_popup.dart';
import '../../widgets/stack_loading_view.dart';
import '../../widgets/text_field_widget.dart';
import '../course_details/widget/describe_page.dart';
import '../detail_cart/detail_cart.dart';

class EventDetailPage extends StatefulWidget {
  final int eventId;
  final int productId;

  EventDetailPage({required this.eventId, required this.productId});

  @override
  State<EventDetailPage> createState() => _EventDetailPageState();
}

class _EventDetailPageState extends State<EventDetailPage> {
  late EventDetailCubit _cubit;
  final RefreshController _refreshController = RefreshController();
  TextEditingController _noteController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _numberController = TextEditingController();
  FocusNode _focusNode = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  FocusNode _nameFocus = FocusNode();

  void initState() {
    // TODO: implement initState
    super.initState();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    AppRepository repository = AppRepository();
    _cubit = EventDetailCubit(
        widget.eventId, repository, graphqlRepository, widget.productId);
    _cubit.getEventDetail();
    _cubit.getListEvent();
    _cubit.getDetailCart();
    _cubit.getPayment();
    _cubit.getItemOrderCourse();
    _nameController.text = _cubit.name ?? "";
    _phoneController.text = _cubit.phone ?? "";
    _emailController.text = _cubit.email ?? "";
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _refreshController.dispose();
    _noteController.dispose();
    _phoneController.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _numberController.dispose();
    _focusNode.dispose();
    _emailFocus.dispose();
    _phoneFocus.dispose();
    _nameFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<EventDetailCubit, EventDetailState>(
        listener: (context, state) {
          // TODO: implement listener
          if (state is! EventDetailLoading) {
            _refreshController.refreshCompleted();
            _refreshController.loadComplete();
            _cubit.lengthText = _noteController.text.trim().length;
          }
          if (state is OrderMaximum) {
            Utils.showToast(
                context, R.string.you_have_registered_for_event.tr());
          }
          if (state is OrderValidate) {
            Utils.showToast(context, R.string.validate_phone_email.tr());
          }
          if (state is OrderValidatePhone) {
            Utils.showToast(context, R.string.validate_register_phone.tr());
          }
          if (state is OrderValidateEmail) {
            Utils.showToast(context, R.string.validate_register_email.tr());
          }
          if (state is OrderFreeSuccess) {
            NavigationUtils.pop(context);
            NavigationUtils.navigatePage(context,
                    OrderFreeSuccessWidget(_emailController.text.trim()))
                .then((value) {
              _nameController.text = _cubit.name ?? "";
              _phoneController.text = _cubit.phone ?? "";
              _emailController.text = _cubit.email ?? "";
            });
          }
          if (state is GetOrderEventSuccess) {
            _noteController.text = _cubit.fetchItemByProductIdV2?.note ?? "";
            _nameController.text =
                _cubit.fetchItemByProductIdV2?.userInformation?.userName ??
                    _cubit.name ??
                    "";
            _phoneController.text =
                _cubit.fetchItemByProductIdV2?.userInformation?.phone ??
                    _cubit.phone ??
                    "";
            _emailController.text =
                _cubit.fetchItemByProductIdV2?.userInformation?.email ??
                    _cubit.email ??
                    "";
          }
          if (state is OrderEventSuccess) {
            NavigationUtils.pop(context);
            _cubit.count = 1;
            NavigationUtils.navigatePage(context, DetailCartPage())
                .then((value) => _cubit.refreshCountCart());
          }
          if (state is OrderEventViewDialogSuccess) {
            NavigationUtils.pop(context);
            _cubit.count = 1;
            _cubit.refreshCountCart();
            showDialog(
                barrierColor: R.color.white.withOpacity(0),
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return AlertDialog(
                    backgroundColor: R.color.darkGrey,
                    contentPadding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    content: Container(
                      height: 80.h,
                      width: 80.w,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            CupertinoIcons.check_mark_circled_solid,
                            size: 21.h,
                            color: R.color.white,
                          ),
                          SizedBox(height: 10.h),
                          Text(
                            R.string.added_to_cart.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 11.sp, color: R.color.white),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                  );
                });
            Future.delayed(Duration(seconds: 3), () {
              NavigationUtils.pop(context);
            });
          }
        },
        builder: (context, state) {
          return Scaffold(
            backgroundColor: R.color.grey100,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              elevation: 0,
              leading: InkWell(
                onTap: () {
                  NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                },
                child: Icon(
                  CupertinoIcons.back,
                  color: R.color.black,
                ),
              ),
              backgroundColor: R.color.grey100,
              actions: [
                InkWell(
                  onTap: () {
                    NavigationUtils.rootNavigatePage(context, DetailCartPage())
                        .then((value) => _cubit.refreshCountCart());
                  },
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 16.h, right: 20.h),
                        child: Icon(
                          CupertinoIcons.shopping_cart,
                          size: 22.h,
                          color: R.color.black,
                        ),
                      ),
                      if (_cubit.cartData?.fetchMyOrder?.total != null)
                        Visibility(
                          visible: _cubit.cartData!.fetchMyOrder!.total! > 0,
                          child: Positioned(
                            left: _cubit.cartData!.fetchMyOrder!.total! > 9
                                ? 18
                                : 16,
                            top: 13,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      _cubit.cartData!.fetchMyOrder!.total! > 9
                                          ? 2
                                          : 4,
                                  vertical: 0.5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.h),
                                  color: R.color.red),
                              child: Text(
                                _cubit.cartData!.fetchMyOrder!.total! > 100
                                    ? "${99}+"
                                    : "${_cubit.cartData?.fetchMyOrder?.total}",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 9.sp,
                                        color: R.color.white,
                                        height: 14.h / 11.sp),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        )
                    ],
                  ),
                )
              ],
            ),
            body: StackLoadingView(
              visibleLoading: state is EventDetailLoading,
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () {
                  _cubit.getEventDetail(isRefresh: true);
                  _cubit.getListEvent(isRefresh: true);
                  _cubit.count = 1;
                  _nameController.text = _cubit.fetchItemByProductIdV2
                              ?.userInformation?.userName !=
                          null
                      ? _cubit.fetchItemByProductIdV2?.userInformation
                              ?.userName ??
                          ""
                      : _cubit.name ?? "";
                  _phoneController.text = _cubit
                              .fetchItemByProductIdV2?.userInformation?.phone !=
                          null
                      ? _cubit.fetchItemByProductIdV2?.userInformation?.phone ??
                          ""
                      : _cubit.phone ?? "";
                  _emailController.text = _cubit
                              .fetchItemByProductIdV2?.userInformation?.email !=
                          null
                      ? _cubit.fetchItemByProductIdV2?.userInformation?.email ??
                          ""
                      : _cubit.email ?? "";
                  _noteController.text =
                      _cubit.fetchItemByProductIdV2?.note != null
                          ? _cubit.fetchItemByProductIdV2?.note ?? ""
                          : "";
                  _cubit.selectedProvince?.cityName = (_cubit
                              .fetchItemByProductIdV2
                              ?.userInformation
                              ?.provinceId !=
                          null
                      ? _cubit.cityName
                      : _cubit.province);
                  _cubit.validateName(_nameController.text);
                  _cubit.validateEmail(_emailController.text);
                  _cubit.validatePhone(_phoneController.text);
                },
                child: ListView(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.h, vertical: 10.h),
                  children: [
                    Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(20.h),
                          child: CachedNetworkImage(
                            fit: BoxFit.fill,
                            height: 200.h,
                            width: MediaQuery.of(context).size.width,
                            imageUrl: _cubit.fetchEventsData?.avatarUrl ?? "",
                            placeholder: (_, __) {
                              return Text(
                                R.string.loading.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(fontSize: 14.sp),
                              );
                            },
                          ),
                        ),
                        Visibility(
                          visible: _cubit.fetchEventsData?.isHot == true,
                          child: Positioned(
                              top: 10.h,
                              left: -6.h,
                              child:
                                  Image.asset(R.drawable.ic_hot, height: 36.h)),
                        ),
                        _cubit.fetchEventsData?.startDate == null
                            ? SizedBox()
                            : Positioned(
                                bottom: 12.h,
                                left: 12.h,
                                right: 32.h,
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5.h, horizontal: 12.h),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30.h),
                                      color: R.color.white),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        CupertinoIcons.clock,
                                        size: 20.h,
                                        color: R.color.green,
                                      ),
                                      SizedBox(width: 4.h),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              EdgeInsetsDirectional.fromSTEB(
                                                  0, 0, 0, 3),
                                          child: Text(
                                            R.string.opening_event_on
                                                    .tr()
                                                    .toUpperCase() +
                                                " " +
                                                DateUtil.parseDateToString(
                                                    DateTime.fromMillisecondsSinceEpoch(
                                                        _cubit.fetchEventsData
                                                                ?.startDate ??
                                                            0,
                                                        isUtc: false),
                                                    "HH:mm dd/MM/yyyy"),
                                            style: Theme.of(context)
                                                .textTheme
                                                .medium500,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                      ],
                    ),
                    SizedBox(height: 10.h),
                    Text(
                      _cubit.fetchEventsData?.name ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 16.sp, height: 24.h / 16.sp),
                    ),
                    SizedBox(height: 10.h),
                    Visibility(
                      visible: state is! EventDetailLoading,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${Utils.formatMoney(_cubit.fetchEventsData?.currentFee ?? 0)} VND",
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: _cubit.fetchEventsData
                                                    ?.preferentialFee ==
                                                _cubit
                                                    .fetchEventsData?.currentFee
                                            ? 16.sp
                                            : 12.sp,
                                        fontWeight: _cubit.fetchEventsData
                                                    ?.preferentialFee ==
                                                _cubit
                                                    .fetchEventsData?.currentFee
                                            ? FontWeight.w700
                                            : FontWeight.w400,
                                        decoration: _cubit.fetchEventsData
                                                    ?.preferentialFee ==
                                                _cubit
                                                    .fetchEventsData?.currentFee
                                            ? TextDecoration.none
                                            : TextDecoration.lineThrough),
                              ),
                              Visibility(
                                visible:
                                    _cubit.fetchEventsData?.preferentialFee !=
                                        _cubit.fetchEventsData?.currentFee,
                                child: Text(
                                    "${Utils.formatMoney(_cubit.fetchEventsData?.preferentialFee ?? 0)} VND",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bold700
                                        .copyWith(
                                            fontSize: 16.sp,
                                            color: R.color.black,
                                            height: 24.h / 16.sp)),
                              ),
                            ],
                          ),
                          Expanded(
                              child: SizedBox(
                            width: 10,
                          )),
                          InkWell(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                context: context,
                                backgroundColor: Colors.transparent,
                                builder: (context) => BlocProvider.value(
                                  child: buildAddToCart(context, state),
                                  value: _cubit,
                                ),
                              );
                            },
                            child: Container(
                              alignment: Alignment.topCenter,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 30.w, vertical: 5.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(48.h),
                                color: R.color.primaryColor,
                              ),
                              child: Text(
                                  _cubit.fetchEventsData?.preferentialFee != 0
                                      ? R.string.buy_ticket.tr()
                                      : R.string.sign_up.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(
                                          fontSize: 16.sp,
                                          color: R.color.white,
                                          height: 20 / 16)),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10.h),
                    Text(
                      R.string.event_information.tr(),
                      style: Theme.of(context).textTheme.bold700.copyWith(
                            fontSize: 14.sp,
                            color: R.color.primaryColor,
                          ),
                    ),
                    SizedBox(height: 10.h),
                    EventInformationWidget(
                        image: R.drawable.ic_clock,
                        title: _cubit.fetchEventsData?.studyTime ?? ""),
                    SizedBox(height: 5.h),
                    Container(height: 1, color: R.color.lightGray),
                    SizedBox(height: 5.h),
                    EventInformationWidget(
                        image: R.drawable.ic_teacher,
                        title: _cubit.fetchEventsData?.teacher ?? ""),
                    SizedBox(height: 5.h),
                    Container(height: 1, color: R.color.lightGray),
                    SizedBox(height: 5.h),
                    EventInformationWidget(
                        image: R.drawable.ic_event_location,
                        title: _cubit.fetchEventsData?.address ?? ""),
                    SizedBox(height: 5.h),
                    Container(height: 1, color: R.color.lightGray),
                    SizedBox(height: 5.h),
                    Text(
                      R.string.describe.tr(),
                      style: Theme.of(context).textTheme.bold700.copyWith(
                          fontSize: 14.sp, color: R.color.primaryColor),
                    ),
                    HtmlWidget(
                      _cubit.fetchEventsData?.description ?? "",
                      factoryBuilder: () => MyWidgetFactory(),
                      enableCaching: true,
                      textStyle: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12.sp),
                      onTapUrl: (url) async {
                        if (await canLaunch(url)) {
                          await launch(
                            url,
                          );
                        } else {
                          throw 'Could not launch $url';
                        }
                        return launch(
                          url,
                        );
                      },
                    ),
                    SizedBox(height: 15.h),
                    if (_cubit.listEventSimilar.length != 0 &&
                        _cubit.fetchEventsData?.topicIds != null &&
                        _cubit.fetchEventsData?.topicIds?.length != 0 &&
                        state is! EventDetailLoading) ...[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            R.string.similar_event.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 16.sp, color: R.color.black),
                          ),
                          GestureDetector(
                            onTap: () {
                              NavigationUtils.navigatePage(
                                  context,
                                  HomeEventPage(
                                    selected: 3,
                                    topicSearch: _cubit.fetchEventsData?.topicIds,
                                  ));
                            },
                            child: Text(R.string.read_more.tr(),
                                style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500,
                                  color: R.color.readMore,
                                )),
                          )
                        ],
                      ),
                      SizedBox(height: 12.h),
                      ListView.separated(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(height: 12.h);
                        },
                        itemCount: _cubit.listEventSimilar.length,
                        itemBuilder: (BuildContext context, int index) {
                          FetchEventsData data = _cubit.listEventSimilar[index];
                          return EventHighlight(
                            callback: () {
                              NavigationUtils.navigatePage(
                                  context,
                                  EventDetailPage(
                                    eventId: data.id ?? 0,
                                    productId: data.productId ?? 0,
                                  ));
                            },
                            imageUrl: data.avatarUrl ?? "",
                            title: data.name ?? "",
                            currentFee: data.currentFee ?? 0,
                            preferentialFee: data.preferentialFee ?? 0,
                            isHot: data.isHot ?? false,
                          );
                        },
                      ),
                      SizedBox(height: 12.h),
                    ],
                    if (state is! EventDetailLoading) ...[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            R.string.prominent_events.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .medium500
                                .copyWith(
                                    fontSize: 16.sp, color: R.color.black),
                          ),
                          GestureDetector(
                            onTap: () {
                              NavigationUtils.navigatePage(
                                  context,
                                  HomeEventPage(
                                    selected: 2,
                                  ));
                            },
                            child: Text(R.string.read_more.tr(),
                                style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500,
                                  color: R.color.readMore,
                                )),
                          )
                        ],
                      ),
                      SizedBox(height: 12.h),
                      ListView.separated(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(height: 12.h);
                        },
                        itemCount: _cubit.listEvent.length,
                        itemBuilder: (BuildContext context, int index) {
                          FetchEventsData data = _cubit.listEvent[index];
                          return EventHighlight(
                            callback: () {
                              NavigationUtils.navigatePage(
                                  context,
                                  EventDetailPage(
                                    eventId: data.id ?? 0,
                                    productId: data.productId ?? 0,
                                  ));
                            },
                            imageUrl: data.avatarUrl ?? "",
                            title: data.name ?? "",
                            currentFee: data.currentFee ?? 0,
                            preferentialFee: data.preferentialFee ?? 0,
                            isHot: data.isHot ?? false,
                          );
                        },
                      )
                    ]
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget buildAddToCart(BuildContext context, EventDetailState state) {
    return BlocBuilder<EventDetailCubit, EventDetailState>(
      buildWhen: (previous, next) =>
          next is EventDetailTextChangeInitial ||
          next is EventDetailNumberChangeInitial ||
          next is SetProvinceSuccess ||
          next is GetProvinceSuccess ||
          next is EmptyProvince ||
          next is OrderEventSuccess || next is EventDetailLoading,
      builder: (context, state) {
        final _cubit = BlocProvider.of<EventDetailCubit>(context);
        return Container(
          margin: EdgeInsets.only(top: 100),
          child: SingleChildScrollView(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: EdgeInsets.all(20.h),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.h),
                  topRight: Radius.circular(24.h),
                ),
                color: R.color.white,
              ),
              child: Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(4.h),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              height: 95.h,
                              width: 118.w,
                              imageUrl: _cubit.fetchEventsData?.avatarUrl ?? "",
                              placeholder: (_, __) {
                                return Text(
                                  R.string.loading.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 14.sp),
                                );
                              },
                            ),
                          ),
                          SizedBox(width: 8.w),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10.h),
                                Text(
                                  _cubit.fetchEventsData?.name ?? "",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bold700
                                      .copyWith(
                                          fontSize: 14.sp, height: 24 / 14),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        "${Utils.formatMoney(_cubit.fetchEventsData?.currentFee ?? 0)} VND",
                                        style: Theme.of(context)
                                            .textTheme
                                            .regular400
                                            .copyWith(
                                                fontSize: _cubit.fetchEventsData
                                                            ?.preferentialFee ==
                                                        _cubit.fetchEventsData
                                                            ?.currentFee
                                                    ? 16.sp
                                                    : 12.sp,
                                                fontWeight: _cubit
                                                            .fetchEventsData
                                                            ?.preferentialFee ==
                                                        _cubit.fetchEventsData
                                                            ?.currentFee
                                                    ? FontWeight.w700
                                                    : FontWeight.w400,
                                                decoration: _cubit
                                                            .fetchEventsData
                                                            ?.preferentialFee ==
                                                        _cubit.fetchEventsData?.currentFee
                                                    ? TextDecoration.none
                                                    : TextDecoration.lineThrough,
                                                height: 18 / 12),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    SizedBox(width: 8.h),
                                    Visibility(
                                      visible: _cubit.fetchEventsData
                                              ?.preferentialFee !=
                                          _cubit.fetchEventsData?.currentFee,
                                      child: Text(
                                        "${Utils.formatMoney(_cubit.fetchEventsData?.preferentialFee ?? 0)} VND",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bold700
                                            .copyWith(
                                                fontSize: 16.sp,
                                                color: R.color.black,
                                                height: 24 / 16),
                                        maxLines: 1,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8.h),
                      _cubit.fetchEventsData?.startDate == null
                          ? SizedBox()
                          : Container(
                              margin: EdgeInsets.symmetric(horizontal: 30.w),
                              padding: EdgeInsets.symmetric(
                                  vertical: 4.h, horizontal: 12.h),
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: R.color.shadowColor,
                                      blurRadius: 15,
                                      offset: Offset(0.0, 5))
                                ],
                                borderRadius: BorderRadius.circular(24.h),
                                color: R.color.white,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    CupertinoIcons.clock,
                                    size: 20.h,
                                    color: R.color.green,
                                  ),
                                  SizedBox(width: 4.h),
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.fromSTEB(
                                          0, 0, 0, 3),
                                      child: Text(
                                        R.string.opening_event_on
                                                .tr()
                                                .toUpperCase() +
                                            " " +
                                            DateUtil.parseDateToString(
                                                DateTime
                                                    .fromMillisecondsSinceEpoch(
                                                        _cubit.fetchEventsData
                                                                ?.startDate ??
                                                            0,
                                                        isUtc: false),
                                                "HH:mm dd/MM/yyyy"),
                                        style: Theme.of(context)
                                            .textTheme
                                            .medium500,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                      SizedBox(height: 10.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(R.drawable.ic_event_location,
                              height: 24.h, color: R.color.dark_blue),
                          SizedBox(width: 10.w),
                          int.parse("${_cubit.fetchEventsData?.address?.length}") >
                                  40
                              ? Expanded(
                                  child: Text(
                                    _cubit.fetchEventsData?.address ?? "",
                                    textAlign: TextAlign.center,
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            height: 18.h / 12.sp,
                                            color: R.color.dark_blue),
                                  ),
                                )
                              : Text(
                                  _cubit.fetchEventsData?.address ?? "",
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 12.sp,
                                          height: 18.h / 12.sp,
                                          color: R.color.dark_blue),
                                ),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Visibility(
                          visible: _cubit.fetchEventsData?.preferentialFee != 0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                R.string.amount.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                        fontSize: 12.sp, color: R.color.gray),
                              ),
                              StatefulBuilder(
                                builder: (BuildContext context,
                                    void Function(void Function()) setState) {
                                  return SetCountCourseWidget(
                                    onChanged: (text) {
                                      setState(() {
                                        _cubit.count = 0;
                                      });
                                    },
                                    controller: _numberController,
                                    decreaseCount: () {
                                      setState(() {
                                        if (_cubit.count > 1) {
                                          _cubit.decreaseCount(_cubit.count);
                                        }
                                      });
                                    },
                                    increaseCount: () {
                                      setState(() {
                                        _cubit.increaseCount(_cubit.count);
                                      });
                                    },
                                    title: "${_cubit.count}",
                                    checkColor: _cubit.count > 1,
                                  );
                                },
                              ),
                            ],
                          )),
                      SizedBox(height: 10.h),
                      Center(
                        child: Text(
                          R.string.contact_info.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(
                                  fontSize: 14.sp,
                                  height: 21.h / 14.sp,
                                  color: R.color.grey),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.first_and_last_name.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterName(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.phone_number.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterPhone(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.email.tr()),
                      SizedBox(height: 2.h),
                      _buildEnterEmail(context, state),
                      SizedBox(height: 10.h),
                      InformationDetailOrder(R.string.area.tr()),
                      SizedBox(height: 2.h),
                      _buildDropdownProvince(context, state),
                      SizedBox(height: 10.h),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            R.string.notes_for_dci.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(
                                    fontSize: 12.sp,
                                    color: R.color.gray,
                                    height: 18 / 12),
                          ),
                          SizedBox(width: 4.w),
                          _cubit.fetchEventsData?.preferentialFee == 0
                              ? SizedBox.shrink()
                              : Tooltip(
                                  message: R.string.note_join_event.tr(),
                                  textStyle: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                        fontSize: 11.sp,
                                        color: R.color.primaryColor,
                                      ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 12.w, vertical: 6.h),
                                  margin: EdgeInsetsDirectional.fromSTEB(
                                      100.w, 0, 30.w, 0),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15.h),
                                      color: R.color.white),
                                  preferBelow: false,
                                  showDuration: Duration(hours: 1),
                                  triggerMode: TooltipTriggerMode.tap,
                                  child: Icon(CupertinoIcons.info_circle,
                                      size: 16.h, color: R.color.primaryColor)),
                        ],
                      ),
                      SizedBox(height: 10.h),
                      Container(
                          height: 200.h,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.h),
                              border:
                                  Border.all(width: 1, color: R.color.gray)),
                          child:
                              SingleChildScrollView(child: buildNote(context))),
                      SizedBox(height: 4.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: "${_cubit.lengthText}",
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, color: R.color.gray),
                              children: <TextSpan>[
                                TextSpan(
                                    text: ' / 500',
                                    style: Theme.of(context)
                                        .textTheme
                                        .regular400
                                        .copyWith(
                                            fontSize: 12.sp,
                                            color: R.color.gray)),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4.h),
                      if (_cubit.fetchEventsData?.preferentialFee == 0) ...[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 80.w),
                          child: ButtonWidget(
                              backgroundColor: R.color.primaryColor,
                              showCircleIndicator: state is EventDetailLoading,
                              padding: EdgeInsetsDirectional.fromSTEB(
                                  5.w, 4.h, 5.w, 10.h),
                              title: R.string.register.tr().toUpperCase(),
                              textStyle: Theme.of(context)
                                  .textTheme
                                  .bold700
                                  .copyWith(
                                      fontSize: 12.sp,
                                      color: R.color.white,
                                      height: 22 / 12),
                              onPressed: state is EventDetailLoading
                                  ? null
                                  : () {
                                if (_numberController.text != "0" &&
                                    _numberController.text != "00" &&
                                    _numberController.text != "000") {
                                  _cubit.createOderFree(
                                    itemId: _cubit.fetchEventsData?.productId,
                                    userName: _nameController.text.trim(),
                                    phone: _phoneController.text.trim(),
                                    email: _emailController.text.trim(),
                                    note: _noteController.text.trim(),
                                  );
                                } else {
                                  Utils.showToast(
                                      context, R.string.invalid_quantity.tr());
                                }
                              }),
                        ),
                      ],
                      if (_cubit.fetchEventsData?.preferentialFee != 0) ...[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ButtonWidget(
                                backgroundColor: R.color.orange,
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    5.w, 4.h, 5.w, 10.h),
                                title: R.string.add_and_view_cart
                                    .tr()
                                    .toUpperCase(),
                                textStyle: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 10.sp,
                                        color: R.color.white,
                                        height: 20 / 10),
                                onPressed: () {
                                  if (_numberController.text != "0" &&
                                      _numberController.text != "00" &&
                                      _numberController.text != "000") {
                                    _cubit.orderCourse(
                                      true,
                                      itemId: _cubit.fetchEventsData?.productId,
                                      userName: _nameController.text.trim(),
                                      phone: _phoneController.text.trim(),
                                      email: _emailController.text.trim(),
                                      note: _noteController.text.trim(),
                                      number: _numberController.text.tr(),
                                    );
                                  } else {
                                    Utils.showToast(context,
                                        R.string.invalid_quantity.tr());
                                  }
                                }),
                            SizedBox(width: 5.w),
                            ButtonWidget(
                                backgroundColor: R.color.primaryColor,
                                padding: EdgeInsetsDirectional.fromSTEB(
                                    5.w, 4.h, 5.w, 10.h),
                                title: R.string.add_to_cart.tr(),
                                textStyle: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 10.sp,
                                        color: R.color.white,
                                        height: 20 / 10),
                                onPressed: () {
                                  if (_numberController.text != "0" &&
                                      _numberController.text != "00" &&
                                      _numberController.text != "000") {
                                    _cubit.orderCourse(
                                      false,
                                      itemId: _cubit.fetchEventsData?.productId,
                                      userName: _nameController.text.trim(),
                                      phone: _phoneController.text.trim(),
                                      email: _emailController.text.trim(),
                                      note: _noteController.text.trim(),
                                      number: _numberController.text.tr(),
                                    );
                                  } else {
                                    Utils.showToast(context,
                                        R.string.invalid_quantity.tr());
                                  }
                                }),
                          ],
                        ),
                      ],
                    ],
                  ),
                  Positioned(
                      top: 0.h,
                      right: 0.w,
                      child: InkWell(
                        onTap: () {
                          NavigationUtils.pop(context);
                        },
                        child: Icon(
                          Icons.close,
                          size: 24.h,
                        ),
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildNote(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 6.h),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _noteController,
        focusNode: _focusNode,
        keyboardType: TextInputType.multiline,
        textInputAction: TextInputAction.newline,
        inputFormatters: [LengthLimitingTextInputFormatter(500)],
        maxLines: 500,
        minLines: 1,
        underLineColor: R.color.white,
        hintText: _cubit.fetchEventsData?.preferentialFee == 0
            ? R.string.please_enter_your_answer_here.tr()
            : R.string.note_join_event.tr(),
        onChanged: (value) {
          setState(() {
            _cubit.changeText(_noteController.text.trim().length);
          });
        },
      ),
    );
  }

  Widget _buildEnterName(BuildContext context, EventDetailState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _nameController,
        focusNode: _nameFocus,
        inputFormatters: [LengthLimitingTextInputFormatter(100)],
        isRequired: true,
        borderSize: BorderSide.none,
        hintText: R.string.enter_your_name.tr(),
        errorText: _cubit.errorName,
        onChanged: _cubit.validateName,
        border: 0,
        onSubmitted: state is EventDetailLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _nameFocus, _phoneFocus),
      ),
    );
  }

  Widget _buildEnterPhone(BuildContext context, EventDetailState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _phoneController,
        focusNode: _phoneFocus,
        keyboardType: TextInputType.number,
        inputFormatters: [LengthLimitingTextInputFormatter(10)],
        isRequired: true,
        borderSize: BorderSide.none,
        hintText: R.string.text_field_hint.tr(),
        errorText: _cubit.errorPhoneNumber,
        onChanged: _cubit.validatePhone,
        border: 0,
        onSubmitted: state is EventDetailLoading
            ? null
            : (_) => Utils.navigateNextFocusChange(
                context, _phoneFocus, _emailFocus),
      ),
    );
  }

  Widget _buildEnterEmail(BuildContext context, EventDetailState state) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: _emailController,
        focusNode: _emailFocus,
        keyboardType: TextInputType.emailAddress,
        isRequired: true,
        maxLines: 1,
        inputFormatters: [LengthLimitingTextInputFormatter(100)],
        hintText: R.string.enter_your_email.tr(),
        borderSize: BorderSide.none,
        errorText: _cubit.errorEmail,
        onChanged: _cubit.validateEmail,
        onTap: () {},
        onSubmitted: state is EventDetailLoading
            ? null
            : (_) =>
                Utils.navigateNextFocusChange(context, _emailFocus, _focusNode),
      ),
    );
  }

  Widget _buildDropdownProvince(BuildContext context, EventDetailState state) {
    List<String> listProvince =
        _cubit.listProvince.map((e) => e.cityName ?? "").toList();
    return Container(
      padding: EdgeInsets.only(left: 10.w, top: 2.h),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: DropdownColumnWidget(
        onTap: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listProvince,
                  hintText: R.string.search.tr() +
                      " " +
                      R.string.province.tr().toLowerCase(),
                  onSelect: (int index) {
                    _cubit.selectProvince(_cubit.listProvince[index]);
                  },
                );
              });
        },
        hintText: R.string.province.tr(),
        errorText: _cubit.errorProvince,
        currentValue: _cubit.selectedProvince?.cityName ??
            (_cubit.fetchItemByProductIdV2?.userInformation?.provinceId != null
                ? _cubit.cityName
                : _cubit.province),
        listData: listProvince,
        selectedValue: (String? value) {},
      ),
    );
  }

  TextStyle labelStyle() {
    return Theme.of(context)
        .textTheme
        .regular400
        .copyWith(fontSize: 14.sp, color: R.color.gray);
  }
}
