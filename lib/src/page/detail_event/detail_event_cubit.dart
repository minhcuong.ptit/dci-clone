import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/data/network/request/event_free_request.dart';
import 'package:imi/src/data/network/request/payment_request.dart';
import 'package:imi/src/data/network/response/event_response.dart';
import 'package:imi/src/data/network/response/payment_response.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/page/detail_event/detail_event_state.dart';

import '../../../res/R.dart';
import '../../data/network/repository/app_graphql_repository.dart';
import '../../data/network/repository/app_repository.dart';
import '../../data/network/response/city_response.dart';
import '../../data/network/response/deatil_cart_response.dart';
import '../../data/network/response/detail_order_product.dart';
import '../../data/network/response/fetch_event_data.dart';
import '../../data/network/response/payment_online_response.dart';
import '../../data/network/service/api_result.dart';
import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';
import '../../utils/enum.dart';
import '../../utils/utils.dart';
import '../../utils/validators.dart';

class EventDetailCubit extends Cubit<EventDetailState> with Validators {
  final int eventId;
  final int productId;
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  FetchEventsData? fetchEventsData;
  List<FetchEventsData> listEvent = [];
  List<FetchEventsData> listEventSimilar = [];
  String? nextToken;
  String? nextTokenSimilar;

  //FetchOrderCourseItemById? fetchOrderCourseItemById;
  int count = 1;
  bool isCheckNote = false;
  int lengthText = 0;

  DetailCartData? cartData;
  bool? hideButtonCart;

  List<FetchPaymentTypes> listPayment = [];
  FetchItemByProductIdV2? fetchItemByProductIdV2;

  String? errorName;
  String? errorEmail;
  String? errorPhoneNumber;

  List<CityData> listProvince = [];
  CityData? selectedProvince;
  String? errorProvince;

  String? get name => appPreferences.getString(Const.FULL_NAME);

  String? get phone => appPreferences.getString(Const.PHONE);

  String? get email => appPreferences.getString(Const.EMAIL);

  String? get province => appPreferences.getString(Const.PROVINCE);

  int? get provinceId => appPreferences.getInt(Const.PROVINCE_ID);

  String? codeInvalid;
  String? cityName;

  int? cityId;

  EventDetailCubit(
      this.eventId, this.repository, this.graphqlRepository, this.productId)
      : super(EventDetailInitial());

  void getEventDetail({bool isRefresh = false, bool isLoadMore = false}) async {
    emit((isRefresh || isLoadMore)
        ? EventDetailInitial()
        : EventDetailLoading());
    ApiResult<FetchEventsData> result =
        await graphqlRepository.getEventDetail(eventId: eventId);
    result.when(success: (FetchEventsData data) {
      fetchEventsData = data;
      emit(EventDetailSuccess());
      getListEventSimilar(isRefresh: true);
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest && error.code == ServerError.course_is_not_open) {
        emit(EventDetailErrorSuccess());
        return;
      }
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListEvent(
      {bool isRefresh = false,
      String? priorityOrder,
      String? priceOrder}) async {
    emit(!isRefresh ? EventDetailLoading() : EventDetailInitial());
    if (isRefresh) {
      nextToken = null;
      listEvent.clear();
    }
    ApiResult<FetchEvents> getLibrary = await graphqlRepository.getEvent(
        nextToken: nextToken, limit: 4, exceptEventId: eventId, isHot: true);
    getLibrary.when(success: (FetchEvents data) async {
      if (data.data != null) {
        listEvent.addAll(data.data ?? []);
        nextToken = data.nextToken;
        emit(EventDetailSuccess());
      } else {
        emit(ListEventDetailEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getListEventSimilar({bool isRefresh = false}) async {
    emit(!isRefresh ? EventDetailLoading() : EventDetailInitial());
    if (isRefresh) {
      nextTokenSimilar = null;
      listEventSimilar.clear();
    }
    ApiResult<FetchEvents> getLibrary = await graphqlRepository.getEvent(
        nextToken: nextTokenSimilar,
        limit: 4,
        exceptEventId: eventId,
        topicIds: fetchEventsData?.topicIds);
    getLibrary.when(success: (FetchEvents data) async {
      if (data.data != null) {
        listEventSimilar.addAll(data.data ?? []);
        nextTokenSimilar = data.nextToken;
        emit(EventDetailSuccess());
      } else {
        emit(ListEventSimilarEmpty());
      }
    }, failure: (NetworkExceptions error) async {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void refreshCountCart() {
    emit(EventDetailLoading());
    getDetailCart();
    emit(EventDetailInitial());
  }

  void getDetailCart() async {
    emit(EventDetailLoading());
    ApiResult<DetailCartData> result = await graphqlRepository.getDetailCart();
    result.when(success: (DetailCartData data) {
      cartData = data;
      emit(EventDetailInitial());
    }, failure: (e) {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void getPayment({bool isRefresh = false}) async {
    emit((isRefresh) ? EventDetailInitial() : EventDetailLoading());
    final res = await graphqlRepository.getPayment();
    res.when(success: (List<FetchPaymentTypes> data) {
      listPayment = data;
      for (int i = 0; i < listPayment.length; i++) {
        if (listPayment[i].status == CommonStatus.ACTIVE.name) {
          hideButtonCart = true;
        }
      }
      emit(GetPaymentSuccess());
    }, failure: (e) {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void changeText(int index) {
    this.lengthText = index;
    emit(EventDetailTextChangeInitial());
  }

  void validateEmail(String? email) {
    errorEmail = checkEmail(email?.trim());
    emit(ValidateError());
  }

  void validateName(String? name) {
    errorName = checkName(name?.trim());
    emit(ValidateError());
  }

  void validatePhone(String? phone) {
    if (Utils.isEmpty(phone)) {
      errorPhoneNumber = R.string.please_enter_phone_number.tr();
    } else {
      errorPhoneNumber = checkPhoneNumber(phone);
    }
    emit(ValidateError());
  }

  void getProvince() async {
    //emit(EventDetailLoading());
    ApiResult<List<CityData>> apiResult =
        await graphqlRepository.getListProvince();
    apiResult.when(success: (List<CityData> data) async {
      listProvince = data;
      listProvince.forEach((element) {
        if (element.cityId ==
            fetchItemByProductIdV2?.userInformation?.provinceId) {
          cityName = element.cityName;
          cityId = element.cityId;
        }
      });
      emit(GetProvinceSuccess());
    }, failure: (NetworkExceptions error) async {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void selectProvince(CityData? city) {
    if (selectedProvince != city) {
      emit(EventDetailLoading());
      this.selectedProvince = city;
      if (Utils.isEmpty(selectedProvince) &&
          Utils.isEmpty(province) &&
          fetchItemByProductIdV2?.userInformation?.provinceId == null) {
        errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      } else {
        errorProvince = null;
      }
      emit(SetProvinceSuccess());
    }
  }

  void createOderFree(
      {int? itemId,
      String? userName,
      String? phone,
      String? email,
      String? note}) async {
    errorName = checkName(userName);
    errorPhoneNumber = checkPhoneNumber(phone);
    errorEmail = checkEmail(email);
    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorPhoneNumber)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (Utils.isEmpty(selectedProvince) &&
        fetchItemByProductIdV2?.userInformation?.provinceId == null &&
        province == null) {
      errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      emit(EmptyProvince());
      return;
    } else {
      errorProvince = null;
    }
    emit(EventDetailLoading());
    final res = await repository.orderFree(EventFreeRequest(
      itemId: itemId,
      productType: "EVENT",
      quantity: 1,
      userName: userName ?? name,
      phone: phone ?? phone,
      email: email ?? email,
      note: note,
      provinceId: selectedProvince?.cityId ??
          (fetchItemByProductIdV2?.userInformation?.provinceId != null
              ? cityId
              : provinceId),
    ));
    res.when(success: (data) {
      emit(OrderFreeSuccess());
    }, failure: (NetworkExceptions error) async {
      if (error is BadRequest) {
        if (error.code == ServerError.event_reach_maximum_usage_error) {
          codeInvalid = R.string.you_have_registered_for_event.tr();
          emit(OrderMaximum());
        }
        if (error.code ==
            ServerError.event_free_duplicate_email_and_phone_error) {
          emit(OrderValidate());
        }
        if (error.code == ServerError.event_free_duplicate_phone_error) {
          emit(OrderValidatePhone());
        }
        if (error.code == ServerError.event_free_duplicate_email_error) {
          emit(OrderValidateEmail());
        }

        return;
      }
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void getItemOrderCourse() async {
    emit(EventDetailLoading());
    ApiResult<FetchItemByProductIdV2> result =
        await graphqlRepository.getFetchDetailOrder(productId: productId);
    result.when(success: (FetchItemByProductIdV2 data) async {
      fetchItemByProductIdV2 = data;
      emit(GetOrderEventSuccess());
      getProvince();
    }, failure: (NetworkExceptions error) async {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(error)));
    });
  }

  void orderCourse(bool type,
      {int? itemId,
      String? userName,
      String? phone,
      String? email,
      String? note,
      String? number}) async {
    errorName = checkName(userName);
    errorPhoneNumber = checkPhoneNumber(phone);
    errorEmail = checkEmail(email);
    if (!Utils.isEmpty(errorName)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorPhoneNumber)) {
      emit(ValidateError());
      return;
    }
    if (!Utils.isEmpty(errorEmail)) {
      emit(ValidateError());
      return;
    }
    if (Utils.isEmpty(selectedProvince) &&
        fetchItemByProductIdV2?.userInformation?.provinceId == null &&
        province == null) {
      errorProvince = R.string.text_empty.tr(args: [R.string.province.tr()]);
      emit(EmptyProvince());
      return;
    } else {
      errorProvince = null;
    }
    emit(EventDetailLoading());
    ApiResult<dynamic> result = await repository.orderCourse(EventFreeRequest(
        quantity: number != "" ? int.parse(number ?? "") : count,
        itemId: itemId,
        productType: "EVENT",
        userName: userName ?? name,
        phone: phone ?? phone,
        email: email ?? email,
        note: note,
        provinceId: selectedProvince?.cityId ??
            (fetchItemByProductIdV2?.userInformation?.provinceId != null
                ? cityId
                : provinceId)));
    result.when(success: (data) {
      if (type == true) {
        emit(OrderEventSuccess());
      } else if (type == false) {
        emit(OrderEventViewDialogSuccess());
      }
    }, failure: (e) {
      emit(EventDetailFailure(NetworkExceptions.getErrorMessage(e)));
    });
  }

  void increaseCount(int index) {
    emit(EventDetailLoading());
    this.count = index + 1;
    emit(EventDetailInitial());
  }

  void decreaseCount(int index) {
    emit(EventDetailLoading());
    this.count = index - 1;
    emit(EventDetailInitial());
  }
}
