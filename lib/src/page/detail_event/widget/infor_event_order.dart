import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';

class InformationDetailOrder extends StatelessWidget {
  final String title;


  InformationDetailOrder(this.title);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          textAlign: TextAlign.left,
          style: Theme.of(context).textTheme.regular400.copyWith(
              fontSize: 12.sp,
              height: 16.h / 12.sp,
              color: R.color.grey),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        Text(
          R.string.star.tr(),
          textAlign: TextAlign.left,
          style: Theme.of(context).textTheme.regular400.copyWith(
              fontSize: 12.sp,
              height: 16.h / 12.sp,
              color: R.color.orange),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }
}
