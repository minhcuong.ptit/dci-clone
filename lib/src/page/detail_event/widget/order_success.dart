import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

import '../../../../res/R.dart';
import '../../../utils/const.dart';

class OrderFreeSuccessWidget extends StatelessWidget {
  final String email;

  OrderFreeSuccessWidget(this.email);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, "",onPop: (){
        NavigationUtils.pop(context,result: Const.ACTION_REFRESH);
      }),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
        children: [
          Text(
            R.string.thank_you_for_subscribing.tr(),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bold700.copyWith(
                  fontSize: 16.sp,
                  color: R.color.black,
                ),
          ),
          SizedBox(height: 20.h),
          Image.asset(R.drawable.ic_frame, height: 200.h, width: 200.w),
          SizedBox(height: 20.h),
          buildText(context,R.string.thank_you_for_signing_up_for_the_dci_event.tr()),
          Text.rich(
             TextSpan(
              text: R.string.please_check_email.tr(),
              style: Theme.of(context).textTheme.regular400.copyWith(
                  fontSize: 12.sp,
                  color: R.color.primaryColor,
                  height: 20.h / 12.sp),
              children: <TextSpan>[
                TextSpan(
                    text: '${email} ',
                    style: Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: 12.sp,
                        color: R.color.primaryColor,
                        height: 20.h / 12.sp)),
                TextSpan(
                    text: R.string.to_update_event_information.tr(),
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12.sp,
                        color: R.color.primaryColor,
                        height: 20.h / 12.sp)),
              ],
            ),
            textAlign: TextAlign.center,
          ),
          buildText(context,R.string.in_case_you_did_not_receive_email.tr()),
         buildText(context, R.string.wish_you_have_a_great_event_with_dci.tr()),
          buildText(context,R.string.best_regards.tr()),
          SizedBox(height: 10.h),
          Image.asset(R.drawable.ic_dci_team, height: 18.h, width: 100.w),
        ],
      ),
    );
  }

  Widget buildText(BuildContext context,String title) {
    return Text(
          title,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.regular400.copyWith(
              fontSize: 12.sp,
              color: R.color.primaryColor,
              height: 20.h / 12.sp),
        );
  }
}
