import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../../../res/R.dart';

class EventInformationWidget extends StatelessWidget {
  final String image;
  final String title;

  EventInformationWidget({required this.image, required this.title});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(image, height: 24.h,color: R.color.black,),
        SizedBox(width: 12.w),
        Expanded(
          child: RichText(
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
              text: "",
              style: Theme.of(context).textTheme.bold700.copyWith(
                  fontSize: 12.sp,
                  height: 18.h / 12.sp,
                  color: R.color.dark_blue),
              children: <TextSpan>[
                TextSpan(
                  text: title,
                  style: Theme.of(context).textTheme.bold700.copyWith(
                      fontSize: 12.sp,
                      height: 18.h / 12.sp,
                      color: R.color.black),

                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
