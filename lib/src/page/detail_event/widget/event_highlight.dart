import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';

import '../../../../res/R.dart';

class EventHighlight extends StatelessWidget {
  final VoidCallback callback;
  final String imageUrl;
  final String title;
  final int currentFee;
  final int preferentialFee;
  final bool isHot;

  EventHighlight(
      {required this.callback,
      required this.imageUrl,
      required this.title,
      required this.currentFee,
      required this.preferentialFee,
      required this.isHot});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
          onTap: callback,
          child: Container(
            height: 73.h,
            padding: EdgeInsets.symmetric(horizontal: 4.h),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.h),
                color: R.color.white),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.h),
                  child: CachedNetworkImage(
                    width: 80.h,
                    height: 60.h,
                    fit: BoxFit.cover,
                    imageUrl: imageUrl,
                    placeholder: (context, url) => Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: R.color.grey,
                        ),
                        borderRadius: BorderRadius.circular(10.h),
                      ),
                      child: Stack(
                        children: [
                          Image.asset(
                            R.drawable.ic_default_item,
                            fit: BoxFit.cover,
                            width: double.infinity,
                            height: 108.h,
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            child: Text(
                              R.string.loading.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(fontSize: 14.sp),
                            ),
                          )
                        ],
                      ),
                    ),
                    errorWidget: (context, url, error) => Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: R.color.grey,
                          ),
                          borderRadius: BorderRadius.circular(10.h)),
                      child: Stack(
                        children: [
                          Image.asset(
                            R.drawable.ic_default_item,
                            fit: BoxFit.cover,
                            width: double.infinity,
                            height: 108.h,
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            child: Text(
                              R.string.loading.tr(),
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(fontSize: 14.sp),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 8.w),
                Expanded(
                  child: Container(
                    height: 70.h,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: Theme.of(context).textTheme.medium500,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Text(
                                "${Utils.formatMoney(currentFee)} VND",
                                style: TextStyle(
                                    decoration: currentFee == preferentialFee
                                        ? TextDecoration.none
                                        : TextDecoration.lineThrough,
                                    fontWeight: currentFee == preferentialFee
                                        ? FontWeight.w700
                                        : FontWeight.w400,
                                    fontSize: currentFee == preferentialFee
                                        ? 14.sp
                                        : 12.sp,
                                    color: currentFee == preferentialFee
                                        ? R.color.black
                                        : R.color.grey),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(width: 8.w),
                            Visibility(
                              visible: currentFee != preferentialFee,
                              child: Text(
                                "${Utils.formatMoney(preferentialFee)} VND",
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 14.sp, color: R.color.black),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Visibility(
          visible: isHot == true,
          child: Positioned(
              top: 8.h,
              left: -3.h,
              child: Image.asset(R.drawable.ic_hot, height: 20.h)),
        ),
      ],
    );
  }
}
