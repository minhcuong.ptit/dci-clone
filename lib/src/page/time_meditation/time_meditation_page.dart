import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/time_meditation/time_meditation.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/custom_appbar.dart';

class TimeMeditationPage extends StatefulWidget {
  const TimeMeditationPage({Key? key}) : super(key: key);

  @override
  State<TimeMeditationPage> createState() => _TimeMeditationPageState();
}

class _TimeMeditationPageState extends State<TimeMeditationPage> {
  late TimeMeditationCubit _cubit;
  Duration initialTimer = new Duration();
  String seconds = " 0";
  int indexTimeStart = 0;
  List<String> listSecond = ["0", "5", "10", "20", "30", "60"];

  @override
  void initState() {
    // TODO: implement initState
    AppRepository repository = AppRepository();
    AppGraphqlRepository graphqlRepository = AppGraphqlRepository();
    _cubit = TimeMeditationCubit(repository, graphqlRepository);
    indexTimeStart =
        listSecond.indexWhere((element) => element == _cubit.timeStart);
    seconds = listSecond[
        listSecond.indexWhere((element) => element == _cubit.timeStart)];
    _cubit.isMeditationUnlimited =
        appPreferences.getBool(Const.TIMER_UNLIMITED) ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _cubit,
      child: BlocConsumer<TimeMeditationCubit, TimeMeditationState>(
        listener: (BuildContext context, state) {},
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: appBar(context, R.string.time.tr().toUpperCase(),
                centerTitle: true,
                backgroundColor: R.color.primaryColor,
                iconColor: R.color.white,
                titleColor: R.color.white),
            body: Column(
              children: [
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(20.h),
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            R.string.meditation_duration.tr(),
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                  fontSize: 14.sp,
                                  color: R.color.black,
                                  height: 24.h / 14.sp,
                                ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _cubit.isMeditationUnlimited = !_cubit.isMeditationUnlimited;
                              });
                            },
                            child: Image.asset(
                              R.drawable.ic_infinity_small,
                              height: 26.h,
                              width: 28.h,
                            ),
                          ),
                        ],
                      ),
                      // Duration(microseconds: (_cubit.timePlan * 1000).toInt())
                      Visibility(
                        visible: !_cubit.isMeditationUnlimited,
                        child: CupertinoTimerPicker(
                            minuteInterval: 1,
                            secondInterval: 1,
                            initialTimerDuration: _cubit.timeMeditation != 0
                                ? Duration(
                                    microseconds:
                                        (_cubit.timeMeditation * 1000).toInt())
                                : initialTimer,
                            mode: CupertinoTimerPickerMode.hms,
                            onTimerDurationChanged: (Duration changedTimer) {
                              setState(() {
                                initialTimer = changedTimer;
                              });
                            }),
                      ),
                      Visibility(
                        visible: _cubit.isMeditationUnlimited,
                        child: Image.asset(
                          R.drawable.ic_infinity_small,
                          fit: BoxFit.contain,
                          height: 150.h,
                          width: 70.w,
                        ),
                      ),
                      Text(
                        R.string.start_up_time.tr(),
                        style: Theme.of(context).textTheme.bold700.copyWith(
                              fontSize: 14.sp,
                              color: R.color.black,
                              height: 24.h / 14.sp,
                            ),
                      ),
                      CupertinoPageScaffold(
                        child: Center(
                          child: Container(
                            height: 120.h,
                            child: CupertinoPicker(
                              selectionOverlay: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.h),
                                    color: R.color.blue.withOpacity(0.1),
                                    border: Border.all(color: R.color.blue)),
                              ),
                              itemExtent: 30.h,
                              useMagnifier: false,
                              scrollController: FixedExtentScrollController(
                                  initialItem: indexTimeStart),
                              children: List.generate(
                                  listSecond.length,
                                  (index) => Text(
                                        listSecond[index] +
                                            " " +
                                            R.string.seconds.tr(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .medium500
                                            .copyWith(
                                                fontSize: 14.sp,
                                                height: 24.h / 14.sp,
                                                color: R.color.black),
                                      )),
                              onSelectedItemChanged: (value) {
                                setState(() {
                                  seconds = listSecond[value];
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: 20.h, vertical: 50.h),
                      child: ButtonWidget(
                          backgroundColor: R.color.primaryColor,
                          padding: EdgeInsets.symmetric(vertical: 14.h),
                          textSize: 14.sp,
                          title: R.string.save.tr(),
                          onPressed:
                              // (initialTimer.inHours != 0 ||
                              //         initialTimer.inMinutes % 60 != 0 &&
                              //             initialTimer.inSeconds % 60 == 0 ||
                              //         initialTimer.inSeconds % 60 != 0)
                              //     ?
                              () {
                            if (_cubit.isMeditationUnlimited == false) {
                              appPreferences.setData(
                                  Const.HOURS, initialTimer.inHours);
                              appPreferences.setData(
                                  Const.MINUTES, initialTimer.inMinutes % 60);
                              appPreferences.setData(
                                  Const.SECONDS, initialTimer.inSeconds % 60);
                              appPreferences.setData(
                                  Const.TIMER_START, seconds);
                              appPreferences.setData(
                                  Const.TIMER,
                                  // initialTimer == Duration()
                                  //     ? _cubit.timeMeditation
                                  //     :
                                  initialTimer.inMilliseconds
                              );
                              appPreferences.setData(
                                  Const.TIMER_UNLIMITED, false);
                            }
                            if (_cubit.isMeditationUnlimited == true) {
                              appPreferences.setData(
                                  Const.TIMER_UNLIMITED, true);
                              appPreferences.setData(Const.HOURS, 24);
                              appPreferences.setData(Const.MINUTES, 59);
                              appPreferences.setData(Const.SECONDS, 59);
                              appPreferences.setData(
                                  Const.TIMER_START, seconds);
                            }
                            NavigationUtils.pop(context,
                                result: Const.ACTION_REFRESH);
                          }
                          // : initialTimer.inSeconds % 60 == 0
                          //     ? null
                          //     : null,
                          ),
                    ),
                    // Visibility(
                    //   visible: _cubit.isMeditationUnlimited,
                    //   child: Container(
                    //     margin: EdgeInsets.symmetric(
                    //         horizontal: 20.h, vertical: 50.h),
                    //     child: ButtonWidget(
                    //         backgroundColor: R.color.red,
                    //         padding: EdgeInsets.symmetric(vertical: 14.h),
                    //         textSize: 14.sp,
                    //         title: R.string.save.tr(),
                    //         onPressed: () {
                    //           NavigationUtils.pop(context,
                    //               result: Const.ACTION_REFRESH);
                    //           appPreferences.setData(
                    //               Const.TIMER_UNLIMITED, true);
                    //           appPreferences.removeData(Const.HOURS);
                    //           appPreferences.removeData(Const.MINUTES);
                    //           appPreferences.removeData(Const.SECONDS);
                    //           appPreferences.removeData(Const.TIMER);
                    //           appPreferences.setData(
                    //               Const.TIMER_START, seconds);
                    //         }),
                    //   ),
                    // ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
