import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:imi/src/data/network/repository/app_graphql_repository.dart';
import 'package:imi/src/data/network/repository/app_repository.dart';
import 'package:imi/src/page/time_meditation/time_meditation_state.dart';

import '../../data/preferences/app_preferences.dart';
import '../../utils/const.dart';

class TimeMeditationCubit extends Cubit<TimeMeditationState> {
  TimeMeditationCubit(this.repository, this.graphqlRepository)
      : super(TimeMeditationInitial());
  final AppRepository repository;
  final AppGraphqlRepository graphqlRepository;
  bool isMeditationUnlimited = true;
  int get timeMeditation => appPreferences.getInt(Const.TIMER) ?? 0;
  String get timeStart => appPreferences.getString(Const.TIMER_START) ?? '0';
  }
