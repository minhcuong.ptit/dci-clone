import 'package:equatable/equatable.dart';

abstract class TimeMeditationState extends Equatable {
  @override
  List<Object> get props => [];
}

class TimeMeditationInitial extends TimeMeditationState {}
