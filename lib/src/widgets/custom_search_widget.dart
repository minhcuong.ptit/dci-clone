import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:imi/res/R.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:imi/src/utils/custom_theme.dart';

class CustomSearchWidget extends StatelessWidget {
  final TextEditingController controller;
  final VoidCallback onSearchChange;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSubmitted;
  List<BoxShadow>? shadow;
  final Color? color;

  CustomSearchWidget(
      {required this.onChanged,
      required this.onSubmitted,
      required this.onSearchChange,
      required this.controller,
      this.shadow,
      this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45.h,
      padding: EdgeInsets.symmetric(horizontal: 20.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.h),
        color: color ?? R.color.white,
        boxShadow: shadow ??
            [
              BoxShadow(
                color: R.color.black.withOpacity(0.3),
                blurRadius: 10,
              ),
            ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
              onTap: onSearchChange,
              child: Icon(CupertinoIcons.search,
                  size: 16.h, color: R.color.black)),
          SizedBox(width: 8.w),
          Expanded(
            child: CupertinoSearchTextField(
              suffixInsets: EdgeInsetsDirectional.fromSTEB(0, 0, 5, 0),
              padding: EdgeInsetsDirectional.fromSTEB(0, 5, 5, 10),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(30.h)),
              // autoFocus: false,
              controller: controller,
              prefixIcon: SizedBox(),
              placeholder: R.string.search.tr(),
              style: Theme.of(context).textTheme.bodyRegular.copyWith(
                    color: R.color.black,
                  ),
              onChanged: onChanged,
              onSubmitted: onSubmitted,
            ),
          ),
        ],
      ),
    );
  }
}
