import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/page/video_player/video_play_widget.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/full_screen_image_widget.dart';
import 'package:imi/src/widgets/page_view_indicator/page_view_indicator.dart';

import '../../../../res/R.dart';
import '../utils/const.dart';

class PostSliderWidget extends StatefulWidget {
  final List<String?> images;
  final bool? autoPlay;
  final bool isInPostDetails;
  final VoidCallback? onPressImage;

  const PostSliderWidget(
      {Key? key,
      required this.images,
      this.autoPlay,
      this.isInPostDetails = false,
      this.onPressImage})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SliderWidgetState();
  }
}

class _SliderWidgetState extends State<PostSliderWidget> {
  var _pageIndexNotifier = ValueNotifier<int>(0);
  List<String?> images = [];
  PageController pageController = PageController(viewportFraction: 1);
  Function eq = const ListEquality().equals;

  @override
  void initState() {
    super.initState();
    images = widget.images;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant PostSliderWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!eq(oldWidget.images, widget.images)) {
      images = widget.images;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Stack(
        alignment: FractionalOffset.bottomCenter,
        children: [
          SizedBox(
            height: Const.POST_ITEM_HEIGHT,
            child: PageView.builder(
                scrollDirection: Axis.horizontal,
                controller: pageController,
                itemCount: images.length,
                onPageChanged: (index) {
                  _pageIndexNotifier.value = index;
                },
                itemBuilder: (context, index) {
                  Widget item = ClipRRect(
                      borderRadius: BorderRadius.circular(8.h),
                      child: GestureDetector(
                        onTap: () {
                          if(widget.isInPostDetails) NavigationUtils.rootNavigatePage(
                              context,
                              FullScreenImageWidget(
                                  images: images, index: index));
                          else widget.onPressImage?.call();
                        },
                        child: CachedNetworkImage(
                          height: Const.POST_ITEM_HEIGHT,
                          imageUrl: images[index]!,
                          fit: BoxFit.cover,
                        ),
                      ));
                  var url = images[index]!;
                  return (url.contains('.jpg?') || url.contains('.png?'))
                      ? item
                      : VideoPlayWidget(
                          url: url,
                    showControls: true,
                        );
                }),
          ),
          images.length > 1 ? _pageViewIndicator() : Container(),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _pageIndexNotifier.dispose();
    super.dispose();
  }

  PageViewIndicator _pageViewIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: _pageIndexNotifier,
      length: images.length,
      normalBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController!,
          curve: Curves.ease,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3.w),
          child: Container(
            width: 4.w,
            height: 4.w,
            color: R.color.indicator,
          ),
        ),
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController!,
          curve: Curves.ease,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3.w),
          child: Container(
            width: 12.w,
            height: 4.w,
            color: R.color.blueFF,
          ),
        ),
      ),
    );
  }
}
