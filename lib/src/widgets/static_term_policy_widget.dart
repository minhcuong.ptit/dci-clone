import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';

class StaticTermPolicyWidget extends StatelessWidget {
  StaticTermPolicyWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 1.h, vertical: 20.h),
      alignment: Alignment.center,
      child: RichText(
        textAlign: TextAlign.start,
        text: TextSpan(children: <TextSpan>[
          TextSpan(
              text: R.string.agree_to_term.tr(),
              style: TextStyle(color: R.color.textTitleGray)),
          TextSpan(
              text: " " + R.string.registration_privacy.tr() + " ",
              style: TextStyle(color: R.color.primaryColor),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Utils.launchURL(Utils.getTypeUrlLauncher(
                      Const.PRIVACY_URL, Const.LAUNCH_TYPE_WEB));
                }),
          TextSpan(
              text: R.string.registration_and.tr() + " ",
              style: TextStyle(color: R.color.textTitleGray)),
          TextSpan(
              text: R.string.registration_term.tr(),
              style: TextStyle(color: R.color.primaryColor),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Utils.launchURL(Utils.getTypeUrlLauncher(
                      Const.TERM_URL, Const.LAUNCH_TYPE_WEB));
                }),
        ]),
      ),
    );
  }
}
