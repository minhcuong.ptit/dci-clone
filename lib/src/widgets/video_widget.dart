import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

import '../../res/R.dart';
import '../page/video_player/video_play_page.dart';
import '../utils/const.dart';
import '../utils/navigation_utils.dart';

class VideoWidget extends StatefulWidget {
  final String url;
  final bool? isFromPage;
  const VideoWidget({Key? key, required this.url, this.isFromPage})
      : super(key: key);
  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}


class _VideoWidgetState extends State<VideoWidget> {
  late VideoPlayerController _controller;
  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.url)
      ..initialize().then((_) {
        setState(() {});
      });
    _controller.addListener(() {
      setState(() {});
    });
  }

  Widget _buildBody() {
    var colors = VideoProgressColors(
      playedColor: R.color.white,
      bufferedColor: Color.fromRGBO(50, 50, 200, 0.2),
      backgroundColor: Color.fromRGBO(200, 200, 200, 0.5),
    );
    return (widget.isFromPage ?? false)
        ? Container(
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                _buildVideo(),
                _ControlsOverlay(controller: _controller),
                VideoProgressIndicator(
                  _controller,
                  allowScrubbing: false,
                  colors: colors,
                ),
              ],
            ),
          )
        : Container(
            height: Const.POST_ITEM_HEIGHT,
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                SingleChildScrollView(
                  physics:const NeverScrollableScrollPhysics(),
                  child: _buildVideo(),
                ),
                _ControlsOverlay(controller: _controller),
                VideoProgressIndicator(
                  _controller,
                  allowScrubbing: true,
                  colors: colors,
                ),
              ],
            ),
          );
  }

  Widget _buildVideo() {
    return AspectRatio(
      aspectRatio: _controller.value.aspectRatio,
      child: VideoPlayer(_controller),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: _controller.value.isInitialized ? _buildBody() : Container(),
        ),
        Positioned(
          child: InkWell(
            onTap: () {
              if (!(widget.isFromPage ?? false)) {
                NavigationUtils.navigatePage(
                    context, VideoPlayPage(url: widget.url));
              } else {
                NavigationUtils.pop(context);
              }
            },
            child: Padding(
              padding: EdgeInsets.all(10.h),
              child: Image.asset(
                R.drawable.ic_fullscreen,
                width: 12.w,
              ),
            ),
          ),
          right: 0.w,
          bottom: 0.h,
        )
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({Key? key, required this.controller})
      : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? const SizedBox.shrink()
              : Center(
                  child: Image.asset(
                    R.drawable.ic_play,
                    width: 50.w,
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
