import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/string_ext.dart';
import 'package:url_launcher/url_launcher.dart';

class LinkPreviewWidget extends StatelessWidget {
  final String? title;
  final String? description;
  final String? image;
  final String? url;

  const LinkPreviewWidget(
      {Key? key, required this.title, this.description, this.image, this.url})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final linkUri = Uri.parse(url ?? "");
    return Container(
      decoration: BoxDecoration(
          color: R.color.lightestGray,
          borderRadius: BorderRadius.circular(8.w),
          border: Border.all(
            width: 1.h,
            color: R.color.lighterGray,
          )),
      child: GestureDetector(
        onTap: () async {
          if (url != null && await canLaunch(url!)) {
            try {
              await launch(url!);
            } catch (e) {}
          }
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
              imageUrl: image ?? "",
              fit: BoxFit.fill,
              width: 104.w,
              height: 90.h,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.w),
                    bottomLeft: Radius.circular(8.w),
                  ),
                ),
              ),
              placeholder: (context, url) => _placeholderImage(),
              errorWidget: (context, url, _) => _placeholderImage(),
            ),
            SizedBox(width: 16.w),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 8.h),
                  if (url == null)
                    Container(
                        margin: EdgeInsets.only(top: 4.h),
                        width: 96.w,
                        height: 12.h,
                        decoration: BoxDecoration(
                          color: R.color.lighterGray,
                          borderRadius: BorderRadius.circular(8.w),
                        ))
                  else
                    Text(
                      linkUri.host,
                      style: Theme.of(context)
                          .textTheme
                          .labelSmallText
                          .copyWith(color: R.color.lightShadesGray),
                    ),
                  SizedBox(height: 4.h),
                  if (url == null) ...[
                    SizedBox(height: 4.h),
                    Container(
                        width: 180.w,
                        height: 12.h,
                        decoration: BoxDecoration(
                          color: R.color.lighterGray,
                          borderRadius: BorderRadius.circular(8.w),
                        )),
                    SizedBox(height: 4.h),
                    Container(
                        width: 120.w,
                        height: 12.h,
                        decoration: BoxDecoration(
                          color: R.color.lighterGray,
                          borderRadius: BorderRadius.circular(8.w),
                        ))
                  ] else
                    Text(
                      (title ?? "").toCapitalized(),
                      maxLines: 2,
                      style: Theme.of(context).textTheme.labelSmallText,
                    ),
                ],
              ),
            ),
            SizedBox(width: 16.w),
          ],
        ),
      ),
    );
  }

  Widget _placeholderImage() {
    return ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8.w),
          bottomLeft: Radius.circular(8.w),
        ),
        child: Container(
            color: Color(0x33000000),
            child: Padding(
                padding: EdgeInsets.all(36.h),
                child: Image.asset(
                  R.drawable.ic_gallery,
                  color: R.color.white,
                  height: 16.h,
                ))));
  }
}
