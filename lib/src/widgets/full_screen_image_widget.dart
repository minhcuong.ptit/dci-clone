import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/video_player/video_play_widget.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class FullScreenImageWidget extends StatefulWidget {
  final List<String?> images;
  final int index;

  FullScreenImageWidget({required this.images, required this.index});

  @override
  State<FullScreenImageWidget> createState() => _FullScreenImageWidgetState();
}

class _FullScreenImageWidgetState extends State<FullScreenImageWidget> {
  late final PageController _pageController;
  List<String?> image = [];

  @override
  void initState() {
    super.initState();
    image = widget.images;
    _pageController = PageController();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (_pageController.hasClients) {
        _pageController.animateToPage(widget.index,
            duration: Duration(milliseconds: 100), curve: Curves.easeIn);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: R.color.black,
        body: Stack(
          children: [
            Center(
                child: InteractiveViewer(
              clipBehavior: Clip.antiAlias,
              panEnabled: true,
              minScale: 1,
              maxScale: 30,
              onInteractionStart: (details) {
                if (details.pointerCount < 2) return;
              },
              onInteractionUpdate: (details) {
                details.focalPoint;
              },
              child: PageView.builder(
                controller: _pageController,
                itemCount: image.length,
                itemBuilder: (BuildContext context, int index) {
                  Widget item = CachedNetworkImage(
                    width: MediaQuery.of(context).size.width,
                    imageUrl: image[index] ?? "",
                    fit: BoxFit.contain,
                  );
                  var url = image[index]!;
                  return (url.contains('.jpg?') || url.contains('.png?'))
                      ? item
                      : SizedBox(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.width / 16 * 9,
                          child: VideoPlayWidget(url: widget.images[index],showControls: true,),
                        );
                },
              ),
            )),
            Positioned(
              right: 10.h,
              child: GestureDetector(
                onTap: () {
                  NavigationUtils.pop(context);
                },
                child: Icon(
                  CupertinoIcons.clear_circled,
                  color: R.color.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
