import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class DiscardPopup extends StatelessWidget {
  final String title;
  final String description;
  final VoidCallback onConfirm;
  const DiscardPopup(
      {Key? key,
      required this.title,
      required this.description,
      required this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.white,
      child: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Container(
        padding: EdgeInsets.all(24.h),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 16.h),
                Text(
                  title,
                  style: Theme.of(context).textTheme.headline3,
                ),
                SizedBox(height: 8.h),
                Text(
                  description,
                  style: Theme.of(context)
                      .textTheme
                      .bodySmallText
                      .copyWith(color: R.color.grey),
                ),
                SizedBox(height: 8.h),
                Container(height: 1.h, color: R.color.lightestGray),
                SizedBox(height: 16.h),
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          NavigationUtils.pop(context);
                        },
                        child: Container(
                          height: 32.h,
                          margin: EdgeInsets.only(top: 8.h),
                          padding: EdgeInsets.symmetric(horizontal: 12.w),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(48.h),
                            border:
                                Border.all(width: 1.h, color: R.color.black),
                          ),
                          child: Text(
                            R.string.no.tr().toUpperCase(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.buttonSmall,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 24.w),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          NavigationUtils.pop(context);
                          onConfirm();
                        },
                        child: Container(
                          height: 32.h,
                          margin: EdgeInsets.only(top: 8.h),
                          padding: EdgeInsets.symmetric(horizontal: 12.w),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: R.color.orange,
                            borderRadius: BorderRadius.circular(48.h),
                          ),
                          child: Text(
                            R.string.yes.tr().toUpperCase(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .buttonSmall
                                .copyWith(color: R.color.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 28.h),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
