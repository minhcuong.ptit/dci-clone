import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/post/post_checkin/post_checkin.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../page/post/post.dart';
import '../utils/enum.dart';
import 'button_widget.dart';

class RemoveConfirmPopup extends StatelessWidget {
  final VoidCallback onOk;
  final String? title;
  final String? content;
  const RemoveConfirmPopup(
      {Key? key, required this.onOk, this.title, this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.white,
      child: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 16.h),
                Text(
                  title ?? R.string.delete_post.tr(),
                  style: Theme.of(context).textTheme.labelLargeText.copyWith(
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w700,
                      color: R.color.black),
                ),
                SizedBox(height: 8.h),
                Text(
                  content ?? R.string.delete_post_confirm.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bodySmallText
                      .copyWith(color: R.color.grey),
                ),
                SizedBox(height: 8.h),
                Container(height: 1.h, color: R.color.lightestGray),
                SizedBox(height: 24.h),
                Row(
                  children: [
                    Expanded(child: buildButtonCancel(context)),
                    SizedBox(
                      width: 16.w,
                    ),
                    Expanded(child: buildButtonOk(context)),
                  ],
                ),
                SizedBox(height: 16.h),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildButtonCancel(BuildContext context) {
    return ButtonWidget(
        borderColor: R.color.black,
        backgroundColor: R.color.white,
        textStyle: Theme.of(context).textTheme.labelSmallText.copyWith(
            fontSize: 12.sp,
            fontWeight: FontWeight.w700,
            color: R.color.black,
            height: 1),
        textSize: 16.sp,
        height: 36.h,
        uppercaseTitle: false,
        padding: EdgeInsets.only(top: 5.h),
        title: R.string.no.tr(),
        onPressed: () {
          NavigationUtils.pop(context);
        });
  }

  Widget buildButtonOk(BuildContext context) {
    return ButtonWidget(
        backgroundColor: R.color.blueFF,
        textStyle: Theme.of(context).textTheme.labelSmallText.copyWith(
            fontSize: 12.sp,
            fontWeight: FontWeight.w700,
            color: R.color.white,
            height: 1),
        textSize: 16.sp,
        height: 36.h,
        uppercaseTitle: false,
        padding: EdgeInsets.only(top: 5.h),
        title: R.string.remove.tr(),
        onPressed: () {
          NavigationUtils.pop(context);
          onOk();
        });
  }
}
