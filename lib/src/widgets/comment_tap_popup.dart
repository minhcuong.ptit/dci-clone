import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class CommentTapPopup extends StatelessWidget {
  final VoidCallback onEdit;
  final VoidCallback onDelete;
  final bool? showEdit;
  final bool isReply;

  const CommentTapPopup(
      {Key? key,
      required this.onEdit,
      required this.onDelete,
      this.showEdit,
      required this.isReply})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.white,
      child: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildRow(context, false),
                Visibility(
                    visible: showEdit ?? true,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(height: 1.h, color: R.color.lightestGray),
                        _buildRow(context, true),
                      ],
                    )),
                SizedBox(height: 28.h),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRow(BuildContext context, bool isEdit) {
    var strEdit =
        isReply ? R.string.edit_reply.tr() : R.string.edit_comment.tr();
    var strDelete =
        isReply ? R.string.delete_reply.tr() : R.string.delete_comment.tr();
    return InkWell(
      onTap: () {
        NavigationUtils.pop(context);
        isEdit ? onEdit() : onDelete();
      },
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 24.h),
            child: Image.asset(
              isEdit ? R.drawable.ic_edit_post : R.drawable.ic_delete_post,
              fit: BoxFit.fill,
              width: 24.w,
            ),
          ),
          SizedBox(width: 16.w),
          Expanded(
            child: Text(isEdit ? strEdit : strDelete,
                style: Theme.of(context).textTheme.bodyBold),
          ),
        ],
      ),
    );
  }
}
