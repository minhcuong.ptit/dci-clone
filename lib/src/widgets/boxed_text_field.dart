import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';

class BoxedTextField extends StatefulWidget {
  final int? maxLength;
  final String? hintText;
  final String? errorText;
  final int? textLength;
  final double? width;
  final BuildContext? context;
  final TextEditingController? controller;
  final Function(String) onChanged;
  final Function(String) onSubmitted;
  final bool? enable;

  BoxedTextField(
      {this.maxLength,
      this.hintText,
      this.context,
      this.controller,
      required this.onChanged,
      required this.onSubmitted,
      this.enable,
      this.width,
      this.errorText, this.textLength});

  @override
  State<BoxedTextField> createState() => _BoxedTextFieldState();
}

class _BoxedTextFieldState extends State<BoxedTextField> {
  bool _isExpanding = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8.w),
          width: widget.width ?? 100.w,
          height: _isExpanding ? 300.h : 150.h,
          decoration: BoxDecoration(
              border: Border.all(color: R.color.grey, width: 1),
              borderRadius: BorderRadius.circular(5.h)),
          child: Column(
            children: [
              SizedBox(
                height: _isExpanding ? 280.h : 125.h,
                child: TextField(
                  // enabled: widget.enable,
                  readOnly: !(widget.enable ?? true),
                  controller: widget.controller,
                  //maxLength: widget.maxLength ?? 2000,
                  maxLines: 1000,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(widget.maxLength ?? 2000)
                  ],
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 2.h),
                    errorText: widget.errorText,
                    border: InputBorder.none,
                    hintStyle: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12.sp,
                        height: 20.h / 12.sp,
                        color: R.color.textGray),
                    hintText: widget.hintText ?? "",
                  ),
                  onChanged: widget.onChanged,
                  onSubmitted: widget.onSubmitted,
                  // buildCounter: _buildCounter,
                  style: Theme.of(context).textTheme.regular400.copyWith(
                      fontSize: 12.sp,
                      height: 20.h / 12.sp,
                      color: R.color.textGray),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _isExpanding = !_isExpanding;
                      });
                    },
                    child: !_isExpanding
                        ? Image.asset(
                            R.drawable.ic_expansion,
                            height: 14.h,
                            width: 18.w,
                          )
                        : Image.asset(
                            R.drawable.ic_collapse,
                            height: 14.h,
                            width: 18.w,
                          ),
                  ),
                  SizedBox(width: 10.h),
                  GestureDetector(
                    onTap: () {
                      Clipboard.setData(
                          ClipboardData(text: widget.controller?.text));
                      Utils.showSnackBar(
                          context, R.string.copied_to_clipboard.tr());
                    },
                    child: Image.asset(
                      R.drawable.ic_copy_v2,
                      height: 18.h,
                      width: 18.h,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          alignment: Alignment.centerRight,
          child: Text(
            "${widget.textLength==0||widget.textLength==null?widget.controller?.text.length:widget.textLength}" +
                "/" +
                "${widget.maxLength ?? 2000}",
            textAlign: TextAlign.end,
          ),
        )
      ],
    );
  }

  Widget? _buildCounter(BuildContext context,
      {required int currentLength,
      required bool isFocused,
      required int? maxLength}) {
    return null;
  }
}
