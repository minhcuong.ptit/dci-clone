import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/button_widget.dart';

class PopupWidget extends StatelessWidget {
  final String? title;
  final String? reasons;
  final VoidCallback? onReport;
  const PopupWidget({Key? key, this.title, this.reasons, this.onReport})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? "",
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
          ),
          Text(
            reasons ?? "",
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 11.sp),
          ),
          SizedBox(height: 20.h),
          Row(
            children: [
              Expanded(
                  child: ButtonWidget(
                      padding: EdgeInsets.symmetric(vertical: 10.h),
                      textSize: 14.sp,
                      uppercaseTitle: false,
                      title: R.string.no.tr(),
                      backgroundColor: R.color.white,
                      textColor: R.color.blue,
                      borderColor: R.color.primaryColor,
                      onPressed: () {
                        NavigationUtils.pop(context);
                      })),
              SizedBox(width: 10.h),
              Expanded(
                child: ButtonWidget(
                    padding: EdgeInsets.symmetric(vertical: 10.h),
                    textSize: 14.sp,
                    uppercaseTitle: false,
                    backgroundColor: R.color.primaryColor,
                    title: R.string.report.tr(),
                    onPressed: onReport),
              )
            ],
          )
        ],
      ),
    );
  }
}
