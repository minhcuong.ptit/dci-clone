import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';

class ButtonWidget extends StatelessWidget {
  final double? height;
  final double? width;
  final Color? backgroundColor;
  final Color? textColor;
  final double? textSize;
  final Color? borderColor;
  final String title;
  final VoidCallback? onPressed;
  final double? radius;
  final bool modeFlatButton;
  final bool uppercaseTitle;
  final bool showCircleIndicator;
  final EdgeInsetsGeometry? padding;
  final TextStyle? textStyle;

  ButtonWidget(
      {this.backgroundColor,
      this.textColor,
      this.borderColor,
      this.height,
      this.width,
      this.padding,
      this.textSize,
      required this.title,
      required this.onPressed,
      this.radius,
      this.modeFlatButton: true,
      this.uppercaseTitle: true,
      this.showCircleIndicator: false,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: height,
        width: width,
//        color: backgroundColor,
        alignment: Alignment.center,
        padding: padding ?? EdgeInsets.symmetric(vertical: 0.h),
        decoration: BoxDecoration(
            boxShadow: modeFlatButton
                ? null
                : <BoxShadow>[
                    BoxShadow(
                        color: R.color.shadowColor,
                        blurRadius: 3.0,
                        offset: Offset(0.0, 5))
                  ],
            color: onPressed != null
                ? (backgroundColor ?? R.color.btnVisible)
                : R.color.btnDisable,
//            gradient: LinearGradient(
//              colors: [Theme.of(context).primaryColor, Colors.lightBlue],
//            ),
            border: Border.all(
                color: onPressed != null
                    ? (borderColor ?? backgroundColor ?? Colors.transparent)
                    : R.color.btnDisable,
                width: 1),
            borderRadius: BorderRadius.circular(radius ?? 48.h)),
        child: showCircleIndicator
            ? Center(
                child: SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(),
                ),
              )
            : Text(
                uppercaseTitle ? title.toUpperCase().trim() : title.trim(),
                maxLines: 1,
                textAlign: TextAlign.center,
                style: textStyle ??
                    Theme.of(context).textTheme.bold700.copyWith(
                        fontSize: textSize,
                        color: textColor ?? R.color.white,
                        height: 1),
              ),
      ),
    );
  }
}
