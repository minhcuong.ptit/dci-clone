import 'package:flutter/material.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/fetch_event_data.dart';

import '../page/detail_event/widget/event_highlight.dart';

class SimilarProduct extends StatelessWidget {
  final List<FetchEventsData> eventSimilar;
  final List<FetchCoursesData> courseSimilar;
  const SimilarProduct({Key? key, required this.eventSimilar, required this.courseSimilar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool _isCourse=courseSimilar.isNotEmpty;
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: List.generate(
            _isCourse? courseSimilar.length:eventSimilar.length,
                (index) {
              return Container(
                margin: EdgeInsets.symmetric(vertical: 6),
                child: EventHighlight(
                  callback: () {
                    // NavigationUtils.pop(context);
                    // NavigationUtils.navigatePage(
                    //     context,
                    //     CourseDetailsPage(
                    //       courseId: _cubit.listCourseSimilar[index].id,
                    //       productId:
                    //       _cubit.listCourseSimilar[index].productId ?? 0,
                    //     ));
                  },
                  imageUrl: _isCourse?courseSimilar[index].avatarUrl ?? "":eventSimilar[index].avatarUrl??"",
                  title: _isCourse?courseSimilar[index].name ?? "":eventSimilar[index].name??"",
                  currentFee: _isCourse?courseSimilar[index].currentFee ?? 0:eventSimilar[index].currentFee??0,
                  preferentialFee:
                  _isCourse?courseSimilar[index].preferentialFee ?? 0:eventSimilar[index].preferentialFee??0,
                  isHot: _isCourse?courseSimilar[index].isHot ?? false:eventSimilar[index].isHot??false,
                ),
              );
            },
          ),
        ),
      ),
    );;
  }
}

