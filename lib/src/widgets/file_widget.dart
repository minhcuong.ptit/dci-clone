import 'package:dotted_border/dotted_border.dart';
import 'package:file_icon/file_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';

class FileWidget extends StatelessWidget {
  final String heroTag;
  final String name;
  final bool isShowName;
  final VoidCallback? onShowDetail;
  final VoidCallback? onClear;

  const FileWidget(
      {Key? key, required this.name, this.isShowName = false, this.onClear, this.onShowDetail, required this.heroTag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: heroTag,
      child: GestureDetector(
        onTap: onShowDetail,
        child: Stack(
          children: [
            DottedBorder(
              padding: EdgeInsets.all(5.h),
              color: R.color.grey300,
              radius: Radius.circular(5),
              borderType: BorderType.RRect,
              dashPattern: [3, 3],
              child: Column(
                children: [
                  Container(
                    height: ScreenUtil().screenWidth / 2 - 40.h,
                    width: ScreenUtil().screenWidth / 2 - 40.h,
                    padding: EdgeInsets.only(bottom: 5.h),
                    child: FileIcon(
                      // File name
                      name,
                      // Icon size
                      size: ScreenUtil().screenWidth / 2 - 80.h,
                    ),
                  ),
                ],
              ),
            ),
            Positioned(bottom: 0,
                left: 0,
                right: 5.h,
                child: Visibility(
                visible: isShowName,
                child: Container(
                  color: R.color.borderColor,
                  padding: EdgeInsets.symmetric(horizontal: 5.h, vertical: 10.h),
                  child: Text(
                    Utils.getFileName(name),
                    textAlign: TextAlign.start,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style:
                    TextStyle(fontSize: 14.sp, color: R.color.black),
                  ),
                ))),
            Positioned(
              top: 5.h,
              right: 5.h,
              child: Visibility(
                visible: onClear != null,
                child: GestureDetector(
                  onTap: onClear,
                  child: Icon(
                    Icons.clear,
                    size: 25.h,
                    color: R.color.primaryColor,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
