import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/utils.dart';

class TermPolicyWidget extends StatefulWidget {
  final VoidCallback checkCallback;

  const TermPolicyWidget({
    required this.checkCallback,
  });

  @override
  State<StatefulWidget> createState() {
    return _TermPolicyState();
  }
}

class _TermPolicyState extends State<TermPolicyWidget> {
  bool isCheck = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => {
              widget.checkCallback(),
              isCheck = !isCheck,
            },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              isCheck
                  ? CupertinoIcons.checkmark_square_fill
                  : CupertinoIcons.square,
              size: 25.h,
              color: isCheck ? R.color.orange : R.color.black.withOpacity(0.8),
            ),
            SizedBox(width: 12.w),
            Expanded(
              child: RichText(
                textAlign: TextAlign.start,
                text: TextSpan(children: <TextSpan>[
                  TextSpan(
                      text: "${R.string.registration_agree.tr()} ",
                      style: TextStyle(color: R.color.textTitleGray)),
                  TextSpan(
                      text: R.string.registration_term.tr(),
                      style: TextStyle(color: R.color.primaryColor),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Utils.launchURL(Utils.getTypeUrlLauncher(
                              Const.TERM_URL, Const.LAUNCH_TYPE_WEB));
                        }),
                  TextSpan(
                      text: " ${R.string.registration_and.tr()} ",
                      style: TextStyle(color: R.color.textTitleGray)),
                  TextSpan(
                      text: R.string.registration_privacy.tr(),
                      style: TextStyle(color: R.color.primaryColor),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Utils.launchURL(Utils.getTypeUrlLauncher(
                              Const.PRIVACY_URL, Const.LAUNCH_TYPE_WEB));
                        }),
                  TextSpan(
                      text: " ${R.string.from_dci.tr()}",
                      style: TextStyle(color: R.color.textTitleGray)),
                ]),
              ),
            )
          ],
        ));
  }
}
