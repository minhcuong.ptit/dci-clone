import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';

class RowButtonWidget extends StatelessWidget {
  final VoidCallback callback;
  final String image;
  final String title;

  RowButtonWidget(
      {required this.callback, required this.image, required this.title});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: callback,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            image,
            height: 24.h,
            color: R.color.black,
          ),
          SizedBox(width: 16.w),
          Text(title,
              style: Theme.of(context).textTheme.bodyBold.copyWith(
                    fontWeight: FontWeight.w700,
                  ))
        ],
      ),
    );
  }
}
