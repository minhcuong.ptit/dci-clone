import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';

class MembershipCardWidget extends StatelessWidget {
  final int? type;

  const MembershipCardWidget({Key? key, required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 170.h,
        margin: EdgeInsets.symmetric(vertical: 8.h),
        padding: EdgeInsets.all(12.h),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.h),
          boxShadow: [
            BoxShadow(
              color: R.color.shadowColor,
              spreadRadius: 0,
              blurRadius: 3.h,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          gradient: LinearGradient(
              colors: type == 0
                  ? [Color(0xFFFFF2DD), Color(0xFFD3954C)]
                  : (type == 1
                      ? [Color(0xFFF0F4FF), Color(0xFF5D7895)]
                      : [Color(0xFFFFF0C8), Color(0xFFFFC671)]),
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              tileMode: TileMode.clamp),
        ),
        // color: R.color.black.withOpacity(0.7),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "Mobility",
                  style: Theme.of(context).textTheme.title2.copyWith(
                      color: type == 0
                          ? Color(0xFF643B00)
                          : type == 1
                              ? Color(0xFF2B3748)
                              : Color(0xFF643B00)),
                ),
                Expanded(
                    child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "View Card  >",
                    style: Theme.of(context).textTheme.labelMarco.copyWith(
                        color: type == 0
                            ? Color(0xFF643B00)
                            : type == 1
                                ? Color(0xFF2E394A)
                                : Color(0xFF643B00)),
                  ),
                ))
              ],
            ),
            SizedBox(
              height: 20.h,
            ),
            Text(
              "Club Joined",
              style: Theme.of(context).textTheme.labelMarco.copyWith(
                  color: type == 0
                      ? Color(0xFFAF6C09)
                      : type == 1
                          ? Color(0xFF505866)
                          : Color(0xFFAF6C09)),
            ),
            Text(
              "Hoi nguoi ngheo",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.titleSmallBold.copyWith(
                  color: type == 0
                      ? Color(0xFF643B00)
                      : type == 1
                          ? Color(0xFF2B3748)
                          : Color(0xFF643B00)),
            ),
            SizedBox(height: 20.h),
            Row(
              children: [
                Expanded(
                    flex: 25,
                    child: Container(
                        height: 4.h,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2.h),
                            color: R.color.white))),
                Expanded(
                    flex: 75,
                    child: Container(
                        height: 4.h,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2.h),
                            color: type == 0
                                ? Color(0xFF762D17)
                                : type == 1
                                    ? Color(0xFF152336)
                                    : Color(0xFFBC740A))))
              ],
            ),
            SizedBox(height: 16.h),
            Row(children: [
              Text(
                "Expired in 20 October 2022",
                style: Theme.of(context).textTheme.labelMarco.copyWith(
                    color: type == 0
                        ? Color(0xFFAF6C09)
                        : type == 1
                            ? Color(0xFF505866)
                            : Color(0xFFAF6C09)),
              ),
              Expanded(
                  child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  "100 Days left",
                  style: Theme.of(context).textTheme.labelMarco.copyWith(
                      color: type == 0
                          ? Color(0xFF643B00)
                          : type == 1
                              ? Color(0xFF2E394A)
                              : Color(0xFF643B00)),
                ),
              ))
            ]),
          ],
        ));
  }
}
