import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/course_details/widget/set_count_course.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/button_widget.dart';
import 'package:imi/src/widgets/search_list_data_popup.dart';
import 'package:imi/src/widgets/text_field_widget.dart';
import 'package:imi/src/utils/validators.dart';

import '../data/network/response/city_response.dart';
import '../page/detail_event/widget/infor_event_order.dart';
import 'dropdown_column_widget.dart';

class EditOrderWidget extends StatefulWidget {
  final String? imageUrl;
  final String? nameCourse;
  final int? currentFee;
  final int? preferentialFee;
  final String? quantity;
  final VoidCallback onCurrentDecrease;
  final VoidCallback onCurrentIncrease;
  final VoidCallback onPopUp;
  final VoidCallback? save;
  final Function(String)? onChanged;
  final bool? checkColor;
  final TextEditingController? textEditingController;
  final TextEditingController? nameEditingController;
  final TextEditingController? phoneEditingController;
  final TextEditingController? emailEditingController;
  String? errorName;
  String? errorPhone;
  String? errorEmail;
  final int? lengthText;
  final TextEditingController quantityEditingController;
  final FocusNode focusNode;
  final List<CityData> province;
  String? currentProvince;
  int? provinceId;
  bool? isEvent;

  EditOrderWidget(
      {Key? key,
      this.imageUrl,
      this.nameCourse,
      this.currentFee,
      this.preferentialFee,
      this.quantity,
      required this.onCurrentDecrease,
      required this.onCurrentIncrease,
      this.save,
      this.checkColor,
      this.onChanged,
      required this.textEditingController,
      required this.province,
      this.provinceId,
      this.currentProvince,
      required this.quantityEditingController,
      required this.focusNode,
      this.lengthText,
      this.isEvent,
      required this.onPopUp,
      this.errorName,
      this.errorEmail,
      this.errorPhone,
      this.nameEditingController,
      this.phoneEditingController,
      this.emailEditingController})
      : super(key: key);

  @override
  State<EditOrderWidget> createState() => _EditOrderWidgetState();
}

class _EditOrderWidgetState extends State<EditOrderWidget> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: MediaQuery.of(context).viewInsets,
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 20.h),
            child: Column(
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10.h),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        width: 118.w,
                        height: 83.h,
                        imageUrl: widget.imageUrl ?? "",
                      ),
                    ),
                    SizedBox(width: 5.w),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.nameCourse ?? "",
                            style: Theme.of(context).textTheme.bold700.copyWith(
                                fontSize: 14.sp, color: R.color.dark_blue),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                child: Text(
                                  "${Utils.formatMoney(widget.currentFee)} VND",
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                        fontSize: 12.sp,
                                        color: R.color.grey,
                                        decoration: TextDecoration.lineThrough,
                                      ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              SizedBox(width: 5.w),
                              Text(
                                "${Utils.formatMoney(widget.preferentialFee)} VND",
                                style: Theme.of(context)
                                    .textTheme
                                    .bold700
                                    .copyWith(
                                        fontSize: 16.sp,
                                        color: R.color.red,
                                        height: 22.h / 16.sp),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      R.string.amount.tr(),
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12.sp, color: R.color.gray),
                    ),
                    SetCountCourseWidget(
                      controller: widget.quantityEditingController,
                      decreaseCount: widget.onCurrentDecrease,
                      increaseCount: widget.onCurrentIncrease,
                      title: widget.quantity ?? "",
                      checkColor: widget.checkColor ?? false,
                    )
                  ],
                ),
                SizedBox(height: 10.h),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(R.string.contact_info.tr(),style: Theme.of(context).textTheme.regular400.copyWith(
                        color: R.color.gray600
                 ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    InformationDetailOrder(R.string.first_and_last_name.tr()),
                    const SizedBox(height: 5),
                    field(
                        widget.nameEditingController,
                        R.string.enter_your_name.tr(),
                        TextInputType.name,
                        [LengthLimitingTextInputFormatter(100)],
                        errorNameText),
                    const SizedBox(height: 5),
                    InformationDetailOrder(R.string.phone_number.tr()),
                    const SizedBox(height: 5),
                    field(
                        widget.phoneEditingController,
                        R.string.text_field_hint.tr(),
                        TextInputType.phone,
                        [LengthLimitingTextInputFormatter(10)],
                        errorPhoneText),
                    const SizedBox(height: 5),
                    InformationDetailOrder(R.string.email.tr()),
                    const SizedBox(height: 5),
                    field(
                        widget.emailEditingController,
                        R.string.enter_your_email.tr(),
                        TextInputType.emailAddress,
                        [LengthLimitingTextInputFormatter(100)],
                        errorEmailText),
                    const SizedBox(height: 5),
                    InformationDetailOrder(R.string.area.tr()),
                    const SizedBox(height: 5),
                    _buildDropdownProvince(context, widget.province),
                    const SizedBox(height: 5),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      R.string.notes_for_dci.tr(),
                      style: Theme.of(context).textTheme.regular400.copyWith(
                          fontSize: 12.sp,
                          color: R.color.gray,
                          height: 18 / 12),
                    ),
                    const SizedBox(width: 5),
                    Tooltip(
                        message:
                            R.string.if_you_are_note_studying_in_person.tr(),
                        textStyle:
                            Theme.of(context).textTheme.regular400.copyWith(
                                  fontSize: 11.sp,
                                  color: R.color.primaryColor,
                                ),
                        padding: EdgeInsets.symmetric(
                            horizontal: 12.w, vertical: 6.h),
                        margin:
                            EdgeInsetsDirectional.fromSTEB(100.w, 0, 30.w, 0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.h),
                            color: R.color.white),
                        preferBelow: false,
                        showDuration:const Duration(hours: 1),
                        triggerMode: TooltipTriggerMode.tap,
                        child: Icon(CupertinoIcons.info_circle,
                            size: 16.h, color: R.color.primaryColor)),
                  ],
                ),
                SizedBox(height: 10.h),
                SizedBox(
                  height: 200.h,
                  child: TextField(
                    autofocus: false,
                    controller: widget.textEditingController,
                    focusNode: widget.focusNode,
                    onChanged: widget.onChanged,
                    keyboardType: TextInputType.text,
                    style: Theme.of(context)
                        .textTheme
                        .medium500
                        .copyWith(fontSize: 12.sp, color: R.color.black),
                    inputFormatters: [LengthLimitingTextInputFormatter(500)],
                    maxLines: 100,
                    decoration: InputDecoration(
                      hintText:
                      widget.isEvent==true?R.string.note_join_event.tr():R.string.if_you_are_note_studying_in_person.tr(),
                      border: InputBorder.none,
                      hintStyle: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 12.sp, color: R.color.grey),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: R.color.grey)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: R.color.grey)),
                    ),
                  ),
                ),
                SizedBox(height: 4.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: '${widget.lengthText}',
                        style: Theme.of(context)
                            .textTheme
                            .regular400
                            .copyWith(fontSize: 12.sp, color: R.color.gray),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' / 500',
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                      fontSize: 12.sp, color: R.color.gray)),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 4.h),
                ButtonWidget(
                  padding: EdgeInsets.symmetric(vertical: 9.h),
                  backgroundColor: R.color.primaryColor,
                  title: R.string.save.tr(),
                  onPressed: widget.save,
                  textSize: 12.sp,
                ),
              ],
            ),
          ),
          Positioned(
            top: 15,
            right: 9,
            child: InkWell(
              onTap: widget.onPopUp,
              child: const Icon(Icons.close),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDropdownProvince(
      BuildContext context, List<CityData> listProvince) {
    return Container(
      padding: EdgeInsets.only(left: 10.w, top: 2.h),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: DropdownColumnWidget(
        onTap: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return SearchListDataPopup(
                  listData: listProvince.map((e) => e.cityName ?? "").toList(),
                  hintText: "${R.string.search.tr()} ${R.string.province.tr().toLowerCase()}",
                  onSelect: (int index) {
                    setState(() {
                      getCity(index);
                    });
                  },
                );
              });
        },
        hintText: R.string.province.tr(),
        // errorText: _cubit.errorProvince,
        currentValue: widget.currentProvince,
        listData: listProvince.map((e) => e.cityName ?? "").toList(),
        selectedValue: (String? value) {},
      ),
    );
  }

  Widget field(
      TextEditingController? textEditingController,
      String title,
      TextInputType inputType,
      List<TextInputFormatter>? fomartter,
      String? error) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.h),
          border: Border.all(width: 1, color: R.color.gray)),
      child: TextFieldWidget(
        autoFocus: false,
        controller: textEditingController!,
        keyboardType: inputType,
        isRequired: true,
        maxLines: 1,
        inputFormatters: fomartter,
        hintText: title,
        borderSize: BorderSide.none,
        // onTap: () {},
        errorText: error,
      ),
    );
  }

  String? get errorPhoneText {
    final text = widget.phoneEditingController?.text;
    if (text != null) {
      if (text.isEmpty) {
        return R.string.phone_number_not_empty.tr();
      }
    }
    return null;
  }

  String? get errorNameText {
    final text = widget.nameEditingController?.text;
    if (text != null) {
      if (text.isEmpty) {
        return R.string.please_enter_full_name.tr();
      }
    }
    return null;
  }

  String? get errorEmailText {
    final text = widget.emailEditingController?.text;
    if (text != null) {
      if (text.isEmpty) {
        return R.string.please_enter_email.tr();
      }
      if (!Validators.emailRegex.hasMatch(text.trim())) {
        return R.string.email_not_valid.tr();
      }
    }
    return null;
  }

  String getCity(index) {
    widget.currentProvince = widget.province[index].cityName ?? '';
    appPreferences.setData('ProvinceId', widget.province[index].cityId ?? 0);
    appPreferences.setData('Province', widget.province[index].cityName ?? 'Hà Nội');
    return widget.currentProvince ?? '';
  }
}
