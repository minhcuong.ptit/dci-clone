import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/string_ext.dart';
import 'package:imi/src/widgets/star_widget.dart';

class CommunityWidget extends StatelessWidget {
  final CommunityData data;
  final VoidCallback onTap;
  final VoidCallback? onTapFollow;
  final bool isSelected;
  final bool isShowStar;
  final double? width;
  final bool? isFromListing;
  final bool? studyGroup;
  const CommunityWidget(
      {Key? key,
      required this.data,
      required this.onTap,
      this.isSelected = false,
      this.isShowStar = false,
      this.onTapFollow,
      this.width,
      this.isFromListing,
      this.studyGroup})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 180.h,
        width: width ?? double.infinity,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 4.h),
              decoration: BoxDecoration(
                  color: R.color.white,
                  border: Border.all(
                      color: (isFromListing ?? false)
                          ? R.color.primaryColor
                          : isSelected
                              ? R.color.primaryColor
                              : R.color.lightestGray,
                      width: 1),
                  borderRadius: BorderRadius.circular(8.h)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 8.h,
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40.h),
                        child: CachedNetworkImage(
                          height: 80.h,
                          width: 80.h,
                          imageUrl: data.avatarUrl ?? "",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    data.name ?? "",
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodySmallBold.copyWith(),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        R.drawable.ic_card_person,
                        width: 12.h,
                        height: 12.h,
                        color: R.color.black,
                      ),
                      SizedBox(
                        width: 4.w,
                      ),
                      Text(
                        NumberFormat.compact().format(
                            (data.communityInfo?.memberNo ??
                                data.memberNo ??
                                0)),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .labelSmallText
                            .copyWith(color: R.color.grey),
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(
                        width: 4.w,
                      ),
                      Container(
                        height: 3.h,
                        width: 3.w,
                        decoration: BoxDecoration(
                          color: R.color.grey,
                          shape: BoxShape.circle,
                        ),
                      ),
                      SizedBox(
                        width: 4.w,
                      ),
                      Image.asset(
                        R.drawable.ic_group_person,
                        width: 12.h,
                        height: 12.h,
                        color: R.color.black,
                      ),
                      SizedBox(
                        width: 4.w,
                      ),
                      Text(
                        NumberFormat.compact().format(
                            (data.communityInfo?.followerNo ??
                                data.followerNo ??
                                0)),
                        style: Theme.of(context)
                            .textTheme
                            .labelSmallText
                            .copyWith(color: R.color.grey),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                  SizedBox(height: 16.h),
                ],
              ),
            ),
            Visibility(
              visible: studyGroup == false,
              child: Positioned(
                  top: 8.h,
                  right: 8.h,
                  child: GestureDetector(
                    onTap: onTapFollow,
                    child: Icon(
                      isSelected
                          ? CupertinoIcons.checkmark_alt
                          : CupertinoIcons.add,
                      color: isSelected ? R.color.primaryColor : R.color.black,
                      size: 20.h,
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
