import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../res/R.dart';

class DCIStepsWidget extends StatefulWidget {
  final List<Widget> children;
  final int currentIndex;
  final Widget saveButton;
  const DCIStepsWidget(
      {Key? key,
      required this.children,
      required this.currentIndex,
      required this.saveButton})
      : assert(currentIndex < children.length, "current index out of bound"),
        super(key: key);

  @override
  DCIStepsWidgetState createState() => DCIStepsWidgetState();
}

class DCIStepsWidgetState extends State<DCIStepsWidget> {
  late final List<double> _childrenHeight;

  @override
  void initState() {
    super.initState();
    _childrenHeight = List.generate(widget.children.length, (index) => 0);
  }

  double getStepLocation(int step) {
    assert(step < widget.children.length, "step out of bound");
    return _childrenHeight
        .take(step)
        .fold(0, (previousValue, value) => previousValue + value);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(widget.children.length + 1, (index) {
        if (index == widget.children.length) {
          return widget.saveButton;
        }

        final child = widget.children[index];
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: widget.currentIndex >= index
                          ? R.color.primaryColor
                          : R.color.textGrey1,
                      borderRadius: BorderRadius.circular(100)),
                  height: 24.h,
                  width: 24.h,
                  alignment: Alignment.center,
                  child: Text(
                    '${index + 1}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 12.sp,
                        height: 16.h / 12.sp,
                        color: R.color.white),
                  ),
                ),
                if (index < widget.children.length - 1)
                  ..._dashLine(
                      _childrenHeight[index],
                      (index <= widget.currentIndex)
                          ? R.color.primaryColor
                          : R.color.gray)
              ],
            ),
            SizedBox(
              width: 8.w,
            ),
            WidgetSizeOffsetWrapper(
                onSizeChange: (Size size) {
                  setState(() {
                    _childrenHeight[index] = size.height;
                  });
                },
                child: child)
          ],
        );
      }),
    );
  }

  List<Widget> _dashLine(double height, Color color) {
    return List.generate(height ~/ 16.h, (index) {
      return Container(
        height: 8.h,
        width: 2.w,
        color: color,
        margin: EdgeInsets.only(bottom: 8.h),
      );
    });
  }
}

typedef void OnWidgetSizeChange(Size size);

class WidgetSizeRenderObject extends RenderProxyBox {
  final OnWidgetSizeChange onSizeChange;
  Size? currentSize;

  WidgetSizeRenderObject(this.onSizeChange);

  @override
  void performLayout() {
    super.performLayout();

    try {
      Size? newSize = child?.size;

      if (newSize != null && currentSize != newSize) {
        currentSize = newSize;
        WidgetsBinding.instance.addPostFrameCallback((_) {
          onSizeChange(newSize);
        });
      }
    } catch (e) {
      print(e);
    }
  }
}

class WidgetSizeOffsetWrapper extends SingleChildRenderObjectWidget {
  final OnWidgetSizeChange onSizeChange;

  const WidgetSizeOffsetWrapper({
    Key? key,
    required this.onSizeChange,
    required Widget child,
  }) : super(key: key, child: child);

  @override
  RenderObject createRenderObject(BuildContext context) {
    return WidgetSizeRenderObject(onSizeChange);
  }
}
