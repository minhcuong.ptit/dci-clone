import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/num_ext.dart';

class CommunityInfoWidget extends StatelessWidget {
  final List<String> titles;
  final List<int> details;
  final List<VoidCallback> callbacks;

  const CommunityInfoWidget(
      {Key? key,
      required this.details,
      required this.titles,
      required this.callbacks})
      : assert(details.length == titles.length, "length not matches"),
        assert(callbacks.length == titles.length, "length not matches"),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle numberStyle =
        Theme.of(context).textTheme.bold700.copyWith(fontSize: 14.sp);
    TextStyle textStyle = Theme.of(context)
        .textTheme
        .regular400
        .copyWith(fontSize: 10.sp, color: R.color.lighterGray);

    return Row(
      children: List.generate(details.length * 2 - 1, (index) {
        if (index % 2 == 0) {
          return InkWell(
            onTap: this.callbacks[index ~/ 2],
            child: SizedBox(
              width: (MediaQuery.of(context).size.width - details.length) /
                  titles.length,
              child: Column(
                children: [
                  Text(
                    details[index ~/ 2].quantity(),
                    style: numberStyle,
                  ),
                  Text(
                    titles[index ~/ 2].toString(),
                    style: textStyle,
                  ),
                ],
              ),
            ),
          );
        }
        return Container(
          width: 1.w,
          height: 40,
          margin: EdgeInsets.symmetric(vertical: 4.h),
          color: R.color.lightGray,
        );
      }),
    );
  }
}
