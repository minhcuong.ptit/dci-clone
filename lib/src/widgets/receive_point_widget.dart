import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';

import '../../res/R.dart';
import '../page/main/main.dart';
import '../utils/navigation_utils.dart';

class ReceivePointWidget extends StatelessWidget {
  final int point;
  final String typePoint;

  ReceivePointWidget({required this.point, required this.typePoint});

  @override
  Widget build(BuildContext context) {
    MainCubit mainCubit=GetIt.I();
    return AlertDialog(
        contentPadding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15))),
        content: SizedBox(
          height: 320,
          width: 180,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset(R.drawable.receive_point,
                fit:BoxFit.cover
                ),
              ),
              // SizedBox(height: 30.h),
              Positioned(
                  child: Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(
                        R.string.congratulations_point.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .bold700
                            .copyWith(fontSize: 28.sp, color: R.color.dark_blue),
                        textAlign: TextAlign.center,
                ),
                    ),
                  ),
              ),
              Positioned(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(left: 15.w,right: 15.w,bottom: 60.h),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: R.string.you_have_received.tr(),
                        style: Theme.of(context)
                            .textTheme
                            .regular400
                            .copyWith(fontSize: 16.sp, color: R.color.black),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' ${point} ${R.string.point.tr()}',
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w500,
                                  color: R.color.primaryColor)),
                          TextSpan(
                              text: ' ${R.string.dci_as.tr()} ',
                              style: Theme.of(context)
                                  .textTheme
                                  .regular400
                                  .copyWith(
                                  fontSize: 16.sp, color: R.color.black)),
                          if (typePoint == "COURSE") ...[
                            TextSpan(
                                text: '${R.string.buy_course.tr()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w500,
                                    color: R.color.primaryColor))
                          ],
                          if (typePoint == "EVENT") ...[
                            TextSpan(
                                text: '${R.string.join_event.tr()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w500,
                                    color: R.color.primaryColor))
                          ],
                          if (typePoint == "PRACTICE") ...[
                            TextSpan(
                                text: '${R.string.challenge_practice.tr()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w500,
                                    color: R.color.primaryColor))
                          ],
                          if (typePoint == "EVENT_COURSE") ...[
                            TextSpan(
                                text: '${R.string.buy_course_and_event.tr()}',
                                style: Theme.of(context)
                                    .textTheme
                                    .regular400
                                    .copyWith(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w500,
                                    color: R.color.primaryColor))
                          ],
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // Positioned(
              //   top: 70,
              //   right: 0,
              //   left: 0,
              //   child: Image.asset(
              //     R.drawable.ic_diamond_point,
              //     height: 124.h,
              //     width: 124.w,
              //   ),
              // )
            ],
          ),
        ));
  }
}
