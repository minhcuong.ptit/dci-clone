import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../res/R.dart';
import '../utils/utils.dart';

class ItemCourse extends StatelessWidget {
  final String url;
  final String name;
  final String price;
  final String discount;
  final bool isHot;

  const ItemCourse(
      {Key? key,
      required this.url,
      required this.name,
      required this.price,
      required this.discount,
      required this.isHot})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.all(4.h),
          decoration: BoxDecoration(
              color: R.color.white, borderRadius: BorderRadius.circular(8.h)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.circular(8.h),
                  child: CachedNetworkImage(
                    height: 108.h,
                    width: double.infinity,
                    imageUrl: url ?? "",
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      height: 108.h,
                      width: double.infinity,
                    ),
                    errorWidget: (context, url, error) => Image.asset(
                      R.drawable.ic_default_item,
                      fit: BoxFit.cover,
                      height: 108.h,
                      width: double.infinity,
                    ),
                  )),
              SizedBox(height: 4.h),
              Text(
                name ?? "",
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.medium500.copyWith(
                    fontSize: 12.sp,
                    color: R.color.neutral1,
                    height: 16.h / 12.sp),
              ),
              SizedBox(height: 6.h),
              Spacer(),
              Row(
                children: [
                  Expanded(
                    child: Visibility(
                      visible: price!= discount,
                      child: Text(
                        "${Utils.formatMoney(price)} VND",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.regular400.copyWith(
                            fontSize: 10.sp,
                            color: R.color.gray,
                            decoration: TextDecoration.lineThrough,
                            height: 15.h / 10.sp),
                      ),
                    ),
                  ),
                  SizedBox(width: 10.w),
                  Text(
                    "${Utils.formatMoney(discount)} VND",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.regular400.copyWith(
                        fontSize: 12.sp,
                        color: R.color.red,
                        height: 18.h / 12.sp),
                  ),
                ],
              )
            ],
          ),
        ),
        isHot == true
            ? Positioned(
                top: 6,
                left: -6,
                child: Stack(
                  children: [
                    Image.asset(R.drawable.ic_hot, height: 18.h, width: 34.w),
                  ],
                ),
              )
            : SizedBox(),
      ],
    );
  }
}
