import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';

class AvatarWidget extends StatelessWidget {
  final String? avatar;
  final double size;
  final String? placeHolder;
  final bool isEnabled;
  final double? radius;

  const AvatarWidget(
      {Key? key,
      required this.avatar,
      required this.size,
      this.placeHolder,
      this.isEnabled = true,
      this.radius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        // clipBehavior: Clip.none,
        borderRadius: BorderRadius.circular(radius ?? (size / 2)),
        child: CachedNetworkImage(
          imageUrl: avatar ?? "",
          fit: BoxFit.cover,
          width: size,
          height: size,
          imageBuilder: (context, imageProvider) =>  Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                    colorFilter: isEnabled
                        ? null
                        : ColorFilter.mode(R.color.grey400, BlendMode.color)),
              ),
            ),
          placeholder: (context, url) => Image.asset(
            placeHolder ?? R.drawable.ic_avatar,
            width: size,
            height: size,
          ),
          errorWidget: (context, url, _) => Image.asset(
            placeHolder ?? R.drawable.ic_avatar,
            // width: size,
            // height: size,
          ),
        ),
      );
  }
}
