import 'package:flutter/material.dart';
import 'package:imi/res/R.dart';

class AvatarNonCacheWidget extends StatelessWidget {
  final String? avatar;
  final double size;
  final String? placeHolder;
  final bool isEnabled;

  const AvatarNonCacheWidget(
      {Key? key,
      required this.avatar,
      required this.size,
      this.placeHolder,
      this.isEnabled = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        // clipBehavior: Clip.none,
        borderRadius: BorderRadius.circular(size / 2),
        child: FadeInImage(
          image: NetworkImage(avatar ?? ""),
          height: size,
          width: size,
          fit: BoxFit.cover,
          placeholder: AssetImage(
            placeHolder ?? R.drawable.ic_avatar,
          ),
        ));
  }
}
