import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../res/R.dart';

AppBar appBar(BuildContext context, String title,
    {Widget? leadingWidget,
    Widget? rightWidget,
      VoidCallback? onPop,
      VoidCallback? directToProfile,
    Color? backgroundColor,
    Color? iconColor,
    Color? titleColor,
    double? elevation,
    TextStyle? textStyle,
    bool centerTitle = false}) {
  return AppBar(
    backgroundColor: backgroundColor ?? R.color.white,
    centerTitle: centerTitle,
    elevation: elevation ?? 0,
    leadingWidth: 60.w,
    titleSpacing: 0,
    leading: leadingWidget ??
        (Navigator.of(context).canPop() == true
            ? IconButton(
                onPressed: () {
                  if(onPop != null) onPop();
                  else if (Navigator.canPop(context)) {
                    Navigator.of(context).pop();
                  }
                },
                icon: Icon(
                  CupertinoIcons.left_chevron,
                  color: iconColor ?? R.color.black,
                ))
            : null),
    iconTheme: IconThemeData(color: iconColor ?? R.color.black),
    title: Padding(
      padding: EdgeInsets.only(bottom: 3.h),
      child: GestureDetector(
        onTap: directToProfile,
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: textStyle ??
              Theme.of(context)
                  .textTheme
                  .medium500
                  .apply(color: titleColor ?? R.color.black)
                  .copyWith(fontSize: 16.sp, fontWeight: FontWeight.w700),
        ),
      ),
    ),
    actions: rightWidget == null ? null : [rightWidget],
  );
}
