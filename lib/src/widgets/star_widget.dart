import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';

class StarWidget extends StatelessWidget {
  final double? itemSize;
  final int avgPoint;
  final int countPoint;
  final bool fill;
  final bool isHideUnRate;

  StarWidget(
      {this.itemSize,
      this.avgPoint = 0,
      this.countPoint = 4,
      this.fill = true,
      this.isHideUnRate = false});

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: RatingBar.builder(
        itemSize: itemSize ?? 20.h,
        initialRating: avgPoint.toDouble(),
        minRating: 0,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: isHideUnRate ? avgPoint : countPoint,
        itemPadding: EdgeInsets.symmetric(horizontal: 2),
        itemBuilder: (context, index) {
          if (fill) {
            return Icon(
              Icons.star,
              color: R.color.primaryColor,
            );
          } else {
            if (index <= avgPoint - 1) {
              return Icon(
                Icons.star,
                color: R.color.primaryColor,
              );
            } else {
              return Icon(
                Icons.star_outline,
                color: R.color.primaryColor,
              );
            }
          }
        },
        unratedColor: fill ? R.color.grey : R.color.primaryColor,
        onRatingUpdate: (rating) {
          // print(rating);
        },
      ),
    );
  }
}
