import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';

class TextFieldWidget extends StatefulWidget {
  const TextFieldWidget(
      {required this.controller,
      this.textInputAction: TextInputAction.next,
      this.isEnable = true,
      this.buildCounter,
      this.maxLength,
      this.autoFocus = true,
      this.isRequired = false,
      this.border,
      this.borderSize,
      this.fillColor,
      this.onChanged,
      this.padding,
      this.isPassword: false,
      this.icon,
      this.errorText,
      this.labelText,
      this.titleText,
      this.hintText,
      this.inputFormatters,
      this.minLines,
      this.maxLines,
      this.height,
      this.keyboardType: TextInputType.text,
      this.focusNode,
      this.readOnly: false,
      this.onTap,
      this.suffixIcon,
      this.onTapRightIcon,
      this.onSubmitted,
      this.disableTap = false,
      this.customInputStyle,
      this.labelStyle,
      this.underLineColor, this.errorTextStyle});

//  final GlobalKey key;
  final TextEditingController controller;
  final bool isEnable;
  final bool autoFocus;
  final EdgeInsets? padding;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputAction textInputAction;
  final FormFieldSetter<String>? onChanged;
  final bool isPassword;
  final bool isRequired;
  final String? errorText;
  final String? labelText;
  final String? titleText;
  final String? hintText;
  final int? minLines;
  final int? maxLines;
  final double? border;
  final Color? fillColor;
  final BorderSide? borderSize;
  final TextInputType keyboardType;
  final FocusNode? focusNode;
  final FormFieldSetter<String>? onSubmitted;
  final dynamic icon;
  final double? height;
  final bool readOnly;
  final bool disableTap;
  final Function()? onTap;
  final Widget? suffixIcon;
  final Function? onTapRightIcon;
  final TextStyle? customInputStyle;
  final TextStyle? labelStyle;
  final TextStyle? errorTextStyle;
  final InputCounterWidgetBuilder? buildCounter;
  final int? maxLength;
  final Color? underLineColor;

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  bool _obscureText = false;

  @override
  void initState() {
    // TODO: implement initState
    if (widget.isPassword == true) {
      Utils.onWidgetDidBuild(() {
        setState(() {
          _obscureText = true;
        });
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
            visible: !Utils.isEmpty(widget.titleText),
            child: RichText(
              text: TextSpan(
                  text: widget.titleText ?? "",
                  style: Theme.of(context)
                      .textTheme
                      .labelLargeText
                      .copyWith(color: R.color.textFieldTitle),
                  children: widget.isRequired
                      ? <TextSpan>[
                          TextSpan(
                            text: R.string.star.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .labelSmallText
                                .copyWith(color: R.color.red),
                          ),
                        ]
                      : []),
            )),
        AbsorbPointer(
          absorbing: widget.disableTap,
          child: TextField(
            enabled: widget.isEnable,
            autofocus: widget.autoFocus,
            focusNode: widget.focusNode,
            controller: widget.controller,
            obscureText: _obscureText,
            textInputAction: widget.textInputAction,
            inputFormatters: widget.inputFormatters,
            readOnly: widget.readOnly,
            buildCounter: widget.buildCounter,
            maxLength: widget.maxLength,
            onTap: widget.onTap,
            cursorColor: R.color.black,
            decoration: InputDecoration(
                labelText: widget.labelText,
                hintText: widget.hintText,
                fillColor: widget.isEnable
                    ? (widget.fillColor ?? R.color.white)
                    :(widget.fillColor?? R.color.grey100),
                filled: true,
                isDense: true,
                contentPadding: widget.padding ??
                    EdgeInsets.only(left: 0, right: 0, top: 7.h, bottom: 7.h),
                focusedBorder: UnderlineInputBorder(
                  borderSide: widget.borderSize ??
                      BorderSide(
                          color: widget.underLineColor ?? R.color.primaryColor,
                          width: 1.0),
                  // borderRadius: BorderRadius.circular(widget.border ?? 10.h),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: widget.borderSize ??
                      BorderSide(
                          color: widget.underLineColor ?? R.color.lighterGray,
                          width: 1.0),
                  // borderRadius: BorderRadius.circular(widget.border ?? 10.h),
                ),
                border: UnderlineInputBorder(
                  borderSide: widget.borderSize ??
                      BorderSide(color: R.color.lighterGray, width: 1.0),
                  // borderRadius: BorderRadius.circular(widget.border ?? 10.h),
                ),
                errorText: widget.errorText,
                errorMaxLines: 1000,
                errorBorder: UnderlineInputBorder(
                  borderSide: widget.borderSize ??
                      BorderSide(color: R.color.red, width: 1.0),
                  // borderRadius: BorderRadius.circular(widget.border ?? 10.h),
                ),
                // prefixText: widget.isRequired ? "*" : "",
                prefixStyle: TextStyle(
                  color: R.color.red,
                ),
                counterStyle: Theme.of(context)
                    .textTheme
                    .regular400
                    .copyWith(fontSize: 10.sp),
                prefixIcon: widget.icon == null
                    ? null
                    : (widget.icon is Widget
                        ? widget.icon
                        : (widget.icon is String
                            ? Padding(
                                child: Image.asset(
                                  widget.icon,
                                  fit: BoxFit.fitHeight,
                                  height: 5,
                                  color: R.color.black,
                                ),
                                padding: EdgeInsets.all(12),
                              )
                            : Icon(
                                widget.icon,
                                size: 20,
                                color: R.color.black,
                              ))),
                suffixIcon: widget.suffixIcon != null
                    ? GestureDetector(
                        onTap: () => widget.onTapRightIcon == null
                            ? null
                            : widget.onTapRightIcon!(),
                        child: widget.suffixIcon,
                      )
                    : widget.isPassword
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: Icon(
                              _obscureText
                                  ? CupertinoIcons.eye_slash
                                  : CupertinoIcons.eye,
                              semanticLabel: _obscureText
                                  ? 'show password'
                                  : 'hide password',
                              color: R.color.black,
                            ),
                          )
                        : null,
                labelStyle: widget.labelStyle ??
                    TextStyle(
                        color: R.color.shadesGray,
                        fontSize: 12.sp,
                        fontWeight: FontWeight
                            .w500) /*.copyWith(color: widget.isEnable ? R.color.black : R.color.borderColor)*/,
                hintStyle: TextStyle(
                  fontSize: 13.sp,
                  height: 24 / 12.sp,
                  color: R.color.grey,
                ),
                errorStyle:widget.errorTextStyle==null? Theme.of(context).textTheme.bodySmallText.copyWith(
                      color: R.color.red,
                    ):widget.errorTextStyle),
            keyboardType: widget.keyboardType,
            onChanged: widget.onChanged,
            onSubmitted: widget.onSubmitted,
            minLines: widget.minLines,
            maxLines: widget.isPassword == true ? 1 : widget.maxLines,
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            style: widget.customInputStyle ??
                Theme.of(context).textTheme.bodyRegular.copyWith(
                      color:
                          widget.isEnable ? R.color.black : R.color.disableGray,
                    ),
          ),
        ),
      ],
    );
  }
}
