import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../res/R.dart';

class DCIPopup extends StatefulWidget {
  final Widget child;
  const DCIPopup({
    required this.child,
  });
  @override
  _dciPopupState createState() => _dciPopupState();
}

class _dciPopupState extends State<DCIPopup>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: [
        Container(
          height: 20,
          decoration: BoxDecoration(
            color: R.color.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(16.h),
              topLeft: Radius.circular(16.h),
            ),
          ),
          child: Center(
            child: Container(
              color: R.color.grey200,
              width: 80.w,
              height: 2.h,
            ),
          ),
        ),
        widget.child,
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
