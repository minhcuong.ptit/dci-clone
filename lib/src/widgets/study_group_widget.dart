import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';

class StudyGroupWidget extends StatelessWidget {
  final CommunityDataV2 data;
  final VoidCallback onTap;
  final VoidCallback? onTapFollow;
  final bool isSelected;
  final bool isShowStar;
  final double? width;
  final bool? isFromListing;
  const StudyGroupWidget({
    Key? key,
    required this.data,
    required this.onTap,
    this.isSelected = false,
    this.isShowStar = false,
    this.onTapFollow,
    this.width,
    this.isFromListing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 180.h,
        width: width ?? double.infinity,
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: R.color.white,
                  border: Border.all(
                      color: (isFromListing ?? false)
                          ? R.color.primaryColor
                          : isSelected
                              ? R.color.primaryColor
                              : R.color.lightestGray,
                      width: 1),
                  borderRadius: BorderRadius.circular(8.h)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 8.h,
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40.h),
                        child: CachedNetworkImage(
                          height: 80.h,
                          width: 80.h,
                          memCacheHeight: 200,
                          memCacheWidth: 200,
                          imageUrl: data.avatarUrl ?? "",
                          fit: BoxFit.cover,
                          placeholder: (context, url) => Image.asset(R.drawable.ic_avatar),
                          errorWidget: (context, url, error) =>  Image.asset(R.drawable.ic_avatar),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    data.name ?? "",
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodySmallBold.copyWith(),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          CupertinoIcons.clock,
                          size: 12.h,
                          color: R.color.grey,
                        ),
                        SizedBox(width: 5.h),
                        Text(
                          DateUtil.parseDateToString(
                              DateTime.fromMillisecondsSinceEpoch(
                                  data.groupView?.event?.startTime ?? 0),
                              Const.TIME_FORMAT),
                          style: Theme.of(context)
                              .textTheme
                              .regular400
                              .copyWith(fontSize: 10.sp, height: 14.h / 10.sp),
                        ),
                        SizedBox(width: 5.h),
                        Expanded(
                          child: Text(
                            data.groupView?.event?.dayOfWeek?.titleDayOfWeek ??
                                "",
                            style: Theme.of(context)
                                .textTheme
                                .regular400
                                .copyWith(fontSize: 8.sp, height: 14.h / 8.sp),
                          ),
                        ),
                        Image.asset(
                          R.drawable.ic_card_person,
                          width: 12.h,
                          height: 12.h,
                          color: R.color.black,
                        ),
                        SizedBox(
                          width: 4.w,
                        ),
                        Text(
                          NumberFormat.compact()
                              .format((data.communityInfo?.memberNo ?? 0)),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .labelSmallText
                              .copyWith(color: R.color.grey, fontSize: 8.sp),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                  for (int i = 0; i < data.groupView!.bomMembers!.length; i++)
                    if (data.groupView?.bomMembers?[i]?.role ==
                            MemberType.LEADER.name &&
                        (data.groupView?.bomMembers?[i]?.bomMembers
                                ?.isNotEmpty ??
                            false))
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.h),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Icon(
                              Icons.perm_contact_calendar_outlined,
                              size: 16.h,
                            ),
                            SizedBox(width: 5.h),
                            Expanded(
                              child: Text(data.groupView?.bomMembers?[i]
                                      ?.bomMembers?.first?.fullName ??
                                  ""),
                            ),
                          ],
                        ),
                      ),
                  SizedBox(height: 16.h),
                ],
              ),
            ),
            // Positioned(
            //     top: 8.h,
            //     right: 8.h,
            //     child: GestureDetector(
            //       onTap: onTapFollow,
            //       child: Icon(
            //         isSelected
            //             ? CupertinoIcons.checkmark_alt
            //             : CupertinoIcons.add,
            //         color: isSelected ? R.color.primaryColor : R.color.black,
            //         size: 20.h,
            //       ),
            //     ))
          ],
        ),
      ),
    );
  }
}
