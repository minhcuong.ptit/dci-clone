import 'package:flutter/material.dart';

class ExpandableCardWidget extends StatefulWidget {
  final bool isExpanded;
  final Widget collapsedChild;
  final Widget expandedChild;

  const ExpandableCardWidget(
      {Key? key,
      required this.isExpanded,
      required this.collapsedChild,
      required this.expandedChild})
      : super(key: key);

  @override
  _ExpandableCardWidgetState createState() => _ExpandableCardWidgetState();
}

class _ExpandableCardWidgetState extends State<ExpandableCardWidget> {
  bool isExpand = true;

  @override
  void initState() {
    isExpand = widget.isExpanded;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new AnimatedContainer(
      duration: new Duration(milliseconds: 200),
      curve: Curves.easeInOut,
      child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            setState(() {
              isExpand = !isExpand;
            });
          },
          child: isExpand ? widget.expandedChild : widget.collapsedChild),
    );
  }
}
