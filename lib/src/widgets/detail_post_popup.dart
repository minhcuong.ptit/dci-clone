import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../data/network/response/post_data.dart';

class DetailPostPopup extends StatelessWidget {
  final VoidCallback onEdit;
  final VoidCallback onDelete;
  final VoidCallback onBookmark;
  final VoidCallback onReportPost;
  final bool? showEdit;
  final bool? showDelete;
  final PostData? post;

  const DetailPostPopup(
      {Key? key,
      required this.onEdit,
      required this.onDelete,
      this.showEdit,
      this.showDelete,
      required this.onBookmark,
      required this.post,
      required this.onReportPost})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.white,
      child: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Visibility(
                    visible: showDelete ?? true,
                    child: _buildRow(context, false)),
                Visibility(
                    visible: showEdit ?? true,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(height: 1.h, color: R.color.lightestGray),
                        _buildRow(context, true),
                      ],
                    )),
                Visibility(
                    visible: (showDelete ?? true) || (showEdit ?? true),
                    child: Container(height: 1.h, color: R.color.lightestGray)),
                _buildRowBookmark(context, post?.isBookmark ?? false),
                Visibility(
                    visible: true,
                    child: Container(height: 1.h, color: R.color.lightestGray)),
                _buildRowCopyLink(context, post?.isBookmark ?? false),
                Visibility(
                    visible: true,
                    child: Container(height: 1.h, color: R.color.lightestGray)),
                post?.userId != appPreferences.personProfile?.userUuid
                    ? _buildRowReportPost(context)
                    : SizedBox(),
                SizedBox(height: 28.h),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRow(BuildContext context, bool isEdit) {
    return InkWell(
        onTap: () {
          NavigationUtils.pop(context);
          isEdit ? onEdit() : onDelete();
        },
        child: _buildItem(
            context,
            isEdit ? R.drawable.ic_edit_post : R.drawable.ic_delete_post,
            isEdit ? R.string.edit_post.tr() : R.string.delete_post.tr()));
  }

  Widget _buildRowBookmark(BuildContext context, bool isBookmarked) {
    return InkWell(
        onTap: () {
          NavigationUtils.pop(context);
          onBookmark();
        },
        child: _buildItem(
            context,
            isBookmarked
                ? R.drawable.ic_bookmark_on
                : R.drawable.ic_bookmark_off,
            isBookmarked ? R.string.unsave.tr() : R.string.save_post.tr()));
  }

  Widget _buildRowCopyLink(BuildContext context, bool isBookmarked) {
    return InkWell(
        onTap: () {
          Clipboard.setData(ClipboardData(text: post?.dynamicLink))
              .whenComplete(() => NavigationUtils.pop(context));
        },
        child:
            _buildItem(context, R.drawable.ic_link, R.string.copy_link.tr()));
  }

  Widget _buildRowReportPost(BuildContext context) {
    return InkWell(
        onTap: () {
          NavigationUtils.pop(context);
          onReportPost();
        },
        child: _buildItem(
            context, R.drawable.ic_report_user, R.string.report_post.tr()));
  }

  Widget _buildItem(BuildContext context, String icon, String text) {
    return Row(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 24.h),
          child: Image.asset(
            icon,
            fit: BoxFit.fill,
            width: 24.w,
          ),
        ),
        SizedBox(width: 10.w),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(bottom: 4.h),
            child: Text(text, style: Theme.of(context).textTheme.bodyBold),
          ),
        ),
      ],
    );
  }
}
