import 'package:flutter/material.dart';
import 'package:imi/res/R.dart';

import 'gradient_circular_progress_indicator.dart';

class PendingAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: GradientCircularProgressIndicator(
            gradient: SweepGradient(colors: [R.color.primaryColor, Color(0xFF28166F)]),
          ),
        ),
        ModalBarrier(
          dismissible: false,
          color: Colors.black.withOpacity(0.2),
        ),
      ],
    );
  }
}
