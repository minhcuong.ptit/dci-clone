import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/detail_library_response.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/stack_loading_view.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class WebViewPdf extends StatefulWidget {
  String? linkPdf;

  WebViewPdf(this.linkPdf);

  @override
  State<WebViewPdf> createState() => _WebViewPdfState();
}

class _WebViewPdfState extends State<WebViewPdf> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              widget.linkPdf == ""
                  ? Container()
                  : SfPdfViewer.network(
                      widget.linkPdf ?? "",
                      pageLayoutMode: PdfPageLayoutMode.single,
                    ),
              Positioned(
                top: 10.h,
                right: 10.h,
                child: InkWell(
                  onTap: () {
                    NavigationUtils.pop(context);
                  },
                  child: const Icon(CupertinoIcons.clear_thick_circled),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
