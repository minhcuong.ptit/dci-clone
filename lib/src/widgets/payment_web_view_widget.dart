import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/widgets/custom_appbar.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPayment extends StatefulWidget {
  final String url;

  WebViewPayment(this.url);

  @override
  WebViewPaymentState createState() => WebViewPaymentState();
}

class WebViewPaymentState extends State<WebViewPayment> {
  @override
  void initState() {
    super.initState();
    // Enable virtual display.
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
              alignment: Alignment.centerRight,
              padding:const EdgeInsets.only(top: 50, right: 10, bottom: 10),
              child: InkWell(
                onTap: () {
                  NavigationUtils.pop(context, result: Const.ACTION_REFRESH);
                },
                child: Icon(
                  CupertinoIcons.clear_thick,
                  size: 20,
                  color: R.color.black,
                ),
              )),
          Expanded(
            child: WebView(
              initialUrl: widget.url,
              javascriptMode: JavascriptMode.unrestricted,
            ),
          ),
          SizedBox(
            height: 30,
          )
        ],
      ),
    );
  }
}
