import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final double? height;
  final double? width;
  final double? sizeCircle;

  CustomSwitch(
      {Key? key,
      required this.value,
      required this.onChanged,
      this.height,
      this.width,
      this.sizeCircle})
      : super(key: key);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  Animation? _circleAnimation;
  AnimationController? _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController!, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController!,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController!.isCompleted) {
              _animationController!.reverse();
            } else {
              _animationController!.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            width: widget.width ?? 28.h,
            height: widget.height ?? 16.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.h),
              color: _circleAnimation!.value == Alignment.centerLeft
                  ? R.color.grey
                  : R.color.green,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 2.h),
              child: Container(
                alignment:
                    widget.value ? Alignment.centerRight : Alignment.centerLeft,
                child: Container(
                  width: widget.sizeCircle ?? 20.h,
                  height: widget.sizeCircle ?? 20.h,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: R.color.white),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
