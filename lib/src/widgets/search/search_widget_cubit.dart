import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imi/src/data/database/sqflite/DBHelper.dart';
import 'package:imi/src/data/database/sqflite/search_suggest.dart';
import 'package:imi/src/widgets/search/search_widget_state.dart';

class SearchWidgetCubit extends Cubit<SearchWidgetState> {
  late DBHelper helper;
  final String type;

  List<SearchSuggest>? data;

  SearchWidgetCubit(this.type) : super(SearchWidgetSuccessState()) {
    this.helper = DBHelper();
    get5MatchedKws("");
  }

  void get5MatchedKws(String keyword) {
    helper.getSearchSuggests(keyword: keyword, type: type).then((value) {
      data = value;
      emit(SearchWidgetSuccessState());
    }).catchError((_) {
      emit(SearchWidgetFailureState());
    });
  }

  void saveKeyword(String keyword) {
    helper.save(SearchSuggest(keyWord: keyword, type: type));
  }

  void deleteKw({required int id, required String recentKeyword}) {
    helper.delete(id).then((_) {
      data?.removeWhere((element) => element.id == id);
      get5MatchedKws(recentKeyword);
    }).catchError((_) {
      emit(SearchWidgetFailureState());
    });
  }

  void deleteAllKws() {
    emit(SearchWidgetSuccessState());
    helper.deleteAll().then((_) {
      data = [];
    }).catchError((_) {
      emit(SearchWidgetFailureState());
    });
  }
}
