import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/search/search_widget_cubit.dart';
import 'package:imi/src/widgets/search/search_widget_state.dart';

import '../../../res/R.dart';
import '../../utils/utils.dart';

class SearchWidget extends StatefulWidget {
  final TextEditingController searchController;
  final void Function(String) onSubmit;
  final void Function(String) onChange;
  final String type;
  final bool autoFocus;
  final double? margin;
  final Color? fillColor;
  const SearchWidget(
      {Key? key,
      required this.searchController,
      required this.onSubmit,
      this.autoFocus = true,
      required this.type,
      required this.onChange,
      this.margin,
      this.fillColor})
      : super(key: key);

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  late final SearchWidgetCubit _cubit;
  late final FocusNode _focusNode;
  final _textFieldKey = UniqueKey();

  @override
  void initState() {
    super.initState();
    _cubit = SearchWidgetCubit(widget.type);
    _focusNode = FocusNode();
    if (widget.autoFocus) _focusNode.requestFocus();
  }

  @override
  void dispose() {
    _cubit.close();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchWidgetCubit, SearchWidgetState>(
      bloc: _cubit,
      builder: (context, state) => Column(
        children: [
          SizedBox(
            height: 8.h,
          ),
          SizedBox(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              child: TextField(
                key: _textFieldKey,
                textAlignVertical: TextAlignVertical.center,
                focusNode: _focusNode,
                inputFormatters: <TextInputFormatter>[
                  // FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z]")),
                ],
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 0),
                    hintStyle: Theme.of(context).textTheme.medium500.copyWith(
                        color: R.color.grey,
                        height: 20 / 14,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                    hintText: R.string.search.tr(),
                    hintMaxLines: 1,
                    fillColor: widget.fillColor ?? R.color.white,
                    filled: true,
                    prefixIcon: InkWell(
                        onTap: () {
                          _cubit.get5MatchedKws(
                              widget.searchController.text.trim());
                        },
                        child: Icon(CupertinoIcons.search,
                            size: 20.h, color: R.color.black)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(100),
                        borderSide: BorderSide(color: R.color.white)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(100),
                        borderSide: BorderSide(color: R.color.white))),
                controller: widget.searchController,
                style: Theme.of(context).textTheme.bodyRegular.copyWith(
                    color: R.color.black,
                    height: 20 / 14,
                    fontSize: 14,
                    fontWeight: FontWeight.w300),
                onChanged: (text) {
                  _cubit.get5MatchedKws(widget.searchController.text);
                  if (text.isEmpty) widget.onChange(text);
                },
                onSubmitted: (text) {
                  widget.onSubmit(text);
                  _cubit.saveKeyword(text);
                  Utils.hideKeyboard(context);
                },
              ),
            ),
          ),
          Visibility(
            visible: _focusNode.hasFocus,
            child: (_cubit.data ?? []).length == 0
                ? SizedBox()
                : Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.h),
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.h, vertical: 10.h),
                    decoration: BoxDecoration(
                      border: Border.all(color: R.color.grey300, width: 1),
                      borderRadius: BorderRadius.circular(8.h),
                      color: R.color.white,
                    ),
                    child: Column(
                      children: List.generate(_cubit.data!.length + 1, (index) {
                        if (index == 0) {
                          return Row(
                            children: [
                              Expanded(
                                child: Text(
                                  R.string.recent_search.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(fontSize: 12.sp),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  _cubit.deleteAllKws();
                                },
                                child: Text(
                                  R.string.delete_all.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .regular400
                                      .copyWith(
                                          fontSize: 12.sp,
                                          color: R.color.primaryColor),
                                ),
                              )
                            ],
                          );
                        } else {
                          final keyword = _cubit.data![index - 1];
                          return Row(
                            children: [
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    widget.searchController.text =
                                        keyword.keyWord ?? "";
                                    _focusNode.unfocus();
                                    widget.onSubmit(keyword.keyWord ?? "");
                                  },
                                  child: Text(
                                    keyword.keyWord ?? "",
                                    style:
                                        Theme.of(context).textTheme.regular400,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    _cubit.deleteKw(
                                        id: keyword.id ?? 0,
                                        recentKeyword:
                                            widget.searchController.text);
                                  },
                                  child: Icon(
                                    Icons.clear,
                                    size: 14.h,
                                  ))
                            ],
                          );
                        }
                      }),
                    ),
                  ),
          )
        ],
      ),
    );
  }
}
