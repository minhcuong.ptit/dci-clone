abstract class SearchWidgetState {}

class SearchWidgetFailureState extends SearchWidgetState {}

class SearchWidgetSuccessState extends SearchWidgetState {}
