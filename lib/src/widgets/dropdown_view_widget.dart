import 'package:easy_localization/src/public_ext.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DropdownViewWidget extends StatelessWidget {
  final List<String> listData;
  final Widget? prefix;
  final String? currentValue;
  final String? labelText;
  final Color? color;
  final String? hintText;
  final bool isRequired;
  final bool isEnabled;
  final VoidCallback? onTap;
  final String? errorText;
  final BoxBorder? borderSize;
  final double? height;

  const DropdownViewWidget(
      {Key? key,
      required this.listData,
      this.currentValue,
      this.labelText,
      this.color,
      this.hintText,
      this.isRequired = false,
      this.isEnabled = true,
      this.onTap,
      this.errorText,
      this.prefix, this.borderSize, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Visibility(
            visible: !Utils.isEmpty(labelText),
            child: Padding(
              padding: EdgeInsets.only(bottom: 10.h),
              child: RichText(
                text: TextSpan(
                    text: labelText ?? "",
                    style: TextStyle(
                      color: R.color.textGreyColor,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w400,
                    ),
                    children: isRequired
                        ? <TextSpan>[
                            TextSpan(
                              text: R.string.star.tr(),
                              style: TextStyle(
                                fontSize: 16.sp,
                                color: R.color.red,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ]
                        : []),
              ),
            )),
        Container(
            height: height ?? 60.h,
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 20.h),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.h),
                border: borderSize ?? Border.all(
                    color: !Utils.isEmpty(errorText)
                        ? R.color.red
                        : R.color.borderColor,
                    width: !Utils.isEmpty(errorText) ? 2 : 1)
            ),
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: isEnabled && onTap != null ? onTap : null,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: currentValue != null
                        ? Row(
                            children: [
                              prefix != null
                                  ? Padding(
                                      padding: EdgeInsets.only(right: 20.h),
                                      child: prefix)
                                  : Container(),
                              Expanded(
                                child: Text(currentValue ?? "",
                                    style: TextStyle(
                                        fontSize: 16.sp, color: R.color.black)),
                              ),
                            ],
                          )
                        : Text(hintText ?? "",
                            style: TextStyle(
                                fontSize: 16.sp, color: R.color.hintGray)),
                  ),
                  SizedBox(
                    height: 60.h,
                    child: Center(
                      child: Icon(
                        Icons.keyboard_arrow_down_outlined,
                        size: 24,
                        color: R.color.hintGray,
                      ),
                    ),
                  )
                ],
              ),
            )),
        Visibility(
          visible: !Utils.isEmpty(errorText),
          child: Container(
              padding: EdgeInsets.only(
                  top: 5.0, bottom: 5.0, left: 25.0, right: 25.0),
              child: Text(
                errorText ?? "",
                maxLines: 2,
                style: TextStyle(
                  fontSize: 15.sp,
                  color: R.color.red,
                ),
                textAlign: TextAlign.start,
                overflow: TextOverflow.ellipsis,
              )),
        )
        //SizedBox(height: 10)
      ],
    );
  }
}
