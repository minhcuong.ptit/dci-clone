import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/utils/navigation_utils.dart';

AppBar appBarMessages(
  BuildContext context, {
  VoidCallback? onSelectedIcon,
  VoidCallback? onCustomerCare,
  IconData? iconBack,
  double? size,
  Widget? rightWidget,
}) {
  return AppBar(
    backgroundColor: R.color.primaryColor,
    automaticallyImplyLeading: false,
    centerTitle: true,
    elevation: 0,
    leadingWidth: 60.w,
    flexibleSpace: Container(
      margin: EdgeInsets.only(top: 10.h),
      height: 60.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.contain,
              image: AssetImage(R.drawable.ic_logo_header))),
    ),
    leading: InkWell(
      highlightColor: R.color.primaryColor,
      onTap: onSelectedIcon,
      child: Icon(
        iconBack ?? Icons.home,
        size: size ?? 30.h,
      ),
    ),
    toolbarHeight: 80.h,
    actions: rightWidget == null
        ? null
        : [
            InkWell(
              highlightColor: R.color.primaryColor,
              onTap: () {
                NavigationUtils.navigatePage(
                    context, ChatPage(chatType: ChatType.support));
              },
              child: Row(
                children: [
                  Image.asset(
                    R.drawable.ic_headphones,
                    height: 22.h,
                    width: 22.h,
                  ),
                  SizedBox(width: 10.w)
                ],
              ),
            ),
            SizedBox(width: 8.h)
          ],
  );
}
