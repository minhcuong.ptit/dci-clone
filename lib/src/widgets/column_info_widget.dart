import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';

class ColumnInfoWidget extends StatelessWidget {
  final VoidCallback? voidCallback;
  final int? count;
  final String? text;
  ColumnInfoWidget({this.voidCallback, this.count, this.text});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: InkWell(
            onTap: voidCallback,
            child: Column(
              children: [
                Text(
                  count.toString(),
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 14.sp,
                    fontFamily: R.font.helvetica,
                    fontWeight: FontWeight.w400,
                    color: R.color.black,
                  ),
                ),
                SizedBox(height: 10.h),
                Text(
                  text ?? "",
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 10.sp,
                    fontFamily: R.font.helvetica,
                    fontWeight: FontWeight.w400,
                    color: R.color.shadesGray,
                  ),
                ),
              ],
            )));
  }
}
