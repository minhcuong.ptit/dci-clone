import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:imi/res/R.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class BackIconButton extends StatelessWidget {

  final VoidCallback? onBack;
  const BackIconButton({Key? key, this.onBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onBack ?? () => NavigationUtils.pop(context),
      child: Container(
        width: 40.h,
        height: 40.h,
        alignment: Alignment.center,
        // padding: EdgeInsets.all(14.h),
        decoration: BoxDecoration(boxShadow: <BoxShadow>[
          BoxShadow(
              color: R.color.shadowColor, blurRadius: 20.0, offset: Offset(5, 10))
        ], color: R.color.white, borderRadius: BorderRadius.circular(10.h)),
        child: Icon(
          Icons.chevron_left,
          size: 20.h,
          color: R.color.black,
        ),
      ),
    );
  }
}
