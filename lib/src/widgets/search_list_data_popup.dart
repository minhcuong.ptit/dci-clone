import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:imi/src/widgets/text_field_widget.dart';

typedef SearchListItemTitleBuilder<T> = String Function(T);

class SearchListDataPopup extends StatefulWidget {
  final List<String> listData;
  final int? defaultIndex;
  final String? hintText;
  final Function(String? keyword)? searchOnline;
  final Function(int index) onSelect;

  const SearchListDataPopup(
      {Key? key,
      required this.listData,
      this.defaultIndex,
      this.hintText,
      required this.onSelect,
      this.searchOnline})
      : super(key: key);

  @override
  State<SearchListDataPopup> createState() => SearchListDataPopupState();
}

class SearchListDataPopupState extends State<SearchListDataPopup> {
  List<String> listDataOriginal = [];
  List<String> listData = [];
  final TextEditingController controller = TextEditingController();
  int? selectedIndex;
  late ScrollController scrollController;

  @override
  void initState() {
    listDataOriginal = widget.listData;
    listData = widget.listData;
    selectedIndex = widget.defaultIndex;
    int currentIndex = widget.defaultIndex ?? 0;
    if (currentIndex < 0) {
      currentIndex = 0;
    }
    scrollController =
        ScrollController(initialScrollOffset: 75.h * currentIndex);
    super.initState();
  }

  void updateListData(List<String> listData) {
    this.listDataOriginal = listData;
    this.listData = listData;
    print(listData);
    setState(() {});
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 35.h),
          Padding(
            padding: EdgeInsets.all(15.h),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: TextFieldWidget(
                    autoFocus: false,
                    controller: controller,
                    hintText: widget.hintText ?? R.string.hint_search.tr(),
                    border: 30.h,
                    fillColor: R.color.grey100,
                    borderSize: BorderSide.none,
                    padding: EdgeInsets.symmetric(horizontal: 15.h),
                    suffixIcon: Icon(Icons.search,
                        size: 25.h, color: R.color.textGreyColor),
                    onChanged: (text) {
                      if (widget.searchOnline != null)
                        widget.searchOnline!(text?.trim());
                      else
                        searchKeyword(text?.trim());
                    },
                    onSubmitted: (text) {
                      searchKeyword(text?.trim());
                    },
                  ),
                ),
                SizedBox(
                  width: 15.h,
                ),
                InkWell(
                  onTap: () => NavigationUtils.pop(context),
                  child: Icon(
                    Icons.close,
                    size: 25.h,
                    color: R.color.primaryColor,
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 1,
            color: R.color.grey200,
          ),
          Expanded(
            child: Utils.isEmpty(listData) && !Utils.isEmpty(listDataOriginal)
                ? Center(child: Text(R.string.not_matching_result.tr()))
                : Scrollbar(
                    child: ListView.separated(
                      controller: scrollController,
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      itemCount: listData.length,
                      itemBuilder: (context, index) {
                        String item = listData[index];
                        return GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            int index = listDataOriginal.indexOf(item);
                            widget.onSelect(index);
                            selectedIndex = index;
                            NavigationUtils.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.all(20.h),
                            color: R.color.white,
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    item,
                                    style: TextStyle(
                                        color: R.color.black,
                                        fontSize: 17.sp,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                                SizedBox(width: 5.w),
                                Visibility(
                                    visible: selectedIndex != null &&
                                        listDataOriginal[selectedIndex!] ==
                                            listData[index],
                                    child: Icon(
                                      CupertinoIcons.checkmark_alt,
                                      size: 20.h,
                                    ))
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (context, index) => Container(
                        height: 1,
                        color: R.color.grey200,
                      ),
                    ),
                  ),
          ),
          Container(
            height: 1,
            color: R.color.grey200,
          ),
          SizedBox(
            height: 15.h,
          ),
        ],
      ),
    );
  }

  void searchKeyword(String? name) {
    if (Utils.isEmpty(name)) {
      listData = widget.listData;
    }
    listData = widget.listData
        .where(
          (e) => Utils.convertVNtoText(e).toLowerCase().contains(
                Utils.convertVNtoText(name!).toLowerCase(),
              ),
        )
        .toList();
    setState(() {});
  }
}
