import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class SimpleChoice {
  final String? icon;
  final String title;
  final bool? checked;
  final VoidCallback? onChosen;

  SimpleChoice(
      {this.icon,
      required this.title,
      required this.onChosen,
      this.checked = false});
}

class SimpleChoicePopup extends StatelessWidget {
  final List<SimpleChoice> choices;
  const SimpleChoicePopup({Key? key, required this.choices}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.white,
      child: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Container(
        padding: EdgeInsets.all(24.h),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 16.h),
                ...choices.map((e) => _buildRow(context, e)).toList(),
                Container(height: 1.h, color: R.color.lightestGray),
                SizedBox(height: 28.h),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRow(BuildContext context, SimpleChoice choice) {
    return InkWell(
        onTap: () {
          NavigationUtils.pop(context);
          if (choice.onChosen != null) {
            choice.onChosen!();
          }
        },
        child: Row(
          children: [
            if (choice.icon != null)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.h),
                child: Image.asset(
                  choice.icon!,
                  fit: BoxFit.fill,
                  width: 24.w,
                ),
              ),
            SizedBox(width: 16.w),
            Expanded(
              child: Text(choice.title,
                  style: Theme.of(context).textTheme.bodyBold),
            ),
            if (choice.checked != null && choice.checked!)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.h),
                child: Icon(
                  Icons.check,
                  size: 24.w,
                  color: R.color.black,
                ),
              ),
          ],
        ));
  }
}
