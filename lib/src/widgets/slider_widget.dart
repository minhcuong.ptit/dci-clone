import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/widgets/page_view_indicator/page_view_indicator.dart';

import '../../../../res/R.dart';

class SliderWidget extends StatefulWidget {
  final List<String?> images;
  final bool? autoPlay;
  const SliderWidget({Key? key, required this.images, this.autoPlay})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _SliderWidgetState();
  }
}

class _SliderWidgetState extends State<SliderWidget> {
  final _pageIndexNotifier = ValueNotifier<int>(0);
  late List<String?> images;
  PageController pageController = PageController(viewportFraction: 328 / 360);
  @override
  void initState() {
    images = widget.images;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.images.forEach((imageUrl) {
        precacheImage(NetworkImage(imageUrl ?? ''), context);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: FractionalOffset.bottomCenter,
      children: [
        (images.length == 1)
            ? ClipRRect(
                borderRadius: BorderRadius.circular(8.w),
                child: Container(
                  child: Center(
                      child: CachedNetworkImage(
                          imageUrl: images[0] ?? '',
                          fit: BoxFit.cover,
                          width: double.infinity)),
                ),
              )
            : CarouselSlider.builder(
                itemCount: images.length,
                options: CarouselOptions(
                  autoPlay: widget.autoPlay ?? false,
                  enlargeCenterPage: true,
                  viewportFraction: 1.0,
                  aspectRatio: 312 / 147,
                  initialPage: 0,
                  autoPlayInterval: Duration(seconds: 4),
                  autoPlayAnimationDuration: Duration(milliseconds: 400),
                  onPageChanged: (index, reason) {
                    _pageIndexNotifier.value = index;
                  },
                ),
                itemBuilder: (context, index, realIdx) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(8.w),
                    child: Container(
                      child: Center(
                          child: CachedNetworkImage(
                              imageUrl: images[index] ?? '',
                              fit: BoxFit.cover,
                              width: double.infinity)),
                    ),
                  );
                },
              ),
        images.length > 1 ? _pageViewIndicator() : Container(),
      ],
    );
  }

  @override
  void dispose() {
    _pageIndexNotifier.dispose();
    super.dispose();
  }

  PageViewIndicator _pageViewIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: _pageIndexNotifier,
      length: images.length,
      normalBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController!,
          curve: Curves.ease,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3.w),
          child: Container(
            width: 4.w,
            height: 4.w,
            color: R.color.indicator,
          ),
        ),
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController!,
          curve: Curves.ease,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(3.w),
          child: Container(
            width: 12.w,
            height: 4.w,
            color: R.color.blueFF,
          ),
        ),
      ),
    );
  }
}
