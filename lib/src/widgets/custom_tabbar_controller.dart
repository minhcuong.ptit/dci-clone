import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../res/R.dart';

class CustomTabBarController extends StatefulWidget {
  final ValueChanged<int> onTabChange;
  final TabController tabController;
  final List<TabBarItem> tabBarItems;
  final TabBarStyle? tabBarStyle;
  final ScrollPhysics? physics;

  const CustomTabBarController(
      {Key? key,
      required this.tabBarItems,
      required this.onTabChange,
      required this.tabController,
      this.tabBarStyle,
      this.physics})
      : super(key: key);

  @override
  State<CustomTabBarController> createState() => _CustomTabBarControllerState();
}

class _CustomTabBarControllerState extends State<CustomTabBarController>
    with TickerProviderStateMixin {
  final TAB_HEIGHT = 32.h;
  final TAB_WIDTH = 40.w;
  final TAB_INDICATOR_HEIGHT = 2.h;
  final TAB_INDICATOR_PADDING = 5.w;

  @override
  void initState() {
    widget.tabController.addListener(() {
      final selectedTabItem = widget.tabBarItems[widget.tabController.index];
      if (selectedTabItem.disable) {
        widget.tabController.index = widget.tabController.previousIndex;
      } else {
        widget.onTabChange(widget.tabController.index);
      }
      if (mounted) {
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TabBar(
      controller: widget.tabController,
      isScrollable: false,
      indicatorColor: R.color.primaryColor,
      indicatorSize: TabBarIndicatorSize.label,
      physics: widget.physics,
      // indicatorWeight: TAB_INDICATOR_HEIGHT,
      indicator: UnderlineTabIndicator(
        borderSide:
            BorderSide(width: TAB_INDICATOR_HEIGHT, color: R.color.primaryColor),
      ),
      labelPadding: EdgeInsets.only(bottom: 10.h),
      tabs: _buildTabItems(),
    );
  }

  _buildTabItems() {
    final List<Widget> listTabItems = [];
    for (var i = 0; i < widget.tabBarItems.length; i++) {
      listTabItems.add(_buildTabItem(i, widget.tabBarItems[i].title));
    }
    return listTabItems;
  }

  _buildTabItem(int index, String label) {
    return Tab(
      height: widget.tabBarStyle?.tabBarHeight ?? TAB_HEIGHT,
      child: Text(
        label,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.buttonSmall.copyWith(
            color: index == widget.tabController.index
                ? R.color.primaryColor
                : R.color.shadesGray),
      ),
    );
  }
}

class TabBarItem {
  final String title;
  final bool disable;

  TabBarItem({required this.title, this.disable = false});
}

class TabBarStyle {
  final double? tabBarHeight;
  final double? tabBarPadding;

  TabBarStyle({
    this.tabBarHeight,
    this.tabBarPadding,
  });
}
