import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';

class BottomConfirmDialog extends StatelessWidget {
  final String title;
  final String description;
  final Function(bool) onConfirm;
  final String confirmString;
  final String rejectString;

  const BottomConfirmDialog(
      {Key? key,
      required this.title,
      required this.description,
      required this.confirmString,
      required this.rejectString,
      required this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double buttonWidth = (MediaQuery.of(context).size.width - 48.h - 16.w) / 2;
    return Padding(
      padding: EdgeInsets.all(24.h),
      child: SafeArea(
        child: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16.h,
              ),
              Text(
                this.title,
                style: Theme.of(context)
                    .textTheme
                    .bold700
                    .copyWith(fontSize: 16.sp, height: 24.h / 16.sp),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                this.description,
                style: Theme.of(context)
                    .textTheme
                    .regular400
                    .copyWith(fontSize: 11.sp, height: 20.h / 11.sp),
              ),
              SizedBox(
                height: 32.h,
              ),
              Row(
                children: [
                  ButtonWidget(
                    title: this.rejectString,
                    onPressed: () {
                      this.onConfirm(false);
                    },
                    height: 32.h,
                    textSize: 12.sp,
                    backgroundColor: R.color.white,
                    textColor: R.color.black,
                    borderColor: R.color.black,
                    width: buttonWidth,
                  ),
                  SizedBox(
                    width: 16.w,
                  ),
                  ButtonWidget(
                    title: this.confirmString,
                    onPressed: () {
                      this.onConfirm(true);
                    },
                    height: 32.h,
                    width: buttonWidth,
                    textSize: 12.sp,
                    backgroundColor: R.color.blue,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
