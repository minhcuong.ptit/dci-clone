import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/post/post_checkin/post_checkin.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../page/post/post.dart';
import '../utils/const.dart';
import '../utils/enum.dart';

class CreateNewPostPopup extends StatelessWidget {
  final int? targetCommunityId;

  const CreateNewPostPopup({Key? key, this.targetCommunityId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: R.color.white,
      child: buildPage(context),
    );
  }

  Widget buildPage(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Container(
        padding: EdgeInsets.all(24.h),
        child: Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 16.h),
                Text(
                  R.string.post_create_new.tr(),
                  style: Theme.of(context).textTheme.headline3,
                ),
                SizedBox(height: 8.h),
                Text(
                  R.string.post_create_new_description.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .bodySmallText
                      .copyWith(color: R.color.grey),
                ),
                SizedBox(height: 8.h),
                Container(height: 1.h, color: R.color.lightestGray),
                _buildRow(context, PostType.MEDIA),
                Container(height: 1.h, color: R.color.lightestGray),
                _buildRow(context, PostType.FREETEXT),
                Container(height: 1.h, color: R.color.lightestGray),
                _buildRow(context, PostType.LINK),
                SizedBox(height: 28.h),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRow(BuildContext context, PostType type) {
    return InkWell(
        onTap: () {
          NavigationUtils.pop(context);
          NavigationUtils.rootNavigatePage(context,
                  PostPage(type: type, targetCommunityId: targetCommunityId))
              .then((value) => Const.ACTION_REFRESH);
        },
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 24.h),
              child: Image.asset(
                type.icon,
                fit: BoxFit.fill,
                width: 24.w,
              ),
            ),
            SizedBox(width: 16.w),
            Expanded(
              child:
                  Text(type.title, style: Theme.of(context).textTheme.bodyBold),
            ),
          ],
        ));
  }
}
