import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/data/network/response/follow_response.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';

import 'avatar_widget.dart';

enum UserListType { follower, following, like }

class UserListWidget extends StatelessWidget {
  late final bool isCurrentUser;
  late final UserListType userListType;
  final FollowDataV2 data;
  final VoidCallback followCallback;
  final VoidCallback profileCallback;
  final bool checkStudyGroup;
  final bool checkMember;

  UserListWidget(
      {Key? key,
      required this.data,
      required this.followCallback,
      required this.profileCallback,
      this.isCurrentUser = false,
      this.userListType = UserListType.follower,
      required this.checkStudyGroup,
      required this.checkMember})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: profileCallback,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 12.h),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AvatarWidget(
              avatar: data.avatarUrl,
              size: 40.h,
            ),
            SizedBox(
              width: 8.w,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    data.name ?? "",
                    style: Theme.of(context)
                        .textTheme
                        .bold700
                        .copyWith(
                        fontSize: 11.sp,
                        height: 20.h / 11.sp),overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                  _description(context)
                ],
              ),
            ),
            if (checkStudyGroup == true && checkMember == true) ...[
              _actionButton(context)
            ],
            if (checkStudyGroup == false && checkMember == true) ...[
              _actionButton(context)
            ],
            if (checkStudyGroup == false && checkMember == false) ...[
              _actionButton(context)
            ]
          ],
        ),
      ),
    );
  }

  Widget _actionButton(BuildContext context) {
    if (data.id == appPreferences.getInt(Const.COMMUNITY_ID)) {
      return const SizedBox.shrink();
    }
    return ButtonWidget(
      title: (checkMember == true && checkStudyGroup == false ||
              checkStudyGroup == false && checkMember == false)
          ? ((data.userView?.isFollowing ?? false)
              ? R.string.following.tr()
              : R.string.follow.tr())
          : R.string.following.tr(),
      onPressed: (checkMember == true && checkStudyGroup == false ||
              checkStudyGroup == false && checkMember == false)
          ? followCallback
          : () {},
      uppercaseTitle: false,
      textSize: 12.sp,
      radius: 8.r,
      textColor: (checkMember == true && checkStudyGroup == false ||
          checkStudyGroup == false && checkMember == false)
          ? R.color.orange
          : R.color.gray,
      backgroundColor: R.color.white,
      borderColor: (checkMember == true && checkStudyGroup == false ||
          checkStudyGroup == false && checkMember == false)
          ? R.color.orange
          : R.color.gray,
      textStyle: Theme.of(context).textTheme.medium500.copyWith(
          color: (checkMember == true && checkStudyGroup == false ||
              checkStudyGroup == false && checkMember == false)
              ? R.color.orange
              : R.color.gray,
          height: 1.2),
      height: 33.h,
      width: 93.w,
    );
  }

  Widget _description(context) {
    if (userListType == UserListType.like) return const SizedBox.shrink();
    String res = "";
    if (data.type == Const.INDIVIDUAL)
      res = R.string.individual.tr();
    else
      res = R.string.club.tr();
    return Text(res,
        style:
            Theme.of(context).textTheme.regular400.copyWith(fontSize: 10.sp));
  }
}
