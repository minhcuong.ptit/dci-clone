import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/navigation_utils.dart';

class FullscreenImagePage extends StatefulWidget {
  final String tag;
  final File? dataFile;
  final String? dataUrl;

  const FullscreenImagePage(
      {Key? key, required this.tag, this.dataFile, this.dataUrl})
      : super(key: key);

  @override
  _FullscreenImagePageState createState() => _FullscreenImagePageState();
}

class _FullscreenImagePageState extends State<FullscreenImagePage> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  void dispose() {
    //SystemChrome.restoreSystemUIOverlays();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: R.color.black,
      body: Stack(
        children: <Widget>[
          widget.dataFile != null
              ? GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: Center(
                      child: Hero(
                    tag: widget.tag,
                    child: widget.dataFile == null
                        ? Container()
                        : Image.file(
                            widget.dataFile!,
                            fit: BoxFit.cover,
                          ),
                  )),
                  onTap: () {
                    NavigationUtils.pop(context);
                  },
                )
              : GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: Center(
                      child: Hero(
                    tag: widget.tag,
                    child: widget.dataUrl == null
                        ? Container()
                        : CachedNetworkImage(
                            imageUrl: widget.dataUrl ?? "",
                            fit: BoxFit.cover,
                          ),
                  )),
                  onTap: () {
                    NavigationUtils.pop(context);
                  },
                ),
          Positioned(
              left: 10,
              top: 20,
              child: InkWell(
                onTap: () => NavigationUtils.pop(context),
                child: Container(
                  width: 40.h,
                  height: 40.h,
                  alignment: Alignment.center,
                  // padding: EdgeInsets.all(14.h),
                  decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: R.color.shadowColor,
                            blurRadius: 20.0,
                            offset: Offset(5, 10))
                      ],
                      color: R.color.white,
                      borderRadius: BorderRadius.circular(10.h)),
                  child: Icon(
                    Icons.close,
                    size: 20.h,
                    color: R.color.black,
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
