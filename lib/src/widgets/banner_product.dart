import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../res/R.dart';
import '../utils/utils.dart';

class BannerProduct extends StatelessWidget {
  final String imageUrl;
  final bool isHot;
  final bool hasStartDate;
  final int startDate;

  const BannerProduct(
      {Key? key,
      required this.imageUrl,
      required this.isHot,
      required this.hasStartDate,
      required this.startDate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.h),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.h),
            child: CachedNetworkImage(
              fit: BoxFit.fill,
              height: 180.h,
              width: context.width,
              imageUrl: imageUrl,
              placeholder: (_, __) {
                return Text(
                  R.string.loading.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .regular400
                      .copyWith(fontSize: 14.sp),
                );
              },
            ),
          ),
        ),
        Visibility(
          visible: isHot,
          child: Positioned(
              top: 10.h,
              left: -4.h,
              child: Image.asset(R.drawable.ic_hot, height: 36.h, width: 75.w)),
        ),
        !hasStartDate
            ? SizedBox()
            : Positioned(
                bottom: 12.h,
                left: 32.h,
                right: 32.h,
                child: Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 5.h, horizontal: 12.h),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.h),
                      color: R.color.white),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.clock,
                        size: 20.h,
                        color: R.color.green,
                      ),
                      SizedBox(width: 4.h),
                      Expanded(
                        child: Padding(
                          padding:const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 3),
                          child: Text(
                            R.string.opening_ceremony_on.tr().toUpperCase() +
                                " " +
                                Utils.getStartDate(startDate),
                            style: Theme.of(context).textTheme.medium500,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
      ],
    );
  }
}
