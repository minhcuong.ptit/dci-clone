import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/utils.dart';

class DropdownColumnWidget extends StatefulWidget {
  final List<String> listData;
  final Widget? prefix;
  final String? currentValue;
  final ValueChanged<String?> selectedValue;
  final String? labelText;
  final Color? color;
  final String? hintText;
  final bool isRequired;
  final bool isEnabled;
  final VoidCallback? onTap;
  final String? errorText;
  final bool? showBorder;

  const DropdownColumnWidget({
    Key? key,
    required this.listData,
    required this.currentValue,
    required this.selectedValue,
    this.labelText,
    this.color,
    this.hintText,
    this.isRequired = false,
    this.isEnabled = true,
    this.onTap,
    this.errorText,
    this.prefix,
    this.showBorder,
  }) : super(key: key);

  @override
  _DropdownColumnWidgetState createState() => _DropdownColumnWidgetState();
}

class _DropdownColumnWidgetState extends State<DropdownColumnWidget> {
  List<String> list = [];

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() {
    list = widget.listData;
    // String firstElement = widget.hintText ?? R.string.choose.tr();
    // if (!Utils.isEmpty(widget.listData)) {
    //   try {
    //     if (list.last != widget.listData.last) _selectedPos ?? null;
    //   } catch (e) {
    //     logger.e(e.toString());
    //   }
    //   list = widget.listData;
    //   if (list.contains(firstElement)) {
    //     if (_selectedPos == null && widget.currentValue != null)
    //       _selectedPos = list.indexOf(widget.currentValue!);
    //     return;
    //   }
    //   list.insert(0, firstElement);
    //
    //   if (_selectedPos == null && widget.currentValue != null)
    //     _selectedPos = list.indexOf(widget.currentValue!);
    //   if (_selectedPos == null || _selectedPos! < 0) {
    //     _selectedPos = 0;
    //   }
    //   if (!Utils.isEmpty(list) && widget.selectedValue != null) {
    //     String? selectedValue = _selectedPos == 0 ? null : list[_selectedPos!];
    //     if (selectedValue != null) widget.selectedValue(selectedValue);
    //   }
    // } else {
    //   list = []..add(firstElement);
    // }
  }

  @override
  Widget build(BuildContext context) {
    initData();
    int sizeList = list.length;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Visibility(
          visible: !Utils.isEmpty(widget.labelText),
          child: RichText(
            text: TextSpan(
                text: widget.labelText ?? "",
                style: Theme.of(context)
                    .textTheme
                    .labelSmallText
                    .copyWith(color: R.color.shadesGray),
                children: widget.isRequired
                    ? <TextSpan>[
                        TextSpan(
                          text: R.string.star.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .labelSmallText
                              .copyWith(color: R.color.red),
                        ),
                      ]
                    : []),
          ),
        ),
        Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 0, right: 0, top: 0.h, bottom: 0.h),
            decoration: widget.showBorder == false
                ? BoxDecoration()
                : BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: !Utils.isEmpty(widget.errorText)
                                ? R.color.red
                                : R.color.borderColor,
                            width: !Utils.isEmpty(widget.errorText) ? 2 : 1))),
            child: sizeList == 0
                ? Text("",
                    style: Theme.of(context).textTheme.bodyRegular.copyWith(
                          color: widget.isEnabled
                              ? (widget.color ?? R.color.black)
                              : R.color.disableGray,
                        ))
                : (sizeList == 1 || widget.onTap != null
                    ? GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: widget.isEnabled ? widget.onTap : null,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Expanded(
                              child: widget.currentValue != null
                                  ? Text(widget.currentValue ?? list[0],
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyRegular
                                          .copyWith(
                                            color: widget.isEnabled
                                                ? R.color.black
                                                : R.color.disableGray,
                                          ))
                                  : Text(widget.hintText ?? "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyRegular
                                          .copyWith(
                                            color: widget.isEnabled
                                                ? R.color.hintGray
                                                : R.color.disableGray,
                                          )),
                            ),
                            Container(
                              height: 35.h,
                              padding: EdgeInsets.only(right: 10),
                              child: Center(
                                child: Icon(
                                  Icons.arrow_drop_down,
                                  size: 24,
                                  color: R.color.black,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : DropdownButtonHideUnderline(
                        child: DropdownButton<String?>(
                            // isDense: true,
                            isExpanded: true,
                            style: Theme.of(context)
                                .textTheme
                                .bodyRegular
                                .copyWith(
                                  color: widget.isEnabled
                                      ? (widget.color ?? R.color.black)
                                      : R.color.disableGray,
                                ),
                            hint: Text(
                              widget.hintText ?? "",
                              style: TextStyle(
                                fontSize: 16.sp,
                                color: R.color.hintGray,
                              ),
                            ),
                            value: widget.currentValue,
                            items: list
                                .map((item) => DropdownMenuItem<String>(
                                      child: Text(
                                        item,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyRegular
                                            .copyWith(
                                              color: widget.isEnabled
                                                  ? (widget.color ??
                                                      R.color.black)
                                                  : R.color.disableGray,
                                            ),
                                      ),
                                      value: item,
                                    ))
                                .toList(),
                            icon: Container(
                              height: 35.h,
                              padding: EdgeInsets.only(right: 10),
                              child: Center(
                                child: Icon(
                                  Icons.arrow_drop_down,
                                  size: 24,
                                  color: R.color.black,
                                ),
                              ),
                            ),
                            onChanged: (value) {
                              setState(() {
                                widget.selectedValue(value);
                              });
                            }),
                      ))),
        Visibility(
          visible: !Utils.isEmpty(widget.errorText),
          child: Container(
              padding:
                  EdgeInsets.only(top: 5.0, bottom: 5.0, left: 0, right: 25.0),
              child: Text(
                widget.errorText ?? "",
                maxLines: 2,
                style: Theme.of(context).textTheme.bodySmallText.copyWith(
                      color: R.color.red,
                    ),
                textAlign: TextAlign.start,
                overflow: TextOverflow.ellipsis,
              )),
        )
        //SizedBox(height: 10)
      ],
    );
  }
}
