import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'expandable_text.dart';

class CustomExpandableText extends StatelessWidget {
  final int maxLines;
  final String text;
  final String? prefixText;
  final TextStyle? prefixStyle;
  final TextAlign? textAlign;
  final TextStyle? textStyle;
  final TextStyle? linkStyle;

  const CustomExpandableText({Key? key, this.maxLines = 2, required this.text, this.textStyle, this.linkStyle, this.textAlign, this.prefixText, this.prefixStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpandableText(
      text,
      textAlign: textAlign,
      expandText: R.string.more.tr(),
      collapseText: R.string.less.tr(),
      maxLines: maxLines,
      style: textStyle ?? Theme.of(context).textTheme.bodySmallText,
      linkStyle: linkStyle ?? Theme.of(context).textTheme.bodySmallText.copyWith(color: R.color.primaryColor),
      prefixText: prefixText,
      prefixStyle: prefixStyle ?? linkStyle ?? Theme.of(context).textTheme.bodySmallText.copyWith(color: R.color.primaryColor, fontWeight: FontWeight.w500),
    );
  }
}
