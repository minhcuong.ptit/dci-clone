import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/widgets/button_widget.dart';

class ConfirmPopup extends StatelessWidget {
  final VoidCallback? onConfirmYes;
  final VoidCallback? onConfirmNo;
  final String? description;
  const ConfirmPopup(
      {Key? key, this.onConfirmYes, this.onConfirmNo, this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            R.string.confirm.tr(),
            style:
                Theme.of(context).textTheme.bold700.copyWith(fontSize: 16.sp),
          ),
          SizedBox(height: 8.h),
          Text(
            description ?? "",
            style: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(fontSize: 11.sp, color: R.color.grey),
          ),
          SizedBox(height: 8.h),
          ButtonWidget(
            title: R.string.yes.tr(),
            onPressed: onConfirmYes,
            padding: EdgeInsets.symmetric(vertical: 9.h),
            backgroundColor: R.color.orange,
            textSize: 14.sp,
            textColor: R.color.white,
          ),
          SizedBox(height: 8.h),
          ButtonWidget(
            title: R.string.no.tr(),
            onPressed: onConfirmNo,
            padding: EdgeInsets.symmetric(vertical: 9.h),
            backgroundColor: R.color.white,
            borderColor: R.color.primaryColor,
            textSize: 14.sp,
            textColor: R.color.primaryColor,
          ),
        ],
      ),
    );
  }
}
