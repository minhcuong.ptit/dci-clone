import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/widgets/rich_input.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../../../../../res/R.dart';
import '../utils/navigation_utils.dart';
import '../utils/utils.dart';

class ChatInputField extends StatefulWidget {
  final Function(String) sendText;
  final Function(XFile) sendFile;
  final Function(XFile) sendImage;
  final RichInputController chatInputController;
  const ChatInputField({
    Key? key,
    required this.chatInputController,
    required this.sendText,
    required this.sendFile,
    required this.sendImage,
  }) : super(key: key);

  @override
  State<ChatInputField> createState() => _ChatInputFieldState();
}

class _ChatInputFieldState extends State<ChatInputField> {
  FocusNode _focusNode = FocusNode();
  ValueNotifier<bool> isText = ValueNotifier(false);
  ValueNotifier<bool> isPickFile = ValueNotifier(false);
  ValueNotifier<bool> checkInputMess = ValueNotifier(false);
  XFile? fileName;
  XFile? image;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          // horizontal: kDefaultPadding,
          // vertical: kDefaultPadding / 2,
          ),
      decoration: BoxDecoration(
        color: R.color.white,
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 4),
            blurRadius: 32,
            color: Color(0xFF087949).withOpacity(0.08),
          ),
        ],
      ),
      child: SafeArea(
        child: ValueListenableBuilder(
          valueListenable: checkInputMess,
          builder: (context, value, _) => Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20 * 0.75,
            ),
            // decoration: BoxDecoration(
            //   color: kPrimaryColor.withOpacity(0.05),
            // ),
            child: Row(
              children: [
                SizedBox(width: 5.w),
                Expanded(
                  child: Container(
                    constraints: BoxConstraints(maxHeight: 175.h),
                    child: isPickFile.value
                        ? Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 5),
                                  child: ConstrainedBox(
                                      constraints: BoxConstraints(
                                          maxWidth: 200.w, maxHeight: 60.h),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 12),
                                        child: Text(
                                          fileName?.path ??
                                              DateTime.now().toString(),
                                          style: TextStyle(color: Colors.blue),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      )),
                                ),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        checkInputMess.value = false;
                                        fileName = null;
                                        isPickFile.value = false;
                                        widget.chatInputController.text = '';
                                        return;
                                      });
                                    },
                                    child: Image.asset(
                                      R.drawable.ic_close,
                                      width: 24.w,
                                    ))
                              ],
                            ),
                          )
                        : image != null
                            ? Stack(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Image.file(
                                      File(image!.path),
                                      width: 100,
                                      height: 100,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Positioned(
                                      top: 5,
                                      left: 73,
                                      child: InkWell(
                                          onTap: () {
                                            checkInputMess.value = false;
                                            this.image = null;
                                            isPickFile.value = false;
                                            widget.chatInputController.text =
                                                '';
                                            return;
                                          },
                                          child: Image.asset(
                                            R.drawable.ic_close_primary,
                                            width: 24.w,
                                          )))
                                ],
                              )
                            : RawKeyboardListener(
                                focusNode: FocusNode(),
                                onKey: (event) {
                                  if (event
                                      .isKeyPressed(LogicalKeyboardKey.enter)) {
                                    int cursorPos = widget.chatInputController
                                        .selection.base.offset;
                                    widget.chatInputController.selection =
                                        TextSelection.fromPosition(TextPosition(
                                            offset: cursorPos + 1));
                                  }
                                },
                                child: TextField(
                                  autofocus: false,
                                  enabled: true,
                                  focusNode: _focusNode,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1000)
                                    //or any number you want
                                  ],
                                  textInputAction: TextInputAction.newline,
                                  controller: widget.chatInputController,
                                  cursorColor: R.color.gray,
                                  keyboardType: TextInputType.multiline,
                                  maxLines: null,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 14, vertical: 8),
                                    hintText: R.string.enter_message.tr(),
                                    hintStyle: Theme.of(context)
                                        .textTheme
                                        .bodyRegular
                                        .copyWith(color: R.color.shadesGray),
                                    disabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1, color: R.color.borderColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1,
                                            color: R.color.borderColor)),
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1,
                                            color: R.color.borderColor)),
                                    border: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 1,
                                            color: R.color.borderColor)),
                                  ),
                                  style:
                                      Theme.of(context).textTheme.bodyRegular,
                                  onSubmitted: (text) {
                                    Utils.hideKeyboard(context);
                                  },
                                  onChanged: (text) {
                                    if (widget.chatInputController.text != '') {
                                      checkInputMess.value = true;
                                      if (widget
                                              .chatInputController.text.length >
                                          1000) {
                                        Fluttertoast.showToast(
                                            msg: R.string
                                                .cannot_enter_1000_character
                                                .tr());
                                      }
                                      return;
                                    } else {
                                      checkInputMess.value = false;
                                    }
                                  },
                                ),
                              ),
                  ),
                ),
                checkInputMess.value
                    ? InkWell(
                        onTap: () {
                          if (widget.chatInputController.text != '' &&
                              fileName == null) {
                            widget.sendText(widget.chatInputController.text);
                          }
                          if (image != null) {
                            widget.sendImage(XFile(image!.path));
                            image = null;
                          }
                          if (fileName != null) {
                            widget.sendFile(XFile(fileName!.path));
                            fileName = null;
                            isPickFile.value = false;
                          }
                          checkInputMess.value = false;
                          widget.chatInputController.clear();
                        },
                        child: Image.asset(
                          R.drawable.ic_send,
                          height: 24.h,
                          color: R.color.primaryColor,
                        ),
                      )
                    : const SizedBox.shrink(),
                checkInputMess.value
                    ? const SizedBox.shrink()
                    : InkWell(
                        onTap: () {
                          Platform.isIOS
                              ? openPostPopup()
                              :Platform.isAndroid? pickFileAndroid():null;
                        },
                        child: Image.asset(
                          R.drawable.ic_attachment,
                          width: 24.w,
                        )),
                checkInputMess.value ? const SizedBox.shrink() : SizedBox(width: 5.w),
                checkInputMess.value
                    ? const SizedBox.shrink()
                    : InkWell(
                        onTap: () async {
                          final image = await ImagePicker()
                              .pickImage(source: ImageSource.gallery);
                          if (image == null) return;
                          var size = await image.length();
                          if (size > 5000000) {
                            Fluttertoast.showToast(
                                msg: R.string.upload_image_invalid.tr());
                            return;
                          }
                          this.image = image;
                          this.checkInputMess.value = true;
                        },
                        child: Image.asset(
                          R.drawable.ic_imageIcon,
                          width: 24.w,
                        ))
              ],
            ),
          ),
        ),
      ),
    );
  }
void pickFileAndroid()async{
  final result = await FilePicker.platform
      .pickFiles(
      type: FileType.custom,
      allowedExtensions: [
        'mp4',
        'xlsx',
        'pdf',
        'txt',
        'docs',
        'docx'
      ]);
  if (result == null) return;
  final file = result.files.first;
  if (file.size > 5000000) {
    Fluttertoast.showToast(
        msg: R.string.upload_file_invalid.tr());
    return;
  }
  fileName = XFile(file.path ?? "");
  widget.chatInputController.text = file.name;
  if (file.name != '') {
    checkInputMess.value = true;
    isPickFile.value = true;
  }
}
  void openPostPopup() {
    showBarModalBottomSheet(
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              // color: R.color.white,
              padding: EdgeInsets.all(20.h),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  GestureDetector(
                    onTap: () async {
                      final result = await FilePicker.platform.pickFiles(
                          type: FileType.custom,
                          allowedExtensions: [
                            'mov',
                            'mp4',
                            'xlsx',
                            'pdf',
                            'txt',
                            'docs',
                            'docx'
                          ]);
                      if (result == null) return;
                      final file = result.files.first;
                      if (file.size > 5000000) {
                        Fluttertoast.showToast(
                            msg: R.string.upload_file_invalid.tr());
                        return;
                      }
                      fileName = XFile(file.path ?? "");
                      widget.chatInputController.text = file.name;
                      if (file.name != '') {
                        checkInputMess.value = true;
                        isPickFile.value = true;
                      }
                      NavigationUtils.pop(context);
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          CupertinoIcons.folder,
                          color: R.color.black,
                        ),
                        SizedBox(width: 8.w),
                        Expanded(
                          child: Text(
                            R.string.open_folder.tr(),
                            style: TextStyle(
                                color: R.color.black,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        SizedBox(width: 13.h),
                      ],
                    ),
                  ),
                  SizedBox(height: 15.h),
                  Container(height: 1, color: R.color.grey200),
                  SizedBox(height: 15.h),
                  GestureDetector(
                      onTap: () async{
                        final result = await ImagePicker()
                              .pickVideo(source: ImageSource.gallery);
                             if (result == null) return;
                      final file = result.path;
                      if (await result.length() > 5000000) {
                        Fluttertoast.showToast(
                            msg: R.string.upload_file_invalid.tr());
                        return;
                      }
                      fileName = XFile(file);
                      widget.chatInputController.text = file;
                      if (file != '') {
                        checkInputMess.value = true;
                        isPickFile.value = true;
                      }
                        NavigationUtils.pop(context);
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            CupertinoIcons.camera,
                            color: R.color.black,
                          ),
                          SizedBox(width: 8.w),
                          Expanded(
                            child: Text(
                              R.string.open_gallery.tr(),
                              style: TextStyle(
                                  color: R.color.black,
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          SizedBox(width: 13.h),
                        ],
                      )),
                  SizedBox(height: 15.h),
                ],
              ),
            ),
          );
        });
  }
}
