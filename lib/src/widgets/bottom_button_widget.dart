import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/res/R.dart';

import 'button_widget.dart';

class BottomButtonWidget extends StatelessWidget {

  final String text;
  final VoidCallback? onTap;
  const BottomButtonWidget({Key? key, required this.text, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        padding: EdgeInsets.all(24.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.h),
            topRight: Radius.circular(10.h),
          ),
          color: R.color.white,
          boxShadow: [
            BoxShadow(
              color: R.color.borderColor,
              spreadRadius: 0,
              blurRadius: 3,
              offset: Offset(0, -3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: ButtonWidget(
            title: text,
            backgroundColor: onTap == null ? R.color.gray : R.color.primaryColor,
            onPressed: onTap,
          ),
        ));
  }
}
