import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../res/R.dart';
import '../data/network/response/category_data.dart';

class CategoryWidget extends StatelessWidget {
  final CategoryData data;
  final bool isSelected;
  final bool? isLoadMore;
  final VoidCallback? onTap;
  final BoxShadow? shadow;
  const CategoryWidget(
      {Key? key,
      required this.data,
      this.isSelected = false,
      this.onTap,
      this.isLoadMore,
      this.shadow})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color bgColor = R.color.white;
    if (isLoadMore ?? false) {
      bgColor = R.color.white;
    } else {
      bgColor = isSelected ? R.color.selectedCategory : R.color.white;
    }

    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 6.h, vertical: 6.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24.h),
          boxShadow: [
            shadow ??
                BoxShadow(
                    color: R.color.shadowColor,
                    spreadRadius: 0,
                    blurRadius: 0.h,
                    offset: Offset(0, 0))
          ],
          color: bgColor,
        ),
        child: IntrinsicWidth(
          child: Row(
            children: [
              (isLoadMore ?? false)
                  ? SizedBox.shrink()
                  : CachedNetworkImage(
                      height: 20.h,
                      width: 20.w,
                      imageUrl: data.emoji ?? '',
                      fit: BoxFit.cover,
                    ),
              SizedBox(width: 4.w),
              Expanded(
                child: Text(
                  "${data.name}",
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.bodySmallText.copyWith(
                      color: (isLoadMore ?? false)
                          ? R.color.readMore
                          : R.color.textFieldTitle,fontSize: 11.sp),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
