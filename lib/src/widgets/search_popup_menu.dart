import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:imi/src/extension/extentions.dart';
import 'package:imi/src/utils/custom_theme.dart';

import '../../res/R.dart';


class SearchPopupMenu extends StatelessWidget {
  final TextEditingController textEditingController;
  final Function()? search;
  const SearchPopupMenu({Key? key, required this.textEditingController,required this.search}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: context.width * 0.7,
        child: TextField(
          maxLines: 1,
          controller:
          textEditingController,
          style: Theme.of(context)
              .textTheme
              .regular400,
          decoration: InputDecoration(
            hintText: R.string.search.tr(),
            hintStyle: Theme.of(context)
                .textTheme
                .regular400
                .copyWith(
                color: R.color.disableGray),
            border: InputBorder.none,
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  color: R.color.disableGray),
              borderRadius:
              BorderRadius.circular(10),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  color: R.color.disableGray),
              borderRadius:
              BorderRadius.circular(10),
            ),
            suffixIcon: IconButton(
              onPressed: search,
              icon: Icon(Icons.search),
            ),
          ),
        ),
      ),
    );
  }
}
