import 'dart:convert';

import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:imi/src/data/network/response/user_data.dart';
import 'package:imi/src/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../network/response/community_data.dart';
import '../network/response/login_response.dart';
import '../network/response/person_profile.dart';

class AppPreferences {
  AppPreferences._privateConstructor();

  static final AppPreferences _instance = AppPreferences._privateConstructor();

  static final String _KEY_PERSONAL_PROFILE = "_KEY_PERSONAL_PROFILE";
  static final String _KEY_LOGIN_RESPONSE = "_KEY_LOGIN_RESPONSE";

  factory AppPreferences() {
    SharedPreferences.getInstance().then((value) {
      _instance._preference = value;
    });
    return _instance;
  }

  SharedPreferences? _preference;
  PersonProfile? _personProfile;
  LoginResponse? _loginResponse;
  bool? isUpdatedProfile;
  bool get isLoggedIn => loginResponse != null;
  bool get hasDCILevel => appPreferences.getBool(Const.DCI_LEVEL) ?? false;

  PersonProfile? get personProfile {
    if (_personProfile == null) {
      String? raw = _preference?.getString(_KEY_PERSONAL_PROFILE);
      if (raw != null) {
        dynamic rawJson = json.decode(raw);
        if (rawJson != null) {
          _personProfile = PersonProfile.fromJson(rawJson);
        }
      }
    }
    return _personProfile;
  }

  void setPersonProfile(PersonProfile? personProfile) {
    if (personProfile == null) {
      _preference?.remove(_KEY_PERSONAL_PROFILE);
    } else {
      String raw = json.encode(personProfile.toJson());
      _preference?.setString(_KEY_PERSONAL_PROFILE, raw);
      setData(Const.ID, personProfile.userUuid);
      setData(Const.FULL_NAME, personProfile.fullName);
      setData(Const.AVATAR, personProfile.avatarUrl);
      setData(Const.EMAIL, personProfile.email);
      setData(Const.DOB, personProfile.birthday);
      setData(Const.PROVINCE, personProfile.provinceName);
      setData(Const.COUNTRY_NAME, personProfile.countryName);
      setData(Const.ADDRESS, personProfile.communityV2?.location);
      setData(Const.GENDER, personProfile.gender);
      setData(Const.PROVINCE_ID, personProfile.provinceId);
    }
    _personProfile = personProfile;
  }

  LoginResponse? get loginResponse {
    if (_loginResponse == null) {
      String? raw = _preference?.getString(_KEY_LOGIN_RESPONSE);
      if (raw != null) {
        dynamic rawJson = json.decode(raw);
        if (rawJson != null) {
          _loginResponse = LoginResponse.fromJson(rawJson);
        }
      }
    }
    return _loginResponse;
  }

  void setLoginResponse(LoginResponse? loginResponse) {
    if (loginResponse == null) {
      _preference?.remove(_KEY_LOGIN_RESPONSE);
    } else {
      String raw = json.encode(loginResponse.toJson());
      _preference?.setString(_KEY_LOGIN_RESPONSE, raw);
    }
    _loginResponse = loginResponse;
  }

  String get appLanguage {
    return _preference?.getString(Const.LANGUAGE) ?? Const.LOCALE_VN;
  }

  String? get email {
    return _preference?.getString(Const.EMAIL);
  }

  LocaleType get locale {
    if (appLanguage == Const.LOCALE_EN) return LocaleType.en;
    return LocaleType.vi;
  }

  void saveAppLanguage(String language) {
    _preference?.setString(Const.LANGUAGE, language);
  }

  void saveData(UserData? user) {
    if (user != null) {
      setData(Const.ID, user.userUuid);
      setData(Const.FULL_NAME, user.fullName);
      setData(Const.USERNAME, user.username);
      setData(Const.AVATAR, user.picture);
      setData(Const.EMAIL, user.email);
      setData(Const.DOB, user.birthdate);
      setData(Const.PROVINCE, user.province);
      setData(Const.GENDER, user.gender);
      setData(Const.GROUP, user.group);
      setData(Const.BIRTH_PLACE, user.birthPlace);
      setData(Const.USER_POINT, user.userPoint);
      setData(Const.REFERRAL_CODE, user.referralCode);
    }
  }

  void saveCommunity(CommunityData? myCommunity) {
    if (myCommunity == null) return;
    setData(Const.COMMUNITY_ID, myCommunity.id);
    setData(Const.OWNER_ID, myCommunity.ownerId);
    setData(Const.AVATAR, myCommunity.avatarUrl);
    setData(Const.COVER, myCommunity.imageUrl);
    setData(Const.POSTS, myCommunity.postNo);
    setData(Const.FOLLOWERS, myCommunity.followerNo);
    setData(Const.FOLLOWINGS, myCommunity.followingNo);
  }

  void saveCommunityV2(CommunityDataV2? myCommunity) {
    if (myCommunity == null) return;
    setData(Const.COMMUNITY_ID, myCommunity.id);
    setData(Const.OWNER_ID, myCommunity.ownerProfile?.userUuid);
    setData(Const.AVATAR, myCommunity.avatarUrl);
    setData(Const.COVER, myCommunity.imageUrl);
    setData(Const.POSTS, myCommunity.communityInfo?.postNo);
    setData(Const.FOLLOWERS, myCommunity.communityInfo?.followerNo);
    setData(Const.FOLLOWINGS, myCommunity.communityInfo?.followingNo);
    if( myCommunity.communityInfo?.communityRole?.length!=0){
      setData(Const.LEADER, myCommunity.communityInfo?.communityRole?[0].name);
    }
    setData(Const.INFO, myCommunity.ownerProfile?.intro);
    setData(Const.COUNTRY_NAME, myCommunity.ownerProfile?.countryName);
    setData(Const.PROVINCE, myCommunity.ownerProfile?.provinceName);
    setData(Const.POINT, myCommunity.ownerProfile?.point);
    var listFavoriteCategory =
        (myCommunity.categories ?? []).map((e) => e?.id ?? 0).toList();
    appPreferences.setData(Const.FAVORITE_CATEGORY, listFavoriteCategory);
    setData(Const.DCI_LEVEL,
        (myCommunity.communityInfo?.communityLevel ?? []).isNotEmpty);
    var levelId =
    (myCommunity.communityInfo?.communityLevel?.map((e) => e?.level).toList());
    setData(Const.DCI_LEVEL_ID, levelId);
    setData(Const.COMMUNITY_V2, jsonEncode(myCommunity.toJson()));
  }

  void savePoint(int? point){
    setData(Const.POINT, point);
  }

  void clearData() {
    setPersonProfile(null);
    setLoginResponse(null);
    removeData(Const.ID);
    removeData(Const.COMMUNITY_ID);
    removeData(Const.OWNER_ID);
    removeData(Const.PROVINCE);
    removeData(Const.HEADER_KEY_DEVICE_TOKEN);
    removeData(Const.EMAIL);
    removeData(Const.FULL_NAME);
    removeData(Const.USERNAME);
    removeData(Const.AVATAR);
    removeData(Const.COVER);
    removeData(Const.GENDER);
    removeData(Const.POINT);
    removeData(Const.POSTS);
    removeData(Const.FOLLOWINGS);
    removeData(Const.FOLLOWERS);
    removeData(Const.GROUP);
    removeData(Const.BIRTH_PLACE);
    removeData(Const.FAVORITE_CATEGORY);
    removeData(Const.FAVORITE_COMMUNITY);
    removeData(Const.DCI_LEVEL);
    removeData(Const.DCI_LEVEL_ID);
    removeData(Const.COMMUNITY_V2);
    removeData(Const.PROVINCE);
    removeData(Const.PROVINCE_ID);
    removeData(Const.COUNT_MESSAGE);
    removeData(Const.COUNTRY_NAME);
    removeData(Const.ADDRESS);
    removeData(Const.COUNT_MESSAGE);
  }

  String? getString(String key) {
    String? value = _preference?.getString(key);
    if (value?.isEmpty == true) value = null;
    return value;
  }

  List<String> getListString(String key) {
    return _preference?.getStringList(key) ?? [];
  }

  bool? getBool(String key) {
    return _preference?.getBool(key);
  }

  int? getInt(String key) {
    return _preference?.getInt(key);
  }

  void setData(String key, Object? data) {
    if (data == null) return;
    if (data is int) {
      _preference?.setInt(key, data);
    }
    if (data is String) {
      _preference?.setString(key, data);
    }
    if (data is bool) {
      _preference?.setBool(key, data);
    }
    if (data is List<dynamic>) {
      _preference?.setStringList(key, data.map((e) => e.toString()).toList());
    }
  }

  void removeData(String key) {
    _preference?.remove(key);
  }
}

AppPreferences appPreferences = AppPreferences();
