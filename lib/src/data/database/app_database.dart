import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'dao/search_dao.dart';
import 'entities/search_keyword_entity.dart';

part 'app_database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [SearchKeywordEntity])
abstract class AppDatabase extends FloorDatabase {
  SearchDao get searchDao;
}