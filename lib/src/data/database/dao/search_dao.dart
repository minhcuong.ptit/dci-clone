import 'package:floor/floor.dart';
import 'package:imi/src/data/database/entities/search_keyword_entity.dart';

@dao
abstract class SearchDao {
  @Query('SELECT * FROM search_keyword')
  Future<List<SearchKeywordEntity>> findAllKeywords();

  @Query('SELECT * FROM search_keyword WHERE keyword = :keyword')
  Stream<SearchKeywordEntity?> findKeywordByText(String keyword);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertSearchKeyword(SearchKeywordEntity item);

  Future<void> insertWithKeyword(SearchKeywordEntity object) async {
    object.id = await insertSearchKeyword(object);
  }

  @delete
  Future<void> deleteKeyword(SearchKeywordEntity item);

  @delete
  Future<int> deleteKeywords(List<SearchKeywordEntity> keywords);

  @Query('DELETE FROM search_keyword')
  Future<void> deleteAllKeyword();
}
