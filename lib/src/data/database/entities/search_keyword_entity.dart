import 'package:floor/floor.dart';

@Entity(tableName: 'search_keyword', primaryKeys: ['id', 'keyword'], indices: [Index(value: ['keyword'], unique: true)])
class SearchKeywordEntity {
  @PrimaryKey(autoGenerate: true)
  int id;

  @ColumnInfo(name: 'keyword')
  final String keyword;

  SearchKeywordEntity(this.id, this.keyword);
}