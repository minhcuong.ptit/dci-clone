import 'dart:async';
import 'dart:io' as io;
import 'package:imi/src/data/database/sqflite/search_suggest.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  static Database? _db;
  static const String ID = 'id';
  static const String KEYWORD = 'keyWord';
  static const String TYPE = 'type';
  static const String TABLE = 'searchSuggest';
  static const String DB_NAME = 'searchSuggest1.db';

  Future<Database?> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    //init db
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    //tạo database
    await db.execute(
        "CREATE TABLE $TABLE ($ID INTEGER PRIMARY KEY, $KEYWORD TEXT UNIQUE, $TYPE TEXT)");
  }

  Future save(SearchSuggest searchSuggest) async {
    if ((searchSuggest.keyWord ?? "").isEmpty) return;
    // insert searchSuggest vào bảng đơn giản
    var dbClient = await db;
    searchSuggest.id = await dbClient!.insert(TABLE, searchSuggest.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return searchSuggest;
    /*
    await dbClient.transaction((txn) async {
      var query = "INSERT INTO $TABLE ($NAME) VALUES ('" + searchSuggest.name + "')";
      return await txn.rawInsert(query); //các bạn có thể sử dụng rawQuery nếu truy vẫn phức tạp để thay thế cho các phước thức có sẵn của lớp Database.
    });
    */
  }

  Future<List<SearchSuggest>> getSearchSuggests(
      {required String keyword, required String type}) async {
    //get list searchSuggests đơn giản
    var dbClient = await db;
    List<Map<String, dynamic>> maps = await dbClient!.rawQuery(
        """SELECT * FROM $TABLE WHERE $KEYWORD LIKE "%$keyword%" AND $TYPE = "$type" ORDER BY $ID DESC LIMIT 5""");
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
    List<SearchSuggest> searchSuggests = [];
    if (maps.isNotEmpty) {
      for (int i = 0; i < maps.length; i++) {
        searchSuggests.add(SearchSuggest.fromJson(maps[i]));
      }
    }
    return searchSuggests;
  }

  Future<int> delete(int id) async {
    // xóa searchSuggest
    var dbClient = await db;
    return await dbClient!.delete(TABLE,
        where: '$ID = ?',
        whereArgs: [id]); //where - xóa tại ID nào, whereArgs - argument là gì?
  }

  deleteAll() async {
    var deleteAll = await db;
    return await deleteAll?.delete(TABLE);
  }

  Future<int> update(SearchSuggest searchSuggest) async {
    var dbClient = await db;
    return await dbClient!.update(TABLE, searchSuggest.toJson(),
        where: '$ID = ?', whereArgs: [searchSuggest.id]);
  }

  Future close() async {
    //close khi không sử dụng
    var dbClient = await db;
    dbClient!.close();
  }
}
