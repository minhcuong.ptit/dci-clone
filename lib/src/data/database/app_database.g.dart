// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  SearchDao? _searchDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `search_keyword` (`id` INTEGER NOT NULL, `keyword` TEXT NOT NULL, PRIMARY KEY (`id`, `keyword`))');
        await database.execute(
            'CREATE UNIQUE INDEX `index_search_keyword_keyword` ON `search_keyword` (`keyword`)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  SearchDao get searchDao {
    return _searchDaoInstance ??= _$SearchDao(database, changeListener);
  }
}

class _$SearchDao extends SearchDao {
  _$SearchDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _searchKeywordEntityInsertionAdapter = InsertionAdapter(
            database,
            'search_keyword',
            (SearchKeywordEntity item) =>
                <String, Object?>{'id': item.id, 'keyword': item.keyword},
            changeListener),
        _searchKeywordEntityDeletionAdapter = DeletionAdapter(
            database,
            'search_keyword',
            ['id', 'keyword'],
            (SearchKeywordEntity item) =>
                <String, Object?>{'id': item.id, 'keyword': item.keyword},
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<SearchKeywordEntity>
      _searchKeywordEntityInsertionAdapter;

  final DeletionAdapter<SearchKeywordEntity>
      _searchKeywordEntityDeletionAdapter;

  @override
  Future<List<SearchKeywordEntity>> findAllKeywords() async {
    return _queryAdapter.queryList('SELECT * FROM search_keyword',
        mapper: (Map<String, Object?> row) =>
            SearchKeywordEntity(row['id'] as int, row['keyword'] as String));
  }

  @override
  Stream<SearchKeywordEntity?> findKeywordByText(String keyword) {
    return _queryAdapter.queryStream(
        'SELECT * FROM search_keyword WHERE keyword = ?1',
        mapper: (Map<String, Object?> row) =>
            SearchKeywordEntity(row['id'] as int, row['keyword'] as String),
        arguments: [keyword],
        queryableName: 'search_keyword',
        isView: false);
  }

  @override
  Future<void> deleteAllKeyword() async {
    await _queryAdapter.queryNoReturn('DELETE FROM search_keyword');
  }

  @override
  Future<int> insertSearchKeyword(SearchKeywordEntity item) {
    return _searchKeywordEntityInsertionAdapter.insertAndReturnId(
        item, OnConflictStrategy.replace);
  }

  @override
  Future<void> deleteKeyword(SearchKeywordEntity item) async {
    await _searchKeywordEntityDeletionAdapter.delete(item);
  }

  @override
  Future<int> deleteKeywords(List<SearchKeywordEntity> keywords) {
    return _searchKeywordEntityDeletionAdapter
        .deleteListAndReturnChangedRows(keywords);
  }
}
