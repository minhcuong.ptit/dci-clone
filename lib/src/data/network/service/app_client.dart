import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:gql_dio_link/gql_dio_link.dart';
import "package:gql_link/gql_link.dart";
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/authentication/authentication_cubit.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/utils/app_config.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../../../main.dart';
import '../app_api.dart';

const _defaultConnectTimeout = Duration.millisecondsPerMinute;
const _defaultReceiveTimeout = Duration.millisecondsPerMinute;

class AppClient {
  late AppApi appClient;
  late Link link;
  late Dio dio;

  AppClient._privateConstructor() {
    _setupClient();
  }

  static final AppClient _instance = AppClient._privateConstructor();

  factory AppClient() {
    return _instance;
  }

  void _setupClient() {
    dio = Dio(BaseOptions(
        baseUrl: AppConfig.environment.apiEndpoint,
        receiveTimeout: 60 * 1000 * 1,
        connectTimeout: 60 * 1000 * 1,
        contentType: "application/json"));
    dio
      ..options.connectTimeout = _defaultConnectTimeout
      ..options.receiveTimeout = _defaultReceiveTimeout
      ..options.headers = {'Content-Type': 'application/json; charset=UTF-8'};

    link = DioLink(
      AppConfig.environment.graphqlEndpoint,
      client: dio,
    );

    dio.interceptors.add(alice.getDioInterceptor());
    if (kDebugMode) {
      // dio.interceptors.add(alice.getDioInterceptor());
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          error: true,
          compact: true,
          maxWidth: 1000));
    }

    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      options.headers["Accept-Language"] = appPreferences.appLanguage;
      String? deviceToken =
          appPreferences.getString(Const.HEADER_KEY_DEVICE_TOKEN);
      if (deviceToken == null) {
        await FirebaseMessaging.instance
            .getToken()
            .then((value) => deviceToken = value);
        appPreferences.setData(Const.HEADER_KEY_DEVICE_TOKEN, deviceToken);
      }
      options.headers["X-Device-Token"] = deviceToken;
      String? accessToken = appPreferences.loginResponse?.accessToken;
      if (!Utils.isEmpty(accessToken)) {
        options.headers["Authorization"] = "Bearer $accessToken";
        dio.options.headers["Authorization"] = "Bearer $accessToken";
      } else {
        options.headers.remove("Authorization");
        dio.options.headers.remove("Authorization");
      }
      if (options.path.contains("s3.")) {
        options.headers.remove("Authorization");
        dio.options.headers.remove("Authorization");
      }
      return handler.next(options);
    }, onResponse: (response, handler) async {
      if (response.data is Map && response.data?["errors"] != null) {
        return handler.reject(DioError(
            requestOptions: response.requestOptions,
            error: "${response.data["errors"].toString()}",
            type: DioErrorType.response,
            response: response));
      }
      return handler.next(response);
    }, onError: (DioError error, ErrorInterceptorHandler handler) async {
      logger.d("DioError request: ${error.requestOptions.toString()}");
      logger.d("DioError message: ${error.message}");
      if (error.response?.statusCode == 401) {
        logger.e("UnAuthorization");
        if(!error.requestOptions.path.contains("keycloak")) {
          final AuthenticationCubit cubit = GetIt.I<AuthenticationCubit>();
          cubit.forceLogout();
        }

        return handler.reject(DioError(
            requestOptions: error.requestOptions,
            error: error,
            type: DioErrorType.response,
            response: error.response));
      }
      if (error.error is SocketException) {
        // TODO: handle SocketException
      }
      return handler.next(error); //continue
    }));
    appClient = AppApi(dio);
  }
}

AppApi appClient = AppClient().appClient;
Link link = AppClient().link;
Dio dio = AppClient().dio;
