import 'package:get_it/get_it.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/page/authentication/authentication_cubit.dart';
import 'package:imi/src/page/main/main.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';

abstract class BaseRepository {
  late AppPreferences appPreferences;
  late String userLogin;

  ApiResult<T> handleErrorApi<T>(dynamic e,
      {String tag = "", forceLogout = true}) {
    logger.e(tag, e);
    NetworkExceptions exceptions = NetworkExceptions.getDioException(e);
    if (exceptions is UnauthorizedRequest && forceLogout) {
      final AuthenticationCubit cubit = GetIt.I<AuthenticationCubit>();
      cubit.forceLogout();
    } else if (forceLogout &&
        exceptions is BadRequest &&
        exceptions.code == ServerError.user_is_banned_error) {
      final AuthenticationCubit cubit = GetIt.I<AuthenticationCubit>();
      cubit.forceLogout();
    }
    return ApiResult.failure(error: exceptions);
  }
}
