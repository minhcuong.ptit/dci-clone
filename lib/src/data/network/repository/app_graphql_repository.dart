import 'package:gql/language.dart' as gqlLang;
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:imi/src/data/network/repository/base_repository.dart';
import 'package:imi/src/data/network/response/association_club_list.dart';
import 'package:imi/src/data/network/response/audio_response.dart';
import 'package:imi/src/data/network/response/bank_response.dart';
import 'package:imi/src/data/network/response/banner_response.dart';
import 'package:imi/src/data/network/response/cancel_order_response.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/challeng_response.dart';
import 'package:imi/src/data/network/response/challenge_detail_response.dart';
import 'package:imi/src/data/network/response/check_phone_forgot_response.dart';
import 'package:imi/src/data/network/response/city_response.dart';
import 'package:imi/src/data/network/response/clean_seed_history_response.dart';
import 'package:imi/src/data/network/response/club_response.dart';
import 'package:imi/src/data/network/response/comment.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/course_detail_response.dart';
import 'package:imi/src/data/network/response/course_response.dart';
import 'package:imi/src/data/network/response/custom_support_response.dart';
import 'package:imi/src/data/network/response/dci_notification_response.dart';
import 'package:imi/src/data/network/response/deatil_cart_response.dart';
import 'package:imi/src/data/network/response/detail_challenge_response.dart';
import 'package:imi/src/data/network/response/detail_community_response.dart';
import 'package:imi/src/data/network/response/detail_coupons_response.dart';
import 'package:imi/src/data/network/response/detail_library_response.dart';
import 'package:imi/src/data/network/response/detail_meditation_response.dart';
import 'package:imi/src/data/network/response/detail_plan_seed_response.dart';
import 'package:imi/src/data/network/response/detail_report_meditation_response.dart';
import 'package:imi/src/data/network/response/detail_report_response.dart';
import 'package:imi/src/data/network/response/follow_response.dart';
import 'package:imi/src/data/network/response/g_point_transaction_response.dart';
import 'package:imi/src/data/network/response/get_club_response.dart';
import 'package:imi/src/data/network/response/histories_meditation_response.dart';
import 'package:imi/src/data/network/response/home_response.dart';
import 'package:imi/src/data/network/response/library_response.dart';
import 'package:imi/src/data/network/response/list_block_user.dart';
import 'package:imi/src/data/network/response/list_category_response.dart';
import 'package:imi/src/data/network/response/list_community_response.dart';
import 'package:imi/src/data/network/response/list_content_response.dart';
import 'package:imi/src/data/network/response/list_membership_response.dart';
import 'package:imi/src/data/network/response/list_order_course_response.dart';
import 'package:imi/src/data/network/response/meditation_response.dart';
import 'package:imi/src/data/network/response/membership_information_response.dart';
import 'package:imi/src/data/network/response/membership_profile_response.dart';
import 'package:imi/src/data/network/response/mission_response.dart';
import 'package:imi/src/data/network/response/my_community_response.dart';
import 'package:imi/src/data/network/response/my_profile_response.dart';
import 'package:imi/src/data/network/response/news_details_response.dart';
import 'package:imi/src/data/network/response/news_response.dart';
import 'package:imi/src/data/network/response/notification_response.dart';
import 'package:imi/src/data/network/response/order_course_response.dart';
import 'package:imi/src/data/network/response/payment_response.dart';
import 'package:imi/src/data/network/response/plan_seed_response.dart';
import 'package:imi/src/data/network/response/plant_seed_goals_response.dart';
import 'package:imi/src/data/network/response/post_data.dart';
import 'package:imi/src/data/network/response/post_response.dart';
import 'package:imi/src/data/network/response/post_story_response.dart';
import 'package:imi/src/data/network/response/report_meditation_response.dart';
import 'package:imi/src/data/network/response/report_reasons_response.dart';
import 'package:imi/src/data/network/response/search_communities_response.dart';
import 'package:imi/src/data/network/response/search_user_chat_response.dart';
import 'package:imi/src/data/network/response/status_order_response.dart';
import 'package:imi/src/data/network/response/user_data.dart';
import 'package:imi/src/data/network/response/user_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';

import '../response/association_response.dart';
import '../response/buy_more_response.dart';
import '../response/category_library_response.dart';
import '../response/category_story_response.dart';
import '../response/clean_seeds_setting_response.dart';
import '../response/coupons_response.dart';
import '../response/course_by_more_response.dart';
import '../response/course_topics_response.dart';
import '../response/detail_event_response.dart';
import '../response/detail_order_product.dart';
import '../response/detail_order_response.dart';
import '../response/document_categories_response.dart';
import '../response/document_topic_response.dart';
import '../response/event_by_more_response.dart';
import '../response/event_response.dart';
import '../response/event_topics_response.dart';
import '../response/exchangeRate.dart';
import '../response/fetch_event_data.dart';
import '../response/history_point_response.dart';
import '../response/list_community_like_post_response.dart';
import '../response/list_dci_community_response.dart';
import '../response/list_joined_channel_response.dart';
import '../response/list_question_response.dart';
import '../response/list_topic_response.dart';
import '../response/media_response.dart';
import '../response/member_community.dart';
import '../response/members_list.dart';
import '../response/membership.dart';
import '../response/merchant_response.dart';
import '../response/person_profile.dart';
import '../response/point_action.dart';
import '../response/point_action_category_response.dart';
import '../response/point_action_out.dart';
import '../response/post_detail_data.dart';
import '../response/product_topic_response.dart';
import '../response/profile_chat_user_response.dart';
import '../response/question_response.dart';
import '../response/related_question_response.dart';
import '../response/un_read_count_response.dart';
import '../response/user_channel_group_response.dart';
import '../service/app_client.dart';
import 'query_graphql.dart';

class AppGraphqlRepository extends BaseRepository {
  Future<ApiResult<UserData>> getMeStatus() async {
    return getMeProfile(query: queryMeStatus);
  }

  Future<ApiResult<UserData>> getMeProfile({String query = queryMe}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(query)),
          ))
          .first;
      UserResponse user = UserResponse.fromJson(response.data);
      if (user.me != null)
        return ApiResult.success(data: user.me!);
      else
        return ApiResult.failure(
            error: NetworkExceptions.notFound("Not found me"));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PersonProfile>> getMyPersonalProfile() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(myProfile)),
          ))
          .first;
      PersonProfileResponse personProfileResponse =
          PersonProfileResponse.fromJson(response.data);
      if (personProfileResponse.profile != null)
        return ApiResult.success(data: personProfileResponse.profile!);
      else
        return ApiResult.failure(
            error: NetworkExceptions.notFound("Not found me"));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CategoryData>>> getListCategory(
      {String type = Const.CLUB}) async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryCategories(type))),
          ))
          .first;
      ListCategoryResponse listCategoryResponse =
          ListCategoryResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse.categories ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  // Future<ApiResult<List<CommunityData>>> getRecommendCommunity(
  //     {String type = Const.CLUB}) async {
  //   try {
  //     Response response = await link
  //         .request(Request(
  //       operation:
  //       Operation(document: gqlLang.parseString(queryCategories(type))),
  //     ))
  //         .first;
  //     ListCategoryResponse listCategoryResponse =
  //     ListCategoryResponse.fromJson(response.data);
  //     return ApiResult.success(data: listCategoryResponse.categories ?? []);
  //   } catch (e) {
  //     return handleErrorApi(e);
  //   }
  // }

  Future<ApiResult<List<BankData>>> getListBank({String name = ""}) async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryBanks(name))),
          ))
          .first;
      BankResponse bankResponse = BankResponse.fromJson(response.data);
      return ApiResult.success(data: bankResponse.banks ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<UserData>>> getListMember({String phone = ""}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMemberByPhone(phone))),
          ))
          .first;
      MembershipProfileResponse memberResponse =
          MembershipProfileResponse.fromJson(response.data);
      List<UserData> listMember = [];
      memberResponse.membershipProfile?.forEach((element) {
        if (element.profile != null && element.isEnable == true) {
          listMember.add(element.profile!);
        }
      });
      return ApiResult.success(data: listMember);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<List<UserData>> searchMember({String phone = ""}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMemberByPhone(phone))),
          ))
          .first;
      MembershipProfileResponse memberResponse =
          MembershipProfileResponse.fromJson(response.data);
      List<UserData> listMember = [];
      memberResponse.membershipProfile?.forEach((element) {
        if (element.profile != null && element.isEnable == true) {
          listMember.add(element.profile!);
        }
      });
      return listMember;
    } catch (e) {
      return [];
    }
  }

  Future<ApiResult<ListMembershipResponse>> getListMembership() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryMyMembership)),
          ))
          .first;
      ListMembershipResponse listMembershipResponse =
          ListMembershipResponse.fromJson(response.data);
      return ApiResult.success(data: listMembershipResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<Membership>>> getMemberships() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryMemberships)),
          ))
          .first;
      MembershipListResponse membershipListResponse =
          MembershipListResponse.fromJson(response.data);
      return ApiResult.success(data: membershipListResponse.memberships);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CommunityData>>> getListCommunity() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryCommunities)),
          ))
          .first;
      ListCommunityResponse listCategoryResponse =
          ListCommunityResponse.fromJson(response.data);
      return ApiResult.success(
          data: listCategoryResponse.communities?.data ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ListContentResponse>> getListContent(
      List<int> categoryId) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryContent(Const.EMOJI, categoryId))),
          ))
          .first;
      ListContentResponse listCategoryResponse =
          ListContentResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ListContentResponse>> getRecommendCommunity(
      List<String> listCommunityTypeIn, List<int> categoryId) async {
    try {
      var _query = queryRecommendCommunity(listCommunityTypeIn, categoryId);
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(_query)),
          ))
          .first;
      ListContentResponse listCategoryResponse =
          ListContentResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ListDCICommunityResponse>> getDCICommunities(
    CommunityType type, {
    CommunityStatus? status,
    int? limit,
    int? communityId,
    String? nextToken,
    String? searchKey,
    bool? isValid,
    List<int>? levelIds,
    int? startTime,
    int? endTime,
    List<String>? dayOfWeek,
  }) async {
    try {
      var _query = queryDCICommunities(
        type,
        status: status,
        limit: limit,
        communityId: communityId,
        nextToken: nextToken,
        searchKey: searchKey,
        isValid: isValid,
        levelIds: levelIds,
        startTime: startTime,
        endTime: endTime,
        dayOfWeek: dayOfWeek,
      );
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(_query)),
          ))
          .first;
      ListDCICommunityResponse listCategoryResponse =
          ListDCICommunityResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<HomeResponse>> getHome({
    List<int>? listCategory,
    bool? myPost,
    String? nextToken,
  }) async {
    try {
      Response response = await link
          .request(
            Request(
              operation: Operation(
                  document: gqlLang.parseString(queryHome(
                      nextToken: nextToken,
                      myPost: myPost,
                      typeCategory: Const.EMOJI,
                      listCategory: listCategory))),
            ),
          )
          .first;
      HomeResponse homeResponse = HomeResponse.fromJson(response.data);
      return ApiResult.success(data: homeResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CommunityDataV2Response>> getCommunityV2(
      int communityId) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryCommunityV2(communityId))),
          ))
          .first;
      CommunityDataV2Response myCommunityResponse =
          CommunityDataV2Response.fromJson(response.data ?? {});
      return ApiResult.success(data: myCommunityResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MyProfileResponse>> getMyProfile() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(queryMyProfile)),
          ))
          .first;
      MyProfileResponse myProfileResponse =
          MyProfileResponse.fromJson(response.data);
      return ApiResult.success(data: myProfileResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CityData>>> getListProvince() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(queryProvinces)),
          ))
          .first;
      CityResponse listCategoryResponse = CityResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse.address ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CityData>>> getListCity(int parentId) async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryCities(parentId))),
          ))
          .first;
      CityResponse listCategoryResponse = CityResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse.address ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CityData>>> getListDistrict(int parentId) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryDistricts(parentId))),
          ))
          .first;
      CityResponse listCategoryResponse = CityResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse.address ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CityData>>> getListWard(int parentId) async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryWards(parentId))),
          ))
          .first;
      CityResponse listCategoryResponse = CityResponse.fromJson(response.data);
      return ApiResult.success(data: listCategoryResponse.address ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Communities>> getListClub(int provinceId,
      {String? searchTerm,
      String? nextToken = null,
      int limit = Const.NETWORK_DEFAULT_LIMIT}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryClub(provinceId,
                    searchTerm: searchTerm,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      GetClubResponse getClubResponse = GetClubResponse.fromJson(response.data);
      if (getClubResponse.communities != null) {
        return ApiResult.success(data: getClubResponse.communities!);
      } else {
        return ApiResult.failure(
            error: NetworkExceptions.notFound("Not found the community"));
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Communities>> searchByKeyword(String text,
      {String? nextToken = null,
      int limit = Const.NETWORK_DEFAULT_LIMIT}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(querySearch(
                    text: text, nextToken: nextToken, limit: limit))),
          ))
          .first;
      GetClubResponse getSearchResponse =
          GetClubResponse.fromJson(response.data);
      if (getSearchResponse.communities != null) {
        return ApiResult.success(data: getSearchResponse.communities!);
      } else {
        return ApiResult.failure(
            error:
                NetworkExceptions.notFound("Not found the search community"));
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<GPointTransactionResponse>> getGPointHistory(
      {String? nextToken = null,
      int limit = Const.NETWORK_DEFAULT_LIMIT}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryGPoint(nextToken: nextToken, limit: limit))),
          ))
          .first;
      GPointTransactionResponse gPointTransactionResponse =
          GPointTransactionResponse.fromJson(response.data);
      return ApiResult.success(data: gPointTransactionResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PostPageData?>> getListPost(String? nextToken,
      {int? communityId,
      List<String>? listCommunityTypeIn,
      bool? isBookmarked,
      String? postStatus,
      int? limit}) async {
    try {
      var query = (isBookmarked ?? false)
          ? queryBookmarks(isBookmarked: isBookmarked, nextToken: nextToken)
          : getQueryPost(
              postStatus: postStatus,
              nextToken: nextToken,
              communityId: communityId,
              listCommunityTypeIn: listCommunityTypeIn,
              limit: limit ?? Const.LIMIT);
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(query)),
          ))
          .first;
      PostResponse postResponse = PostResponse.fromJson(response.data);
      return ApiResult.success(data: postResponse.posts);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PostData?>> getPostDetail({required int? postId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(getQueryPostDetail(postId: postId))),
          ))
          .first;
      PostDetailData postResponse = PostDetailData.fromJson(response.data);
      return ApiResult.success(data: postResponse.data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CommentPage>> getComment(
      {int? limit, int? parentId, String? nextToken, int? postId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryComment(
                    postId: postId,
                    parentId: parentId,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      CommentResponse commentResponse = CommentResponse.fromJson(response.data);
      if (commentResponse.comments != null)
        return ApiResult.success(data: commentResponse.comments!);
      else
        return ApiResult.failure(
            error: NetworkExceptions.notFound("Not found the comment"));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CommunityMedia?>> getMedia(String? nextToken,
      {required int communityId, List<String>? listCommunityTypeIn}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(getQueryMedia(
                    nextToken: nextToken, communityId: communityId))),
          ))
          .first;
      MediaResponse mediaResponse = MediaResponse.fromJson(response.data ?? {});
      return ApiResult.success(data: mediaResponse.communityMedia);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FollowsV2>> getFollows(bool? isFollowers, int? communityId,
      int? limit, String? nextToken) async {
    try {
      // Response response = await link
      //     .request(Request(
      //       operation: Operation(
      //           document: gqlLang.parseString(queryFollows(
      //               communityId: communityId,
      //               type: isFollowers == true ? "FOLLOWER" : "FOLLOWING",
      //               nextToken: nextToken,
      //               limit: limit))),
      //     ))
      //     .first;
      // FollowResponse followResponse = FollowResponse.fromJson(response.data);
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFollowsV2(
                    communityId: communityId,
                    type: isFollowers == true ? "FOLLOWER" : "FOLLOWING",
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      FollowResponseV2 followResponse =
          FollowResponseV2.fromJson(response.data);
      return ApiResult.success(data: followResponse.follows ?? FollowsV2());
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ListCommunityLikePostResponse>> getLikes(
      int? postId, int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryLikes(
                    postId: postId, nextToken: nextToken, limit: limit))),
          ))
          .first;
      ListCommunityLikePostResponse likeResponse =
          ListCommunityLikePostResponse.fromJson(response.data);
      if (likeResponse.dciCommunities?.data?.isNotEmpty ?? false) {
        return ApiResult.success(data: likeResponse);
      } else {
        return ApiResult.success(data: ListCommunityLikePostResponse());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MembershipInformation>> getMembershipInformation(
      {int? membershipId, MembershipType? membershipType}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMembershipInfo(
                    membershipId: membershipId,
                    membershipType: membershipType))),
          ))
          .first;
      MembershipInformationResponse membershipResponse =
          MembershipInformationResponse.fromJson(response.data);
      if (membershipResponse.membershipInformation != null)
        return ApiResult.success(
            data: membershipResponse.membershipInformation!);
      else
        return ApiResult.failure(
            error:
                NetworkExceptions.notFound("Not found membership information"));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MembersListResponse>> getMembers({
    int? communityId,
    String? nextToken,
    int limit = Const.NETWORK_DEFAULT_LIMIT,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMembersOfCommunity(
              communityId: communityId,
              nextToken: nextToken,
              limit: limit,
            ))),
          ))
          .first;
      dynamic data = response.data?["memberOfCommunity"];
      return ApiResult.success(data: MembersListResponse.fromJson(data));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MembersListResponse>> getRequestToJoin({
    required int communityId,
    String? nextToken,
    int limit = Const.NETWORK_DEFAULT_LIMIT,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(fetchGroupMemberRequest(
              communityId: communityId,
              nextToken: nextToken,
              limit: limit,
            ))),
          ))
          .first;
      return ApiResult.success(
          data: MembersListResponse.fromJson(
              response.data?['fetchGroupMemberRequest'] ?? {}));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Notifications>> getNotifications(
      String userUuid, bool? isRead, int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryNotifications(
                    userUuid: userUuid,
                    read: isRead,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      NotificationResponse notificationResponse =
          NotificationResponse.fromJson(response.data);
      if (notificationResponse.notifications != null) {
        return ApiResult.success(data: notificationResponse.notifications!);
      } else {
        return ApiResult.success(data: Notifications());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<int>> getNotificationCount(bool? isRead) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryNotificationCount(
              read: isRead,
            ))),
          ))
          .first;
      NotificationCountResponse countResponse =
          NotificationCountResponse.fromJson(response.data);
      return ApiResult.success(data: countResponse.count ?? 0);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Clubs>> getFollowingClubs(int? communityId, String? type,
      String? communityType, int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFollowingCommunities(
                    communityId: communityId,
                    type: type,
                    communityType: communityType,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      ClubResponse clubResponse = ClubResponse.fromJson(response.data);
      if (clubResponse.clubs != null) {
        return ApiResult.success(data: clubResponse.clubs!);
      } else {
        return ApiResult.success(data: Clubs());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Clubs>> getMyClubs(int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryMyClubs(nextToken: nextToken, limit: limit))),
          ))
          .first;
      MyClubResponse clubResponse = MyClubResponse.fromJson(response.data);
      if (clubResponse.clubs != null) {
        return ApiResult.success(data: clubResponse.clubs!);
      } else {
        return ApiResult.success(data: Clubs());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Merchants>> getFollowingMerchants(
      int? communityId,
      String? type,
      String? communityType,
      int? limit,
      String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFollowingCommunities(
                    communityId: communityId,
                    type: type,
                    communityType: communityType,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      MerchantResponse merchantResponse =
          MerchantResponse.fromJson(response.data);
      if (merchantResponse.merchants != null) {
        return ApiResult.success(data: merchantResponse.merchants!);
      } else {
        return ApiResult.success(data: Merchants());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Merchants>> getMerchants(
      int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMyCommunities(
                    type: Const.MERCHANT, nextToken: nextToken, limit: limit))),
          ))
          .first;
      MerchantResponse merchantResponse =
          MerchantResponse.fromJson(response.data);
      if (merchantResponse.merchants != null) {
        return ApiResult.success(data: merchantResponse.merchants!);
      } else {
        return ApiResult.success(data: Merchants());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Associations>> getFollowingAssociations(
      int? communityId,
      String? type,
      String? communityType,
      int? limit,
      String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFollowingCommunities(
                    communityId: communityId,
                    type: type,
                    communityType: communityType,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      AssociationResponse associationResponse =
          AssociationResponse.fromJson(response.data);
      if (associationResponse.associations != null) {
        return ApiResult.success(data: associationResponse.associations!);
      } else {
        return ApiResult.success(data: Associations());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Associations>> getAssociations(
      int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMyCommunities(
                    type: Const.ASSOCIATION,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      AssociationResponse associationResponse =
          AssociationResponse.fromJson(response.data);
      if (associationResponse.associations != null) {
        return ApiResult.success(data: associationResponse.associations!);
      } else {
        return ApiResult.success(data: Associations());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PostPageData>> getBookmarks(
      bool? isBookmarked, int? limit, String? nextToken) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryBookmarks(
                    isBookmarked: isBookmarked,
                    nextToken: nextToken,
                    limit: limit))),
          ))
          .first;
      PostResponse bookmarkResponse = PostResponse.fromJson(response.data);
      if (bookmarkResponse.posts != null) {
        return ApiResult.success(data: bookmarkResponse.posts!);
      } else {
        return ApiResult.success(data: PostPageData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<AssociationClubList>> getAssociationClubs(
      {int? associationId,
      int? clubId,
      int? provinceId,
      int? communityId,
      List<AssociationClubStatusType>? statuses,
      String? nextToken,
      int limit = Const.NETWORK_DEFAULT_LIMIT}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryAssociationClubs(
                    associationId: associationId,
                    clubId: clubId,
                    limit: limit,
                    provinceId: provinceId,
                    communityId: communityId,
                    statuses: statuses,
                    nextToken: nextToken))),
          ))
          .first;
      dynamic data = response.data?["associations"];
      return ApiResult.success(data: AssociationClubList.fromJson(data));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CommunityData>>> getPostingCommunities() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(postingCommunities)),
          ))
          .first;
      ListPostingCommunityResponse parsedResponse =
          ListPostingCommunityResponse.fromJson(response.data);
      return ApiResult.success(data: parsedResponse.data ?? []);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<LibraryDataFetchDocuments>> getLibrary(
      {String? nextToken,
      String? type,
      String? keyWord,
      bool? globalSearch,
      int limit = Const.NETWORK_DEFAULT_LIMIT}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryListLibrary(
                    type: type,
                    nextToken: nextToken,
                    keyWord: keyWord,
                    globalSearch: globalSearch,
                    limit: limit))),
          ))
          .first;
      LibraryResponse libraryResponse =
          LibraryResponse.fromJson(response.data!);
      if (libraryResponse.fetchDocumentsByType != null) {
        return ApiResult.success(data: libraryResponse.fetchDocumentsByType!);
      } else {
        return ApiResult.success(data: LibraryDataFetchDocuments());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchDocumentData>> getDetailLibrary(
      {int? documentId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryFetchDocumentById(documentId ?? 0))),
          ))
          .first;
      DetailLibraryResponse libraryResponse =
          DetailLibraryResponse.fromJson(response.data!);
      if (libraryResponse.fetchDocumentById != null) {
        return ApiResult.success(data: libraryResponse.fetchDocumentById!);
      } else {
        return ApiResult.success(data: FetchDocumentData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<NewsDataFetch>> getNews(
      {required NewsCategory typeCategory,
      List<int>? typeIds,
      List<int>? topicIds,
      List<int>? excludeIds,
      String? searchKey,
      String? nextToken,
      int? limit = Const.NETWORK_DEFAULT_LIMIT}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryNews(
                    typeCategory: typeCategory,
                    nextToken: nextToken,
                    typeIds: typeIds,
                    topicIds: topicIds,
                    searchKey: searchKey,
                    excludeIds: excludeIds,
                    limit: limit))),
          ))
          .first;
      NewsResponse newsResponse = NewsResponse.fromJson(response.data!);
      if (newsResponse.fetchNews != null) {
        return ApiResult.success(data: newsResponse.fetchNews!);
      } else {
        return ApiResult.success(data: NewsDataFetch());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<NewsDetailsData>> getNewsDetail({int? postId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryNewsDetails(postId: postId))),
          ))
          .first;
      NewsDetailsResponse newsDetailsResponse =
          NewsDetailsResponse.fromJson(response.data!);
      if (newsDetailsResponse.fetchNewsById != null) {
        return ApiResult.success(data: newsDetailsResponse.fetchNewsById!);
      } else {
        return ApiResult.success(data: NewsDetailsData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchCourse>> getCourse({
    String? searchKey,
    String? nextToken,
    String? priorityOrder,
    String? priceOrder,
    List<int>? topicIds,
    int? courseId,
    int? limit = Const.NETWORK_DEFAULT_LIMIT,
    bool? isHot,
    int? couponId,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(fetchCourses(
                    topicIds: topicIds,
                    courseId: courseId,
                    nextToken: nextToken,
                    searchKey: searchKey,
                    priorityOrder: priorityOrder,
                    priceOrder: priceOrder,
                    limit: limit,
                    isHot: isHot,
                    couponId: couponId))),
          ))
          .first;
      CourseResponseData courseResponse =
          CourseResponseData.fromJson(response.data!);
      if (courseResponse.fetchCourses != null) {
        return ApiResult.success(data: courseResponse.fetchCourses!);
      } else {
        return ApiResult.success(data: FetchCourse());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CourseDetailData>> getCourseDetail({int? courseId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryCourseDetail(courseId: courseId))),
          ))
          .first;
      CourseDetailResponse courseDetailResponse =
          CourseDetailResponse.fromJson(response.data!);
      if (courseDetailResponse.fetchCourseById != null) {
        return ApiResult.success(data: courseDetailResponse.fetchCourseById!);
      } else {
        return ApiResult.success(data: CourseDetailData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchCleanSeedHistory>> getCleanSeedHistory({
    String? nextToken,
    int? limit = Const.NETWORK_DEFAULT_LIMIT,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFetchCleanSeed(
              nextToken: nextToken,
              limit: limit,
            ))),
          ))
          .first;
      CleanSeedHistoryData cleanSeedHistoryResponse =
          CleanSeedHistoryData.fromJson(response.data!);
      if (cleanSeedHistoryResponse.fetchCleanSeedHistories != null) {
        return ApiResult.success(
            data: cleanSeedHistoryResponse.fetchCleanSeedHistories!);
      } else {
        return ApiResult.success(data: FetchCleanSeedHistory());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchCleanSeedHistoriesData>> getCleanSeedById(
      {required int cleanSeedId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryFetchCleanSeedById(cleanSeedId: cleanSeedId))),
          ))
          .first;
      FetchCleanSeedHistoriesData fetchCleanSeedHistoriesData =
          FetchCleanSeedHistoriesData.fromJson(
              response.data!['fetchCleanSeedHistoryById']);
      return ApiResult.success(data: fetchCleanSeedHistoriesData);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CleanSeedsSettingResponse>> getCleanSeedsSetting(
      {String? nextToken,
      int? limit = Const.NETWORK_DEFAULT_LIMIT,
      String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFetchCleanSeedsSetting(
                    nextToken: nextToken, limit: limit, searchKey: searchKey))),
          ))
          .first;
      CleanSeedsSettingResponse cleanSeedHistoryResponse =
          CleanSeedsSettingResponse.fromJson(response.data!);
      return ApiResult.success(data: cleanSeedHistoryResponse);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchPlantSeedHistory>> getPlanSeedHistory({
    String? nextToken,
    int? limit = Const.NETWORK_DEFAULT_LIMIT,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFetchPlanSeed(
              nextToken: nextToken,
              limit: limit,
            ))),
          ))
          .first;
      PlanSeedData planSeedData = PlanSeedData.fromJson(response.data!);
      if (planSeedData.fetchPlantSeedHistories != null) {
        return ApiResult.success(data: planSeedData.fetchPlantSeedHistories!);
      } else {
        return ApiResult.success(data: FetchPlantSeedHistory());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PlantSeedHistory>> detailPlanSeed(
      {int? plantHistoryId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryDetailPlanSeed(plantHistoryId: plantHistoryId))),
          ))
          .first;
      DetailPlanSeedData data = DetailPlanSeedData.fromJson(response.data!);
      if (data.fetchPlantSeedHistoryById != null) {
        return ApiResult.success(data: data.fetchPlantSeedHistoryById!);
      } else {
        return ApiResult.success(data: PlantSeedHistory());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ReportReasonsData>> getReportReasons(
      {String? reasonType}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryFetchReportReasons(reasonType: reasonType))),
          ))
          .first;
      ReportReasonsData reasonsResponse =
          ReportReasonsData.fromJson(response.data!);
      if (reasonsResponse != null) {
        return ApiResult.success(data: reasonsResponse);
      } else {
        return ApiResult.success(data: ReportReasonsData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchReportedPostInfo>> getDetailReport(
      {String? nextToken, int? postId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryDetailReport(
              nextToken: nextToken,
              postId: postId,
            ))),
          ))
          .first;
      ReportedPostResponse reportedPostResponse =
          ReportedPostResponse.fromJson(response.data!);
      if (reportedPostResponse.fetchReportedPostInfo != null) {
        return ApiResult.success(
            data: reportedPostResponse.fetchReportedPostInfo!);
      } else {
        return ApiResult.success(data: FetchReportedPostInfo());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PlantSeedGoalsData>> getFetchPlantSeedGoals(
      {String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFetchPlantSeedGoals(
              nextToken: nextToken,
            ))),
          ))
          .first;
      PlantSeedGoalsResponse plantSeedGoalsResponse =
          PlantSeedGoalsResponse.fromJson(response.data!);
      if (plantSeedGoalsResponse.fetchPlantSeedGoals != null) {
        return ApiResult.success(
            data: plantSeedGoalsResponse.fetchPlantSeedGoals!);
      } else {
        return ApiResult.success(data: PlantSeedGoalsData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchMeditation>> getMeditation({
    String? nextToken,
    int? limit = Const.NETWORK_DEFAULT_LIMIT,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFetchMeditation(
              nextToken: nextToken,
              limit: limit,
            ))),
          ))
          .first;
      MeditationResponseData meditation =
          MeditationResponseData.fromJson(response.data!);
      if (meditation.fetchMeditations != null) {
        return ApiResult.success(data: meditation.fetchMeditations!);
      } else {
        return ApiResult.success(data: FetchMeditation());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchMeditationByIdData>> getDetailMeditation(
      {int? meditationId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryDetailMeditation(meditationId: meditationId))),
          ))
          .first;
      DetailMeditationData detailMeditationResponse =
          DetailMeditationData.fromJson(response.data!);
      if (detailMeditationResponse.fetchMeditationById != null) {
        return ApiResult.success(
            data: detailMeditationResponse.fetchMeditationById!);
      } else {
        return ApiResult.success(data: FetchMeditationByIdData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchMeditationHistories>> getHistoriesMeditation(
      {String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryHistoriesMeditation(nextToken: nextToken))),
          ))
          .first;
      HistoriesMeditationData historiesMeditationData =
          HistoriesMeditationData.fromJson(response.data!);
      if (historiesMeditationData.fetchMeditationHistories != null) {
        return ApiResult.success(
            data: historiesMeditationData.fetchMeditationHistories!);
      } else {
        return ApiResult.success(data: FetchMeditationHistories());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchBlockedUsers>> getListBlock({String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryListBlock(nextToken: nextToken))),
          ))
          .first;
      ListBlockResponse listBlockResponse =
          ListBlockResponse.fromJson(response.data!);
      if (listBlockResponse.fetchBlockedUsers != null) {
        return ApiResult.success(data: listBlockResponse.fetchBlockedUsers!);
      } else {
        return ApiResult.success(data: FetchBlockedUsers());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<AudioData>> getAudio({AudioType? audioType}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryAudio(audioType: audioType))),
          ))
          .first;
      AudioData audioData = AudioData.fromJson(response.data!);
      if (audioData != null) {
        return ApiResult.success(data: audioData);
      } else {
        return ApiResult.success(data: AudioData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchGeneralMeditationReport>> getReportMeditation(
      {List<int>? meditationIds, ReportType? reportType}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryReportMeditation(
                    meditationIds: meditationIds, reportType: reportType))),
          ))
          .first;
      ReportMeditationData reportMeditationData =
          ReportMeditationData.fromJson(response.data!);
      if (reportMeditationData.fetchGeneralMeditationReport != null) {
        return ApiResult.success(
            data: reportMeditationData.fetchGeneralMeditationReport!);
      } else {
        return ApiResult.success(data: FetchGeneralMeditationReport());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<StudyGroupSearchCommunities>> getSearchCommunities({
    String? nextToken,
    String? keyWord,
    CommunityType? communityType,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(querySearchCommunities(
                    communityType: communityType,
                    searchKey: keyWord,
                    nextToken: nextToken))),
          ))
          .first;
      StudyGroupResponse studyGroupResponse =
          StudyGroupResponse.fromJson(response.data!);
      if (studyGroupResponse.searchCommunities != null) {
        return ApiResult.success(data: studyGroupResponse.searchCommunities!);
      } else {
        return ApiResult.success(data: StudyGroupSearchCommunities());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchDetailMeditationReport>> getDetailReportMeditation(
      {ReportType? reportType, List<int>? meditationIds}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryDetailReportMeditation(
                    reportType: reportType, meditationIds: meditationIds))),
          ))
          .first;
      DetailReportMeditationData reportMeditationData =
          DetailReportMeditationData.fromJson(response.data!);
      if (reportMeditationData.fetchDetailMeditationReport != null) {
        return ApiResult.success(
            data: reportMeditationData.fetchDetailMeditationReport!);
      } else {
        return ApiResult.success(data: FetchDetailMeditationReport());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchChallenge>> getListChallenges(
      {String? nextToken, List<String>? status, bool? todo}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryListChallenges(
                    nextToken: nextToken, status: status, todo: todo))),
          ))
          .first;
      ChallengesData challengesData = ChallengesData.fromJson(response.data!);
      if (challengesData.fetchChallenge != null) {
        return ApiResult.success(data: challengesData.fetchChallenge!);
      } else {
        return ApiResult.success(data: FetchChallenge());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<DetailChallengeData>> getDetailChallenge(
      {int? challengeVersionId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryDetailChallenge(
                    challengeVersionId: challengeVersionId))),
          ))
          .first;
      DetailChallengeData challengeData =
          DetailChallengeData.fromJson(response.data!);
      return ApiResult.success(data: challengeData);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<BannerData>> getBanner() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(queryBanner())),
          ))
          .first;
      BannerData bannerResponse = BannerData.fromJson(response.data!);
      if (bannerResponse != null) {
        return ApiResult.success(data: bannerResponse);
      } else {
        return ApiResult.success(data: BannerData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> countChallenge() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(countAvailableChallenge())),
          ))
          .first;
      dynamic data = response.data?["countAvailableChallenge"];
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MissionData>> getMission(
      {int? challengeMissionHistoryId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryMission(
                    challengeMissionHistoryId: challengeMissionHistoryId))),
          ))
          .first;
      MissionResponse missionResponse =
          MissionResponse.fromJson(response.data!);
      if (missionResponse.fetchMissionOfDay != null) {
        return ApiResult.success(data: missionResponse.fetchMissionOfDay!);
      } else {
        return ApiResult.success(data: MissionData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<DetailCartData>> getDetailCart() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryDetailCart())),
          ))
          .first;
      DetailCartData detailCartData = DetailCartData.fromJson(response.data!);
      if (detailCartData != null) {
        return ApiResult.success(data: detailCartData);
      } else {
        return ApiResult.success(data: DetailCartData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchOrderHistoryV2>> getListOrder(
      {OrderStatus? status, String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(getListOrderCourse(
                    orderStatus: status, nextToken: nextToken))),
          ))
          .first;
      ListOrderCourseData listOrderCourseData =
          ListOrderCourseData.fromJson(response.data!);
      if (listOrderCourseData.fetchOrderHistoryV2 != null) {
        return ApiResult.success(
            data: listOrderCourseData.fetchOrderHistoryV2!);
      } else {
        return ApiResult.success(data: FetchOrderHistoryV2());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchOrderHistoryByIdV2>> getDetailOrderCourse(
      {int? orderHistoryId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    getDetailOrder(orderHistoryId: orderHistoryId))),
          ))
          .first;
      DetailOrderData data = DetailOrderData.fromJson(response.data!);
      if (data.fetchOrderHistoryByIdV2 != null) {
        return ApiResult.success(data: data.fetchOrderHistoryByIdV2!);
      } else {
        return ApiResult.success(data: FetchOrderHistoryByIdV2());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CancelOrderReason>>> getListCancelOrderReason() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(fetchCancelOrderReason)),
          ))
          .first;
      List<CancelOrderReason> listCancelOrderReason = [];
      response.data?['fetchCancelOrderReason'].forEach(
          (e) => listCancelOrderReason.add(CancelOrderReason.fromJson(e)));
      return ApiResult.success(data: listCancelOrderReason);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<int>> getUnreadNotificationCount() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(unreadNotificationCount)),
          ))
          .first;
      return ApiResult.success(data: response.data!['notificationNo']);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<FetchPaymentTypes>>> getPayment() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(document: gqlLang.parseString(queryPayment())),
          ))
          .first;
      PaymentData paymentResponse = PaymentData.fromJson(response.data!);
      if (paymentResponse.fetchPaymentTypes != null) {
        return ApiResult.success(data: paymentResponse.fetchPaymentTypes!);
      } else {
        return ApiResult.success(data: paymentResponse.fetchPaymentTypes!);
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Object handleData(dynamic response, {String? key}) {
    dynamic data = response["data"];
    if (key != null) {
      return data[key];
    }
    return data;
  }

  Future<ApiResult<GetPrivateNotificationDetailById>>
      getDetailNotificationDciAdmin(int id) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryDetaiNotificationDciAdmin(id: id))),
          ))
          .first;
      GetPrivateNotificationDetailById detailNotification =
          GetPrivateNotificationDetailById.fromJson(
              response.data!['getPrivateNotificationDetailById']);
      return ApiResult.success(data: detailNotification);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchJoinedChannelsData>> getSearchUserChat(
      {String? nextToken,
      CommunityType? communityType,
      String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
              document: gqlLang.parseString(
                querySearchUserChat(
                    nextToken: nextToken,
                    communityType: communityType,
                    searchKey: searchKey),
              ),
            ),
          ))
          .first;
      FetchJoinedChannelsResponse fetchJoinedChannelsResponse =
          FetchJoinedChannelsResponse.fromJson(response.data!);
      if (fetchJoinedChannelsResponse.fetchJoinedChannels != null) {
        return ApiResult.success(
            data: fetchJoinedChannelsResponse.fetchJoinedChannels!);
      } else {
        return ApiResult.success(data: FetchJoinedChannelsData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchChatChannels>> getListChatChannel(
      {String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(getListJoinedChannel(nextToken: nextToken))),
          ))
          .first;
      ListJoinedChannelResponse joinedChannelResponse =
          ListJoinedChannelResponse.fromJson(response.data!);
      return ApiResult.success(
          data: joinedChannelResponse.fetchJoinedChannels!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<UserChannelGroups>>> getUserChannelGroup() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(getUserChannel())),
          ))
          .first;
      UserChannelGroupResponse userChannelGroupResponse =
          UserChannelGroupResponse.fromJson(response.data!);
      return ApiResult.success(
          data: userChannelGroupResponse.fetchUserChannelGroups!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<ProfileUserChatChannel>>> fetchProfilesByUserUuids(
      List<String> uuids) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryProfilesByUserUuids(channelUuid: uuids))),
          ))
          .first;
      ProfileChatUserData profileChatUser =
          ProfileChatUserData.fromJson(response.data!);
      return ApiResult.success(data: profileChatUser.fetchProfilesByUserUuids!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<ListUserGroup>>> fetchGroupMember(
      {String? nextToken, required int communityId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryGroupMember(
                    communityId: communityId, nextToken: nextToken))),
          ))
          .first;
      MemberGroup profileChatUser = MemberGroup.fromJson(response.data!);
      return ApiResult.success(data: profileChatUser.memberOfCommunity!.data!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CustomerSupportDTO>> fetchCustomerSupport() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryGetSupportChannelId)),
          ))
          .first;
      return ApiResult.success(
          data: CustomerSupportResponse.fromJson(response.data ?? {})
              .fetchCustomerSupport);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<TotalCount>> unreadCount(String userUuid) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryUnreadTotalCount(userUuid: userUuid))),
          ))
          .first;
      UnReadTotalData unReadTotalCount =
          UnReadTotalData.fromJson(response.data!);
      return ApiResult.success(data: unReadTotalCount.unReadTotalCount!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchTopics>> getListTopic({String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryTopic(nextToken: nextToken))),
          ))
          .first;
      ListTopicData listTopicData = ListTopicData.fromJson(response.data!);
      return ApiResult.success(data: listTopicData.fetchTopics!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchQuestions>> getListQuestion(
      {String? nextToken, List<int>? topicIds, String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryFetchQuestion(
                    nextToken: nextToken,
                    topicIds: topicIds,
                    searchKey: searchKey))),
          ))
          .first;
      ListQuestionData questionData = ListQuestionData.fromJson(response.data!);
      return ApiResult.success(data: questionData.fetchQuestions!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchQuestionById>> getDetailQuestion(
      {int? questionId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryDetailQuestion(questionId: questionId))),
          ))
          .first;
      QuestionData data = QuestionData.fromJson(response.data!);
      return ApiResult.success(data: data.fetchQuestionById!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<RelatedQuestions>> getRelatedQuestion(
      {List<int>? excludeIds, List<int>? topicIds}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryRelatedQuestion(
                    excludeIds: excludeIds, topicIds: topicIds))),
          ))
          .first;
      RelatedQuestionData data = RelatedQuestionData.fromJson(response.data!);
      return ApiResult.success(data: data.fetchQuestions!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchEvents>> getEvent(
      {String? searchKey,
      String? nextToken,
      String? priorityOrder,
      String? priceOrder,
      int? courseId,
      int? limit = Const.NETWORK_DEFAULT_LIMIT,
      int? exceptEventId,
      List<int>? topicIds,
      bool? isHot,
      int? couponId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryEvent(
                    nextToken: nextToken,
                    searchKey: searchKey,
                    priorityOrder: priorityOrder,
                    priceOrder: priceOrder,
                    limit: limit,
                    exceptEventId: exceptEventId,
                    topicIds: topicIds,
                    isHot: isHot,
                    couponId: couponId))),
          ))
          .first;
      EventData eventData = EventData.fromJson(response.data!);
      if (eventData.fetchEvents != null) {
        return ApiResult.success(data: eventData.fetchEvents!);
      } else {
        return ApiResult.success(data: FetchEvents());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchEventsData>> getEventDetail({
    int? eventId,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryEventDetail(eventId: eventId))),
          ))
          .first;
      DetailEventData detailEventResponse =
          DetailEventData.fromJson(response.data!);
      if (detailEventResponse.fetchEventById != null) {
        return ApiResult.success(data: detailEventResponse.fetchEventById!);
      } else {
        return ApiResult.success(data: FetchEventsData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchItemByProductIdV2>> getFetchDetailOrder({
    int? productId,
  }) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryItemByProductId(productId: productId))),
          ))
          .first;
      DetailOrderProductData data =
          DetailOrderProductData.fromJson(response.data!);
      if (data.fetchItemByProductIdV2 != null) {
        return ApiResult.success(data: data.fetchItemByProductIdV2!);
      } else {
        return ApiResult.success(data: FetchItemByProductIdV2());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchPointHistory>> getHistoryPoint(
      {String? nextToken, ActionTypePoint? actionTypePoint}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryHistoryPoint(
                    nextToken: nextToken, actionTypePoint: actionTypePoint))),
          ))
          .first;
      HistoryPointData data = HistoryPointData.fromJson(response.data!);
      if (data.fetchPointHistoryOfCurrentUser != null) {
        return ApiResult.success(data: data.fetchPointHistoryOfCurrentUser!);
      } else {
        return ApiResult.success(data: FetchPointHistory());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ExchangeRateData>> getExchangeRate() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryExchangeRate)),
          ))
          .first;
      ExchangeRateData data = ExchangeRateData.fromJson(response.data!);
      if (data.pointExchangeRate != null) {
        return ApiResult.success(data: data);
      } else {
        return ApiResult.success(data: ExchangeRateData());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PointActionData>> getPointActionIn() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryPointAction)),
          ))
          .first;
      PointActionData data = PointActionData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<GetLastAuditByOrderHistoryId>> getStatusOrder(
      int orderHistoryId) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryCheckStatusOrder(orderHistoryId: orderHistoryId))),
          ))
          .first;
      StatusOrderData data = StatusOrderData.fromJson(response.data!);
      return ApiResult.success(data: data.getLastAuditByOrderHistoryId!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PointActionOutData>> getPointActionOut() async {
    try {
      Response response = await link
          .request(Request(
            operation:
                Operation(document: gqlLang.parseString(queryPointActionOut)),
          ))
          .first;
      PointActionOutData data = PointActionOutData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PersonProfile>> getExitProfilePhone(String phone) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryCheckPhoneForgot(phone: phone))),
          ))
          .first;
      CheckPhoneForgotData data = CheckPhoneForgotData.fromJson(response.data!);
      return ApiResult.success(data: data.existsProfileByPhone!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchCoupons>> getListCoupons(
      {String? nextToken, int? limit}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryCoupons(nextToken: nextToken, limit: limit))),
          ))
          .first;
      CouponsData data = CouponsData.fromJson(response.data!);
      return ApiResult.success(data: data.fetchCoupons!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ProductTopicData>> getProductTopic(
      {String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryProductTopic(searchKey: searchKey))),
          ))
          .first;
      ProductTopicData data = ProductTopicData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  //chi tiết mã giảm giá
  Future<ApiResult<CouponDetail>> getDetailCoupons({int? couponId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryDetailCoupons(couponId: couponId))),
          ))
          .first;
      DetailCouponsData data = DetailCouponsData.fromJson(response.data!);
      return ApiResult.success(data: data.couponDetailGraph!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<PointCategoryData>> getPointActionCategories() async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(fetchPointActionCategories)),
          ))
          .first;
      PointCategoryData data = PointCategoryData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FetchProductBuyMore>> getBuyMore({int? couponId}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryCourseByMore(couponId: couponId))),
          ))
          .first;
      BuyMoreProduct data = BuyMoreProduct.fromJson(response.data!);
      return ApiResult.success(data: data.fetchProductBuyMore!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<EventTopicsData>> getEventTopics({String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document:
                    gqlLang.parseString(queryEventTopic(searchKey: searchKey))),
          ))
          .first;
      EventTopicsData data = EventTopicsData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CourseTopicsData>> getCourseTopics(
      {String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(queryCourseTopics(searchKey: searchKey))),
          ))
          .first;
      CourseTopicsData data = CourseTopicsData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<LibraryDataFetchDocuments>> getLibraryPage(
      {List<int>? categoryIds,
        List<int>?  topicIds,
        int? limit,
        bool? isLocked,
        bool? isPaging,
        String? nextToken,
        String? type,
        String? libraryDocumentType,
        String? privacy,
        String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
        operation: Operation(
            document: gqlLang
                .parseString(queryLibrary(
              libraryDocumentType: libraryDocumentType,
              limit: limit,
                nextToken: nextToken,
                searchKey: searchKey,type: type,isPaging:isPaging,categoryIds: categoryIds,topicIds: topicIds,privacy: privacy))),
      ))
          .first;
      LibraryResponse libraryResponse =
          LibraryResponse.fromJson(response.data!);
      if (libraryResponse.fetchDocumentsByType != null) {
        return ApiResult.success(data: libraryResponse.fetchDocumentsByType!);
      } else {
        return ApiResult.success(data: LibraryDataFetchDocuments());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<categoriesLibraryData>> getCategoryLibrary(
      {String? searchKey, String? type}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(
                    queryCategoryLibrary(searchKey: searchKey, type: type))),
          ))
          .first;
      categoriesLibraryData data =
          categoriesLibraryData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<documentTopicsData>> getTopicLibrary(
      {String? searchKey}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang
                    .parseString(getDocumentTopics(searchKey: searchKey))),
          ))
          .first;
      documentTopicsData data = documentTopicsData.fromJson(response.data!);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<DocumentCategories>> getDocumentByCategory(
      {String? searchKey, String? type, String? nextToken,List<int>? categoryIds}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(getDocumentByCategories(categoryIds: categoryIds,
                    searchKey: searchKey, type: type, nextToken: nextToken))),
          ))
          .first;
      DocumentCategories data = DocumentCategories.fromJson(response.response);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CategoriesByLanguage>> getCategoryStory(
      {String? searchKey, String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
            operation: Operation(
                document: gqlLang.parseString(queryCategoryStory(
                    searchKey: searchKey, nextToken: nextToken))),
          ))
          .first;
      CategoryStoryData data = CategoryStoryData.fromJson(response.data!);
      return ApiResult.success(data: data.categoriesByLanguage!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FilterPostTopic>> getPostStory(
      {String? searchKey, String? nextToken}) async {
    try {
      Response response = await link
          .request(Request(
        operation: Operation(
            document: gqlLang.parseString(queryPostStory(
                searchKey: searchKey, nextToken: nextToken))),
      ))
          .first;
      PostStoryData data = PostStoryData.fromJson(response.data!);
      return ApiResult.success(data: data.filterPostTopic!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }
}
