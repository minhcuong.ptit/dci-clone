import 'dart:io';

import 'package:dio/dio.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:imi/src/data/network/repository/base_repository.dart';
import 'package:imi/src/data/network/request/admin_update_community_request.dart';
import 'package:imi/src/data/network/request/apply_coupon_request.dart';
import 'package:imi/src/data/network/request/association_invite_club_request.dart';
import 'package:imi/src/data/network/request/association_update_invite_club_request.dart';
import 'package:imi/src/data/network/request/bill_request.dart';
import 'package:imi/src/data/network/request/change_language_request.dart';
import 'package:imi/src/data/network/request/checkout_request.dart';
import 'package:imi/src/data/network/request/clean_seeds_request.dart';
import 'package:imi/src/data/network/request/comment_request.dart';
import 'package:imi/src/data/network/request/create_form_request.dart';
import 'package:imi/src/data/network/request/create_order_request.dart';
import 'package:imi/src/data/network/request/create_zen_request.dart';
import 'package:imi/src/data/network/request/event_free_request.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/request/group_admin_request.dart';
import 'package:imi/src/data/network/request/group_member_request.dart';
import 'package:imi/src/data/network/request/kis_enroll_request.dart';
import 'package:imi/src/data/network/request/login_request.dart';
import 'package:imi/src/data/network/request/logout_request.dart';
import 'package:imi/src/data/network/request/member_action_request.dart';
import 'package:imi/src/data/network/request/mission_request.dart';
import 'package:imi/src/data/network/request/notification_chat_request.dart';
import 'package:imi/src/data/network/request/payment_request.dart';
import 'package:imi/src/data/network/request/post_request.dart';
import 'package:imi/src/data/network/request/practice_time_setting_request.dart';
import 'package:imi/src/data/network/request/reaction_request.dart';
import 'package:imi/src/data/network/request/report_request.dart';
import 'package:imi/src/data/network/request/seen_message_request.dart';
import 'package:imi/src/data/network/request/submit_favorite_request.dart';
import 'package:imi/src/data/network/request/time_token_read_messages_request.dart';
import 'package:imi/src/data/network/request/update_community_request.dart';
import 'package:imi/src/data/network/request/update_member_request.dart';
import 'package:imi/src/data/network/request/update_member_role_request.dart';
import 'package:imi/src/data/network/request/update_order_items_request.dart';
import 'package:imi/src/data/network/request/update_profile_request_v2.dart';
import 'package:imi/src/data/network/request/upgrade_form_request.dart';
import 'package:imi/src/data/network/response/association_club.dart';
import 'package:imi/src/data/network/response/bill_response.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/checkout_order.dart';
import 'package:imi/src/data/network/response/country.dart';
import 'package:imi/src/data/network/response/create_order_response.dart';
import 'package:imi/src/data/network/response/create_zen_response.dart';
import 'package:imi/src/data/network/response/detail_package_response.dart';
import 'package:imi/src/data/network/response/e_card_data.dart';
import 'package:imi/src/data/network/response/form_response.dart';
import 'package:imi/src/data/network/response/image_seed_data.dart';
import 'package:imi/src/data/network/response/kis_province_list.dart';
import 'package:imi/src/data/network/response/login_response.dart';
import 'package:imi/src/data/network/response/member_dto.dart';
import 'package:imi/src/data/network/response/my_individual.dart';
import 'package:imi/src/data/network/response/payment_online_response.dart';
import 'package:imi/src/data/network/response/request_to_join.dart';
import 'package:imi/src/data/network/response/send_comment_response.dart';
import 'package:imi/src/data/network/response/sowing_diary_response.dart';
import 'package:imi/src/data/network/response/start_challenge_response.dart';
import 'package:imi/src/data/network/response/target_response.dart';
import 'package:imi/src/data/network/response/upload_list_url_response.dart';
import 'package:imi/src/data/network/response/upload_url_response.dart';
import 'package:imi/src/data/network/service/api_result.dart';
import 'package:imi/src/data/network/service/network_exceptions.dart';
import 'package:imi/src/utils/app_config.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

import '../request/buy_more_request.dart';
import '../request/change_status_request.dart';
import '../request/profile_request.dart';
import '../request/update_password_request.dart';
import '../response/level_data.dart';
import '../response/profile.dart';
import '../service/app_client.dart';

class AppRepository extends BaseRepository {
  Future<ApiResult<dynamic>> updateProfile(
      UpdateProfileRequestV2 request) async {
    try {
      dynamic response = await appClient.updateProfile(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<LoginResponse>> login(LoginRequest request) async {
    try {
      LoginResponse response = await appClient.login(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> auth(LoginRequest request) async {
    try {
      dynamic response =
          await appClient.auth(AppConfig.environment.authEndpoint, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e, forceLogout: false);
    }
  }

  Future<ApiResult<UploadUrlResponse>> getUploadUrl(String fileName) async {
    try {
      UploadUrlResponse response = await appClient.getUploadUrl(fileName);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<UploadListUrlResponse>>> getListUploadUrl(
      List<String> fileNames,
      {String? moduleName,
      String? channelPubnubId,
      bool? isPublic}) async {
    try {
      List<UploadListUrlResponse> response = await appClient.getListUploadUrl({
        "fileNames": fileNames,
        "moduleName": moduleName,
        "channelPubnubId": channelPubnubId,
        "isPublic": isPublic
      });
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<Response<dynamic>>> uploadImage(
      String url, File file) async {
    try {
      final response = await dio.put(
        url,
        data: file.openRead(),
        options: Options(
          contentType: 'application/octet-stream', // set right content-type
          headers: {
            HttpHeaders.contentLengthHeader: file.lengthSync(),
            // set content-length
          },
        ),
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> logout(LogoutRequest request) async {
    try {
      dynamic response =
          await appClient.logout(AppConfig.environment.logoutEndpoint, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateCommunity(
      UpdateCommunityRequest request) async {
    try {
      dynamic response = await appClient.updateCommunity(request.id, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> submitFavorite(
      SubmitFavoriteRequest request) async {
    try {
      await appClient.submitFavorite(request);
      return ApiResult.success(data: "");
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<ECardData>>> getListECard(int membershipId) async {
    try {
      List<ECardData> response = await appClient.getListECard(membershipId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<ECardData>>> getDetailPackage(
      String? packageCode) async {
    try {
      List<ECardData> response = await appClient.getDetailPackage(packageCode);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<FormResponse>> createForm(CreateFormRequest request) async {
    try {
      FormResponse response = await appClient.createForm(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<BillResponse>> upgradeForm(
      UpgradeFormRequest request) async {
    try {
      BillResponse response = await appClient.upgradeForm(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<BillResponse>> renewForm(UpgradeFormRequest request) async {
    try {
      BillResponse response = await appClient.upgradeForm(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MemberDTO>> memberAction(MemberActionRequest request) async {
    try {
      MemberDTO response = await appClient.memberAction(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> createPost(PostRequest request) async {
    try {
      dynamic response = await appClient.createPost(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> editPost(
      PostRequest request, String postId) async {
    try {
      dynamic response = await appClient.editPost(request, postId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<BillResponse>> bill(BillRequest request) async {
    try {
      BillResponse response = await appClient.getBill(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<MyIndividual>> myIndividual() async {
    try {
      MyIndividual response = await appClient.myIndividual();
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<DetailPackageResponse>> getDetailUserPackage(
      int userPackageId) async {
    try {
      DetailPackageResponse response =
          await appClient.getDetailUserPackage(userPackageId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<SendCommentResponse>> comment(int postId, String text,
      {int? parentId}) async {
    try {
      SendCommentResponse response = await appClient.comment(
          CommentRequest(postId: postId, text: text, parentId: parentId));
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> markNotificationRead(
      int? id, String? readAction) async {
    try {
      dynamic response = await appClient.markNotificationRead(id, readAction);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> markAllAsRead() async {
    try {
      dynamic response = await appClient.markNotificationsRead();
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<bool> downloadPdf(String pdfUrl, String savePath) async {
    final store = await getApplicationDocumentsDirectory();
    File file = File('${store.path}/${savePath}');
    try {
      Response response = await dio.get(
        pdfUrl,
        options: Options(
          responseType: ResponseType.bytes,
          followRedirects: false,
        ),
      );
      var raf = file.openSync(mode: FileMode.write);
      raf.writeFromSync(response.data);
      await raf.close();
      OpenFile.open(file.path);
      return true;
      // return const ApiResult.success(data: true);
    } catch (e) {
      return false;
    }
  }

  Future<ApiResult<dynamic>> follow(FollowRequest request) async {
    try {
      dynamic response = await appClient.follow(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ECardData>> getAddOnByType(String type) async {
    try {
      ECardData response = await appClient.getAddOnByType(type);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<KisProvinceList>> getListKis(int provinceId) async {
    try {
      KisProvinceList response = await appClient.getListKis(provinceId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CheckoutOrder>> enrollKis(KisEnrollRequest request) async {
    try {
      dynamic response = await appClient.enrollKis(request);
      dynamic checkoutOrderResponse = response['checkoutOrder'];
      return ApiResult.success(
          data: CheckoutOrder.fromJson(checkoutOrderResponse));
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> removeKis(List<int> kisProvinceIds) async {
    try {
      dynamic response = await appClient.removeKis(kisProvinceIds);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> removePost(int postId) async {
    try {
      dynamic response = await appClient.removePost(postId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> removeComment(
      int commentId, int communityId) async {
    try {
      dynamic response = await appClient.removeComment(commentId, communityId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateMember(
      int id, UpdateMemberRequest request) async {
    try {
      dynamic response = await appClient.updateMember(id, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateMemberRole(
      UpdateMemberRoleRequest request) async {
    try {
      dynamic response = await appClient.updateMemberRole(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> reaction(ReactionRequest request) async {
    try {
      dynamic response = await appClient.reaction(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> bookmark(ReactionRequest request) async {
    try {
      dynamic response = await appClient.bookmark(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<AssociationClub>> inviteClubToAssociation(
      AssociationInviteClubRequest request) async {
    try {
      AssociationClub response =
          await appClient.inviteClubToAssociation(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateInviteClubToAssociation(
      int id, AssociationUpdateInviteClubRequest request) async {
    try {
      dynamic response =
          await appClient.updateClubAssociationInvite(id, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<String>> checkoutPayment(CheckoutRequest request) async {
    try {
      dynamic response = await appClient.checkoutPayment(request);
      return ApiResult.success(data: response.toString());
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<String>> requestToJoinGroup(GroupMemberActionRequest request,
      {MemberActionType? memberActionType}) async {
    try {
      dynamic response = await appClient.groupMemberAction(
          memberActionType?.name ?? MemberActionType.JOIN.name, request);
      return ApiResult.success(data: response.toString());
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<RequestToJoin>> getRequestsToJoinList(
      int communityId, int page) async {
    try {
      RequestToJoin res =
          await appClient.getListRequestToJoin(communityId, page);
      return ApiResult.success(data: res);
    } on Exception catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> approveRequest(
      AdminAction action, int communityId, String userUuid) async {
    try {
      dynamic res =
          await appClient.approveRequest(action.name, userUuid, communityId);
      return ApiResult.success(data: res);
    } on Exception catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> approveInvitation(
      AdminAction action, int communityId, String userUuid) async {
    try {
      dynamic res = await appClient.acceptInvitation(action.name,
          GroupMemberActionRequest(userUuid: [userUuid], clubId: communityId));
      return ApiResult.success(data: res);
    } on Exception catch (e) {
      return handleErrorApi(e);
    }
  }

  // Future<ApiResult<MemberListResponse>> getMembersList(
  //     int communityId, int page) async {
  //   try {
  //     final res = await appClient.getMembersList(communityId, page);
  //     return ApiResult.success(data: res);
  //   } on Exception catch (e) {
  //     return handleErrorApi(e);
  //   }
  // }

  Future<ApiResult<List<Profile>>> getProfileByPhone(String phoneNumber) async {
    try {
      List<Profile> response = await appClient.getProfileByPhone(phoneNumber);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<Country>>> getCountries() async {
    try {
      List<Country> response = await appClient.getCountries();
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> profileUpdate(
      String userUuid, ProfileRequest request) async {
    try {
      dynamic response = await appClient.profileUpdate(userUuid, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateComment(CommentRequest request) async {
    try {
      dynamic response = await appClient.updateComment(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> changePinCode(
      String userUuid, ChangePasswordRequest request) async {
    try {
      dynamic response = await appClient.changePinCode(userUuid, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateDeleteMember(
      {required String memberAction,
      required String userUuid,
      required List<String>? roles,
      required int communityId}) async {
    try {
      dynamic response = await appClient.updateDeleteMember(
          memberAction,
          GroupAdminRequest(communityId: communityId, members: [
            GroupMemberRequest(role: roles ?? [], userUuid: userUuid)
          ]));
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<LevelData>>> getLevels() async {
    try {
      List<LevelData> response = await appClient.getLevels();
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<CategoryData>>> getCategories(String types) async {
    try {
      final response = await appClient.getCategories(types);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> changeLanguage(
      ChangeLanguageRequest request) async {
    try {
      dynamic response = await appClient.changeLanguage(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> createCleanSeed(CleanSeedsRequest request) async {
    try {
      dynamic response = await appClient.createCleanSeed(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateCleanSeed(
      int cleanSeedId, CleanSeedsRequest request) async {
    try {
      dynamic response = await appClient.updateCleanSeed(cleanSeedId, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> deleteCleanSeed(int cleanSeedId) async {
    try {
      dynamic response = await appClient.deleteCleanSeed(cleanSeedId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> createSeed(SeedingDiary request) async {
    try {
      dynamic response = await appClient.createSeed(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<List<TargetData>>> getTarget() async {
    try {
      TargetResponse response = await appClient.getTarget();
      if (response.data != null) {
        return ApiResult.success(data: response.data!);
      } else {
        return ApiResult.failure(error: NetworkExceptions.unableToProcess());
      }
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updatePlanSeed(
      int plantSeedHistoryId, SeedingDiary request) async {
    try {
      dynamic response =
          await appClient.updatePlanSeed(plantSeedHistoryId, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> reportPost(ReportRequest request,
      {String? postId}) async {
    try {
      dynamic response = await appClient.reportPost(postId ?? "", request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> reportUser(ReportRequest request,
      {String? userUuid}) async {
    try {
      dynamic response = await appClient.reportUser(userUuid ?? "", request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> deletePlanSeed(int plantSeedHistoryId) async {
    try {
      dynamic response = await appClient.deletePlanSeed(plantSeedHistoryId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> copyPlanSeed(
      int plantSeedHistoryId, SeedingDiary request) async {
    try {
      dynamic response =
          await appClient.copyPlanSeed(plantSeedHistoryId, request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> deletePost(
      {String? postId, String? reportPostAction}) async {
    try {
      dynamic response =
          await appClient.deletePost(postId ?? "", reportPostAction ?? "");
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ImageSeedData>> getImageSeed(
      {int? plantSeedHistoryId}) async {
    try {
      ImageSeedData response =
          await appClient.getImageSeed(plantSeedHistoryId ?? 0);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> blockUser(
      {String? userUuid, String? action}) async {
    try {
      dynamic response =
          await appClient.blockUser(userUuid ?? "", action ?? "");
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<ImageSeedData>> getImageCleanSeed(
      {int? cleanSeedHistoryId}) async {
    try {
      ImageSeedData response =
          await appClient.getImageCleanSeed(cleanSeedHistoryId ?? 0);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<StartChallengeResponse>> getStartPractice(
      {int? challengeId}) async {
    try {
      dynamic response = await appClient.startPractice(challengeId ?? 0);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> getPracticeTimeSetting(
      PracticeTimeSettingRequest request) async {
    try {
      dynamic response = await appClient.getPracticeTimeSetting(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CreateZenResponse>> createZen(
      CreateZenRequest request) async {
    try {
      final data = await appClient.createZen(request);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> getMission(MissionRequest request,
      {int? challengeId, int? challengeHistoryId}) async {
    try {
      final data = await appClient.getMission(
          request, challengeId ?? 0, challengeHistoryId ?? 0);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> cancelMission(
      {int? challengeId, int? challengeHistoryId}) async {
    try {
      final data = await appClient.cancelMission(
          challengeId ?? 0, challengeHistoryId ?? 0);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> cleanAccount() async {
    try {
      final data = await appClient.deleteCleanAccount();
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> orderCourse(EventFreeRequest request) async {
    try {
      dynamic response = await appClient.orderCourse(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> deleteOrderItem(
      {int? orderId, int? orderDetailId}) async {
    try {
      final data =
          await appClient.deleteOrderItem(orderId ?? 0, orderDetailId ?? 0);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> applyCoupon(ApplyCouponRequest request,
      {int? orderId}) async {
    try {
      final data = await appClient.applyCoupon(request, orderId ?? 0);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateOrderQuantity(
      {int? orderId, int? orderDetailId, int? quantity}) async {
    try {
      final data = await appClient.updateOrderQuantity(
          orderId ?? 0, orderDetailId ?? 0, quantity ?? 0);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> deleteCoupon({
    int? orderId,
    int? couponId,
  }) async {
    try {
      final data = await appClient.deleteCoupon(orderId ?? 0, couponId ?? 0);
      return ApiResult.success(data: data);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<CreateOrderResponse>> createOder(
      CreateOrderRequest request) async {
    try {
      dynamic response = await appClient.createOder(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateOrderItem(UpdateOrderItemRequest request,
      {int? orderId, int? orderDetailId}) async {
    try {
      dynamic response = await appClient.updateOrderItem(
          request, orderId ?? 0, orderDetailId ?? 0);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> cancelOrderHistory(
      {required int orderId, required int cancelReasonId}) async {
    try {
      dynamic response =
          await appClient.cancelOrderHistory(orderId, cancelReasonId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> adminupdateCommunity(
      AdminUpdateCommunity request, communityId) async {
    try {
      dynamic response =
          await appClient.adminUpdateCommunity(request, communityId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> createPrivateChannel(String userUID) async {
    try {
      dynamic response = await appClient.createPrivateChannel(userUID);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> createSupportChat() async {
    try {
      dynamic response = await appClient.createSupportChat();
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> updateMessageSeen(
      String channelId, int timeToken, String chatType) async {
    try {
      dynamic response = await appClient.updateMessageSeen(SeenMessageRequest(
          channelId: channelId, timeToken: timeToken, chatType: chatType));
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<bool> downLoadImage(String url) async {
    if (url != null) {
      var imageId = await ImageDownloader.downloadImage(url );
      if (imageId != null) {
        return true;
      }
    }
    return false;
  }

  Future<ApiResult<dynamic>> timeTokenRead(TimeTokenReadRequest request) async {
    try {
      dynamic response = await appClient.timeTokenReadMessages(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> getGroupChannel(int communityId) async {
    try {
      dynamic response = await appClient.getGroupChannel(communityId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<AdminUpdateCommunity>> getGroupInfo(int communityId) async {
    try {
      AdminUpdateCommunity response = await appClient.getInfoGroup(communityId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> notificationChat(
      NotificationChatRequest request) async {
    try {
      dynamic response = await appClient.notificationChat(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> changeStatus(
      ChangeStatusRequest request,String supportChannelId) async {
    try {
      dynamic response = await appClient.changeStatusSupportChat(supportChannelId,request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> orderFree(
      EventFreeRequest request) async {
    try {
      dynamic response = await appClient.orderEventFree(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> togglePayment(
      int orderId) async {
    try {
      dynamic response = await appClient.togglePayment(orderId);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<AlepayResponse>> paymentOnline(
      PaymentRequest request) async {
    try {
      PaymentOnlineResponse response = await appClient.paymentOnline(request);
      return ApiResult.success(data: response.alepayResponse!);
    } catch (e) {
      return handleErrorApi(e);
    }
  }

  Future<ApiResult<dynamic>> buyMore(
      BuyMoreRequest request) async {
    try {
      dynamic response = await appClient.buyMore(request);
      return ApiResult.success(data: response);
    } catch (e) {
      return handleErrorApi(e);
    }
  }
}
