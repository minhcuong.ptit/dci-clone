import 'dart:convert';

import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';

const queryMe = """{
    me {
        accumulatedPoints
        userUuid
        address
        birthdate
        bloodType
        email
        fullName
        gender
        picture
        province
        status
        username
        birthPlace
    }
}""";

const myProfile = """{
    profile {
        userUuid
        avatarUrl
        fullName
        gender
        email
        phone
        status
        birthday
        provinceName
        intro
        community {
            id
            location
            communityInfo{
             communityLevel {
                communityId
                levelName
                level
                year
               } 
            communityRole  
          }
        }
        districtId
        provinceId
        countryId
        workingUnit
        currentPosition
    }
}""";

const queryMeStatus = """{
    me {
        status
        userUuid
        fullName
    }
}""";

String queryBanks(String name) => """{
    banks(name: "$name") {
        id
        name
        code
    }
}""";

String queryCategories(String type) => """{
    categories(type: "$type") {
        id
        name
        color
        order
        emoji
    }
}""";

String queryMemberByPhone(String phone) => """{
    membershipProfile(phoneOrName: "${phone}") {
        isEnable
        phone
        status
        profile {
            userUuid
            fullName
            picture
            username
        }
    }
}""";

const queryCommunities = """{
    communities (input: {} ){
        data {
           id 
           name
           type
           avatarUrl
           followerNo
           imageUrl
           memberNo
           ownerId
        }
    }
}""";

String queryContent(String type, List<int> categoryId) => """{
    categories(type: "$type") {
        id
        name
        color
        order
        emoji
    }
    recommendCommunities(
    token: { nextToken: null, limit: 10 }
    criteria: { 
      typeIn: [STUDY_GROUP, BASIC_GROUP],
      categories: $categoryId }
    ) {
    data {
      id
      name
      type
      avatarUrl
      communityInfo {
        followerNo
        memberNo
      }
    }
  }
}""";

String queryRecommendCommunity(
        List<String> listCommunityTypeIn, List<int> categoryId) =>
    """{
    recommendCommunities(
    token: { nextToken: null, limit: 10 }
    criteria: { typeIn: $listCommunityTypeIn,categories: $categoryId }
    ) {
    data {
      id
      name
      type
      avatarUrl
      communityInfo {
        followerNo
        memberNo
      }
      userView {
            isFollowing
        }
      ownerProfile {
        userUuid
      }
    }
  }
}""";

String queryDCICommunities(CommunityType type,
        {CommunityStatus? status,
        int? limit,
        String? nextToken,
        String? searchKey,
        bool? isValid,
        int? communityId,
        List<String>? dayOfWeek,
        int? startTime,
        int? endTime,
        List<int>? levelIds}) =>
    """{
    dciCommunities(
    token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: ${limit}
        }
    criteria: { communityType: [${type.name}],
     communityStatus:${status?.name}, 
     searchKey: ${searchKey == null ? null : jsonEncode(searchKey)},
      isValid: ${isValid}
      communityId: ${communityId}
      levelIds:${levelIds == null ? [] : jsonEncode(levelIds)},
      daysOfWeek:${dayOfWeek},
      startTime:${startTime},
      endTime:${endTime}
      }
    ) {
    nextToken
    data {
        avatarUrl
        categories {
            color
            emoji
            id
            name
            order
        }
        communityInfo {
            communityLevel {
                communityId
                level
                year
                levelName
            }
            followerNo
            followingNo
            memberNo
            postNo
        }
        description
        id
        imageUrl
        location
        memberOfCommunities {
            name
            id
            description
        }
        name
        ownerProfile {
            userUuid
            provinceName
            status
        }
        type
        ownerProfile {
            userUuid
        }
        groupView {
            event {
                name
                startDate
                endDate
                dayOfWeek
                startTime
                endTime
            }
            bomMembers {
                role
                bomMembers {
                    avatarUrl
                    userUuid
                    phone
                    fullName
                    communityId
                }
            }
            myRoles
            request {
                isRequest
                inviter {
                    userUuid
                    avatarUrl
                    fullName
                }
            }
        }
    }
  }
}""";

const queryProvinces = """{
    address(activeClub: false, addressType: PROVINCE) {
        id
        name
        parentId
    }
}""";

String queryCities(int parentId) => """{
    address(addressType: CITY, parentId: $parentId) {
        id
        name
        clubNumber
        parentId
    }
}""";

String queryDistricts(int parentId) => """{
    address(addressType: DISTRICT, parentId: $parentId) {
        id
        name
        clubNumber
        parentId
    }
}""";

String queryWards(int parentId) => """{
    address(addressType: WARD, parentId: $parentId) {
        id
        name
        clubNumber
        parentId
    }
}""";

String queryCommunityV2(int communityId) => """{
    communityV2(communityId: $communityId) {
        dynamicLink
        imageUrl
        avatarUrl
        description
        location
        privacy
        socialLinks {
            name
            url
        }
        categories {
            color
            emoji
            id
            name
            order
        }
        communityInfo {
            followerNo
            followingNo
            memberNo
            postNo
            communityLevel {
                communityId
                level
                levelName
                levelStatus
                year
            }
            communityRole
        }
        id
        memberOfCommunities {
            name
            id
            groupView {
                myRoles
            }
        }
        name
        ownerProfile {
            intro
            userUuid
            provinceName
            countryName
            workingUnit
            numberOfLeadGroup
            currentPosition
            achievement
            leaderType
            point
        }
        type
        userView {
            associationNamePromotor
            clubNamePromotor
            isFollowing
            numberPending
            joinedGroups {
                group {
                    name
                    id
                }
                userRoles
            }
        }
        groupView {
            event {
                name
                startDate
                endDate
                dayOfWeek
                startTime
                endTime
            }
            bomMembers {
                role
                bomMembers {
                    avatarUrl
                    userUuid
                    phone
                    fullName
                    communityId
                }
            }
            myRoles
            request {
                isRequest
                inviter {
                    userUuid
                    avatarUrl
                    fullName
                }
            }
        }
    }
}""";

String queryFollows(
        {int? communityId,
        String? type,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{
    followCommunities(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
        criteria: {
            type: $type
            communityId: $communityId
        }
    ){
        nextToken
        data {
            id
            name
            followerNo
            memberNo
            avatarUrl
            imageUrl
            star
            type 
            location
            myCommunityContent{
                isFollowing
                isMember
                clubRole
            }
        }
    }
}""";

String queryFollowsV2(
        {int? communityId,
        String? type,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{
    followCommunitiesV2(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
        criteria: {
            type: $type
            communityId: $communityId
        }
    ){
        nextToken
        data {
            communityInfo {
                followerNo
                followingNo
                memberNo
                postNo
            }
            id
            name
            avatarUrl
            imageUrl
            memberOfCommunities {
                id
                avatarUrl
                imageUrl
                name
                type
            }
              userView {
                isFollowing
                numberPending
                clubNamePromotor
                associationNamePromotor
            }
            type 
            location
            groupView{
            myRoles
            }
        }
    }
}""";

String queryLikes(
        {int? postId,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{
    communitiesLikePost(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
        postId: $postId
    ){
        nextToken
        data {
            id
            name
            avatarUrl
            type
            userView {
              associationNamePromotor
              clubNamePromotor
              isFollowing
              numberPending
          }
        }
    }
}""";

String queryLikesOld(
        {int? postId,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{
    communitiesLikePost(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
        postId: $postId
    ){
        nextToken
        data {
            id
            name
            followerNo
            memberNo
            avatarUrl
            imageUrl
            star
            type 
            location
            myCommunityContent{
                isFollowing
                isMember
                clubRole
            }
            
        }
    }
}""";

const queryMyProfile = """{
    myCommunity {
        id
        imageUrl
        avatarUrl
        information {
            id
            bankHolderName
            bankName
            bankNumber
            clubName
            personInCharge
            personInChargeName
        }
        followerNo
        memberNo
        star
        name
        ownerId
        type
    }
    me {
        accumulatedPoints
        userUuid
        address
        birthdate
        bloodType
        email
        fullName
        gender
        picture
        province
        status
        username
        birthPlace
        userPoint
        referralCode
    }
}""";

String queryHome(
        {required bool? myPost,
        String? nextToken,
        String? typeCategory,
        List<int>? listCategory}) =>
    """{
    posts(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: ${Const.LIMIT}
        }
        criteria: {
            myPost: $myPost
            favCategories: $listCategory
        }
    ) {
        nextToken
        data {
            id
            fullName
            avatar
            userUuid
            posterCommunityId
            communityName
            communityAvatar
            text
            textColor
            backgroundImage
            backgroundImages
            backgroundColor
            type
            privacy
            createdDate
            modifiedDate
            likeNumber
            commentNumber
            shareNumber
            description
            isLiked
            isBookmarked
            communityV2 {
                id
                name 
                avatarUrl
                type
            } 
            dynamicLink
        }
    }
    categories(type: "$typeCategory") {
        id
        name
        color
        emoji
    }
}""";

String queryClub(int provinceId,
        {String? searchTerm = null,
        String? nextToken = null,
        int limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    searchTerm != null && searchTerm.isNotEmpty
        ? """{
    communities(input: {provinceId: $provinceId, nameFullText: ${jsonEncode(searchTerm)}, types: ["CLUB"], nextToken: ${jsonEncode(nextToken)}, limit: $limit}) {
       nextToken
       data {
           id
           name
           followerNo
           avatarUrl
           imageUrl
           ownerId
       }
    }
}"""
        : """{
    communities(input: {provinceId: $provinceId, types: ["CLUB"], nextToken: ${jsonEncode(nextToken)}, limit: $limit}) {
       nextToken
       data {
           id
           name
           followerNo
           avatarUrl
           imageUrl
           ownerId
       }
    }
}""";

String querySearch(
        {required String text,
        String? nextToken = null,
        int limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """{
    communities(input: {nextToken: ${jsonEncode(nextToken)}, limit: $limit, nameFullText: "$text"}) {
       nextToken
       data {
           id
           name
           type
           followerNo
           memberNo
           avatarUrl
           imageUrl
           ownerId
           location
           star
       }
    }
}""";

String getQueryPost({
  bool? favPost = false,
  String? nextToken,
  String? nextTokenReport,
  int? communityId,
  List<String>? listCommunityTypeIn,
  String? postStatus,
  int limit = Const.LIMIT,
}) =>
    """{
    posts(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
            limit: ${limit}
        }
        criteria: {
            status:$postStatus
            myPost: ${false}
            communityId: $communityId,
            favPosts: $favPost,
            communityTypeIn: ${listCommunityTypeIn ?? []}
        }
    ) {
        nextToken
        data {
            id
            fullName
            avatar
            userUuid
            posterCommunityId
            communityName
            communityAvatar
            text
            textColor
            backgroundImage
            backgroundImages
            backgroundColor
            type
            privacy
            createdDate
            modifiedDate
            likeNumber
            commentNumber
            shareNumber
            isBookmarked
            isLiked
            description
            communityV2 {
                id
                name 
                avatarUrl
                type
            } 
            dynamicLink
            reportedPostInfo(
               token: {
            nextToken: ${nextTokenReport == null ? null : jsonEncode(nextTokenReport)},
            limit: ${Const.NETWORK_DEFAULT_LIMIT}
                      }
            ){
            nextToken
            total
            data {
                 avatarUrl
                 createdDate
                 fullName
                 id
                 reason {
                 id
                 reason
                 reasonType
                 }
                 reporterUserUuid
            }
            }
        }
    }
}""";

String getQueryPostDetail({required int? postId}) => """{
    postById(postId: $postId) {
            id
            fullName
            avatar
            userUuid
            posterCommunityId
            communityName
            communityAvatar
            text
            textColor
            backgroundImage
            backgroundImages
            backgroundColor
            type
            privacy
            createdDate
            modifiedDate
            likeNumber
            commentNumber
            shareNumber
            isBookmarked
            isLiked
            description
            communityV2 {
                id
                name 
                avatarUrl
                type
                groupView {
                    myRoles
                }
            } 
            dynamicLink
    }
}""";

String queryComment(
        {int? limit = Const.NETWORK_DEFAULT_LIMIT,
        int? parentId,
        String? nextToken,
        int? postId}) =>
    """{
    comments(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
        criteria: {
            postId: $postId,
            parentId: $parentId
        }
    ) {
        nextToken
        hasNext
        data {
            id
            text
            avatar
            createdDate
            fullName
            ownerProfile {
                userUuid
            }
            replies(
                token: {
                    nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
                    limit: $limit
                }
                criteria: {
                    postId: $postId,
                    parentId: $parentId
                }
            ) {
                nextToken
                hasNext
                data {
                    id
                    text
                    avatar
                    createdDate
                    fullName
                    ownerProfile {
                        userUuid
                    }
                }
            }
        }
    }
}""";

String getQueryMedia(
        {String? nextToken,
        required int communityId,
        int limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """
  {
    communityMedia(token: {
            nextToken: "$nextToken",
            limit: $limit
        }, communityId: $communityId ) {
        nextToken
        data {
            id
            communityId
            postId
            s3Key
        }
    }
}

""";

const queryMyMembership = """
query {
    myMembership {
        membershipType,
        userPackageId,
        packageName,
        packageCode,
        packageId,
        communityId,
        name,
        status,
        clubStatus,
        clubName,
        name,
        defaultAvatar,
        ktaNumber,
        informationId,
        expiredDate,
    }
    memberships {
        id
        name
        type
        code
        icon
    }
}
""";

const queryMemberships = """
{
  memberships {
    id
    name
    code
    icon
    description
    packageActive {
      packageStatus
      packageInfo {
        iconUrl
        part
        name
      }
      expiredDate
    }
  }
}
""";

String queryGetPackage({int? membershipId}) => """
{
  packages(membershipId: $membershipId) {
    packageInfo {
      id
      iconUrl
      name
      packageDescriptions {
        title
        type
        description
      }
    }
    packageStatus
    expiredDate
  }
}
""";

String queryMembershipInfo(
        {int? membershipId, MembershipType? membershipType}) =>
    """{
membershipInformation(membershipId: $membershipId, membershipType: ${membershipType?.name}) {
                        address
                        artDocumentLinks
                        artDocuments
                        associationId
                        bankHolderName
                        bankName
                        bankNumber
                        certDocumentLinks
                        certDocuments
                        cityId
                        cityName
                        clubCategories
                        clubCategoriesLink
                        clubJoin
                        clubName
                        clubStatus
                        clubProvince
                        clubProvinceName
                        coverPicture
                        districtId
                        districtName
                        documentLinks
                        documents
                        eCertificateNumber
                        expiredDate
                        id
                        imiPaid
                        ktaNumber
                        memberStatus
                        memberRole
                        members {
                          id
                          communityId
                          clubId
                          userUuid
                          roles
                          phone
                          name
                          nikNumber
                          ktaNumber
                          status
                          title
                          createdDate
                          modifiedDate
                          avatarUrl
                          provinceName
                          packageStatus
                        }
                        permission
                        personInCharge
                        personInChargeName
                        picPhoneNumber
                        provinceId
                        provinceName
                        rtRwNumber
                        wardId
                        wardName
                        packageStatus
                        bloodType
                        clubProvinceName
                        linkQR
                        membershipKis {
                          kisProvinceId
                          kisId
                          kisType
                          kisName
                          provinceName
                          status
                          kisStatus
                        }
                        idProvinceClubJoin
                        simMotor
                        simMotorPicture
                        simMotorPictureLink
                        simCar
                        simCarPicture
                        simCarPictureLink
                        userPackageId
                }
}""";

String queryMembersOfCommunity({
  int? communityId,
  String? nextToken,
  int limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """{
    memberOfCommunity(
        token: {
            nextToken: "$nextToken"
            limit: $limit
        }
        criteria: {
            communityId: $communityId
        }
    ){
        totalMembers
        nextToken
        leaderNo
        adminNo
        data {
            userUuid
            groupRoles
            communityV2 {
                id
                name
                avatarUrl
                communityInfo {
                  communityRole
                }
                userView{
            isFollowing
            numberPending
            associationNamePromotor
            clubNamePromotor
        }
            }
        }

    }
}""";

String queryNotifications(
        {String? userUuid,
        bool? read,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{ 
    notifications(        
      token: {            
        nextToken: "$nextToken"
        limit: $limit
      }        
      criteria: {
        read: $read
        userUuid: "$userUuid"
      }    
    ){
      nextToken        
      data {            
        id            
        read           
        title            
        body            
        createdDate            
        image            
        params {
          key
          value
        }            
        notificationType        
      }    
    }
}""";

String queryNotificationCount({bool? read}) => """{ 
  notificationNo(               
    read: false         
  )
}""";

String queryFollowingCommunities(
        {int? communityId,
        String? type,
        String? communityType,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{ 
  followCommunities(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
        criteria: {
            communityId: $communityId
            type: $type
            communityType: $communityType
        }
   ){
        nextToken
        data {
            id
            name
            followerNo
            memberNo
            avatarUrl
            star
            location
            myCommunityContent{
               isFollowing
               isMember
               clubRole
            }
        }
   }
}""";

String queryMyClubs(
        {int? limit = Const.NETWORK_DEFAULT_LIMIT, String? nextToken}) =>
    """{ 
  myClubs(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
   ){
        nextToken
        data {
            id
            name
            followerNo
            memberNo
            avatarUrl
            star
            location
            clubType
        }
   }
}""";

String queryBookmarks(
        {bool? isBookmarked = true,
        String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """{
    posts(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
            limit: $limit
        }
        criteria: {
            favPosts: $isBookmarked
        }
    ) {
        nextToken
        data {
            id
            fullName
            avatar
            userUuid
            posterCommunityId
            communityName
            communityAvatar
            text
            textColor
            backgroundImage
            backgroundImages
            backgroundColor
            type
            privacy
            createdDate
            modifiedDate
            likeNumber
            commentNumber
            shareNumber
            isBookmarked
            isLiked
            description
            communityV2 {
                id
                name 
                avatarUrl
                type
            } 
            dynamicLink
        }
    }
}""";

String queryAssociationClubs(
        {int? provinceId = null,
        int? associationId,
        int? clubId,
        int? communityId,
        List<AssociationClubStatusType>? statuses,
        String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """{
    associations(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
            limit: $limit
        }
        criteria: {
            associationId: $associationId
            provinceClubId: $provinceId
            clubId: $clubId
            communityId: $communityId
            statusType: ${statuses?.map((e) => e.name).toList().join(",")}
        }
    ) {
        nextToken
        total
        data {
            id
            associationId
            communityId
            clubId
            status
            createdDate
            modifiedDate
            avatarUrl
            associationName
            adminName
            message
            star
            provinceClub
            clubName
            followerNo
            memberNo
        }
    }
}""";

String queryGPoint(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? fromDate,
        String? endDate}) =>
    """{
    gpointTransactions(
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
            limit: $limit
        }
        criteria: {
            from: $fromDate
            to: $endDate
        }
    ) {
        nextToken
        hasNext
        data {
            id
            changedPoint
            createdDate
            detail
            transactionType
        }
    }
}""";

String queryMyCommunities(
        {String? type,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? nextToken}) =>
    """{ 
  yourCommunities(
        type: $type
        token: {
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        }
   ){
        nextToken
        data {
            id
            name
            location
            avatarUrl
            star
        }
   }
}""";

String queryListLibrary(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        String? type,
        String? keyWord,
        bool? globalSearch}) =>
    """
{
    fetchDocuments(
        token:{
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: ${limit}
        }
        criteria:{
            keyword:${keyWord == null ? null : jsonEncode(keyWord)},
            privacyScope:$type
            globalSearch:${globalSearch == null ? false : globalSearch}

        }
    ){
        nextToken
        total
        data{
                id
                title
                imageUrl
                isLocked
                link
                type
                modifiedDate
        }
    }
}
""";

String queryFetchDocumentById(int documentId) => """
{
    fetchDocumentById(documentId:$documentId){
         categories {
            color
            emoji
            id
            name
            order
        }
        description
        id
        imageUrl
        isLocked
        leaderTypes
        levels{
            id
            name
            status
              }
        link
        modifiedDate
        privacy
        title
        type
    }
}
""";

String queryNews(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        NewsCategory? typeCategory,
        String? searchKey,
        List<int>? typeIds,
        List<int>? topicIds,
        List<int>? excludeIds}) =>
    """
{
fetchNews(
    criteria: {
        category:${typeCategory?.name}   ,
        typeIds: $typeIds,
        topicIds: $topicIds,
        excludeIds:$excludeIds,
        searchKey: ${jsonEncode(searchKey)}
    },
     token: {
         limit: ${limit}
         nextToken:  ${nextToken == null ? null : jsonEncode(nextToken)}
     }
)
{
        total
        nextToken
        data {
            author
            createdDate
            id
            imageUrl
            title
        }
}
}
""";

String queryNewsDetails({int? postId}) => """
{
fetchNewsById(postId:$postId)
{
author
category
content
createdDate
id
imageUrl
language
modifiedDate
title
types{
  color
  emoji
  id
  name
  order
    }
views
}
}
""";

const String postingCommunities = """{
    postingCommunities {
        id
        name
        type
    }
}""";

String fetchCourses({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
  String? searchKey,
  String? priorityOrder,
  List<int>? topicIds,
  String? priceOrder,
  int? courseId,
  bool? isHot,
  int? couponId,
}) =>
    """{
   fetchCourses(
       token:{
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)},
           limit:${limit}
       } ,
    criteria: {
        courseId:$courseId,
        searchKey: ${jsonEncode(searchKey)},
        priorityOrder: ${priorityOrder},
        priceOrder: ${priceOrder},
        isHot: ${isHot == null ? null : isHot},
        topicIds : ${topicIds == null ? null : topicIds}
        couponId : ${couponId == null ? null : couponId}
    }){
        total
        nextToken
        data{
            id
            productId
            name
            avatarUrl
            priority
            preferentialFee
            code
            currentFee
            description
            isHot
            startDate
            status
            studyForm
            studyTime
            teacher
        }
    }
}""";

String queryCourseDetail({int? courseId}) => """
{
fetchCourseById(courseId: $courseId)
{
  avatarUrl
  code
  currentFee
  description
  id
  isHot
  name
  preferentialFee
  priority
  startDate
  status
  studyForm
  studyTime
  teacher
  productTopicIds
  testimonials {
          avatarUrl
          description
          fullName
          id
          postId
          priority
          title
                }
}
}
""";

String queryFetchCleanSeed({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """{
    fetchCleanSeedHistories(token: {
         nextToken:${nextToken == null ? null : jsonEncode(nextToken)},
           limit:${limit}
    }){
        total
        uncompletedTotal
        nextToken
        data {
             id
       createdDate
       completedDate
       modifiedDate
       userUuid
       steps
       step1 {
           id
           action
           actionType
           status
           seed {
               id
               name
               cleanSeedGuide
               plantSeedGuide
               status
           }
       }
       step2
       step3
       step4
        }
    }
}""";

String queryFetchCleanSeedById({required int cleanSeedId}) => """{
fetchCleanSeedHistoryById(cleanSeedHistoryId: $cleanSeedId){
id
createdDate
completedDate
modifiedDate
userUuid
steps
step1{
id
action
actionType
status
seed

{ id name cleanSeedGuide plantSeedGuide status }
}
step2
step3
step4
}
}""";

String queryFetchPlanSeed({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """{
   fetchPlantSeedHistories(
       token:{
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)},
           limit:${limit}
       },
       
       ){
        nextToken
        total
        uncompletedTotal
        data{
         id
         step1
         steps
        completedDate
        createdDate
        }
    }
}""";

String queryFetchCleanSeedsSetting({
  String? nextToken,
  String? searchKey,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """{
   cleanSeedSettings(
       token:{
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)},
           limit:${limit}
       },
      criteria: {
        searchKey: ${searchKey == null ? null : jsonEncode(searchKey)}
      }
       ){
        nextToken
        data{
          id
          action
          actionType
          seed {
            id
            name
            plantSeedGuide
            cleanSeedGuide
            status
          }
        }
    }
}""";

String queryDetailPlanSeed({int? plantHistoryId}) => """{
fetchPlantSeedHistoryById(
    plantHistoryId: $plantHistoryId
){   
    id    
    userUuid    
    goalsId   
    steps   
    step1    
    step2  
    step3    
    step4     
    completedDate   
     createdDate
}
}""";

String queryDetailReport({
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
  String? nextToken,
  int? postId,
}) =>
    """
{   fetchReportedPostInfo(      
     token:{          
          nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},           
          limit:${limit}   
           },      
            postId: $postId      
             )
             {    
              nextToken      
               total       
                data{       
                  id         
                   reporterUserUuid         
                   avatarUrl         
                   fullName       
                   reason{          
                       id            
                       reason        
                         }         
                   createdDate            
                             }    
                        }
} 
""";

String queryFetchReportReasons({String? reasonType}) => """
{
fetchReportReasons(reasonType: $reasonType)
{
 id
  reason
  reasonType
}
}
""";

String queryFetchPlantSeedGoals({
  String? nextToken,
  String? searchKey,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """
{
    fetchPlantSeedGoals(token: {
        nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}, 
        limit:${limit}      
    }, criteria: {
        searchKey: ${searchKey == null ? null : jsonEncode(searchKey)}
    }){
        data{
            id
            name
        }
        nextToken
    }
    }
""";

String queryFetchMeditation({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """{ 
    fetchMeditations(token: 
    {    
        nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},           
        limit:${limit}    
    })
        { 
            nextToken 
            data
            { 
                id 
                title 
                imageUrl 
                isLocked
            }
        } 
}""";

String queryDetailMeditation({int? meditationId}) => """{ 
    fetchMeditationById(meditationId: ${meditationId})
    { 
        id 
        title 
        description 
        fileWithMusicUrl 
        fileWithNoMusicUrl 
        createdDate 
        modifiedDate 
        isLocked 
        fileWithMusicDuration
        fileWithNoMusicDuration
    } 
}""";

String queryHistoriesMeditation({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """{
     fetchMeditationHistories(
     token: {  nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},           
                limit:${limit}        
              }){
                  nextToken
                  total
                  data {
                    id
                    duration
                    meditation{
                        title
                        createdDate
                    }
                    createdDate
        }
    }
     } """;

String queryListBlock({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
}) =>
    """
{    
fetchBlockedUsers(
        token:
{
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
            limit: ${limit}  
        }
    )
{
        nextToken
        total
        data{
            id
            userUuid 
            profile{
                userUuid 
               avatarUrl
                fullName 
                community{
                id
                }
           }      
             }    }
             } 
""";

String queryAudio({AudioType? audioType}) => """
{
    fetchAudio(type:${audioType?.name})
        {
             code
            createdDate
            duration
            fileName
            fileUrl
            id
            modifiedDate
            type
        }

}
""";

String queryReportMeditation(
        {List<int>? meditationIds, ReportType? reportType}) =>
    """
{
 fetchGeneralMeditationReport(criteria: 
    {       meditationIds: ${meditationIds}, 
            type: ${reportType?.name}
    }) 
        {       
            dayStreak       
            totalDays        
            totalHours   
        }
}
""";

String querySearchCommunities({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
  String? searchKey,
  CommunityType? communityType,
}) =>
    """
{
   searchCommunities(
       token:{
           limit:${limit},
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
       },
       criteria:{   
           communityType: ${communityType?.name}
           searchKey:${searchKey == null ? null : jsonEncode(searchKey)}
       }
   ){
       nextToken
       total
       data{
           id
           name
            avatarUrl
           groupView{
               bomMembers{
                   role
                   bomMembers{
                       fullName
                   }
               }
               event{
                   startTime
                   dayOfWeek
               }
           }
           communityInfo{
            followerNo
            memberNo
            postNo
           communityLevel{
               levelName
           }
           }
       }
   }
}
""";

String queryDetailReportMeditation(
        {ReportType? reportType, List<int>? meditationIds}) =>
    """{
    fetchDetailMeditationReport(
        criteria: {
            type:  ${reportType?.name},
            meditationIds: ${meditationIds}
        }
    ){
        countData{
            horizontal
            vertical
        }
        durationData{
            horizontal
            vertical
        }
    }
}""";

String queryListChallenges(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        List<String>? status,
        bool? todo}) =>
    """{
   fetchChallenge(
       token:{
           limit:${limit},
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
       },
       criteria:{
            status: ${status},
            todo : $todo
       }
   ){
       nextToken
       total
       data{
           id
           challengeVersionId
           version
           name
           imageUrl
           description
           level{
               id
               name
           }
           challengeHistory{
               id
               userUuid
               startedDate
               completedDate
               completedTotal
               isLocked
               status
               missionHistory{
                   id
                   dayNo 
                   completedDate
                  missions{
                       id
                       name
                   }
                   completedMissionIds
               }
           }
           dayNo
       }   
   }
} """;

String queryDetailChallenge({int? challengeVersionId}) => """
{
   fetchChallengeVersionById(
       challengeVersionId: ${challengeVersionId}
   )
{
               id
           challengeVersionId
           version
           name
           imageUrl
           description
           level{
               id
               name
           }
           challengeHistory{
               id
               userUuid
               startedDate
               completedDate
               completedTotal
               isLocked
               status
               missionHistory{
                   id
                   dayNo 
                   completedDate
                  missions{
                       id
                       name
                   }
                   completedMissionIds
               }
           }
           dayNo
       }   
}  
""";

String queryBanner() => """
{
    fetchBanners{
        id
        name
        status
        imageUrl
        type
        createdDate
        modifiedDate
        externalLink
        link {
            id
            name
            productId
        }
    }
}
""";

String countAvailableChallenge() => """{countAvailableChallenge}""";

String queryMission({int? challengeMissionHistoryId}) => """
{
    fetchMissionOfDay(
    challengeMissionHistoryId:$challengeMissionHistoryId
            )
    {
        missions{
            completed
            completedDate
            id
            mission{
                id
                name
                type
            }
        }
        note
    }
}
""";

String fetchGroupMemberRequest(
        {String? nextToken,
        required int communityId,
        int? limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """
  {
    fetchGroupMemberRequest(
      token:{
           limit:${limit},
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
       },
       criteria:{
            communityId: $communityId
       }
    ) {
      totalMembers
      leaderNo
      adminNo
      nextToken
      data {
        userUuid
        communityV2 {
          id
          name 
          avatarUrl
        }

      }

    }
  }

""";

String queryDetailCart() => """
{
    fetchMyOrderV2{
        coupons{
            coupon{
                code
                compile
                createdDate
                description
                discount
                discountType
                expiredDate
                id
                levels{
                    id
                    name
                    status
                }
                maxQuantity
                minPrice
                modifiedDate
                status
                type
            }
            isValid
        }
        items{
           course{
                avatarUrl
                code
                currentFee
                description
                id
                isHot
                name
                preferentialFee
                priority
                startDate
                status
                studyForm
                studyTime
                teacher
                testimonials{
                    avatarUrl
                    description
                    fullName
                    id
                    postId
                    priority
                    title
                }
                vat 
              }
           event{
               address
               avatarUrl
               code
               currentFee
               description
               endDate
               id
               isHot
               name
               preferentialFee
               priority
               productId
               startDate
               status
               studyTime
               teacher
           }
           userInformation{
               email
               phone
               provinceId
               userName
           }
           productType
           discount
           id
           price
           note
           quantity
           validCouponIds
        }
        pointExchangeRate
        orderId
        total
        usePoint
        totalComboCouponDiscount
    }
} 
""";

String getListOrderCourse(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        OrderStatus? orderStatus}) =>
    """
{
    fetchOrderHistoryV2(
         token:{          
                  limit:${limit},
                  nextToken:${nextToken == null ? null : jsonEncode(nextToken)}       
                },
        criteria: {         
                  status  : ${orderStatus?.name}        
                 }
                 ){
                     nextToken
                     data{
                        totalItem
                        lastAudit{                 
                            id                 
                            note                 
                            status                 
                            createdDate        
                            createdBySide                    
                            }
            orderHistory{
                id
                price
                paymentType
                code
                status
                createdDate
            }
        }
    }
}""";

String getDetailOrder({int? orderHistoryId, int}) => """
{
      fetchOrderHistoryByIdV2(
          orderHistoryId:$orderHistoryId
          isSeen:${jsonEncode(true)}
      ){
            lastAudit{                
                 id                
                  note                
                   status                 
                   createdDate                
                    createdBySide 
                    cancelReason            
                    }
          orderHistoryDTO{
                id
                finalPrice
                paymentType
                code
                status
                createdDate
                isSeen
              cancelReason
              canceledDate
              modifiedDate
              paymentDate
              paymentType
              productTypes
              totalDiscount
              totalComboCouponDiscount
              totalSingleCouponDiscount
              finalPrice
              totalPrice
              pointExchangeRate
              usePoint
              usedPointAmount
              coupons{
                  code
                  id
              }
              orderItems{
                 email 
                 id
                 imageUrl
                 itemId
                 name
                 phone
                 preCalculatedPrice
                 price
                 productType
                 provinceId
                 quantity
                 totalDiscountPrice
                 totalPrice
                 userName
              }
        }
    }
}""";
String fetchCancelOrderReason = """
  {
    fetchCancelOrderReason {
      id
      reason
    }
}
""";

String unreadNotificationCount = """
{
    notificationNo(read: false)
}
""";

String queryPayment() => """
{
    fetchPaymentTypes{
            status
            type
             childs{            
               status            
               type        
               }
    }
}
""";

String queryDetaiNotificationDciAdmin({required int id}) => """ 
  { 
    getPrivateNotificationDetailById(        
      privateNotificationId:          
       $id
      ){
         body,
         createdDate,
         id,
         text
    }
 }
 """;

String querySearchUserChat({
  CommunityType? communityType,
  String? searchKey,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
  String? nextToken,
}) =>
    """
{
    fetchJoinedChannels(
        criteria:{
            communityType:${communityType?.name}
            searchKey:${searchKey == null ? null : jsonEncode(searchKey)}
        }
        token:{
            limit:${limit}
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)}  
        }
    ){
        count
        nextToken
        data{
            channelPubnubId
            communityImage
            communityName
            lastMessage{
                isRead
                message
                timeToken
            }
            unReadCount
        }
    }
}
""";

String getListJoinedChannel(
        {String? nextToken, int? limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """{
   fetchJoinedChannels(
       token:{            
           limit:${limit}            
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)}     
           },
       criteria:{}
   ){
       nextToken
       data{
        channelPubnubId
        communityName
        communityImage
        lastMessage{            
            timeToken           
            message            
            isRead        
            }
       unReadCount
       userUuid
       communityId
       lastReadTimeToken
       }
   }

}""";

String getUserChannel() => """{
    fetchUserChannelGroups{
        channelGroupsPubnubId
    }
}""";

String queryProfilesByUserUuids({required List<String> channelUuid}) => """ 
 { 
    fetchProfilesByUserUuids(        
      userUuids:${jsonEncode(channelUuid)}
      ){
userUuid
fullName
avatarUrl
communityId
  }
}
 """;

String queryGroupMember({required int communityId, String? nextToken}) => """ 
 { 
     memberOfCommunity(
         token:{            
           limit:null            
           nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
           }     
                 criteria: {
                     communityId:$communityId
                 blockedUsers:false
                 } ){
adminNo
data{
    communityV2{
        avatarUrl
        name
        id
    }
    groupRoles
    userUuid
}
leaderNo
nextToken
totalMembers
     }
  }
 """;

String get queryGetSupportChannelId => """
  {
    fetchCustomerSupport {
        channelPubnubId,
        status
    }
}
""";

String queryUnreadTotalCount({required String userUuid}) => """
  {
    unReadTotalCount(
        userUuid: ${jsonEncode(userUuid)}
    ){
        count
    }
}
""";

String queryTopic(
        {String? nextToken, int? limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """{
      fetchTopics(token:{
            limit:${limit}            
            nextToken:${nextToken == null ? null : jsonEncode(nextToken)}  
            }
          ){
          data
          { id 
            name 
            imageUrl 
          }
          total
          nextToken
          }
      }
      """;

String queryFetchQuestion(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        List<int>? topicIds,
        String? searchKey}) =>
    """{
    fetchQuestions(
        token:{          
                limit:${limit}            
                nextToken:${nextToken == null ? null : jsonEncode(nextToken)}     
                },
        criteria: {             
            searchKey: ${jsonEncode(searchKey)}             
            topicIds : $topicIds        
            }
          ){
            nextToken
            data{
                id
                question
                answer
                status
                topics{                 
                        id                
                        name             
                    }
            }
        }
}""";

String queryDetailQuestion({int? questionId}) => """{
      fetchQuestionById(questionId: $questionId)
{    
    id 
    question 
    answer 
    status
}
}""";

String queryRelatedQuestion({
  int? limit = 4,
  List<int>? excludeIds,
  List<int>? topicIds,
}) =>
    """{
    fetchQuestions(
    criteria:{ 
        excludeIds: $excludeIds 
        topicIds: $topicIds
        }
    token:{ 
        limit:${limit}            
        nextToken: null
        }){
            data
            { 
              id 
              question
              topics{                 
                        id                
                        name             
                    }
            }
        }
}""";

String queryEvent({
  String? nextToken,
  int? limit = Const.NETWORK_DEFAULT_LIMIT,
  String? searchKey,
  String? priorityOrder,
  String? priceOrder,
  int? exceptEventId,
  List<int>? topicIds,
  bool? isHot,
  int? couponId,
}) =>
    """ {
     fetchEvents(
        token:{             
            limit:${limit}            
            nextToken:${nextToken == null ? null : jsonEncode(nextToken)}    
         },
        criteria:{             
            searchKey: ${jsonEncode(searchKey)}             
            priorityOrder: ${priorityOrder}            
             priceOrder: ${priceOrder}    
             isHot: ${isHot == null ? null : isHot}        
             exceptEventId: $exceptEventId
             topicIds: ${topicIds == null ? null : topicIds}
             couponId: ${couponId == null ? null : couponId}
              })
    {
        nextToken
        data{             
            id                  
            productId     
            name     
            avatarUrl     
            status     
            currentFee     
            preferentialFee     
            description     
            code     
            teacher     
            startDate     
            endDate     
            priority     
            isHot     
            address         
            }
    }
}""";

String queryEventDetail({int? eventId}) => """ {
    fetchEventById( 
        eventId: $eventId
        ){            
         id                 
         productId     
         name    
         avatarUrl     
         status     
         currentFee     
         preferentialFee     
         description     
         code     
         teacher     
         startDate     
         endDate     
         priority     
         isHot     
         address     
         studyTime
         topicIds
        }
}""";

String queryItemByProductId({int? productId}) => """ {
    fetchItemByProductIdV2(
        productId: $productId
     ){
        id
        quantity
        validCouponIds
        productType
        note
        userInformation{                 
            email                 
            userName  
            phone
            provinceId           
            }
            course{                 
                id                 
                name                 
                productId                              
                avatarUrl             
                }
            event{                 
                id                 
                name                
                productId                                
                avatarUrl             
                }
    }
}""";

String queryHistoryPoint(
        {String? nextToken,
        int? limit = Const.NETWORK_DEFAULT_LIMIT,
        ActionTypePoint? actionTypePoint}) =>
    """{
    fetchPointHistoryOfCurrentUser(
    criteria:
        { 
            actionTypes: [${actionTypePoint?.name}] 
        }
    token:{ 
         limit:${limit}            
         nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
           }
        ){
        nextToken
        data{ 
            id 
            actionName 
            action 
            actionType 
            point 
            createdDate 
            sourceId 
            targetId 
        }
     }
}""";

String queryExchangeRate = """{
   pointExchangeRate
}""";

String queryPointAction = """{
        fetchPointActionIn  
          {   
            id  
            name      
            action       
            category{          
                     id
                     name 
                     type     
                    }      
            actionType
            point  
            status     
            maximumPoint
    } 
   }""";

String queryCheckStatusOrder({int? orderHistoryId}) => """{
    getLastAuditByOrderHistoryId(
        orderHistoryId: $orderHistoryId )
        {
            status      
        }
}""";
String queryPointActionOut = """{
    fetchPointActionOut {        
                    id       
                    name        
                    action       
                    actionType      
                    point     
                    status   
                    maximumPoint   
    }
}""";

String queryCheckPhoneForgot({String? phone}) => """{
    existsProfileByPhone(phone:${phone == null ? null : jsonEncode(phone)})
    {           
    userUuid            
    phone           
    fullName     
    }
}""";

String queryCoupons(
        {String? nextToken, int? limit = Const.NETWORK_DEFAULT_LIMIT}) =>
    """{
    fetchCoupons(
        token:{
            limit:${limit}            
            nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
        },
        criteria: {
            searchKey: null
        }
        
    )
    {
        nextToken
        data{
          id
          code
          description
          discountType
          discount
          type
          expiredDate
          maxQuantity
          minPrice
          usedNo
          compile
          limitType
          privacy
          isValid
        }
    }
}""";

String queryProductTopic({
  String? nextToken,
  String? searchKey,
  int? limit = 40,
}) =>
    """{
       fetchProductTopics(
                  token:{
                  nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
                  limit:$limit
                  },
                 criteria: {
                 searchKey: ${jsonEncode(searchKey)}
                 }
                  )
                   {
                 nextToken
                 data{
                  id
                  topicName
                    }
                }
} """;

String queryDetailCoupons({int? couponId}) => """{
    couponDetailGraph(couponId: ${couponId}){
    createdDate
    code
    totalCourse
    totalCourseCombo
    totalEvent
    totalEventCombo
    discount
    discountType
    minPrice
    limitType
    description
    totalDiscount
    expiredDate
 }
}""";

String fetchPointActionCategories = """{ 
   fetchPointActionCategories{ 
   id  
    name
     type 
      actions{ 
        id
         name 
         }
    }} """;

String queryCourseByMore({int? couponId}) => """{
    fetchProductBuyMore(
        couponId: $couponId
    ) {
        courses{
                id
                productId
                currentFee
                preferentialFee
                quantity
                name
                avatarUrl
                isHot
        },
        events{
                 id 
                 productId
                  currentFee
                  preferentialFee
                  quantity
                  name
                  avatarUrl
                  isHot
        }
    }
}""";

String queryEventTopic({
  String? nextToken,
  String? searchKey,
  int? limit = 100,
}) =>
    """{
       fetchEventTopics(
                  token:{
                  nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
                  limit:$limit
                  },
                 criteria: {
                 searchKey: ${jsonEncode(searchKey)}
                 }
                  )
                   {
                 nextToken
                 data{
                  id
                  topicName
                    }
                }
} """;

String queryCourseTopics({
  String? nextToken,
  String? searchKey,
  int? limit = 100,
}) =>
    """{
       fetchCourseTopics(
                  token:{
                  nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
                  limit:$limit
                  },
                 criteria: {
                 searchKey: ${jsonEncode(searchKey)}
                 }
                  )
                   {
                 nextToken
                 data{
                  id
                  topicName
                    }
                }
} """;


String queryLibrary({
  List<int>? categoryIds,
  List<int>?  topicIds,
  bool? isLocked,
  bool? isPaging,
  String? searchKey,
  String? type,
  String? libraryDocumentType,
  String? privacy,
  String? nextToken,
  int? limit
}) =>
    """{
    fetchDocumentsByType(
      token: {
    nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
    limit: ${limit!=null?limit:Const.LIMIT}
},
    criteria: {
                   isLocked:$isLocked
                   types:$type
                   privacyScope:$privacy
                   categoryIds:$categoryIds
                   libraryDocumentType: $libraryDocumentType
                   searchKey:${jsonEncode(searchKey)}
       },
       isPaging: $isPaging
      )
       {
         nextToken
          data {
           link
           isLeader
           id
           type
           title
           imageUrl
           isLocked
           modifiedDate
           priority
           privacy
       }
  }
} """;

String queryCategoryLibrary({
  String? searchKey,
  int? limit = 100,
   String? nextToken,
  String? type,
})=>
"""{
    categoriesByLanguage(
    token:{
    nextToken:${nextToken == null ? null : jsonEncode(nextToken)}
    limit:$limit
    }
    criteria:{
        categoryTypes:${type},
        searchKey:${jsonEncode(searchKey)}
    }
    ){
              nextToken
         data {    
     id  
     name
          }
       }
      }
""";

String getDocumentTopics({
  String? nextToken,
  int? limit=100,
  String? searchKey
})=>"""
{
    fetchDocumentTopics(
        token:{
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            limit: $limit
        },
        criteria:{
            searchKey: ${jsonEncode(searchKey)}
        } 
    )
    {
        nextToken
        data{
            id
            topicName
        }
    }
}
""";

String getDocumentByCategories({
  String? nextToken,
  int? limit=Const.LIMIT,
  String? type,
  String? searchKey,
  List<int>? categoryIds
})=>"""
{
    fetchCategoriesByDocument(
        token: {nextToken: ${nextToken == null ? null : jsonEncode(nextToken)},
         limit: $limit},
criteria: {
nameDocument:${jsonEncode(searchKey)}
 libraryDocumentType: $type,categoryIds:$categoryIds, userUuid: null})
{
    nextToken
    total
   data {
       category {
           id
           name
       }
       categoryDocuments(libraryDocumentType: $type,
       name: ${jsonEncode(searchKey)}){
           data {
               link
               id
               title
               imageUrl
               isLocked
               type
               priority
           }
       }
   }
}
}
""";

String queryCategoryStory({
  String? nextToken,
  int? limit=Const.LIMIT,
  String? searchKey,
})=>"""{    
    categoriesByLanguage(        
    token:{            
        nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
        limit: $limit          
        },        
    criteria:{                       
            categoryTypes:[SUCCESS_GOAL]
            searchKey:${searchKey == null ? null : jsonEncode(searchKey)}
            })
            {        
            nextToken       
            data{           
                id           
                name       
                }    
            }
}""";

String queryPostStory({
  String? nextToken,
  int? limit = 100,
  String? searchKey,
})=>"""{
    filterPostTopic(
        criteria: {
            postCategory: SUCCESS_GOAL,
            searchKey:${searchKey == null ? null : jsonEncode(searchKey)} 
            isActive: true
            },
        token: {
            limit: $limit,
            nextToken: ${nextToken == null ? null : jsonEncode(nextToken)}
            }){
                nextToken
                data {
                    topicName
                    id
                }
            }
}""";