class AdminUpdateCommunity {
  int? id;
  String? avatarUrl;
  String? imageUrl;
  String? name;
  String? privacy;
  String? type;
  List<int>? contentPrefence;
  String? status;
  String? description;
  String? dynamicLink;
  int? memberLimit;
  List<SocialLinks>? socialLinks;
  GroupEvent? groupEvent;
  GroupLevel? groupLevel;
  List<GroupMembers>? groupMembers;

  AdminUpdateCommunity(
      {this.id,
        this.avatarUrl,
        this.imageUrl,
        this.name,
        this.privacy,
        this.type,
        this.contentPrefence,
        this.status,
        this.description,
        this.dynamicLink,
        this.memberLimit,
        this.socialLinks,
        this.groupEvent,
        this.groupLevel,
        this.groupMembers});

  AdminUpdateCommunity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    avatarUrl = json['avatarUrl'];
    imageUrl = json['imageUrl'];
    name = json['name'];
    privacy = json['privacy'];
    type = json['type'];
    contentPrefence = json['contentPrefence'].cast<int>();
    status = json['status'];
    description = json['description'];
    dynamicLink = json['dynamicLink'];
    memberLimit = json['memberLimit'];
    if (json['socialLinks'] != null) {
      socialLinks = <SocialLinks>[];
      json['socialLinks'].forEach((v) {
        socialLinks!.add(new SocialLinks.fromJson(v));
      });
    }
    groupEvent = json['groupEvent'] != null
        ? new GroupEvent.fromJson(json['groupEvent'])
        : null;
    groupLevel = json['groupLevel'] != null
        ? new GroupLevel.fromJson(json['groupLevel'])
        : null;
    if (json['groupMembers'] != null) {
      groupMembers = <GroupMembers>[];
      json['groupMembers'].forEach((v) {
        groupMembers!.add(new GroupMembers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['avatarUrl'] = this.avatarUrl;
    data['imageUrl'] = this.imageUrl;
    data['name'] = this.name;
    data['privacy'] = this.privacy;
    data['type'] = this.type;
    data['contentPrefence'] = this.contentPrefence;
    data['status'] = this.status;
    data['description'] = this.description;
    data['dynamicLink'] = this.dynamicLink;
    data['memberLimit'] = this.memberLimit;
    if (this.socialLinks != null) {
      data['socialLinks'] = this.socialLinks!.map((v) => v.toJson()).toList();
    }
    if (this.groupEvent != null) {
      data['groupEvent'] = this.groupEvent!.toJson();
    }
    if (this.groupLevel != null) {
      data['groupLevel'] = this.groupLevel!.toJson();
    }
    if (this.groupMembers != null) {
      data['groupMembers'] = this.groupMembers!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SocialLinks {
  String? name;
  String? url;

  SocialLinks({this.name, this.url});

  SocialLinks.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['url'] = this.url;
    return data;
  }
}

class GroupEvent {
  String? name;
  int? startDate;
  int? endDate;
  String? dayOfWeek;
  int? startTime;
  int? endTime;

  GroupEvent(
      {this.name,
        this.startDate,
        this.endDate,
        this.dayOfWeek,
        this.startTime,
        this.endTime});

  GroupEvent.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    startDate = json['startDate'];
    endDate = json['endDate'];
    dayOfWeek = json['dayOfWeek'];
    startTime = json['startTime'];
    endTime = json['endTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    data['dayOfWeek'] = this.dayOfWeek;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    return data;
  }
}

class GroupLevel {
  int? communityId;
  int? level;
  String? levelName;
  String? levelStatus;
  int? year;

  GroupLevel(
      {this.communityId,
        this.level,
        this.levelName,
        this.levelStatus,
        this.year});

  GroupLevel.fromJson(Map<String, dynamic> json) {
    communityId = json['communityId'];
    level = json['level'];
    levelName = json['levelName'];
    levelStatus = json['levelStatus'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['communityId'] = this.communityId;
    data['level'] = this.level;
    data['levelName'] = this.levelName;
    data['levelStatus'] = this.levelStatus;
    data['year'] = this.year;
    return data;
  }
}

class GroupMembers {
  String? avatarUrl;
  String? userUuid;
  String? phone;
  String? fullName;
  int? communityId;
  String? status;
  List<String>? role;

  GroupMembers(
      {this.avatarUrl,
        this.userUuid,
        this.phone,
        this.fullName,
        this.communityId,
        this.status,
        this.role});

  GroupMembers.fromJson(Map<String, dynamic> json) {
    avatarUrl = json['avatarUrl'];
    userUuid = json['userUuid'];
    phone = json['phone'];
    fullName = json['fullName'];
    communityId = json['communityId'];
    status = json['status'];
    role = json['role'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatarUrl'] = this.avatarUrl;
    data['userUuid'] = this.userUuid;
    data['phone'] = this.phone;
    data['fullName'] = this.fullName;
    data['communityId'] = this.communityId;
    data['status'] = this.status;
    data['role'] = this.role;
    return data;
  }
}