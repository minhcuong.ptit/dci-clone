import 'package:imi/src/utils/app_config.dart';

///
/// Code generated by jsonToDartModel https://ashamp.github.io/jsonToDartModel/
///
class LogoutRequest {
  String? clientId;
  String? refreshToken;

  LogoutRequest({
    this.refreshToken,
  }) {
    this.clientId = AppConfig.environment.clientId;
  }

  LogoutRequest.fromJson(Map<String, dynamic> json) {
    clientId = json["client_id"]?.toString();
    refreshToken = json["refresh_token"]?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (clientId != null) data["client_id"] = clientId;
    if (refreshToken != null) data["refresh_token"] = refreshToken;
    return data;
  }
}
