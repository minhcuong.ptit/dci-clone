// To parse this JSON data, do
//
//     final profileRequest = profileRequestFromJson(jsonString);

import 'dart:convert';

import '../response/Level.dart';

ProfileRequest profileRequestFromJson(String str) =>
    ProfileRequest.fromJson(json.decode(str));

String profileRequestToJson(ProfileRequest data) => json.encode(data.toJson());

class ProfileRequest {
  ProfileRequest({
    this.intro,
    this.fullName,
    this.email,
    this.birthday,
    this.countryId,
    this.provinceId,
    this.districtId,
    this.location,
    this.socialLinks,
    this.memberType,
    this.levels,
    this.workingUnit,
    this.currentPosition,
  });

  String? intro;
  String? fullName;
  String? email;
  String? birthday;
  int? countryId;
  int? provinceId;
  int? districtId;
  String? location;
  List<SocialLink>? socialLinks;
  String? memberType;
  List<Level>? levels;
  String? workingUnit;
  String? currentPosition;

  factory ProfileRequest.fromJson(Map<String, dynamic> json) => ProfileRequest(
        intro: json["intro"],
        fullName: json["fullName"],
        email: json["email"],
        birthday: json["birthday"],
        countryId: json["countryId"],
        provinceId: json["provinceId"],
        districtId: json["districtId"],
        location: json["location"],
        socialLinks: List<SocialLink>.from(
            json["socialLinks"].map((x) => SocialLink.fromJson(x))),
        memberType: json["memberType"],
        levels: List<Level>.from(json["levels"].map((x) => Level.fromJson(x))),
        workingUnit: json["workingUnit"],
        currentPosition: json["currentPosition"],
      );

  Map<String, dynamic> toJson() => {
        "intro": intro,
        "fullName": fullName,
        "email": email,
        "birthday": birthday,
        "countryId": countryId,
        "provinceId": provinceId,
        "districtId": districtId,
        "location": location,
        "socialLinks":
            List<dynamic>.from((socialLinks ?? []).map((x) => x.toJson())),
        "memberType": memberType,
        "levels": List<dynamic>.from((levels ?? []).map((x) => x.toJson())),
        "workingUnit": workingUnit,
        "currentPosition": currentPosition,
      };
}

class SocialLink {
  SocialLink({
    this.name,
    this.url,
  });

  String? name;
  String? url;

  factory SocialLink.fromJson(Map<String, dynamic> json) => SocialLink(
        name: json["name"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "url": url,
      };
}
