import 'dart:convert';

ReactionRequest reactionRequestFromJson(String str) =>
    ReactionRequest.fromJson(json.decode(str));

String reactionRequestToJson(ReactionRequest data) =>
    json.encode(data.toJson());

class ReactionRequest {
  ReactionRequest({
    this.postId,
    this.action,
  });

  int? postId;
  String? action;

  factory ReactionRequest.fromJson(Map<String, dynamic> json) {
    return ReactionRequest(
      postId: json["postId"],
      action: json["action"],
    );
  }

  Map<String, dynamic> toJson() => {
        "postId": postId,
        "action": action,
      };
}
