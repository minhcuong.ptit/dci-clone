/// clubId : 38
/// action : "JOIN"
/// memberType : "SUB_MEMBER"

class MemberActionRequest {
  MemberActionRequest({
      this.communityId,
      this.action, 
      this.memberType,});

  MemberActionRequest.fromJson(dynamic json) {
    communityId = json['communityId'];
    action = json['action'];
    memberType = json['memberType'];
  }
  int? communityId;
  String? action;
  String? memberType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['communityId'] = communityId;
    map['action'] = action;
    map['memberType'] = memberType;
    return map;
  }

}