class CleanSeedsRequest {
  CleanSeedsAction step1;
  String step2;
  String step3;
  String step4;
  List<int> steps;
  CleanSeedsRequest({
    required this.step1,
    required this.step2,
    required this.step3,
    required this.step4,
    required this.steps,
  });

  Map<String, dynamic> toJson() {
    return {
      'step1': step1.toJson(),
      'step2': step2,
      'step3': step3,
      'step4': step4,
      'steps': steps,
    };
  }
}

class CleanSeedsAction {
  int actionId;
  CleanSeedsAction({
    required this.actionId,
  });

  Map<String, dynamic> toJson() {
    return {
      'actionId': actionId,
    };
  }
}
