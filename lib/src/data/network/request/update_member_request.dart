class UpdateMemberRequest {
  String? status;

  UpdateMemberRequest({
    required this.status,
  });

  UpdateMemberRequest.fromJson(Map<String, dynamic> json) {
    status = json["status"].toString();
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}..["status"] = status;
}
