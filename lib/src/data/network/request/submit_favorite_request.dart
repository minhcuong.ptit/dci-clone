/// categories : [0]
/// communities : [0]

class SubmitFavoriteRequest {
  SubmitFavoriteRequest({
      List<int>? categories, 
      List<int>? communities,}){
    _categories = categories;
    _communities = communities;
}

  SubmitFavoriteRequest.fromJson(dynamic json) {
    _categories = json['categories'] != null ? json['categories'].cast<int>() : [];
    _communities = json['communities'] != null ? json['communities'].cast<int>() : [];
  }
  List<int>? _categories;
  List<int>? _communities;

  List<int>? get categories => _categories;
  List<int>? get communities => _communities;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['categories'] = _categories;
    map['communities'] = _communities;
    return map;
  }

}