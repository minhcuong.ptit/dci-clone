class ChangeLanguageRequest {
  String? deviceToken;
  String? userUuid;

  ChangeLanguageRequest({
    this.deviceToken,
    this.userUuid,
  });

  ChangeLanguageRequest.fromJson(Map<String, dynamic> json) {
    deviceToken = json['deviceToken']?.toString();
    userUuid = json['userUuid']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['deviceToken'] = deviceToken;
    data['userUuid'] = userUuid;
    return data;
  }
}
