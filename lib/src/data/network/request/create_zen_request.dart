import 'dart:convert';

class CreateZenRequest {
  int meditationId;
  int startedDate;
  int completedDate;
  int duration;
  CreateZenRequest({
    required this.meditationId,
    required this.startedDate,
    required this.completedDate,
    required this.duration,
  });

  Map<String, dynamic> toJson() {
    return {
      'meditationId': meditationId,
      'startedDate': startedDate,
      'completedDate': completedDate,
      'duration': duration,
    };
  }
}
