import 'dart:convert';

FollowRequest followRequestFromJson(String str) =>
    FollowRequest.fromJson(json.decode(str));

String FollowRequestToJson(FollowRequest data) => json.encode(data.toJson());

class FollowRequest {
  FollowRequest({
    this.communityId,
    this.action,
  });

  int? communityId;
  String? action;

  factory FollowRequest.fromJson(Map<String, dynamic> json) {
    return FollowRequest(
      communityId: json["communityId"],
      action: json["action"],
    );
  }

  Map<String, dynamic> toJson() => {
    "communityId": communityId,
    "action": action,
  };
}