import 'dart:convert';

class CreateSupportChat {
  final String type;

  const CreateSupportChat([this.type = "PRIVATE"]);

  Map<String, dynamic> toJson() {
    return {
      'type': type,
    };
  }
}
