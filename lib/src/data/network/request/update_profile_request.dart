/// userUuid : "string"
/// nickname : "string"
/// email : "string"
/// updatedAt : "string"
/// accumulatedPoints : "string"
/// profile : "string"
/// provinceId : "string"
/// website : "string"
/// status : "string"
/// middleName : "string"
/// fullName : "string"
/// bloodType : "string"
/// followers : "string"
/// familyName : "string"
/// locale : "string"
/// gender : "string"
/// givenName : "string"
/// driveLicenseNumber : "string"
/// username : "string"
/// avatarUrl : "string"
/// birthdate : "string"
/// pinCode : "string"
/// address : "string"
/// memberType : "string"
/// expireDate : "string"
/// imiId : "string"
/// provinceIdentityCardNumber : "string"
/// qrCode : "string"
/// postalCode : "string"
/// birthPlace : "string"
/// hobby : "string"
/// invitorPhone : "string"
/// phone : "string"
/// group : "string"

class UpdateProfileRequest {
  UpdateProfileRequest({
    String? userUuid,
    String? nickname,
    String? email,
    String? updatedAt,
    String? accumulatedPoints,
    String? profile,
    int? provinceId,
    String? website,
    String? status,
    String? middleName,
    String? fullName,
    String? bloodType,
    String? followers,
    String? familyName,
    String? locale,
    String? gender,
    String? givenName,
    String? driveLicenseNumber,
    String? username,
    String? avatarUrl,
    String? cover,
    String? birthdate,
    String? pinCode,
    String? address,
    String? memberType,
    String? expireDate,
    String? imiId,
    String? provinceIdentityCardNumber,
    String? qrCode,
    String? postalCode,
    String? birthPlace,
    String? hobby,
    String? invitorPhone,
    String? phone,
    String? group,
  }) {
    _userUuid = userUuid;
    _nickname = nickname;
    _email = email;
    _updatedAt = updatedAt;
    _accumulatedPoints = accumulatedPoints;
    _profile = profile;
    _provinceId = provinceId;
    _website = website;
    _status = status;
    _middleName = middleName;
    _fullName = fullName;
    _bloodType = bloodType;
    _followers = followers;
    _familyName = familyName;
    _locale = locale;
    _gender = gender;
    _givenName = givenName;
    _driveLicenseNumber = driveLicenseNumber;
    _username = username;
    _avatarUrl = avatarUrl;
    _cover = cover;
    _birthdate = birthdate;
    _pinCode = pinCode;
    _address = address;
    _memberType = memberType;
    _expireDate = expireDate;
    _imiId = imiId;
    _provinceIdentityCardNumber = provinceIdentityCardNumber;
    _qrCode = qrCode;
    _postalCode = postalCode;
    _birthPlace = birthPlace;
    _hobby = hobby;
    _invitorPhone = invitorPhone;
    _phone = phone;
    _group = group;
  }

  UpdateProfileRequest.fromJson(dynamic json) {
    _userUuid = json['userUuid'];
    _nickname = json['nickname'];
    _email = json['email'];
    _updatedAt = json['updatedAt'];
    _accumulatedPoints = json['accumulatedPoints'];
    _profile = json['profile'];
    _provinceId = json['provinceId'];
    _website = json['website'];
    _status = json['status'];
    _middleName = json['middleName'];
    _fullName = json['fullName'];
    _bloodType = json['bloodType'];
    _followers = json['followers'];
    _familyName = json['familyName'];
    _locale = json['locale'];
    _gender = json['gender'];
    _givenName = json['givenName'];
    _driveLicenseNumber = json['driveLicenseNumber'];
    _username = json['username'];
    _avatarUrl = json['avatarUrl'];
    _cover = json['cover'];
    _birthdate = json['birthdate'];
    _pinCode = json['pinCode'];
    _address = json['address'];
    _memberType = json['memberType'];
    _expireDate = json['expireDate'];
    _imiId = json['imiId'];
    _provinceIdentityCardNumber = json['provinceIdentityCardNumber'];
    _qrCode = json['qrCode'];
    _postalCode = json['postalCode'];
    _birthPlace = json['birthPlace'];
    _hobby = json['hobby'];
    _invitorPhone = json['invitorPhone'];
    _phone = json['phone'];
    _group = json['group'];
  }

  String? _userUuid;
  String? _nickname;
  String? _email;
  String? _updatedAt;
  String? _accumulatedPoints;
  String? _profile;
  int? _provinceId;
  String? _website;
  String? _status;
  String? _middleName;
  String? _fullName;
  String? _bloodType;
  String? _followers;
  String? _familyName;
  String? _locale;
  String? _gender;
  String? _givenName;
  String? _driveLicenseNumber;
  String? _username;
  String? _avatarUrl;
  String? _cover;
  String? _birthdate;
  String? _pinCode;
  String? _address;
  String? _memberType;
  String? _expireDate;
  String? _imiId;
  String? _provinceIdentityCardNumber;
  String? _qrCode;
  String? _postalCode;
  String? _birthPlace;
  String? _hobby;
  String? _invitorPhone;
  String? _phone;
  String? _group;

  String? get userUuid => _userUuid;

  String? get nickname => _nickname;

  String? get email => _email;

  String? get updatedAt => _updatedAt;

  String? get accumulatedPoints => _accumulatedPoints;

  String? get profile => _profile;

  int? get provinceId => _provinceId;

  String? get website => _website;

  String? get status => _status;

  String? get middleName => _middleName;

  String? get fullName => _fullName;

  String? get bloodType => _bloodType;

  String? get followers => _followers;

  String? get familyName => _familyName;

  String? get locale => _locale;

  String? get gender => _gender;

  String? get givenName => _givenName;

  String? get driveLicenseNumber => _driveLicenseNumber;

  String? get username => _username;

  String? get avatarUrl => _avatarUrl;

  String? get cover => _cover;

  String? get birthdate => _birthdate;

  String? get pinCode => _pinCode;

  String? get address => _address;

  String? get memberType => _memberType;

  String? get expireDate => _expireDate;

  String? get imiId => _imiId;

  String? get provinceIdentityCardNumber => _provinceIdentityCardNumber;

  String? get qrCode => _qrCode;

  String? get postalCode => _postalCode;

  String? get birthPlace => _birthPlace;

  String? get hobby => _hobby;

  String? get invitorPhone => _invitorPhone;

  String? get phone => _phone;

  String? get group => _group;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_userUuid != null) map['userUuid'] = _userUuid;
    if (_nickname != null) map['nickname'] = _nickname;
    if (_email != null) map['email'] = _email;
    if (_updatedAt != null) map['updatedAt'] = _updatedAt;
    if (_accumulatedPoints != null)
      map['accumulatedPoints'] = _accumulatedPoints;
    if (_profile != null) map['profile'] = _profile;
    if (_provinceId != null) map['provinceId'] = _provinceId;
    if (_website != null) map['website'] = _website;
    if (_status != null) map['status'] = _status;
    if (_middleName != null) map['middleName'] = _middleName;
    if (_fullName != null) map['fullName'] = _fullName;
    if (_bloodType != null) map['bloodType'] = _bloodType;
    if (_followers != null) map['followers'] = _followers;
    if (_familyName != null) map['familyName'] = _familyName;
    if (_locale != null) map['locale'] = _locale;
    if (_gender != null) map['gender'] = _gender;
    if (_givenName != null) map['givenName'] = _givenName;
    if (_driveLicenseNumber != null)
      map['driveLicenseNumber'] = _driveLicenseNumber;
    if (_username != null) map['username'] = _username;
    if (_avatarUrl != null) map['avatarUrl'] = _avatarUrl;
    if (_cover != null) map['cover'] = _cover;
    if (_birthdate != null) map['birthdate'] = _birthdate;
    if (_pinCode != null) map['pinCode'] = _pinCode;
    if (_address != null) map['address'] = _address;
    if (_memberType != null) map['memberType'] = _memberType;
    if (_expireDate != null) map['expireDate'] = _expireDate;
    if (_imiId != null) map['imiId'] = _imiId;
    if (_provinceIdentityCardNumber != null)
      map['provinceIdentityCardNumber'] = _provinceIdentityCardNumber;
    if (_qrCode != null) map['qrCode'] = _qrCode;
    if (_group != null) map['group'] = _group;
    if (_postalCode != null) map['postalCode'] = _postalCode;
    if (_birthPlace != null) map['birthPlace'] = _birthPlace;
    if (_hobby != null) map['hobby'] = _hobby;
    if (_invitorPhone != null) map['invitorPhone'] = _invitorPhone;
    if (_phone != null) map['phone'] = _phone;
    return map;
  }
}
