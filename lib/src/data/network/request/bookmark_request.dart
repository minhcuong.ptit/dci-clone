import 'dart:convert';

BookmarkRequest bookmarkRequestFromJson(String str) =>
    BookmarkRequest.fromJson(json.decode(str));

String bookmarkRequestToJson(BookmarkRequest data) => json.encode(data.toJson());

class BookmarkRequest {
  BookmarkRequest({
    this.postId,
    this.action,
  });

  int? postId;
  String? action;

  factory BookmarkRequest.fromJson(Map<String, dynamic> json) {
    return BookmarkRequest(
      postId: json["postId"],
      action: json["action"],
    );
  }

  Map<String, dynamic> toJson() => {
        "postId": postId,
        "action": action,
      };
}
