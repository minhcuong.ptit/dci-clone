import 'package:imi/src/data/network/response/member_data.dart';

import 'form_data.dart';

/// addonType : "string"
/// totalFee : 0
/// forms : [{"key":"string","values":["string"]}]
/// members : [{"id":0,"userPackageId":0,"role":"string","phone":"string","ktp":"string","ktaNumber":"string","userUuid":"string","status":"string"}]

class UpgradeFormRequest {
  UpgradeFormRequest({
    String? addonType,
    num? totalFee,
    int? userPackageId,
    List<FormData>? forms,
  }) {
    _addonType = addonType;
    _totalFee = totalFee;
    _userPackageId = userPackageId;
    _forms = forms;
  }

  UpgradeFormRequest.fromJson(dynamic json) {
    _addonType = json['addonType'];
    _totalFee = json['totalFee'];
    _userPackageId = json['userPackageId'];
    if (json['forms'] != null) {
      _forms = [];
      json['forms'].forEach((v) {
        _forms?.add(FormData.fromJson(v));
      });
    }
  }

  String? _addonType;
  num? _totalFee;
  int? _userPackageId;
  List<FormData>? _forms;

  String? get addonType => _addonType;

  num? get totalFee => _totalFee;

  int? get userPackageId => _userPackageId;

  List<FormData>? get forms => _forms;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_addonType != null) map['addonType'] = _addonType;
    map['totalFee'] = _totalFee;
    if (_userPackageId != null) map['userPackageId'] = _userPackageId;
    if (_forms != null) {
      map['forms'] = _forms?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
