class BuyMoreProducts {
  int? itemId;
  int? quantity;

  BuyMoreProducts({
    this.itemId,
    this.quantity,
  });

  BuyMoreProducts.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId']?.toInt();
    quantity = json['quantity']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['itemId'] = itemId;
    data['quantity'] = quantity;
    return data;
  }
}

class BuyMoreRequest {
  String? userName;
  String? phone;
  String? email;
  String? note;
  int? provinceId;
  List<BuyMoreProducts>? products;

  BuyMoreRequest({
    this.userName,
    this.phone,
    this.email,
    this.note,
    this.provinceId,
    this.products,
  });

  BuyMoreRequest.fromJson(Map<String, dynamic> json) {
    userName = json['userName']?.toString();
    phone = json['phone']?.toString();
    email = json['email']?.toString();
    note = json['note']?.toString();
    provinceId = json['provinceId']?.toInt();
    if (json['products'] != null) {
      final v = json['products'];
      final arr0 = <BuyMoreProducts>[];
      v.forEach((v) {
        arr0.add(BuyMoreProducts.fromJson(v));
      });
      products = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['userName'] = userName;
    data['phone'] = phone;
    data['email'] = email;
    data['note'] = note;
    data['provinceId'] = provinceId;
    if (products != null) {
      final v = products;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['products'] = arr0;
    }
    return data;
  }
}
