class EventFreeRequest {
  int? itemId;
  String? productType;
  int? quantity;
  String? userName;
  String? phone;
  String? email;
  String? note;
  int? provinceId;

  EventFreeRequest({
    this.itemId,
    this.productType,
    this.quantity,
    this.userName,
    this.phone,
    this.email,
    this.note,
    this.provinceId,
  });

  EventFreeRequest.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId']?.toInt();
    productType = json['productType']?.toString();
    quantity = json['quantity']?.toInt();
    userName = json['userName']?.toString();
    phone = json['phone']?.toString();
    email = json['email']?.toString();
    note = json['note']?.toString();
    provinceId = json['provinceId']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['itemId'] = itemId;
    data['productType'] = productType;
    data['quantity'] = quantity;
    data['userName'] = userName;
    data['phone'] = phone;
    data['email'] = email;
    data['note'] = note;
    data['provinceId'] = provinceId;
    return data;
  }
}
