/// postId : 1
/// parentId : 1
/// text : "Nothing to say"

class CommentRequest {
  CommentRequest({
      int? postId, 
      int? parentId, 
      String? text,}){
    _postId = postId;
    _parentId = parentId;
    _text = text;
}

  CommentRequest.fromJson(dynamic json) {
    _postId = json['postId'];
    _parentId = json['parentId'];
    _text = json['text'];
  }
  int? _postId;
  int? _parentId;
  String? _text;

  int? get postId => _postId;
  int? get parentId => _parentId;
  String? get text => _text;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['postId'] = _postId;
    if (_parentId != null)
    map['parentId'] = _parentId;
    if (_text != null)
    map['text'] = _text;
    return map;
  }

}