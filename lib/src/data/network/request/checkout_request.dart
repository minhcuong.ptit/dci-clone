class CheckoutRequest {
  int? id;

  CheckoutRequest({
    this.id,
  });
  CheckoutRequest.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    return data;
  }
}
