class NotificationChatRequest {
  String? channelId;
  String? content;
  String? contentType;

  NotificationChatRequest({
    this.channelId,
    this.content,
    this.contentType
  });

  NotificationChatRequest.fromJson(Map<String, dynamic> json) {
    channelId = json['channelId']?.toString();
    content = json['content']?.toString();
    contentType = json['contentType']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['channelId'] = channelId;
    data['content'] = content;
    data['contentType'] = contentType;
    return data;
  }
}
