class KisEnrollMetadata {
  int? provinceId;
  String? simMotorPictureLink;
  String? simCarPictureLink;

  KisEnrollMetadata({
    this.provinceId,
    this.simMotorPictureLink,
    this.simCarPictureLink,
  });
}

class KisEnrollForm {
  String? simMotor;
  String? simMotorPicture;
  String? simCar;
  String? simCarPicture;

  KisEnrollForm({
    this.simMotor,
    this.simMotorPicture,
    this.simCar,
    this.simCarPicture,
  });

  KisEnrollForm.fromJson(Map<String, dynamic> json) {
    simMotor = json["simMotor"];
    simMotorPicture = json["simMotorPicture"];
    simCar = json["simCar"];
    simCarPicture = json["simCarPicture"];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..["simMotor"] = simMotor
    ..["simMotorPicture"] = simMotorPicture
    ..["simCar"] = simCar
    ..["simCarPicture"] = simCarPicture;
}

class KisEnrollRequest {
  KisEnrollMetadata? metadata;
  KisEnrollForm? form;
  List<int>? kisProvinceIds;

  KisEnrollRequest({
    this.metadata,
    this.form,
    this.kisProvinceIds,
  });
  KisEnrollRequest.fromJson(Map<String, dynamic> json) {
    form = KisEnrollForm.fromJson(json['form']);
    if (json['kisProvinceIds'] != null) {
      final v = json['kisProvinceIds'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      kisProvinceIds = arr0;
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..["form"] = form?.toJson()
    ..["kisProvinceIds"] = kisProvinceIds;
}
