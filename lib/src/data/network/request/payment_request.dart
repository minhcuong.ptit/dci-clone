class PaymentRequest {
  int? id;
  String? paymentType;
  String? buyerName;
  String? buyerEmail;
  String? buyerPhone;
  String? buyerAddress;
  String? buyerCity;
  String? buyerCountry;

  PaymentRequest({
    this.id,
    this.paymentType,
    this.buyerName,
    this.buyerEmail,
    this.buyerPhone,
    this.buyerAddress,
    this.buyerCity,
    this.buyerCountry,
  });

  PaymentRequest.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    paymentType = json['paymentType']?.toString();
    buyerName = json['buyerName']?.toString();
    buyerEmail = json['buyerEmail']?.toString();
    buyerPhone = json['buyerPhone']?.toString();
    buyerAddress = json['buyerAddress']?.toString();
    buyerCity = json['buyerCity']?.toString();
    buyerCountry = json['buyerCountry']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['paymentType'] = paymentType;
    data['buyerName'] = buyerName;
    data['buyerEmail'] = buyerEmail;
    data['buyerPhone'] = buyerPhone;
    data['buyerAddress'] = buyerAddress;
    data['buyerCity'] = buyerCity;
    data['buyerCountry'] = buyerCountry;
    return data;
  }
}
