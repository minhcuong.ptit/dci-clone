class ChangeStatusRequest {
  String? status;

  ChangeStatusRequest({
    this.status,

  });

  ChangeStatusRequest.fromJson(Map<String, dynamic> json) {
    status = json['status']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    return data;
  }
}
