// To parse this JSON data, do
//
//     final profileRequest = profileRequestFromJson(jsonString);

import 'dart:convert';

ChangePasswordRequest profileRequestFromJson(String str) =>
    ChangePasswordRequest.fromJson(json.decode(str));

String profileRequestToJson(ChangePasswordRequest data) => json.encode(data.toJson());

class ChangePasswordRequest {
  ChangePasswordRequest({
    this.oldPinCode,
    this.newPinCode,
  });

  String? newPinCode;
  String? oldPinCode;

  factory ChangePasswordRequest.fromJson(Map<String, dynamic> json) => ChangePasswordRequest(
      oldPinCode: json["oldPinCode"], newPinCode: json["newPinCode"]);

  Map<String, dynamic> toJson() => {
        "oldPinCode": oldPinCode,
        "newPinCode": newPinCode,
      };
}
