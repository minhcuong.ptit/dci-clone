import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/enum.dart';

class UpdateMemberRoleRequest {
  late List<UpdateMemberRolePayload> data;

  UpdateMemberRoleRequest({
    required this.data,
  });

  UpdateMemberRoleRequest.fromJson(Map<String, dynamic> json) {
    data = [];
    if (json['data'] != null) {
      json['data'].forEach((v) {
        data.add(UpdateMemberRolePayload.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() =>
      <String, dynamic>{}..['data'] = data.map((e) => e.toJson()).toList();
}

class UpdateMemberRolePayload {
/*
{
  "memberId": 0,
  "clubId": 0,
  "communityId": 0,
  "roles": [
    "SUB_MEMBER"
  ]
}
*/

  int? memberId;
  int? clubId;
  int? communityId;
  List<MemberRole>? roles;

  UpdateMemberRolePayload({
    this.memberId,
    this.clubId,
    this.communityId,
    this.roles,
  });
  UpdateMemberRolePayload.fromJson(Map<String, dynamic> json) {
    memberId = json['memberId']?.toInt();
    clubId = json['clubId']?.toInt();
    communityId = json['communityId']?.toInt();
    List<MemberRole?> rawRoles = json['roles']
            ?.map<MemberRole?>((e) => MemberRole.values
                .firstWhereOrNull((element) => element.name == e))
            ?.toList() ??
        [];
    roles = rawRoles.whereNotNull().toList();
  }
  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['memberId'] = memberId
    ..['clubId'] = clubId
    ..['communityId'] = communityId
    ..['roles'] = roles?.map((e) => e.name).toList();
}
