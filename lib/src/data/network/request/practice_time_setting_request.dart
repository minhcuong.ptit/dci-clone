class ReminderMeditationCoffee {
  int? time;
  bool? enable;

  ReminderMeditationCoffee({
    this.time,
    this.enable,
  });

  ReminderMeditationCoffee.fromJson(Map<String, dynamic> json) {
    time = json['time']?.toInt();
    enable = json['enable'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['time'] = time;
    data['enable'] = enable;
    return data;
  }
}

class ReminderPlanSeed {
  int? time;
  bool? enable;

  ReminderPlanSeed({
    this.time,
    this.enable,
  });

  ReminderPlanSeed.fromJson(Map<String, dynamic> json) {
    time = json['time']?.toInt();
    enable = json['enable'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['time'] = time;
    data['enable'] = enable;
    return data;
  }
}

class ReminderMeditation {
  List<int>? times;
  bool? enable;

  ReminderMeditation({
    this.times,
    this.enable,
  });

  ReminderMeditation.fromJson(Map<String, dynamic> json) {
    if (json['times'] != null) {
      final v = json['times'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      times = arr0;
    }
    enable = json['enable'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (times != null) {
      final v = times;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['times'] = arr0;
    }
    data['enable'] = enable;
    return data;
  }
}

class Reminder {
  ReminderMeditation? meditation;
  ReminderPlanSeed? planSeed;
  ReminderMeditationCoffee? meditationCoffee;

  Reminder({
    this.meditation,
    this.planSeed,
    this.meditationCoffee,
  });

  Reminder.fromJson(Map<String, dynamic> json) {
    meditation = (json['meditation'] != null)
        ? ReminderMeditation.fromJson(json['meditation'])
        : null;
    planSeed = (json['planSeed'] != null)
        ? ReminderPlanSeed.fromJson(json['planSeed'])
        : null;
    meditationCoffee = (json['meditationCoffee'] != null)
        ? ReminderMeditationCoffee.fromJson(json['meditationCoffee'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (meditation != null) {
      data['meditation'] = meditation!.toJson();
    }
    if (planSeed != null) {
      data['planSeed'] = planSeed!.toJson();
    }
    if (meditationCoffee != null) {
      data['meditationCoffee'] = meditationCoffee!.toJson();
    }
    return data;
  }
}

class RequestNotification {
  Reminder? reminder;

  RequestNotification({
    this.reminder,
  });

  RequestNotification.fromJson(Map<String, dynamic> json) {
    reminder =
        (json['reminder'] != null) ? Reminder.fromJson(json['reminder']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (reminder != null) {
      data['reminder'] = reminder!.toJson();
    }
    return data;
  }
}

class PracticeTimeSettingRequest {
  RequestNotification? notification;

  PracticeTimeSettingRequest({
    this.notification,
  });

  PracticeTimeSettingRequest.fromJson(Map<String, dynamic> json) {
    notification = (json['notification'] != null)
        ? RequestNotification.fromJson(json['notification'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (notification != null) {
      data['notification'] = notification!.toJson();
    }
    return data;
  }
}
