/// id : 1
/// name : "string"
/// type : "INDIVIDUAL"
/// ownerId : 0
/// imageUrl : "string"
/// avatarUrl : "string"
/// followerNo : 0
/// memberNo : 0
/// personInCharge : "string"
/// personInChargeName : "string"
/// clubName : "string"
/// clubCategories : "string"
/// kisCategories : "string"
/// bankName : "string"
/// bankHolderName : "string"
/// bankNumber : "string"
/// picPhoneNumber : "string"
/// clubStatus : "string"
/// permission : "string"
/// documents : ["string"]
/// provinceId : 0
/// provinceName : "string"
/// members : [{"id":0,"userPackageId":0,"role":"string","phone":"string","ktp":"string","ktaNumber":"string","userUuid":"string","status":"string"}]
/// phone : "string"
/// address : "string"
/// email : "string"
/// clubId : 0
/// bloodType : "string"
/// birthPlace : "string"
/// hobby : "string"
/// invitorPhoneNumber : "string"
/// gender : "string"
/// profilePicture : "string"
/// ktp : "string"
/// ktpPicture : "string"
/// sim : "string"
/// simPicture : "string"
/// dob : 0
/// ktaNumber : "string"
/// postalCode : "string"
/// expiredDate : 0
/// registerTime : 0

class UpdateCommunityRequest {
  UpdateCommunityRequest(
      {int? id,
      String? name,
      String? type,
      int? ownerId,
      String? imageUrl,
      String? avatarUrl,
      int? followerNo,
      int? memberNo,
      String? personInCharge,
      String? personInChargeName,
      String? clubName,
      String? clubCategories,
      String? kisCategories,
      String? bankName,
      String? bankHolderName,
      String? bankNumber,
      String? picPhoneNumber,
      String? clubStatus,
      String? permission,
      List<String>? documents,
      int? provinceId,
      String? provinceName,
      List<Members>? members,
      String? phone,
      String? address,
      String? email,
      int? clubId,
      String? bloodType,
      String? birthPlace,
      String? hobby,
      String? invitorPhoneNumber,
      String? gender,
      String? profilePicture,
      String? ktp,
      String? ktpPicture,
      String? sim,
      String? simPicture,
      String? dob,
      String? ktaNumber,
      String? postalCode,
      int? expiredDate,
      int? registerTime,
      String? description,
        String? communityCategories,}) {
    _id = id;
    _name = name;
    _type = type;
    _ownerId = ownerId;
    _imageUrl = imageUrl;
    _avatarUrl = avatarUrl;
    _followerNo = followerNo;
    _memberNo = memberNo;
    _personInCharge = personInCharge;
    _personInChargeName = personInChargeName;
    _clubName = clubName;
    _clubCategories = clubCategories;
    _kisCategories = kisCategories;
    _bankName = bankName;
    _bankHolderName = bankHolderName;
    _bankNumber = bankNumber;
    _picPhoneNumber = picPhoneNumber;
    _clubStatus = clubStatus;
    _permission = permission;
    _documents = documents;
    _provinceId = provinceId;
    _provinceName = provinceName;
    _members = members;
    _phone = phone;
    _address = address;
    _email = email;
    _clubId = clubId;
    _bloodType = bloodType;
    _birthPlace = birthPlace;
    _hobby = hobby;
    _invitorPhoneNumber = invitorPhoneNumber;
    _gender = gender;
    _profilePicture = profilePicture;
    _ktp = ktp;
    _ktpPicture = ktpPicture;
    _sim = sim;
    _simPicture = simPicture;
    _dob = dob;
    _ktaNumber = ktaNumber;
    _postalCode = postalCode;
    _expiredDate = expiredDate;
    _registerTime = registerTime;
    _description = description;
    _communityCategories = communityCategories;
  }

  UpdateCommunityRequest.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _type = json['type'];
    _ownerId = json['ownerId'];
    _imageUrl = json['imageUrl'];
    _avatarUrl = json['avatarUrl'];
    _followerNo = json['followerNo'];
    _memberNo = json['memberNo'];
    _personInCharge = json['personInCharge'];
    _personInChargeName = json['personInChargeName'];
    _clubName = json['clubName'];
    _clubCategories = json['clubCategories'];
    _kisCategories = json['kisCategories'];
    _bankName = json['bankName'];
    _bankHolderName = json['bankHolderName'];
    _bankNumber = json['bankNumber'];
    _picPhoneNumber = json['picPhoneNumber'];
    _clubStatus = json['clubStatus'];
    _permission = json['permission'];
    _documents = json['documents'] != null ? json['documents']?.cast<String>() : [];
    _provinceId = json['provinceId'];
    _provinceName = json['provinceName'];
    if (json['members'] != null) {
      _members = [];
      json['members'].forEach((v) {
        _members?.add(Members.fromJson(v));
      });
    }
    _phone = json['phone'];
    _address = json['address'];
    _email = json['email'];
    _clubId = json['clubId'];
    _bloodType = json['bloodType'];
    _birthPlace = json['birthPlace'];
    _hobby = json['hobby'];
    _invitorPhoneNumber = json['invitorPhoneNumber'];
    _gender = json['gender'];
    _profilePicture = json['profilePicture'];
    _ktp = json['ktp'];
    _ktpPicture = json['ktpPicture'];
    _sim = json['sim'];
    _simPicture = json['simPicture'];
    _dob = json['dob'];
    _ktaNumber = json['ktaNumber'];
    _postalCode = json['postalCode'];
    _expiredDate = json['expiredDate'];
    _registerTime = json['registerTime'];
    _description = json['description'];
    _communityCategories = json['communityCategories'];
  }

  int? _id;
  String? _name;
  String? _type;
  int? _ownerId;
  String? _imageUrl;
  String? _avatarUrl;
  int? _followerNo;
  int? _memberNo;
  String? _personInCharge;
  String? _personInChargeName;
  String? _clubName;
  String? _clubCategories;
  String? _kisCategories;
  String? _bankName;
  String? _bankHolderName;
  String? _bankNumber;
  String? _picPhoneNumber;
  String? _clubStatus;
  String? _permission;
  List<String>? _documents;
  int? _provinceId;
  String? _provinceName;
  List<Members>? _members;
  String? _phone;
  String? _address;
  String? _email;
  int? _clubId;
  String? _bloodType;
  String? _birthPlace;
  String? _hobby;
  String? _invitorPhoneNumber;
  String? _gender;
  String? _profilePicture;
  String? _ktp;
  String? _ktpPicture;
  String? _sim;
  String? _simPicture;
  String? _dob;
  String? _ktaNumber;
  String? _postalCode;
  int? _expiredDate;
  int? _registerTime;
  String? _description;
  String? _communityCategories;

  int? get id => _id;

  String? get name => _name;

  String? get type => _type;

  int? get ownerId => _ownerId;

  String? get imageUrl => _imageUrl;

  String? get avatarUrl => _avatarUrl;

  int? get followerNo => _followerNo;

  int? get memberNo => _memberNo;

  String? get personInCharge => _personInCharge;

  String? get personInChargeName => _personInChargeName;

  String? get clubName => _clubName;

  String? get clubCategories => _clubCategories;

  String? get kisCategories => _kisCategories;

  String? get bankName => _bankName;

  String? get bankHolderName => _bankHolderName;

  String? get bankNumber => _bankNumber;

  String? get picPhoneNumber => _picPhoneNumber;

  String? get clubStatus => _clubStatus;

  String? get permission => _permission;

  List<String>? get documents => _documents;

  int? get provinceId => _provinceId;

  String? get provinceName => _provinceName;

  List<Members>? get members => _members;

  String? get phone => _phone;

  String? get address => _address;

  String? get email => _email;

  int? get clubId => _clubId;

  String? get bloodType => _bloodType;

  String? get birthPlace => _birthPlace;

  String? get hobby => _hobby;

  String? get invitorPhoneNumber => _invitorPhoneNumber;

  String? get gender => _gender;

  String? get profilePicture => _profilePicture;

  String? get ktp => _ktp;

  String? get ktpPicture => _ktpPicture;

  String? get sim => _sim;

  String? get simPicture => _simPicture;

  String? get dob => _dob;

  String? get ktaNumber => _ktaNumber;

  String? get postalCode => _postalCode;

  int? get expiredDate => _expiredDate;

  int? get registerTime => _registerTime;

  String? get description => _description;

  String? get communityCategories => _communityCategories;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_id != null) map['id'] = _id;
    if (_name != null) map['name'] = _name;
    if (_type != null) map['type'] = _type;
    if (_ownerId != null) map['ownerId'] = _ownerId;
    if (_imageUrl != null) map['imageUrl'] = _imageUrl;
    if (_avatarUrl != null) map['avatarUrl'] = _avatarUrl;
    if (_followerNo != null) map['followerNo'] = _followerNo;
    if (_memberNo != null) map['memberNo'] = _memberNo;
    if (_personInCharge != null) map['personInCharge'] = _personInCharge;
    if (_personInChargeName != null) map['personInChargeName'] = _personInChargeName;
    if (_clubName != null) map['clubName'] = _clubName;
    if (_clubCategories != null) map['clubCategories'] = _clubCategories;
    if (_kisCategories != null) map['kisCategories'] = _kisCategories;
    if (_bankName != null) map['bankName'] = _bankName;
    if (_bankHolderName != null) map['bankHolderName'] = _bankHolderName;
    if (_bankNumber != null) map['bankNumber'] = _bankNumber;
    if (_picPhoneNumber != null) map['picPhoneNumber'] = _picPhoneNumber;
    if (_clubStatus != null) map['clubStatus'] = _clubStatus;
    if (_permission != null) map['permission'] = _permission;
    if (_documents != null) map['documents'] = _documents;
    if (_provinceId != null) map['provinceId'] = _provinceId;
    if (_provinceName != null) map['provinceName'] = _provinceName;
    if (_members != null) {
      map['members'] = _members?.map((v) => v.toJson()).toList();
    }
    if (_phone != null) map['phone'] = _phone;
    if (_address != null) map['address'] = _address;
    if (_email != null) map['email'] = _email;
    if (_clubId != null) map['clubId'] = _clubId;
    if (_bloodType != null) map['bloodType'] = _bloodType;
    if (_birthPlace != null) map['birthPlace'] = _birthPlace;
    if (_hobby != null) map['hobby'] = _hobby;
    if (_invitorPhoneNumber != null) map['invitorPhoneNumber'] = _invitorPhoneNumber;
    if (_gender != null) map['gender'] = _gender;
    if (_profilePicture != null) map['profilePicture'] = _profilePicture;
    if (_ktp != null) map['ktp'] = _ktp;
    if (_ktpPicture != null) map['ktpPicture'] = _ktpPicture;
    if (_sim != null) map['sim'] = _sim;
    if (_simPicture != null) map['simPicture'] = _simPicture;
    if (_dob != null) map['dob'] = _dob;
    if (_ktaNumber != null) map['ktaNumber'] = _ktaNumber;
    if (_postalCode != null) map['postalCode'] = _postalCode;
    if (_expiredDate != null) map['expiredDate'] = _expiredDate;
    if (_registerTime != null) map['registerTime'] = _registerTime;
    if (_description != null) map['description'] = _description;
    if (_communityCategories != null) map['communityCategories'] = _communityCategories;
    return map;
  }
}

/// id : 0
/// userPackageId : 0
/// role : "string"
/// phone : "string"
/// ktp : "string"
/// ktaNumber : "string"
/// userUuid : "string"
/// status : "string"

class Members {
  Members({
    int? id,
    int? userPackageId,
    String? role,
    String? phone,
    String? ktp,
    String? ktaNumber,
    String? userUuid,
    String? status,
  }) {
    _id = id;
    _userPackageId = userPackageId;
    _role = role;
    _phone = phone;
    _ktp = ktp;
    _ktaNumber = ktaNumber;
    _userUuid = userUuid;
    _status = status;
  }

  Members.fromJson(dynamic json) {
    _id = json['id'];
    _userPackageId = json['userPackageId'];
    _role = json['role'];
    _phone = json['phone'];
    _ktp = json['ktp'];
    _ktaNumber = json['ktaNumber'];
    _userUuid = json['userUuid'];
    _status = json['status'];
  }

  int? _id;
  int? _userPackageId;
  String? _role;
  String? _phone;
  String? _ktp;
  String? _ktaNumber;
  String? _userUuid;
  String? _status;

  int? get id => _id;

  int? get userPackageId => _userPackageId;

  String? get role => _role;

  String? get phone => _phone;

  String? get ktp => _ktp;

  String? get ktaNumber => _ktaNumber;

  String? get userUuid => _userUuid;

  String? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['userPackageId'] = _userPackageId;
    map['role'] = _role;
    map['phone'] = _phone;
    map['ktp'] = _ktp;
    map['ktaNumber'] = _ktaNumber;
    map['userUuid'] = _userUuid;
    map['status'] = _status;
    return map;
  }
}
