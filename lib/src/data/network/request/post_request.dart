import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/utils/enum.dart';

abstract class PostMetadata {
  PostType get type;
  Map<String, dynamic> toJson();

  static PostMetadata fromJson(String? rawJson) {
    try {
      dynamic metadata = json.decode(rawJson ?? "");
      PostType type = PostType.values.firstWhereOrNull(
              (element) => element.name == metadata['type']) ??
          PostType.MEDIA;
      switch (type) {
        case PostType.MEDIA:
          return PostMetadataMedia.fromJson(metadata);
        case PostType.FREETEXT:
          return PostMetadataFreeText.fromJson(metadata);
        case PostType.LINK:
          return PostMetadataLink.fromJson(metadata);
      }
    } catch (e) {
      // can not parse metadata
      return PostMetadataMedia();
    }
  }
}

class PostMetadataMedia implements PostMetadata {
  PostMetadataMedia();

  PostMetadataMedia.fromJson(dynamic json) {}

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{}..['type'] = type.name;

  @override
  PostType get type => PostType.MEDIA;
}

class PostMetadataFreeText implements PostMetadata {
  PostMetadataFreeText();

  PostMetadataFreeText.fromJson(dynamic json) {}

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{}..['type'] = type.name;

  @override
  PostType get type => PostType.FREETEXT;
}

class PostMetadataLink implements PostMetadata {
  String? url;
  String? rootUrl;
  String? title;
  String? description;
  String? image;

  PostMetadataLink(
      {this.url, this.rootUrl, this.title, this.description, this.image});

  PostMetadataLink.fromJson(dynamic json) {
    url = json['url'];
    rootUrl = json['rootUrl'];
    title = json['title'];
    description = json['description'];
    image = json['image'];
  }

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['type'] = type.name
    ..["url"] = url
    ..["rootUrl"] = rootUrl
    ..["title"] = title
    ..["description"] = description
    ..["image"] = image;

  @override
  PostType get type => PostType.LINK;
}

class PostRequest {
  PostRequest({
    String? type,
    String? text,
    String? textColor,
    PostMetadata? metadata,
    String? backgroundColor,
    List<String>? backgroundImages,
    String? privacy,
    int? communityId,
  }) {
    _type = type;
    _text = text;
    _textColor = textColor;
    _metadata = metadata;
    _backgroundColor = backgroundColor;
    _backgroundImages = backgroundImages;
    _privacy = privacy;
    _communityId = communityId;
  }

  PostRequest.fromJson(dynamic json) {
    _type = json['type'];
    _text = json['text'];
    _textColor = json['textColor'];
    _metadata = PostMetadata.fromJson(json['description']);
    _backgroundColor = json['backgroundColor'];
    _backgroundImages = json['backgroundImages'] != null
        ? json['backgroundImages'].cast<String>()
        : [];
    _privacy = json['privacy'];
    _communityId = json['communityId'];
  }
  String? _type;
  String? _text;
  String? _textColor;
  PostMetadata? _metadata;
  String? _backgroundColor;
  List<String>? _backgroundImages;
  String? _privacy;
  int? _communityId;

  String? get type => _type;
  String? get text => _text;
  String? get textColor => _textColor;
  PostMetadata? get metadata => _metadata;
  String? get backgroundColor => _backgroundColor;
  List<String>? get backgroundImages => _backgroundImages;
  String? get privacy => _privacy;
  int? get communityId => _communityId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['text'] = _text;
    map['textColor'] = _textColor;
    map['description'] = json.encode(_metadata);
    map['backgroundColor'] = _backgroundColor;
    map['backgroundImages'] = _backgroundImages;
    map['privacy'] = _privacy;
    map['communityId'] = _communityId;
    return map;
  }
}
