class GroupMemberActionRequest {
  List<String>? userUuid;
  int clubId;
  GroupMemberActionRequest({
    required this.userUuid,
    required this.clubId,
  });

  GroupMemberActionRequest copyWith({
    List<String>? userUuid,
    int? clubId,
  }) {
    return GroupMemberActionRequest(
      userUuid: userUuid ?? this.userUuid,
      clubId: clubId ?? this.clubId,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'userUuid': userUuid,
      'clubId': clubId,
    };
  }
}
