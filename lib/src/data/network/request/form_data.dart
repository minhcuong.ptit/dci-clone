/// key : "string"
/// values : ["string"]

class FormData {
  FormData({
    String? key,
    List<String>? values,}){
    _key = key;
    _values = values;
  }

  FormData.fromJson(dynamic json) {
    _key = json['key'];
    _values = json['values'] != null ? json['values'].cast<String>() : [];
  }
  String? _key;
  List<String>? _values;

  String? get key => _key;
  List<String>? get values => _values;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['key'] = _key;
    map['values'] = _values;
    return map;
  }

}