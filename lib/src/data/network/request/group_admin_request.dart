import 'dart:convert';

import 'package:flutter/foundation.dart';

class GroupAdminRequest {
  final int communityId;
  final List<GroupMemberRequest> members;
  GroupAdminRequest({
    required this.communityId,
    required this.members,
  });

  GroupAdminRequest copyWith({
    int? communityId,
    List<GroupMemberRequest>? members,
  }) {
    return GroupAdminRequest(
      communityId: communityId ?? this.communityId,
      members: members ?? this.members,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'communityId': communityId,
      'members': members.map((x) => x.toJson()).toList(),
    };
  }
}

class GroupMemberRequest {
  final List<String> role;
  final String userUuid;
  GroupMemberRequest({
    required this.role,
    required this.userUuid,
  });

  GroupMemberRequest copyWith({
    List<String>? role,
    String? userUuid,
  }) {
    return GroupMemberRequest(
      role: role ?? this.role,
      userUuid: userUuid ?? this.userUuid,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'role': role,
      'userUuid': userUuid,
    };
  }

  factory GroupMemberRequest.fromJson(Map<String, dynamic> map) {
    return GroupMemberRequest(
      role: List<String>.from(map['role']),
      userUuid: map['userUuid'] ?? '',
    );
  }
}
