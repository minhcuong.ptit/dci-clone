import 'dart:convert';

class SeenMessageRequest {
  final String channelId;
  final int timeToken;
  final String chatType;
  SeenMessageRequest({
    required this.chatType,
    required this.channelId,
    required this.timeToken,
  });

  Map<String, dynamic> toJson() {
    return {'channelId': channelId, 'timeToken': timeToken, 'type': chatType};
  }
}
