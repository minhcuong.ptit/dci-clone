import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/data/network/response/member_data.dart';
import 'package:imi/src/utils/enum.dart';

import 'form_data.dart';

/// membershipType : "string"
/// packageId : 0
/// forms : [{"key":"string","values":["string"]}]
/// members : [{"id":0,"userPackageId":0,"role":"string","phone":"string","ktp":"string","ktaNumber":"string","userUuid":"string","status":"string"}]

class CreateFormRequest {
  CreateFormRequest({
    this.membershipType,
    this.packageId,
    this.userPackageId,
    this.userUuid,
    this.forms,
    this.members,
    this.associationMembers,
  });

  CreateFormRequest.fromJson(dynamic json) {
    membershipType = MembershipType.values
        .firstWhereOrNull((element) => element.name == json['membershipType']);
    userUuid = json['userUuid'];
    packageId = json['packageId'];
    userPackageId = json['userPackageId'];
    if (json['forms'] != null) {
      forms = [];
      json['forms'].forEach((v) {
        forms?.add(FormData.fromJson(v));
      });
    }
    if (json['members'] != null) {
      members = [];
      json['members'].forEach((v) {
        members?.add(MemberData.fromJson(v));
      });
    }
    if (json['associationMembers'] != null) {
      associationMembers = [];
      json['associationMembers'].forEach((v) {
        associationMembers?.add(MemberData.fromJson(v));
      });
    }
  }

  MembershipType? membershipType;
  String? userUuid;
  int? packageId;
  int? userPackageId;
  List<FormData>? forms;
  List<MemberData>? members;
  List<MemberData>? associationMembers;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['membershipType'] = membershipType?.name;
    map['userUuid'] = userUuid;
    map['packageId'] = packageId;
    if (userPackageId != null) map['userPackageId'] = userPackageId;
    if (forms != null) {
      map['forms'] = forms?.map((v) => v.toJson()).toList();
    }
    if (members != null) {
      map['members'] = members?.map((v) => v.toJson()).toList();
    }
    if (associationMembers != null) {
      map['associationMembers'] =
          associationMembers?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
