/// objectId : 10000
/// type : "PACKAGE"
/// totalFee : 160000
/// products : [{"qty":1,"product":"Package 1","amount":150000}]

class BillRequest {
  BillRequest({
      int? objectId,
    int? userPackageId,
    String? type,
      num? totalFee,
      List<Products>? products,}){
    _objectId = objectId;
    _userPackageId = userPackageId;
    _type = type;
    _totalFee = totalFee;
    _products = products;
}

  BillRequest.fromJson(dynamic json) {
    _objectId = json['objectId'];
    _userPackageId = json['userPackageId'];
    _type = json['type'];
    _totalFee = json['totalFee'];
    if (json['products'] != null) {
      _products = [];
      json['products'].forEach((v) {
        _products?.add(Products.fromJson(v));
      });
    }
  }
  int? _objectId;
  int? _userPackageId;
  String? _type;
  num? _totalFee;
  List<Products>? _products;

  int? get objectId => _objectId;
  int? get userPackageId => _userPackageId;
  String? get type => _type;
  num? get totalFee => _totalFee;
  List<Products>? get products => _products;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['objectId'] = _objectId;
    map['userPackageId'] = _userPackageId;
    map['type'] = _type;
    map['totalFee'] = _totalFee;
    if (_products != null) {
      map['products'] = _products?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// qty : 1
/// product : "Package 1"
/// amount : 150000

class Products {
  Products({
      int? qty, 
      String? product, 
      num? amount,}){
    _qty = qty;
    _product = product;
    _amount = amount;
}

  Products.fromJson(dynamic json) {
    _qty = json['qty'];
    _product = json['product'];
    _amount = json['amount'];
  }
  int? _qty;
  String? _product;
  num? _amount;

  int? get qty => _qty;
  String? get product => _product;
  num? get amount => _amount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['qty'] = _qty;
    map['product'] = _product;
    map['amount'] = _amount;
    return map;
  }

}