class TimeTokenReadRequest {
  String? channelId;
  int? timeToken;
  String? userUuid;

  TimeTokenReadRequest({
    this.channelId,
    this.timeToken,
    this.userUuid,
  });

  TimeTokenReadRequest.fromJson(Map<String, dynamic> json) {
    channelId = json['channelId']?.toString();
    timeToken = json['timeToken']?.toInt();
    userUuid = json['userUuid']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['channelId'] = channelId;
    data['timeToken'] = timeToken;
    data['userUuid'] = userUuid;
    return data;
  }
}
