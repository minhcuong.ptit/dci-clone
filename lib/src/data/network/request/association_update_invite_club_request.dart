import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/enum.dart';

class AssociationUpdateInviteClubRequest {
  AssociationClubStatusType? status;

  AssociationUpdateInviteClubRequest({
    this.status,
  });
  AssociationUpdateInviteClubRequest.fromJson(Map<String, dynamic> json) {
    status = AssociationClubStatusType.values
        .firstWhereOrNull((element) => element.name == json['status']);
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status?.name;
    return data;
  }
}
