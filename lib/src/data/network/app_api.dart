import 'dart:io';

import 'package:dio/dio.dart';
import 'package:imi/src/data/network/request/apply_coupon_request.dart';
import 'package:imi/src/data/network/request/association_invite_club_request.dart';
import 'package:imi/src/data/network/request/association_update_invite_club_request.dart';
import 'package:imi/src/data/network/request/buy_more_request.dart';
import 'package:imi/src/data/network/request/change_language_request.dart';
import 'package:imi/src/data/network/request/change_status_request.dart';
import 'package:imi/src/data/network/request/checkout_request.dart';
import 'package:imi/src/data/network/request/clean_seeds_request.dart';
import 'package:imi/src/data/network/request/comment_request.dart';
import 'package:imi/src/data/network/request/create_form_request.dart';
import 'package:imi/src/data/network/request/create_order_request.dart';
import 'package:imi/src/data/network/request/create_support_chat_request.dart';
import 'package:imi/src/data/network/request/create_zen_request.dart';
import 'package:imi/src/data/network/request/event_free_request.dart';
import 'package:imi/src/data/network/request/follow_request.dart';
import 'package:imi/src/data/network/request/group_admin_request.dart';
import 'package:imi/src/data/network/request/group_member_request.dart';
import 'package:imi/src/data/network/request/kis_enroll_request.dart';
import 'package:imi/src/data/network/request/mission_request.dart';
import 'package:imi/src/data/network/request/notification_chat_request.dart';
import 'package:imi/src/data/network/request/payment_request.dart';
import 'package:imi/src/data/network/request/post_request.dart';
import 'package:imi/src/data/network/request/practice_time_setting_request.dart';
import 'package:imi/src/data/network/request/profile_request.dart';
import 'package:imi/src/data/network/request/reaction_request.dart';
import 'package:imi/src/data/network/request/report_request.dart';
import 'package:imi/src/data/network/request/submit_favorite_request.dart';
import 'package:imi/src/data/network/request/time_token_read_messages_request.dart';
import 'package:imi/src/data/network/request/update_community_request.dart';
import 'package:imi/src/data/network/request/update_member_request.dart';
import 'package:imi/src/data/network/request/update_member_role_request.dart';
import 'package:imi/src/data/network/request/update_order_items_request.dart';
import 'package:imi/src/data/network/request/update_password_request.dart';
import 'package:imi/src/data/network/request/admin_update_community_request.dart';
import 'package:imi/src/data/network/response/association_club.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/country.dart';
import 'package:imi/src/data/network/response/create_order_response.dart';
import 'package:imi/src/data/network/response/create_zen_response.dart';
import 'package:imi/src/data/network/response/e_card_data.dart';
import 'package:imi/src/data/network/response/image_seed_data.dart';
import 'package:imi/src/data/network/response/kis_province_list.dart';
import 'package:imi/src/data/network/response/level_data.dart';
import 'package:imi/src/data/network/response/my_individual.dart';
import 'package:imi/src/data/network/response/payment_online_response.dart';
import 'package:imi/src/data/network/response/profile.dart';
import 'package:imi/src/data/network/response/request_to_join.dart';
import 'package:imi/src/data/network/response/send_comment_response.dart';
import 'package:imi/src/data/network/response/sowing_diary_response.dart';
import 'package:imi/src/data/network/response/start_challenge_response.dart';
import 'package:imi/src/data/network/response/target_response.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';
import 'package:retrofit/retrofit.dart' as retrofit;

import 'request/bill_request.dart';
import 'request/login_request.dart';
import 'request/logout_request.dart';
import 'request/member_action_request.dart';
import 'request/seen_message_request.dart';
import 'request/update_profile_request_v2.dart';
import 'request/upgrade_form_request.dart';
import 'response/bill_response.dart';
import 'response/city_response.dart';
import 'response/detail_package_response.dart';
import 'response/form_response.dart';
import 'response/list_category_response.dart';
import 'response/list_community_response.dart';
import 'response/login_response.dart';
import 'response/member_dto.dart';
import 'response/upload_list_url_response.dart';
import 'response/upload_url_response.dart';

part 'app_api.g.dart';

@RestApi()
abstract class AppApi {
  factory AppApi(Dio dio, {String baseUrl}) = _AppApi;

  @PUT("v2/user/profile")
  Future<dynamic> updateProfile(@Body() UpdateProfileRequestV2 request);

  @POST("login")
  Future<LoginResponse> login(@Body() LoginRequest request);

  @POST("{authEndpoint}")
  @FormUrlEncoded()
  Future<dynamic> auth(
      @Path() String authEndpoint, @Body() LoginRequest request);

  @PUT("v1/profile/avatar")
  Future<UploadUrlResponse> getUploadUrl(
      @Query("mediaFileName") String fileName);

  @GET("v1/file/upload-url")
  Future<List<UploadListUrlResponse>> getListUploadUrl(
      @Queries() Map<String, dynamic> queries);

  @PUT("{url}")
  @retrofit.Headers(<String, dynamic>{
    "Content-Type": "application/octet-stream",
    "Ocp-Apim-Subscription-Key": "abc"
  })
  Future<dynamic> uploadAvatar(
    @Path() String url,
    @Body() File file,
  );

  @POST("{logoutEndpoint}")
  @FormUrlEncoded()
  Future<dynamic> logout(
      @Path() String logoutEndpoint, @Body() LogoutRequest request);

  @POST("refresh")
  Future<LoginResponse> refreshToken(@Body() Map<String, dynamic> map);

  @GET("choose-category")
  Future<ListCategoryResponse> getListCategory();

  @GET("choose-community")
  Future<ListCommunityResponse> getListCommunity();

  @GET("cities")
  Future<CityResponse> getListCity();

  @PUT("v1/community/favorite")
  Future submitFavorite(@Body() SubmitFavoriteRequest request);

  @POST("v1/form")
  Future<FormResponse> createForm(@Body() CreateFormRequest request);

  @POST("v1/form/upgrade")
  Future<BillResponse> upgradeForm(@Body() UpgradeFormRequest request);

  @POST("v1/form/renew")
  Future<BillResponse> renewForm(@Body() UpgradeFormRequest request);

  @POST("v1/member/action")
  Future<MemberDTO> memberAction(@Body() MemberActionRequest request);

  @POST("v1/post")
  Future<dynamic> createPost(@Body() PostRequest request);

  @PUT("v1/community/{communityId}")
  Future<dynamic> updateCommunity(@Path("communityId") int? communityId,
      @Body() UpdateCommunityRequest request);

  @DELETE("v1/post/{postId}")
  Future<dynamic> removePost(@Path("postId") int postId);

  @PUT("v1/post/{postId}")
  Future<dynamic> editPost(
      @Body() PostRequest request, @Path("postId") String postId);

  // E Card
  @GET("v1/package")
  Future<List<ECardData>> getListECard(@Query("membershipId") int membershipId);

  @GET("v1/package")
  Future<List<ECardData>> getDetailPackage(
      @Query("packageCode") String? packageCode);

  @GET("v1/addon/{type}")
  Future<ECardData> getAddOnByType(@Path("type") String type);

  @POST("v1/transaction/bill")
  Future<BillResponse> getBill(@Body() BillRequest request);

  @POST("v1/comment")
  Future<SendCommentResponse> comment(@Body() CommentRequest request);

  @POST("/v1/bookmark")
  Future<dynamic> bookmark(@Body() ReactionRequest request);

  @POST("/v1/reaction")
  Future<dynamic> reaction(@Body() ReactionRequest request);

  @GET("v1/membership/my-individual")
  Future<MyIndividual> myIndividual();

  @GET("v1/user-package/{userPackageId}/info")
  Future<DetailPackageResponse> getDetailUserPackage(
      @Path("userPackageId") int userPackageId);

  @POST("v1/notifications/read")
  Future<dynamic> markNotificationsRead();

  @PUT("v1/notification/read")
  Future<dynamic> markNotificationRead(@Query("notificationId") int? id,
      @Query("readAction") String? readAction);

  @POST("v1/following")
  Future<dynamic> follow(@Body() FollowRequest request);

  // KIS
  @GET("v1/kis-province")
  Future<KisProvinceList> getListKis(@Query("provinceId") int provinceId);

  @POST("v2/membership/kis")
  Future<dynamic> enrollKis(@Body() KisEnrollRequest request);

  @DELETE("v2/membership/kis")
  Future<dynamic> removeKis(@Query("kisProvinceIds") List<int> kisProvinceIds);

  // Members
  @PUT("v1/member/{memberId}")
  Future<dynamic> updateMember(
      @Path("memberId") int memberId, @Body() UpdateMemberRequest request);

  @POST("v1/member/role")
  Future<dynamic> updateMemberRole(@Body() UpdateMemberRoleRequest request);

  // Association Invites
  @POST("v1/association/club")
  Future<AssociationClub> inviteClubToAssociation(
      @Body() AssociationInviteClubRequest request);

  @PUT("v1/association/club")
  Future<dynamic> updateClubAssociationInvite(
      @Query("idAssociationClub") int idAssociationClub,
      @Body() AssociationUpdateInviteClubRequest request);

  // Payment
  @POST("v2/payment/checkout")
  Future<dynamic> checkoutPayment(@Body() CheckoutRequest request);

  // Group member action
  @POST("/v3/user/community/member/{memberActionType}")
  Future<dynamic> groupMemberAction(
      @Path("memberActionType") String memberActionType,
      @Body() GroupMemberActionRequest request);

  @POST("/v3/user/community/member/invitation/{adminAction}")
  Future<dynamic> acceptInvitation(@Path("adminAction") String adminAction,
      @Body() GroupMemberActionRequest request);

  @GET("/v3/group/{communityId}/members/request")
  Future<RequestToJoin> getListRequestToJoin(
      @Path("communityId") int communityId, @Query("page") int page);

  @POST("/v3/admin/community/member/{adminAction}")
  Future<dynamic> approveRequest(
      @Path("adminAction") String adminAction,
      @Query('userUuid') String userUuid,
      @Query('communityId') int communityId);
  // @GET("/v3/admin/community/group/{communityId}/member")
  // Future<MemberListResponse> getMembersList(
  //     @Path('communityId') int communityId, @Query('page') int page,
  //     [@Query('size') int size = 20]);

  @GET("/v1/profile/phone/{phoneNumber}")
  Future<List<Profile>> getProfileByPhone(
      @Path("phoneNumber") String phoneNumber);

  @GET("/v1/countries")
  Future<List<Country>> getCountries();

  @DELETE("v1/comment")
  Future<dynamic> removeComment(
      @Query("commentId") int commentId, @Query("communityId") int communityId);

  @PUT("/v1/profile/{userUuid}")
  Future<dynamic> profileUpdate(
      @Path("userUuid") String userUuid, @Body() ProfileRequest request);

  @PUT("v1/comment")
  Future<dynamic> updateComment(@Body() CommentRequest request);

  @PUT("/v2/user/profile/{userUuid}/change-pin-code")
  Future<dynamic> changePinCode(
      @Path("userUuid") String userUuid, @Body() ChangePasswordRequest request);

  // update, delete member
  @POST("/v3/admin/community/group/member/{memberActionType}")
  Future<dynamic> updateDeleteMember(
      @Path("memberActionType") String memberActionType,
      @Body() GroupAdminRequest request);

  @GET("/v1/levels")
  Future<List<LevelData>> getLevels();

  @GET("v1/category?type={typeCategory}")
  Future<List<CategoryData>> getCategories(
      @Path("typeCategory") String typeCategory);

  @PUT("/v1/device")
  Future<dynamic> changeLanguage(@Body() ChangeLanguageRequest request);

  @POST("/v1/seed/clean")
  Future<dynamic> createCleanSeed(@Body() CleanSeedsRequest request);

  @PUT("/v1/seed/clean/{cleanSeedHistoryId}")
  Future<dynamic> updateCleanSeed(@Path("cleanSeedHistoryId") int cleanSeedId,
      @Body() CleanSeedsRequest request);

  @DELETE("/v1/seed/clean/{cleanSeedHistoryId}")
  Future<dynamic> deleteCleanSeed(
    @Path("cleanSeedHistoryId") int cleanSeedId,
  );

  @POST("/v1/seed/plant/history")
  Future<dynamic> createSeed(@Body() SeedingDiary request);

  @GET("/v1/seed/goals")
  Future<TargetResponse> getTarget();

  @PUT("/v1/seed/plant/history/{plantSeedHistoryId}")
  Future<dynamic> updatePlanSeed(
      @Path("plantSeedHistoryId") int plantSeedHistoryId,
      @Body() SeedingDiary request);

  @POST("/v1/report/post/{postId}")
  Future<dynamic> reportPost(
      @Path("postId") String postId, @Body() ReportRequest request);

  @POST("/v1/report/users/{userUuid}")
  Future<dynamic> reportUser(
      @Path("userUuid") String userUuid, @Body() ReportRequest request);

  @DELETE("/v1/seed/plant/history/{plantSeedHistoryId}")
  Future<dynamic> deletePlanSeed(
      @Path("plantSeedHistoryId") int plantSeedHistoryId);

  @POST("/v1/seed/plant/history/{plantSeedHistoryId}/copy")
  Future<dynamic> copyPlanSeed(
      @Path("plantSeedHistoryId") int plantSeedHistoryId,
      @Body() SeedingDiary request);

  @POST("/v1/report/post/{postId}/{reportPostAction}")
  Future<dynamic> deletePost(@Path("postId") String postId,
      @Path("reportPostAction") String reportPostAction);

  @GET("/v1/file/download-url/plant/{plantSeedHistoryId}")
  Future<ImageSeedData> getImageSeed(
      @Path("plantSeedHistoryId") int plantSeedHistoryId);

  @POST("/v2/profile/{userUuid}/{action}")
  Future<dynamic> blockUser(
      @Path("userUuid") String userUuid, @Path("action") String action);

  @POST("/v1/meditations")
  Future<CreateZenResponse> createZen(@Body() CreateZenRequest request);

  @GET("/v1/file/download-url/clean/{cleanSeedHistoryId}")
  Future<ImageSeedData> getImageCleanSeed(
      @Path("cleanSeedHistoryId") int cleanSeedHistoryId);

  @POST("/v1/challenge/{challengeId}/history")
  Future<StartChallengeResponse> startPractice(
      @Path("challengeId") int challengeId);

  @POST("/v1/user/setting")
  Future<dynamic> getPracticeTimeSetting(
      @Body() PracticeTimeSettingRequest request);

  @POST("/v1/challenge/{challengeId}/history/{challengeHistoryId}")
  Future<dynamic> getMission(
      @Body() MissionRequest request,
      @Path("challengeId") int challengeId,
      @Path("challengeHistoryId") int challengeHistoryId);

  @DELETE("/v1/challenge/{challengeId}/history/{challengeHistoryId}")
  Future<dynamic> cancelMission(@Path("challengeId") int challengeId,
      @Path("challengeHistoryId") int challengeHistoryId);

  @DELETE("/v1/profile/me")
  Future<dynamic> deleteCleanAccount();

  @POST("/v1/order")
  Future<dynamic> orderCourse(@Body() EventFreeRequest request);

  @DELETE("/v1/order/{orderId}/order-items/{orderDetailId}")
  Future<dynamic> deleteOrderItem(
      @Path("orderId") int orderId, @Path("orderDetailId") int orderDetailId);

  @POST("/v1/order/{orderId}/coupons")
  Future<dynamic> applyCoupon(
      @Body() ApplyCouponRequest request, @Path("orderId") int orderId);

  @PUT("/v1/order/{orderId}/order-items/{orderDetailId}/quantity/{quantity}")
  Future<dynamic> updateOrderQuantity(@Path("orderId") int orderId,
      @Path("orderDetailId") int orderDetailId, @Path("quantity") int quantity);

  @DELETE("/v1/order/{orderId}/coupons/{couponId}")
  Future<dynamic> deleteCoupon(
      @Path("orderId") int orderId, @Path("couponId") int couponId);

  @DELETE("/v1/order-history/{orderHistoryId}")
  Future<dynamic> cancelOrderHistory(@Path("orderHistoryId") int orderId,
      @Query("cancelReasonId") int cancelReasonId);

  @POST("/v1/order-history")
  Future<CreateOrderResponse> createOder(@Body() CreateOrderRequest request);

  @PUT("/v1/order/{orderId}/order-items/{orderDetailId}")
  Future<dynamic> updateOrderItem(@Body() UpdateOrderItemRequest request,
      @Path("orderId") int orderId, @Path("orderDetailId") int orderDetailId);

  @PUT("/v3/admin/community/group/{communityId}")
  Future<dynamic> adminUpdateCommunity(@Body() AdminUpdateCommunity request,
      @Path("communityId") int communityId);

  @POST("/v1/user/channel/individual")
  Future<dynamic> createPrivateChannel(@Query("userUuid") String userUID);

  @POST("/v1/channel/message/timeToken")
  Future<dynamic> updateMessageSeen(@Body() SeenMessageRequest request);

  @POST("/v1/support")
  Future<dynamic> createSupportChat(
      [@Body() CreateSupportChat request = const CreateSupportChat()]);

  @POST("/v1/channel/message/lastReadTimeToken")
  Future<dynamic> timeTokenReadMessages(@Body() TimeTokenReadRequest request);

  @GET("/v1/channel/group/{communityId}")
  Future<dynamic> getGroupChannel(@Path("communityId") int communityId);

  @GET("/v3/admin/community/group/{communityId}")
  Future<AdminUpdateCommunity> getInfoGroup(
      @Path("communityId") int communityId);

  @POST("/v1/channel/notification")
  Future<dynamic> notificationChat(@Body() NotificationChatRequest request);

  @PUT("/v1/admin/support/{supportChannelId}")
  Future<dynamic> changeStatusSupportChat( @Path("supportChannelId") String supportChannelId,@Body() ChangeStatusRequest request);

  @POST("/v1/order-history/event")
  Future<dynamic> orderEventFree(@Body() EventFreeRequest request);

  @POST("/v1/order/{orderId}/point/toggle")
  Future<dynamic> togglePayment( @Path("orderId") int orderId);

  @POST("/v2/payment/checkout")
  Future<PaymentOnlineResponse> paymentOnline(@Body() PaymentRequest request);

  @POST("/v1/order-buy-more")
  Future<dynamic> buyMore(@Body() BuyMoreRequest request);
}
