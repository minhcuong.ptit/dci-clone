class DetailNotificationDciAdmin {
  Data? data;

  DetailNotificationDciAdmin({this.data});

  DetailNotificationDciAdmin.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  GetPrivateNotificationDetailById? getPrivateNotificationDetailById;

  Data({this.getPrivateNotificationDetailById});

  Data.fromJson(Map<String, dynamic> json) {
    getPrivateNotificationDetailById =
        json['getPrivateNotificationDetailById'] != null
            ? new GetPrivateNotificationDetailById.fromJson(
                json['getPrivateNotificationDetailById'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.getPrivateNotificationDetailById != null) {
      data['getPrivateNotificationDetailById'] =
          this.getPrivateNotificationDetailById!.toJson();
    }
    return data;
  }
}

class GetPrivateNotificationDetailById {
  String? body;
  int? createdDate;
  int? id;
  String? text;

  GetPrivateNotificationDetailById(
      {this.body, this.createdDate, this.id, this.text});

  GetPrivateNotificationDetailById.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    createdDate = json['createdDate'];
    id = json['id'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    data['createdDate'] = this.createdDate;
    data['id'] = this.id;
    data['text'] = this.text;
    return data;
  }
}