import 'community_data.dart';

/// communities : {"data":[{"id":5,"name":"test","type":"CLUB","ownerId":1,"clubInfo":{"id":5,"bankName":null,"bankNumber":null,"bankHolderName":null,"personInCharge":"d4ef2d32-92ae-4fde-9012-794312ab8d14","personInChargeName":null},"individualInfo":null}]}

class GetClubResponse {
  GetClubResponse({
    Communities? communities,
  }) {
    _communities = communities;
  }

  GetClubResponse.fromJson(dynamic json) {
    _communities = json['communities'] != null
        ? Communities.fromJson(json['communities'])
        : null;
  }
  Communities? _communities;

  Communities? get communities => _communities;
}

/// data : [{"id":5,"name":"test","type":"CLUB","ownerId":1,"clubInfo":{"id":5,"bankName":null,"bankNumber":null,"bankHolderName":null,"personInCharge":"d4ef2d32-92ae-4fde-9012-794312ab8d14","personInChargeName":null},"individualInfo":null}]

class Communities {
  Communities({
    List<CommunityData>? data,
  }) {
    _data = data;
  }

  Communities.fromJson(dynamic json) {
    nextToken = json['nextToken'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CommunityData.fromJson(v));
      });
    }
  }
  List<CommunityData>? _data;
  String? nextToken;
  List<CommunityData>? get data => _data;
}
