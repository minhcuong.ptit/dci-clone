class TaaAssociationMembership {
  int? id;
  String? name;

  TaaAssociationMembership({
    this.id,
    this.name,
  });

  TaaAssociationMembership.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['id'] = id
    ..['name'] = name;
}
