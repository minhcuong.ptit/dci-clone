///
/// Code generated by jsonToDartModel https://ashamp.github.io/jsonToDartModel/
///
class PointActionDataFetchPointActionInCategory {
/*
{
  "id": 1,
  "name": "Thực hành DCI",
  "type": "PRACTICE"
}
*/

  int? id;
  String? name;
  String? type;

  PointActionDataFetchPointActionInCategory({
    this.id,
    this.name,
    this.type,
  });
  PointActionDataFetchPointActionInCategory.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
    type = json['type']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['type'] = type;
    return data;
  }
}

class PointActionDataFetchPointActionIn {
/*
{
  "id": 1,
  "name": "Ghi nhật ký gieo hạt",
  "action": "PLAN_SEED",
  "category": {
    "id": 1,
    "name": "Thực hành DCI",
    "type": "PRACTICE"
  },
  "actionType": "IN",
  "point": 13,
  "status": "ACTIVE",
  "maximumPoint": 0
}
*/

  int? id;
  String? name;
  String? action;
  PointActionDataFetchPointActionInCategory? category;
  String? actionType;
  int? point;
  String? status;
  int? maximumPoint;

  PointActionDataFetchPointActionIn({
    this.id,
    this.name,
    this.action,
    this.category,
    this.actionType,
    this.point,
    this.status,
    this.maximumPoint,
  });
  PointActionDataFetchPointActionIn.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
    action = json['action']?.toString();
    category = (json['category'] != null) ? PointActionDataFetchPointActionInCategory.fromJson(json['category']) : null;
    actionType = json['actionType']?.toString();
    point = json['point']?.toInt();
    status = json['status']?.toString();
    maximumPoint = json['maximumPoint']?.toInt();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['action'] = action;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['actionType'] = actionType;
    data['point'] = point;
    data['status'] = status;
    data['maximumPoint'] = maximumPoint;
    return data;
  }
}

class PointActionData {
/*
{
  "fetchPointActionIn": [
    {
      "id": 1,
      "name": "Ghi nhật ký gieo hạt",
      "action": "PLAN_SEED",
      "category": {
        "id": 1,
        "name": "Thực hành DCI",
        "type": "PRACTICE"
      },
      "actionType": "IN",
      "point": 13,
      "status": "ACTIVE",
      "maximumPoint": 0
    }
  ]
}
*/

  List<PointActionDataFetchPointActionIn?>? fetchPointActionIn;

  PointActionData({
    this.fetchPointActionIn,
  });
  PointActionData.fromJson(Map<String, dynamic> json) {
    if (json['fetchPointActionIn'] != null) {
      final v = json['fetchPointActionIn'];
      final arr0 = <PointActionDataFetchPointActionIn>[];
      v.forEach((v) {
        arr0.add(PointActionDataFetchPointActionIn.fromJson(v));
      });
      fetchPointActionIn = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchPointActionIn != null) {
      final v = fetchPointActionIn;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['fetchPointActionIn'] = arr0;
    }
    return data;
  }
}

class PointAction {
/*
{
  "data": {
    "fetchPointActionIn": [
      {
        "id": 1,
        "name": "Ghi nhật ký gieo hạt",
        "action": "PLAN_SEED",
        "category": {
          "id": 1,
          "name": "Thực hành DCI",
          "type": "PRACTICE"
        },
        "actionType": "IN",
        "point": 13,
        "status": "ACTIVE",
        "maximumPoint": 0
      }
    ]
  }
}
*/

  PointActionData? data;

  PointAction({
    this.data,
  });
  PointAction.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? PointActionData.fromJson(json['data']) : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
