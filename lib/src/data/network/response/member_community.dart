class MemberGroup {
  MemberOfCommunity? memberOfCommunity;

  MemberGroup({this.memberOfCommunity});

  MemberGroup.fromJson(Map<String, dynamic> json) {
    memberOfCommunity = json['memberOfCommunity'] != null
        ? new MemberOfCommunity.fromJson(json['memberOfCommunity'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.memberOfCommunity != null) {
      data['memberOfCommunity'] = this.memberOfCommunity!.toJson();
    }
    return data;
  }
}

class MemberOfCommunity {
  int? adminNo;
  List<ListUserGroup>? data;
  int? leaderNo;
  String? nextToken;
  int? totalMembers;

  MemberOfCommunity(
      {this.adminNo,
        this.data,
        this.leaderNo,
        this.nextToken,
        this.totalMembers});

  MemberOfCommunity.fromJson(Map<String, dynamic> json) {
    adminNo = json['adminNo'];
    if (json['data'] != null) {
      data = <ListUserGroup>[];
      json['data'].forEach((v) {
        data!.add(new ListUserGroup.fromJson(v));
      });
    }
    leaderNo = json['leaderNo'];
    nextToken = json['nextToken'];
    totalMembers = json['totalMembers'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adminNo'] = this.adminNo;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['leaderNo'] = this.leaderNo;
    data['nextToken'] = this.nextToken;
    data['totalMembers'] = this.totalMembers;
    return data;
  }
}

class ListUserGroup {
  CommunityV2? communityV2;
  List<String>? groupRoles;
  String? userUuid;

  ListUserGroup({this.communityV2, this.groupRoles, this.userUuid});

  ListUserGroup.fromJson(Map<String, dynamic> json) {
    communityV2 = json['communityV2'] != null
        ? new CommunityV2.fromJson(json['communityV2'])
        : null;
    groupRoles = json['groupRoles'].cast<String>();
    userUuid = json['userUuid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.communityV2 != null) {
      data['communityV2'] = this.communityV2!.toJson();
    }
    data['groupRoles'] = this.groupRoles;
    data['userUuid'] = this.userUuid;
    return data;
  }
}

class CommunityV2 {
  String? avatarUrl;
  String? name;
  int? id;

  CommunityV2({this.avatarUrl, this.name,this.id});

  CommunityV2.fromJson(Map<String, dynamic> json) {
    avatarUrl = json['avatarUrl'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatarUrl'] = this.avatarUrl;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}