class CurrentUserData {
  int? id;
  String? actionName;
  String? action;
  String? actionType;
  int? point;
  int? createdDate;
  String? sourceId;
  String? targetId;

  CurrentUserData({
    this.id,
    this.actionName,
    this.action,
    this.actionType,
    this.point,
    this.createdDate,
    this.sourceId,
    this.targetId,
  });

  CurrentUserData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    actionName = json['actionName']?.toString();
    action = json['action']?.toString();
    actionType = json['actionType']?.toString();
    point = json['point']?.toInt();
    createdDate = json['createdDate']?.toInt();
    sourceId = json['sourceId']?.toString();
    targetId = json['targetId']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['actionName'] = actionName;
    data['action'] = action;
    data['actionType'] = actionType;
    data['point'] = point;
    data['createdDate'] = createdDate;
    data['sourceId'] = sourceId;
    data['targetId'] = targetId;
    return data;
  }
}

class FetchPointHistory {
  String? nextToken;
  List<CurrentUserData>? data;

  FetchPointHistory({
    this.nextToken,
    this.data,
  });

  FetchPointHistory.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <CurrentUserData>[];
      v.forEach((v) {
        arr0.add(CurrentUserData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class HistoryPointData {
  FetchPointHistory? fetchPointHistoryOfCurrentUser;

  HistoryPointData({
    this.fetchPointHistoryOfCurrentUser,
  });

  HistoryPointData.fromJson(Map<String, dynamic> json) {
    fetchPointHistoryOfCurrentUser =
        (json['fetchPointHistoryOfCurrentUser'] != null)
            ? FetchPointHistory.fromJson(
                json['fetchPointHistoryOfCurrentUser'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchPointHistoryOfCurrentUser != null) {
      data['fetchPointHistoryOfCurrentUser'] =
          fetchPointHistoryOfCurrentUser!.toJson();
    }
    return data;
  }
}

class HistoryPointResponse {
  HistoryPointData? data;

  HistoryPointResponse({
    this.data,
  });

  HistoryPointResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? HistoryPointData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
