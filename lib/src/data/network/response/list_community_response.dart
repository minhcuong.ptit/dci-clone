import 'package:imi/src/data/network/response/community_data.dart';

/// communities : {"data":[{"id":1,"name":"Test","type":"CLUB","avatarUrl":null,"followerNo":null,"imageUrl":null,"memberNo":null,"ownerId":1},{"id":2,"name":"Redbull","type":"CLUB","avatarUrl":null,"followerNo":null,"imageUrl":null,"memberNo":null,"ownerId":1},{"id":3,"name":"string","type":"INDIVIDUAL","avatarUrl":"string","followerNo":0,"imageUrl":"string","memberNo":0,"ownerId":0}]}

class ListCommunityResponse {
  ListCommunityResponse({
    ListCommunityData? communities,
  }) {
    _communities = communities;
  }

  ListCommunityResponse.fromJson(dynamic json) {
    _communities = json['communities'] != null
        ? ListCommunityData.fromJson(json['communities'])
        : null;
  }
  ListCommunityData? _communities;

  ListCommunityData? get communities => _communities;
}

class ListPostingCommunityResponse {
  ListPostingCommunityResponse({
    List<CommunityData>? data,
  }) {
    _data = data;
  }

  ListPostingCommunityResponse.fromJson(dynamic json) {
    if (json['postingCommunities'] != null) {
      _data = [];
      json['postingCommunities'].forEach((v) {
        _data?.add(CommunityData.fromJson(v));
      });
    }
  }
  List<CommunityData>? _data;

  List<CommunityData>? get data => _data;
}

/// data : [{"id":1,"name":"Test","type":"CLUB","avatarUrl":null,"followerNo":null,"imageUrl":null,"memberNo":null,"ownerId":1},{"id":2,"name":"Redbull","type":"CLUB","avatarUrl":null,"followerNo":null,"imageUrl":null,"memberNo":null,"ownerId":1},{"id":3,"name":"string","type":"INDIVIDUAL","avatarUrl":"string","followerNo":0,"imageUrl":"string","memberNo":0,"ownerId":0}]

class ListCommunityData {
  ListCommunityData({
    List<CommunityData>? data,
  }) {
    _data = data;
  }

  ListCommunityData.fromJson(dynamic json) {
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CommunityData.fromJson(v));
      });
    }
  }
  List<CommunityData>? _data;

  List<CommunityData>? get data => _data;
}
