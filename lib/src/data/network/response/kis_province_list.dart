import 'package:imi/src/data/network/response/kis_province.dart';
import 'package:imi/src/utils/enum.dart';

class KisProvinceList {
  KisProvinceList({
    this.total,
    this.data,
  });

  KisProvinceList.fromJson(dynamic json) {
    total = json['total'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        KisProvince kisProvince = KisProvince.fromJson(v);
        if (kisProvince.status == KisStatus.ENABLE) {
          data?.add(kisProvince);
        }
      });
    }
  }
  int? total;
  List<KisProvince>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total'] = total;
    map['data'] = data;
    return map;
  }
}
