/// url : "string"
/// key : "string"

class UploadListUrlResponse {
  UploadListUrlResponse({
      String? url, 
      String? key,}){
    _url = url;
    _key = key;
}

  UploadListUrlResponse.fromJson(dynamic json) {
    _url = json['url'];
    _key = json['key'];
  }
  String? _url;
  String? _key;

  String? get url => _url;
  String? get key => _key;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = _url;
    map['key'] = _key;
    return map;
  }

}