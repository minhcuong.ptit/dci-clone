import 'package:hive/hive.dart';
import 'package:imi/src/utils/enum.dart';
part 'category_data.g.dart';
/// id : 1
/// name : "#Racing"
/// color : "#FF6B00"
/// order : 1
@HiveType(typeId: 1)
class CategoryData {
  CategoryData(
      {this.id,
      this.name,
      this.color,
      this.order,
      this.emoji,
      this.isSelected});

  CategoryData.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    color = json['color'];
    order = json['order'];
    emoji = json['emoji'];
    isSelected = json['isSelected'];
    type = null;
    for (var i in CategoryType.values) {
      if (i.name == json['type']) type = i;
    }
  }
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? name;
  @HiveField(2)
  String? color;
  @HiveField(3)
  int? order;
  @HiveField(4)
  String? emoji;
  @HiveField(5)
  bool? isSelected;
  @HiveField(6)
  CategoryType? type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['color'] = color;
    map['order'] = order;
    map['emoji'] = emoji;
    map['isSelected'] = isSelected;
    return map;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryData &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
