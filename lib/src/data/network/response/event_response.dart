import 'fetch_event_data.dart';

class FetchEvents {
  String? nextToken;
  List<FetchEventsData>? data;

  FetchEvents({
    this.nextToken,
    this.data,
  });

  FetchEvents.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchEventsData>[];
      v.forEach((v) {
        arr0.add(FetchEventsData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class EventData {
  FetchEvents? fetchEvents;

  EventData({
    this.fetchEvents,
  });

  EventData.fromJson(Map<String, dynamic> json) {
    fetchEvents = (json['fetchEvents'] != null)
        ? FetchEvents.fromJson(json['fetchEvents'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchEvents != null) {
      data['fetchEvents'] = fetchEvents!.toJson();
    }
    return data;
  }
}

class EventResponse {
  EventData? data;

  EventResponse({
    this.data,
  });

  EventResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? EventData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
