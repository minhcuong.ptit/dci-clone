import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../utils/enum.dart';
import 'community_data.dart';
import 'kta_membership.dart';

class PersonProfileResponse {
  PersonProfile? profile;

  PersonProfileResponse({this.profile});

  PersonProfileResponse.fromJson(dynamic json) {
    profile = PersonProfile.fromJson(json['profile']);
  }

  Map<String, dynamic> toJson() =>
      <String, dynamic>{}..['profile'] = profile?.toJson();
}

class PersonProfile {
  PersonProfile(
      {this.userUuid,
      this.avatarUrl,
      this.fullName,
      this.gender,
      this.email,
      this.phone,
      required this.status,
      this.birthday,
      this.provinceName,
      this.registeredDate,
      this.ktaMembership,
      this.districtId,
      this.provinceId,
      this.countryId,
      this.point,
      this.workingUnit,
      this.numberOfLeadGroup,
      this.currentPosition,
      this.achievement,
      this.countryName,
      this.communityV2,
      this.leaderType,});

  PersonProfile.fromJson(dynamic json) {
    userUuid = json['userUuid'];
    avatarUrl = json['avatarUrl'];
    fullName = json['fullName'];
    gender = json['gender'];
    email = json['email'];
    phone = json['phone'];
    intro = json['intro'];
    status = PersonProfileStatus.values
            .firstWhereOrNull((element) => element.name == json['status']) ??
        PersonProfileStatus.INIT;
    birthday = json['birthday'];
    provinceName = json['provinceName'];
    registeredDate = json['registeredDate'];
    districtId = json['districtId'];
    provinceId = json['provinceId'];
    countryId = json['countryId'];
    point = json['point'];
    workingUnit = json['workingUnit'];
    numberOfLeadGroup = json['numberOfLeadGroup'];
    currentPosition = json['currentPosition'];
    achievement = json['achievement'];
    countryName = json['countryName'];
    ktaMembership = (json['ktaMembership'] != null)
        ? KtaMembership.fromJson(json['ktaMembership'])
        : null;
    communityV2 = (json['community'] != null)
        ? CommunityDataV2.fromJson(json['community'])
        : null;
    leaderType = json['leaderType'];
  }

  String? userUuid;
  String? avatarUrl;
  String? fullName;
  String? gender;
  String? email;
  String? phone;
  String? intro;
  late PersonProfileStatus status;
  String? birthday;
  String? provinceName;
  int? registeredDate;
  int? districtId;
  int? provinceId;
  String? countryName;
  int? countryId;
  int? point;
  String? workingUnit;
  int? numberOfLeadGroup;
  String? currentPosition;
  String? achievement;
  KtaMembership? ktaMembership;
  CommunityDataV2? communityV2;
  String? leaderType;

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['userUuid'] = userUuid
    ..['avatarUrl'] = avatarUrl
    ..['fullName'] = fullName
    ..['gender'] = gender
    ..['email'] = email
    ..['phone'] = phone
    ..['intro'] = intro
    ..['status'] = status.name
    ..['birthday'] = birthday
    ..['status'] = status.name
    ..['provinceName'] = provinceName
    ..['registeredDate'] = registeredDate
    ..['ktaMembership'] = ktaMembership
    ..['districtId'] = districtId
    ..['provinceId'] = provinceId
    ..['point'] = point
    ..['workingUnit'] = workingUnit
    ..['numberOfLeadGroup'] = numberOfLeadGroup
    ..['currentPosition'] = currentPosition
    ..['achievement'] = achievement
    ..['countryId'] = countryId
    ..['leaderType'] = leaderType;
}
