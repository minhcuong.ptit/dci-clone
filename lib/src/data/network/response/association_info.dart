/// id : 0
/// adminUuid : "string"
/// adminName : "string"
/// associationName : "string"
/// associationCategories : "string"
/// associationCategoryLinks : ["string"]
/// bankName : "string"
/// bankHolderName : "string"
/// bankNumber : "string"
/// adminPhoneNumber : "string"
/// documents : ["string"]
/// documentLinks : ["string"]
/// artDocuments : ["string"]
/// artDocumentLinks : ["string"]
/// certDocuments : ["string"]
/// certDocumentLinks : ["string"]
/// provinceId : 0
/// provinceName : "string"
/// cityId : 0
/// cityName : "string"
/// districtId : 0
/// districtName : "string"
/// wardId : 0
/// wardName : "string"
/// rtRwNumber : "string"
/// associationMembers : [{"id":0,"associationId":0,"communityId":0,"phone":"string","nikNumber":"string","ktaNumber":"string","userUuid":"string","name":"string","status":"VERIFYING","title":"string","titles":["string"]}]
/// coverPicture : "string"
/// expiredDate : 0
/// address : "string"
/// ktaNumber : "string"
/// clubCount : 5
/// star : 1

class AssociationInfo {
  AssociationInfo({
    this.id,
    this.communityId,
    this.adminUuid,
    this.adminName,
    this.description,
    this.associationName,
    this.associationCategories,
    this.associationCategoryLinks,
    this.bankName,
    this.bankHolderName,
    this.bankNumber,
    this.adminPhoneNumber,
    this.documents,
    this.documentLinks,
    this.artDocuments,
    this.artDocumentLinks,
    this.certDocuments,
    this.certDocumentLinks,
    this.provinceId,
    this.provinceName,
    this.cityId,
    this.cityName,
    this.districtId,
    this.districtName,
    this.wardId,
    this.wardName,
    this.rtRwNumber,
    this.associationMembers,
    this.coverPicture,
    this.expiredDate,
    this.address,
    this.ktaNumber,
    this.clubCount,
    this.star,
  });

  AssociationInfo.fromJson(dynamic json) {
    id = json['id'];
    communityId = json['communityId'];
    adminUuid = json['adminUuid'];
    adminName = json['adminName'];
    description = json['description'];
    associationName = json['associationName'];
    associationCategories = json['associationCategories'];
    associationCategoryLinks = json['associationCategoryLinks'] != null
        ? json['associationCategoryLinks'].cast<String>()
        : [];
    bankName = json['bankName'];
    bankHolderName = json['bankHolderName'];
    bankNumber = json['bankNumber'];
    adminPhoneNumber = json['adminPhoneNumber'];
    documents = json['documents'] != null ? json['documents'].cast<String>() : [];
    documentLinks = json['documentLinks'] != null ? json['documentLinks'].cast<String>() : [];
    artDocuments = json['artDocuments'] != null ? json['artDocuments'].cast<String>() : [];
    artDocumentLinks =
        json['artDocumentLinks'] != null ? json['artDocumentLinks'].cast<String>() : [];
    certDocuments = json['certDocuments'] != null ? json['certDocuments'].cast<String>() : [];
    certDocumentLinks =
        json['certDocumentLinks'] != null ? json['certDocumentLinks'].cast<String>() : [];
    provinceId = json['provinceId'];
    provinceName = json['provinceName'];
    cityId = json['cityId'];
    cityName = json['cityName'];
    districtId = json['districtId'];
    districtName = json['districtName'];
    wardId = json['wardId'];
    wardName = json['wardName'];
    rtRwNumber = json['rtRwNumber'];
    if (json['associationMembers'] != null) {
      associationMembers = [];
      json['associationMembers'].forEach((v) {
        associationMembers?.add(AssociationMembers.fromJson(v));
      });
    }
    eCertificateNumber = json['eCertificateNumber'];
    coverPicture = json['coverPicture'];
    expiredDate = json['expiredDate'];
    address = json['address'];
    ktaNumber = json['ktaNumber'];
    clubCount = json['clubCount'];
    star = json['star'];
  }

  int? id;
  int? communityId;
  String? adminUuid;
  String? adminName;
  String? description;
  String? associationName;
  String? associationCategories;
  List<String>? associationCategoryLinks;
  String? bankName;
  String? bankHolderName;
  String? bankNumber;
  String? adminPhoneNumber;
  List<String>? documents;
  List<String>? documentLinks;
  List<String>? artDocuments;
  List<String>? artDocumentLinks;
  List<String>? certDocuments;
  List<String>? certDocumentLinks;
  int? provinceId;
  String? provinceName;
  int? cityId;
  String? cityName;
  int? districtId;
  String? districtName;
  int? wardId;
  String? wardName;
  String? rtRwNumber;
  List<AssociationMembers>? associationMembers;
  String? eCertificateNumber;
  String? coverPicture;
  int? expiredDate;
  String? address;
  String? ktaNumber;
  int? clubCount;
  int? star;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['communityId'] = communityId;
    map['adminUuid'] = adminUuid;
    map['adminName'] = adminName;
    map['description'] = description;
    map['associationName'] = associationName;
    map['associationCategories'] = associationCategories;
    map['associationCategoryLinks'] = associationCategoryLinks;
    map['bankName'] = bankName;
    map['bankHolderName'] = bankHolderName;
    map['bankNumber'] = bankNumber;
    map['adminPhoneNumber'] = adminPhoneNumber;
    map['eCertificateNumber'] = eCertificateNumber;
    map['documents'] = documents;
    map['documentLinks'] = documentLinks;
    map['artDocuments'] = artDocuments;
    map['artDocumentLinks'] = artDocumentLinks;
    map['certDocuments'] = certDocuments;
    map['certDocumentLinks'] = certDocumentLinks;
    map['provinceId'] = provinceId;
    map['provinceName'] = provinceName;
    map['cityId'] = cityId;
    map['cityName'] = cityName;
    map['districtId'] = districtId;
    map['districtName'] = districtName;
    map['wardId'] = wardId;
    map['wardName'] = wardName;
    map['rtRwNumber'] = rtRwNumber;
    if (associationMembers != null) {
      map['associationMembers'] = associationMembers?.map((v) => v.toJson()).toList();
    }
    map['coverPicture'] = coverPicture;
    map['expiredDate'] = expiredDate;
    map['address'] = address;
    map['ktaNumber'] = ktaNumber;
    map['clubCount'] = clubCount;
    map['star'] = star;
    return map;
  }
}

/// id : 0
/// associationId : 0
/// communityId : 0
/// phone : "string"
/// nikNumber : "string"
/// ktaNumber : "string"
/// userUuid : "string"
/// name : "string"
/// status : "VERIFYING"
/// title : "string"
/// titles : ["string"]

class AssociationMembers {
  AssociationMembers({
    this.id,
    this.associationId,
    this.communityId,
    this.phone,
    this.nikNumber,
    this.ktaNumber,
    this.userUuid,
    this.name,
    this.status,
    this.title,
    this.titles,
  });

  AssociationMembers.fromJson(dynamic json) {
    id = json['id'];
    associationId = json['associationId'];
    communityId = json['communityId'];
    phone = json['phone'];
    nikNumber = json['nikNumber'];
    ktaNumber = json['ktaNumber'];
    userUuid = json['userUuid'];
    name = json['name'];
    status = json['status'];
    title = json['title'];
    titles = json['titles'] != null ? json['titles'].cast<String>() : [];
  }

  int? id;
  int? associationId;
  int? communityId;
  String? phone;
  String? nikNumber;
  String? ktaNumber;
  String? userUuid;
  String? name;
  String? status;
  String? title;
  List<String>? titles;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['associationId'] = associationId;
    map['communityId'] = communityId;
    map['phone'] = phone;
    map['nikNumber'] = nikNumber;
    map['ktaNumber'] = ktaNumber;
    map['userUuid'] = userUuid;
    map['name'] = name;
    map['status'] = status;
    map['title'] = title;
    map['titles'] = titles;
    return map;
  }
}
