/// code : ""
/// message : null

class CommonResponse {
  CommonResponse({
      String? code, 
      dynamic message,}){
    _code = code;
    _message = message;
}

  CommonResponse.fromJson(dynamic json) {
    _code = json['code'];
    _message = json['message'];
  }
  String? _code;
  dynamic _message;

  String? get code => _code;
  dynamic get message => _message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['message'] = _message;
    return map;
  }

}