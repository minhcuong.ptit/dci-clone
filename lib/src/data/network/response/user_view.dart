import 'dart:convert';

import 'gourp_of_member.dart';

class UserView {
/*
{
  "associationNamePromotor": null,
  "clubNamePromotor": null,
  "isFollowing": false,
  "numberPending": 0
}
*/

  String? associationNamePromotor;
  String? clubNamePromotor;
  bool? isFollowing;
  int? numberPending;
  List<GroupOfMember>? joinedGroups;
  UserView({
    this.associationNamePromotor,
    this.clubNamePromotor,
    this.isFollowing,
    this.numberPending,
    this.joinedGroups,
  });

  factory UserView.fromJson(Map<String, dynamic> map) {
    return UserView(
      associationNamePromotor: map['associationNamePromotor'],
      clubNamePromotor: map['clubNamePromotor'],
      isFollowing: map['isFollowing'],
      numberPending: map['numberPending']?.toInt(),
      joinedGroups: map['joinedGroups'] != null
          ? List<GroupOfMember>.from(
              map['joinedGroups']?.map((x) => GroupOfMember.fromJson(x)))
          : null,
    );
  }

  set following(bool? following) {
    isFollowing = following;
  }

  Map<String, dynamic> toJson() {
    return {
      'associationNamePromotor': associationNamePromotor,
      'clubNamePromotor': clubNamePromotor,
      'isFollowing': isFollowing,
      'numberPending': numberPending,
      'joinedGroups': joinedGroups?.map((x) => x.toJson()).toList(),
    };
  }
}
