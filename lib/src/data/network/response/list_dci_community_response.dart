import 'package:imi/src/data/network/response/community_data.dart';

class ListDCICommunityResponse {
  ListDCICommunityResponse({this.dciCommunities, this.nextToken});

  DciCommunities? dciCommunities;
  String? nextToken;

  factory ListDCICommunityResponse.fromJson(Map<String, dynamic>? json) =>
      ListDCICommunityResponse(
        dciCommunities: DciCommunities.fromJson(json?["dciCommunities"]),
        nextToken: json?['nextToken']?.toString(),
      );
}

class DciCommunities {
  DciCommunities({this.data, this.nextToken});
  String? nextToken;
  List<CommunityDataV2>? data;

  factory DciCommunities.fromJson(Map<String, dynamic> json) => DciCommunities(
        data: List<CommunityDataV2>.from(
            json["data"].map((x) => CommunityDataV2.fromJson(x))),
        nextToken: json['nextToken']?.toString(),
      );
}
