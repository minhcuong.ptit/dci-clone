class BuyMoreData {
  int? id;
  int? productId;
  int? quantity;
  String? name;
  int? preferentialFee;
  int? currentFee;
  String? avatarUrl;
  bool? isHot;

  BuyMoreData({
    this.id,
    this.productId,
    this.quantity,
    this.name,
    this.preferentialFee,
    this.currentFee,
    this.avatarUrl,
    this.isHot,
  });

  BuyMoreData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    productId = json['productId']?.toInt();
    quantity = json['quantity']?.toInt();
    name = json['name']?.toString();
    preferentialFee = json['preferentialFee']?.toInt();
    currentFee = json['currentFee']?.toInt();
    avatarUrl = json['avatarUrl']?.toString();
    isHot = json['isHot'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['quantity'] = quantity;
    data['name'] = name;
    data['preferentialFee'] = preferentialFee;
    data['currentFee'] = currentFee;
    data['avatarUrl'] = avatarUrl;
    data['isHot'] = isHot;
    return data;
  }
}

class FetchCoursesBuyMore {
  List<BuyMoreData>? data;

  FetchCoursesBuyMore({
    this.data,
  });

  FetchCoursesBuyMore.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <BuyMoreData>[];
      v.forEach((v) {
        arr0.add(BuyMoreData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class CourseByMoreData {
  FetchCoursesBuyMore? fetchCoursesBuyMore;

  CourseByMoreData({
    this.fetchCoursesBuyMore,
  });

  CourseByMoreData.fromJson(Map<String, dynamic> json) {
    fetchCoursesBuyMore = (json['fetchCoursesBuyMore'] != null)
        ? FetchCoursesBuyMore.fromJson(json['fetchCoursesBuyMore'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchCoursesBuyMore != null) {
      data['fetchCoursesBuyMore'] = fetchCoursesBuyMore!.toJson();
    }
    return data;
  }
}

class CourseByMoreResponse {
  CourseByMoreData? data;

  CourseByMoreResponse({
    this.data,
  });

  CourseByMoreResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? CourseByMoreData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
