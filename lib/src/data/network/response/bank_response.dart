/// banks : [{"id":0,"code":"code","name":"name"}]

class BankResponse {
  BankResponse({
      this.banks,});

  BankResponse.fromJson(dynamic json) {
    if (json['banks'] != null) {
      banks = [];
      json['banks'].forEach((v) {
        banks?.add(BankData.fromJson(v));
      });
    }
  }
  List<BankData>? banks;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (banks != null) {
      map['banks'] = banks?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 0
/// code : "code"
/// name : "name"

class BankData {
  BankData({
      this.id, 
      this.code, 
      this.name,});

  BankData.fromJson(dynamic json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
  }
  int? id;
  String? code;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['code'] = code;
    map['name'] = name;
    return map;
  }

}