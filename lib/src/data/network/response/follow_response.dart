import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/data/network/response/user_view.dart';

class FollowResponse {
  Follows? follows;

  FollowResponse({required Follows followCommunity}) {
    follows = followCommunity;
  }

  FollowResponse.fromJson(dynamic json) {
    follows = (json['followCommunities'] != null)
        ? Follows.fromJson(json['followCommunities'])
        : null;
  }
}

class FollowResponseV2 {
  FollowsV2? follows;

  FollowResponseV2({required FollowsV2 followCommunity}) {
    follows = followCommunity;
  }

  FollowResponseV2.fromJson(dynamic json) {
    follows = (json['followCommunitiesV2'] != null)
        ? FollowsV2.fromJson(json['followCommunitiesV2'])
        : null;
  }
}

class LikeResponse {
  Follows? likes;

  LikeResponse({required Follows followCommunity}) {
    likes = followCommunity;
  }

  LikeResponse.fromJson(dynamic json) {
    likes = (json['communitiesLikePost'] != null)
        ? Follows.fromJson(json['communitiesLikePost'])
        : null;
  }
}

class Follows {
  String? nextToken;
  List<FollowData>? data;

  Follows({
    this.nextToken,
    this.data,
  });

  Follows.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FollowData>[];
      v.forEach((v) {
        arr0.add(FollowData.fromJson(v));
      });
      this.data = arr0;
    }
  }
}

class FollowsV2 {
  String? nextToken;
  List<FollowDataV2>? data;

  FollowsV2({
    this.nextToken,
    this.data,
  });

  FollowsV2.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FollowDataV2>[];
      v.forEach((v) {
        arr0.add(FollowDataV2.fromMap(v));
      });
      this.data = arr0;
    }
  }
}

class FollowData {
  FollowData(
      {int? id,
      String? name,
      String? avatar,
      String? type,
      CommunityContent? communityContent}) {
    _id = id;
    _name = name;
    _avatar = avatar;
    _communityContent = communityContent;
    _type = type;
  }

  FollowData.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _avatar = json['avatarUrl'];
    _communityContent = CommunityContent.fromJson(json['myCommunityContent']);
    _type = json['type'];
  }

  int? _id;
  String? _name;
  String? _avatar;
  String? _type;
  CommunityContent? _communityContent;

  int? get id => _id;

  String? get name => _name;

  String? get type => _type;

  String? get avatarUrl => _avatar;

  CommunityContent? get communityContent => _communityContent;

  FollowDataV2 convertToV2() {
    return FollowDataV2(id, name, avatarUrl, type, CommunityInfo(), [],
        UserView(isFollowing: this.communityContent?.isFollowing),
        GroupView( ));
  }
}

class FollowDataV2 {
  int? id;
  String? name;
  String? avatarUrl;
  String? type;
  CommunityInfo? communityInfo;
  List<FollowDataV2>? memberOfCommunities;
  UserView? userView;
  GroupView? groupView;

  FollowDataV2(
    this.id,
    this.name,
    this.avatarUrl,
    this.type,
    this.communityInfo,
    this.memberOfCommunities,
    this.userView,
      this.groupView,
  );

  factory FollowDataV2.fromMap(Map<String, dynamic> map) {
    return FollowDataV2(
      map['id']?.toInt(),
      map['name'],
      map['avatarUrl'],
      map['type'],
      map['communityInfo'] != null
          ? CommunityInfo.fromJson(map['communityInfo'])
          : null,
      map['memberOfCommunities'] != null
          ? List<FollowDataV2>.from(
              map['memberOfCommunities']?.map((x) => FollowDataV2.fromMap(x)))
          : null,
      map['userView'] != null ? UserView.fromJson(map['userView']) : null,
      map['groupView'] != null ? GroupView.fromJson(map['groupView']) : null,
    );
  }
}

class CommunityContent {
  CommunityContent({bool? isFollowing, bool? isMember}) {
    _isMember = isMember;
    _isFollowing = isFollowing;
  }

  CommunityContent.fromJson(dynamic json) {
    _isMember = json['isMember'];
    _isFollowing = json['isFollowing'];
  }

  bool? _isFollowing = false;
  bool? _isMember = false;

  bool? get isFollowing => _isFollowing;

  set following(bool? isFollowing) {
    _isFollowing = isFollowing;
  }

  bool? get isMember => _isMember;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isMember'] = _isMember;
    map['isFollowing'] = _isFollowing;
    return map;
  }
}
