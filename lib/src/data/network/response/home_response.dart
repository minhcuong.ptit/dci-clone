import 'category_data.dart';
import 'list_community_response.dart';
import 'post_data.dart';


class HomeResponse {
  HomeResponse({
    PostPageData? posts,
    List<CategoryData>? categories,
    ListCommunityData? communities,
  }) {
    _posts = posts;
    _categories = categories;
    _communities = communities;
  }

  HomeResponse.fromJson(dynamic json) {
    _posts =
        json['posts'] != null ? PostPageData.fromJson(json['posts']) : null;
    if (json['categories'] != null) {
      _categories = [];
      json['categories'].forEach((v) {
        _categories?.add(CategoryData.fromJson(v));
      });
    }
    _communities = json['communities'] != null
        ? ListCommunityData.fromJson(json['communities'])
        : null;
  }
  PostPageData? _posts;
  List<CategoryData>? _categories;
  ListCommunityData? _communities;

  PostPageData? get posts => _posts;
  List<CategoryData>? get categories => _categories;
  ListCommunityData? get communities => _communities;
}
