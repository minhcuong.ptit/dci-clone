/// assign : true
/// title : "PERSONINCHARGE"
///
/// userUuid : "239b3fda-c77a-47b4-b2c0-f15ebff417d2"
/// phone : "820000002"
/// name : "820000002"
/// nikNumber : "0010000002"
/// ktaNumber : "00000011002"

class MemberData {
  MemberData({
    this.assign,
    this.title,
    this.userUuid,
    this.phone,
    this.name,
    this.nikNumber,
    this.ktaNumber,
  });

  MemberData.fromJson(dynamic json) {
    assign = json['assign'];
    title = json['title'];
    userUuid = json['userUuid'];
    phone = json['phone'];
    name = json['name'];
    nikNumber = json['nikNumber'];
    ktaNumber = json['ktaNumber'];
  }

  bool? assign;
  String? title;
  String? userUuid;
  String? phone;
  String? name;
  String? nikNumber;
  String? ktaNumber;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (assign != null)
    map['assign'] = assign;
    if (title != null) map['title'] = title;
    if (userUuid != null) map['userUuid'] = userUuid;
    if (phone != null) map['phone'] = phone;
    if (name != null) map['name'] = name;
    if (nikNumber != null) map['nikNumber'] = nikNumber;
    if (ktaNumber != null) map['ktaNumber'] = ktaNumber;
    return map;
  }
}
