import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/date_util.dart';
import 'package:imi/src/utils/enum.dart';

/// id : 0
/// personInCharge : "string"
/// personInChargeName : "string"
/// clubName : "string"
/// clubCategories : "string"
/// clubCategoriesLink : ["string"]
/// bankName : "string"
/// bankHolderName : "string"
/// bankNumber : "string"
/// picPhoneNumber : "string"
/// clubStatus : "string"
/// permission : ["string"]
/// documents : ["string"]
/// documentLinks : ["string"]
/// artDocuments : ["string"]
/// artDocumentLinks : ["string"]
/// certDocuments : ["string"]
/// certDocumentLinks : ["string"]
/// provinceId : 0
/// provinceName : "string"
/// cityId : 0
/// cityName : "string"
/// districtId : 0
/// districtName : "string"
/// wardId : 0
/// wardName : "string"
/// rtRwNumber : "string"
/// members : [{"id":0,"clubId":0,"communityId":0,"role":"string","phone":"string","nikNumber":"string","ktaNumber":"string","userUuid":"string","name":"string","status":"VERIFYING","title":"PRESIDENT"}]
/// coverPicture : "string"
/// expiredDate : "string"
/// address : "string"
/// ktaNumber : "string"
/// imiPaid : "string"
/// eCertificateNumber : "string"
/// associationId : 0

class ClubInfo {
  ClubInfo(
      {this.id,
      this.description,
      this.personInCharge,
      this.personInChargeName,
      this.clubName,
      this.clubCategories,
      this.clubCategoriesLink,
      this.bankName,
      this.bankHolderName,
      this.bankNumber,
      this.picPhoneNumber,
      this.clubStatus,
      this.permission,
      this.documents,
      this.documentLinks,
      this.artDocuments,
      this.artDocumentLinks,
      this.certDocuments,
      this.certDocumentLinks,
      this.provinceId,
      this.provinceName,
      this.cityId,
      this.cityName,
      this.districtId,
      this.districtName,
      this.wardId,
      this.wardName,
      this.rtRwNumber,
      this.members,
      this.coverPicture,
      this.expiredDate,
      this.address,
      this.ktaNumber,
      this.imiPaid,
      this.eCertificateNumber,
      this.associationId});

  ClubInfo.fromJson(dynamic json) {
    id = json['id'];
    description = json['description'];
    personInCharge = json['personInCharge'];
    personInChargeName = json['personInChargeName'];
    clubName = json['clubName'];
    clubCategories = json['clubCategories'];
    clubCategoriesLink =
        json['clubCategoriesLink'] != null ? json['clubCategoriesLink'].cast<String>() : [];
    bankName = json['bankName'];
    bankHolderName = json['bankHolderName'];
    bankNumber = json['bankNumber'];
    picPhoneNumber = json['picPhoneNumber'];
    clubStatus =
        UserPackageStatus.values.firstWhereOrNull((element) => element.name == json['clubStatus']);
    permission = json['permission'] != null ? json['permission'].cast<String?>() : [];
    documents = json['documents'] != null ? json['documents'].cast<String?>() : [];
    documentLinks = json['documentLinks'] != null ? json['documentLinks'].cast<String?>() : [];
    artDocuments = json['artDocuments'] != null ? json['artDocuments'].cast<String?>() : [];
    artDocumentLinks =
        json['artDocumentLinks'] != null ? json['artDocumentLinks'].cast<String?>() : [];
    certDocuments = json['certDocuments'] != null ? json['certDocuments'].cast<String?>() : [];
    certDocumentLinks =
        json['certDocumentLinks'] != null ? json['certDocumentLinks'].cast<String?>() : [];
    provinceId = json['provinceId'];
    provinceName = json['provinceName'];
    cityId = json['cityId'];
    cityName = json['cityName'];
    districtId = json['districtId'];
    districtName = json['districtName'];
    wardId = json['wardId'];
    wardName = json['wardName'];
    rtRwNumber = json['rtRwNumber'];
    if (json['members'] != null) {
      members = [];
      json['members'].forEach((v) {
        members?.add(Members.fromJson(v));
      });
    }
    coverPicture = json['coverPicture'];
    expiredDate = json['expiredDate'];
    address = json['address'];
    ktaNumber = json['ktaNumber'];
    imiPaid = json['imiPaid'];
    eCertificateNumber = json['eCertificateNumber'];
    associationId = json['associationId'];
    description = json['description'];
    associationName = json['associationName'];
    memberCount = json['memberCount'];
    star = json['star'];
  }

  int? id;
  String? description;
  String? personInCharge;
  String? personInChargeName;
  String? clubName;
  String? clubCategories;
  List<String>? clubCategoriesLink;
  String? bankName;
  String? bankHolderName;
  String? bankNumber;
  String? picPhoneNumber;
  UserPackageStatus? clubStatus;
  List<String?>? permission;
  List<String?>? documents;
  List<String?>? documentLinks;
  List<String?>? artDocuments;
  List<String?>? artDocumentLinks;
  List<String?>? certDocuments;
  List<String?>? certDocumentLinks;
  int? provinceId;
  String? provinceName;
  int? cityId;
  String? cityName;
  int? districtId;
  String? districtName;
  int? wardId;
  String? wardName;
  String? rtRwNumber;
  List<Members>? members;
  String? coverPicture;
  int? expiredDate;
  String? address;
  String? ktaNumber;
  String? imiPaid;
  String? eCertificateNumber;
  int? associationId;
  String? associationName;
  int? memberCount;
  int? star;

  bool get isExpiredIn30Days {
    if (clubStatus == UserPackageStatus.APPROVED) {
      int now = DateTime.now().millisecondsSinceEpoch;
      return ((expiredDate ?? now) - now) <
          Const.PACKAGE_WARNING_EXPIRE_THRESHOLD_DAYS * 24 * 3600 * 1000;
    }
    return false;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['description'] = description;
    map['personInCharge'] = personInCharge;
    map['personInChargeName'] = personInChargeName;
    map['clubName'] = clubName;
    map['clubCategories'] = clubCategories;
    map['clubCategoriesLink'] = clubCategoriesLink;
    map['bankName'] = bankName;
    map['bankHolderName'] = bankHolderName;
    map['bankNumber'] = bankNumber;
    map['picPhoneNumber'] = picPhoneNumber;
    map['clubStatus'] = clubStatus;
    map['permission'] = permission;
    map['documents'] = documents;
    map['documentLinks'] = documentLinks;
    map['artDocuments'] = artDocuments;
    map['artDocumentLinks'] = artDocumentLinks;
    map['certDocuments'] = certDocuments;
    map['certDocumentLinks'] = certDocumentLinks;
    map['provinceId'] = provinceId;
    map['provinceName'] = provinceName;
    map['cityId'] = cityId;
    map['cityName'] = cityName;
    map['districtId'] = districtId;
    map['districtName'] = districtName;
    map['wardId'] = wardId;
    map['wardName'] = wardName;
    map['rtRwNumber'] = rtRwNumber;
    if (members != null) {
      map['members'] = members?.map((v) => v.toJson()).toList();
    }
    map['coverPicture'] = coverPicture;
    map['expiredDate'] = expiredDate;
    map['address'] = address;
    map['ktaNumber'] = ktaNumber;
    map['imiPaid'] = imiPaid;
    map['eCertificateNumber'] = eCertificateNumber;
    map['associationId'] = associationId;
    map['description'] = description;
    map['star'] = star;
    map['associationName'] = associationName;
    map['memberCount'] = memberCount;
    return map;
  }
}

/// id : 0
/// clubId : 0
/// communityId : 0
/// role : "string"
/// phone : "string"
/// nikNumber : "string"
/// ktaNumber : "string"
/// userUuid : "string"
/// name : "string"
/// status : "VERIFYING"
/// title : "PRESIDENT"

class Members {
  Members({
    this.id,
    this.clubId,
    this.communityId,
    this.role,
    this.phone,
    this.nikNumber,
    this.ktaNumber,
    this.userUuid,
    this.name,
    this.status,
    this.title,
  });

  Members.fromJson(dynamic json) {
    id = json['id'];
    clubId = json['clubId'];
    communityId = json['communityId'];
    role = json['role'];
    phone = json['phone'];
    nikNumber = json['nikNumber'];
    ktaNumber = json['ktaNumber'];
    userUuid = json['userUuid'];
    name = json['name'];
    status = json['status'];
    title = json['title'];
  }

  int? id;
  int? clubId;
  int? communityId;
  String? role;
  String? phone;
  String? nikNumber;
  String? ktaNumber;
  String? userUuid;
  String? name;
  String? status;
  String? title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['clubId'] = clubId;
    map['communityId'] = communityId;
    map['role'] = role;
    map['phone'] = phone;
    map['nikNumber'] = nikNumber;
    map['ktaNumber'] = ktaNumber;
    map['userUuid'] = userUuid;
    map['name'] = name;
    map['status'] = status;
    map['title'] = title;
    return map;
  }
}
