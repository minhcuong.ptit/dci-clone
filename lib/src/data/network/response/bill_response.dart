/// errorCode : "20211114136209"
/// data : "<form method='post' name='form' action='https://xpress-sandbox.faspay.co.id/v3/payment'><input type='hidden' name='merchant_id' value='34345'><input type='hidden' name='merchant_name' value='Gaspol'><input type='hidden' name='order_id' value='20211114136209'><input type='hidden' name='bill_no' value='20211114136209'><input type='hidden' name='bill_reff' value='20211114136209'><input type='hidden' name='bill_date' value='2021-11-14 13:06:20'><input type='hidden' name='bill_expired' value='2021-11-14 21:06:20'><input type='hidden' name='bill_desc' value='Pay for PACKAGE'><input type='hidden' name='bill_currency' value='IDR'><input type='hidden' name='bill_total' value='150000'><input type='hidden' name='bill_miscfee' value='0'><input type='hidden' name='bill_gross' value='150000'><input type='hidden' name='return_url' value='https://gaspol.co.id/callback'><input type='hidden' name='cust_no' value='2b412fb6-758f-410e-b9ee-aa500db4'><input type='hidden' name='display_cust' value='true'><input type='hidden' name='custName' value='service-account-localhost-test'><input type='hidden' name='custPhone' value='0123456789'><input type='hidden' name='custEmail' value='dev@gaspol.co.id'><input type='hidden' name='merchant_logo' value='https://e7.pngegg.com/pngimages/575/704/png-clipart-computer-icons-test-scalable-graphics-test-score-angle-pencil.png'><input type='hidden' name='signature' value='765765bec097f56edc0ac07b8a3fd47d2b40f527'><input type='hidden' name='term_condition' value='1'><input type='hidden' name='products' value='a:1:{i:0;a:3:{s:7:&quot;product&quot;;s:17:&quot;IMI CLUB STANDARD&quot;;s:6:&quot;amount&quot;;i:150000;s:3:&quot;qty&quot;;i:1;}}'></form><script> document.form.submit();</script>"

class BillResponse {
  BillResponse({
      String? errorCode, 
      String? data,}){
    _errorCode = errorCode;
    _data = data;
}

  BillResponse.fromJson(dynamic json) {
    _errorCode = json['errorCode'];
    _data = json['data'];
  }
  String? _errorCode;
  String? _data;

  String? get errorCode => _errorCode;
  String? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['errorCode'] = _errorCode;
    map['data'] = _data;
    return map;
  }

}