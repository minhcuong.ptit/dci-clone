class ReactionResponse {
  ReactionResponse({
      String? errorCode, 
      String? data,}){
    _errorCode = errorCode;
    _data = data;
}

  ReactionResponse.fromJson(dynamic json) {
    _errorCode = json['errorCode'];
    _data = json['data'];
  }
  String? _errorCode;
  String? _data;

  String? get errorCode => _errorCode;
  String? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['errorCode'] = _errorCode;
    map['data'] = _data;
    return map;
  }

}