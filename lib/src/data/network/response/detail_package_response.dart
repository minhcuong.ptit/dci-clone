import 'package:imi/src/data/network/response/association_info.dart';
import 'package:imi/src/utils/logger.dart';

import 'business_partner_info.dart';
import 'club_info.dart';
import 'merchant_info.dart';

/// transactionInfo : {"userPackageId":0,"packageId":0,"packageName":"string","packageCode":"string","transactionId":"string","initFee":0,"annualFee":0,"processingFee":0}
/// individualInfo : {"id":0,"userUuid":"string","name":"string","phone":"string","address":"string","email":"string","clubId":0,"clubName":"string","clubPic":"string","clubStatus":"string","picPhoneNumber":"string","bloodType":"string","birthPlace":"string","hobby":"string","invitorPhoneNumber":"string","gender":"string","profilePicture":"string","profilePictureLink":"string","coverPicture":"string","coverPictureLink":"string","nikNumber":"string","nikPicture":"string","nikPictureLink":"string","clubProvinceId":0,"clubProvinceName":"string","provinceId":0,"provinceName":"string","cityId":0,"cityName":"string","districtId":0,"districtName":"string","wardId":0,"wardName":"string","birthday":"string","ktaNumber":"string","postalCode":"string","expiredDate":0,"registerTime":0,"nationality":"string","packageName":"string","status":"string","rtRwNumber":"string","imiPaid":"string","clubPicName":"string","clubPicPhone":"string","referralCode":"string"}
/// clubInfo : {"id":0,"personInCharge":"string","personInChargeName":"string","clubName":"string","clubCategories":"string","clubCategoriesLink":["string"],"bankName":"string","bankHolderName":"string","bankNumber":"string","picPhoneNumber":"string","clubStatus":"string","permission":["string"],"documents":["string"],"documentLinks":["string"],"artDocuments":["string"],"artDocumentLinks":["string"],"certDocuments":["string"],"certDocumentLinks":["string"],"provinceId":0,"provinceName":"string","cityId":0,"cityName":"string","districtId":0,"districtName":"string","wardId":0,"wardName":"string","rtRwNumber":"string","members":[{"id":0,"clubId":0,"communityId":0,"role":"string","phone":"string","nikNumber":"string","ktaNumber":"string","userUuid":"string","name":"string","status":"VERIFYING","title":"PRESIDENT"}],"coverPicture":"string","expiredDate":"string","address":"string","ktaNumber":"string","imiPaid":"string","eCertificateNumber":"string","associationId":0}
/// merchantInfo : {"id":0,"name":"string","phone":"string","nikNumber":"string","merchantPic":"string","bankName":"string","bankHolderName":"string","bankNumber":"string","companyName":"string","companyNpwp":"string","hotline":"string","businessType":"string","companyAddress":"string","companyEmail":"string","documents":["string"],"userPackageId":0,"userUuid":"string","registerTime":0,"expiredDate":0}
/// promoterInfo : {"id":0,"phone":"string","name":"string","promotorName":"string","nikNumber":"string","ktaNumber":"string","documents":["string"],"userPackageId":0,"userUuid":"string"}

class DetailPackageResponse {
  DetailPackageResponse({
    this.transactionInfo,
    this.individualInfo,
    this.clubInfo,
    this.associationInfo,
    this.merchantInfo,
    this.promoterInfo,
  });

  DetailPackageResponse.fromJson(dynamic json) {
    transactionInfo =
        json['transactionInfo'] != null ? TransactionInfo.fromJson(json['transactionInfo']) : null;
    individualInfo =
        json['individualInfo'] != null ? IndividualInfo.fromJson(json['individualInfo']) : null;
    clubInfo = json['clubInfo'] != null ? ClubInfo.fromJson(json['clubInfo']) : null;
    associationInfo =
        json['associationInfo'] != null ? AssociationInfo.fromJson(json['associationInfo']) : null;
    merchantInfo =
        json['merchantInfo'] != null ? MerchantInfo.fromJson(json['merchantInfo']) : null;
    promoterInfo =
        json['promotorInfo'] != null ? PromoterInfo.fromJson(json['promotorInfo']) : null;
    partnerInfo = json['partnerInfo'] != null ? BusinessPartnerInfo.fromJson(json['partnerInfo']) : null;
  }

  TransactionInfo? transactionInfo;
  IndividualInfo? individualInfo;
  ClubInfo? clubInfo;
  AssociationInfo? associationInfo;
  MerchantInfo? merchantInfo;
  PromoterInfo? promoterInfo;
  BusinessPartnerInfo? partnerInfo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (transactionInfo != null) {
      map['transactionInfo'] = transactionInfo?.toJson();
    }
    if (individualInfo != null) {
      map['individualInfo'] = individualInfo?.toJson();
    }
    if (clubInfo != null) {
      map['clubInfo'] = clubInfo?.toJson();
    }
    if (associationInfo != null) {
      map['associationInfo'] = associationInfo?.toJson();
    }
    if (merchantInfo != null) {
      map['merchantInfo'] = merchantInfo?.toJson();
    }
    if (promoterInfo != null) {
      map['promotorInfo'] = promoterInfo?.toJson();
    }
    if (partnerInfo != null) {
      map['partnerInfo'] = partnerInfo?.toJson();
    }
    return map;
  }
}

/// id : 0
/// phone : "string"
/// name : "string"
/// promotorName : "string"
/// nikNumber : "string"
/// ktaNumber : "string"
/// documents : ["string"]
/// userPackageId : 0
/// userUuid : "string"

class PromoterInfo {
  PromoterInfo({
    this.id,
    this.description,
    this.phone,
    this.name,
    this.promotorName,
    this.provinceName,
    this.pic,
    this.picName,
    this.ktaNumber,
    this.documents,
    this.documentLinks,
    this.userPackageId,
    this.communityId,
    this.userUuid,
    this.expiredDate,
    this.clubInfo,
    this.associationInfo
  });

  PromoterInfo.fromJson(dynamic json) {
    id = json['id'];
    description = json['description'];
    phone = json['phone'];
    name = json['name'];
    promotorName = json['promotorName'];
    provinceName = json['provinceName'];
    pic = json['pic'];
    picName = json['picName'];
    ktaNumber = json['ktaNumber'];
    documents = json['documents'] != null ? json['documents'].cast<String>() : [];
    documentLinks = json['documentLinks'] != null ? json['documentLinks'].cast<String>() : [];
    userPackageId = json['userPackageId'];
    communityId = json['communityId'];
    userUuid = json['userUuid'];
    expiredDate = json['expiredDate'];
    clubInfo = json['clubInfo'] != null ? ClubInfo.fromJson(json['clubInfo']) : null;
    associationInfo =
    json['associationInfo'] != null ? AssociationInfo.fromJson(json['associationInfo']) : null;
  }

  int? id;
  String? description;
  String? phone;
  String? name;
  String? promotorName;
  String? provinceName;
  String? pic;
  String? picName;
  String? ktaNumber;
  List<String>? documents;
  List<String>? documentLinks;
  int? userPackageId;
  int? communityId;
  String? userUuid;
  int? expiredDate;
  ClubInfo? clubInfo;
  AssociationInfo? associationInfo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['description'] = description;
    map['phone'] = phone;
    map['name'] = name;
    map['promotorName'] = promotorName;
    map['provinceName'] = provinceName;
    map['pic'] = pic;
    map['picName'] = picName;
    map['ktaNumber'] = ktaNumber;
    map['documents'] = documents;
    map['documentLinks'] = documentLinks;
    map['userPackageId'] = userPackageId;
    map['communityId'] = communityId;
    map['userUuid'] = userUuid;
    map['expiredDate'] = expiredDate;
    if (clubInfo != null) {
      map['clubInfo'] = clubInfo?.toJson();
    }
    if (associationInfo != null) {
      map['associationInfo'] = associationInfo?.toJson();
    }
    return map;
  }
}

/// id : 0
/// clubId : 0
/// communityId : 0
/// role : "string"
/// phone : "string"
/// nikNumber : "string"
/// ktaNumber : "string"
/// userUuid : "string"
/// name : "string"
/// status : "VERIFYING"
/// title : "PRESIDENT"

class Members {
  Members({
    this.id,
    this.clubId,
    this.communityId,
    this.role,
    this.phone,
    this.nikNumber,
    this.ktaNumber,
    this.userUuid,
    this.name,
    this.status,
    this.title,
  });

  Members.fromJson(dynamic json) {
    id = json['id'];
    clubId = json['clubId'];
    communityId = json['communityId'];
    role = json['role'];
    phone = json['phone'];
    nikNumber = json['nikNumber'];
    ktaNumber = json['ktaNumber'];
    userUuid = json['userUuid'];
    name = json['name'];
    status = json['status'];
    title = json['title'];
  }

  int? id;
  int? clubId;
  int? communityId;
  String? role;
  String? phone;
  String? nikNumber;
  String? ktaNumber;
  String? userUuid;
  String? name;
  String? status;
  String? title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['clubId'] = clubId;
    map['communityId'] = communityId;
    map['role'] = role;
    map['phone'] = phone;
    map['nikNumber'] = nikNumber;
    map['ktaNumber'] = ktaNumber;
    map['userUuid'] = userUuid;
    map['name'] = name;
    map['status'] = status;
    map['title'] = title;
    return map;
  }
}

/// id : 0
/// userUuid : "string"
/// name : "string"
/// phone : "string"
/// address : "string"
/// email : "string"
/// clubId : 0
/// clubName : "string"
/// clubPic : "string"
/// clubStatus : "string"
/// picPhoneNumber : "string"
/// bloodType : "string"
/// birthPlace : "string"
/// hobby : "string"
/// invitorPhoneNumber : "string"
/// gender : "string"
/// profilePicture : "string"
/// profilePictureLink : "string"
/// coverPicture : "string"
/// coverPictureLink : "string"
/// nikNumber : "string"
/// nikPicture : "string"
/// nikPictureLink : "string"
/// clubProvinceId : 0
/// clubProvinceName : "string"
/// provinceId : 0
/// provinceName : "string"
/// cityId : 0
/// cityName : "string"
/// districtId : 0
/// districtName : "string"
/// wardId : 0
/// wardName : "string"
/// birthday : "string"
/// ktaNumber : "string"
/// postalCode : "string"
/// expiredDate : 0
/// registerTime : 0
/// nationality : "string"
/// packageName : "string"
/// status : "string"
/// rtRwNumber : "string"
/// imiPaid : "string"
/// clubPicName : "string"
/// clubPicPhone : "string"
/// referralCode : "string"

class IndividualInfo {
  IndividualInfo({
    this.id,
    this.userUuid,
    this.name,
    this.phone,
    this.address,
    this.email,
    this.clubId,
    this.clubName,
    this.clubPic,
    this.clubStatus,
    this.picPhoneNumber,
    this.bloodType,
    this.birthPlace,
    this.hobby,
    this.invitorPhoneNumber,
    this.gender,
    this.profilePicture,
    this.profilePictureLink,
    this.coverPicture,
    this.coverPictureLink,
    this.nikNumber,
    this.nikPicture,
    this.nikPictureLink,
    this.clubProvinceId,
    this.clubProvinceName,
    this.provinceId,
    this.provinceName,
    this.cityId,
    this.cityName,
    this.districtId,
    this.districtName,
    this.wardId,
    this.wardName,
    this.birthday,
    this.ktaNumber,
    this.postalCode,
    this.expiredDate,
    this.registerTime,
    this.nationality,
    this.packageName,
    this.status,
    this.rtRwNumber,
    this.imiPaid,
    this.clubPicName,
    this.clubPicPhone,
    this.referralCode,
  });

  IndividualInfo.fromJson(dynamic json) {
    id = json['id'];
    userUuid = json['userUuid'];
    name = json['name'];
    phone = json['phone'];
    address = json['address'];
    email = json['email'];
    clubId = json['clubId'];
    clubName = json['clubName'];
    clubPic = json['clubPic'];
    clubStatus = json['clubStatus'];
    picPhoneNumber = json['picPhoneNumber'];
    bloodType = json['bloodType'];
    birthPlace = json['birthPlace'];
    hobby = json['hobby'];
    invitorPhoneNumber = json['invitorPhoneNumber'];
    gender = json['gender'];
    profilePicture = json['profilePicture'];
    profilePictureLink = json['profilePictureLink'];
    coverPicture = json['coverPicture'];
    coverPictureLink = json['coverPictureLink'];
    nikNumber = json['nikNumber'];
    nikPicture = json['nikPicture'];
    nikPictureLink = json['nikPictureLink'];
    clubProvinceId = json['clubProvinceId'];
    clubProvinceName = json['clubProvinceName'];
    provinceId = json['provinceId'];
    provinceName = json['provinceName'];
    cityId = json['cityId'];
    cityName = json['cityName'];
    districtId = json['districtId'];
    districtName = json['districtName'];
    wardId = json['wardId'];
    wardName = json['wardName'];
    birthday = json['birthday'];
    ktaNumber = json['ktaNumber'];
    postalCode = json['postalCode'];
    expiredDate = json['expiredDate'];
    registerTime = json['registerTime'];
    nationality = json['nationality'];
    packageName = json['packageName'];
    status = json['status'];
    rtRwNumber = json['rtRwNumber'];
    imiPaid = json['imiPaid'];
    clubPicName = json['clubPicName'];
    clubPicPhone = json['clubPicPhone'];
    referralCode = json['referralCode'];
  }

  int? id;
  String? userUuid;
  String? name;
  String? phone;
  String? address;
  String? email;
  int? clubId;
  String? clubName;
  String? clubPic;
  String? clubStatus;
  String? picPhoneNumber;
  String? bloodType;
  String? birthPlace;
  String? hobby;
  String? invitorPhoneNumber;
  String? gender;
  String? profilePicture;
  String? profilePictureLink;
  String? coverPicture;
  String? coverPictureLink;
  String? nikNumber;
  String? nikPicture;
  String? nikPictureLink;
  int? clubProvinceId;
  String? clubProvinceName;
  int? provinceId;
  String? provinceName;
  int? cityId;
  String? cityName;
  int? districtId;
  String? districtName;
  int? wardId;
  String? wardName;
  String? birthday;
  String? ktaNumber;
  String? postalCode;
  int? expiredDate;
  int? registerTime;
  String? nationality;
  String? packageName;
  String? status;
  String? rtRwNumber;
  String? imiPaid;
  String? clubPicName;
  String? clubPicPhone;
  String? referralCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['userUuid'] = userUuid;
    map['name'] = name;
    map['phone'] = phone;
    map['address'] = address;
    map['email'] = email;
    map['clubId'] = clubId;
    map['clubName'] = clubName;
    map['clubPic'] = clubPic;
    map['clubStatus'] = clubStatus;
    map['picPhoneNumber'] = picPhoneNumber;
    map['bloodType'] = bloodType;
    map['birthPlace'] = birthPlace;
    map['hobby'] = hobby;
    map['invitorPhoneNumber'] = invitorPhoneNumber;
    map['gender'] = gender;
    map['profilePicture'] = profilePicture;
    map['profilePictureLink'] = profilePictureLink;
    map['coverPicture'] = coverPicture;
    map['coverPictureLink'] = coverPictureLink;
    map['nikNumber'] = nikNumber;
    map['nikPicture'] = nikPicture;
    map['nikPictureLink'] = nikPictureLink;
    map['clubProvinceId'] = clubProvinceId;
    map['clubProvinceName'] = clubProvinceName;
    map['provinceId'] = provinceId;
    map['provinceName'] = provinceName;
    map['cityId'] = cityId;
    map['cityName'] = cityName;
    map['districtId'] = districtId;
    map['districtName'] = districtName;
    map['wardId'] = wardId;
    map['wardName'] = wardName;
    map['birthday'] = birthday;
    map['ktaNumber'] = ktaNumber;
    map['postalCode'] = postalCode;
    map['expiredDate'] = expiredDate;
    map['registerTime'] = registerTime;
    map['nationality'] = nationality;
    map['packageName'] = packageName;
    map['status'] = status;
    map['rtRwNumber'] = rtRwNumber;
    map['imiPaid'] = imiPaid;
    map['clubPicName'] = clubPicName;
    map['clubPicPhone'] = clubPicPhone;
    map['referralCode'] = referralCode;
    return map;
  }
}

/// userPackageId : 0
/// packageId : 0
/// packageName : "string"
/// packageCode : "string"
/// transactionId : "string"
/// initFee : 0
/// annualFee : 0
/// processingFee : 0

class TransactionInfo {
  TransactionInfo({
    this.userPackageId,
    this.packageId,
    this.packageName,
    this.packageCode,
    this.transactionId,
    this.initFee,
    this.annualFee,
    this.processingFee,
  });

  TransactionInfo.fromJson(dynamic json) {
    userPackageId = json['userPackageId'];
    packageId = json['packageId'];
    packageName = json['packageName'];
    packageCode = json['packageCode'];
    transactionId = json['transactionId'];
    initFee = json['initFee'];
    annualFee = json['annualFee'];
    processingFee = json['processingFee'];
  }

  int? userPackageId;
  int? packageId;
  String? packageName;
  String? packageCode;
  String? transactionId;
  int? initFee;
  int? annualFee;
  int? processingFee;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['userPackageId'] = userPackageId;
    map['packageId'] = packageId;
    map['packageName'] = packageName;
    map['packageCode'] = packageCode;
    map['transactionId'] = transactionId;
    map['initFee'] = initFee;
    map['annualFee'] = annualFee;
    map['processingFee'] = processingFee;
    return map;
  }
}
