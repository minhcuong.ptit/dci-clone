class FetchCouponsData {
  int? id;
  String? code;
  String? description;
  String? discountType;
  int? discount;
  String? type;
  String? expiredDate;
  String? maxQuantity;
  int? minPrice;
  int? usedNo;
  bool? compile;
  bool? isValid;
  String? limitType;
  String? privacy;

  FetchCouponsData({
    this.id,
    this.code,
    this.description,
    this.discountType,
    this.discount,
    this.type,
    this.expiredDate,
    this.maxQuantity,
    this.minPrice,
    this.usedNo,
    this.compile,
    this.limitType,
    this.privacy,
    this.isValid,
  });

  FetchCouponsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    code = json['code']?.toString();
    description = json['description']?.toString();
    discountType = json['discountType']?.toString();
    discount = json['discount']?.toInt();
    type = json['type']?.toString();
    expiredDate = json['expiredDate']?.toString();
    maxQuantity = json['maxQuantity']?.toString();
    minPrice = json['minPrice']?.toInt();
    usedNo = json['usedNo']?.toInt();
    compile = json['compile'];
    isValid = json['isValid'];
    limitType = json['limitType']?.toString();
    privacy = json['privacy']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['description'] = description;
    data['discountType'] = discountType;
    data['discount'] = discount;
    data['type'] = type;
    data['expiredDate'] = expiredDate;
    data['maxQuantity'] = maxQuantity;
    data['minPrice'] = minPrice;
    data['usedNo'] = usedNo;
    data['compile'] = compile;
    data['isValid'] = isValid;
    data['limitType'] = limitType;
    data['privacy'] = privacy;
    return data;
  }
}

class FetchCoupons {
  String? nextToken;
  List<FetchCouponsData>? data;

  FetchCoupons({
    this.nextToken,
    this.data,
  });

  FetchCoupons.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchCouponsData>[];
      v.forEach((v) {
        arr0.add(FetchCouponsData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class CouponsData {
  FetchCoupons? fetchCoupons;

  CouponsData({
    this.fetchCoupons,
  });

  CouponsData.fromJson(Map<String, dynamic> json) {
    fetchCoupons = (json['fetchCoupons'] != null)
        ? FetchCoupons.fromJson(json['fetchCoupons'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchCoupons != null) {
      data['fetchCoupons'] = fetchCoupons!.toJson();
    }
    return data;
  }
}

class CouponsResponse {
  CouponsData? data;

  CouponsResponse({
    this.data,
  });

  CouponsResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? CouponsData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
