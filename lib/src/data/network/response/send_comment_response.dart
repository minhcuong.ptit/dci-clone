/// id : 16

class SendCommentResponse {
  SendCommentResponse({
      int? id,}){
    _id = id;
}

  SendCommentResponse.fromJson(dynamic json) {
    _id = json['id'];
  }
  int? _id;

  int? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    return map;
  }

}