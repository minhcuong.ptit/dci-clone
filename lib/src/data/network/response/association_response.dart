import 'package:imi/src/data/network/response/community_data.dart';

class AssociationResponse {
  Associations? associations;

  AssociationResponse({
    Associations? mAssociations,
  }) {
    associations = mAssociations;
  }

  AssociationResponse.fromJson(dynamic json) {
    associations = (json['yourCommunities'] != null)
        ? Associations.fromJson(json['yourCommunities'])
        : null;
  }
}

class Associations {
  String? nextToken;
  List<CommunityData>? data;

  Associations({
    this.nextToken,
    this.data,
  });

  Associations.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <CommunityData>[];
      v.forEach((v) {
        arr0.add(CommunityData.fromJson(v));
      });
      this.data = arr0;
    }
  }
}
