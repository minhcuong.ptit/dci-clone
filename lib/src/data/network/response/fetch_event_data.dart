class FetchEventsData {

  int? id;
  int? productId;
  String? name;
  String? avatarUrl;
  String? status;
  int? currentFee;
  int? preferentialFee;
  String? description;
  String? code;
  String? teacher;
  int? startDate;
  int? endDate;
  int? priority;
  bool? isHot;
  String? address;
  String? studyTime;
  List<int>? topicIds;

  FetchEventsData({
    this.id,
    this.productId,
    this.name,
    this.avatarUrl,
    this.status,
    this.currentFee,
    this.preferentialFee,
    this.description,
    this.code,
    this.teacher,
    this.startDate,
    this.endDate,
    this.priority,
    this.isHot,
    this.address,
    this.studyTime,
    this.topicIds,
  });
  FetchEventsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    productId = json['productId']?.toInt();
    name = json['name']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    status = json['status']?.toString();
    currentFee = json['currentFee']?.toInt();
    preferentialFee = json['preferentialFee']?.toInt();
    description = json['description']?.toString();
    code = json['code']?.toString();
    teacher = json['teacher']?.toString();
    startDate = json['startDate']?.toInt();
    endDate = json['endDate']?.toInt();
    priority = json['priority']?.toInt();
    isHot = json['isHot'];
    address = json['address']?.toString();
    studyTime = json['studyTime']?.toString();
    if (json['topicIds'] != null) {
      final v = json['topicIds'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      topicIds = arr0;
    }

  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['name'] = name;
    data['avatarUrl'] = avatarUrl;
    data['status'] = status;
    data['currentFee'] = currentFee;
    data['preferentialFee'] = preferentialFee;
    data['description'] = description;
    data['code'] = code;
    data['teacher'] = teacher;
    data['startDate'] = startDate;
    data['endDate'] = endDate;
    data['priority'] = priority;
    data['isHot'] = isHot;
    data['address'] = address;
    data['studyTime'] = studyTime;
    if (topicIds != null) {
      final v = topicIds;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['topicIds'] = arr0;
    }
    return data;
  }
}