import 'user_data.dart';

class UserResponse {
  UserResponse({
    UserData? me,
  }) {
    _me = me;
  }

  UserResponse.fromJson(dynamic json) {
    _me = json['me'] != null ? UserData.fromJson(json['me']) : null;
  }

  UserData? _me;

  UserData? get me => _me;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_me != null) {
      map['me'] = _me?.toJson();
    }
    return map;
  }
}
