class DataMeditation {
  String? title;
  int? createdDate;

  DataMeditation({
    this.title,
    this.createdDate,
  });

  DataMeditation.fromJson(Map<String, dynamic> json) {
    title = json['title']?.toString();
    createdDate = json['createdDate']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['createdDate'] = createdDate;
    return data;
  }
}

class FetchMeditationHistoriesData {
  int? id;
  int? duration;
  DataMeditation? meditation;
  int? createdDate;

  FetchMeditationHistoriesData({
    this.id,
    this.duration,
    this.meditation,
    this.createdDate,
  });

  FetchMeditationHistoriesData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    duration = json['duration']?.toInt();
    meditation = (json['meditation'] != null)
        ? DataMeditation.fromJson(json['meditation'])
        : null;
    createdDate = json['createdDate']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['duration'] = duration;
    if (meditation != null) {
      data['meditation'] = meditation!.toJson();
    }
    data['createdDate'] = createdDate;
    return data;
  }
}

class FetchMeditationHistories {
  String? nextToken;
  int? total;
  List<FetchMeditationHistoriesData>? data;

  FetchMeditationHistories({
    this.nextToken,
    this.total,
    this.data,
  });

  FetchMeditationHistories.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    total = json['total']?.toInt();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchMeditationHistoriesData>[];
      v.forEach((v) {
        arr0.add(FetchMeditationHistoriesData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['total'] = total;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class HistoriesMeditationData {
  FetchMeditationHistories? fetchMeditationHistories;

  HistoriesMeditationData({
    this.fetchMeditationHistories,
  });

  HistoriesMeditationData.fromJson(Map<String, dynamic> json) {
    fetchMeditationHistories = (json['fetchMeditationHistories'] != null)
        ? FetchMeditationHistories.fromJson(json['fetchMeditationHistories'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchMeditationHistories != null) {
      data['fetchMeditationHistories'] = fetchMeditationHistories!.toJson();
    }
    return data;
  }
}

class HistoriesMeditationResponse {
  HistoriesMeditationData? data;

  HistoriesMeditationResponse({
    this.data,
  });

  HistoriesMeditationResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? HistoriesMeditationData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
