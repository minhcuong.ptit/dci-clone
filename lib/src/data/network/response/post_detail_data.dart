import 'dart:convert';

import 'package:imi/src/data/network/request/post_request.dart';

import 'post_data.dart';

class PostDetailData {
  PostDetailData({
    this.data,
  });

  PostDetailData.fromJson(dynamic json) {
    data =
        json['postById'] != null ? PostData.fromJson(json['postById']) : null;
  }

  PostData? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['postById'] = data?.toJson();
    }
    return map;
  }
}
