import 'dart:convert';

MyIndividual myIndividualFromJson(String str) =>
    MyIndividual.fromJson(json.decode(str));
String myIndividualToJson(MyIndividual data) => json.encode(data.toJson());

class MyIndividual {
  MyIndividual({
    this.id,
    this.name,
    this.phone,
    this.address,
    this.email,
    this.clubId,
    this.clubName,
    this.clubPic,
    this.clubStatus,
    this.picPhoneNumber,
    this.bloodType,
    this.birthPlace,
    this.hobby,
    this.invitorPhoneNumber,
    this.gender,
    this.profilePicture,
    this.coverPicture,
    this.nikNumber,
    this.nikPicture,
    this.sim,
    this.simPicture,
    this.documents,
    this.provinceId,
    this.provinceName,
    this.dob,
    this.ktaNumber,
    this.postalCode,
    this.expiredDate,
    this.registerTime,
    this.nationality,
    this.packagaName,
    this.status,
    this.rtRwNumber,
  });

  int? id;
  String? name;
  String? phone;
  String? address;
  String? email;
  int? clubId;
  String? clubName;
  String? clubPic;
  String? clubStatus;
  String? picPhoneNumber;
  String? bloodType;
  String? birthPlace;
  String? hobby;
  String? invitorPhoneNumber;
  String? gender;
  String? profilePicture;
  String? coverPicture;
  String? nikNumber;
  String? nikPicture;
  String? sim;
  String? simPicture;
  List<String>? documents;
  int? provinceId;
  String? provinceName;
  String? dob;
  String? ktaNumber;
  String? postalCode;
  int? expiredDate;
  int? registerTime;
  String? nationality;
  String? packagaName;
  String? status;
  String? rtRwNumber;

  factory MyIndividual.fromJson(Map<String, dynamic> json) {
    return MyIndividual(
      id: json["id"],
      name: json["name"],
      phone: json["phone"],
      address: json["address"],
      email: json["email"],
      clubId: json["clubId"],
      clubName: json["clubName"],
      clubPic: json["clubPic"],
      clubStatus: json["clubStatus"],
      picPhoneNumber: json["picPhoneNumber"],
      bloodType: json["bloodType"],
      birthPlace: json["birthPlace"],
      hobby: json["hobby"],
      invitorPhoneNumber: json["invitorPhoneNumber"],
      gender: json["gender"],
      profilePicture: json["profilePicture"],
      coverPicture: json["coverPicture"],
      nikNumber: json["nikNumber"],
      nikPicture: json["nikPicture"],
      sim: json["sim"],
      simPicture: json["simPicture"],
      provinceId: json["provinceId"],
      provinceName: json["provinceName"],
      dob: json["dob"],
      ktaNumber: json["ktaNumber"],
      postalCode: json["postalCode"],
      expiredDate: json["expiredDate"],
      registerTime: json["registerTime"],
      nationality: json["nationality"],
      packagaName: json["packagaName"],
      status: json["status"],
      rtRwNumber: json["rtRwNumber"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone": phone,
        "address": address,
        "email": email,
        "clubId": clubId,
        "clubName": clubName,
        "clubPic": clubPic,
        "clubStatus": clubStatus,
        "picPhoneNumber": picPhoneNumber,
        "bloodType": bloodType,
        "birthPlace": birthPlace,
        "hobby": hobby,
        "invitorPhoneNumber": invitorPhoneNumber,
        "gender": gender,
        "profilePicture": profilePicture,
        "coverPicture": coverPicture,
        "nikNumber": nikNumber,
        "nikPicture": nikPicture,
        "sim": sim,
        "simPicture": simPicture,
        "provinceId": provinceId,
        "provinceName": provinceName,
        "dob": dob,
        "ktaNumber": ktaNumber,
        "postalCode": postalCode,
        "expiredDate": expiredDate,
        "registerTime": registerTime,
        "nationality": nationality,
        "packagaName": packagaName,
        "status": status,
        "rtRwNumber": rtRwNumber,
      };
}
