class ImageSeedData {
  String? url;
  String? key;

  ImageSeedData({
    this.url,
    this.key,
  });

  ImageSeedData.fromJson(Map<String, dynamic> json) {
    url = json['url']?.toString();
    key = json['key']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['url'] = url;
    data['key'] = key;
    return data;
  }
}
