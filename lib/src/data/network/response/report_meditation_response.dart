class FetchGeneralMeditationReport {
  int? dayStreak;
  int? totalDays;
  int? totalHours;

  FetchGeneralMeditationReport({
    this.dayStreak,
    this.totalDays,
    this.totalHours,
  });

  FetchGeneralMeditationReport.fromJson(Map<String, dynamic> json) {
    dayStreak = json['dayStreak']?.toInt();
    totalDays = json['totalDays']?.toInt();
    totalHours = json['totalHours']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['dayStreak'] = dayStreak;
    data['totalDays'] = totalDays;
    data['totalHours'] = totalHours;
    return data;
  }
}

class ReportMeditationData {
  FetchGeneralMeditationReport? fetchGeneralMeditationReport;

  ReportMeditationData({
    this.fetchGeneralMeditationReport,
  });

  ReportMeditationData.fromJson(Map<String, dynamic> json) {
    fetchGeneralMeditationReport =
        (json['fetchGeneralMeditationReport'] != null)
            ? FetchGeneralMeditationReport.fromJson(
                json['fetchGeneralMeditationReport'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchGeneralMeditationReport != null) {
      data['fetchGeneralMeditationReport'] =
          fetchGeneralMeditationReport!.toJson();
    }
    return data;
  }
}

class ReportMeditationResponse {
  ReportMeditationData? data;

  ReportMeditationResponse({
    this.data,
  });

  ReportMeditationResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? ReportMeditationData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
