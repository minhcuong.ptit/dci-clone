import 'package:imi/src/data/network/response/community_data.dart';

class MerchantResponse {
  Merchants? merchants;

  MerchantResponse({
    Merchants? mMerchants,
  }) {
    merchants = mMerchants;
  }

  MerchantResponse.fromJson(dynamic json) {
    merchants = (json['followCommunities'] != null)
        ? Merchants.fromJson(json['followCommunities'])
        : null;
  }
}

class Merchants {
  String? nextToken;
  List<CommunityData>? data;

  Merchants({
    this.nextToken,
    this.data,
  });

  Merchants.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <CommunityData>[];
      v.forEach((v) {
        arr0.add(CommunityData.fromJson(v));
      });
      this.data = arr0;
    }
  }
}
