import 'package:imi/src/data/network/response/taa_association_membership.dart';

class TktMembership {
  int? id;
  String? name;
  TaaAssociationMembership? primaryAssociation;

  TktMembership({
    this.id,
    this.name,
    this.primaryAssociation,
  });

  TktMembership.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    if (json['primaryAssociation'] != null) {
      primaryAssociation =
          TaaAssociationMembership.fromJson(json['primaryAssociation']);
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['id'] = id
    ..['name'] = name
    ..['primaryAssociation'] = primaryAssociation?.toJson();
}
