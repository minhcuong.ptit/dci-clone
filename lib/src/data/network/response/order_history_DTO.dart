import 'order_information_response.dart';

class OrderHistoryDTO {
  int? id;
  int? finalPrice;
  String? paymentType;
  String? code;
  String? status;
  int? createdDate;
  String? cancelReason;
  int? canceledDate;
  int? modifiedDate;
  int? paymentDate;
  bool? usePoint;
  bool? isSeen;
  int? usedPointAmount;
  int? pointExchangeRate;
  List<String>? productTypes;
  int? totalDiscount;
  int? totalSingleCouponDiscount;
  int? totalComboCouponDiscount;
  int? totalPrice;
  List<Coupons>? coupons;
  List<OrderItems>? orderItems;

  OrderHistoryDTO(
      {this.id,
        this.finalPrice,
        this.paymentType,
        this.code,
        this.status,
        this.createdDate,
        this.cancelReason,
        this.canceledDate,
        this.modifiedDate,
        this.paymentDate,
        this.productTypes,
        this.totalDiscount,
        this.usePoint,
        this.isSeen,
        this.pointExchangeRate,
        this.usedPointAmount,
        this.totalPrice,
        this.totalSingleCouponDiscount,
        this.totalComboCouponDiscount,
        this.coupons,
        this.orderItems});

  OrderHistoryDTO.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    finalPrice = json['finalPrice'];
    paymentType = json['paymentType'];
    code = json['code'];
    status = json['status'];
    createdDate = json['createdDate'];
    cancelReason = json['cancelReason'];
    canceledDate = json['canceledDate'];
    modifiedDate = json['modifiedDate'];
    paymentDate = json['paymentDate'];
    usePoint = json['usePoint'];
    isSeen=json['isSeen'];
    pointExchangeRate = json['pointExchangeRate'];
    usedPointAmount = json['usedPointAmount'];
    productTypes = json['productTypes'].cast<String>();
    totalDiscount = json['totalDiscount'];
    totalSingleCouponDiscount = json['totalSingleCouponDiscount'];
    totalComboCouponDiscount = json['totalComboCouponDiscount'];
    totalPrice = json['totalPrice'];
    if (json['coupons'] != null) {
      coupons = <Coupons>[];
      json['coupons'].forEach((v) {
        coupons!.add(new Coupons.fromJson(v));
      });
    }
    if (json['orderItems'] != null) {
      orderItems = <OrderItems>[];
      json['orderItems'].forEach((v) {
        orderItems!.add(new OrderItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['finalPrice'] = this.finalPrice;
    data['paymentType'] = this.paymentType;
    data['code'] = this.code;
    data['status'] = this.status;
    data['createdDate'] = this.createdDate;
    data['cancelReason'] = this.cancelReason;
    data['canceledDate'] = this.canceledDate;
    data['modifiedDate'] = this.modifiedDate;
    data['paymentDate'] = this.paymentDate;
    data['productTypes'] = this.productTypes;
    data['totalDiscount'] = this.totalDiscount;
    data['totalSingleCouponDiscount'] = this.totalSingleCouponDiscount;
    data['totalComboCouponDiscount'] = this.totalComboCouponDiscount;
    data['usePoint'] = this.usePoint;
    data['isSeen']=this.isSeen;
    data['usedPointAmount'] = this.usedPointAmount;
    data['pointExchangeRate'] = this.pointExchangeRate;
    data['totalPrice'] = this.totalPrice;
    if (this.coupons != null) {
      data['coupons'] = this.coupons!.map((v) => v.toJson()).toList();
    }
    if (this.orderItems != null) {
      data['orderItems'] = this.orderItems!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Coupons {
  String? code;
  int? id;

  Coupons({this.code, this.id});

  Coupons.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}