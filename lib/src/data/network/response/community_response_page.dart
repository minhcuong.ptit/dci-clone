import 'community_data.dart';

class CommunityPage {
  CommunityPage({
    List<CommunityDataV2>? data,
  }) {
    _data = data;
  }

  CommunityPage.fromJson(dynamic json) {
    nextToken = json['nextToken'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(CommunityDataV2.fromJson(v));
      });
    }
  }
  List<CommunityDataV2>? _data;
  String? nextToken;

  List<CommunityDataV2>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.map((v) => v).toList();
    }
    map['nextToken'] = nextToken;
    return map;
  }
}
