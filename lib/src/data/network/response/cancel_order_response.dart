import 'dart:convert';

class CancelOrderReason {
  final int id;
  final String reason;

  CancelOrderReason({required this.id, required this.reason});

  factory CancelOrderReason.fromJson(Map<String, dynamic> map) {
    return CancelOrderReason(
      id: map['id']?.toInt() ?? 0,
      reason: map['reason'] ?? '',
    );
  }
}
