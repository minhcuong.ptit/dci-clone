/// data : {"informationId":20,"userPackageId":383}

class FormResponse {
  FormResponse({
      Data? data,}){
    _data = data;
}

  FormResponse.fromJson(dynamic json) {
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  Data? _data;

  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// informationId : 20
/// userPackageId : 383

class Data {
  Data({
      int? informationId, 
      int? userPackageId,}){
    _informationId = informationId;
    _userPackageId = userPackageId;
}

  Data.fromJson(dynamic json) {
    _informationId = json['informationId'];
    _userPackageId = json['userPackageId'];
  }
  int? _informationId;
  int? _userPackageId;

  int? get informationId => _informationId;
  int? get userPackageId => _userPackageId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['informationId'] = _informationId;
    map['userPackageId'] = _userPackageId;
    return map;
  }

}