import 'package:collection/collection.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/enum.dart';

class MemberDTO {
  MemberDTO({
    this.id,
    this.communityId,
    this.clubId,
    this.userUuid,
    required this.roles,
    this.phone,
    this.name,
    this.nikNumber,
    this.ktaNumber,
    this.status,
    this.title,
    this.createdDate,
    this.avatarUrl,
    this.provinceName,
    this.packageStatus,
  });

  MemberDTO.fromJson(dynamic json) {
    id = json['id'];
    communityId = json['communityId'];
    clubId = json['clubId'];
    userUuid = json['userUuid'];
    List<MemberRole?>? rawRoles = json['roles']
        ?.map<MemberRole?>((e) =>
            MemberRole.values.firstWhereOrNull((element) => element.name == e))
        ?.toList();
    roles = rawRoles?.whereNotNull().toList() ?? [];
    phone = json['phone'];
    name = json['name'];
    nikNumber = json['nikNumber'];
    ktaNumber = json['ktaNumber'];
    status = MemberStatusType.values
        .firstWhereOrNull((element) => element.name == json['status']);
    title = json['title'];
    createdDate = json['createdDate']?.toInt();
    avatarUrl = json['avatarUrl'];
    provinceName = json['provinceName'];
    packageStatus = UserPackageStatus.values
        .firstWhereOrNull((element) => element.name == json['status']);
  }

  int? id;
  int? communityId;
  int? clubId;
  String? userUuid;
  List<MemberRole> roles = [];
  String? phone;
  String? name;
  String? nikNumber;
  String? ktaNumber;
  MemberStatusType? status;
  String? title;
  int? createdDate;
  String? avatarUrl;
  String? provinceName;
  UserPackageStatus? packageStatus;

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..["id"] = id
    ..["communityId"] = communityId
    ..["clubId"] = clubId
    ..["userUuid"] = userUuid
    ..['roles'] = roles.map((e) => e.name).toList()
    ..["phone"] = phone
    ..["name"] = name
    ..["nikNumber"] = nikNumber
    ..["ktaNumber"] = ktaNumber
    ..["status"] = status?.name
    ..["title"] = title
    ..["createdDate"] = createdDate
    ..["avatarUrl"] = avatarUrl
    ..["provinceName"] = provinceName
    ..['packageStatus'] = packageStatus?.name;

  String get roleDisplay => roles
      .whereNot((element) => MemberRole.CLUB_MODERATOR == element)
      .map((e) => e.asDisplayString())
      .join(" - ");

  bool get hasModeratorPermission => roles.hasModeratorPermission;
  bool get canToggleModerator => !roles.contains(MemberRole.CLUB_ADMIN);
}
