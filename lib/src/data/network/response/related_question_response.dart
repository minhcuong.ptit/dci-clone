import 'package:imi/src/data/network/response/topic_response.dart';

class FetchQuestionsData {
  int? id;
  String? question;
  List<Topics>? topics;

  FetchQuestionsData({
    this.id,
    this.question,
    this.topics,
  });

  FetchQuestionsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    question = json['question']?.toString();
    if (json['topics'] != null) {
      final v = json['topics'];
      final arr0 = <Topics>[];
      v.forEach((v) {
        arr0.add(Topics.fromJson(v));
      });
      topics = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['question'] = question;
    if (topics != null) {
      final v = topics;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['topics'] = arr0;
    }
    return data;
  }
}

class RelatedQuestions {
  List<FetchQuestionsData>? data;

  RelatedQuestions({
    this.data,
  });

  RelatedQuestions.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchQuestionsData>[];
      v.forEach((v) {
        arr0.add(FetchQuestionsData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class RelatedQuestionData {
  RelatedQuestions? fetchQuestions;

  RelatedQuestionData({
    this.fetchQuestions,
  });

  RelatedQuestionData.fromJson(Map<String, dynamic> json) {
    fetchQuestions = (json['fetchQuestions'] != null)
        ? RelatedQuestions.fromJson(json['fetchQuestions'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchQuestions != null) {
      data['fetchQuestions'] = fetchQuestions!.toJson();
    }
    return data;
  }
}

class RelatedQuestionResponse {
  RelatedQuestionData? data;

  RelatedQuestionResponse({
    this.data,
  });

  RelatedQuestionResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? RelatedQuestionData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
