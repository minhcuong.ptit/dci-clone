import 'package:imi/src/utils/enum.dart';

/// id : 1
/// code : "IMICLUBSTANDARD"
/// name : "IMI CLUB STANDARD"
/// amount : 1000000
/// initFee : 20000
/// annualFee : 130000
/// renewalFee : 130000
/// processingFee : 10000
/// packageDescriptions : [{"id":1,"code":"UNLIMITEDACCESSTODEDICATEDCLUB","name":"Unlimited access to dedicated club, province events"},{"id":2,"code":"LIMITEDACCESSTOOTHERCLUB","name":"Limited access to other clubs"},{"id":3,"code":"UNLIMITEDACCESSTOIMI","name":"Unlimited access to IMI networks"},{"id":4,"code":"DISCOUNTEDONSERVICES","name":"Discounted on services, products from IMI merchants"},{"id":5,"code":"INSURANCEINCLUDED","name":"Insurance included"}]

class ECardData {
  ECardData({
    this.id,
    this.iconUrl,
    this.active,
    this.code,
    this.name,
    this.color,
    this.amount,
    this.initFee,
    this.upgradeFee,
    this.annualFee,
    this.renewalFee,
    this.processingFee,
    List<PackageDescription>? packageDescriptions,
    this.numberEnrolled,
  });

  ECardData.fromJson(dynamic json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    color = json['color'];
    amount = json['amount'];
    initFee = json['initFee'];
    upgradeFee = json['upgradeFee'];
    annualFee = json['annualFee'];
    renewalFee = json['renewalFee'];
    processingFee = json['processingFee'];
    if (json['packageDescriptions'] != null) {
      packageDescriptions = [];
      json['packageDescriptions'].forEach((v) {
        packageDescriptions?.add(PackageDescription.fromJson(v));
      });
    }
    iconUrl = json['iconUrl'];
    active = json['active'];
    numberEnrolled = json['numberEnrolled'];
  }

  int? id;
  String? iconUrl;
  bool? active;
  String? code;
  String? name;
  String? color;
  int? amount;
  int? initFee;
  int? upgradeFee;
  int? annualFee;
  int? renewalFee;
  int? processingFee;
  List<PackageDescription>? packageDescriptions;
  int? numberEnrolled;

  bool get needInitFee {
    // only need initFee if Association, Business Partner or Promotor and have not enrolled ever
    if (code == TaaPackageType.IMI_CLUB_ASSOCIATION ||
        code == TaaPackageType.IMI_BUSINESS_PARTNER ||
        code == TaaPackageType.IMI_PROMOTOR) {
      return (numberEnrolled ?? 0) == 0;
    }
    return true;
  }

  int get totalFee {
    int totalFee = (annualFee ?? 0) + (processingFee ?? 0);
    if (needInitFee) {
      totalFee += (initFee ?? 0);
    }
    return totalFee;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['iconUrl'] = iconUrl;
    map['active'] = active;
    map['code'] = code;
    map['name'] = name;
    map['color'] = color;
    map['amount'] = amount;
    map['initFee'] = initFee;
    map['upgradeFee'] = upgradeFee;
    map['annualFee'] = annualFee;
    map['renewalFee'] = renewalFee;
    map['processingFee'] = processingFee;
    if (packageDescriptions != null) {
      map['packageDescriptions'] =
          packageDescriptions?.map((v) => v.toJson()).toList();
    }
    map['numberEnrolled'] = numberEnrolled;
    return map;
  }
}

/// id : 1
/// code : "UNLIMITEDACCESSTODEDICATEDCLUB"
/// name : "Unlimited access to dedicated club, province events"

class PackageDescription {
  PackageDescription({
    this.id,
    this.code,
    this.name,
  });

  PackageDescription.fromJson(dynamic json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
  }

  int? id;
  String? code;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['code'] = code;
    map['name'] = name;
    return map;
  }
}
