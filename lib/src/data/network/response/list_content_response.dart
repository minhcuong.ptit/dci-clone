import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/get_club_response.dart';

/// categories : [{"id":1,"name":"#Racing","color":"#FF6B00"},{"id":2,"name":"#Automotive","color":"#FF4264"},{"id":3,"name":"#Clubs","color":"#C300C7"},{"id":4,"name":"#Gocart","color":"#FF22C1"},{"id":5,"name":"#Tamiya","color":"#00D3D3"},{"id":6,"name":"#Area","color":"#AD4828"}]
/// communities : {"data":[{"id":1,"name":"Test","type":"CLUB","avatarUrl":null,"followerNo":null,"imageUrl":null,"memberNo":null,"ownerId":1},{"id":2,"name":"Redbull","type":"CLUB","avatarUrl":null,"followerNo":null,"imageUrl":null,"memberNo":null,"ownerId":1},{"id":3,"name":"string","type":"INDIVIDUAL","avatarUrl":"string","followerNo":0,"imageUrl":null,"memberNo":0,"ownerId":0}]}

class ListContentResponse {
  ListContentResponse({
    List<CategoryData>? categories,
    Communities? communities,
  }) {
    _categories = categories;
    _communities = communities;
  }

  ListContentResponse.fromJson(dynamic json) {
    if (json['categories'] != null) {
      _categories = [];
      json['categories'].forEach((v) {
        _categories?.add(CategoryData.fromJson(v));
      });
    }
    _communities = json['recommendCommunities'] != null
        ? Communities.fromJson(json['recommendCommunities'])
        : null;
  }
  List<CategoryData>? _categories;
  Communities? _communities;

  List<CategoryData>? get categories => _categories;
  Communities? get communities => _communities;
}
