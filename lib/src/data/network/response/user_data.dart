/// accumulatedPoints : 1
/// userUuid : "f933a6a2-196b-4d08-9f50-9dfc05f1b01c"
/// address : ""
/// birthdate : ""
/// bloodType : ""
/// driveLicenseNumber : ""
/// email : ""
/// expireDate : ""
/// familyName : ""
/// followers : ""
/// fullName : ""
/// gender : ""
/// givenName : ""
/// group : "free_user"
/// imiId : ""
/// locale : ""
/// memberType : "FREE"
/// middleName : ""
/// nickname : ""
/// picture : ""
/// cover : ""
/// profile : ""
/// province : ""
/// provinceIdentityCardNumber : ""
/// qrCode : ""
/// status : "PHONE_VERIFIED"
/// updatedAt : ""
/// username : "0363757740"
/// website : ""
/// zoneinfo : ""

class UserData {
  UserData({
    int? accumulatedPoints,
    String? userUuid,
    String? phoneNumber,
    String? address,
    String? birthdate,
    String? bloodType,
    String? email,
    String? fullName,
    String? gender,
    String? group,
    String? picture,
    String? cover,
    String? profile,
    String? status,
    String? username,
    String? birthPlace,
    String? ktpNumber,
    String? ktaNumber,
    int? userPoint,
    String? referralCode,
  }) {
    _accumulatedPoints = accumulatedPoints;
    _userUuid = userUuid;
    _phoneNumber = phoneNumber;
    _address = address;
    _birthdate = birthdate;
    _bloodType = bloodType;
    _email = email;
    _fullName = fullName;
    _gender = gender;
    _group = group;
    _picture = picture;
    _cover = cover;
    _province = province;
    _status = status;
    _username = username;
    _birthPlace = birthPlace;
    _ktpNumber = ktpNumber;
    _ktaNumber = ktaNumber;
    _userPoint = userPoint;
    _referralCode = referralCode;
  }

  UserData.fromJson(dynamic json) {
    _accumulatedPoints = json['accumulatedPoints'];
    _userUuid = json['userUuid'];
    _phoneNumber = json['phoneNumber'];
    _address = json['address'];
    _birthdate = json['birthdate'];
    _bloodType = json['bloodType'];
    _email = json['email'];
    _fullName = json['fullName'];
    _gender = json['gender'];
    _group = json['group'];
    _picture = json['picture'];
    _cover = json['cover'];
    _province = json['province'];
    _provinceIdentityCardNumber = json['provinceIdentityCardNumber'];
    _status = json['status'];
    _username = json['username'];
    _birthPlace = json['birthPlace'];
    _ktaNumber = json['ktaNumber'];
    _ktpNumber = json['ktpNumber'];
    _userPoint = json['userPoint'];
    _referralCode = json['referralCode'];
  }

  int? _accumulatedPoints;
  String? _userUuid;
  String? _phoneNumber;
  String? _address;
  String? _birthdate;
  String? _bloodType;
  String? _email;
  String? _fullName;
  String? _gender;
  String? _group;
  String? _picture;
  String? _cover;
  String? _province;
  String? _provinceIdentityCardNumber;
  String? _status;
  String? _username;
  String? _birthPlace;
  String? _ktpNumber;
  String? _ktaNumber;
  int? _userPoint;
  String? _referralCode;

  int? get accumulatedPoints => _accumulatedPoints;

  String? get userUuid => _userUuid;

  String? get phoneNumber => _phoneNumber;

  String? get address => _address;

  String? get birthdate => _birthdate;

  String? get bloodType => _bloodType;

  String? get email => _email;

  String? get fullName => _fullName;

  String? get gender => _gender;

  String? get group => _group;

  String? get picture => _picture;

  String? get cover => _cover;

  String? get province => _province;

  String? get status => _status;

  String? get username => _username;

  String? get birthPlace => _birthPlace;

  String? get ktpNumber => _ktpNumber;

  String? get ktaNumber => _ktaNumber;

  int? get userPoint => _userPoint;

  String? get referralCode => _referralCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['accumulatedPoints'] = _accumulatedPoints;
    map['userUuid'] = _userUuid;
    map['phoneNumber'] = phoneNumber;
    map['address'] = _address;
    map['birthdate'] = _birthdate;
    map['bloodType'] = _bloodType;
    map['email'] = _email;
    map['fullName'] = _fullName;
    map['gender'] = _gender;
    map['group'] = _group;
    map['picture'] = _picture;
    map['cover'] = _cover;
    map['province'] = _province;
    map['provinceIdentityCardNumber'] = _provinceIdentityCardNumber;
    map['status'] = _status;
    map['username'] = _username;
    map['birthPlace'] = _birthPlace;
    map['ktaNumber'] = _ktaNumber;
    map['ktpNumber'] = _ktpNumber;
    map['userPoint'] = _userPoint;
    map['referralCode'] = _referralCode;
    return map;
  }
}
