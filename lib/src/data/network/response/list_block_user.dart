import 'package:imi/src/data/network/response/person_profile.dart';

class ListBlockDataProfile {

  String? userUuid;
  String? avatarUrl;
  String? fullName;

  ListBlockDataProfile({
    this.userUuid,
    this.avatarUrl,
    this.fullName,
  });
  ListBlockDataProfile.fromJson(Map<String, dynamic> json) {
    userUuid = json['userUuid']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    fullName = json['fullName']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['userUuid'] = userUuid;
    data['avatarUrl'] = avatarUrl;
    data['fullName'] = fullName;
    return data;
  }
}

class ListBlockData {

  int? id;
  String? userUuid;
  PersonProfile? profile;

  ListBlockData({
    this.id,
    this.userUuid,
    this.profile,
  });
  ListBlockData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    userUuid = json['userUuid']?.toString();
    profile = (json['profile'] != null)
        ? PersonProfile.fromJson(json['profile'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['userUuid'] = userUuid;
    if (profile != null) {
      data['profile'] = profile!.toJson();
    }
    return data;
  }
}

class FetchBlockedUsers {

  String? nextToken;
  int? total;
  List<ListBlockData>? data;

  FetchBlockedUsers({
    this.nextToken,
    this.total,
    this.data,
  });
  FetchBlockedUsers.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    total = json['total']?.toInt();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <ListBlockData>[];
      v.forEach((v) {
        arr0.add(ListBlockData.fromJson(v));
      });
      this.data = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['total'] = total;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class ListBlockResponse {

  FetchBlockedUsers? fetchBlockedUsers;

  ListBlockResponse({
    this.fetchBlockedUsers,
  });
  ListBlockResponse.fromJson(Map<String, dynamic> json) {
    fetchBlockedUsers = (json['fetchBlockedUsers'] != null)
        ? FetchBlockedUsers.fromJson(json['fetchBlockedUsers'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchBlockedUsers != null) {
      data['fetchBlockedUsers'] = fetchBlockedUsers!.toJson();
    }
    return data;
  }
}

class ListBlock {

  ListBlockResponse? data;

  ListBlock({
    this.data,
  });
  ListBlock.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? ListBlockResponse.fromJson(json['data'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
