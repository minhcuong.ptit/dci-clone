import 'package:imi/src/data/network/response/detail_challenge_response.dart';

class FetchChallenge {
  String? nextToken;
  String? total;
  List<ChallengeVersionById>? data;

  FetchChallenge({
    this.nextToken,
    this.total,
    this.data,
  });

  FetchChallenge.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    total = json['total']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <ChallengeVersionById>[];
      v.forEach((v) {
        arr0.add(ChallengeVersionById.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['total'] = total;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class ChallengesData {
  FetchChallenge? fetchChallenge;

  ChallengesData({
    this.fetchChallenge,
  });

  ChallengesData.fromJson(Map<String, dynamic> json) {
    fetchChallenge = (json['fetchChallenge'] != null)
        ? FetchChallenge.fromJson(json['fetchChallenge'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchChallenge != null) {
      data['fetchChallenge'] = fetchChallenge!.toJson();
    }
    return data;
  }
}

class ChallengesResponse {
  ChallengesData? data;

  ChallengesResponse({
    this.data,
  });

  ChallengesResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? ChallengesData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
