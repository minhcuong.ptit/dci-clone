class Missions {
  int? id;
  String? name;

  Missions({
    this.id,
    this.name,
  });

  Missions.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class ChallengeMissionHistory {
  int? id;
  int? dayNo;
  DateTime? completedDate;
  List<Missions?>? missions;
  List<int?>? completedMissionIds;

  ChallengeMissionHistory(
      {this.id,
      this.dayNo,
      this.missions,
      this.completedMissionIds,
      this.completedDate});

  ChallengeMissionHistory.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    dayNo = json['dayNo']?.toInt();
    if (json['missions'] != null) {
      final v = json['missions'];
      final arr0 = <Missions>[];
      v.forEach((v) {
        arr0.add(Missions.fromJson(v));
      });
      missions = arr0;
    }
    if (json['completedMissionIds'] != null) {
      final v = json['completedMissionIds'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      completedMissionIds = arr0;
    }
    if (json['completedDate'] != null)
      completedDate =
          DateTime.fromMillisecondsSinceEpoch(json['completedDate']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['dayNo'] = dayNo;
    if (missions != null) {
      final v = missions;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['missions'] = arr0;
    }
    if (completedMissionIds != null) {
      final v = completedMissionIds;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['completedMissionIds'] = arr0;
    }
    return data;
  }
}

class ChallengeHistory {
  int? id;
  String? userUuid;
  DateTime? startedDate;
  DateTime? completedDate;
  int? completedTotal;
  bool? isLocked;
  String? status;
  List<ChallengeMissionHistory?>? missionHistory;

  ChallengeHistory({
    this.id,
    this.userUuid,
    this.startedDate,
    this.completedDate,
    this.completedTotal,
    this.isLocked,
    this.status,
    this.missionHistory,
  });

  ChallengeHistory.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    userUuid = json['userUuid']?.toString();
    if (json['startedDate'] != null)
      startedDate =
          DateTime.fromMillisecondsSinceEpoch(json['startedDate']?.toInt());
    if (json['completedDate'] != null)
      completedDate =
          DateTime.fromMillisecondsSinceEpoch(json['completedDate']?.toInt());
    completedTotal = json['completedTotal']?.toInt();
    isLocked = json['isLocked'];
    status = json['status']?.toString();
    if (json['missionHistory'] != null) {
      final v = json['missionHistory'];
      final arr0 = <ChallengeMissionHistory>[];
      v.forEach((v) {
        arr0.add(ChallengeMissionHistory.fromJson(v));
      });
      missionHistory = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['userUuid'] = userUuid;
    data['startedDate'] = startedDate;
    data['completedTotal'] = completedTotal;
    data['isLocked'] = isLocked;
    data['status'] = status;
    if (missionHistory != null) {
      final v = missionHistory;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['missionHistory'] = arr0;
    }
    return data;
  }
}

class VersionByIdLevel {
/*
{
  "id": 1,
  "name": "Chương trình học level 1"
}
*/

  int? id;
  String? name;

  VersionByIdLevel({
    this.id,
    this.name,
  });

  VersionByIdLevel.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class ChallengeVersionById {
  int? id;
  int? challengeVersionId;
  int? version;
  String? name;
  String? description;
  String? imageUrl;
  VersionByIdLevel? level;
  ChallengeHistory? challengeHistory;
  int? dayNo;

  ChallengeVersionById({
    this.id,
    this.challengeVersionId,
    this.version,
    this.name,
    this.imageUrl,
    this.description,
    this.level,
    this.challengeHistory,
    this.dayNo,
  });

  ChallengeVersionById.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    challengeVersionId = json['challengeVersionId']?.toInt();
    version = json['version']?.toInt();
    name = json['name']?.toString();
    imageUrl = json['imageUrl']?.toString();
    description = json['description']?.toString();
    level = (json['level'] != null)
        ? VersionByIdLevel.fromJson(json['level'])
        : null;
    challengeHistory = (json['challengeHistory'] != null)
        ? ChallengeHistory.fromJson(json['challengeHistory'])
        : null;
    dayNo = json['dayNo']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['challengeVersionId'] = challengeVersionId;
    data['version'] = version;
    data['name'] = name;
    data['description'] = description;
    data['imageUrl'] = imageUrl;
    if (level != null) {
      data['level'] = level!.toJson();
    }
    if (challengeHistory != null) {
      data['challengeHistory'] = challengeHistory!.toJson();
    }
    data['dayNo'] = dayNo;
    return data;
  }
}

class DetailChallengeData {
  ChallengeVersionById? fetchChallengeVersionById;

  DetailChallengeData({
    this.fetchChallengeVersionById,
  });

  DetailChallengeData.fromJson(Map<String, dynamic> json) {
    fetchChallengeVersionById = (json['fetchChallengeVersionById'] != null)
        ? ChallengeVersionById.fromJson(json['fetchChallengeVersionById'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchChallengeVersionById != null) {
      data['fetchChallengeVersionById'] = fetchChallengeVersionById!.toJson();
    }
    return data;
  }
}

class DetailChallenge {
  DetailChallengeData? data;

  DetailChallenge({
    this.data,
  });

  DetailChallenge.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? DetailChallengeData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
