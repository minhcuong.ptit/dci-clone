import 'dart:convert';

class NotificationResponse {
  Notifications? notifications;

  NotificationResponse({
    Notifications? mNotifications,
  }) {
    notifications = mNotifications;
  }

  NotificationResponse.fromJson(dynamic json) {
    notifications = (json['notifications'] != null)
        ? Notifications.fromJson(json['notifications'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (notifications != null) {
      map['notifications'] = notifications?.toJson();
    }
    return map;
  }
}

class Notifications {
  String? nextToken;
  List<NotificationData>? data;

  Notifications({
    this.nextToken,
    this.data,
  });

  Notifications.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <NotificationData>[];
      v.forEach((v) {
        arr0.add(NotificationData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class NotificationData {
  NotificationData(
      {String? type,
      int? id,
      String? title,
      String? body,
      int? createdDate,
      bool? read,
      String? image,
      List<Params>? params}) {
    _type = type;
    _id = id;
    _title = title;
    _body = body;
    _createdDate = createdDate;
    _read = read;
    _image = image;
    _params = params;
  }

  NotificationData.fromJson(dynamic json) {
    _type = json['notificationType'];
    _id = json['id'];
    _title = json['title'];
    _body = json['body'];
    _createdDate = json['createdDate'];
    _read = json['read'];
    _image = json['image'];
    if (json['params'] != null) {
      _params = <Params>[];
      json['params'].forEach((v) {
        _params!.add(new Params.fromJson(v));
      });
    }
  }

  String? _type;
  int? _id;
  String? _title;
  String? _body;
  int? _createdDate;
  bool? _read;
  String? _image;
  List<Params>? _params;

  String? get type => _type;

  int? get id => _id;

  String? get title => _title;

  String? get body => _body;

  int? get createdDate => _createdDate;

  String? get image => _image;

  bool? get read => _read;

  List<Params>? get params => _params;

  set read(bool? isRead) {
    _read = isRead;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['notificationType'] = _type;
    map['id'] = _id;
    map['title'] = title;
    map['body'] = _body;
    map['createdDate'] = _createdDate;
    map['read'] = _read;
    map['image'] = _image;
    if (this.params != null) {
      map['params'] = this.params!.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class NotificationCountResponse {
  int? count;

  NotificationCountResponse({
    int? notificationCount,
  }) {
    count = notificationCount;
  }

  NotificationCountResponse.fromJson(dynamic json) {
    count = json['notificationNo'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (count != null) {
      map['notificationNo'] = count;
    }
    return map;
  }
}

class Params {
  String? value;
  String? key;

  Params({this.value, this.key});

  Params.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['key'] = this.key;
    return data;
  }
}
