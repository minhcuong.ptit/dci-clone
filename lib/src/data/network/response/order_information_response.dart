class OrderItems {
  String? email;
  int? id;
  String? imageUrl;
  int? itemId;
  String? name;
  String? phone;
  int? preCalculatedPrice;
  int? price;
  String? productType;
  int? provinceId;
  int? quantity;
  int? totalDiscountPrice;
  int? totalPrice;
  String? userName;

  OrderItems(
      {this.email,
        this.id,
        this.imageUrl,
        this.itemId,
        this.name,
        this.phone,
        this.preCalculatedPrice,
        this.price,
        this.productType,
        this.provinceId,
        this.quantity,
        this.totalDiscountPrice,
        this.totalPrice,
        this.userName});

  OrderItems.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    id = json['id'];
    imageUrl = json['imageUrl'];
    itemId = json['itemId'];
    name = json['name'];
    phone = json['phone'];
    preCalculatedPrice = json['preCalculatedPrice'];
    price = json['price'];
    productType = json['productType'];
    provinceId = json['provinceId'];
    quantity = json['quantity'];
    totalDiscountPrice = json['totalDiscountPrice'];
    totalPrice = json['totalPrice'];
    userName = json['userName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['id'] = this.id;
    data['imageUrl'] = this.imageUrl;
    data['itemId'] = this.itemId;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['preCalculatedPrice'] = this.preCalculatedPrice;
    data['price'] = this.price;
    data['productType'] = this.productType;
    data['provinceId'] = this.provinceId;
    data['quantity'] = this.quantity;
    data['totalDiscountPrice'] = this.totalDiscountPrice;
    data['totalPrice'] = this.totalPrice;
    data['userName'] = this.userName;
    return data;
  }
}
