import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/enum.dart';

import 'enrolled_kis.dart';
import 'member_dto.dart';

/// membershipInformation : {"address":"aloalo","artDocumentLinks":[],"artDocuments":[],"associationId":1,"bankHolderName":"","bankName":"","bankNumber":"","certDocumentLinks":[],"certDocuments":[],"cityId":"1","cityName":"KABUPATEN ACEH BARAT","clubCategories":"","clubCategoriesLink":null,"clubJoin":null,"clubName":"qwerqwer","clubStatus":"Sub Club","coverPicture":"","districtId":1,"districtName":"ARONGAN LAMBALEK","documentLinks":[],"documents":[],"eCertificateNumber":"","expiredDate":"1670579308844","id":15,"imiPaid":"","ktaNumber":"1111111583","memberStatus":"","members":"","permission":"","personInCharge":"","personInChargeName":"","picPhoneNumber":"620949550395","provinceId":1,"provinceName":"ACEH","rtRwNumber":"123/456","wardId":1,"wardName":"PANTE MUTIA"}

class MembershipInformationResponse {
  MembershipInformationResponse({
    this.membershipInformation,
  });

  MembershipInformationResponse.fromJson(dynamic json) {
    membershipInformation = json['membershipInformation'] != null
        ? MembershipInformation.fromJson(json['membershipInformation'])
        : null;
  }
  MembershipInformation? membershipInformation;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (membershipInformation != null) {
      map['membershipInformation'] = membershipInformation?.toJson();
    }
    return map;
  }
}

/// address : "aloalo"
/// artDocumentLinks : []
/// artDocuments : []
/// associationId : 1
/// bankHolderName : ""
/// bankName : ""
/// bankNumber : ""
/// certDocumentLinks : []
/// certDocuments : []
/// cityId : "1"
/// cityName : "KABUPATEN ACEH BARAT"
/// clubCategories : ""
/// clubCategoriesLink : null
/// clubJoin : null
/// clubName : "qwerqwer"
/// clubStatus : "Sub Club"
/// coverPicture : ""
/// districtId : 1
/// districtName : "ARONGAN LAMBALEK"
/// documentLinks : []
/// documents : []
/// eCertificateNumber : ""
/// expiredDate : "1670579308844"
/// id : 15
/// imiPaid : ""
/// ktaNumber : "1111111583"
/// memberStatus : ""
/// members : ""
/// permission : ""
/// personInCharge : ""
/// personInChargeName : ""
/// picPhoneNumber : "620949550395"
/// provinceId : 1
/// provinceName : "ACEH"
/// rtRwNumber : "123/456"
/// wardId : 1
/// wardName : "PANTE MUTIA"

class MembershipInformation {
  MembershipInformation({
    this.address,
    this.artDocumentLinks,
    this.artDocuments,
    this.associationId,
    this.bankHolderName,
    this.bankName,
    this.bankNumber,
    this.certDocumentLinks,
    this.certDocuments,
    this.cityId,
    this.cityName,
    this.clubCategories,
    this.clubCategoriesLink,
    this.clubJoin,
    this.clubName,
    this.clubStatus,
    this.coverPicture,
    this.districtId,
    this.districtName,
    this.documentLinks,
    this.documents,
    this.eCertificateNumber,
    this.expiredDate,
    this.id,
    this.imiPaid,
    this.ktaNumber,
    this.memberStatus,
    this.members,
    this.permission,
    this.personInCharge,
    this.personInChargeName,
    this.picPhoneNumber,
    this.provinceId,
    this.provinceName,
    this.rtRwNumber,
    this.wardId,
    this.wardName,
    this.bloodType,
    this.clubId,
    this.clubProvinceId,
    this.clubProvince,
    this.clubProvinceName,
    this.linkQR,
    this.membershipKis,
    this.packageStatus,
    this.associationName,
    this.idProvinceClubJoin,
    this.simMotor,
    this.simMotorPicture,
    this.simMotorPictureLink,
    this.simCar,
    this.simCarPicture,
    this.simCarPictureLink,
    this.isPayment,
    this.userPackageId,
  });

  MembershipInformation.fromJson(dynamic json) {
    address = json['address'];
    associationId = json['associationId'];
    associationName = json['associationName'];
    bankHolderName = json['bankHolderName'];
    bankName = json['bankName'];
    bankNumber = json['bankNumber'];
    cityId = json['cityId'];
    cityName = json['cityName'];
    clubCategories = json['clubCategories'];
    clubCategoriesLink = json['clubCategoriesLink'];
    clubJoin = json['clubJoin'];
    clubName = json['clubName'];
    clubId = json['clubId'];
    clubProvinceId = json['clubProvinceId'];
    clubProvince = json['clubProvince'];
    clubProvinceName = json['clubProvinceName'];
    clubStatus = json['clubStatus'];
    coverPicture = json['coverPicture'];
    districtId = json['districtId'];
    districtName = json['districtName'];
    permission =
        json['permission'] != null ? json['permission'].cast<String>() : [];
    documents =
        json['documents'] != null ? json['documents'].cast<String>() : [];
    documentLinks = json['documentLinks'] != null
        ? json['documentLinks'].cast<String>()
        : [];
    artDocuments =
        json['artDocuments'] != null ? json['artDocuments'].cast<String>() : [];
    artDocumentLinks = json['artDocumentLinks'] != null
        ? json['artDocumentLinks'].cast<String>()
        : [];
    certDocuments = json['certDocuments'] != null
        ? json['certDocuments'].cast<String>()
        : [];
    certDocumentLinks = json['certDocumentLinks'] != null
        ? json['certDocumentLinks'].cast<String>()
        : [];
    linkQR = json['linkQR'];
    if (json['membershipKis'] != null) {
      membershipKis = [];
      json['membershipKis'].forEach((v) {
        EnrolledKis item = EnrolledKis.fromJson(v);
        if (item.kisStatus == KisStatus.ENABLE) {
          membershipKis?.add(item);
        }
      });
    }
    eCertificateNumber = json['eCertificateNumber'];
    expiredDate = json['expiredDate'];
    id = json['id'];
    bloodType = json['bloodType'];
    imiPaid = json['imiPaid'];
    ktaNumber = json['ktaNumber'];
    memberStatus = MemberStatusType.values
        .firstWhereOrNull((element) => element.name == json['memberStatus']);
    List<MemberRole?> rawRoles = json['memberRole']
            ?.map<MemberRole?>((e) => MemberRole.values
                .firstWhereOrNull((element) => element.name == e))
            ?.toList() ??
        [];
    memberRole = rawRoles.whereNotNull().toList();
    List<MemberDTO?> rawMembers = json['members']
            ?.map<MemberDTO?>((e) => MemberDTO.fromJson(e))
            .toList() ??
        [];
    members = rawMembers.whereNotNull().toList();
    personInCharge = json['personInCharge'];
    personInChargeName = json['personInChargeName'];
    picPhoneNumber = json['picPhoneNumber'];
    provinceId = json['provinceId'];
    provinceName = json['provinceName'];
    rtRwNumber = json['rtRwNumber'];
    wardId = json['wardId'];
    wardName = json['wardName'];
    packageStatus = json['packageStatus'];
    idProvinceClubJoin = json['idProvinceClubJoin'];
    simMotor = json['simMotor'];
    simMotorPicture = json['simMotorPicture'];
    simMotorPictureLink = json['simMotorPictureLink'];
    simCar = json['simCar'];
    simCarPicture = json['simCarPicture'];
    simCarPictureLink = json['simCarPictureLink'];
    isPayment = json['isPayment'];
    userPackageId = json['userPackageId'];
  }
  String? address;
  List<String>? artDocumentLinks;
  List<String>? artDocuments;
  int? associationId;
  String? bankHolderName;
  String? bankName;
  String? bankNumber;
  List<String>? certDocumentLinks;
  List<String>? certDocuments;
  int? cityId;
  String? cityName;
  String? clubCategories;
  dynamic clubCategoriesLink;
  dynamic clubJoin;
  String? clubName;
  int? clubId;
  int? clubProvinceId;
  String? clubProvince;
  String? clubProvinceName;
  String? associationName;
  String? clubStatus;
  String? coverPicture;
  int? districtId;
  String? districtName;
  List<String>? documentLinks;
  List<String>? documents;
  String? linkQR;
  List<EnrolledKis>? membershipKis;
  String? eCertificateNumber;
  String? expiredDate;
  int? id;
  bool? imiPaid;
  String? ktaNumber;
  MemberStatusType? memberStatus;
  List<MemberRole>? memberRole;
  List<MemberDTO>? members;
  List<String>? permission;
  String? personInCharge;
  String? personInChargeName;
  String? picPhoneNumber;
  String? bloodType;
  int? provinceId;
  String? provinceName;
  String? rtRwNumber;
  int? wardId;
  String? wardName;
  String? packageStatus;
  int? idProvinceClubJoin;
  String? simMotor;
  String? simMotorPicture;
  String? simMotorPictureLink;
  String? simCar;
  String? simCarPicture;
  String? simCarPictureLink;
  bool? isPayment;
  int? userPackageId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['address'] = address;
    map['artDocumentLinks'] = artDocumentLinks;
    map['artDocuments'] = artDocuments;
    map['associationId'] = associationId;
    map['bankHolderName'] = bankHolderName;
    map['bankName'] = bankName;
    map['bankNumber'] = bankNumber;
    map['certDocumentLinks'] = certDocumentLinks;
    map['certDocuments'] = certDocuments;
    map['cityId'] = cityId;
    map['cityName'] = cityName;
    map['clubCategories'] = clubCategories;
    map['clubCategoriesLink'] = clubCategoriesLink;
    map['clubJoin'] = clubJoin;
    map['clubName'] = clubName;
    map['clubId'] = clubId;
    map['clubProvinceId'] = clubProvinceId;
    map['clubProvince'] = clubProvince;
    map['clubProvinceName'] = clubProvinceName;
    map['associationName'] = associationName;
    map['clubStatus'] = clubStatus;
    map['coverPicture'] = coverPicture;
    map['districtId'] = districtId;
    map['districtName'] = districtName;
    map['documentLinks'] = documentLinks;
    map['documents'] = documents;
    map['eCertificateNumber'] = eCertificateNumber;
    map['expiredDate'] = expiredDate;
    map['id'] = id;
    map['bloodType'] = bloodType;
    map['imiPaid'] = imiPaid;
    map['ktaNumber'] = ktaNumber;
    map['memberStatus'] = memberStatus?.name;
    map['memberRole'] = memberRole?.map((e) => e.name).toList();
    map['members'] = members?.map((e) => e.toJson()).toList();
    map['permission'] = permission;
    map['personInCharge'] = personInCharge;
    map['personInChargeName'] = personInChargeName;
    map['picPhoneNumber'] = picPhoneNumber;
    map['provinceId'] = provinceId;
    map['provinceName'] = provinceName;
    map['rtRwNumber'] = rtRwNumber;
    map['wardId'] = wardId;
    map['wardName'] = wardName;
    map['packageStatus'] = packageStatus;
    map['linkQR'] = linkQR;
    if (membershipKis != null) {
      map['membershipKis'] = membershipKis?.map((v) => v.toJson()).toList();
    }
    map['idProvinceClubJoin'] = idProvinceClubJoin;
    map['simMotor'] = simMotor;
    map['simMotorPicture'] = simMotorPicture;
    map['simMotorPictureLink'] = simMotorPictureLink;
    map['simCar'] = simCar;
    map['simCarPicture'] = simCarPicture;
    map['simCarPictureLink'] = simCarPictureLink;
    map['isPayment'] = isPayment;
    map['userPackageId'] = userPackageId;
    return map;
  }
}
