import 'category_data.dart';
import 'list_community_response.dart';
import 'post_data.dart';

class PostResponse {
  PostResponse({PostPageData? posts}) {
    _posts = posts;
  }

  PostResponse.fromJson(dynamic json) {
    _posts =
        json['posts'] != null ? PostPageData.fromJson(json['posts']) : null;
  }

  PostPageData? _posts;

  PostPageData? get posts => _posts;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_posts != null) {
      map['posts'] = _posts?.toJson();
    }
    return map;
  }
}
