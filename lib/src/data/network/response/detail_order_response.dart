import 'last_audit_response.dart';
import 'order_history_DTO.dart';

class FetchOrderHistoryByIdV2 {
  LastAudit? lastAudit;
  OrderHistoryDTO? orderHistoryDTO;

  FetchOrderHistoryByIdV2({
    this.lastAudit,
    this.orderHistoryDTO,
  });

  FetchOrderHistoryByIdV2.fromJson(Map<String, dynamic> json) {
    lastAudit = (json['lastAudit'] != null)
        ? LastAudit.fromJson(json['lastAudit'])
        : null;
    orderHistoryDTO = (json['orderHistoryDTO'] != null)
        ? OrderHistoryDTO.fromJson(json['orderHistoryDTO'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (lastAudit != null) {
      data['lastAudit'] = lastAudit!.toJson();
    }
    if (orderHistoryDTO != null) {
      data['orderHistoryDTO'] = orderHistoryDTO!.toJson();
    }
    return data;
  }
}

class DetailOrderData {

  FetchOrderHistoryByIdV2? fetchOrderHistoryByIdV2;

  DetailOrderData({
    this.fetchOrderHistoryByIdV2,
  });

  DetailOrderData.fromJson(Map<String, dynamic> json) {
    fetchOrderHistoryByIdV2 = (json['fetchOrderHistoryByIdV2'] != null)
        ? FetchOrderHistoryByIdV2.fromJson(json['fetchOrderHistoryByIdV2'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchOrderHistoryByIdV2 != null) {
      data['fetchOrderHistoryById'] = fetchOrderHistoryByIdV2!.toJson();
    }
    return data;
  }
}

class DetailOrderResponse {

  DetailOrderData? data;

  DetailOrderResponse({
    this.data,
  });

  DetailOrderResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? DetailOrderData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
