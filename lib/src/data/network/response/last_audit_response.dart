class LastAudit {

  int? id;
  String? note;
  String? status;
  int? createdDate;
  String? createdBySide;
  String? cancelReason;

  LastAudit({
    this.id,
    this.note,
    this.status,
    this.createdDate,
    this.createdBySide,
    this.cancelReason,
  });
  LastAudit.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    note = json['note']?.toString();
    status = json['status']?.toString();
    createdDate = json['createdDate']?.toInt();
    createdBySide = json['createdBySide']?.toString();
    cancelReason = json['cancelReason']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['note'] = note;
    data['status'] = status;
    data['createdDate'] = createdDate;
    data['createdBySide'] = createdBySide;
    data['cancelReason'] = cancelReason;
    return data;
  }
}