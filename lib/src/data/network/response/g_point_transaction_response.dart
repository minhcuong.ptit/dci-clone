/// nextToken : ""
/// hasNext : true
/// data : {"changedPoint":1,"createdDate":1,"detail":"String","id":1,"transactionType":"INVITE_FRIEND"}

class GPointTransactionResponse {
  GPointTransactionResponse({
      this.nextToken, 
      this.hasNext, 
      this.data,});

  GPointTransactionResponse.fromJson(dynamic json) {
    nextToken = json['nextToken'];
    hasNext = json['hasNext'];
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <GPointTransaction>[];
      v.forEach((v) {
        arr0.add(GPointTransaction.fromJson(v));
      });
      this.data = arr0;
    }
  }
  String? nextToken;
  bool? hasNext;
  List<GPointTransaction>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['nextToken'] = nextToken;
    map['hasNext'] = hasNext;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      map['data'] = arr0;
    }
    return map;
  }

}

/// changedPoint : 1
/// createdDate : 1
/// detail : "String"
/// id : 1
/// transactionType : "INVITE_FRIEND"

class GPointTransaction {
  GPointTransaction({
      this.changedPoint, 
      this.createdDate, 
      this.detail, 
      this.id, 
      this.transactionType,});

  GPointTransaction.fromJson(dynamic json) {
    changedPoint = json['changedPoint'];
    createdDate = json['createdDate'];
    detail = json['detail'];
    id = json['id'];
    transactionType = json['transactionType'];
  }
  int? changedPoint;
  int? createdDate;
  String? detail;
  int? id;
  String? transactionType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['changedPoint'] = changedPoint;
    map['createdDate'] = createdDate;
    map['detail'] = detail;
    map['id'] = id;
    map['transactionType'] = transactionType;
    return map;
  }

}