class UserChannelGroups {
  String? channelGroupsPubnubId;

  UserChannelGroups({
    this.channelGroupsPubnubId,
  });

  UserChannelGroups.fromJson(Map<String, dynamic> json) {
    channelGroupsPubnubId = json['channelGroupsPubnubId']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['channelGroupsPubnubId'] = channelGroupsPubnubId;
    return data;
  }
}

class UserChannelGroupResponse {
  List<UserChannelGroups>? fetchUserChannelGroups;

  UserChannelGroupResponse({
    this.fetchUserChannelGroups,
  });

  UserChannelGroupResponse.fromJson(Map<String, dynamic> json) {
    if (json['fetchUserChannelGroups'] != null) {
      final v = json['fetchUserChannelGroups'];
      final arr0 = <UserChannelGroups>[];
      v.forEach((v) {
        arr0.add(UserChannelGroups.fromJson(v));
      });
      fetchUserChannelGroups = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchUserChannelGroups != null) {
      final v = fetchUserChannelGroups;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['fetchUserChannelGroups'] = arr0;
    }
    return data;
  }
}
