///
/// Code generated by jsonToDartModel https://ashamp.github.io/jsonToDartModel/
///
class EventTopicsDataFetchCourseTopicsData {
/*
{
  "id": 2,
  "topicName": "dd hihi"
}
*/

  int? id;
  String? topicName;

  EventTopicsDataFetchCourseTopicsData({
    this.id,
    this.topicName,
  });
  EventTopicsDataFetchCourseTopicsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    topicName = json['topicName']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['topicName'] = topicName;
    return data;
  }
}

class EventTopicsDataFetchCourseTopics {
/*
{
  "nextToken": "MQ==",
  "data": [
    {
      "id": 2,
      "topicName": "dd hihi"
    }
  ]
}
*/

  String? nextToken;
  List<EventTopicsDataFetchCourseTopicsData?>? data;

  EventTopicsDataFetchCourseTopics({
    this.nextToken,
    this.data,
  });
  EventTopicsDataFetchCourseTopics.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <EventTopicsDataFetchCourseTopicsData>[];
      v.forEach((v) {
        arr0.add(EventTopicsDataFetchCourseTopicsData.fromJson(v));
      });
      this.data = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class EventTopicsData {
/*
{
  "fetchCourseTopics": {
    "nextToken": "MQ==",
    "data": [
      {
        "id": 2,
        "topicName": "dd hihi"
      }
    ]
  }
}
*/

  EventTopicsDataFetchCourseTopics? fetchEventTopics;

  EventTopicsData({
    this.fetchEventTopics,
  });
  EventTopicsData.fromJson(Map<String, dynamic> json) {
    fetchEventTopics = (json['fetchEventTopics'] != null) ? EventTopicsDataFetchCourseTopics.fromJson(json['fetchEventTopics']) : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchEventTopics != null) {
      data['fetchEventTopics'] = fetchEventTopics!.toJson();
    }
    return data;
  }
}

class EventTopics {
/*
{
  "data": {
    "fetchCourseTopics": {
      "nextToken": "MQ==",
      "data": [
        {
          "id": 2,
          "topicName": "dd hihi"
        }
      ]
    }
  }
}
*/

  EventTopicsData? data;

  EventTopics({
    this.data,
  });
  EventTopics.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? EventTopicsData.fromJson(json['data']) : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
