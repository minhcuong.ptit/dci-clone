/// id : 5
/// name : "test"
/// type : "CLUB"
/// ownerId : 1
/// clubInfo : {"id":5,"bankName":null,"bankNumber":null,"bankHolderName":null,"personInCharge":"d4ef2d32-92ae-4fde-9012-794312ab8d14","personInChargeName":null}
/// individualInfo : null

class ClubData {
  ClubData({
    int? id,
    String? name,
    String? type,
    int? ownerId,
    ClubInfo? clubInfo,}){
    _id = id;
    _name = name;
    _type = type;
    _ownerId = ownerId;
    _clubInfo = clubInfo;
  }

  ClubData.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _type = json['type'];
    _ownerId = json['ownerId'];
    _clubInfo = json['clubInfo'] != null ? ClubInfo.fromJson(json['clubInfo']) : null;
  }
  int? _id;
  String? _name;
  String? _type;
  int? _ownerId;
  ClubInfo? _clubInfo;

  int? get id => _id;
  String? get name => _name;
  String? get type => _type;
  int? get ownerId => _ownerId;
  ClubInfo? get clubInfo => _clubInfo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['type'] = _type;
    map['ownerId'] = _ownerId;
    if (_clubInfo != null) {
      map['clubInfo'] = _clubInfo?.toJson();
    }
    return map;
  }

}

/// id : 5
/// bankName : null
/// bankNumber : null
/// bankHolderName : null
/// personInCharge : "d4ef2d32-92ae-4fde-9012-794312ab8d14"
/// personInChargeName : null

class ClubInfo {
  ClubInfo({
    int? id,
    dynamic bankName,
    dynamic bankNumber,
    dynamic bankHolderName,
    String? personInCharge,
    dynamic personInChargeName,}){
    _id = id;
    _bankName = bankName;
    _bankNumber = bankNumber;
    _bankHolderName = bankHolderName;
    _personInCharge = personInCharge;
    _personInChargeName = personInChargeName;
  }

  ClubInfo.fromJson(dynamic json) {
    _id = json['id'];
    _bankName = json['bankName'];
    _bankNumber = json['bankNumber'];
    _bankHolderName = json['bankHolderName'];
    _personInCharge = json['personInCharge'];
    _personInChargeName = json['personInChargeName'];
  }
  int? _id;
  dynamic _bankName;
  dynamic _bankNumber;
  dynamic _bankHolderName;
  String? _personInCharge;
  dynamic _personInChargeName;

  int? get id => _id;
  dynamic get bankName => _bankName;
  dynamic get bankNumber => _bankNumber;
  dynamic get bankHolderName => _bankHolderName;
  String? get personInCharge => _personInCharge;
  dynamic get personInChargeName => _personInChargeName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['bankName'] = _bankName;
    map['bankNumber'] = _bankNumber;
    map['bankHolderName'] = _bankHolderName;
    map['personInCharge'] = _personInCharge;
    map['personInChargeName'] = _personInChargeName;
    return map;
  }

}