import 'package:imi/src/data/network/response/community_data.dart';

class ClubResponse {
  Clubs? clubs;

  ClubResponse({
    Clubs? mClubs,
  }) {
    clubs = mClubs;
  }

  ClubResponse.fromJson(dynamic json) {
    clubs = (json['followCommunities'] != null)
        ? Clubs.fromJson(json['followCommunities'])
        : null;
  }
}

class MyClubResponse {
  Clubs? clubs;

  ClubResponse({
    Clubs? mClubs,
  }) {
    clubs = mClubs;
  }

  MyClubResponse.fromJson(dynamic json) {
    clubs = (json['myClubs'] != null) ? Clubs.fromJson(json['myClubs']) : null;
  }
}

class Clubs {
  String? nextToken;
  List<CommunityData>? data;

  Clubs({
    this.nextToken,
    this.data,
  });

  Clubs.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <CommunityData>[];
      v.forEach((v) {
        arr0.add(CommunityData.fromJson(v));
      });
      this.data = arr0;
    }
  }
}
