class FetchQuestionById {
  int? id;
  String? question;
  String? answer;
  String? status;

  FetchQuestionById({
    this.id,
    this.question,
    this.answer,
    this.status,
  });

  FetchQuestionById.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    question = json['question']?.toString();
    answer = json['answer']?.toString();
    status = json['status']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['question'] = question;
    data['answer'] = answer;
    data['status'] = status;
    return data;
  }
}

class QuestionData {
  FetchQuestionById? fetchQuestionById;

  QuestionData({
    this.fetchQuestionById,
  });

  QuestionData.fromJson(Map<String, dynamic> json) {
    fetchQuestionById = (json['fetchQuestionById'] != null)
        ? FetchQuestionById.fromJson(json['fetchQuestionById'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchQuestionById != null) {
      data['fetchQuestionById'] = fetchQuestionById!.toJson();
    }
    return data;
  }
}

class QuestionResponse {
  QuestionData? data;

  QuestionResponse({
    this.data,
  });

  QuestionResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? QuestionData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
