// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'community_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CommunityDataV2Adapter extends TypeAdapter<CommunityDataV2> {
  @override
  final int typeId = 0;

  @override
  CommunityDataV2 read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CommunityDataV2(
      associationView: fields[0] as AssociationView?,
      imageUrl: fields[1] as String?,
      dynamicLink: fields[17] as String?,
      avatarUrl: fields[2] as String?,
      description: fields[3] as String?,
      location: fields[4] as String?,
      privacy: fields[5] as String?,
      socialLinks: (fields[6] as List?)?.cast<SocialLink?>(),
      categories: (fields[7] as List?)?.cast<CategoryData?>(),
      communityInfo: fields[8] as CommunityInfo?,
      id: fields[9] as int?,
      memberOfCommunities: (fields[10] as List?)?.cast<CommunityDataV2?>(),
      name: fields[11] as String?,
      ownerProfile: fields[12] as PersonProfile?,
      setting: fields[13] as CommunityV2Setting?,
      type: fields[14] as String?,
      userView: fields[15] as UserView?,
      groupView: fields[16] as GroupView?,
    );
  }

  @override
  void write(BinaryWriter writer, CommunityDataV2 obj) {
    writer
      ..writeByte(18)
      ..writeByte(0)
      ..write(obj.associationView)
      ..writeByte(1)
      ..write(obj.imageUrl)
      ..writeByte(2)
      ..write(obj.avatarUrl)
      ..writeByte(3)
      ..write(obj.description)
      ..writeByte(4)
      ..write(obj.location)
      ..writeByte(5)
      ..write(obj.privacy)
      ..writeByte(6)
      ..write(obj.socialLinks)
      ..writeByte(7)
      ..write(obj.categories)
      ..writeByte(8)
      ..write(obj.communityInfo)
      ..writeByte(9)
      ..write(obj.id)
      ..writeByte(10)
      ..write(obj.memberOfCommunities)
      ..writeByte(11)
      ..write(obj.name)
      ..writeByte(12)
      ..write(obj.ownerProfile)
      ..writeByte(13)
      ..write(obj.setting)
      ..writeByte(14)
      ..write(obj.type)
      ..writeByte(15)
      ..write(obj.userView)
      ..writeByte(16)
      ..write(obj.groupView)
      ..writeByte(17)
      ..write(obj.dynamicLink);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CommunityDataV2Adapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
