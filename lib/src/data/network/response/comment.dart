import 'package:imi/src/data/network/response/person_profile.dart';

class CommentData {

  int? id;
  String? text;
  String? avatar;
  int? createdDate;
  String? fullName;
  CommentPage? replies;
  PersonProfile? ownerProfile;

  CommentData({
    this.id,
    this.text,
    this.avatar,
    this.createdDate,
    this.fullName,
    this.replies,
    this.ownerProfile
  });

  CommentData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    text = json['text']?.toString();
    avatar = json['avatar']?.toString();
    createdDate = json['createdDate']?.toInt();
    fullName = json['fullName']?.toString();
    replies = (json['replies'] != null)
        ? CommentPage.fromJson(json['replies'])
        : null;
    ownerProfile = json['ownerProfile'] != null
        ? PersonProfile.fromJson(json['ownerProfile'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['text'] = text;
    data['avatar'] = avatar;
    data['createdDate'] = createdDate;
    data['fullName'] = fullName;
    if (replies != null) {
      data['replies'] = replies!.toJson();
    }
    if (ownerProfile != null) {
      data['ownerProfile'] = ownerProfile!.toJson();
    }
    return data;
  }
}

class CommentResponse {

  CommentPage? comments;

  CommentResponse({
    this.comments,
  });

  CommentResponse.fromJson(dynamic json) {
    comments = (json['comments'] != null)
        ? CommentPage.fromJson(json['comments'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (comments != null) {
      data['replies'] = comments!.toJson();
    }
    return data;
  }
}

class CommentPage {

  String? nextToken;
  bool? hasNext;
  List<CommentData>? data;

  CommentPage({this.nextToken, this.hasNext, this.data});

  CommentPage.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    hasNext = json['hasNext'];
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <CommentData>[];
      v.forEach((v) {
        arr0.add(CommentData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['hasNext'] = hasNext;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}
