import 'dart:convert';

LevelData LevelDataFromJson(String str) => LevelData.fromJson(json.decode(str));

String LevelDataToJson(LevelData data) => json.encode(data.toJson());

class LevelData {
  LevelData({this.id, this.name, this.status});

  int? id;
  String? name;
  String? status;

  factory LevelData.fromJson(Map<String, dynamic> json) => LevelData(
        id: json["id"],
        name: json["name"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {"id": id, "name": name, "status": status};
}
