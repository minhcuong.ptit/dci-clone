import 'dart:convert';

import 'package:imi/src/data/network/response/Level.dart';
import 'package:imi/src/utils/enum.dart';

class ChallengeDetailResponse {
  int? id;
  int? challengeVersionId;
  int? version;
  String? name;
  Level? level;
  int? dayNo;
  String? imageUrl;
  ChallengeHistory? challengeHistory;

  ChallengeDetailResponse({
    this.id,
    this.challengeVersionId,
    this.version,
    this.name,
    this.level,
    this.dayNo,
    this.imageUrl,
    this.challengeHistory,
  });

  factory ChallengeDetailResponse.fromJson(Map<String, dynamic> map) {
    return ChallengeDetailResponse(
      id: map['id']?.toInt(),
      challengeVersionId: map['challengeVersionId']?.toInt(),
      version: map['version']?.toInt(),
      name: map['name'],
      level: map['level'] != null ? Level.fromJson(map['level']) : null,
      dayNo: map['dayNo']?.toInt(),
      imageUrl: map['imageUrl'],
      challengeHistory: map['challengeHistory'] != null
          ? ChallengeHistory.fromJson(map['challengeHistory'])
          : null,
    );
  }
}

class ChallengeHistory {
  int? id;
  String? userUuid;
  DateTime? startedDate;
  DateTime? completedDate;
  int? completedTotal;
  bool? isLocked;
  ChallengeHistoryStatus? status;
  List<ChallengeMissionHistory>? missionHistory;

  ChallengeHistory(
      {this.id,
      this.userUuid,
      this.startedDate,
      this.completedDate,
      this.completedTotal,
      this.isLocked,
      this.missionHistory,
      this.status});

  factory ChallengeHistory.fromJson(Map<String, dynamic> map) {
    return ChallengeHistory(
        id: map['id']?.toInt(),
        userUuid: map['userUuid'],
        startedDate: map['startedDate'] != null
            ? DateTime.fromMillisecondsSinceEpoch(map['startedDate'])
            : null,
        completedDate: map['completedDate'] != null
            ? DateTime.fromMillisecondsSinceEpoch(map['completedDate'])
            : null,
        completedTotal: map['completedTotal']?.toInt(),
        isLocked: map['isLocked'],
        missionHistory: map['missionHistory'] != null
            ? List<ChallengeMissionHistory>.from(map['missionHistory']
                ?.map((x) => ChallengeMissionHistory.fromJson(x)))
            : null,
        status: ChallengeHistoryStatus.values.firstWhere(
            (element) => element.name == map['status'],
            orElse: () => ChallengeHistoryStatus.TODO));
  }
}

class ChallengeMissionHistory {
  int? id;
  int? dayNo;
  List<MissionGraph>? missions;
  List<int>? completedMissionIds;
  String? note;
  ChallengeMissionHistory({
    this.id,
    this.dayNo,
    this.missions,
    this.completedMissionIds,
    this.note,
  });

  factory ChallengeMissionHistory.fromJson(Map<String, dynamic> map) {
    return ChallengeMissionHistory(
      id: map['id']?.toInt(),
      dayNo: map['dayNo']?.toInt(),
      missions: map['missions'] != null
          ? List<MissionGraph>.from(
              map['missions']?.map((x) => MissionGraph.fromJson(x)))
          : null,
      completedMissionIds: map['completedMissionIds'] != null
          ? List<int>.from(map['completedMissionIds'])
          : null,
      note: map['note'],
    );
  }
}

class MissionGraph {
  int? id;
  String? name;
  MissionGraph({
    this.id,
    this.name,
  });

  factory MissionGraph.fromJson(Map<String, dynamic> map) {
    return MissionGraph(
      id: map['id']?.toInt(),
      name: map['name'],
    );
  }
}
