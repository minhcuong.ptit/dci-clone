import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/utils/enum.dart';

class PackageDescription {
  String? code;
  String? name;
  DescriptionType? type;
  String? title;
  String? description;
  String? icon;
  int? orderNo;

  PackageDescription({
    this.code,
    this.name,
    this.type,
    this.title,
    this.description,
    this.icon,
    this.orderNo,
  });

  PackageDescription.fromJson(dynamic json) {
    code = json['code'];
    name = json['name'];
    type =
        DescriptionType.values.firstWhereOrNull((e) => e.name == json['type']);
    title = json['title'];
    description = json['description'];
    icon = json['icon'];
    orderNo = json['orderNo'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['code'] = code
    ..['name'] = name
    ..['type'] = type?.name
    ..['title'] = title
    ..["description"] = description
    ..['icon'] = icon
    ..['orderNo'] = orderNo;
}
