import 'package:imi/src/data/network/response/e_card_data.dart';

class Package {
  int? id;
  String? code;
  String? name;
  int? amount;
  int? initFee;
  int? annualFee;
  int? renewalFee;
  int? processingFee;
  String? color;
  String? iconUrl;
  int? part;
  List<PackageDescription> packageDescriptions = [];

  Package({
    this.id,
    this.code,
    this.name,
    this.amount,
    this.initFee,
    this.annualFee,
    this.renewalFee,
    this.processingFee,
    this.color,
    this.iconUrl,
    this.part,
    required this.packageDescriptions,
  });

  Package.fromJson(dynamic json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    amount = json['amount'];
    initFee = json['initFee'];
    annualFee = json['annualFee'];
    renewalFee = json['renewalFee'];
    processingFee = json['processingFee'];
    color = json['color'];
    iconUrl = json['iconUrl'];
    part = json['part'];
    if (json['packageDescriptions'] != null) {
      json['packageDescriptions'].forEach((v) {
        packageDescriptions.add(PackageDescription.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..["id"] = id
    ..["code"] = code
    ..["name"] = name
    ..["amount"] = amount
    ..['initFee'] = initFee
    ..["annualFee"] = annualFee
    ..['renewalFee'] = renewalFee
    ..['processingFee'] = processingFee
    ..['color'] = color
    ..['iconUrl'] = iconUrl
    ..['part'] = part
    ..['packageDescriptions'] =
        packageDescriptions.map((e) => e.toJson()).toList();
}
