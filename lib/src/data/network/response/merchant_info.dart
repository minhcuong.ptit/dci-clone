/// id : 0
/// userPackageId : 0
/// merchantName : "string"
/// pic : "string"
/// picName : "string"
/// picPhone : "string"
/// picAddress : "string"
/// picRtRw : "string"
/// picProvinceId : 0
/// picCityId : 0
/// picDistrictId : 0
/// picWardId : 0
/// nikNumber : "string"
/// bankName : "string"
/// bankHolderName : "string"
/// bankNumber : "string"
/// companyName : "string"
/// headAddress : "string"
/// headRtRw : "string"
/// headProvinceId : 0
/// headCityId : 0
/// headDistrictId : 0
/// headWardId : 0
/// headPhone : "string"
/// domAddress : "string"
/// domRtRw : "string"
/// domProvinceId : 0
/// domCityId : 0
/// domDistrictId : 0
/// domWardId : 0
/// domPhone : "string"
/// companyEmail : "string"
/// adArtNumber : "string"
/// adArtDate : "string"
/// npwpNumber : "string"
/// pkpNumber : "string"
/// pkpDate : "string"
/// businessType : "string"
/// documents : ["string"]
/// documentLinks : ["string"]
/// expiredDate : 0
/// merchantType : "IMI_INDIVIDUAL_MERCHANT"
/// ktaNumber : "string"
/// picProvinceName : "string"
/// picCityName : "string"
/// picDistrictName : "string"
/// picWardName : "string"
/// headProvinceName : "string"
/// headDistrictName : "string"
/// headCityName : "string"
/// headWardName : "string"
/// domProvinceName : "string"
/// domCityName : "string"
/// domDistrictName : "string"
/// domWardName : "string"

class MerchantInfo {
  MerchantInfo({
    this.id,
    this.description,
    this.userPackageId,
    this.communityId,
    this.merchantName,
    this.pic,
    this.picName,
    this.picPhone,
    this.picAddress,
    this.picRtRw,
    this.picProvinceId,
    this.picCityId,
    this.picDistrictId,
    this.picWardId,
    this.nikNumber,
    this.bankName,
    this.bankHolderName,
    this.bankNumber,
    this.companyName,
    this.headAddress,
    this.headRtRw,
    this.headProvinceId,
    this.headCityId,
    this.headDistrictId,
    this.headWardId,
    this.headPhone,
    this.domAddress,
    this.domRtRw,
    this.domProvinceId,
    this.domCityId,
    this.domDistrictId,
    this.domWardId,
    this.domPhone,
    this.companyEmail,
    this.adArtNumber,
    this.adArtDate,
    this.npwpNumber,
    this.pkpNumber,
    this.pkpDate,
    this.businessType,
    this.documents,
    this.documentLinks,
    this.expiredDate,
    this.merchantType,
    this.ktaNumber,
    this.picProvinceName,
    this.picCityName,
    this.picDistrictName,
    this.picWardName,
    this.headProvinceName,
    this.headDistrictName,
    this.headCityName,
    this.headWardName,
    this.domProvinceName,
    this.domCityName,
    this.domDistrictName,
    this.domWardName,
  });

  MerchantInfo.fromJson(dynamic json) {
    id = json['id'];
    userPackageId = json['userPackageId'];
    description = json['description'];
    communityId = json['communityId'];
    merchantName = json['merchantName'];
    pic = json['pic'];
    picName = json['picName'];
    picPhone = json['picPhone'];
    picAddress = json['picAddress'];
    picRtRw = json['picRtRw'];
    picProvinceId = json['picProvinceId'];
    picCityId = json['picCityId'];
    picDistrictId = json['picDistrictId'];
    picWardId = json['picWardId'];
    nikNumber = json['nikNumber'];
    bankName = json['bankName'];
    bankHolderName = json['bankHolderName'];
    bankNumber = json['bankNumber'];
    companyName = json['companyName'];
    headAddress = json['headAddress'];
    headRtRw = json['headRtRw'];
    headProvinceId = json['headProvinceId'];
    headCityId = json['headCityId'];
    headDistrictId = json['headDistrictId'];
    headWardId = json['headWardId'];
    headPhone = json['headPhone'];
    domAddress = json['domAddress'];
    domRtRw = json['domRtRw'];
    domProvinceId = json['domProvinceId'];
    domCityId = json['domCityId'];
    domDistrictId = json['domDistrictId'];
    domWardId = json['domWardId'];
    domPhone = json['domPhone'];
    companyEmail = json['companyEmail'];
    adArtNumber = json['adArtNumber'];
    adArtDate = json['adArtDate'];
    npwpNumber = json['npwpNumber'];
    pkpNumber = json['pkpNumber'];
    pkpDate = json['pkpDate'];
    businessType = json['businessType'];
    documents = json['documents'] != null ? json['documents'].cast<String>() : [];
    documentLinks = json['documentLinks'] != null ? json['documentLinks'].cast<String>() : [];
    expiredDate = json['expiredDate'];
    merchantType = json['merchantType'];
    ktaNumber = json['ktaNumber'];
    picProvinceName = json['picProvinceName'];
    picCityName = json['picCityName'];
    picDistrictName = json['picDistrictName'];
    picWardName = json['picWardName'];
    headProvinceName = json['headProvinceName'];
    headDistrictName = json['headDistrictName'];
    headCityName = json['headCityName'];
    headWardName = json['headWardName'];
    domProvinceName = json['domProvinceName'];
    domCityName = json['domCityName'];
    domDistrictName = json['domDistrictName'];
    domWardName = json['domWardName'];
  }

  int? id;
  int? userPackageId;
  int? communityId;
  String? description;
  String? merchantName;
  String? pic;
  String? picName;
  String? picPhone;
  String? picAddress;
  String? picRtRw;
  int? picProvinceId;
  int? picCityId;
  int? picDistrictId;
  int? picWardId;
  String? nikNumber;
  String? bankName;
  String? bankHolderName;
  String? bankNumber;
  String? companyName;
  String? headAddress;
  String? headRtRw;
  int? headProvinceId;
  int? headCityId;
  int? headDistrictId;
  int? headWardId;
  String? headPhone;
  String? domAddress;
  String? domRtRw;
  int? domProvinceId;
  int? domCityId;
  int? domDistrictId;
  int? domWardId;
  String? domPhone;
  String? companyEmail;
  String? adArtNumber;
  String? adArtDate;
  String? npwpNumber;
  String? pkpNumber;
  String? pkpDate;
  String? businessType;
  List<String>? documents;
  List<String>? documentLinks;
  int? expiredDate;
  String? merchantType;
  String? ktaNumber;
  String? picProvinceName;
  String? picCityName;
  String? picDistrictName;
  String? picWardName;
  String? headProvinceName;
  String? headDistrictName;
  String? headCityName;
  String? headWardName;
  String? domProvinceName;
  String? domCityName;
  String? domDistrictName;
  String? domWardName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['userPackageId'] = userPackageId;
    map['communityId'] = communityId;
    map['description'] = description;
    map['merchantName'] = merchantName;
    map['pic'] = pic;
    map['picName'] = picName;
    map['picPhone'] = picPhone;
    map['picAddress'] = picAddress;
    map['picRtRw'] = picRtRw;
    map['picProvinceId'] = picProvinceId;
    map['picCityId'] = picCityId;
    map['picDistrictId'] = picDistrictId;
    map['picWardId'] = picWardId;
    map['nikNumber'] = nikNumber;
    map['bankName'] = bankName;
    map['bankHolderName'] = bankHolderName;
    map['bankNumber'] = bankNumber;
    map['companyName'] = companyName;
    map['headAddress'] = headAddress;
    map['headRtRw'] = headRtRw;
    map['headProvinceId'] = headProvinceId;
    map['headCityId'] = headCityId;
    map['headDistrictId'] = headDistrictId;
    map['headWardId'] = headWardId;
    map['headPhone'] = headPhone;
    map['domAddress'] = domAddress;
    map['domRtRw'] = domRtRw;
    map['domProvinceId'] = domProvinceId;
    map['domCityId'] = domCityId;
    map['domDistrictId'] = domDistrictId;
    map['domWardId'] = domWardId;
    map['domPhone'] = domPhone;
    map['companyEmail'] = companyEmail;
    map['adArtNumber'] = adArtNumber;
    map['adArtDate'] = adArtDate;
    map['npwpNumber'] = npwpNumber;
    map['pkpNumber'] = pkpNumber;
    map['pkpDate'] = pkpDate;
    map['businessType'] = businessType;
    map['documents'] = documents;
    map['documentLinks'] = documentLinks;
    map['expiredDate'] = expiredDate;
    map['merchantType'] = merchantType;
    map['ktaNumber'] = ktaNumber;
    map['picProvinceName'] = picProvinceName;
    map['picCityName'] = picCityName;
    map['picDistrictName'] = picDistrictName;
    map['picWardName'] = picWardName;
    map['headProvinceName'] = headProvinceName;
    map['headDistrictName'] = headDistrictName;
    map['headCityName'] = headCityName;
    map['headWardName'] = headWardName;
    map['domProvinceName'] = domProvinceName;
    map['domCityName'] = domCityName;
    map['domDistrictName'] = domDistrictName;
    map['domWardName'] = domWardName;
    return map;
  }
}
