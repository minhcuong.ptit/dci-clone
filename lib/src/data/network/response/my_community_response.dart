import 'community_data.dart';

/// myCommunity : {"avatarUrl":"String","followerNo":1,"id":0,"imageUrl":"String","memberNo":0,"name":"String","ownerId":0,"type":"String","clubInfo":{"bankHolderName":"String","bankName":"String","bankNumber":"String","clubName":"String","id":0,"personInCharge":"String","personInChargeName":"String"},"individualInfo":{"id":3,"name":"Mobile user","phone":"0982211222","email":"investidea@yopmail.com","address":"86 Dich vong hau","birthPlace":"Hanoi","postalCode":"","hobby":"Music","bloodType":"A","userUuid":"dfd46148-90df-4b06-a9b8-3996245b0c00"}}

class MyCommunityResponse {
  MyCommunityResponse({
    CommunityData? myCommunity,
  }) {
    _myCommunity = myCommunity;
  }

  MyCommunityResponse.fromJson(dynamic json) {
    _myCommunity = json['myCommunity'] != null
        ? CommunityData.fromJson(json['myCommunity'])
        : null;
  }

  CommunityData? _myCommunity;

  CommunityData? get myCommunity => _myCommunity;
}
