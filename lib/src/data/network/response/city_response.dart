/// Code generated by jsonToDartModel https://ashamp.github.io/jsonToDartModel/
///
class CityData {
/*
{
  "CityId": 1,
  "CityName": "Hà Nội"
}
*/

  int? cityId;
  int? clubNumber;
  String? cityName;

  CityData({
    this.cityId,
    this.clubNumber,
    this.cityName,
  });
  CityData.fromJson(Map<String, dynamic> json) {
    cityId = json["id"];
    clubNumber = json["clubNumber"];
    cityName = json["name"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = cityId;
    data["clubNumber"] = clubNumber;
    data["name"] = cityName;
    return data;
  }
}

class CityResponse {
  List<CityData>? address;

  CityResponse({
    this.address,
  });
  CityResponse.fromJson(dynamic json) {
    if (json["address"] != null) {
      final v = json["address"];
      final arr0 = <CityData>[];
      v.forEach((v) {
        arr0.add(CityData.fromJson(v));
      });
      address = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.address != null) {
      final v = this.address;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data["address"] = arr0;
    }
    return data;
  }
}
