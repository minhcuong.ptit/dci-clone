class CouponDetail {
  int? createdDate;
  String? code;
  int? totalCourse;
  int? totalCourseCombo;
  int? totalEvent;
  int? totalEventCombo;
  int? discount;
  String? discountType;
  int? minPrice;
  int? totalDiscount;
  int? expiredDate;
  String? limitType;
  String? description;

  CouponDetail({
    this.createdDate,
    this.code,
    this.totalCourse,
    this.totalCourseCombo,
    this.totalEvent,
    this.totalEventCombo,
    this.discount,
    this.discountType,
    this.minPrice,
    this.totalDiscount,
    this.expiredDate,
    this.limitType,
    this.description,
  });

  CouponDetail.fromJson(Map<String, dynamic> json) {
    createdDate = json['createdDate']?.toInt();
    code = json['code']?.toString();
    totalCourse = json['totalCourse']?.toInt();
    totalCourseCombo = json['totalCourseCombo']?.toInt();
    totalEvent = json['totalEvent']?.toInt();
    totalEventCombo = json['totalEventCombo']?.toInt();
    discount = json['discount']?.toInt();
    discountType = json['discountType']?.toString();
    minPrice = json['minPrice']?.toInt();
    totalDiscount = json['totalDiscount']?.toInt();
    expiredDate = json['expiredDate']?.toInt();
    limitType = json['limitType']?.toString();
    description = json['description']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['createdDate'] = createdDate;
    data['code'] = code;
    data['totalCourse'] = totalCourse;
    data['totalCourseCombo'] = totalCourseCombo;
    data['totalEvent'] = totalEvent;
    data['totalEventCombo'] = totalEventCombo;
    data['discount'] = discount;
    data['discountType'] = discountType;
    data['minPrice'] = minPrice;
    data['totalDiscount'] = totalDiscount;
    data['expiredDate'] = expiredDate;
    data['limitType'] = limitType;
    data['description'] = description;
    return data;
  }
}

class DetailCouponsData {
  CouponDetail? couponDetailGraph;

  DetailCouponsData({
    this.couponDetailGraph,
  });

  DetailCouponsData.fromJson(Map<String, dynamic> json) {
    couponDetailGraph = (json['couponDetailGraph'] != null)
        ? CouponDetail.fromJson(json['couponDetailGraph'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (couponDetailGraph != null) {
      data['couponDetailGraph'] = couponDetailGraph!.toJson();
    }
    return data;
  }
}

class DetailCouponsResponse {
  DetailCouponsData? data;

  DetailCouponsResponse({
    this.data,
  });

  DetailCouponsResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? DetailCouponsData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
