import 'dart:ui';

import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/enum.dart';

/// memberships : [{"id":"1","name":"Individual","type":"INDIVIDUAL","code":"INDIVIDUAL","icon":"https://i.imgur.com/3IA4CA2.jpeg"},{"id":"2","name":"Create Club","type":"CLUB","code":"CLUB","icon":"https://i.imgur.com/3IA4CA2.jpeg"},{"id":"3","name":"Create Association","type":"ASSOCICATION","code":"ASSOCIATION","icon":"https://i.imgur.com/3IA4CA2.jpeg"},{"id":"4","name":"Promotor","type":"PROMOTOR","code":"PROMOTOR","icon":"https://i.imgur.com/3IA4CA2.jpeg"},{"id":"5","name":"Merchant","type":"MERCHANT","code":"MERCHANT","icon":"https://i.imgur.com/3IA4CA2.jpeg"}]

class ListMembershipResponse {
  ListMembershipResponse({
    List<MembershipData>? memberships,
    List<MyMembership>? myMembership,
  }) {
    _memberships = memberships;
    _myMembership = myMembership;
  }

  ListMembershipResponse.fromJson(dynamic json) {
    if (json['memberships'] != null) {
      _memberships = [];
      json['memberships'].forEach((v) {
        _memberships?.add(MembershipData.fromJson(v));
      });
    }
    if (json['myMembership'] != null) {
      _myMembership = [];
      json['myMembership'].forEach((v) {
        _myMembership?.add(MyMembership.fromJson(v));
      });
    }
  }

  List<MembershipData>? _memberships;

  List<MyMembership>? _myMembership;

  List<MyMembership>? get myMembership => _myMembership;

  List<MembershipData>? get memberships => _memberships;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_memberships != null) {
      map['memberships'] = _memberships?.map((v) => v.toJson()).toList();
    }
    if (_myMembership != null) {
      map['myMembership'] = _myMembership?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : "1"
/// name : "Individual"
/// type : "INDIVIDUAL"
/// code : "INDIVIDUAL"
/// icon : "https://i.imgur.com/3IA4CA2.jpeg"

class MembershipData {
  MembershipData({
    String? id,
    String? name,
    MembershipType? type,
    String? code,
    String? icon,
  }) {
    _id = id;
    _name = name;
    _type = type;
    _code = code;
    _icon = icon;
  }

  MembershipData.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _type = MembershipType.values.firstWhereOrNull((element) => element.name == json['type']);
    _code = json['code'];
    _icon = json['icon'];
  }

  String? _id;
  String? _name;
  MembershipType? _type;
  String? _code;
  String? _icon;

  String? get id => _id;

  String? get name => _name;

  MembershipType? get type => _type;

  String? get code => _code;

  String? get icon => _icon;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['type'] = _type?.name;
    map['code'] = _code;
    map['icon'] = _icon;
    return map;
  }
}

/// membershipType : "INDIVIDUAL"
/// userPackageId : 165
/// packageName : "IMI CLUB STANDARD"
/// name : "thanh vu"
/// status : "draft"
/// clubStatus : null
/// clubName : null
/// defaultAvatar : "https://i.imgur.com/3IA4CA2.jpeg"
/// ktaNumber : "1111111299"

class MyMembership {
  MyMembership(
      {this.membershipType,
      this.userPackageId,
      this.informationId,
      this.communityId,
      this.packageName,
      this.packageCode,
      this.packageId,
      this.name,
      this.status,
      this.clubStatus,
      this.clubName,
      this.defaultAvatar,
      this.ktaNumber});

  MyMembership.fromJson(dynamic json) {
    membershipType =
        MembershipType.values.firstWhereOrNull((element) => element.name == json['membershipType']);
    userPackageId = json['userPackageId'];
    informationId = json['informationId'];
    communityId = json['communityId'];
    packageName = json['packageName'];
    packageCode = json['packageCode'];
    packageId = json['packageId'];
    name = json['name'];
    status = UserPackageStatus.values.firstWhereOrNull((element) => element.name == json['status']);
    clubStatus = json['clubStatus'];
    clubName = json['clubName'];
    defaultAvatar = json['defaultAvatar'];
    ktaNumber = json['ktaNumber'];
    expiredDate = json['expiredDate'];
  }

  MembershipType? membershipType;
  int? userPackageId;
  int? informationId;
  int? communityId;
  String? packageName;
  String? packageCode;
  int? packageId;
  String? name;
  UserPackageStatus? status;
  String? clubStatus;
  String? clubName;
  String? defaultAvatar;
  String? ktaNumber;
  int? expiredDate;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['membershipType'] = membershipType?.name;
    map['userPackageId'] = userPackageId;
    map['informationId'] = informationId;
    map['communityId'] = communityId;
    map['packageName'] = packageName;
    map['packageCode'] = packageCode;
    map['packageId'] = packageId;
    map['name'] = name;
    map['status'] = status?.name;
    map['clubStatus'] = clubStatus;
    map['clubName'] = clubName;
    map['defaultAvatar'] = defaultAvatar;
    map['ktaNumber'] = ktaNumber;
    map['expiredDate'] = expiredDate;
    return map;
  }

  bool get isExpiredIn30Days {
    if (status == UserPackageStatus.APPROVED) {
      int now = DateTime.now().millisecondsSinceEpoch;
      int actualExpiredDate = expiredDate ?? now;
      return (actualExpiredDate - now) <
          Const.PACKAGE_WARNING_EXPIRE_THRESHOLD_DAYS * 24 * 3600 * 1000;
    }
    return false;
  }

  Color get color {
    switch (status) {
      case UserPackageStatus.REJECTED:
      case UserPackageStatus.EXPIRED:
        return R.color.red;
      default:
        return R.color.primaryColor;
    }
  }

  String get icon {
    switch (status) {
      case UserPackageStatus.REJECTED:
      case UserPackageStatus.EXPIRED:
        return R.drawable.ic_alert_circle;
      case UserPackageStatus.PROCESSING:
      case UserPackageStatus.VERIFYING:
        return R.drawable.ic_switch;
      case UserPackageStatus.APPROVED:
        if (isExpiredIn30Days) {
          return R.drawable.ic_triangle;
        }
        break;
      default:
        break;
    }
    return R.drawable.ic_draft;
  }
}
