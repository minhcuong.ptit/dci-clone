class Event {
  int? id;
  String? name;
  int? productId;
  String? avatarUrl;

  Event({
    this.id,
    this.name,
    this.productId,
    this.avatarUrl,
  });

  Event.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
    productId = json['productId']?.toInt();
    avatarUrl = json['avatarUrl']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['productId'] = productId;
    data['avatarUrl'] = avatarUrl;
    return data;
  }
}

class UserInformation {
  String? email;
  String? userName;
  String? phone;
  int? provinceId;

  UserInformation({
    this.email,
    this.userName,
    this.phone,
    this.provinceId,
  });

  UserInformation.fromJson(Map<String, dynamic> json) {
    email = json['email']?.toString();
    userName = json['userName']?.toString();
    phone = json['phone']?.toString();
    provinceId = json['provinceId']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = email;
    data['userName'] = userName;
    data['phone'] = phone;
    data['provinceId'] = provinceId;
    return data;
  }
}

class FetchItemByProductIdV2 {
  int? id;
  int? quantity;
  String? validCouponIds;
  String? note;
  String? productType;
  UserInformation? userInformation;
  String? course;
  Event? event;

  FetchItemByProductIdV2({
    this.id,
    this.quantity,
    this.validCouponIds,
    this.note,
    this.productType,
    this.userInformation,
    this.course,
    this.event,
  });

  FetchItemByProductIdV2.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    quantity = json['quantity']?.toInt();
    validCouponIds = json['validCouponIds']?.toString();
    note = json['note']?.toString();
    productType = json['productType']?.toString();
    userInformation = (json['userInformation'] != null)
        ? UserInformation.fromJson(json['userInformation'])
        : null;
    course = json['course']?.toString();
    event = (json['event'] != null) ? Event.fromJson(json['event']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['quantity'] = quantity;
    data['validCouponIds'] = validCouponIds;
    data['note'] = note;
    data['productType'] = productType;
    if (userInformation != null) {
      data['userInformation'] = userInformation!.toJson();
    }
    data['course'] = course;
    if (event != null) {
      data['event'] = event!.toJson();
    }
    return data;
  }
}

class DetailOrderProductData {
  FetchItemByProductIdV2? fetchItemByProductIdV2;

  DetailOrderProductData({
    this.fetchItemByProductIdV2,
  });

  DetailOrderProductData.fromJson(Map<String, dynamic> json) {
    fetchItemByProductIdV2 = (json['fetchItemByProductIdV2'] != null)
        ? FetchItemByProductIdV2.fromJson(json['fetchItemByProductIdV2'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchItemByProductIdV2 != null) {
      data['fetchItemByProductIdV2'] = fetchItemByProductIdV2!.toJson();
    }
    return data;
  }
}

class DetailOrderProductResponse {
  DetailOrderProductData? data;

  DetailOrderProductResponse({
    this.data,
  });

  DetailOrderProductResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? DetailOrderProductData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
