import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:imi/src/data/network/response/community_data.dart';

class MemberOfCommunityData {
/*
{
  "userUuid": "dc6baa60-c1a4-410e-b4e2-bae1fb150d85",
  "groupRoles": [
    "MEMBER"
  ],
  "communityV2": {
    "userView": {
      "isFollowing": false
    }
  }
} 
*/

  String? userUuid;
  List<String?>? groupRoles;
  CommunityDataV2? communityV2;

  MemberOfCommunityData({
    this.userUuid,
    this.groupRoles,
    this.communityV2,
  });
  MemberOfCommunityData.fromJson(Map<String, dynamic> json) {
    userUuid = json['userUuid']?.toString();
    if (json['groupRoles'] != null) {
      final v = json['groupRoles'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v.toString());
      });
      groupRoles = arr0;
    }
    communityV2 = (json['communityV2'] != null)
        ? CommunityDataV2.fromJson(json['communityV2'])
        : null;
  }
}

class MembersListResponse {
/*
{
  "totalMembers": 5,
  "nextToken": "MQ==",
  "data": [
    {
      "userUuid": "dc6baa60-c1a4-410e-b4e2-bae1fb150d85",
      "groupRoles": [
        "MEMBER"
      ],
      "communityV2": {
        "userView": {
          "isFollowing": false
        }
      }
    }
  ]
} 
*/

  int? totalMembers;
  String? nextToken;
  List<MemberOfCommunityData?>? data;
  int? adminNo;
  int? leaderNo;
  MembersListResponse({
    this.totalMembers,
    this.nextToken,
    this.data,
    this.adminNo,
    this.leaderNo,
  });

  factory MembersListResponse.fromJson(Map<String, dynamic> map) {
    return MembersListResponse(
      totalMembers: map['totalMembers']?.toInt(),
      nextToken: map['nextToken'],
      data: map['data'] != null
          ? List<MemberOfCommunityData?>.from(
              map['data']?.map((x) => MemberOfCommunityData?.fromJson(x)))
          : null,
      adminNo: map['adminNo']?.toInt(),
      leaderNo: map['leaderNo']?.toInt(),
    );
  }
}

class MemberOfCommunityResponse {
/*
{
  "memberOfCommunity": {
    "totalMembers": 5,
    "nextToken": "MQ==",
    "data": [
      {
        "userUuid": "dc6baa60-c1a4-410e-b4e2-bae1fb150d85",
        "groupRoles": [
          "MEMBER"
        ],
        "communityV2": {
          "userView": {
            "isFollowing": false
          }
        }
      }
    ]
  }
} 
*/

  MembersListResponse? memberOfCommunity;

  MemberOfCommunityResponse({
    this.memberOfCommunity,
  });
  MemberOfCommunityResponse.fromJson(Map<String, dynamic> json) {
    memberOfCommunity = (json['memberOfCommunity'] != null)
        ? MembersListResponse.fromJson(json['memberOfCommunity'])
        : null;
  }
}
