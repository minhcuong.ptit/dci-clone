
import 'package:imi/src/data/network/response/topic_response.dart';

class FetchQuestionsData {
  int? id;
  String? question;
  String? answer;
  String? status;
  List<Topics>? topics;

  FetchQuestionsData({
    this.id,
    this.question,
    this.answer,
    this.status,
    this.topics,
  });

  FetchQuestionsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    question = json['question']?.toString();
    answer = json['answer']?.toString();
    status = json['status']?.toString();
    if (json['topics'] != null) {
      final v = json['topics'];
      final arr0 = <Topics>[];
      v.forEach((v) {
        arr0.add(Topics.fromJson(v));
      });
      topics = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['question'] = question;
    data['answer'] = answer;
    data['status'] = status;
    if (topics != null) {
      final v = topics;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['topics'] = arr0;
    }
    return data;
  }
}

class FetchQuestions {
  String? nextToken;
  List<FetchQuestionsData>? data;

  FetchQuestions({
    this.nextToken,
    this.data,
  });

  FetchQuestions.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchQuestionsData>[];
      v.forEach((v) {
        arr0.add(FetchQuestionsData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class ListQuestionData {
  FetchQuestions? fetchQuestions;

  ListQuestionData({
    this.fetchQuestions,
  });

  ListQuestionData.fromJson(Map<String, dynamic> json) {
    fetchQuestions = (json['fetchQuestions'] != null)
        ? FetchQuestions.fromJson(json['fetchQuestions'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchQuestions != null) {
      data['fetchQuestions'] = fetchQuestions!.toJson();
    }
    return data;
  }
}

class ListQuestionResponse {
  ListQuestionData? data;

  ListQuestionResponse({
    this.data,
  });

  ListQuestionResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? ListQuestionData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
