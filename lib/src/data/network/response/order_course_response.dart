class IdItem {
  int? id;

  IdItem({
    this.id,
  });

  IdItem.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    return data;
  }
}

class FetchOrderCourseItemById {
  int? id;
  IdItem? item;
  int? quantity;
  String? note;

  FetchOrderCourseItemById({
    this.id,
    this.item,
    this.quantity,
    this.note,
  });

  FetchOrderCourseItemById.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    item = (json['item'] != null) ? IdItem.fromJson(json['item']) : null;
    quantity = json['quantity']?.toInt();
    note = json['note']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    if (item != null) {
      data['item'] = item!.toJson();
    }
    data['quantity'] = quantity;
    data['note'] = note;
    return data;
  }
}

class OrderCourseData {
  FetchOrderCourseItemById? fetchOrderCourseItemById;

  OrderCourseData({
    this.fetchOrderCourseItemById,
  });

  OrderCourseData.fromJson(Map<String, dynamic> json) {
    fetchOrderCourseItemById = (json['fetchOrderCourseItemById'] != null)
        ? FetchOrderCourseItemById.fromJson(json['fetchOrderCourseItemById'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchOrderCourseItemById != null) {
      data['fetchOrderCourseItemById'] = fetchOrderCourseItemById!.toJson();
    }
    return data;
  }
}

class OrderCourseResponse {
  OrderCourseData? data;

  OrderCourseResponse({
    this.data,
  });

  OrderCourseResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? OrderCourseData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
