import 'package:imi/src/data/network/response/user_package.dart';

class MembershipListResponse {
  late List<Membership> memberships;

  MembershipListResponse(this.memberships);

  MembershipListResponse.fromJson(dynamic json) {
    memberships = [];
    if (json['memberships'] != null) {
      json['memberships'].forEach((v) {
        memberships.add(Membership.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}..['memberships'] =
      memberships.map((e) => e.toJson()).toList();
}

class Membership {
  int? id;
  String? code;
  String? name;
  String? type;
  String? icon;
  String? description;
  UserPackage? packageActive;

  Membership({
    this.id,
    this.code,
    this.name,
    this.type,
    this.icon,
    this.description,
    this.packageActive,
  });

  Membership.fromJson(dynamic json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    type = json['type'];
    icon = json['icon'];
    description = json['description'];
    if (json['packageActive'] != null) {
      packageActive = UserPackage.fromJson(json['packageActive']);
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..["id"] = id
    ..["code"] = code
    ..["name"] = name
    ..["type"] = type
    ..['icon'] = icon
    ..["description"] = description
    ..['packageActive'] = packageActive?.toJson();
}
