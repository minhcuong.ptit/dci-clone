class Child {
  String? status;
  String? type;

  Child({
    this.status,
    this.type,
  });

  Child.fromJson(Map<String, dynamic> json) {
    status = json['status']?.toString();
    type = json['type']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['type'] = type;
    return data;
  }
}

class FetchPaymentTypes {
  String? type;
  String? status;
  List<Child>? childs;

  FetchPaymentTypes({
    this.type,
    this.status,
    this.childs,
  });

  FetchPaymentTypes.fromJson(Map<String, dynamic> json) {
    type = json['type']?.toString();
    status = json['status']?.toString();
    if (json['childs'] != null) {
      final v = json['childs'];
      final arr0 = <Child>[];
      v.forEach((v) {
        arr0.add(Child.fromJson(v));
      });
      childs = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['type'] = type;
    data['status'] = status;
    if (childs != null) {
      final v = childs;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['childs'] = arr0;
    }
    return data;
  }
}

class PaymentData {
  List<FetchPaymentTypes>? fetchPaymentTypes;

  PaymentData({
    this.fetchPaymentTypes,
  });

  PaymentData.fromJson(Map<String, dynamic> json) {
    if (json['fetchPaymentTypes'] != null) {
      final v = json['fetchPaymentTypes'];
      final arr0 = <FetchPaymentTypes>[];
      v.forEach((v) {
        arr0.add(FetchPaymentTypes.fromJson(v));
      });
      fetchPaymentTypes = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchPaymentTypes != null) {
      final v = fetchPaymentTypes;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['fetchPaymentTypes'] = arr0;
    }
    return data;
  }
}

class PaymentResponse {
  PaymentData? data;

  PaymentResponse({
    this.data,
  });

  PaymentResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? PaymentData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
