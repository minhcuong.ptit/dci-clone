import 'package:imi/src/data/network/response/community_data.dart';

class ListCommunityLikePostResponse {
  ListCommunityLikePostResponse({this.dciCommunities, this.nextToken});

  DciCommunities? dciCommunities;
  String? nextToken;

  factory ListCommunityLikePostResponse.fromJson(Map<String, dynamic>? json) =>
      ListCommunityLikePostResponse(
        dciCommunities: DciCommunities.fromJson(json?["communitiesLikePost"]),
        nextToken: json?['nextToken']?.toString(),
      );
}

class DciCommunities {
  DciCommunities({
    this.data,
  });

  List<CommunityDataV2>? data;

  factory DciCommunities.fromJson(Map<String, dynamic> json) => DciCommunities(
        data: List<CommunityDataV2>.from(
            json["data"].map((x) => CommunityDataV2.fromJson(x))),
      );
}
