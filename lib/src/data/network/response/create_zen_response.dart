class CreateZenResponse {
  CreateZenResponse({
    required this.dayStreak,
    required this.dayOfWeeks,
    required this.startedDate,
    required this.completedDate,
    required this.duration,
  });

  int dayStreak;
  List<int> dayOfWeeks;
  int startedDate;
  int completedDate;
  int duration;

  factory CreateZenResponse.fromJson(Map<String, dynamic> json) =>
      CreateZenResponse(
        dayStreak: json["dayStreak"],
        dayOfWeeks: List<int>.from(json["dayOfWeeks"].map((x) => x)),
        startedDate: json["startedDate"],
        completedDate: json["completedDate"],
        duration: json["duration"],
      );

  Map<String, dynamic> toJson() => {
        "dayStreak": dayStreak,
        "dayOfWeeks": List<dynamic>.from(dayOfWeeks.map((x) => x)),
        "startedDate": startedDate,
        "completedDate": completedDate,
        "duration": duration,
      };
}
