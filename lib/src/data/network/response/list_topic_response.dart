class FetchTopicsData {
  int? id;
  String? name;
  String? imageUrl;

  FetchTopicsData({
    this.id,
    this.name,
    this.imageUrl,
  });

  FetchTopicsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
    imageUrl = json['imageUrl']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['imageUrl'] = imageUrl;
    return data;
  }
}

class FetchTopics {
  List<FetchTopicsData>? data;
  int? total;
  String? nextToken;

  FetchTopics({
    this.data,
    this.total,
    this.nextToken,
  });

  FetchTopics.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchTopicsData>[];
      v.forEach((v) {
        arr0.add(FetchTopicsData.fromJson(v));
      });
      this.data = arr0;
    }
    total = json['total']?.toInt();
    nextToken = json['nextToken']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data['data'] = arr0;
    }
    data['total'] = total;
    data['nextToken'] = nextToken;
    return data;
  }
}

class ListTopicData {
  FetchTopics? fetchTopics;

  ListTopicData({
    this.fetchTopics,
  });

  ListTopicData.fromJson(Map<String, dynamic> json) {
    fetchTopics = (json['fetchTopics'] != null)
        ? FetchTopics.fromJson(json['fetchTopics'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchTopics != null) {
      data['fetchTopics'] = fetchTopics!.toJson();
    }
    return data;
  }
}

class ListTopicResponse {
  ListTopicData? data;

  ListTopicResponse({
    this.data,
  });

  ListTopicResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? ListTopicData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
