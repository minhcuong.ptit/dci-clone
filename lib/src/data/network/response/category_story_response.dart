class ByLanguageData {
  int? id;
  String? name;

  ByLanguageData({
    this.id,
    this.name,
  });

  ByLanguageData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class CategoriesByLanguage {
  String? nextToken;
  List<ByLanguageData>? data;

  CategoriesByLanguage({
    this.nextToken,
    this.data,
  });

  CategoriesByLanguage.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <ByLanguageData>[];
      v.forEach((v) {
        arr0.add(ByLanguageData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class CategoryStoryData {
  CategoriesByLanguage? categoriesByLanguage;

  CategoryStoryData({
    this.categoriesByLanguage,
  });

  CategoryStoryData.fromJson(Map<String, dynamic> json) {
    categoriesByLanguage = (json['categoriesByLanguage'] != null)
        ? CategoriesByLanguage.fromJson(json['categoriesByLanguage'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (categoriesByLanguage != null) {
      data['categoriesByLanguage'] = categoriesByLanguage!.toJson();
    }
    return data;
  }
}

class CategoryStoryResponse {
  CategoryStoryData? data;

  CategoryStoryResponse({
    this.data,
  });

  CategoryStoryResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? CategoryStoryData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
