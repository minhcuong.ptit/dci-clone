import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/utils/enum.dart';

class InfoCommunityLevel {
  String? levelName;

  InfoCommunityLevel({
    this.levelName,
  });

  InfoCommunityLevel.fromJson(Map<String, dynamic> json) {
    levelName = json['levelName']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['levelName'] = levelName;
    return data;
  }
}

class CommunityInfo {
  int? followerNo;
  int? memberNo;
  int? postNo;
  List<InfoCommunityLevel>? communityLevel;

  CommunityInfo({
    this.followerNo,
    this.memberNo,
    this.postNo,
    this.communityLevel,
  });

  CommunityInfo.fromJson(Map<String, dynamic> json) {
    followerNo = json['followerNo']?.toInt();
    memberNo = json['memberNo']?.toInt();
    postNo = json['postNo']?.toInt();
    if (json['communityLevel'] != null) {
      final v = json['communityLevel'];
      final arr0 = <InfoCommunityLevel>[];
      v.forEach((v) {
        arr0.add(InfoCommunityLevel.fromJson(v));
      });
      communityLevel = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['followerNo'] = followerNo;
    data['memberNo'] = memberNo;
    data['postNo'] = postNo;
    if (communityLevel != null) {
      final v = communityLevel;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['communityLevel'] = arr0;
    }
    return data;
  }
}

class GroupViewEvent {
  int? startTime;
  DayOfWeek? dayOfWeek;

  GroupViewEvent({
    this.startTime,
    this.dayOfWeek,
  });

  GroupViewEvent.fromJson(Map<String, dynamic> json) {
    startTime = json['startTime']?.toInt();
    dayOfWeek = DayOfWeek.values
        .firstWhereOrNull((element) => element.name == json['dayOfWeek']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['startTime'] = startTime;
    data['dayOfWeek'] = dayOfWeek;
    return data;
  }
}

class Members {
  String? fullName;

  Members({
    this.fullName,
  });

  Members.fromJson(Map<String, dynamic> json) {
    fullName = json['fullName']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['fullName'] = fullName;
    return data;
  }
}

class GroupViewBomMembers {
  String? role;
  List<Members>? bomMembers;

  GroupViewBomMembers({
    this.role,
    this.bomMembers,
  });

  GroupViewBomMembers.fromJson(Map<String, dynamic> json) {
    role = json['role']?.toString();
    if (json['bomMembers'] != null) {
      final v = json['bomMembers'];
      final arr0 = <Members>[];
      v.forEach((v) {
        arr0.add(Members.fromJson(v));
      });
      bomMembers = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['role'] = role;
    if (bomMembers != null) {
      final v = bomMembers;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['bomMembers'] = arr0;
    }
    return data;
  }
}

class GroupView {
  List<GroupViewBomMembers>? bomMembers;
  GroupViewEvent? event;

  GroupView({
    this.bomMembers,
    this.event,
  });

  GroupView.fromJson(Map<String, dynamic> json) {
    if (json['bomMembers'] != null) {
      final v = json['bomMembers'];
      final arr0 = <GroupViewBomMembers>[];
      v.forEach((v) {
        arr0.add(GroupViewBomMembers.fromJson(v));
      });
      bomMembers = arr0;
    }
    event =
        (json['event'] != null) ? GroupViewEvent.fromJson(json['event']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (bomMembers != null) {
      final v = bomMembers;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['bomMembers'] = arr0;
    }
    if (event != null) {
      data['event'] = event!.toJson();
    }
    return data;
  }
}

class StudyGroupDataSearchCommunitiesData {
  int? id;
  String? name;
  String? avatarUrl;
  GroupView? groupView;
  CommunityInfo? communityInfo;

  StudyGroupDataSearchCommunitiesData({
    this.id,
    this.name,
    this.avatarUrl,
    this.groupView,
    this.communityInfo,
  });

  StudyGroupDataSearchCommunitiesData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    groupView = (json['groupView'] != null)
        ? GroupView.fromJson(json['groupView'])
        : null;
    communityInfo = (json['communityInfo'] != null)
        ? CommunityInfo.fromJson(json['communityInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['avatarUrl'] = avatarUrl;
    if (groupView != null) {
      data['groupView'] = groupView!.toJson();
    }
    if (communityInfo != null) {
      data['communityInfo'] = communityInfo!.toJson();
    }
    return data;
  }
}

class StudyGroupSearchCommunities {
  String? nextToken;
  int? total;
  List<StudyGroupDataSearchCommunitiesData>? data;

  StudyGroupSearchCommunities({
    this.nextToken,
    this.total,
    this.data,
  });

  StudyGroupSearchCommunities.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    total = json['total']?.toInt();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <StudyGroupDataSearchCommunitiesData>[];
      v.forEach((v) {
        arr0.add(StudyGroupDataSearchCommunitiesData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['total'] = total;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class StudyGroupResponse {
  StudyGroupSearchCommunities? searchCommunities;

  StudyGroupResponse({
    this.searchCommunities,
  });

  StudyGroupResponse.fromJson(Map<String, dynamic> json) {
    searchCommunities = (json['searchCommunities'] != null)
        ? StudyGroupSearchCommunities.fromJson(json['searchCommunities'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (searchCommunities != null) {
      data['searchCommunities'] = searchCommunities!.toJson();
    }
    return data;
  }
}

class StudyGroup {
  StudyGroupResponse? data;

  StudyGroup({
    this.data,
  });

  StudyGroup.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? StudyGroupResponse.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
