import 'dart:convert';

class PlantSeedHistoriesData {
  int? id;
  String? step1;
  List<int>? steps;
  String? completedDate;
  DateTime? createdDate;

  PlantSeedHistoriesData({
    this.id,
    this.step1,
    this.steps,
    this.completedDate,
    this.createdDate,
  });

  PlantSeedHistoriesData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    step1 = json['step1']?.toString();
    if (json['steps'] != null) {
      final v = json['steps'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      steps = arr0;
    }
    completedDate = json['completedDate']?.toString();
    createdDate =
        DateTime.fromMillisecondsSinceEpoch(json['createdDate']?.toInt());
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['step1'] = json.decode("step1");
    if (steps != null) {
      final v = steps;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['steps'] = arr0;
    }
    data['completedDate'] = completedDate;
    data['createdDate'] = createdDate;
    return data;
  }
}

class FetchPlantSeedHistory {
  String? nextToken;
  int? total;
  int? uncompletedTotal;
  List<PlantSeedHistoriesData>? data;

  FetchPlantSeedHistory({
    this.nextToken,
    this.total,
    this.data,
  });

  FetchPlantSeedHistory.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    total = json['total']?.toInt();
    uncompletedTotal = json['uncompletedTotal']?.toInt();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <PlantSeedHistoriesData>[];
      v.forEach((v) {
        arr0.add(PlantSeedHistoriesData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['total'] = total;
    data['uncompletedTotal'] = uncompletedTotal;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class PlanSeedData {
  FetchPlantSeedHistory? fetchPlantSeedHistories;

  PlanSeedData({
    this.fetchPlantSeedHistories,
  });

  PlanSeedData.fromJson(Map<String, dynamic> json) {
    fetchPlantSeedHistories = (json['fetchPlantSeedHistories'] != null)
        ? FetchPlantSeedHistory.fromJson(json['fetchPlantSeedHistories'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchPlantSeedHistories != null) {
      data['fetchPlantSeedHistories'] = fetchPlantSeedHistories!.toJson();
    }
    return data;
  }
}

class PlanSeedResponse {
  PlanSeedData? data;

  PlanSeedResponse({
    this.data,
  });

  PlanSeedResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? PlanSeedData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
