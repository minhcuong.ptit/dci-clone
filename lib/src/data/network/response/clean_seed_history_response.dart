class Seed {
  int? id;
  String? name;
  String? cleanSeedGuide;
  String? plantSeedGuide;
  String? status;

  Seed({
    this.id,
    this.name,
    this.cleanSeedGuide,
    this.plantSeedGuide,
    this.status,
  });

  Seed.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    name = json['name']?.toString();
    cleanSeedGuide = json['cleanSeedGuide']?.toString();
    plantSeedGuide = json['plantSeedGuide']?.toString();
    status = json['status']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['cleanSeedGuide'] = cleanSeedGuide;
    data['plantSeedGuide'] = plantSeedGuide;
    data['status'] = status;
    return data;
  }
}

class Step1 {
  int? id;
  String? action;
  String? actionType;
  String? status;
  Seed? seed;

  Step1({
    this.id,
    this.action,
    this.actionType,
    this.status,
    this.seed,
  });

  Step1.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    action = json['action']?.toString();
    actionType = json['actionType']?.toString();
    status = json['status']?.toString();
    seed = (json['seed'] != null) ? Seed.fromJson(json['seed']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['action'] = action;
    data['actionType'] = actionType;
    data['status'] = status;
    if (seed != null) {
      data['seed'] = seed!.toJson();
    }
    return data;
  }
}

class FetchCleanSeedHistoriesData {
  int? id;
  DateTime? completedDate;
  DateTime? modifiedDate;
  DateTime? createdDate;
  String? userUuid;
  List<int>? steps;
  Step1? step1;
  String? step2;
  String? step3;
  String? step4;

  FetchCleanSeedHistoriesData({
    this.id,
    this.completedDate,
    this.modifiedDate,
    this.createdDate,
    this.userUuid,
    this.steps,
    this.step1,
    this.step2,
    this.step3,
    this.step4,
  });

  FetchCleanSeedHistoriesData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    if (json['completedDate'] != null)
      completedDate =
          DateTime.fromMillisecondsSinceEpoch(json['completedDate']?.toInt());
    if (json['modifiedDate'] != null)
      modifiedDate =
          DateTime.fromMillisecondsSinceEpoch(json['modifiedDate']?.toInt());
    if (json['createdDate'] != null)
      createdDate =
          DateTime.fromMillisecondsSinceEpoch(json['createdDate']?.toInt());
    userUuid = json['userUuid']?.toString();
    if (json['steps'] != null) {
      final v = json['steps'];
      final arr0 = <int>[];
      v.forEach((v) {
        arr0.add(v.toInt());
      });
      steps = arr0;
    }
    step1 = (json['step1'] != null) ? Step1.fromJson(json['step1']) : null;
    step2 = json['step2']?.toString();
    step3 = json['step3']?.toString();
    step4 = json['step4']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['completedDate'] = completedDate;
    data['modifiedDate'] = modifiedDate;
    data['userUuid'] = userUuid;
    if (steps != null) {
      final v = steps;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v);
      });
      data['steps'] = arr0;
    }
    if (step1 != null) {
      data['step1'] = step1!.toJson();
    }
    data['step2'] = step2;
    data['step3'] = step3;
    data['step4'] = step4;
    return data;
  }
}

class FetchCleanSeedHistory {
  int? total;
  int? uncompletedTotal;
  String? nextToken;
  List<FetchCleanSeedHistoriesData>? data;

  FetchCleanSeedHistory({
    this.total,
    this.uncompletedTotal,
    this.nextToken,
    this.data,
  });

  FetchCleanSeedHistory.fromJson(Map<String, dynamic> json) {
    total = json['total']?.toInt();
    uncompletedTotal = json['uncompletedTotal']?.toInt();
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchCleanSeedHistoriesData>[];
      v.forEach((v) {
        arr0.add(FetchCleanSeedHistoriesData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['total'] = total;
    data['uncompletedTotal'] = uncompletedTotal;
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class CleanSeedHistoryData {
  FetchCleanSeedHistory? fetchCleanSeedHistories;

  CleanSeedHistoryData({
    this.fetchCleanSeedHistories,
  });

  CleanSeedHistoryData.fromJson(Map<String, dynamic> json) {
    fetchCleanSeedHistories = (json['fetchCleanSeedHistories'] != null)
        ? FetchCleanSeedHistory.fromJson(json['fetchCleanSeedHistories'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchCleanSeedHistories != null) {
      data['fetchCleanSeedHistories'] = fetchCleanSeedHistories!.toJson();
    }
    return data;
  }
}

class CleanSeedHistoryResponse {
  CleanSeedHistoryData? data;

  CleanSeedHistoryResponse({
    this.data,
  });

  CleanSeedHistoryResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? CleanSeedHistoryData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
