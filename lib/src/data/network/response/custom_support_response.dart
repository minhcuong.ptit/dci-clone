import 'dart:convert';

class CustomerSupportResponse {
  final CustomerSupportDTO fetchCustomerSupport;
  CustomerSupportResponse({
    required this.fetchCustomerSupport,
  });

  factory CustomerSupportResponse.fromJson(Map<String, dynamic> map) {
    return CustomerSupportResponse(
      fetchCustomerSupport:
          CustomerSupportDTO.fromJson(map['fetchCustomerSupport']),
    );
  }
}

class CustomerSupportDTO {
  final String channelPubnubId;
  final String status;

  factory CustomerSupportDTO.fromJson(Map<String, dynamic> map) {
    return CustomerSupportDTO(
      channelPubnubId: map['channelPubnubId'] ?? '',
        status:map['status']
    );
  }

  CustomerSupportDTO({required this.channelPubnubId,required this.status});
}
