import 'package:imi/src/data/network/response/user_data.dart';

class MemberResponse {
  List<UserData>? members;

  MemberResponse({
    this.members,
  });
  MemberResponse.fromJson(dynamic json) {
    if (json["members"] != null) {
      final v = json["members"];
      final arr0 = <UserData>[];
      v.forEach((v) {
        arr0.add(UserData.fromJson(v));
      });
      members = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.members != null) {
      final v = this.members;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data["members"] = arr0;
    }
    return data;
  }
}
