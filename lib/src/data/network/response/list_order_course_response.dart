import 'last_audit_response.dart';
import 'order_history_response.dart';

class FetchOrderHistoryData {
  int? totalItem;
  LastAudit? lastAudit;
  OrderHistory? orderHistory;

  FetchOrderHistoryData({
    this.totalItem,
    this.lastAudit,
    this.orderHistory,
  });

  FetchOrderHistoryData.fromJson(Map<String, dynamic> json) {
    totalItem = json['totalItem']?.toInt();
    lastAudit = (json['lastAudit'] != null)
        ? LastAudit.fromJson(json['lastAudit'])
        : null;
    orderHistory = (json['orderHistory'] != null)
        ? OrderHistory.fromJson(json['orderHistory'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['totalItem'] = totalItem;
    if (lastAudit != null) {
      data['lastAudit'] = lastAudit!.toJson();
    }
    if (orderHistory != null) {
      data['orderHistory'] = orderHistory!.toJson();
    }
    return data;
  }
}

class FetchOrderHistoryV2 {
  String? nextToken;
  List<FetchOrderHistoryData>? data;

  FetchOrderHistoryV2({
    this.nextToken,
    this.data,
  });

  FetchOrderHistoryV2.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchOrderHistoryData>[];
      v.forEach((v) {
        arr0.add(FetchOrderHistoryData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class ListOrderCourseData {
  FetchOrderHistoryV2? fetchOrderHistoryV2;

  ListOrderCourseData({
    this.fetchOrderHistoryV2,
  });

  ListOrderCourseData.fromJson(Map<String, dynamic> json) {
    fetchOrderHistoryV2 = (json['fetchOrderHistoryV2'] != null)
        ? FetchOrderHistoryV2.fromJson(json['fetchOrderHistoryV2'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchOrderHistoryV2 != null) {
      data['fetchOrderHistoryV2'] = fetchOrderHistoryV2!.toJson();
    }
    return data;
  }
}

class ListOrderCourseResponse {
  ListOrderCourseData? data;

  ListOrderCourseResponse({
    this.data,
  });

  ListOrderCourseResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? ListOrderCourseData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
