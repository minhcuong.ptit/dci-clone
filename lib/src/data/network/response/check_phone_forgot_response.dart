import 'package:imi/src/data/network/response/person_profile.dart';

class CheckPhoneForgotData {
  PersonProfile? existsProfileByPhone;

  CheckPhoneForgotData({
    this.existsProfileByPhone,
  });

  CheckPhoneForgotData.fromJson(Map<String, dynamic> json) {
    existsProfileByPhone = (json['existsProfileByPhone'] != null)
        ? PersonProfile.fromJson(json['existsProfileByPhone'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (existsProfileByPhone != null) {
      data['existsProfileByPhone'] = existsProfileByPhone!.toJson();
    }
    return data;
  }
}

class CheckPhoneForgotResponse {
  CheckPhoneForgotData? data;

  CheckPhoneForgotResponse({
    this.data,
  });

  CheckPhoneForgotResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? CheckPhoneForgotData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
