class BuyMoreEvents {
  int? id;
  int? productId;
  int? currentFee;
  int? preferentialFee;
  int? quantity;
  String? name;
  String? avatarUrl;
  bool? isHot;

  BuyMoreEvents({
    this.id,
    this.productId,
    this.currentFee,
    this.preferentialFee,
    this.quantity,
    this.name,
    this.avatarUrl,
    this.isHot,
  });

  BuyMoreEvents.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    productId = json['productId']?.toInt();
    currentFee = json['currentFee']?.toInt();
    preferentialFee = json['preferentialFee']?.toInt();
    quantity = json['quantity']?.toInt();
    name = json['name']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    isHot = json['isHot'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['currentFee'] = currentFee;
    data['preferentialFee'] = preferentialFee;
    data['quantity'] = quantity;
    data['name'] = name;
    data['avatarUrl'] = avatarUrl;
    data['isHot'] = isHot;
    return data;
  }
}

class BuyMoreCourses {
  int? id;
  int? productId;
  int? currentFee;
  int? preferentialFee;
  int? quantity;
  String? name;
  String? avatarUrl;
  bool? isHot;

  BuyMoreCourses({
    this.id,
    this.productId,
    this.currentFee,
    this.preferentialFee,
    this.quantity,
    this.name,
    this.avatarUrl,
    this.isHot,
  });

  BuyMoreCourses.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    productId = json['productId']?.toInt();
    currentFee = json['currentFee']?.toInt();
    preferentialFee = json['preferentialFee']?.toInt();
    quantity = json['quantity']?.toInt();
    name = json['name']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    isHot = json['isHot'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['currentFee'] = currentFee;
    data['preferentialFee'] = preferentialFee;
    data['quantity'] = quantity;
    data['name'] = name;
    data['avatarUrl'] = avatarUrl;
    data['isHot'] = isHot;
    return data;
  }
}

class FetchProductBuyMore {
  List<BuyMoreCourses>? courses;
  List<BuyMoreEvents>? events;

  FetchProductBuyMore({
    this.courses,
    this.events,
  });

  FetchProductBuyMore.fromJson(Map<String, dynamic> json) {
    if (json['courses'] != null) {
      final v = json['courses'];
      final arr0 = <BuyMoreCourses>[];
      v.forEach((v) {
        arr0.add(BuyMoreCourses.fromJson(v));
      });
      courses = arr0;
    }
    if (json['events'] != null) {
      final v = json['events'];
      final arr0 = <BuyMoreEvents>[];
      v.forEach((v) {
        arr0.add(BuyMoreEvents.fromJson(v));
      });
      events = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (courses != null) {
      final v = courses;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['courses'] = arr0;
    }
    if (events != null) {
      final v = events;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['events'] = arr0;
    }
    return data;
  }
}

class BuyMoreProduct {
  FetchProductBuyMore? fetchProductBuyMore;

  BuyMoreProduct({
    this.fetchProductBuyMore,
  });

  BuyMoreProduct.fromJson(Map<String, dynamic> json) {
    fetchProductBuyMore = (json['fetchProductBuyMore'] != null)
        ? FetchProductBuyMore.fromJson(json['fetchProductBuyMore'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchProductBuyMore != null) {
      data['fetchProductBuyMore'] = fetchProductBuyMore!.toJson();
    }
    return data;
  }
}

class BuyMoreResponse {
  BuyMoreProduct? data;

  BuyMoreResponse({
    this.data,
  });

  BuyMoreResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? BuyMoreProduct.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
