import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/enum.dart';

class EnrolledKis {
  int? kisProvinceId;
  String? kisId;
  KisType? kisType;
  String? kisName;
  String? provinceName;
  EnrolledKisStatus? status;
  KisStatus? kisStatus;

  EnrolledKis({
    this.kisProvinceId,
    this.kisId,
    this.kisType,
    this.kisName,
    this.provinceName,
    this.status,
    this.kisStatus,
  });

  EnrolledKis.fromJson(Map<String, dynamic> json) {
    kisProvinceId = json['kisProvinceId']?.toInt();
    kisId = json['kisId'];
    kisType = KisType.values
        .firstWhereOrNull((element) => element.name == json['kisType']);
    kisName = json['kisName'];
    provinceName = json['provinceName'];
    status = EnrolledKisStatus.values
        .firstWhereOrNull((element) => element.name == json['status']);
    kisStatus = KisStatus.values
        .firstWhereOrNull((element) => element.name == json['kisStatus']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['kisProvinceId'] = kisProvinceId;
    data['kisId'] = kisId;
    data['kisType'] = kisType?.name;
    data['kisName'] = kisName;
    data['provinceName'] = provinceName;
    data['status'] = status?.name;
    data['kisStatus'] = kisStatus?.name;
    return data;
  }
}
