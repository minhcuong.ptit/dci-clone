import 'fetch_event_data.dart';

class DetailEventData {
  FetchEventsData? fetchEventById;

  DetailEventData({
    this.fetchEventById,
  });

  DetailEventData.fromJson(Map<String, dynamic> json) {
    fetchEventById = (json['fetchEventById'] != null)
        ? FetchEventsData.fromJson(json['fetchEventById'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchEventById != null) {
      data['fetchEventById'] = fetchEventById!.toJson();
    }
    return data;
  }
}

class DetailEventResponse {
  DetailEventData? data;

  DetailEventResponse({
    this.data,
  });

  DetailEventResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? DetailEventData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
