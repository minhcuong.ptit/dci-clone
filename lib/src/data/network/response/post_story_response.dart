class PostTopicData {
  String? topicName;
  int? id;

  PostTopicData({
    this.topicName,
    this.id,
  });

  PostTopicData.fromJson(Map<String, dynamic> json) {
    topicName = json['topicName']?.toString();
    id = json['id']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['topicName'] = topicName;
    data['id'] = id;
    return data;
  }
}

class FilterPostTopic {
  String? nextToken;
  List<PostTopicData>? data;

  FilterPostTopic({
    this.nextToken,
    this.data,
  });

  FilterPostTopic.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <PostTopicData>[];
      v.forEach((v) {
        arr0.add(PostTopicData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class PostStoryData {
  FilterPostTopic? filterPostTopic;

  PostStoryData({
    this.filterPostTopic,
  });

  PostStoryData.fromJson(Map<String, dynamic> json) {
    filterPostTopic = (json['filterPostTopic'] != null)
        ? FilterPostTopic.fromJson(json['filterPostTopic'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (filterPostTopic != null) {
      data['filterPostTopic'] = filterPostTopic!.toJson();
    }
    return data;
  }
}

class PostStoryResponse {

  PostStoryData? data;

  PostStoryResponse({
    this.data,
  });

  PostStoryResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? PostStoryData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
