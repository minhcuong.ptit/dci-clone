import 'package:equatable/equatable.dart';

class ProfileUserChatChannel extends Equatable {
  String? userUuid;
  String? fullName;
  String? avatarUrl;
  int? id;

  ProfileUserChatChannel({
    this.userUuid,
    this.fullName,
    this.avatarUrl,
    this.id
  });

  ProfileUserChatChannel.fromJson(Map<String, dynamic> json) {
    userUuid = json['userUuid']?.toString();
    fullName = json['fullName']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['userUuid'] = userUuid;
    data['fullName'] = fullName;
    data['avatarUrl'] = avatarUrl;
    data['id'] = id;
    return data;
  }

  @override
  // TODO: implement props
  List<Object?> get props => [userUuid];
}

class ProfileChatUserData {
  List<ProfileUserChatChannel>? fetchProfilesByUserUuids;

  ProfileChatUserData({
    this.fetchProfilesByUserUuids,
  });

  ProfileChatUserData.fromJson(Map<String, dynamic> json) {
    if (json['fetchProfilesByUserUuids'] != null) {
      final v = json['fetchProfilesByUserUuids'];
      final arr0 = <ProfileUserChatChannel>[];
      v.forEach((v) {
        arr0.add(ProfileUserChatChannel.fromJson(v));
      });
      fetchProfilesByUserUuids = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchProfilesByUserUuids != null) {
      final v = fetchProfilesByUserUuids;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['fetchProfilesByUserUuids'] = arr0;
    }
    return data;
  }
}

class ProfileChatUser {
  ProfileChatUserData? data;

  ProfileChatUser({
    this.data,
  });

  ProfileChatUser.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? ProfileChatUserData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
