import 'user_data.dart';

class MembershipProfileResponse {
  MembershipProfileResponse({
    List<MembershipProfile>? membershipProfile,
  }) {
    _membershipProfile = membershipProfile;
  }

  MembershipProfileResponse.fromJson(dynamic json) {
    if (json['membershipProfile'] != null) {
      _membershipProfile = [];
      json['membershipProfile'].forEach((v) {
        _membershipProfile?.add(MembershipProfile.fromJson(v));
      });
    }
  }

  List<MembershipProfile>? _membershipProfile;

  List<MembershipProfile>? get membershipProfile => _membershipProfile;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_membershipProfile != null) {
      map['membershipProfile'] =
          _membershipProfile?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// isEnable : false
/// phone : "006685698"
/// status : null
/// profile : {"userUuid":"d4ef2d32-92ae-4fde-9012-794312ab8d14","fullName":null,"nickname":null,"picture":null,"username":"006685698"}

class MembershipProfile {
  MembershipProfile({
    bool? isEnable,
    String? phone,
    dynamic status,
    UserData? profile,
  }) {
    _isEnable = isEnable;
    _phone = phone;
    _status = status;
    _profile = profile;
  }

  MembershipProfile.fromJson(dynamic json) {
    _isEnable = json['isEnable'];
    _phone = json['phone'];
    _status = json['status'];
    _profile =
        json['profile'] != null ? UserData.fromJson(json['profile']) : null;
  }

  bool? _isEnable;
  String? _phone;
  dynamic _status;
  UserData? _profile;

  bool? get isEnable => _isEnable;

  String? get phone => _phone;

  dynamic get status => _status;

  UserData? get profile => _profile;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isEnable'] = _isEnable;
    map['phone'] = _phone;
    map['status'] = _status;
    if (_profile != null) {
      map['profile'] = _profile?.toJson();
    }
    return map;
  }
}
