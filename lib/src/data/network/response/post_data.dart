import 'dart:convert';

import 'package:imi/src/data/network/request/post_request.dart';

import 'community_data.dart';

class PostPageData {
  PostPageData({
    this.nextToken,
    this.data,
  });

  PostPageData.fromJson(dynamic json) {
    nextToken = json['nextToken'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(PostData.fromJson(v));
      });
    }
  }

  String? nextToken;
  List<PostData>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['nextToken'] = nextToken;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class PostData {
  PostData({
    this.id,
    this.userId,
    this.communityId,
    this.fullName,
    this.avatar,
    this.communityName,
    this.communityAvatar,
    this.text,
    this.textColor,
    this.backgroundImage,
    this.backgroundImages,
    this.backgroundColor,
    this.type,
    this.privacy,
    this.createdDate,
    this.modifiedDate,
    this.metadata,
    this.isLike,
    this.isBookmark,
    this.likeNumber,
    this.commentNumber,
    this.shareNumber,
    this.communityV2,
    this.dynamicLink,
    this.reportedPostInfo,
  });

  PostData.fromJson(dynamic json) {
    id = json['id'];
    userId = json['userUuid'];
    communityId = json['posterCommunityId'];
    fullName = json['fullName'];
    avatar = json['avatar'];
    communityName = json['communityName'];
    communityAvatar = json['communityAvatar'];
    text = json['text'];
    textColor = json['textColor'];
    backgroundImage = json['backgroundImage'];
    backgroundImages = json['backgroundImages'] != null
        ? json['backgroundImages']?.cast<String?>()
        : [];
    backgroundColor = json['backgroundColor'];
    type = json['type'];
    privacy = json['privacy'];
    createdDate = json['createdDate'];
    modifiedDate = json['modifiedDate'];
    metadata = PostMetadata.fromJson(json['description']);
    isLike = json['isLiked'];
    isBookmark = json['isBookmarked'];
    likeNumber = json['likeNumber'];
    commentNumber = json['commentNumber'];
    shareNumber = json['shareNumber'];
    communityV2 = (json['communityV2'] != null)
        ? CommunityDataV2.fromJson(json['communityV2'])
        : null;
    dynamicLink = json['dynamicLink'];
    reportedPostInfo = (json['reportedPostInfo'] != null)
        ? FetchReportedPost.fromJson(json['reportedPostInfo'])
        : null;
  }

  String? id;
  String? userId;
  int? communityId;
  String? fullName;
  String? avatar;
  String? communityName;
  String? communityAvatar;
  String? text;
  String? textColor;
  String? backgroundImage;
  List<String?>? backgroundImages;
  String? backgroundColor;
  String? type;
  String? privacy;
  int? createdDate;
  int? modifiedDate;
  PostMetadata? metadata;
  bool? isLike;
  bool? isBookmark;
  int? likeNumber = 0;
  int? commentNumber = 0;
  int? shareNumber = 0;
  CommunityDataV2? communityV2;
  String? dynamicLink;
  FetchReportedPost? reportedPostInfo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['userUuid'] = userId;
    map['posterCommunityId'] = communityId;
    map['fullName'] = fullName;
    map['avatar'] = avatar;
    map['text'] = text;
    map['textColor'] = textColor;
    map['backgroundImage'] = backgroundImage;
    map['backgroundImages'] = backgroundImages;
    map['backgroundColor'] = backgroundColor;
    map['type'] = type;
    map['privacy'] = privacy;
    map['createdDate'] = createdDate;
    map['modifiedDate'] = modifiedDate;
    map['description'] = json.encode(metadata);
    map['isLiked'] = isLike;
    map['isBookmarked'] = isBookmark;
    map['likeNumber'] = likeNumber;
    map['commentNumber'] = commentNumber;
    map['shareNumber'] = shareNumber;
    map['communityV2'] = communityV2;
    if (reportedPostInfo != null) {
      map['reportedPostInfo'] = reportedPostInfo!.toJson();
    }
    return map;
  }
}

class FetchReportedPost {
  String? nextToken;
  int? total;
  List<FetchReportedPostInfoData>? data;

  FetchReportedPost({
    this.nextToken,
    this.total,
    this.data,
  });

  FetchReportedPost.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    total = json['total']?.toInt();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchReportedPostInfoData>[];
      v.forEach((v) {
        arr0.add(FetchReportedPostInfoData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    data['total'] = total;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class FetchReportedPostInfoData {
  String? avatarUrl;
  int? createdDate;
  String? fullName;
  int? id;
  Reason? reason;
  String? reporterUserUuid;

  FetchReportedPostInfoData({
    this.avatarUrl,
    this.createdDate,
    this.fullName,
    this.id,
    this.reason,
    this.reporterUserUuid,
  });

  FetchReportedPostInfoData.fromJson(Map<String, dynamic> json) {
    avatarUrl = json['avatarUrl']?.toString();
    createdDate = json['createdDate']?.toInt();
    fullName = json['fullName']?.toString();
    id = json['id']?.toInt();
    reason = (json['reason'] != null) ? Reason.fromJson(json['reason']) : null;
    reporterUserUuid = json['reporterUserUuid']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['avatarUrl'] = avatarUrl;
    data['createdDate'] = createdDate;
    data['fullName'] = fullName;
    data['id'] = id;
    if (reason != null) {
      data['reason'] = reason!.toJson();
    }
    data['reporterUserUuid'] = reporterUserUuid;
    return data;
  }
}

class Reason {
  int? id;
  String? reason;
  String? reasonType;

  Reason({
    this.id,
    this.reason,
    this.reasonType,
  });

  Reason.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    reason = json['reason']?.toString();
    reasonType = json['reasonType']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['reason'] = reason;
    data['reasonType'] = reasonType;
    return data;
  }
}
