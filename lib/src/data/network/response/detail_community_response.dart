import 'package:imi/src/data/network/response/club_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';

class DetailCommunityResponse {

  CommunityData? community;

  DetailCommunityResponse({
    this.community,
  });

  DetailCommunityResponse.fromJson(dynamic json) {
    community = (json['community'] != null)
        ? CommunityData.fromJson(json['community'])
        : null;
  }
}

class DetailCommunityResponseV2 {
  CommunityDataV2Response? community;

  DetailCommunityResponseV2({
    this.community,
  });

  DetailCommunityResponseV2.fromJson(dynamic json) {
    community = (json['communityV2'] != null)
        ? CommunityDataV2Response.fromJson(json['communityV2'])
        : null;
  }
}
