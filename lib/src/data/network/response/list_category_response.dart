import 'package:imi/src/data/network/response/category_data.dart';

/// categories : [{"id":1,"name":"#Racing","color":"#FF6B00","order":1},{"id":2,"name":"#Automotive","color":"#FF4264","order":2},{"id":3,"name":"#Clubs","color":"#C300C7","order":3},{"id":4,"name":"#Gocart","color":"#FF22C1","order":4},{"id":5,"name":"#Tamiya","color":"#00D3D3","order":5},{"id":6,"name":"#Area","color":"#AD4828","order":6}]

class ListCategoryResponse {
  ListCategoryResponse({
      List<CategoryData>? categories,}){
    _categories = categories;
}

  ListCategoryResponse.fromJson(dynamic json) {
    if (json['categories'] != null) {
      _categories = [];
      json['categories'].forEach((v) {
        _categories?.add(CategoryData.fromJson(v));
      });
    }
  }
  List<CategoryData>? _categories;

  List<CategoryData>? get categories => _categories;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_categories != null) {
      map['categories'] = _categories?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}