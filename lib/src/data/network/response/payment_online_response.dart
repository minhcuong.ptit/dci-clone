class AlepayResponse {
  int? code;
  String? message;
  String? checkoutUrl;

  AlepayResponse({
    this.code,
    this.message,
    this.checkoutUrl,
  });

  AlepayResponse.fromJson(Map<String, dynamic> json) {
    code = json['code']?.toInt();
    message = json['message']?.toString();
    checkoutUrl = json['checkoutUrl']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['code'] = code;
    data['message'] = message;
    data['checkoutUrl'] = checkoutUrl;
    return data;
  }
}

class PaymentOnlineResponse {
  AlepayResponse? alepayResponse;

  PaymentOnlineResponse({
    this.alepayResponse,
  });

  PaymentOnlineResponse.fromJson(Map<String, dynamic> json) {
    alepayResponse = (json['alepayResponse'] != null)
        ? AlepayResponse.fromJson(json['alepayResponse'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (alepayResponse != null) {
      data['alepayResponse'] = alepayResponse!.toJson();
    }
    return data;
  }
}
