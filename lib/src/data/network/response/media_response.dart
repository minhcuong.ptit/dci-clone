///
/// Code generated by jsonToDartModel https://ashamp.github.io/jsonToDartModel/
///
class MediaData {
  int? id;
  int? communityId;
  int? postId;
  String? s3Key;

  MediaData({
    this.id,
    this.communityId,
    this.postId,
    this.s3Key,
  });

  MediaData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    communityId = json['communityId']?.toInt();
    postId = json['postId']?.toInt();
    s3Key = json['s3Key']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['communityId'] = communityId;
    data['postId'] = postId;
    data['s3Key'] = s3Key;
    return data;
  }
}

class CommunityMedia {
  String? nextToken;
  List<MediaData>? data;

  CommunityMedia({
    this.nextToken,
    this.data,
  });

  CommunityMedia.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <MediaData>[];
      v.forEach((v) {
        arr0.add(MediaData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class MediaResponse {
  CommunityMedia? communityMedia;

  MediaResponse({
    this.communityMedia,
  });

  MediaResponse.fromJson(Map<String, dynamic> json) {
    communityMedia = (json['communityMedia'] != null)
        ? CommunityMedia.fromJson(json['communityMedia'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (communityMedia != null) {
      data['communityMedia'] = communityMedia!.toJson();
    }
    return data;
  }
}
