import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';

import '../../../../res/R.dart';
import '../../../utils/enum.dart';
import 'community_data.dart';

class GroupOfMember {
  CommunityDataV2? group;
  List<String>? userRoles;
  GroupOfMember({
    this.group,
    this.userRoles,
  });

  factory GroupOfMember.fromJson(Map<String, dynamic> map) {
    return GroupOfMember(
      group:
          map['group'] != null ? CommunityDataV2.fromJson(map['group']) : null,
      userRoles: map['userRoles'] != null
          ? List<String>.from(map['userRoles']?.map((x) {
              if (GroupMemberRole.ADMIN.name == x)
                return R.string.admin.tr();
              else if (GroupMemberRole.LEADER.name == x)
                return R.string.leader.tr();
              else if (GroupMemberRole.TREASURER.name == x)
                return R.string.treasurer.tr();
              else if (GroupMemberRole.SECRETARY.name == x)
                return R.string.secretary.tr();
              else if (GroupMemberRole.MEMBER.name == x)
                return R.string.member.tr();
            }))
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'group': group?.toJson(),
      'userRoles': userRoles,
    };
  }
}
