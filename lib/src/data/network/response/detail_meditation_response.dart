class FetchMeditationByIdData {
  int? id;
  String? title;
  String? description;
  String? fileWithMusicUrl;
  String? fileWithNoMusicUrl;
  int? createdDate;
  int? modifiedDate;
  bool? isLocked;
  int? fileWithMusicDuration;
  int? fileWithNoMusicDuration;

  FetchMeditationByIdData({
    this.id,
    this.title,
    this.description,
    this.fileWithMusicUrl,
    this.fileWithNoMusicUrl,
    this.createdDate,
    this.modifiedDate,
    this.isLocked,
    this.fileWithMusicDuration,
    this.fileWithNoMusicDuration,
  });

  FetchMeditationByIdData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    title = json['title']?.toString();
    description = json['description']?.toString();
    fileWithMusicUrl = json['fileWithMusicUrl']?.toString();
    fileWithNoMusicUrl = json['fileWithNoMusicUrl']?.toString();
    createdDate = json['createdDate']?.toInt();
    modifiedDate = json['modifiedDate']?.toInt();
    isLocked = json['isLocked'];
    fileWithMusicDuration = json['fileWithMusicDuration']?.toInt();
    fileWithNoMusicDuration = json['fileWithNoMusicDuration']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['fileWithMusicUrl'] = fileWithMusicUrl;
    data['fileWithNoMusicUrl'] = fileWithNoMusicUrl;
    data['createdDate'] = createdDate;
    data['modifiedDate'] = modifiedDate;
    data['isLocked'] = isLocked;
    data['fileWithMusicDuration'] = fileWithMusicDuration;
    data['fileWithNoMusicDuration'] = fileWithNoMusicDuration;
    return data;
  }
}

class DetailMeditationData {
  FetchMeditationByIdData? fetchMeditationById;

  DetailMeditationData({
    this.fetchMeditationById,
  });

  DetailMeditationData.fromJson(Map<String, dynamic> json) {
    fetchMeditationById = (json['fetchMeditationById'] != null)
        ? FetchMeditationByIdData.fromJson(json['fetchMeditationById'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchMeditationById != null) {
      data['fetchMeditationById'] = fetchMeditationById!.toJson();
    }
    return data;
  }
}

class DetailMeditationResponse {
  DetailMeditationData? data;

  DetailMeditationResponse({
    this.data,
  });

  DetailMeditationResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? DetailMeditationData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
