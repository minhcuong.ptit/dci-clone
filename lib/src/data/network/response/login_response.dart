/// access_token : "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWRW9PY2JEMUVZbE1kcGV2NUFtRlBBTElKM3VKZ2V0M3MzSDRtUm1MT2o4In0.eyJleHAiOjE2MzM5NTQ3NjQsImlhdCI6MTYzMzkxODc2NCwianRpIjoiYWM3M2E0MjYtNzBkMi00MjYxLTliMmMtODRlOGM5YTViNzI1IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5nYXNwb2wuY28uaWQvYXV0aC9yZWFsbXMvZGV2LmltaSIsInN1YiI6ImY5MzNhNmEyLTE5NmItNGQwOC05ZjUwLTlkZmMwNWYxYjAxYyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImRldi5pbWktbW9iaWxlLWFwcC1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiODA4ODk0ZWMtMDZmMy00NWVjLTgwYzUtMTgzODFlMmI5ZmE2IiwiYWNyIjoiMSIsInNjb3BlIjoiYXV0aGVudGljYXRlZC9hcGkvZ3JhcGhxbCIsInNpZCI6IjgwODg5NGVjLTA2ZjMtNDVlYy04MGM1LTE4MzgxZTJiOWZhNiJ9.BRrGkg226sireg9jDXFfv4Uo8MzfVb_QrM9jO48d6DD66lpsDXHeCC-rfsegmNcoJ75qcY2mw0h72a3QolUPOUl1dT10MbbKG8wo5J2JiLf8Gwu4Rcf9yru-xqUu2nV4aQP5ua_uFTjUmekfDwaNFyuFly9xYpc1MZPSJJcBoa8Mgh7gNThJNJr62dWZtBozHcY0_GPMC90USlYj-7B9Jy-87gzHV7jEiq26nC8BQaFzO-aGvQbwJ0nmcYeq3lccLoQjphSWWiXJtrOm4Q2TfkPkh0ywXc7vvEdN30fRV5MN5Zdd_mc0xK_qpHyA8Xryn2rnUv_N5v5gsCD8k_j9eA"
/// expires_in : 36000
/// refresh_expires_in : 1800
/// refresh_token : "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIwNWZmNjY1MC1jMzAxLTRiMjEtODY4NS00YTI5NGVlMjZmYjYifQ.eyJleHAiOjE2MzM5MjA1NjQsImlhdCI6MTYzMzkxODc2NCwianRpIjoiYTQxY2NjNzUtYjE1NS00ZjBlLTk0YzQtZWY1NjBkYTNiYWM4IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5nYXNwb2wuY28uaWQvYXV0aC9yZWFsbXMvZGV2LmltaSIsImF1ZCI6Imh0dHBzOi8va2V5Y2xvYWsuZ2FzcG9sLmNvLmlkL2F1dGgvcmVhbG1zL2Rldi5pbWkiLCJzdWIiOiJmOTMzYTZhMi0xOTZiLTRkMDgtOWY1MC05ZGZjMDVmMWIwMWMiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiZGV2LmltaS1tb2JpbGUtYXBwLWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiI4MDg4OTRlYy0wNmYzLTQ1ZWMtODBjNS0xODM4MWUyYjlmYTYiLCJzY29wZSI6ImF1dGhlbnRpY2F0ZWQvYXBpL2dyYXBocWwiLCJzaWQiOiI4MDg4OTRlYy0wNmYzLTQ1ZWMtODBjNS0xODM4MWUyYjlmYTYifQ.wHOArvanzXSuDCLzGYJiHyhdTzIq3CBx1ZZpdQj6GWY"
/// token_type : "Bearer"
/// not-before-policy : 0
/// session_state : "808894ec-06f3-45ec-80c5-18381e2b9fa6"
/// scope : "authenticated/api/graphql"

class LoginResponse {
  LoginResponse({
      String? accessToken, 
      int? expiresIn, 
      int? refreshExpiresIn, 
      String? refreshToken, 
      String? tokenType, 
      String? sessionState,
      String? scope,}){
    _accessToken = accessToken;
    _expiresIn = expiresIn;
    _refreshExpiresIn = refreshExpiresIn;
    _refreshToken = refreshToken;
    _tokenType = tokenType;
    _sessionState = sessionState;
    _scope = scope;
}

  LoginResponse.fromJson(dynamic json) {
    _accessToken = json['access_token'];
    _expiresIn = json['expires_in'];
    _refreshExpiresIn = json['refresh_expires_in'];
    _refreshToken = json['refresh_token'];
    _tokenType = json['token_type'];
    _sessionState = json['session_state'];
    _scope = json['scope'];
  }
  String? _accessToken;
  int? _expiresIn;
  int? _refreshExpiresIn;
  String? _refreshToken;
  String? _tokenType;
  String? _sessionState;
  String? _scope;

  String? get accessToken => _accessToken;
  int? get expiresIn => _expiresIn;
  int? get refreshExpiresIn => _refreshExpiresIn;
  String? get refreshToken => _refreshToken;
  String? get tokenType => _tokenType;
  String? get sessionState => _sessionState;
  String? get scope => _scope;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = _accessToken;
    map['expires_in'] = _expiresIn;
    map['refresh_expires_in'] = _refreshExpiresIn;
    map['refresh_token'] = _refreshToken;
    map['token_type'] = _tokenType;
    map['session_state'] = _sessionState;
    map['scope'] = _scope;
    return map;
  }

}