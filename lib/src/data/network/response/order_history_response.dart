import 'order_information_response.dart';

class OrderHistory {
  int? id;
  int? price;
  String? paymentType;
  String? code;
  String? status;
  int? createdDate;


  OrderHistory({
    this.id,
    this.price,
    this.paymentType,
    this.code,
    this.status,
    this.createdDate,

  });

  OrderHistory.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    price = json['price']?.toInt();
    paymentType = json['paymentType']?.toString();
    code = json['code']?.toString();
    status = json['status']?.toString();
    createdDate = json['createdDate']?.toInt();

  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['price'] = price;
    data['paymentType'] = paymentType;
    data['code'] = code;
    data['status'] = status;
    data['createdDate'] = createdDate;
      return data;
  }
}
