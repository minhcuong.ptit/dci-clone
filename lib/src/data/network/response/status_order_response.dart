class GetLastAuditByOrderHistoryId {
  String? status;

  GetLastAuditByOrderHistoryId({
    this.status,
  });

  GetLastAuditByOrderHistoryId.fromJson(Map<String, dynamic> json) {
    status = json['status']?.toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    return data;
  }
}

class StatusOrderData {
  GetLastAuditByOrderHistoryId? getLastAuditByOrderHistoryId;

  StatusOrderData({
    this.getLastAuditByOrderHistoryId,
  });

  StatusOrderData.fromJson(Map<String, dynamic> json) {
    getLastAuditByOrderHistoryId =
        (json['getLastAuditByOrderHistoryId'] != null)
            ? GetLastAuditByOrderHistoryId.fromJson(
                json['getLastAuditByOrderHistoryId'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (getLastAuditByOrderHistoryId != null) {
      data['getLastAuditByOrderHistoryId'] =
          getLastAuditByOrderHistoryId!.toJson();
    }
    return data;
  }
}

class StatusOrderResponse {
  StatusOrderData? data;

  StatusOrderResponse({
    this.data,
  });

  StatusOrderResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? StatusOrderData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
