import 'dart:convert';

import 'package:collection/src/iterable_extensions.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

import 'package:imi/src/data/network/response/club_info.dart';
import 'package:imi/src/data/network/response/person_profile.dart';
import 'package:imi/src/data/network/response/user_view.dart';
import 'package:imi/src/utils/enum.dart';

import '../../../../res/R.dart';
import 'category_data.dart';
import 'follow_response.dart';
part 'community_data.g.dart';
/// id : 1
/// name : "Test"
/// type : "CLUB"
/// avatarUrl : null
/// followerNo : null
/// imageUrl : null
/// memberNo : null
/// ownerId : 1

class CommunityData {
  CommunityData({
    this.id,
    this.name,
    this.description,
    this.association,
    this.type,
    this.avatarUrl,
    this.postNo,
    this.followingNo,
    this.followerNo,
    this.imageUrl,
    this.memberNo,
    this.star,
    this.location,
    this.ownerId,
    this.myCommunityContent,
    this.listCategory,
    this.clubInfo,
    this.numberPending,
    this.userRole,
    this.userView,
    this.clubModerator,
    this.clubType,
    this.birthPlace,
    this.communityInfo,
    this.ownerProfile,
  });

  CommunityData.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    association = json['association'];
    type = CommunityType.values
        .firstWhereOrNull((element) => element.name == json['type']);
    avatarUrl = json['avatarUrl'];
    postNo = json['postNo'];
    followerNo = json['followerNo'];
    followingNo = json['followingNo'];
    imageUrl = json['imageUrl'];
    memberNo = json['memberNo'];
    star = json['star'];
    location = json['location'];
    ownerId = json['ownerId'];
    clubInfo = json['information'] != null
        ? ClubInfo.fromJson(json['information'])
        : null;
    myCommunityContent = json['myCommunityContent'] != null
        ? CommunityContent.fromJson(json['myCommunityContent'])
        : null;
    if (json['categories'] != null) {
      final v = json['categories'];
      final arr0 = <CategoryData>[];
      v.forEach((v) {
        arr0.add(CategoryData.fromJson(v));
      });
      this.listCategory = arr0;
    }
    List<MemberRole?> rawRoles = json['userRole']
            ?.map<MemberRole?>((e) => MemberRole.values
                .firstWhereOrNull((element) => element.name == e))
            ?.toList() ??
        [];
    userRole = rawRoles.whereNotNull().toList();
    numberPending = json['numberPending'];
    clubModerator = json['clubModerator'];
    clubType = json['clubType'];
    birthPlace = json['birthPlace'];
    communityInfo = json['communityInfo'] != null
        ? CommunityInfo.fromJson(json['communityInfo'])
        : null;
    userView =
        json['userView'] != null ? UserView.fromJson(json['userView']) : null;
    ownerProfile = json['ownerProfile'] != null
        ? PersonProfile.fromJson(json['ownerProfile'])
        : null;
  }

  int? id;
  String? name;
  String? description;
  String? association;
  CommunityType? type;
  String? avatarUrl;
  int? postNo;
  int? followerNo;
  int? followingNo;
  String? imageUrl;
  int? memberNo;
  int? star;
  String? location;
  int? ownerId;
  ClubInfo? clubInfo;
  CommunityContent? myCommunityContent;
  List<CategoryData>? listCategory;
  int? numberPending;
  List<MemberRole>? userRole;
  bool? clubModerator;
  String? clubType;
  String? birthPlace;
  CommunityInfo? communityInfo;
  UserView? userView;
  PersonProfile? ownerProfile;

  String? get postingTitle {
    if (type == CommunityType.INDIVIDUAL) {
      return R.string.public.tr();
    }
    return name;
  }

  String? get roleDisplay => userRole
      ?.whereNot((element) => MemberRole.CLUB_MODERATOR == element)
      .map((e) => e.asDisplayString())
      .join(" - ");

  bool get isAdmin {
    bool isAdmin = userRole?.contains(MemberRole.CLUB_ADMIN) ?? false;
    return isAdmin;
  }

  bool get canViewPendingInvite {
    bool isAdmin = userRole?.contains(MemberRole.CLUB_ADMIN) ?? false;
    return isAdmin;
  }

  bool get mustTransferAdminRoleToLeave {
    bool isAdmin = userRole?.contains(MemberRole.CLUB_ADMIN) ?? false;
    return isAdmin;
  }

  bool get canViewPendingMember {
    bool isAdmin = userRole?.contains(MemberRole.CLUB_ADMIN) ?? false;
    bool isModerator = userRole?.hasModeratorPermission ?? false;
    bool clubModerator = this.clubModerator ?? false;
    return isAdmin || (isModerator && clubModerator);
  }
}

class CommunityContent {
  CommunityContent({
    required this.clubRole,
    this.isFollowing,
    this.isMember,
  });

  CommunityContent.fromJson(dynamic json) {
    List<ClubRole?> rawRoles = json['clubRole']
        ?.map<ClubRole?>((e) =>
            ClubRole.values.firstWhereOrNull((element) => element.name == e))
        ?.toList();
    clubRole = rawRoles.whereNotNull().toList();
    isFollowing = json['isFollowing'];
    isMember = json['isMember'];
  }

  List<ClubRole> clubRole = [];
  bool? isFollowing;
  bool? isMember;

  bool? get getFollowing => isFollowing;

  set following(bool? follow) {
    isFollowing = follow;
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['clubRole'] = clubRole.map((e) => e.name).toList();
    map['isFollowing'] = isFollowing;
    map['isMember'] = isMember;
    return map;
  }
}

class PrimaryClub {
  PrimaryClub({
    this.name,
  });

  PrimaryClub.fromJson(dynamic json) {
    name = json['name'];
  }
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    return map;
  }
}

class Setting {
  bool? clubModerator;

  Setting({
    this.clubModerator,
  });
  Setting.fromJson(Map<String, dynamic> json) {
    clubModerator = json['clubModerator'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['clubModerator'] = clubModerator;
    return data;
  }
}

class AssociationView {
  String? clubNo;
  List<MemberRole>? memberRoles;

  AssociationView({
    this.clubNo,
    this.memberRoles,
  });
  AssociationView.fromJson(Map<String, dynamic> json) {
    clubNo = json['clubNo']?.toString();
    List<MemberRole?> rawRoles = json['memberRoles']
            ?.map<MemberRole?>((e) => MemberRole.values
                .firstWhereOrNull((element) => element.name == e))
            ?.toList() ??
        [];
    memberRoles = rawRoles.whereNotNull().toList();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['clubNo'] = clubNo;
    data['memberRoles'] = memberRoles?.map((e) => e.name).toList();
    return data;
  }
}

class GroupMember {
  String? avatarUrl;
  String? userUuid;
  String? phone;
  String? fullName;
  int? communityId;
  GroupMember(
      {this.avatarUrl,
      this.userUuid,
      this.phone,
      this.fullName,
      this.communityId});

  factory GroupMember.fromJson(Map<String, dynamic> map) {
    return GroupMember(
        avatarUrl: map['avatarUrl'],
        userUuid: map['userUuid'],
        phone: map['phone'],
        fullName: map['fullName'],
        communityId: map['communityId']);
  }

  Map<String, dynamic> toJson() {
    return {
      'avatarUrl': avatarUrl,
      'userUuid': userUuid,
      'phone': phone,
      'fullName': fullName,
      'communityId': communityId,
    };
  }
}

class BomMemberResponse {
/*
{
  "role": "TREASURER",
  "bomMembers": [
    null
  ]
}
*/

  String? role;
  List<GroupMember?>? bomMembers;
  BomMemberResponse({
    this.role,
    this.bomMembers,
  });

  BomMemberResponse copyWith({
    String? role,
    List<GroupMember?>? bomMembers,
  }) {
    return BomMemberResponse(
      role: role ?? this.role,
      bomMembers: bomMembers ?? this.bomMembers,
    );
  }

  factory BomMemberResponse.fromJson(Map<String, dynamic> map) {
    return BomMemberResponse(
      role: map['role'],
      bomMembers: map['bomMembers'] != null
          ? List<GroupMember?>.from(
              map['bomMembers']?.map((x) => GroupMember?.fromJson(x)))
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'role': role,
      'bomMembers': bomMembers?.map((x) => x?.toJson()).toList(),
    };
  }
}

class GroupViewEvent {
/*
{
  "name": null,
  "startDate": null,
  "endDate": null,
  "dayOfWeek": null,
  "startTime": null,
  "endTime": null
}
*/

  String? name;
  int? startDate;
  int? endDate;
  DayOfWeek? dayOfWeek;
  int? startTime;
  int? endTime;

  GroupViewEvent({
    this.name,
    this.startDate,
    this.endDate,
    this.dayOfWeek,
    this.startTime,
    this.endTime,
  });
  GroupViewEvent.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    startDate = json['startDate'];
    endDate = json['endDate'];
    dayOfWeek = DayOfWeek.values
        .firstWhereOrNull((element) => element.name == json['dayOfWeek']);
    startTime = json['startTime'];
    endTime = json['endTime'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['startDate'] = startDate;
    data['endDate'] = endDate;
    data['dayOfWeek'] = dayOfWeek;
    data['startTime'] = startTime;
    data['endTime'] = endTime;
    return data;
  }
}

class RequestJoin {
  bool? isRequest;
  PersonProfile? inviter;
  RequestJoin({
    this.isRequest,
    this.inviter,
  });

  factory RequestJoin.fromJson(Map<String, dynamic> map) {
    return RequestJoin(
      isRequest: map['isRequest'],
      inviter: map['inviter'] != null
          ? PersonProfile.fromJson(map['inviter'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isRequest': isRequest,
      'inviter': inviter?.toJson(),
    };
  }
}

class GroupView {
/*
{
  "event": {
    "name": null,
    "startDate": null,
    "endDate": null,
    "dayOfWeek": null,
    "startTime": null,
    "endTime": null
  },
  "bomMembers": [
    {
      "role": "TREASURER",
      "bomMembers": [
        null
      ]
    }
  ],
  "myRoles": null
}
*/

  GroupViewEvent? event;
  List<BomMemberResponse?>? bomMembers;
  List<String>? myRoles;
  RequestJoin? request;

  GroupView({
    this.event,
    this.bomMembers,
    this.myRoles,
  });
  GroupView.fromJson(Map<String, dynamic> json) {
    event =
        (json['event'] != null) ? GroupViewEvent.fromJson(json['event']) : null;
    request = (json['request'] != null)
        ? RequestJoin.fromJson(json['request'])
        : null;
    if (json['bomMembers'] != null) {
      final v = json['bomMembers'];
      final arr0 = <BomMemberResponse>[];
      v.forEach((v) {
        arr0.add(BomMemberResponse.fromJson(v));
      });
      bomMembers = arr0;
    }
    if (json['myRoles'] != null) {
      final v = json['myRoles'];
      final arr0 = <String>[];
      v.forEach((v) {
        arr0.add(v);
      });
      myRoles = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'bomMembers': bomMembers?.map((x) => x?.toJson()).toList(),
      'myRoles': myRoles,
      'request': request?.toJson(),
    };
  }
}

class CommunityV2Setting {
/*
{
  "clubModerator": false
}
*/

  bool? clubModerator;

  CommunityV2Setting({
    this.clubModerator,
  });
  CommunityV2Setting.fromJson(Map<String, dynamic> json) {
    clubModerator = json['clubModerator'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['clubModerator'] = clubModerator;
    return data;
  }
}

class CommunityLevel {
/*
{
  "communityId": 105,
  "level": 6,
  "year": null
  levelName
}
*/

  int? communityId;
  int? level;
  int? year;
  String? levelName;
  String? status;

  CommunityLevel({
    this.communityId,
    this.level,
    this.year,
    this.levelName,
    this.status,
  });
  Map<String, dynamic> toJson() {
    return {
      'communityId': communityId,
      'level': level,
      'year': year,
      'levelName': levelName,
      'status': status,
    };
  }

  factory CommunityLevel.fromJson(Map<String, dynamic> map) {
    return CommunityLevel(
      communityId: map['communityId']?.toInt(),
      level: map['level']?.toInt(),
      year: map['year']?.toInt(),
      levelName: map['levelName'],
      status: map['status'],
    );
  }
}

class CommunityInfo {
/*
{
  "followerNo": 6,
  "followingNo": 4,
  "memberNo": 0,
  "postNo": 26,
  "communityLevel": [
    null
  ]
}
*/

  int? followerNo;
  int? followingNo;
  int? memberNo;
  int? postNo;
  List<CommunityLevel?>? communityLevel;
  List<MemberType>? communityRole;
  CommunityInfo(
      {this.followerNo,
      this.followingNo,
      this.memberNo,
      this.postNo,
      this.communityLevel,
      this.communityRole});

  factory CommunityInfo.fromJson(Map<String, dynamic> map) {
    List<MemberType?> rawRoles = map['communityRole']
            ?.map<MemberType?>((e) => MemberType.values
                .firstWhereOrNull((element) => element.name == e))
            ?.toList() ??
        [];
    return CommunityInfo(
        followerNo: map['followerNo']?.toInt(),
        followingNo: map['followingNo']?.toInt(),
        memberNo: map['memberNo']?.toInt(),
        postNo: map['postNo']?.toInt(),
        communityLevel: map['communityLevel'] != null
            ? List<CommunityLevel?>.from(
                map['communityLevel']?.map((x) => CommunityLevel.fromJson(x)))
            : null,
        communityRole: rawRoles.whereNotNull().toList());
  }

  Map<String, dynamic> toJson() {
    return {
      'followerNo': followerNo,
      'followingNo': followingNo,
      'memberNo': memberNo,
      'postNo': postNo,
      'communityLevel': communityLevel?.map((x) => x?.toJson()).toList(),
      'communityRole': communityRole?.map((x) => x.toString()).toList(),
    };
  }
}

class SocialLink {
/*
{
  "name": "",
  "url": ""
}
*/

  String? name;
  String? url;

  SocialLink({
    this.name,
    this.url,
  });
  SocialLink.fromJson(Map<String, dynamic> json) {
    name = json['name']?.toString();
    url = json['url']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['url'] = url;
    return data;
  }
}
@HiveType(typeId: 0)
class CommunityDataV2 {
/*
{
  "associationView": {
    "clubNo": null,
    "memberRoles": null
  },
  "imageUrl": null,
  "avatarUrl": null,
  "description": "a @Chau tạo hộ e ít data về communities với ạ",
  "location": null,
  "privacy": "PUBLIC",
  "socialLinks": [
    null
  ],
  "categories": [
    {
      "color": null,
      "emoji": "U+1F3E2",
      "id": 39,
      "name": "Sức khỏe & Sự bình an ",
      "order": null
    }
  ],
  "communityInfo": {
    "followerNo": 6,
    "followingNo": 4,
    "memberNo": 0,
    "postNo": 26,
    "communityLevel": [
      null
    ]
  },
  "id": 1,
  "memberOfCommunities": [
    null
  ],
  "name": "Tan",
  "ownerProfile": {
    "userUuid": "dce4cc42-f76a-4a1c-a247-4a0e319401d5",
    "provinceName": "Banjarmasin"
  },
  "setting": {
    "clubModerator": false
  },
  "type": "INDIVIDUAL",
  "userView": {
    "associationNamePromotor": null,
    "clubNamePromotor": null,
    "isFollowing": false,
    "numberPending": 0
  },
  "groupView": {
    "event": {
      "name": null,
      "startDate": null,
      "endDate": null,
      "dayOfWeek": null,
      "startTime": null,
      "endTime": null
    },
    "bomMembers": [
      {
        "role": "TREASURER",
        "bomMembers": [
          null
        ]
      }
    ],
    "myRoles": null
  }
}
*/

  @HiveField(0)
  AssociationView? associationView;
  @HiveField(1)
  String? imageUrl;
  @HiveField(2)
  String? avatarUrl;
  @HiveField(3)
  String? description;
  @HiveField(4)
  String? location;
  @HiveField(5)
  String? privacy;
  @HiveField(6)
  List<SocialLink?>? socialLinks;
  @HiveField(7)
  List<CategoryData?>? categories;
  @HiveField(8)
  CommunityInfo? communityInfo;
  @HiveField(9)
  int? id;
  @HiveField(10)
  List<CommunityDataV2?>? memberOfCommunities;
  @HiveField(11)
  String? name;
  @HiveField(12)
  PersonProfile? ownerProfile;
  @HiveField(13)
  CommunityV2Setting? setting;
  @HiveField(14)
  String? type;
  @HiveField(15)
  UserView? userView;
  @HiveField(16)
  GroupView? groupView;
  @HiveField(17)
  String? dynamicLink;

  CommunityDataV2({
    this.associationView,
    this.imageUrl,
    this.dynamicLink,
    this.avatarUrl,
    this.description,
    this.location,
    this.privacy,
    this.socialLinks,
    this.categories,
    this.communityInfo,
    this.id,
    this.memberOfCommunities,
    this.name,
    this.ownerProfile,
    this.setting,
    this.type,
    this.userView,
    this.groupView,
  });

  factory CommunityDataV2.fromJson(Map<String, dynamic> map) {
    return CommunityDataV2(
        associationView: map['associationView'] != null
            ? AssociationView.fromJson(map['associationView'])
            : null,
        imageUrl: map['imageUrl'],
        avatarUrl: map['avatarUrl'],
        description: map['description'],
        location: map['location'],
        privacy: map['privacy'],
        socialLinks: map['socialLinks'] != null
            ? List<SocialLink?>.from(
                map['socialLinks']?.map((x) => SocialLink?.fromJson(x)))
            : null,
        categories: map['categories'] != null
            ? List<CategoryData?>.from(
                map['categories']?.map((x) => CategoryData?.fromJson(x)))
            : null,
        communityInfo: map['communityInfo'] != null
            ? CommunityInfo.fromJson(map['communityInfo'])
            : null,
        id: map['id']?.toInt(),
        memberOfCommunities: map['memberOfCommunities'] != null
            ? List<CommunityDataV2?>.from(map['memberOfCommunities']
                ?.map((x) => CommunityDataV2?.fromJson(x)))
            : null,
        name: map['name'],
        ownerProfile: map['ownerProfile'] != null
            ? PersonProfile.fromJson(map['ownerProfile'])
            : null,
        setting: map['setting'] != null
            ? CommunityV2Setting.fromJson(map['setting'])
            : null,
        type: map['type'],
        userView:
            map['userView'] != null ? UserView.fromJson(map['userView']) : null,
        groupView: map['groupView'] != null
            ? GroupView.fromJson(map['groupView'])
            : null,
        dynamicLink: map['dynamicLink']);
  }
  @override
  String toString() {
    return 'CommunityDataV2(associationView: $associationView, imageUrl: $imageUrl, avatarUrl: $avatarUrl, description: $description, location: $location, privacy: $privacy, socialLinks: $socialLinks, categories: $categories, communityInfo: $communityInfo, id: $id, memberOfCommunities: $memberOfCommunities, name: $name, ownerProfile: $ownerProfile, setting: $setting, type: $type, userView: $userView, groupView: $groupView)';
  }

  FollowDataV2 convertToV2() {
    return FollowDataV2(
        id,
        name,
        avatarUrl,
        type,
        CommunityInfo(),
        [],
        UserView(isFollowing: this.userView?.isFollowing ?? false),
        GroupView(myRoles: this.groupView?.myRoles));
  }

  Map<String, dynamic> toJson() {
    return {
      'associationView': associationView?.toJson(),
      'imageUrl': imageUrl,
      'avatarUrl': avatarUrl,
      'description': description,
      'location': location,
      'privacy': privacy,
      'socialLinks': socialLinks?.map((x) => x?.toJson()).toList(),
      'categories': categories?.map((x) => x?.toJson()).toList(),
      'communityInfo': communityInfo?.toJson(),
      'id': id,
      'memberOfCommunities':
          memberOfCommunities?.map((x) => x?.toJson()).toList(),
      'name': name,
      'ownerProfile': ownerProfile?.toJson(),
      'setting': setting?.toJson(),
      'type': type,
      'userView': userView?.toJson(),
      'groupView': groupView?.toJson(),
      'dynamicLink': dynamicLink,
    };
  }
}

class CommunityDataV2Response {
  CommunityDataV2? communityV2;

  CommunityDataV2Response({
    this.communityV2,
  });
  CommunityDataV2Response.fromJson(Map<String, dynamic> json) {
    communityV2 = (json['communityV2'] != null)
        ? CommunityDataV2.fromJson(json['communityV2'])
        : null;
  }
}
