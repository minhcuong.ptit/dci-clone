import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/data/network/response/tkt_membership.dart';
import 'package:imi/src/utils/enum.dart';

class KtaMembership {
  int? id;
  int? memberSince;
  String? fullName;
  String? ktaNumber;
  String? profilePicture;
  String? bloodType;
  String? qrUrl;

  // address block
  String? address;
  String? provinceName;
  String? districtName;
  String? cityName;
  String? wardName;

  // status block
  String? expiredAt;
  KtaMembershipStatus? status;

  // tkt club
  TktMembership? primaryClub;
  List<ClubRole>? primaryClubRoles;

  KtaMembership({
    this.id,
    this.memberSince,
    this.fullName,
    this.ktaNumber,
    this.profilePicture,
    this.bloodType,
    this.qrUrl,
    this.address,
    this.provinceName,
    this.districtName,
    this.cityName,
    this.wardName,
    this.expiredAt,
    this.status,
    this.primaryClub,
  });

  KtaMembership.fromJson(dynamic json) {
    id = json['id'];
    memberSince = json['memberSince'];
    fullName = json['fullName'];
    ktaNumber = json['ktaNumber'];
    profilePicture = json['profilePicture'];
    bloodType = json['bloodType'];
    qrUrl = json['qrUrl'];

    address = json['address'];
    provinceName = json['provinceName'];
    districtName = json['districtName'];
    cityName = json['cityName'];
    wardName = json['wardName'];

    expiredAt = json['expiredAt'];
    status = KtaMembershipStatus.values
        .firstWhereOrNull((element) => element == json['status']);

    if (json['primaryClub'] != null) {
      primaryClub = TktMembership.fromJson(json['primaryClub']);
    }
    List<ClubRole?>? rawRoles = json['primaryClubRoles']
        ?.map<ClubRole?>((e) =>
            ClubRole.values.firstWhereOrNull((element) => element.name == e))
        ?.toList();
    primaryClubRoles = rawRoles?.whereNotNull().toList() ?? [];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..['id'] = id
    ..['memberSince'] = memberSince
    ..['fullName'] = fullName
    ..['ktaNumber'] = ktaNumber
    ..['profilePicture'] = profilePicture
    ..['bloodType'] = bloodType
    ..['qrUrl'] = qrUrl
    ..['address'] = address
    ..['provinceName'] = provinceName
    ..['districtName'] = districtName
    ..['cityName'] = cityName
    ..['wardName'] = wardName
    ..['expiredAt'] = expiredAt
    ..['status'] = status
    ..['primaryClub'] = primaryClub?.toJson()
    ..['primaryClubRoles'] = primaryClubRoles?.map((e) => e.name).toList();
}
