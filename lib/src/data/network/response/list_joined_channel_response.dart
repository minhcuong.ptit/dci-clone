import 'dart:convert';

class LastMessage {
  int? timeToken;
  Map? message;
  bool? isRead;

  LastMessage({
    this.timeToken,
    this.message,
    this.isRead,
  });

  LastMessage.fromJson(Map<String, dynamic> data) {
    timeToken = data['timeToken']?.toInt();
    message = data['message'] !=null
        ?(json.decode(data['message']) as Map)
        :null;
    isRead = data['isRead'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['timeToken'] = timeToken;
    data['message'] = message;
    data['isRead'] = isRead;
    return data;
  }
}

class JoinedChannelsData {
  String? channelPubnubId;
  String? communityName;
  int? communityId;
  String? communityImage;
  LastMessage? lastMessage;
  int? unReadCount;
  String? userUuid;
  int? lastReadTimeToken;

  JoinedChannelsData({
    this.channelPubnubId,
    this.communityName,
    this.communityImage,
    this.lastMessage,
    this.unReadCount,
    this.userUuid,
    this.lastReadTimeToken,
  });

  JoinedChannelsData.fromJson(Map<String, dynamic> json) {
    channelPubnubId = json['channelPubnubId']?.toString();
    communityName = json['communityName']?.toString();
    communityImage = json['communityImage']?.toString();
    lastMessage = (json['lastMessage'] != null)
        ? LastMessage.fromJson(json['lastMessage'])
        : null;
    unReadCount = json['unReadCount']?.toInt();
    userUuid = json['userUuid']?.toString();
    communityId = json['communityId']?.toInt();
    lastReadTimeToken = json['lastReadTimeToken']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['channelPubnubId'] = channelPubnubId;
    data['communityName'] = communityName;
    data['communityImage'] = communityImage;
    if (lastMessage != null) {
      data['lastMessage'] = lastMessage!.toJson();
    }
    data['unReadCount'] = unReadCount;
    data['userUuid'] = userUuid;
    data['communityId'] = communityId;
    data['lastReadTimeToken'] = lastReadTimeToken;
    return data;
  }
}

class FetchChatChannels {
  String? nextToken;
  List<JoinedChannelsData>? data;

  FetchChatChannels({
    this.nextToken,
    this.data,
  });

  FetchChatChannels.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <JoinedChannelsData>[];
      v.forEach((v) {
        arr0.add(JoinedChannelsData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class ListJoinedChannelResponse {
  FetchChatChannels? fetchJoinedChannels;

  ListJoinedChannelResponse({
    this.fetchJoinedChannels,
  });

  ListJoinedChannelResponse.fromJson(Map<String, dynamic> json) {
    fetchJoinedChannels = (json['fetchJoinedChannels'] != null)
        ? FetchChatChannels.fromJson(json['fetchJoinedChannels'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchJoinedChannels != null) {
      data['fetchJoinedChannels'] = fetchJoinedChannels!.toJson();
    }
    return data;
  }
}
