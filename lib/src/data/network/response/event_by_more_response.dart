
class EventsBuyMore {


  int? id;
  int? productId;
  int? currentFee;
  int? preferentialFee;
  int? quantity;
  String? name;
  String? avatarUrl;
  bool? isHot;

  EventsBuyMore({
    this.id,
    this.productId,
    this.currentFee,
    this.preferentialFee,
    this.quantity,
    this.name,
    this.avatarUrl,
    this.isHot,
  });

  EventsBuyMore.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    productId = json['productId']?.toInt();
    currentFee = json['currentFee']?.toInt();
    preferentialFee = json['preferentialFee']?.toInt();
    quantity = json['quantity']?.toInt();
    name = json['name']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    isHot = json['isHot'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['currentFee'] = currentFee;
    data['preferentialFee'] = preferentialFee;
    data['quantity'] = quantity;
    data['name'] = name;
    data['avatarUrl'] = avatarUrl;
    data['isHot'] = isHot;
    return data;
  }
}

class EventByMoreData {
  List<EventsBuyMore>? fetchEventsBuyMore;

  EventByMoreData({
    this.fetchEventsBuyMore,
  });

  EventByMoreData.fromJson(Map<String, dynamic> json) {
    if (json['fetchEventsBuyMore'] != null) {
      final v = json['fetchEventsBuyMore'];
      final arr0 = <EventsBuyMore>[];
      v.forEach((v) {
        arr0.add(EventsBuyMore.fromJson(v));
      });
      fetchEventsBuyMore = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchEventsBuyMore != null) {
      final v = fetchEventsBuyMore;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['fetchEventsBuyMore'] = arr0;
    }
    return data;
  }
}

class EventByMoreResponse {
  EventByMoreData? data;

  EventByMoreResponse({
    this.data,
  });

  EventByMoreResponse.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? EventByMoreData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
