class TotalCount {
  int? count;

  TotalCount({
    this.count,
  });

  TotalCount.fromJson(Map<String, dynamic> json) {
    count = json['count']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['count'] = count;
    return data;
  }
}

class UnReadTotalData {
  TotalCount? unReadTotalCount;

  UnReadTotalData({
    this.unReadTotalCount,
  });

  UnReadTotalData.fromJson(Map<String, dynamic> json) {
    unReadTotalCount = (json['unReadTotalCount'] != null)
        ? TotalCount.fromJson(json['unReadTotalCount'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (unReadTotalCount != null) {
      data['unReadTotalCount'] = unReadTotalCount!.toJson();
    }
    return data;
  }
}

class UnReadTotal {
  UnReadTotalData? data;

  UnReadTotal({
    this.data,
  });

  UnReadTotal.fromJson(Map<String, dynamic> json) {
    data =
        (json['data'] != null) ? UnReadTotalData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
