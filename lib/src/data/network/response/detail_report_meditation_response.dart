class DurationData {
  int? horizontal;
  int? vertical;

  DurationData({
    this.horizontal,
    this.vertical,
  });

  DurationData.fromJson(Map<String, dynamic> json) {
    horizontal = json['horizontal']?.toInt();
    vertical = json['vertical']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['horizontal'] = horizontal;
    data['vertical'] = vertical;
    return data;
  }
}

class CountData {
  int? horizontal;
  int? vertical;

  CountData({
    this.horizontal,
    this.vertical,
  });

  CountData.fromJson(Map<String, dynamic> json) {
    horizontal = json['horizontal']?.toInt();
    vertical = json['vertical']?.toInt();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['horizontal'] = horizontal;
    data['vertical'] = vertical;
    return data;
  }
}

class FetchDetailMeditationReport {
  List<CountData>? countData;
  List<DurationData>? durationData;

  FetchDetailMeditationReport({
    this.countData,
    this.durationData,
  });

  FetchDetailMeditationReport.fromJson(Map<String, dynamic> json) {
    if (json['countData'] != null) {
      final v = json['countData'];
      final arr0 = <CountData>[];
      v.forEach((v) {
        arr0.add(CountData.fromJson(v));
      });
      countData = arr0;
    }
    if (json['durationData'] != null) {
      final v = json['durationData'];
      final arr0 = <DurationData>[];
      v.forEach((v) {
        arr0.add(DurationData.fromJson(v));
      });
      durationData = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (countData != null) {
      final v = countData;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['countData'] = arr0;
    }
    if (durationData != null) {
      final v = durationData;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['durationData'] = arr0;
    }
    return data;
  }
}

class DetailReportMeditationData {
  FetchDetailMeditationReport? fetchDetailMeditationReport;

  DetailReportMeditationData({
    this.fetchDetailMeditationReport,
  });

  DetailReportMeditationData.fromJson(Map<String, dynamic> json) {
    fetchDetailMeditationReport = (json['fetchDetailMeditationReport'] != null)
        ? FetchDetailMeditationReport.fromJson(
            json['fetchDetailMeditationReport'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchDetailMeditationReport != null) {
      data['fetchDetailMeditationReport'] =
          fetchDetailMeditationReport!.toJson();
    }
    return data;
  }
}

class DetailReportMeditationResponse {
  DetailReportMeditationData? data;

  DetailReportMeditationResponse({
    this.data,
  });

  DetailReportMeditationResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? DetailReportMeditationData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
