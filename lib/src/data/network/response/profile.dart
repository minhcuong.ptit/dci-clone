import 'dart:convert';

Profile profileFromJson(String str) => Profile.fromJson(json.decode(str));

String profileToJson(Profile data) => json.encode(data.toJson());

class Profile {
  Profile({
    this.uuid,
    this.phone,
    this.isEnable,
    this.status,
    this.isIndividual,
    this.isClubPic,
    this.membership,
    this.nikNumber,
    this.fullName,
    this.userUuid,
    this.username,
    this.picture,
    this.pictureLink,
    this.communityRole,
  });

  String? uuid;
  String? phone;
  bool? isEnable;
  String? status;
  bool? isIndividual;
  bool? isClubPic;
  List<String>? membership;
  String? nikNumber;
  String? fullName;
  String? userUuid;
  String? username;
  String? picture;
  String? pictureLink;
  String? communityRole;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        uuid: json["uuid"],
        phone: json["phone"],
        isEnable: json["isEnable"],
        status: json["status"],
        isIndividual: json["isIndividual"],
        isClubPic: json["isClubPic"],
        membership: json["membership"] != null
            ? List<String>.from(json["membership"].map((x) => x))
            : [],
        nikNumber: json["nikNumber"],
        fullName: json["fullName"],
        userUuid: json["userUuid"],
        username: json["username"],
        picture: json["picture"],
        pictureLink: json["pictureLink"],
        communityRole: json["communityRole"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid,
        "phone": phone,
        "isEnable": isEnable,
        "status": status,
        "isIndividual": isIndividual,
        "isClubPic": isClubPic,
        "membership": (membership?.isEmpty ?? true)
            ? []
            : List<String>.from(membership!.map((x) => x)),
        "nikNumber": nikNumber,
        "fullName": fullName,
        "userUuid": userUuid,
        "username": username,
        "picture": picture,
        "pictureLink": pictureLink,
        "communityRole": communityRole,
      };
}
