import 'community_data.dart';
import 'my_community_response.dart';
import 'user_data.dart';

/// myCommunity : {"avatarUrl":"String","followerNo":1,"id":0,"imageUrl":"String","memberNo":0,"name":"String","ownerId":0,"type":"String","clubInfo":{"bankHolderName":"String","bankName":"String","bankNumber":"String","clubName":"String","id":0,"personInCharge":"String","personInChargeName":"String"},"individualInfo":{"id":3,"name":"Mobile user","phone":"0982211222","email":"investidea@yopmail.com","address":"86 Dich vong hau","birthPlace":"Hanoi","postalCode":"","hobby":"Music","bloodType":"A","userUuid":"dfd46148-90df-4b06-a9b8-3996245b0c00"}}

class MyProfileResponse {
  MyProfileResponse({
    CommunityData? myCommunity,
    UserData? me,
  }) {
    _myCommunity = myCommunity;
    _me = me;
  }

  MyProfileResponse.fromJson(dynamic json) {
    _myCommunity = json['myCommunity'] != null
        ? CommunityData.fromJson(json['myCommunity'])
        : null;
    _me = json['me'] != null ? UserData.fromJson(json['me']) : null;
  }
  UserData? _me;

  CommunityData? _myCommunity;

  UserData? get me => _me;

  CommunityData? get myCommunity => _myCommunity;
}
