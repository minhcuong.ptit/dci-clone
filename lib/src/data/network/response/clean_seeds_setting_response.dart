import 'clean_seed_history_response.dart';

class CleanSeedsSettingResponse {
  CleanSeedSettings? cleanSeedSettings;

  CleanSeedsSettingResponse({this.cleanSeedSettings});

  CleanSeedsSettingResponse.fromJson(Map<String, dynamic> json) {
    cleanSeedSettings = json['cleanSeedSettings'] != null
        ? new CleanSeedSettings.fromJson(json['cleanSeedSettings'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.cleanSeedSettings != null) {
      data['cleanSeedSettings'] = this.cleanSeedSettings!.toJson();
    }
    return data;
  }
}

class CleanSeedSettings {
  String? nextToken;
  List<Step1>? data;

  CleanSeedSettings({this.nextToken, this.data});

  CleanSeedSettings.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken'];
    if (json['data'] != null) {
      data = <Step1>[];
      json['data'].forEach((v) {
        data!.add(new Step1.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nextToken'] = this.nextToken;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
