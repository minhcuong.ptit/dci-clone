import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/utils/enum.dart';

import 'package.dart';

class UserPackage {
  Package? packageInfo;
  PackageStatus? packageStatus;
  int? expiredDate;

  UserPackage({
    this.packageInfo,
    this.packageStatus,
    this.expiredDate,
  });

  UserPackage.fromJson(dynamic json) {
    packageInfo = Package.fromJson(json['packageInfo']);
    packageStatus = PackageStatus.values
        .firstWhereOrNull((e) => e.name == json['packageStatus']);
    expiredDate = json['expiredDate'];
  }

  Map<String, dynamic> toJson() => <String, dynamic>{}
    ..["packageInfo"] = packageInfo?.toJson()
    ..["packageStatus"] = packageStatus?.name
    ..["expiredDate"] = expiredDate;
}
