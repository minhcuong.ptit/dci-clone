import 'package:collection/src/iterable_extensions.dart';
import 'package:imi/src/utils/enum.dart';

/// id : 3
/// kisId : "1"
/// kisName : "aaaa"
/// kisType : "MOTOR"
/// provincePrice : 1.1111111E7

class KisProvince {
  KisProvince({
    this.id,
    this.kisId,
    this.status,
    this.kisName,
    this.kisType,
    this.provincePrice,
  });

  KisProvince.fromJson(dynamic json) {
    id = json['id'];
    kisId = json['kisId'];
    status = KisStatus.values
        .firstWhereOrNull((element) => element.name == json['status']);
    kisName = json['kisName'];
    kisType = KisType.values
        .firstWhereOrNull((element) => element.name == json['kisType']);
    provincePrice = json['provincePrice'];
  }
  int? id;
  String? kisId;
  KisStatus? status;
  String? kisName;
  KisType? kisType;
  double? provincePrice;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['kisId'] = kisId;
    map['status'] = status;
    map['kisName'] = kisName;
    map['kisType'] = kisType?.name;
    map['provincePrice'] = provincePrice;
    return map;
  }
}
