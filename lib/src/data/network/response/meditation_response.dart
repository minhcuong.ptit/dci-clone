class FetchMeditationsData {
  int? id;
  String? title;
  String? imageUrl;
  bool? isLocked;

  FetchMeditationsData({
    this.id,
    this.title,
    this.imageUrl,
    this.isLocked,
  });

  FetchMeditationsData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    title = json['title']?.toString();
    imageUrl = json['imageUrl']?.toString();
    isLocked = json['isLocked'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['imageUrl'] = imageUrl;
    data['isLocked'] = isLocked;
    return data;
  }
}

class FetchMeditation {
  String? nextToken;
  List<FetchMeditationsData>? data;

  FetchMeditation({
    this.nextToken,
    this.data,
  });

  FetchMeditation.fromJson(Map<String, dynamic> json) {
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchMeditationsData>[];
      v.forEach((v) {
        arr0.add(FetchMeditationsData.fromJson(v));
      });
      this.data = arr0;
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class MeditationResponseData {
  FetchMeditation? fetchMeditations;

  MeditationResponseData({
    this.fetchMeditations,
  });

  MeditationResponseData.fromJson(Map<String, dynamic> json) {
    fetchMeditations = (json['fetchMeditations'] != null)
        ? FetchMeditation.fromJson(json['fetchMeditations'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchMeditations != null) {
      data['fetchMeditations'] = fetchMeditations!.toJson();
    }
    return data;
  }
}

class MeditationResponse {
  MeditationResponseData? data;

  MeditationResponse({
    this.data,
  });

  MeditationResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null)
        ? MeditationResponseData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
