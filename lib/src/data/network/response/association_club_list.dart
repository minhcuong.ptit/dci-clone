import 'package:imi/src/data/network/response/association_club.dart';

class AssociationClubList {
  AssociationClubList({
    this.total,
    this.nextToken,
    this.data,
  });

  AssociationClubList.fromJson(dynamic json) {
    total = json['total'];
    nextToken = json['nextToken'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        AssociationClub element = AssociationClub.fromJson(v);
        data?.add(element);
      });
    }
  }
  int? total;
  String? nextToken;
  List<AssociationClub>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total'] = total;
    map['nextToken'] = nextToken;
    map['data'] = data?.map((e) => e.toJson()).toList();
    return map;
  }
}
