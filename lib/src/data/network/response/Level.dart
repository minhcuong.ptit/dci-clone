
class Level {
  Level({
    this.communityId,
    this.level,
    this.year,
    this.levelName,
    this.levelStatus,
  });

  int? communityId;
  int? level;
  int? year;
  String? levelName;
  String? levelStatus;

  factory Level.fromJson(Map<String, dynamic> json) => Level(
    communityId: json["communityId"],
    level: json["level"],
    year: json["year"],
    levelName: json["levelName"],
    levelStatus: json["levelStatus"],
  );

  Map<String, dynamic> toJson() => {
    "communityId": communityId,
    "level": level,
    "year": year,
    "levelName": levelName,
    "levelStatus": levelStatus,
  };
}