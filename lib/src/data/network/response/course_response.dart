class FetchCoursesData {

  int? id;
  int? productId;
  String? name;
  String? avatarUrl;
  int? priority;
  int? preferentialFee;
  String? code;
  int? currentFee;
  String? description;
  bool? isHot;
  int? startDate;
  String? status;
  String? studyForm;
  String? studyTime;
  String? teacher;

  FetchCoursesData({
    this.id,
    this.productId,
    this.name,
    this.avatarUrl,
    this.priority,
    this.preferentialFee,
    this.code,
    this.currentFee,
    this.description,
    this.isHot,
    this.startDate,
    this.status,
    this.studyForm,
    this.studyTime,
    this.teacher,
  });
  FetchCoursesData.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    productId = json['productId']?.toInt();
    name = json['name']?.toString();
    avatarUrl = json['avatarUrl']?.toString();
    priority = json['priority']?.toInt();
    preferentialFee = json['preferentialFee']?.toInt();
    code = json['code']?.toString();
    currentFee = json['currentFee']?.toInt();
    description = json['description']?.toString();
    isHot = json['isHot'];
    startDate = json['startDate']?.toInt();
    status = json['status']?.toString();
    studyForm = json['studyForm']?.toString();
    studyTime = json['studyTime']?.toString();
    teacher = json['teacher']?.toString();
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['productId'] = productId;
    data['name'] = name;
    data['avatarUrl'] = avatarUrl;
    data['priority'] = priority;
    data['preferentialFee'] = preferentialFee;
    data['code'] = code;
    data['currentFee'] = currentFee;
    data['description'] = description;
    data['isHot'] = isHot;
    data['startDate'] = startDate;
    data['status'] = status;
    data['studyForm'] = studyForm;
    data['studyTime'] = studyTime;
    data['teacher'] = teacher;
    return data;
  }
}

class FetchCourse {

  int? total;
  String? nextToken;
  List<FetchCoursesData>? data;

  FetchCourse({
    this.total,
    this.nextToken,
    this.data,
  });
  FetchCourse.fromJson(Map<String, dynamic> json) {
    total = json['total']?.toInt();
    nextToken = json['nextToken']?.toString();
    if (json['data'] != null) {
      final v = json['data'];
      final arr0 = <FetchCoursesData>[];
      v.forEach((v) {
        arr0.add(FetchCoursesData.fromJson(v));
      });
      this.data = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['total'] = total;
    data['nextToken'] = nextToken;
    if (this.data != null) {
      final v = this.data;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v.toJson());
      });
      data['data'] = arr0;
    }
    return data;
  }
}

class CourseResponseData {

  FetchCourse? fetchCourses;

  CourseResponseData({
    this.fetchCourses,
  });
  CourseResponseData.fromJson(Map<String, dynamic> json) {
    fetchCourses = (json['fetchCourses'] != null) ? FetchCourse.fromJson(json['fetchCourses']) : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (fetchCourses != null) {
      data['fetchCourses'] = fetchCourses!.toJson();
    }
    return data;
  }
}

class CourseResponse {

  CourseResponseData? data;

  CourseResponse({
    this.data,
  });
  CourseResponse.fromJson(Map<String, dynamic> json) {
    data = (json['data'] != null) ? CourseResponseData.fromJson(json['data']) : null;
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
