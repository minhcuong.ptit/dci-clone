import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/logger.dart';

import 'csv_loader/csv_asset_loader.dart';

class Localization {
  static const List<Locale> supportedLanguage = [
    Locale(
      Const.LOCALE_EN,
    ),
    Locale(Const.LOCALE_VN, "VN")
  ];
  static const String languageFilePath = 'lib/res/translations/langs.csv';

  static Widget getLocalizationWidget({app}) {
    return EasyLocalization(
        supportedLocales: supportedLanguage,
        path: languageFilePath,
        fallbackLocale: Locale(appPreferences.appLanguage),
        startLocale: Locale(appPreferences.appLanguage),
        useOnlyLangCode: true,
        saveLocale: false,
        assetLoader: CsvAssetLoader(),
        child: app);
  }

  static changeLanguage(BuildContext context) {
    String? currentLanguage = appPreferences.appLanguage;
    if (currentLanguage != Const.LOCALE_EN) {
      context.setLocale(supportedLanguage[0]);
      appPreferences.saveAppLanguage(Const.LOCALE_EN);
    } else {
      context.setLocale(supportedLanguage[1]);
      appPreferences.saveAppLanguage(Const.LOCALE_VN);
    }
  }

  static loadLanguage(BuildContext context) {
    logger.i(appPreferences.appLanguage);
    context.setLocale(Locale(appPreferences.appLanguage));
  }
}
