import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:imi/main.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/page/auth_user/login/login.dart';
import 'package:imi/src/page/chat/chat/chat_page.dart';
import 'package:imi/src/page/chat/chat/cubit/chat_cubit.dart';
import 'package:imi/src/page/club_info/club_information/club_information_page.dart';
import 'package:imi/src/page/comment/comment_page.dart';
import 'package:imi/src/page/dci_challenge_details/dci_challenge_page.dart';
import 'package:imi/src/page/dci_notification/detail_dci_notification_page.dart';
import 'package:imi/src/page/detail_order/detail_order_page.dart';
import 'package:imi/src/page/favorite/favorite.dart';
import 'package:imi/src/page/history_point/history_point_page.dart';
import 'package:imi/src/page/home_sow/home_sow_page.dart';
import 'package:imi/src/page/home_zen/home_zen_page.dart';
import 'package:imi/src/page/manage_reported_posts/manage_reported_posts_page.dart';
import 'package:imi/src/page/pin_code/change_password/change_password_cubit.dart';
import 'package:imi/src/page/post/post_page.dart';
import 'package:imi/src/page/start_practice/start_practice_page.dart';
import 'package:imi/src/page/welcome/welcome_page.dart';
import 'package:imi/src/utils/const.dart';
import 'package:imi/src/utils/dci_event_handle.dart';
import 'package:imi/src/utils/enum.dart';
import 'package:imi/src/utils/logger.dart';
import 'package:imi/src/utils/navigation_utils.dart';
import 'package:imi/src/utils/utils.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'localization/localization.dart';
import 'page/authentication/authentication.dart';
import 'page/main/main_page.dart';
import 'page/manage_members/manage_members.dart';
import 'page/splash/splash_page.dart';
import 'utils/navigation_utils.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

class CustomEn extends timeago.EnMessages {
  @override
  String suffixAgo() => '';
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  late AuthenticationCubit _authenticationCubit;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    _authenticationCubit = GetIt.I<AuthenticationCubit>();
    Future.delayed(Duration(seconds: 3), () {
      Localization.loadLanguage(context);
      _authenticationCubit.openApp();
    });
    timeago.setDefaultLocale(Const.LOCALE_EN);
    // This will override `en` locale with the custom messages
    timeago.setLocaleMessages(Const.LOCALE_EN, CustomEn());

    _initNotification();
    _initDynamicLinks();
  }

  void _initNotification() async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

    _firebaseMessaging
        .requestPermission(
          alert: true,
          announcement: false,
          badge: true,
          carPlay: false,
          criticalAlert: false,
          provisional: false,
          sound: true,
        )
        .then((value) =>
            logger.d('User granted permission: ${value.authorizationStatus}'));
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );

    // await flutterLocalNotificationsPlugin
    //     .resolvePlatformSpecificImplementation<
    //         IOSFlutterLocalNotificationsPlugin>()
    //     ?.requestPermissions(
    //       alert: true,
    //       badge: true,
    //       sound: true,
    //     );

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/launcher_icon2');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            onDidReceiveLocalNotification: _onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) {
      _redirectNotification(jsonDecode(payload ?? ""));
    });

    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description:
          'This channel is used for important notifications.', // description
      importance: Importance.max,
    );
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    final appLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
    if (appLaunchDetails?.didNotificationLaunchApp ?? false) {
      _redirectNotification(jsonDecode(appLaunchDetails?.payload ?? ""));
    }

    // on foreground
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      logger.d('on foreground', message.data);
      const AndroidNotificationDetails androidPlatformChannelSpecifics =
          AndroidNotificationDetails(
              'high_importance_channel', 'High Importance Notifications',
              channelDescription: 'your channel description',
              importance: Importance.max,
              priority: Priority.high,
              ticker: 'ticker',
          );
      const NotificationDetails platformChannelSpecifics =
          NotificationDetails(android: androidPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
          10291,
          message.notification?.title,
          message.notification?.body,
          platformChannelSpecifics,
          payload: jsonEncode(message.data));
    });

    // on background
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      logger.d('on background', message.data);
      _redirectNotification(message.data);
    });

    // launch app from a terminated state via a notification
    _firebaseMessaging.getInitialMessage().then((message) {
      logger.d('on terminated', message?.data);
      if (message?.data != null) {
        _redirectNotification(message!.data);
      }
    });
  }

  void _onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) {
    _redirectNotification(jsonDecode(payload ?? ""));
  }

  // redirect khi bấm vào notification của hệ thống
  void _redirectNotification(dynamic data) {
    Future.delayed(const Duration(milliseconds: 500))
        .whenComplete(() => DCIEventHandler().send(NewNotificationEvent()));
    if (data is! Map) return;
    if (!appPreferences.isLoggedIn) return;
    if (data['redirectPage'] == RedirectPage.GROUP_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => ViewClubPage(
                communityId: int.parse(data['communityId']),
              )));
    } else if (data['redirectPage'] == RedirectPage.REQUEST_JOIN_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) =>
              ViewClubPage(communityId: int.parse(data['communityId']))));
    } else if (data['redirectPage'] == RedirectPage.REPORT_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => ManageReportPostsPage(
                communityId: int.parse(data['communityId'] ?? 0),
              )));
    } else if (data['redirectPage'] == RedirectPage.CHALLENGE_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => DCIChallengePage(
                challengeVersionId: int.parse(data['challengeVersionId'] ?? 0),
                showPopup: false,
              )));
    } else if (data['redirectPage'] == RedirectPage.PLAN_SEED_PAGE.name) {
      alice
          .getNavigatorKey()
          ?.currentState
          ?.push(MaterialPageRoute(builder: (context) => HomeSowPage()));
    } else if (data['redirectPage'] ==
        RedirectPage.MEDITATION_COFFEE_PAGE.name) {
      alice
          .getNavigatorKey()
          ?.currentState
          ?.push(MaterialPageRoute(builder: (context) => HomeZenPage()));
    } else if (data['redirectPage'] == RedirectPage.MEDITATION_PAGE.name) {
      alice
          .getNavigatorKey()
          ?.currentState
          ?.push(MaterialPageRoute(builder: (context) => HomeZenPage()));
    } else if (data['redirectPage'] ==
        RedirectPage.PRIVATE_NOTIFICATION_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => DciNotificationPage(
                notificationPrivateId:
                    int.parse(data['notificationPrivateId'] ?? 0),
                // showPopup: false,
              )));
    } else if (data['redirectPage'] == RedirectPage.ORDER_HISTORY_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => DetailOrderPage(
                orderHistoryId: int.parse(data['orderHistoryId'] ?? 0),
            paymentType: data['paymentType'],
              )));
    } else if (data['redirectPage'] == RedirectPage.ORDER_WAS_CANCELED.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => DetailOrderPage(
                orderHistoryId: int.parse(data['orderHistoryId'] ?? 0),
            paymentType: data['paymentType'],
              )));
    } else if (data['redirectPage'] == RedirectPage.CHAT_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => ChatPage(
                chatType: data['channelType'] == "PRIVATE"
                    ? ChatType.private
                    : data['channelType'] == "GROUP"
                        ? ChatType.group
                        : data['channelType'] == "CUSTOMER_SUPPORT"
                            ? ChatType.support
                            : ChatType.private,
                channelId: data['channelId'],
                communityId: int.parse(data['communityId'] ?? 0),
                userUID: data['receiverUuid'],
                channelName: data['channelName'],
              )));
    } else if (data['redirectPage'] == RedirectPage.COMMUNITY_POST_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.pushReplacement(MaterialPageRoute(
          builder: (context) => CommentPage(
                postId: int.parse(data['postId'] ?? 0),
                onCommentSuccess: (countComment) {},
                onLikeSuccess: (like, likeNumber) {},
                onRemovePostSuccess: () {},
                onBookmarkSuccess: (bool bookmark) {},
                isNotification: true,
              )));
    } else if (data['redirectPage'] ==
        RedirectPage.POINT_TRANSACTION_IN_PAGE.name) {
      alice.getNavigatorKey()?.currentState?.pushReplacement(MaterialPageRoute(
          builder: (context) => HistoryPointPage(
                isNotification: true,
              )));
    }
  }

  void _initDynamicLinks() async {
    // Get any initial links from a terminated state
    final PendingDynamicLinkData? initialLink =
        await FirebaseDynamicLinks.instance.getInitialLink();
    logger.d(initialLink?.link);
    if (initialLink?.link.path != null) {
      Future.delayed(Duration(seconds: 2)).then((_) {
        _redirectFromDynamicLink(initialLink!.link.path);
      });
    }

    // background or foreground
    FirebaseDynamicLinks.instance.onLink.listen((dynamicLinkData) {
      _redirectFromDynamicLink(dynamicLinkData.link.path);
    }).onError((error) {
      // Handle errors
      logger.e(error);
    });
  }

  void _redirectFromDynamicLink(String path) {
    if (!appPreferences.isLoggedIn) return;
    final data = path.split("/");
    if (data[1].contains("GROUP")) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => ViewClubPage(communityId: int.parse(data[3]))));
    } else if (data[1].contains("post")) {
      alice.getNavigatorKey()?.currentState?.push(MaterialPageRoute(
          builder: (context) => CommentPage(
                postId: int.parse(data[2]),
                onBookmarkSuccess: (_) {},
                onCommentSuccess: (_) {},
                onLikeSuccess: (_, __) {},
                onRemovePostSuccess: () {},
              )));
    }
  }

  @override
  Widget build(BuildContext context) {
    Utils.portraitModeOnly();
    return BlocProvider<AuthenticationCubit>(
      create: (context) => _authenticationCubit,
      child: ScreenUtilInit(
          designSize: Size(360, 800),
          builder: (_, __) => RefreshConfiguration(
                headerBuilder: () => WaterDropMaterialHeader(),
                // Configure the default header indicator. If you have the same header indicator for each page, you need to set this
                footerBuilder: () => ClassicFooter(
                  loadStyle: LoadStyle.HideAlways,
                  noDataText: R.string.no_data_to_show.tr(),
                ),
                child: MaterialApp(
                  navigatorKey: alice.getNavigatorKey(),
                  title: "DCI",
                  debugShowCheckedModeBanner: false,
                  theme: ThemeData(
                      primaryColor: R.color.primaryColor,
                      accentColor: R.color.primaryColor,
                      buttonColor: R.color.primaryColor,
                      fontFamily: R.font.helvetica,
                      colorScheme: ColorScheme.fromSwatch(
                        primarySwatch: Colors.blue,
                      ).copyWith(),
                      brightness: Brightness.light,
                      backgroundColor: R.color.white,
                      scaffoldBackgroundColor: R.color.white,
                      dialogBackgroundColor: R.color.white,
                      textTheme: TextTheme(
                        headline1: TextStyle(
                            color: R.color.black,
                            fontSize: 28.sp,
                            fontWeight: FontWeight.w700,
                            height: 36 / 28),
                        headline2: TextStyle(
                            color: R.color.black,
                            fontSize: 24.sp,
                            fontWeight: FontWeight.bold,
                            height: 34 / 24),
                        headline3: TextStyle(
                            color: R.color.black,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold,
                            height: 32 / 20),
                        headline4: TextStyle(
                            color: R.color.black,
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w600,
                            height: 24 / 14),
                        headline5: TextStyle(
                            color: R.color.black,
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w300,
                            height: 24 / 14),
                        subtitle1: TextStyle(
                            color: R.color.black,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.w600,
                            height: 24 / 18),
                        subtitle2: TextStyle(
                            color: R.color.black,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            height: 24 / 16),
                        bodyText1: TextStyle(
                            color: R.color.black,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.w400,
                            height: 24 / 13),
                        bodyText2: TextStyle(
                            color: R.color.black,
                            fontSize: 11.sp,
                            fontWeight: FontWeight.w400,
                            height: 20 / 11),
                      )),
                  localizationsDelegates: context.localizationDelegates,
                  supportedLocales: context.supportedLocales,
                  locale: context.locale,
                  navigatorObservers: [routeObserver],
                  home: WillPopScope(
                    onWillPop: () async {
                      if (_authenticationCubit.state
                          is AuthenticationUnauthenticated) {
                        _authenticationCubit.openWelcome();
                        return false;
                      }
                      return true;
                    },
                    child:
                        BlocBuilder<AuthenticationCubit, AuthenticationState>(
                      builder: (context, state) {
                        if (state is AuthenticationUninitialized)
                          return SplashPage();
                        else if (state is AuthenticationChooseCategory)
                          return FavoritePage(
                            onRefresh: () {},
                            checkPage: false,
                          );
                        else if (state is AuthenticationWelcome)
                          return WelcomePage();
                        else if (state is AuthenticationUnauthenticated)
                          return LoginPage();
                        else
                          return MainPage();
                      },
                    ),
                  ),
                ),
              )),
    );
  }
}
