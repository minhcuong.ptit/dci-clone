import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart' as flutterToast;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imi/res/R.dart';
import 'package:imi/src/utils/custom_theme.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path/path.dart';
import 'package:styled_text/styled_text.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

import '../widgets/button_widget.dart';
import 'const.dart';
import 'date_util.dart';
import 'enum.dart';
import 'logger.dart';
import 'navigation_utils.dart';
import 'package:path/path.dart' as p;
import 'package:mime/mime.dart';

class Utils {
  /// blocks rotation; sets orientation to: portrait
  static void portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  static void enableLandscape() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  static void enableRotation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  static Future<bool> checkConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      logger.d('Internet not connect');
    }
    return false;
  }

  static void showSnackBar(BuildContext context, String? text) {
    if (Utils.isEmpty(text)) return;
    final snackBar = SnackBar(
      content: Text(text ?? ""),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static void onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  static void showErrorSnackBar(BuildContext context, String? text) {
    onWidgetDidBuild(() => showSnackBar(context, text));
  }

  static String getTypeUrlLauncher(String url, int type) {
    switch (type) {
      case Const.LAUNCH_TYPE_WEB:
        return url;
      case Const.LAUNCH_TYPE_EMAIL:
        return "mailto:$url";
      case Const.LAUNCH_TYPE_PHONE:
        return "tel:$url";
      case Const.LAUNCH_TYPE_SMS:
        return "sms:$url";
    }
    return url;
  }

  static Future launchURL(String url) async {
    url = url.replaceAll(" ", "");
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      logger.d('Đã có lỗi xảy ra');
    }
  }

  static bool isEmpty(Object? text) {
    if (text is String) return text.isEmpty;
    if (text is List) return text.isEmpty;
    return text == null;
  }

  static bool isEmptyArray(List? list) {
    return list == null || list.isEmpty;
  }

  static bool isInteger(num value) =>
      value is int || value == value.roundToDouble();

  static Color parseStringToColor(String? color, {Color? defaultColor}) {
    if (Utils.isEmpty(color))
      return defaultColor ?? Colors.black;
    else
      return Color(int.parse('0xff' + color!.trim().substring(1)));
  }

  static Future showDialogTextTwoButton(
      {required BuildContext context,
      required String title,
      required String contentText,
      required String submitText,
      required VoidCallback submitCallback,
      bool dismissible: false}) {
    return showDialog(
        barrierDismissible: dismissible,
        context: context,
        builder: (context) {
          return AlertDialog(
              title: !Utils.isEmpty(title) ? Text(title) : null,
              content: Text(contentText),
              actions: [
                // FlatButton(
                //   onPressed: () => popDialog(context),
                //   child: Text(S.of(context).close),
                // ),
                Visibility(
                  visible: !Utils.isEmpty(submitText),
                  child: TextButton(
                    onPressed: () {
                      NavigationUtils.popDialog(context);
                      submitCallback();
                    },
                    child: Text(submitText),
                  ),
                )
              ]);
        });
  }

  static Future showDialogTwoButton(
      {required BuildContext context,
      required String? title,
      required Widget contentWidget,
      required String submitText,
      required VoidCallback submitCallback,
      bool dismissible: false}) {
    return showDialog(
        barrierDismissible: dismissible,
        context: context,
        builder: (context) {
          return AlertDialog(
              title: title != null ? Text(title) : null,
              content: contentWidget,
              actions: [
                // FlatButton(
                //   onPressed: () => popDialog(context),
                //  // child: Text(S.of(context).close),
                // ),
                Visibility(
                  visible: !Utils.isEmpty(submitText),
                  child: TextButton(
                    onPressed: () {
                      NavigationUtils.popDialog(context);
                      submitCallback();
                    },
                    child: Text(submitText),
                  ),
                )
              ]);
        });
  }

  static void showDialogTwoButtonAfterLayout(
      {required BuildContext context,
      required String? title,
      required Widget contentWidget,
      required List<Widget> actions}) async {
    onWidgetDidBuild(() => showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
              title: title != null ? Text(title) : null,
              content: contentWidget,
              actions: actions);
        }));
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  static int? convertPriceToNumber(String? price) {
    if (price == null) return null;
    String newPrice = price
        .replaceAll(" ", "")
        .replaceAll("đ", "")
        .replaceAll("₫", "")
        .replaceAll("\$", "")
        .replaceAll(",", "");
    try {
      return int.parse(newPrice);
    } catch (e) {
      logger.e(e.toString());
      return null;
    }
  }

  static String? formatMoney(dynamic amount) {
    if (amount == null) return null;
    if (amount is String) {
      amount = double.parse(amount);
    }
    return NumberFormat("#,##0").format(amount);
  }

  static String formatPoint(int point) {
    if (point == 1) {
      return R.string.number_pointsone.tr(args: ["$point"]);
    }
    return R.string.number_pointsmany.tr(args: ["$point"]);
  }

  static void showCommonBottomSheet(
      {required BuildContext context,
      required String title,
      required String formattedDetails,
      required List<ButtonWidget> buttons}) {
    showBarModalBottomSheet(
        context: context,
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(24),
            child: IntrinsicHeight(
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 16.sp, height: 24 / 16.sp),
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    StyledText(
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 11.sp, height: 20 / 11.sp),
                      text: formattedDetails,
                      tags: {
                        'bold': StyledTextTag(
                            style: Theme.of(context)
                                .textTheme
                                .bold700
                                .copyWith(fontSize: 11.sp, height: 20 / 11.sp))
                      },
                    ),
                    SizedBox(
                      height: 16.h,
                    ),
                    ...List.generate(buttons.length * 2 - 1, (index) {
                      if (index % 2 == 0) {
                        return buttons[index ~/ 2];
                      }
                      return SizedBox(
                        height: 16.h,
                      );
                    })
                  ],
                ),
              ),
            ),
          );
        });
  }

  static Future showConfirmationBottomSheet(
      {required BuildContext context,
      required String title,
      bool showCancelButton = true,
      String? confirmText,
      String? cancelText,
      required String formatedDetails,
      String? confirmationDescription,
      required Function() onConfirm,
      Function? onCancel}) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(24),
            child: IntrinsicHeight(
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: Theme.of(context)
                          .textTheme
                          .bold700
                          .copyWith(fontSize: 16.sp),
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    StyledText(
                      style: Theme.of(context)
                          .textTheme
                          .regular400
                          .copyWith(fontSize: 11.sp),
                      text: formatedDetails,
                      tags: {
                        'bold': StyledTextTag(
                            style: Theme.of(context)
                                .textTheme
                                .bold700
                                .copyWith(fontSize: 11.sp))
                      },
                    ),
                    SizedBox(
                      height: 16.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Visibility(
                          visible: showCancelButton,
                          child: ButtonWidget(
                            title: cancelText ?? R.string.cancel.tr(),
                            onPressed: () {
                              NavigationUtils.pop(context);
                              onCancel?.call();
                            },
                            borderColor: R.color.black,
                            textColor: R.color.black,
                            backgroundColor: R.color.white,
                            textSize: 12.sp,
                            height: 32.h,
                            width:
                                (MediaQuery.of(context).size.width - 48 - 16) /
                                    2,
                          ),
                        ),
                        Spacer(),
                        ButtonWidget(
                          title:
                              confirmationDescription ?? R.string.confirm.tr(),
                          onPressed: () {
                            NavigationUtils.pop(context);
                            onConfirm();
                          },
                          textSize: 12.sp,
                          backgroundColor: R.color.lightBlue,
                          height: 32.h,
                          width: showCancelButton
                              ? (MediaQuery.of(context).size.width - 48 - 16) /
                                  2
                              : (MediaQuery.of(context).size.width - 48),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  static void showToast(BuildContext context, String? text,
      {ToastType? type = ToastType.SUCCESS,
      bool? isPrefixIcon = false,
      double fromTop = 0,
      bool setTime = false}) {
    Color mainColor = R.color.blue;
    Color textColor = R.color.white;
    IconData iconData = CupertinoIcons.info;
    if (type == ToastType.WARNING) {
      mainColor = R.color.orangeLight;
    } else if (type == ToastType.ERROR) {
      mainColor = R.color.red;
    }
    onWidgetDidBuild(() {
      FToast fToast = FToast();
      fToast.init(context);
      Widget toast = Column(
        children: [
          SizedBox(
            height: fromTop,
          ),
          Container(
            width: ScreenUtil().screenWidth - 48.h,
            padding: EdgeInsets.symmetric(horizontal: 16.h, vertical: 6.h),
            decoration: BoxDecoration(
                borderRadius: isPrefixIcon == true
                    ? BorderRadius.zero
                    : BorderRadius.circular(16),
                border: Border.all(color: mainColor, width: 1),
                boxShadow: [
                  BoxShadow(
                      color: R.color.gray.withOpacity(0.2),
                      offset: const Offset(0, 4),
                      blurRadius: 12,
                      spreadRadius: 0)
                ],
                color: mainColor),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                isPrefixIcon == true
                    ? Container(
                        margin: EdgeInsets.only(right: 10.w),
                        child: Icon(iconData, size: 12.h, color: mainColor))
                    : Container(),
                Expanded(
                  child: Text(text ?? "",
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall
                          ?.copyWith(color: textColor)),
                ),
                SizedBox(
                  width: 8.w,
                ),
                GestureDetector(
                  onTap: () => fToast.removeCustomToast(),
                  child: Icon(
                    CupertinoIcons.xmark,
                    size: 16.h,
                    color: textColor,
                  ),
                )
              ],
            ),
          )
        ],
      );

      fToast.showToast(
        child: toast,
        gravity: ToastGravity.TOP,
        positionedToastBuilder: (context, child) {
          return Positioned(
            child: child,
            top: MediaQuery.of(context).padding.top + 5.h,
            left: 0,
            right: 0,
          );
        },
        toastDuration:
            setTime == true ? Duration(seconds: 5) : Duration(seconds: 3),
      );
    });
  }

  static void showToastDefault(String text, {bool isLong = false}) {
    if (!isEmpty(text))
      // toast(text, duration: isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
      flutterToast.Fluttertoast.showToast(
          msg: text,
          toastLength: isLong
              ? flutterToast.Toast.LENGTH_LONG
              : flutterToast.Toast.LENGTH_SHORT);
  }

  static void copyText(BuildContext context, String text) {
    Clipboard.setData(ClipboardData(text: text));
    showToast(context, "Copied to Clipboard");
  }

  static void hideKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  // static Future updateBadge(int count) async {
  //   if (await FlutterAppBadger.isAppBadgeSupported()) {
  //     FlutterAppBadger.updateBadgeCount(count);
  //   }
  // }

  static navigateNextFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  static String getFileNameFromFile(File file) {
    return basename(file.path);
  }

  static String getFileName(String path) {
    return basenameWithoutExtension(path);
  }

  static String? base64Image(File? file) {
    if (file == null) return null;
    List<int> imageBytes = file.readAsBytesSync();
    return base64Encode(imageBytes);
  }

  static String timeAgo(String? time) {
    if (Utils.isEmpty(time)) return "";
    return timeago.format(DateTime.parse(time!), locale: Const.LOCALE_EN);
  }

  static String timeAgoByDate(DateTime? dateTime,
      {String locale = Const.LOCALE_EN}) {
    if (Utils.isEmpty(dateTime)) return "";
    timeago.setLocaleMessages(
      locale,
      TimeagoLookupMessagesWithoutPrefix(),
    );
    return timeago.format(dateTime!.subtract(dateTime.timeZoneOffset),
        locale: locale);
  }

  static String timeAgoForComment(DateTime? dateTime) {
    if (Utils.isEmpty(dateTime)) return "";
    timeago.setDefaultLocale(Const.LOCALE_EN);
    return timeago.format(dateTime!, locale: Const.LOCALE_EN);
  }

  static Future<Map<String, dynamic>> parseJson(String fileName) async {
    return jsonDecode(await rootBundle.loadString("assets/$fileName"));
  }

  static String getPhoneNumber(String? phoneNumber, String countryCode) {
    String? phone = phoneNumber?.trim();
    if (phone?.startsWith("0") == true) {
      phone = phone?.substring(1, phone.length);
    }
    phone = "${countryCode}$phone";
    phone = phone.replaceAll("+", "");
    return phone;
  }

  static String getAddress(
      {String? address,
      String? ward,
      String? district,
      String? province,
      String? city,
      String? country}) {
    String result = "";
    if (!Utils.isEmpty(address)) {
      result += address!;
    }
    if (!Utils.isEmpty(ward)) {
      if (!Utils.isEmpty(result)) result += ", ";
      result += ward!;
    }
    if (!Utils.isEmpty(district)) {
      if (!Utils.isEmpty(result)) result += ", ";
      result += district!;
    }
    if (!Utils.isEmpty(city)) {
      if (!Utils.isEmpty(result)) result += ", ";
      result += city!;
    }
    if (!Utils.isEmpty(province)) {
      if (!Utils.isEmpty(result)) result += ", ";
      result += province!;
    }
    if (!Utils.isEmpty(country)) {
      if (!Utils.isEmpty(result)) result += ", ";
      result += country!;
    }
    return result;
  }

  static String getTextBreakLine(List<String?> listText) {
    String result = "";
    listText.forEach((element) {
      int index = listText.indexOf(element);
      if (index == 0) {
        result += listText[0] ?? "";
      } else {
        if (!Utils.isEmpty(listText[index])) {
          if (!Utils.isEmpty(result)) result += "\n";
          result += listText[index]!;
        }
      }
    });
    return result;
  }

  static String convertVNtoText(String str) {
    str = str.replaceAll(RegExp(r'[à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ]'), 'a');

    str = str.replaceAll(RegExp(r'[è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ]'), 'e');
    str = str.replaceAll(RegExp(r'[ì|í|ị|ỉ|ĩ]'), 'i');
    str = str.replaceAll(RegExp(r'[ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ]'), 'o');
    str = str.replaceAll(RegExp(r'[ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ]'), 'u');
    str = str.replaceAll(RegExp(r'[ỳ|ý|ỵ|ỷ|ỹ]'), 'y');
    str = str.replaceAll(RegExp(r'[đ]'), 'd');

    str = str.replaceAll(RegExp(r'[À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ]'), 'A');
    str = str.replaceAll(RegExp(r'[È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ]'), 'E');
    str = str.replaceAll(RegExp(r'[Ì|Í|Ị|Ỉ|Ĩ]'), 'I');
    str = str.replaceAll(RegExp(r'[Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ]'), 'O');
    str = str.replaceAll(RegExp(r'[Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ]'), 'U');
    str = str.replaceAll(RegExp(r'[Ỳ|Ý|Ỵ|Ỷ|Ỹ]'), 'Y');
    str = str.replaceAll(RegExp(r'[Đ]'), 'D');
    return str;
  }

  static String getRandString(int len) {
    var random = Random.secure();
    var values = List<int>.generate(len, (i) => random.nextInt(255));
    return base64UrlEncode(values);
  }

  static String formatNikText(String text) {
    var buffer = StringBuffer();
    for (int i = 0; i < text.length; i++) {
      buffer.write(text[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 4 == 0 && nonZeroIndex != text.length) {
        buffer.write(
            ' '); // Replace this with anything you want to put after each 4 numbers
      }
    }

    var string = buffer.toString();
    return string;
  }

  static String getPhoneNumberNonPrefix(
      String phoneNumber, String countryCode) {
    if (countryCode.startsWith("+")) {
      countryCode = countryCode.substring(1);
    }
    return phoneNumber.substring(countryCode.length);
  }

  static String getCountryCode(
      String phoneNumber, List<String> listNationalityCode) {
    if (phoneNumber.startsWith("+")) {
      phoneNumber = phoneNumber.substring(1);
    }
    String threeFirstChars = "";
    String twoFirstChars = "";
    String oneFirstChars = "";
    if (phoneNumber.length > 3) {
      threeFirstChars = phoneNumber.substring(0, 3);
      twoFirstChars = phoneNumber.substring(0, 2);
      oneFirstChars = phoneNumber.substring(0, 1);
    } else if (phoneNumber.length > 2) {
      twoFirstChars = phoneNumber.substring(0, 2);
      oneFirstChars = phoneNumber.substring(0, 1);
    } else if (phoneNumber.length > 1) {
      oneFirstChars = phoneNumber.substring(0, 1);
    }
    String code = "";
    if (!Utils.isEmpty(threeFirstChars)) {
      try {
        code = listNationalityCode.firstWhere(
            (element) => element.substring(1).startsWith(threeFirstChars));
      } catch (e) {}
      if (!Utils.isEmpty(code)) {
        return code;
      }
    }
    if (!Utils.isEmpty(twoFirstChars)) {
      try {
        code = listNationalityCode.firstWhere(
            (element) => element.substring(1).startsWith(twoFirstChars));
      } catch (e) {}
      if (!Utils.isEmpty(code)) {
        return code;
      }
    }
    if (!Utils.isEmpty(oneFirstChars)) {
      try {
        code = listNationalityCode.firstWhere(
            (element) => element.substring(1).startsWith(oneFirstChars));
      } catch (e) {}
      if (!Utils.isEmpty(code)) {
        return code;
      }
    }
    return code;
  }

  String formatNumberLike(int number) {
    if (number < 999) return '$number';
    RegExp regex = RegExp(r'([.]*0)(?!.*\d)');

    String s = (number / 1000).toStringAsFixed(2).replaceAll(regex, '');
    return '${s}K';
  }

  static bool isImage(String path) {
    final mimeType = lookupMimeType(path);
    return mimeType?.startsWith('image/') ?? false;
  }

  static String getKeyPostData(String url) {
    var uri = Uri.parse(url);
    var path = uri.path;
    var start = "upload/user";
    final end = p.extension(uri.path);
    final startIndex = path.indexOf(start);
    if (startIndex > 0 && end.isNotEmpty) {
      final endIndex = path.indexOf(end, startIndex + start.length);
      return path.substring(startIndex, endIndex + end.length);
    }
    return '';
  }

  static String getStartDate(int date){
    return DateUtil.parseDateToString(
        DateTime.fromMillisecondsSinceEpoch(
            date,
            isUtc: false),
        "HH:mm dd/MM/yyyy");
  }
}

class TimeagoLookupMessagesWithoutPrefix extends timeago.EnShortMessages {
  @override
  String aboutAnHour(int minutes) => '1h';

  @override
  String aDay(int hours) => '1d';

  @override
  String aboutAMonth(int days) => '1m';

  @override
  String aboutAYear(int year) => '1y';
}

class RandomString {
  var _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
}
