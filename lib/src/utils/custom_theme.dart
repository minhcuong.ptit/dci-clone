import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../res/R.dart';

extension CustomTheme on TextTheme {
  TextStyle get subHeading1 {
    return TextStyle(
        color: R.color.black,
        fontSize: 14.sp,
        fontWeight: FontWeight.w600,
        height: 24 / 14);
  }

  TextStyle get subHeading2 {
    return TextStyle(
        color: R.color.black,
        fontSize: 14.sp,
        fontWeight: FontWeight.w300,
        height: 24 / 14);
  }

  TextStyle get bodyRegular => TextStyle(
        color: R.color.primaryBlack,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        height: 20 / 14,
      );

  TextStyle get bodyMediumText => TextStyle(
        color: R.color.primaryBlack,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        height: 20 / 14,
      );

  TextStyle get bodyBold => TextStyle(
        color: R.color.black,
        fontSize: 13.sp,
        fontWeight: FontWeight.w600,
        height: 24 / 13,
      );

  TextStyle get bodySmallText => TextStyle(
        color: R.color.black,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        height: 21 / 14,
      );

  TextStyle get bodySmallBold => TextStyle(
        color: R.color.black,
        fontSize: 11.sp,
        fontWeight: FontWeight.w600,
        height: 20 / 11,
      );

  TextStyle get subTitleRegular => TextStyle(
        color: R.color.black,
        fontSize: 14.sp,
        fontWeight: FontWeight.w500,
        height: 24 / 14,
      );

  TextStyle get buttonCapital => TextStyle(
        color: R.color.black,
        fontSize: 15.sp,
        fontWeight: FontWeight.bold,
        height: 24 / 16,
      );

  TextStyle get buttonRegular => TextStyle(
        color: R.color.black,
        fontSize: 15.sp,
        fontWeight: FontWeight.bold,
        height: 24 / 16,
      );

  TextStyle get buttonSmall => TextStyle(
        color: R.color.black,
        fontSize: 12,
        fontWeight: FontWeight.bold,
        height: 20 / 12,
      );

  TextStyle get buttonLink => TextStyle(
      color: R.color.black,
      fontSize: 11.sp,
      fontWeight: FontWeight.w500,
      height: 24 / 11);

  TextStyle get labelTag => TextStyle(
      color: R.color.black,
      fontSize: 14.sp,
      fontWeight: FontWeight.bold,
      height: 20 / 14);

  TextStyle get tooltip => TextStyle(
      color: R.color.black,
      fontSize: 12.sp,
      fontWeight: FontWeight.w400,
      height: 16 / 12);

  TextStyle get labelLargeText => TextStyle(
      color: R.color.black,
      fontSize: 12.sp,
      fontWeight: FontWeight.w600,
      height: 20 / 12);

  TextStyle get labelSmallText => TextStyle(
      color: R.color.black,
      fontSize: 10.sp,
      fontWeight: FontWeight.w400,
      height: 16 / 10);

  TextStyle get labelMiniText => TextStyle(
      color: R.color.black,
      fontSize: 9.sp,
      fontWeight: FontWeight.w400,
      height: 12 / 9);

  TextStyle get labelMicroText => TextStyle(
      color: R.color.black,
      fontSize: 8.sp,
      fontWeight: FontWeight.w400,
      height: 12 / 8);

  TextStyle get labelMicroBoldText => TextStyle(
      color: R.color.black,
      fontSize: 8.sp,
      fontWeight: FontWeight.w600,
      height: 12 / 8);

  TextStyle get labelNanoBoldText => TextStyle(
      color: R.color.black,
      fontSize: 7.sp,
      fontWeight: FontWeight.w600,
      height: 11 / 7);

  TextStyle get labelNanoText => TextStyle(
      color: R.color.black,
      fontSize: 6.sp,
      fontWeight: FontWeight.w400,
      height: 10 / 6);

  TextStyle get title2 => TextStyle(
      color: R.color.black,
      fontSize: 16.sp,
      fontWeight: FontWeight.w700,
      height: 26 / 16);

  TextStyle get labelMarco => TextStyle(
      color: R.color.black,
      fontSize: 8.sp,
      fontWeight: FontWeight.w400,
      height: 12 / 8);

  TextStyle get titleSmallBold => TextStyle(
      color: R.color.black,
      fontSize: 11.sp,
      fontWeight: FontWeight.w600,
      height: 20 / 11);

  TextStyle get title => TextStyle(
        color: R.color.primaryLightGrey,
        fontSize: 30.sp,
        fontWeight: FontWeight.w700,
      );

  TextStyle get bold700 => TextStyle(
      color: R.color.black, fontSize: 21.sp, fontWeight: FontWeight.w700);

  TextStyle get regular400 => TextStyle(
      color: R.color.black, fontSize: 14.sp, fontWeight: FontWeight.w400);

  TextStyle get medium500 => TextStyle(
      color: R.color.black, fontWeight: FontWeight.w500, fontSize: 12.sp);
}
