import 'dart:ui';

import 'package:collection/src/iterable_extensions.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:imi/res/R.dart';

enum MemberStatusType {
  VERIFYING,
  APPROVED,
  INVITED,
}

enum MemberRole {
  CLUB_ADMIN,
  CLUB_MODERATOR,
  SUB_MEMBER,
  PRIME_MEMBER,
}

enum MemberType { ADMIN, LEADER, MEMBER, NEW_USER, NORMAL_USER, PARTNER }

enum ChallengeHistoryStatus { INPROGRESS, RESOLVED, TODO }

extension MemberRoleExt on MemberRole {
  String asDisplayString() {
    switch (this) {
      case MemberRole.CLUB_ADMIN:
        return R.string.member_role_club_admin.tr();
      case MemberRole.CLUB_MODERATOR:
        return R.string.member_role_club_moderator.tr();
      case MemberRole.SUB_MEMBER:
        return R.string.club_role_sub_member.tr();
      case MemberRole.PRIME_MEMBER:
        return R.string.club_role_prime_member.tr();
    }
  }
}

extension ListRoleExt on List<MemberRole> {
  bool get hasModeratorPermission =>
      contains(MemberRole.CLUB_ADMIN) || contains(MemberRole.CLUB_MODERATOR);
}

enum MembershipTitle {
  PRESIDENT,
  CLUB_ADMIN,
  VICE_PRESIDENT,
  SECRETARY,
  FINANCE,
  HUMAN_RESOURCE,
}

class MembershipTitleHelper {
  static List<MembershipTitle> parse(String raw) {
    if (raw.startsWith("[") && raw.endsWith("]")) {
      List<String> rawTitles = raw.substring(1, raw.length - 1).split(", ");
      return rawTitles
          .map((e) => MembershipTitle.values
              .firstWhereOrNull((element) => element.name == e))
          .whereNotNull()
          .toList();
    }
    return List.empty();
  }
}

enum KisType {
  CAR,
  MOTOR,
}

enum UserPackageStatus {
  DRAFT,
  PROCESSING,
  APPROVED,
  EXPIRED,
  VERIFYING,
  PAYMENT_FAILED,
  REJECTED,
}

enum ClubRole {
  PRESIDENT,
  SECRETARY,
  ADMIN,
  FINANCE,
  VICE_PRESIDENT,
  HUMAN_RESOURCE,
}

enum MembershipType {
  KTA,
  TKT,
  TAA,
  GASPOL_MERCHANT,
}

extension MembershipTypeExt on MembershipType {
  String get icon {
    switch (this) {
      case MembershipType.KTA:
        return R.drawable.ic_person;
      case MembershipType.TKT:
        return R.drawable.ic_group_person;
      case MembershipType.TAA:
        return R.drawable.ic_person_switch;
      case MembershipType.GASPOL_MERCHANT:
        return R.drawable.ic_marketplace;
    }
  }

  Color get color {
    switch (this) {
      case MembershipType.KTA:
        return R.color.primaryColor;
      case MembershipType.TKT:
        return R.color.blue;
      case MembershipType.TAA:
        return R.color.green;
      case MembershipType.GASPOL_MERCHANT:
        return R.color.purpleColor;
    }
  }
}

enum SwitchClubAction {
  SWITCH_CLUB,
  JOIN_CLUB,
  JOIN_SUB_CLUB,
}

extension SwitchClubActionExt on SwitchClubAction {
  String get addonType {
    switch (this) {
      case SwitchClubAction.SWITCH_CLUB:
        return this.name;
      case SwitchClubAction.JOIN_CLUB:
      case SwitchClubAction.JOIN_SUB_CLUB:
        return SwitchClubAction.JOIN_CLUB.name;
    }
  }
}

enum KisStatus {
  ENABLE,
  DISABLE,
}

enum EnrolledKisStatus {
  ACTIVE,
  PROCESSING,
}

enum TaaPackageType {
  IMI_BUSINESS_PARTNER,
  IMI_CLUB_ASSOCIATION,
  IMI_PROMOTOR,
}

enum MerchantPackageType {
  IMI_CORPORATE_MERCHANT,
  IMI_INDIVIDUAL_MERCHANT,
}

enum NotificationCode {
  ASSOCIATION,
  CLUB,
  COMMENT,
}

enum RedirectPage {
  REQUEST_JOIN_PAGE, // only admin get notified
  GROUP_PAGE, // invitation or request to join
  REPORT_PAGE,
  CHALLENGE_PAGE, //notification challenges
  PLAN_SEED_PAGE,
  MEDITATION_COFFEE_PAGE,
  MEDITATION_PAGE,
  PRIVATE_NOTIFICATION_PAGE,
  ORDER_HISTORY_PAGE,
  ORDER_WAS_CANCELED,
  CHAT_PAGE,
  COMMUNITY_POST_PAGE, // post
  POINT_TRANSACTION_IN_PAGE,
}

enum AssociationClubStatusType { WAITING, ACCEPTED, REJECTED }

enum ToastType {
  SUCCESS,
  WARNING,
  ERROR,
}

enum OtpChannel {
  SMS,
}

enum FollowingTab { ALL, INDIVIDUAL, GROUP, BOOKMARK }

extension FollowingTabExt on FollowingTab {
  String get title {
    switch (this) {
      case FollowingTab.ALL:
        return R.string.all.tr();
      case FollowingTab.INDIVIDUAL:
        return R.string.individual.tr();
      case FollowingTab.GROUP:
        return R.string.group.tr();
      case FollowingTab.BOOKMARK:
        return R.string.bookmarked.tr();
    }
  }
}

// with NEW account, after login, status can be empty or INIT, then must be REGISTERED after sign-up
// with OLD (Legacy) account, after login, status is LEGACY_REGISTERED, then must be REGISTERED after claim
enum PersonProfileStatus {
  INIT, // this marks user just logged in without profile completetion
  REGISTERED, // this marks user has completed registration flow
}

enum PostType {
  MEDIA,
  FREETEXT,
  LINK,
}

extension PostTypeExt on PostType {
  String get icon {
    switch (this) {
      case PostType.MEDIA:
        return R.drawable.ic_post_media;
      case PostType.FREETEXT:
        return R.drawable.ic_post_freetext;
      case PostType.LINK:
        return R.drawable.ic_post_link;
    }
  }

  String get title {
    switch (this) {
      case PostType.MEDIA:
        return R.string.post_type_media.tr();
      case PostType.FREETEXT:
        return R.string.post_type_checkin.tr();
      case PostType.LINK:
        return R.string.post_type_link.tr();
    }
  }
}

enum CommunityType {
  ASSOCIATION,
  BASIC_GROUP,
  CLUB,
  PROMOTOR,
  MERCHANT,
  PARTNER,
  INDIVIDUAL,
  STUDY_GROUP
}

enum GroupPrivacy { PUBLIC, PRIVATE }

enum GroupMemberRole { ADMIN, LEADER, TREASURER, SECRETARY, MEMBER }

enum DayOfWeek {
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
  SUNDAY
}

enum DayOfWeekTitle { T2, T3, T4, T5, T6, T7, CN }

enum CommunityStatus { ACTIVE, BANNED, COMPLETED }

enum MemberActionType { JOIN, LEAVE, ADD, INVITE, DELETE, UPDATE }

enum AdminAction { APPROVED, REJECTED }

enum NotificationType { ACCOUNT, SOCIAL }

enum CategoryType { KIS,DOCUMENT, CLUB, EMOJI, NEWS, MEDITATION_GUIDE, PLANT_SEED_GUIDE }

enum CleanSeedStatus { ACTIVE, DISABLED }

enum SeedStatus { ACTIVE, DISABLED }

enum CleanSeedAction { GOOD, BAD }

enum LibraryDocumentType {
  DOCUMENT_LEADER_EBOOK,
  DOCUMENT_LEADER_VIDEO,
  DOCUMENT_MEMBER_EBOOK,
  DOCUMENT_MEMBER_VIDEO,
  PUBLIC_EBOOK,
  PUBLIC_VIDEO
}

extension LibraryDocumentTypeTitle on LibraryDocumentType {
  String get title {
    switch (this) {
      case LibraryDocumentType.PUBLIC_VIDEO:
        return R.string.video_opening_document.tr();
      case LibraryDocumentType.PUBLIC_EBOOK:
        return R.string.ebook_opening_document.tr();
      case LibraryDocumentType.DOCUMENT_MEMBER_VIDEO:
        return R.string.video_material_for_students.tr();
      case LibraryDocumentType.DOCUMENT_MEMBER_EBOOK:
        return R.string.ebook_material_for_students.tr();
      case LibraryDocumentType.DOCUMENT_LEADER_VIDEO:
        return R.string.video_material_for_leader.tr();
      case LibraryDocumentType.DOCUMENT_LEADER_EBOOK:
        return R.string.ebook_material_for_leader.tr();

    }
  }
}

extension NotificationTypeExt on NotificationType {
  NotificationType get other {
    if (this == NotificationType.ACCOUNT) return NotificationType.SOCIAL;
    return NotificationType.ACCOUNT;
  }
}

extension CommunityTypeExt on CommunityType {
  String get postingIcon {
    switch (this) {
      case CommunityType.INDIVIDUAL:
        return R.drawable.ic_post_individual;
      default:
        return R.drawable.ic_post_club;
    }
  }
}

extension DayOfWeekExt on DayOfWeek {
  String get title {
    switch (this) {
      case DayOfWeek.MONDAY:
        return "T2";
      case DayOfWeek.TUESDAY:
        return "T3";
      case DayOfWeek.WEDNESDAY:
        return "T4";
      case DayOfWeek.THURSDAY:
        return "T5";
      case DayOfWeek.FRIDAY:
        return "T6";
      case DayOfWeek.SATURDAY:
        return "T7";
      case DayOfWeek.SUNDAY:
        return "CN";
    }
  }
}

extension DayOfWeekExtTitle on DayOfWeekTitle {
  String get getTitle {
    switch (this) {
      case DayOfWeekTitle.T2:
        return "MONDAY";
      case DayOfWeekTitle.T3:
        return "TUESDAY";
      case DayOfWeekTitle.T4:
        return "WEDNESDAY";
      case DayOfWeekTitle.T5:
        return "THURSDAY";
      case DayOfWeekTitle.T6:
        return "FRIDAY";
      case DayOfWeekTitle.T7:
        return "SATURDAY";
      case DayOfWeekTitle.CN:
        return "SUNDAY";
    }
  }
}

extension DayOfWeekString on DayOfWeek {
  String get titleDayOfWeek {
    switch (this) {
      case DayOfWeek.MONDAY:
        return R.string.monday.tr();
      case DayOfWeek.TUESDAY:
        return R.string.tuesday.tr();
      case DayOfWeek.WEDNESDAY:
        return R.string.wednesday.tr();
      case DayOfWeek.THURSDAY:
        return R.string.thursday.tr();
      case DayOfWeek.FRIDAY:
        return R.string.friday.tr();
      case DayOfWeek.SATURDAY:
        return R.string.saturday.tr();
      case DayOfWeek.SUNDAY:
        return R.string.sunday.tr();
    }
  }
}

enum PackageStatus {
  CREATED,
  VERIFYING,
  PAYMENT_WAITING,
  PAYMENT_PROCESSING,
  PAYMENT_ERROR,
  ACTIVE,
  EXPIRED,
}

enum DescriptionType {
  BENEFIT,
  REQUIREMENT,
}

enum KtaMembershipStatus {
  ACTIVE,
  EXPIRED,
}

enum DocumentPrivacyScope {
  LEADER,
  MEMBER,
  PUBLIC,
}

enum DocumentType {
  E_BOOK,
  VIDEO,
}

enum NewsCategory {
  MEDITATION_GUIDE,
  NEWS,
  PLANT_SEED_GUIDE,
  SELF_PRACTICE,
  SUCCESS_GOAL,
}

enum LeaderType {
  DIAMOND_LEADER,
  LEADER,
  PRE_LEADER,
  SCIM_DCI_SPEAKER,
  SENIOR_LEADER,
}

extension RankDCI on LeaderType {
  String title() {
    switch (this) {
      case LeaderType.DIAMOND_LEADER:
        return R.string.diamond_leader.tr();
      case LeaderType.LEADER:
        return R.string.leader.tr();
      case LeaderType.PRE_LEADER:
        return R.string.pre_leader.tr();
      case LeaderType.SCIM_DCI_SPEAKER:
        return R.string.scim_dci_speaker.tr();
      case LeaderType.SENIOR_LEADER:
        return R.string.senior_leader.tr();
    }
  }
}

enum PopMenu {
  SORT_BY_POPULARITY,
  LATEST,
  LOW_TO_HIGH,
  HIGH_TO_LOW,
  OUTSTANDING,
  SIMILAR
}

extension Menu on PopMenu {
  String title() {
    switch (this) {
      case PopMenu.SORT_BY_POPULARITY:
        return R.string.sort_by_popularity.tr();
      case PopMenu.LATEST:
        return R.string.lastest.tr();
      case PopMenu.LOW_TO_HIGH:
        return R.string.low_to_high.tr();
      case PopMenu.HIGH_TO_LOW:
        return R.string.high_to_low.tr();
      case PopMenu.OUTSTANDING:
        return R.string.outstanding.tr();
      case PopMenu.SIMILAR:
        return R.string.similar.tr();
    }
  }
}

enum ReportReasonType { INDIVIDUAL, POST, GROUP }

enum Order { ASC, DESC }

enum PostStatus {
  ACTIVE,
  DISABLED,
  REPORTED,
}

enum ReportPostAction { APPROVE, REJECT }

enum AudioType { ENVIRONMENT }

enum ReportType {
  DAY,
  MONTH,
  WEEK,
}

enum BannerType {
  COURSE,
  EXTERNAL_LINK,
  LIBRARY,
  NEWS,
  NOT_LINK,
}

enum MissionType {
  MEDITATION,
  PLAN_SEED,
  PRACTICE,
}

extension MissionTypeExt on MissionType {
  String title() {
    switch (this) {
      case MissionType.MEDITATION:
        return R.string.meditation.tr();
      case MissionType.PLAN_SEED:
        return R.string.plan_seed.tr();
      case MissionType.PRACTICE:
        return R.string.practice.tr();
    }
  }
}

enum PaymentType {
  ALEPAY_ATM,
  ALEPAY_GATEWAY,
  ALEPAY_INSTALLMENT,
  ALEPAY_VISA,
  ONLINE,
  SUPPORT,
}

extension PaymentTypeExt on PaymentType {
  String title() {
    switch (this) {
      case PaymentType.SUPPORT:
        return R.string.dci_will_contact_to_support_payment.tr();
      case PaymentType.ONLINE:
        return R.string.online_payments.tr();
      case PaymentType.ALEPAY_ATM:
        return R.string.payment_by_atm_card.tr();
      case PaymentType.ALEPAY_GATEWAY:
        return R.string.payment_with_ale_pay.tr();
      case PaymentType.ALEPAY_VISA:
        return R.string.payment_by_international_card.tr();
      case PaymentType.ALEPAY_INSTALLMENT:
        return R.string.installment_payment.tr();
    }
  }
}

enum OrderStatus {
  CANCELED,
  PAID,
  WAITING_PAYMENT,
}

enum AuditSide {
  ADMIN_DASHBOARD,
  MOBILE_APP,
}

enum CommonStatus { ACTIVE, DISABLED }

enum ForgotPinType {
  SIGN_UP,
  FORGOT_PIN,
}

extension ForgotPinTypeExt on ForgotPinType {
  String get title {
    switch (this) {
      case ForgotPinType.SIGN_UP:
        return R.string.verification.tr();
      case ForgotPinType.FORGOT_PIN:
        return R.string.forgot_pin.tr();
    }
  }
}

enum ProductType { COURSE, EVENT }

enum ActionTypePoint {
  IN,
  OUT,
}

enum CouponLimitType {
  SINGLE,
  COMBO,
}

enum DiscountType {
  PERCENTAGE,
  PRICE,
}
