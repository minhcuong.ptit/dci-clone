import 'package:easy_localization/easy_localization.dart';
import 'package:imi/res/R.dart';

import 'const.dart';
import 'utils.dart';

class Validators {
  static final RegExp nikNumberRegex = RegExp(r'[0-9 ]');
  static final RegExp rtrwRegex = RegExp(r'[0-9 /]');
  static final RegExp numberRegex = RegExp(r'[0-9]');
  static final RegExp digitRegex = RegExp(r'[a-zA-Z]');
  static final RegExp nameRegex = RegExp("[a-z A-Z á-ú Á-Ú]");
  static final RegExp alphanumericRegex = RegExp("[a-z A-Z á-ú Á-Ú 0-9]");
  static final RegExp defaultAlphanumericRegex = RegExp("[a-zA-Z0-9]");
  static final RegExp phoneRegex = RegExp(r'(^(?:[+0])?[0-9]{10,12}$)');
  static final RegExp emailRegex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  bool isContainNumber(String? text) {
    if (Utils.isEmpty(text)) return false;
    return text!.contains(numberRegex);
  }

  bool isAlphabet(String? text) {
    if (Utils.isEmpty(text)) return false;
    return text!.contains(digitRegex);
  }

  bool isSpecialCharacter(String? text) {
    if (Utils.isEmpty(text)) return false;
    return !(text!.contains(digitRegex) || text.contains(numberRegex));
  }

  String? checkPhoneNumber(String? phoneNumber,
      {String? errorEmpty, String? errorInvalid}) {
    phoneNumber = phoneNumber?.trim();
    if (Utils.isEmpty(phoneNumber)) {
      return errorEmpty ?? R.string.please_enter_phone_number.tr();
    }
    /* else if (!phoneRegex.hasMatch(phoneNumber!)) {
      return errorInvalid ?? R.string.phone_not_valid.tr();
    } */
    else {
      return null;
    }
  }

  String? checkPhoneNumber2(String? phoneNumber, {String? errorInvalid}) {
    phoneNumber = phoneNumber?.trim();
    if (Utils.isEmpty(phoneNumber)) return null;
    // if (!phoneRegex.hasMatch(phoneNumber!)) {
    //   return errorInvalid ?? R.string.phone_not_valid.tr();
    // } else {
    //   return null;
    // }
    return null;
  }

  String? checkEmail(String? email) {
    email = email?.trim();
    if (Utils.isEmpty(email)) {
      return R.string.please_enter_email.tr();
    } else if (!emailRegex.hasMatch(email!)) {
      return R.string.email_not_valid.tr();
    } else if (email.length > 255) {
      return R.string.email_max_character.tr();
    } else {
      return null;
    }
  }

  String? checkEmail2(String? email) {
    email = email?.trim();
    if (Utils.isEmpty(email)) return null;
    if (!emailRegex.hasMatch(email!)) {
      return R.string.email_not_valid.tr();
    } else {
      return null;
    }
  }

  String? checkPin(String? pin) {
    if (Utils.isEmpty(pin)) {
      return R.string.please_enter_pin.tr();
    } else if (pin!.length < 6 || pin.length > 20) {
      return R.string.password_validate_length.tr();
    } else if (!Validators.validateStructure(pin)) {
      return R.string.password_contains_invalid_characters.tr();
    } else {
      return null;
    }
  }

  static bool validateStructure(String value) {
    String pattern = r'^[a-zA-Z0-9!@#\%$^&*~]+$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  String? checkTextEmpty(String? text, {String? errorEmpty, String? title}) {
    text = text?.trim();
    if (Utils.isEmpty(text))
      return errorEmpty ?? R.string.text_empty.tr(args: [title ?? ""]);
    // else if (name!.length < 4)
    //   return R.string.full_name_at_least_character.tr();
    // else if (errorInvalid != null && )
    //   return errorInvalid
    return null;
  }

  String? checkName(String? name,
      {String? errorEmpty, String? errorInvalid, String? errorMaxLength}) {
    name = name?.trim();
    if (Utils.isEmpty(name))
      return errorEmpty ?? R.string.please_enter_full_name.tr();
    // else if (name!.length < 4)
    //   return R.string.full_name_at_least_character.tr();
    // else if (errorInvalid != null && )
    //   return errorInvalid
    else if (name!.length > 255) {
      return errorMaxLength ?? R.string.full_name_max_character.tr();
    } else if (!nameRegex.hasMatch(name)) {
      return R.string.invalid_name.tr();
    }
    return null;
  }


  int? checkTypeId(String id) {
    if (Utils.isEmpty(id)) return null;
    if (phoneRegex.hasMatch(id)) return Const.LAUNCH_TYPE_PHONE;
    if (emailRegex.hasMatch(id)) return Const.LAUNCH_TYPE_EMAIL;
    return null;
  }
}
