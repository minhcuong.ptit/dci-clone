import '../data/network/response/post_data.dart';

abstract class DCIEventBase {}

///Event handler that execute multiple callback function.
///
///It is implemented with a Singleton pattern and is called executing its factory constructor.
class DCIEventHandler {
  static DCIEventHandler? _singleton;

  DCIEventHandler._internal();

  factory DCIEventHandler() {
    _singleton ??= DCIEventHandler._internal();
    return _singleton!;
  }

  Map<Type, List<Function>> events = {};

  ///Subscribe the function to event
  ///
  ///The function must receive an object that inherits from EventBase.
  ///Whenever an event is sent with an object of that type, the function passed by parameter will be called.
  ///
  ///Returns the instance of the eventhandler for multiple subscriptions.
  Future<DCIEventHandler> subscribe<T extends DCIEventBase>(
      void Function(T) callback) async {
    if (T == Null) {
      return this;
    }

    if (!events.containsKey(T)) {
      events[T] = <Function(T)>[];
    }

    if (events[T]!.contains(callback)) {
      return this;
    }

    events[T]!.add(callback);

    return this;
  }

  ///Send a event
  ///
  ///Send an object that inherits from the Base event and that will call all the subscribers of the data type of the object.
  ///
  ///Returns the instance of the eventhandler to send multiple events
  Future<DCIEventHandler> send<T extends DCIEventBase>(T data) async {
    events[T]?.forEach((funcCallback) => funcCallback(data));
    return this;
  }

  ///Unsubscribe to event
  ///
  ///The function passed by parameter must be the same as that sent in the subscription.
  ///
  ///Returns the instance of the eventhandler for multiple unsubscriptions.
  Future<DCIEventHandler> unsubscribe<T extends DCIEventBase>(
      void Function(T) callback) async {
    if (events.containsKey(T)) events[T]!.remove(callback);
    return this;
  }
}

enum PostEventType { CREATE_NEW_PAGE, EDIT_POST }

class PostEventEvent extends DCIEventBase {
  PostEventType? postEventType;
  PostData? postData;
}

class UpdateProfileEvent extends DCIEventBase {}

class YoutubeEventEvent extends DCIEventBase {
  double ratio = 16 / 9;
}

class NewNotificationEvent extends DCIEventBase {}
