import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:imi/src/utils/const.dart';

class AppConfig {
  static final Environment environment = _load();

  static Environment _load() {
    const env = String.fromEnvironment("DCI_ENV");
    return Environment.values
            .firstWhereOrNull((element) => element.name == env) ??
        Environment.STAGING;
  }
}

enum Environment {
  STAGING,
  HOTFIX, // also is UAT
  PRODUCTION,
}

extension EnvironmentExt on Environment {
  String get suffix {
    switch (this) {
      case Environment.STAGING:
        return ".stg";
      case Environment.HOTFIX:
        return ".hotfix";
      case Environment.PRODUCTION:
        return "";
    }
  }

  String get clientId {
    switch (this) {
      case Environment.STAGING:
        return "mobile-app-client";
      case Environment.HOTFIX:
        return "mobile-app-client";
      case Environment.PRODUCTION:
        return "mobile-app-client";
    }
  }

  String get apiEndpoint {
    switch (this) {
      case Environment.STAGING:
        return "https://api.stg.dci.vn/";
      case Environment.HOTFIX:
        return "https://api.hotfix.dci.vn/";
      case Environment.PRODUCTION:
        return "https://api.dci.vn/";
    }
  }

  String get graphqlEndpoint {
    return "${apiEndpoint}graphql/";
  }

  String get authEndpoint {
    switch (this) {
      case Environment.STAGING:
        return "https://keycloak.dci.vn/auth/realms/dci.staging/protocol/openid-connect/token";
      case Environment.HOTFIX:
        return "https://keycloak.dci.vn/auth/realms/dci.hotfix/protocol/openid-connect/token";
      case Environment.PRODUCTION:
        return "https://keycloak.dci.vn/auth/realms/dci.prod/protocol/openid-connect/token";
    }
  }

  String get pubNubPublishKey {
    switch (this) {
      case Environment.PRODUCTION:
        return Const.PUBLISH_KEY_PROD;
      case Environment.HOTFIX:
        return Const.PUBLISH_KEY_STG;
      default:
        return Const.PUBLISH_KEY_STG;
    }
  }

  String get pubNubSubscribeKey {
    switch (this) {
      case Environment.PRODUCTION:
        return Const.SUBSCRIBE_KEY_PROD;
      case Environment.HOTFIX:
        return Const.SUBSCRIBE_KEY_STG;
      default:
        return Const.SUBSCRIBE_KEY_STG;
    }
  }

  String get logoutEndpoint {
    switch (this) {
      case Environment.STAGING:
        return "https://keycloak.dci.vn/auth/realms/dci.staging/protocol/openid-connect/logout";
      case Environment.HOTFIX:
        return "https://keycloak.dci.vn/auth/realms/dci.hotfix/protocol/openid-connect/logout";
      case Environment.PRODUCTION:
        return "https://keycloak.dci.vn/auth/realms/dci.prod/protocol/openid-connect/logout";
    }
  }
}
