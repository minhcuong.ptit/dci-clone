extension StringCasingExtension on String {
  String toCapitalized() =>
      this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1).toLowerCase()}' : '';

  String toTitleCase() =>
      this.replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.toCapitalized()).join(" ");

  String toFormatKtaNumber() => (!this.contains(" ") && this.length >= 10)
      ? (this.substring(0, 3) + " " + this.substring(3, 6) + " " + this.substring(6))
      : this;

  String? getFlagFromCountryCode() => this.toUpperCase().replaceAllMapped(
      RegExp(r'[A-Z]'), (match) => String.fromCharCode(match.group(0)!.codeUnitAt(0) + 127397));

  String? toEmoji() => this.startsWith("U+") == true ? String.fromCharCode(int.parse("0x${this.replaceAll("U+", "")}")) : this;
}
