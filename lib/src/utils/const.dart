import 'package:flutter_screenutil/flutter_screenutil.dart';

class Const {
  static const videoToolbarHeight = 60.0;
  static int bytes = 1024 * 1024;
  static int IMAGE_SIZE = 5 * bytes; //bytes
  static int VIDEO_SIZE = 100 * bytes; //bytes
  static double POST_ITEM_HEIGHT = 246.h;
  static int CACHE_IMAGE_HEIGHT_COMMUNITY = 246;
  static int CACHE_IMAGE_WIDTH_COMMUNITY = 200;
  static const String PRIVACY_URL =
      "https://dci.edu.vn/chinh-sach-bao-mat-va-quyen-rieng-tu-dci-app";
  static const String TERM_URL =
      "https://dci.edu.vn/dieu-khoan-dich-vu-dci-app";
  static const String ABOUT = "https://dci.edu.vn/";
  static const String SUPPORT = "https://dci.edu.vn/";
  static const TEMPLATE_URL_ADART_ASSOCIATION =
      "https://s3.ap-southeast-1.amazonaws.com/storage.gaspol.co.id/public/documents/TEMPLATE+ADART+FOR+ASOSIASI.docx";
  static const TEMPLATE_URL_ADART_CLUB =
      "https://s3.ap-southeast-1.amazonaws.com/storage.gaspol.co.id/public/documents/TEMPLATE+ADART+FOR+CLUB.docx";
  static const SEE_THE_INSTRUCTIONS = "https://dci.edu.vn/huong-dan-dang-ky-app-dcivn";

  static const String HEADER_KEY_DEVICE_TOKEN = "DEVICE_TOKEN";
  static const int NETWORK_DEFAULT_LIMIT = 20;
  static const int LIMIT = 5;
  static const int IMI_COMMUNITY_ID = 17327;

  // shared preferences keys
  static const String LANGUAGE = "LANGUAGE";
  static const String FIRST_LAUNCH = "FIRST_LAUNCH";

  // locales
  static const String LOCALE_EN = "en"; // accept: en, en-US
  static const String LOCALE_EN_SHORT = "en_short";
  static const String LOCALE_VN = "vi"; // accept: vi
  static const String LOCALE_CN = "zh"; // accept: cn
  static const String DEFAULT_COUNTRY_CODE = "+84";

  // date & time
  static const String DATE_FORMAT_REQUEST = "dd/MM/yyyy";
  static const String DATE_FORMAT = "dd MMMM yyyy";
  static const String DATE_TIME_FORMAT = "dd MMMM yyyy | HH:mm";
  static const String TIME_FORMAT = "HH:mm";
  static const String TIME_FORMAT_V2 = "HH:mm:ss";
  static const String DATE_FORMAT_EXPIRATION = "dd/MM/yy";
  static const String DATE_FORMAT_POINT = "dd/MM/yyyy  -  HH:mm";
  static const String DATE_FORMAT_POINT_2 = "dd-MM-yyyy   HH:mm";
  static const String DATE_FORMAT_POINT_3 = "HH:mm dd-MM-yyyy";

  // system
  static const String OS_ANDROID = "ANDROID";
  static const String OS_IOS = "IOS";

  // global actions
  static const String ACTION_REFRESH = "Refresh";
  static const String ACTION_CHANGE_TAB_MEMBERSHIP =
      "ACTION_CHANGE_TAB_MEMBERSHIP";
  static const int LAUNCH_TYPE_WEB = 0;
  static const int LAUNCH_TYPE_EMAIL = 1;
  static const int LAUNCH_TYPE_PHONE = 2;
  static const int LAUNCH_TYPE_SMS = 3;

  // generic
  static const String MALE = "male";
  static const String FEMALE = "female";

  // screens
  static const int SCREEN_HOME = 0;
  static const int SCREEN_SEARCH = 1;
  static const int SCREEN_MEMBERSHIP = 2;
  static const int SCREEN_SETTING = 3;

  // payment
  static const int PAYMENT_KIS_PROCESSING_FEE = 10000;

  // kis
  static const int KIS_MAXIMUM_SELECTION = 4;

  // package
  static const int PACKAGE_WARNING_EXPIRE_THRESHOLD_DAYS = 30;

  // post
  static const int POST_MAXIMUM_CHARACTER = 1500;
  static const int POST_MAXIMUM_MEDIA = 4;

  // to clean

  static const String REGISTERED = "REGISTERED";
  static const String INDIVIDUAL = "INDIVIDUAL";
  static const String CLUB = "CLUB";
  static const String ASSOCIATION = "ASSOCIATION";
  static const String MERCHANT = "MERCHANT";
  static const String PROMOTOR = "PROMOTOR";
  static const String KIS = "KIS";
  static const String EMOJI = "EMOJI";

  static const String JOIN = "JOIN";
  static const String LEAVE = "LEAVE";
  static const String SUB_MEMBER = "SUB_MEMBER";

  static const String CLUB_ADMIN = "CLUB_ADMIN";
  static const String MEMBER = "MEMBER";

  static const String ID = "ID";
  static const String COMMUNITY_ID = "COMMUNITY_ID";
  static const String COMMUNITY_V2 = "COMMUNITY_V2";
  static const String DCI_LEVEL = "DCI_LEVEL";
  static const String DCI_LEVEL_ID = "DCI_LEVEL_ID";
  static const String OWNER_ID = "OWNER_ID";
  static const String EMAIL = "EMAIL";
  static const String PHONE = "PHONE";
  static const String BIRTH_PLACE = "BIRTH_PLACE";
  static const String BIRTH_DATE = "BIRTH_DATE";
  static const String FIRST_NAME = "FIRST_NAME";
  static const String FULL_NAME = "FULL_NAME";
  static const String USERNAME = "USERNAME";
  static const String USER_STATUS = "USER_STATUS";
  static const String PROVINCE = "PROVINCE";
  static const String PROVINCE_ID = "PROVINCE_ID";
  static const String CITY = "CITY";
  static const String DISTRICT = "DISTRICT";
  static const String WARD = "WARD";
  static const String DOB = "DOB";
  static const String GENDER = "GENDER";
  static const String AVATAR = "AVATAR";
  static const String COVER = "COVER";
  static const String POINT = "POINT";
  static const String POSTS = "POSTS";
  static const String FOLLOWERS = "FOLLOWERS";
  static const String FOLLOWINGS = "FOLLOWINGS";
  static const String FAVORITE_CATEGORY = "FAVORITE_CATEGORY";
  static const String SAVE_CATEGORY = "SAVE_CATEGORY";
  static const String FAVORITE_COMMUNITY = "FAVORITE_COMMUNITY";
  static const String REFERRAL_CODE = "REFERRAL_CODE";
  static const String USER_POINT = "USER_POINT";
  static const String FOLLOW = "FOLLOW";
  static const String UNFOLLOW = "UNFOLLOW";
  static const String PIN_CODE = "PIN_CODE";
  static const String ADDRESS = "ADDRESS";

  // static const String MEMBERSHIP_NAME = "MEMBERSHIP_NAME";
  // static const String MEMBERSHIP_STATUS = "MEMBERSHIP_STATUS";
  // static const String MEMBERSHIP_ADDRESS = "MEMBERSHIP_ADDRESS";
  // static const String MEMBERSHIP_PROVINCE = "MEMBERSHIP_PROVINCE";
  // static const String MEMBERSHIP_CITY = "MEMBERSHIP_CITY";
  // static const String MEMBERSHIP_DISTRICT = "MEMBERSHIP_DISTRICT";
  // static const String MEMBERSHIP_WARD = "MEMBERSHIP_WARD";
  // static const String MEMBERSHIP_EXPIRED = "MEMBERSHIP_EXPIRED";
  // static const String MEMBERSHIP_CLUB = "MEMBERSHIP_CLUB";

  static const String GROUP = "group";
  static const String DIRECT = "direct";

  static const String PUBLIC = "PUBLIC";
  static const String PRIVATE = "PRIVATE";
  static const String ONLY_MEMBER = "ONLY_MEMBER";

  static const String TWITTER = "twitter";
  static const String FACEBOOK = "facebook";

  static const String LIKE = "LIKE";
  static const String DISLIKE = "DISLIKE";
  static const String BOOKMARK = "BOOKMARK";
  static const String REMOVE = "REMOVE";

  static const String DETAIL_LIBRARY = "DETAIL_LIBRARY";
  static const String LIBRARY = "LIBRARY";
  static const String NEWS = "NEWS";
  static const String COURSE = "COURSE";
  static const String LEADER = "LEADER";
  static const String INFO = "INFO";
  static const String COUNTRY_NAME = "COUNTRY_NAME";

  /// BLOCK
  static const String BLOCK = "BLOCK";
  static const String UNBLOCK = "UNBLOCK";

  /// TIMER
  static const String HOURS = "HOURS";
  static const String MINUTES = "MINUTES";
  static const String SECONDS = "SECONDS";
  static const String TIMER_START = "TIMER_START";
  static const String TIMER_START_MUSIC = "TIMER_START_MUSIC";
  static const String TIMER_START_NO_MUSIC = "TIMER_START_NO_MUSIC";
  static const String TIMER_MEDITATION = "TIMER_MEDITATION";
  static const String TIMER_PLAN_SEED = "TIMER_PLAN_SEED";
  static const String TIMER_MEDITATION_CAFE = "TIMER_MEDITATION_CAFE";
  static const String TIME_PLAN_SEED = "TIME_PLAN_SEED";
  static const String SET_TIME_CAFE = "SET_TIME_CAFE";
  static const String SET_TIME_MEDITATION = "SET_TIME_MEDITATION";
  static const String SET_TIME_MEDITATION1 = "SET_TIME_MEDITATION1";
  static const String SET_TIME_MEDITATION2 = "SET_TIME_MEDITATION2";
  static const String SET_TIME_MEDITATION3 = "SET_TIME_MEDITATION3";
  static const String SET_TIME_MEDITATION4 = "SET_TIME_MEDITATION4";
  static const String SET_TIME_MEDITATION5 = "SET_TIME_MEDITATION5";
  static const String SET_TIME_MEDITATION6 = "SET_TIME_MEDITATION6";
  static const String SET_TIME_MEDITATION7 = "SET_TIME_MEDITATION7";
  static const String TIMER = "TIMER";
  static const String SAVE_SET_TIME_MEDITATION = "SAVE_SET_TIME_MEDITATION";
  static const String SAVE_TIME_PLAN_SEED = "SAVE_TIME_PLAN_SEED";
  static const String SAVE_SET_TIME_CAFE = "SAVE_SET_TIME_CAFE";
  static const String COUNT_TIME = "COUNT_TIME";
  static const String TIMER_UNLIMITED = "TIMER_UNLIMITED";

  /// SEED
  static const KEY1 = "KEY1";
  static const KEY2 = "KEY2";
  static const KEY3 = "KEY3";
  static const KEY4 = "KEY4";
  static const KEY5 = "KEY5";
  static const KEY6 = "KEY6";
  static const KEY7 = "KEY7";
  static const KEY8 = "KEY8";
  static const KEY9 = "KEY9";
  static const KEY10 = "KEY10";

  ///BELLS MEDITATION

  static const String BOOL_BELL_START = "BOOL_BELL_START";
  static const String BOOL_BELL_END = "BOOL_BELL_END";
  static const String BOOL_BELL_DELAY = "BOOL_BELL_DELAY";
  static const String BOOL_AMBIENT_SOUND = "BOOL_AMBIENT_SOUND";
  static const String INDEX_BELL_START = "INDEX_BELL_START";
  static const String INDEX_BELL_END = "INDEX_BELL_END";
  static const String INDEX_BELL_DELAY = "INDEX_BELL_DELAY";
  static const String INDEX_BELL_AMBIENT_SOUND = "INDEX_BELL_AMBIENT_SOUND";
  static const String REPETITION_BELL_START = "REPETITION_BELL_START";
  static const String REPETITION_BELL_END = "REPETITION_BELL_END";
  static const String REPETITION_DELAY = "REPETITION_DELAY";
  static const String BELLS_START = "BELLS_START";
  static const String BELLS_END = "BELLS_END";
  static const String BELLS_DELAY = "BELLS_DELAY";
  static const String BELLS_AMBIENT_SOUND_INDEX = "BELLS_AMBIENT_SOUND_INDEX";
  static const String AUDIO = "AUDIO";
  static const String FILE_AUDIO = "FILE_AUDIO";

  ///
  static const String SEARCH_SYSTEM = "SEARCH_SYSTEM";
  static const String SEARCH_DETAIL_SYSTEM = "SEARCH_DETAIL_SYSTEM";
  static const String SEARCH_USER_MESSAGES = "SEARCH_USER_MESSAGES";

  static const String ZERO_ITEM = "ZERO_ITEM";

  static const String SUPPORT_PATTERN = "%%support%%";
  static const String SUPPORT_DESCRIPTION_PATTERN = "%%support_description%%";

  //count noti
  static const String COUNT_CART = "COUNT_CART";
  static const String COUNT_MESSAGE = "COUNT_MESSAGE";
  //key pubnub prod
  static const String SUBSCRIBE_KEY_PROD = "sub-c-f99d3420-4854-40ca-9eae-e61da43f6de2";
  static const String PUBLISH_KEY_PROD = "pub-c-a77568c1-9623-40dc-aa79-28fb514e66bf";
  //key pubnub stg
  static const String SUBSCRIBE_KEY_STG = "sub-c-ab59b982-e58f-4291-91bd-d49228cdad6b";
  static const String PUBLISH_KEY_STG = "pub-c-729e0a49-7800-4bd7-b179-91e56c792746";
//
  static const String POPUP_BUY_COURSE_EVENT = "POPUP_BUY_COURSE_EVENT";
  //coupons
  static const String COUPONS = "COUPONS";
  //Challenge
  static const String RESOLVE_CHALLENGE = "RESOLVE_CHALLENGE";
  static const String RESOLVE_CHALLENGE_DAY = "RESOLVE_CHALLENGE_DAY";

  // predefined static values
  static const List<String> FILE_EXTENSIONS = [
    "doc",
    "pdf",
    "docx",
    "xlsx",
    "xls",
    "xltx",
    "pptx",
    "ppt",
    "odt",
    "txt",
    "jpg",
    "png",
    "jpeg"
  ];

}

class ServerError {
  static const String level_error = "level_error";
  static const String leave_group_error = "leave_group_error";
  static const String unassign_role_error = "unassign_role_error";
  static const String delete_member_error = "delete_member_error";
  static const String user_is_banned_error = "user_is_banned_error";
  static const String update_history_error = "update_history_error";
  static const String user_is_blocked_error = "user_is_blocked_error";
  static const String unavailable_post = "unavailable_post";
  static const String permissions_error = "permissions_error";
  static const String user_not_exists_error = "user_not_exists_error";
  static const String do_only_1_challenge_at_the_same_level =
      "do_only_1_challenge_at_the_same_level_at_the_same_time_error";
  static const String document_is_not_published = "Document is not published";
  static const String course_is_not_open = "Course is not open";
  static const String news_is_not_published = "News is not published";
  static const String coupon_is_already_applied_error =
      "coupon_is_already_applied_error";
  static const String coupon_is_expired_error = "coupon_is_expired_error";
  static const String not_found = "not_found";
  static const String coupon_is_invalid_error = "coupon_is_invalid_error";
  static const String coupon_can_not_combine_error =
      "coupon_can_not_combine_error";
  static const String coupon_level_is_unavailable_for_this_item_error =
      "coupon_level_is_unavailable_for_this_item_error";
  static const String can_not_cancel_order_error = "can_not_cancel_order_error";
  static const String order_history_already_canceled_error =
      "order_history_already_canceled_error";
  static const String coupon_is_unavailable_for_this_item_error =
      "coupon_is_unavailable_for_this_item_error";
  static const String coupon_is_unavailable_for_all_item_order_error =
      "coupon_is_unavailable_for_all_item_order_error";
  static const String coupon_invalid_min_price_error =
      "coupon_invalid_min_price_error";
  static const String coupon_reach_maximum_usage_error =
      "coupon_reach_maximum_usage_error";
  static const String challenge_not_match_your_level_error =
      "challenge_not_match_your_level_error";
  static const String event_reach_maximum_usage_error = "event_reach_maximum_usage_error";
  static const String product_is_not_available_error =
      "product_is_not_available_error";
  static const String event_free_duplicate_email_and_phone_error =
      "event_free_duplicate_email_and_phone_error";
  static const String event_free_duplicate_phone_error =
      "event_free_duplicate_phone_error";
  static const String event_free_duplicate_email_error =
      "event_free_duplicate_email_error";
  static const String min_price_payment_error ="min_price_payment_error";
  static const String not_support_payment_method_error ="not_support_payment_method_error";
  static const String only_one_combo_coupon_error ="only_one_combo_coupon_error";
}
