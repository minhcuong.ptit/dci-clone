extension NumberExtension on int {
  String quantity() {
    int integer = this;
    int real = 0;
    String postfix = "";
    if (this >= 1e6) {
      integer = this ~/ 1e6;
      real = (this % 1e6) ~/ 1e5;
      postfix = ".${real}M";
    } else if (this >= 1e3) {
      integer = this ~/ 1e3;
      real = (this % 1e3) ~/ 1e2;
      postfix = ".${real}K";
    }
    return "$integer$postfix";
  }
}
