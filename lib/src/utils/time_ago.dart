import 'package:easy_localization/easy_localization.dart';
import 'package:imi/src/data/preferences/app_preferences.dart';

import '../../res/R.dart';
import 'const.dart';
import 'utils.dart';

class TimeAgo{
  static String timeAgoSinceDate(DateTime? dateTime, {bool numericDates = true}) {
    if (Utils.isEmpty(dateTime))
      return "";
    final date2 = DateTime.now();
    final difference = date2.difference(dateTime!);

    if (difference.inDays > 2) {
      return DateFormat(Const.DATE_TIME_FORMAT, appPreferences.appLanguage).format(dateTime);
    } /*else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } */else if (difference.inDays >= 2) {
      return '${difference.inDays} ${R.string.days_ago.tr()}';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 ${R.string.day_ago.tr().toLowerCase()}' : R.string.yesterday.tr();
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} ${R.string.hours_ago.tr().toLowerCase()}';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 ${R.string.hour_ago.tr().toLowerCase()}' : R.string.an_hour.tr();
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} ${R.string.minutes_ago.tr().toLowerCase()}';
    } else if (difference.inMinutes >= 1) {
      return (numericDates) ? '1 ${R.string.min_ago.tr().toLowerCase()}' : R.string.a_minute.tr();
    } else if (difference.inSeconds >= 3) {
      return '${difference.inSeconds} ${R.string.seconds_ago.tr().toLowerCase()}';
    } else {
      return R.string.just_now.tr();
    }
  }

  static String timeFormat(DateTime? dateTime, {bool numericDates = true}) {
    if (Utils.isEmpty(dateTime))
      return "";
    return DateFormat(Const.TIME_FORMAT, appPreferences.appLanguage).format(dateTime!);
  }

  static String timeAgoSinceDateChat(DateTime? dateTime, {bool numericDates = true}) {
    if (Utils.isEmpty(dateTime))
      return "";
    final date2 = DateTime.now();
    final difference = date2.difference(dateTime!);

    if (difference.inDays > 7) {
      return DateFormat(Const.DATE_TIME_FORMAT, appPreferences.appLanguage).format(dateTime);
    } /*else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } */else if (difference.inDays >= 2) {
      return '${difference.inDays} ${R.string.days_ago.tr()}';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 ${R.string.day_ago.tr().toLowerCase()}' : R.string.yesterday.tr();
    } else if (difference.inHours >= 2) {
      return '${difference.inHours} ${R.string.hours_ago.tr().toLowerCase()}';
    } else if (difference.inHours >= 1) {
      return (numericDates) ? '1 ${R.string.hour_ago.tr().toLowerCase()}' : R.string.an_hour.tr();
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} ${R.string.minutes_ago.tr().toLowerCase()}';
    } else if (difference.inMinutes >= 1) {
      return (numericDates) ? '1 ${R.string.min_ago.tr().toLowerCase()}' : R.string.a_minute.tr();
    } else if (difference.inSeconds >= 3) {
      return '1 ${R.string.min_ago.tr().toLowerCase()}';
    } else {
      return R.string.just_now.tr();
    }
  }
}