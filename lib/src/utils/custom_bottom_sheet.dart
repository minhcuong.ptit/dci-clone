import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:imi/src/utils/navigation_utils.dart';

import '../../res/R.dart';

Future<void> showCustomBottomSheet(
    context, List<CustomBottomSheetItem> items) async {
  double bottomInset = MediaQuery.of(context).padding.bottom;
  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          height: (56 * items.length).toDouble(),
          padding: EdgeInsets.symmetric(horizontal: 24.w),
          margin: EdgeInsets.only(bottom: bottomInset),
          child: ListView.separated(
            itemCount: items.length,
            separatorBuilder: (context, index) => Container(
              height: 1.h,
              color: R.color.lightestGray,
            ),
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  items[index].onPress();
                  NavigationUtils.pop(context);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Row(
                    children: [
                      Image.asset(
                        items[index].icon,
                        width: 24.w,
                        height: 24.h,
                      ),
                      SizedBox(
                        width: 16.w,
                      ),
                      Text(
                        items[index].title,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        );
      });
}

class CustomBottomSheetItem {
  final VoidCallback onPress;
  final String icon;
  final String title;

  const CustomBottomSheetItem(this.onPress, this.icon, this.title);
}
