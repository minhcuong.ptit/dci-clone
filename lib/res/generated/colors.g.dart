// GENERATED CODE - DO NOT MODIFY

// ignore_for_file: non_constant_identifier_names

import 'dart:ui';

class Colors {
	Color get primaryColor => Color(0xFF1243E9);
	Color get primaryLightGrey => Color(0xFFF7F6F5);
	Color get primaryBlack => Color(0xFF212529);
	Color get secondaryColor => Color(0xFFABABAB);
	Color get secondaryButtonColor => Color(0xFF0020B4);
	Color get secondaryDarkGrey => Color(0xFFABABAB);
	Color get accentColor => white;
	Color get neutral1 => Color(0xFF344356);
	Color get dropShadow => Color(0xFF6C6F73);
	Color get black => Color(0xFF000000);
	Color get textBlack => Color(0xFF262628);
	Color get zenBackgroundBlack => Color(0xFF101B37);
	Color get zenCircleBackgroundBlack => Color(0xFF1A2572);
	Color get challengeBackgroundLightBlue => Color(0xFF4AB4F6);
	Color get challengeBackgroundBlue => Color(0xFF0D6EFD);
	Color get skyBlue => Color(0xFF4AB4F6);
	Color get chatBackground => Color(0xFFE8F5FF);
	Color get lightBlack => Color(0xFF2F2E2E);
	Color get white => Color(0xFFFFFFFF);
	Color get baseWhite => Color(0xFFFAFAFA);
	Color get yellow => Color(0xFFFFB800);
	Color get goldenYellow => Color(0xFFF9CD46);
	Color get red => Color(0xFFF01010);
	Color get brightRed => Color(0xFFEC4729);
	Color get lightGray => Color(0xFFBBBBBB);
	Color get lighterGray => Color(0xFF8C8C8C);
	Color get lightestGray => Color(0xFFF5F5F5);
	Color get cameraBackGroundGray => Color(0xFFF5F5F5);
	Color get textGray => Color(0xFF555555);
	Color get gray => Color(0xFF7E7E82);
	Color get orange => Color(0xFFEC4829);
	Color get tertiaryOrange => Color(0xFFE2633A);
	Color get orangeLight => Color(0xFFF08910);
	Color get blue => Color(0xFF1A73E8);
	Color get darkerBlue => Color(0xFF2C5BCE);
	Color get blueF6 => Color(0xFF4BB5F6);
	Color get lightBlue => Color(0xFF3AA0FF);
	Color get pink => Color(0xFFFF4264);
	Color get navy => Color(0xFF27099D);
	Color get dark_blue => Color(0xFF000662);
	Color get lightNavy => Color(0xFFECF8FF);
	Color get textNavy => Color(0xFF1A9AE4);
	Color get blueB4 => Color(0xFF0052B4);
	Color get blueEA => Color(0xFF365DEA);
	Color get blueFF => Color(0xFF3AA0FF);
	Color get green => Color(0xFF00AB55);
	Color get greenLight => Color(0xFFE1FEC6);
	Color get grey500 => Color(0xFF9E9E9E);
	Color get grey400 => Color(0xFFBDBDBD);
	Color get grey300 => Color(0xFFE0E0E0);
	Color get grey200 => Color(0xFFEEEEEE);
	Color get grey100 => Color(0xFFF5F5F5);
	Color get grey50 => Color(0xFFFAFAFA);
	Color get grey => Color(0xFF999999);
	Color get disableGray => Color(0xFF9F9F9F);
	Color get backgroundGray => Color(0xFFF9F9F9);
	Color get textTitleGray => Color(0xFF626262);
	Color get hintGray => Color(0xFFC4C4C4);
	Color get backgroundGrey => Color.fromRGBO(239, 239, 243, 1);
	Color get challengeBackgroundGrey => Color(0xFFE9ECEF);
	Color get borderColor => Color(0xFFEEEEEE);
	Color get popupDividerColor => Color.fromRGBO(60, 60, 67, 0.3);
	Color get textRedColor => Color.fromRGBO(236, 64, 64, 1);
	Color get textGreyColor => Color(0xFF9796A1);
	Color get textGrey1 => Color(0xFF8F9195);
	Color get shadowColor => Color(0xFF323F3D3D);
	Color get onlineGreenTextColor => Color.fromRGBO(33, 192, 4, 1);
	Color get addMemberAppBarColor => Color.fromRGBO(246, 246, 246, 1);
	Color get subBrownColor => Color.fromRGBO(142, 142, 147, 1);
	Color get checkedIconColor => Color.fromRGBO(0, 122, 255, 1);
	Color get cyanColor => Color(0xFF00A5A5);
	Color get purpleColor => Color(0xFFD800CD);
	Color get avatarDefaultBackgroundColor => Color(0xFF8B8B8B);
	Color get tabUnselected => Color(0xFFAFAFAF);
	Color get memberRoleHighlighted => Color(0xFFD44C00);
	Color get memberTitle => Color(0xFF292929);
	Color get memberSubtitle => Color(0xFFAEAEB2);
	Color get statusFailed => Color(0xFFED2626);
	Color get backgroundClub => Color(0xFFE6E5E5);
	Color get disabledButtonTitle => Color(0xFFCACACA);
	Color get darkGray => Color(0xFF555555);
	Color get shadesGray => Color(0xFF777777);
	Color get lightShadesGray => Color(0xFFAAAAAA);
	Color get grayBackground => Color(0xFF333333);
	Color get textFieldTitle => Color(0xFF262628);
	Color get btnDisable => Color(0xFFCFCFCF);
	Color get btnVisible => Color(0xFFEC4829);
	Color get indicator => Color(0xFFB5B9CA);
	Color get readMore => Color(0xFF2C5BCE);
	Color get contentGrey => Color(0xFF7F8295);
	Color get newsTitle => Color(0xFF3A3F4D);
	Color get selectedCategory => Color(0xFFC6E3FF);
	Color get follow => Color(0xFFF56F6F);
	Color get categoryBackground => Color(0xFF2C5BCE);
	Color get icHomeBackground => Color(0xFFFE9063);
	Color get pointShadowColor => Color(0xFFE5FFFA);
	Color get greyF4 => Color(0xFFF4F4F4);
	Color get darkGrey => Color(0xFF575656);
	Color get gray600 => Color(0xFF6C757D);
	Color get blueChart => Color(0xFF929FFF);
	Color get periBlue => Color(0xFFB9E0FC);
	Color get zaffre => Color(0xFF0112BC);
	Color get light_grey => Color(0xFFE7E7E7);
}
