// GENERATED CODE - DO NOT MODIFY

// ignore_for_file: non_constant_identifier_names

class Drawables {
	String get background_app => "lib/res/drawables/background_app.png";
	String get background_course => "lib/res/drawables/background_course.png";
	String get background_library => "lib/res/drawables/background_library.png";
	String get bg_category => "lib/res/drawables/bg_category.png";
	String get bg_chat => "lib/res/drawables/bg_chat.png";
	String get bg_group => "lib/res/drawables/bg_group.png";
	String get bg_home_header => "lib/res/drawables/bg_home_header.png";
	String get bg_profile => "lib/res/drawables/bg_profile.png";
	String get bg_sign_up => "lib/res/drawables/bg_sign_up.png";
	String get bg_splash => "lib/res/drawables/bg_splash.png";
	String get bg_welcome => "lib/res/drawables/bg_welcome.png";
	String get bg_with_opacity => "lib/res/drawables/bg_with_opacity.png";
	String get challenge_day_1 => "lib/res/drawables/challenge_day_1.png";
	String get challenge_day_10 => "lib/res/drawables/challenge_day_10.png";
	String get challenge_day_11 => "lib/res/drawables/challenge_day_11.png";
	String get challenge_day_12 => "lib/res/drawables/challenge_day_12.png";
	String get challenge_day_13 => "lib/res/drawables/challenge_day_13.png";
	String get challenge_day_14 => "lib/res/drawables/challenge_day_14.png";
	String get challenge_day_15 => "lib/res/drawables/challenge_day_15.png";
	String get challenge_day_16 => "lib/res/drawables/challenge_day_16.png";
	String get challenge_day_17 => "lib/res/drawables/challenge_day_17.png";
	String get challenge_day_18 => "lib/res/drawables/challenge_day_18.png";
	String get challenge_day_19 => "lib/res/drawables/challenge_day_19.png";
	String get challenge_day_2 => "lib/res/drawables/challenge_day_2.png";
	String get challenge_day_20 => "lib/res/drawables/challenge_day_20.png";
	String get challenge_day_21 => "lib/res/drawables/challenge_day_21.png";
	String get challenge_day_3 => "lib/res/drawables/challenge_day_3.png";
	String get challenge_day_4 => "lib/res/drawables/challenge_day_4.png";
	String get challenge_day_5 => "lib/res/drawables/challenge_day_5.png";
	String get challenge_day_6 => "lib/res/drawables/challenge_day_6.png";
	String get challenge_day_7 => "lib/res/drawables/challenge_day_7.png";
	String get challenge_day_8 => "lib/res/drawables/challenge_day_8.png";
	String get challenge_day_9 => "lib/res/drawables/challenge_day_9.png";
	String get filter_horizontal => "lib/res/drawables/filter_horizontal.png";
	String get ic_account => "lib/res/drawables/ic_account.png";
	String get ic_add => "lib/res/drawables/ic_add.png";
	String get ic_add_circle => "lib/res/drawables/ic_add_circle.png";
	String get ic_add_member => "lib/res/drawables/ic_add_member.png";
	String get ic_add_member_button => "lib/res/drawables/ic_add_member_button.png";
	String get ic_alepay => "lib/res/drawables/ic_alepay.png";
	String get ic_alert_circle => "lib/res/drawables/ic_alert_circle.png";
	String get ic_alert_infor => "lib/res/drawables/ic_alert_infor.png";
	String get ic_approve => "lib/res/drawables/ic_approve.png";
	String get ic_arrow_down => "lib/res/drawables/ic_arrow_down.png";
	String get ic_arrow_right => "lib/res/drawables/ic_arrow_right.png";
	String get ic_attach => "lib/res/drawables/ic_attach.png";
	String get ic_attachment => "lib/res/drawables/ic_attachment.png";
	String get ic_avatar => "lib/res/drawables/ic_avatar.png";
	String get ic_avatar_female => "lib/res/drawables/ic_avatar_female.png";
	String get ic_avatar_male => "lib/res/drawables/ic_avatar_male.png";
	String get ic_back => "lib/res/drawables/ic_back.png";
	String get ic_background_challenge => "lib/res/drawables/ic_background_challenge.png";
	String get ic_back_white => "lib/res/drawables/ic_back_white.png";
	String get ic_bar_chart => "lib/res/drawables/ic_bar_chart.png";
	String get ic_bell => "lib/res/drawables/ic_bell.png";
	String get ic_block => "lib/res/drawables/ic_block.png";
	String get ic_block_user => "lib/res/drawables/ic_block_user.png";
	String get ic_bookmark => "lib/res/drawables/ic_bookmark.png";
	String get ic_bookmark_off => "lib/res/drawables/ic_bookmark_off.png";
	String get ic_bookmark_on => "lib/res/drawables/ic_bookmark_on.png";
	String get ic_book_of_three_times => "lib/res/drawables/ic_book_of_three_times.png";
	String get ic_bx_time => "lib/res/drawables/ic_bx_time.png";
	String get ic_calendar => "lib/res/drawables/ic_calendar.png";
	String get ic_camera => "lib/res/drawables/ic_camera.png";
	String get ic_camera_profile => "lib/res/drawables/ic_camera_profile.png";
	String get ic_cancel => "lib/res/drawables/ic_cancel.png";
	String get ic_card_person => "lib/res/drawables/ic_card_person.png";
	String get ic_characters => "lib/res/drawables/ic_characters.png";
	String get ic_chat => "lib/res/drawables/ic_chat.png";
	String get ic_checkbox_off => "lib/res/drawables/ic_checkbox_off.png";
	String get ic_checkbox_on => "lib/res/drawables/ic_checkbox_on.png";
	String get ic_checkin => "lib/res/drawables/ic_checkin.png";
	String get ic_checkin_location => "lib/res/drawables/ic_checkin_location.png";
	String get ic_check_box => "lib/res/drawables/ic_check_box.png";
	String get ic_check_circle => "lib/res/drawables/ic_check_circle.png";
	String get ic_check_false => "lib/res/drawables/ic_check_false.png";
	String get ic_check_off => "lib/res/drawables/ic_check_off.png";
	String get ic_check_on => "lib/res/drawables/ic_check_on.png";
	String get ic_check_true => "lib/res/drawables/ic_check_true.png";
	String get ic_chevron_right => "lib/res/drawables/ic_chevron_right.png";
	String get ic_china => "lib/res/drawables/ic_china.png";
	String get ic_clean_account => "lib/res/drawables/ic_clean_account.png";
	String get ic_clean_seed => "lib/res/drawables/ic_clean_seed.png";
	String get ic_clock => "lib/res/drawables/ic_clock.png";
	String get ic_close => "lib/res/drawables/ic_close.png";
	String get ic_close_primary => "lib/res/drawables/ic_close_primary.png";
	String get ic_close_transparent => "lib/res/drawables/ic_close_transparent.png";
	String get ic_club_follower => "lib/res/drawables/ic_club_follower.png";
	String get ic_club_location => "lib/res/drawables/ic_club_location.png";
	String get ic_club_member => "lib/res/drawables/ic_club_member.png";
	String get ic_coffee_meditation => "lib/res/drawables/ic_coffee_meditation.png";
	String get ic_collapse => "lib/res/drawables/ic_collapse.png";
	String get ic_comment => "lib/res/drawables/ic_comment.png";
	String get ic_comment_active => "lib/res/drawables/ic_comment_active.png";
	String get ic_community_checked => "lib/res/drawables/ic_community_checked.png";
	String get ic_copy => "lib/res/drawables/ic_copy.png";
	String get ic_copy_seed => "lib/res/drawables/ic_copy_seed.png";
	String get ic_copy_v2 => "lib/res/drawables/ic_copy_v2.png";
	String get ic_create_message => "lib/res/drawables/ic_create_message.png";
	String get ic_current_location => "lib/res/drawables/ic_current_location.png";
	String get ic_DCI => "lib/res/drawables/ic_DCI.png";
	String get ic_dci_team => "lib/res/drawables/ic_dci_team.png";
	String get ic_DCI_v2 => "lib/res/drawables/ic_DCI_v2.png";
	String get ic_default_avatar => "lib/res/drawables/ic_default_avatar.png";
	String get ic_default_banner => "lib/res/drawables/ic_default_banner.png";
	String get ic_default_item => "lib/res/drawables/ic_default_item.png";
	String get ic_delete => "lib/res/drawables/ic_delete.png";
	String get ic_delete_post => "lib/res/drawables/ic_delete_post.png";
	String get ic_delete_rounded => "lib/res/drawables/ic_delete_rounded.png";
	String get ic_diamond => "lib/res/drawables/ic_diamond.png";
	String get ic_diamond_history => "lib/res/drawables/ic_diamond_history.png";
	String get ic_diamond_point => "lib/res/drawables/ic_diamond_point.png";
	String get ic_discount => "lib/res/drawables/ic_discount.png";
	String get ic_download => "lib/res/drawables/ic_download.png";
	String get ic_draft => "lib/res/drawables/ic_draft.png";
	String get ic_edit => "lib/res/drawables/ic_edit.png";
	String get ic_edit1 => "lib/res/drawables/ic_edit1.png";
	String get ic_edit_post => "lib/res/drawables/ic_edit_post.png";
	String get ic_empathy => "lib/res/drawables/ic_empathy.png";
	String get ic_empty_cart => "lib/res/drawables/ic_empty_cart.png";
	String get ic_english => "lib/res/drawables/ic_english.png";
	String get ic_event => "lib/res/drawables/ic_event.png";
	String get ic_event_detail => "lib/res/drawables/ic_event_detail.png";
	String get ic_event_location => "lib/res/drawables/ic_event_location.png";
	String get ic_expansion => "lib/res/drawables/ic_expansion.png";
	String get ic_explore_off => "lib/res/drawables/ic_explore_off.png";
	String get ic_explore_on => "lib/res/drawables/ic_explore_on.png";
	String get ic_eye_off => "lib/res/drawables/ic_eye_off.png";
	String get ic_eye_on => "lib/res/drawables/ic_eye_on.png";
	String get ic_failed => "lib/res/drawables/ic_failed.png";
	String get ic_failed_circle => "lib/res/drawables/ic_failed_circle.png";
	String get ic_filter => "lib/res/drawables/ic_filter.png";
	String get ic_filter_list => "lib/res/drawables/ic_filter_list.png";
	String get ic_filter_on => "lib/res/drawables/ic_filter_on.png";
	String get ic_flag => "lib/res/drawables/ic_flag.png";
	String get ic_follow => "lib/res/drawables/ic_follow.png";
	String get ic_followed => "lib/res/drawables/ic_followed.png";
	String get ic_following => "lib/res/drawables/ic_following.png";
	String get ic_follow_person => "lib/res/drawables/ic_follow_person.png";
	String get ic_forward => "lib/res/drawables/ic_forward.png";
	String get ic_frame => "lib/res/drawables/ic_frame.png";
	String get ic_fullscreen => "lib/res/drawables/ic_fullscreen.png";
	String get ic_gallery => "lib/res/drawables/ic_gallery.png";
	String get ic_global => "lib/res/drawables/ic_global.png";
	String get ic_google => "lib/res/drawables/ic_google.png";
	String get ic_group => "lib/res/drawables/ic_group.png";
	String get ic_group_person => "lib/res/drawables/ic_group_person.png";
	String get ic_g_point => "lib/res/drawables/ic_g_point.png";
	String get ic_headphones => "lib/res/drawables/ic_headphones.png";
	String get ic_heart => "lib/res/drawables/ic_heart.png";
	String get ic_help => "lib/res/drawables/ic_help.png";
	String get ic_help_zen => "lib/res/drawables/ic_help_zen.png";
	String get ic_home => "lib/res/drawables/ic_home.png";
	String get ic_home_off => "lib/res/drawables/ic_home_off.png";
	String get ic_home_on => "lib/res/drawables/ic_home_on.png";
	String get ic_home_selected => "lib/res/drawables/ic_home_selected.png";
	String get ic_hot => "lib/res/drawables/ic_hot.png";
	String get ic_illustration => "lib/res/drawables/ic_illustration.png";
	String get ic_image => "lib/res/drawables/ic_image.png";
	String get ic_imageIcon => "lib/res/drawables/ic_imageIcon.png";
	String get ic_infinity => "lib/res/drawables/ic_infinity.png";
	String get ic_infinity_small => "lib/res/drawables/ic_infinity_small.png";
	String get ic_instagram_share => "lib/res/drawables/ic_instagram_share.png";
	String get ic_intersect => "lib/res/drawables/ic_intersect.png";
	String get ic_joined_group => "lib/res/drawables/ic_joined_group.png";
	String get ic_leader => "lib/res/drawables/ic_leader.png";
	String get ic_learn => "lib/res/drawables/ic_learn.png";
	String get ic_leave => "lib/res/drawables/ic_leave.png";
	String get ic_like => "lib/res/drawables/ic_like.png";
	String get ic_like_active => "lib/res/drawables/ic_like_active.png";
	String get ic_like_off => "lib/res/drawables/ic_like_off.png";
	String get ic_like_on => "lib/res/drawables/ic_like_on.png";
	String get ic_like_reaction => "lib/res/drawables/ic_like_reaction.png";
	String get ic_link => "lib/res/drawables/ic_link.png";
	String get ic_list => "lib/res/drawables/ic_list.png";
	String get ic_location_event => "lib/res/drawables/ic_location_event.png";
	String get ic_location_post => "lib/res/drawables/ic_location_post.png";
	String get ic_lock => "lib/res/drawables/ic_lock.png";
	String get ic_lock_rounded => "lib/res/drawables/ic_lock_rounded.png";
	String get ic_logo_dci => "lib/res/drawables/ic_logo_dci.png";
	String get ic_logo_dci_66_41 => "lib/res/drawables/ic_logo_dci_66_41.png";
	String get ic_logo_header => "lib/res/drawables/ic_logo_header.png";
	String get ic_mail => "lib/res/drawables/ic_mail.png";
	String get ic_manage_accounts => "lib/res/drawables/ic_manage_accounts.png";
	String get ic_map => "lib/res/drawables/ic_map.png";
	String get ic_map_marker => "lib/res/drawables/ic_map_marker.png";
	String get ic_map_marker_tooltip => "lib/res/drawables/ic_map_marker_tooltip.png";
	String get ic_marketplace => "lib/res/drawables/ic_marketplace.png";
	String get ic_market_off => "lib/res/drawables/ic_market_off.png";
	String get ic_market_on => "lib/res/drawables/ic_market_on.png";
	String get ic_mark_as_read => "lib/res/drawables/ic_mark_as_read.png";
	String get ic_meditation_cafe => "lib/res/drawables/ic_meditation_cafe.png";
	String get ic_member => "lib/res/drawables/ic_member.png";
	String get ic_menu => "lib/res/drawables/ic_menu.png";
	String get ic_messages_empty => "lib/res/drawables/ic_messages_empty.png";
	String get ic_message_off => "lib/res/drawables/ic_message_off.png";
	String get ic_message_on => "lib/res/drawables/ic_message_on.png";
	String get ic_mic => "lib/res/drawables/ic_mic.png";
	String get ic_minus_circle => "lib/res/drawables/ic_minus_circle.png";
	String get ic_more => "lib/res/drawables/ic_more.png";
	String get ic_more_off => "lib/res/drawables/ic_more_off.png";
	String get ic_more_on => "lib/res/drawables/ic_more_on.png";
	String get ic_mute => "lib/res/drawables/ic_mute.png";
	String get ic_new_off => "lib/res/drawables/ic_new_off.png";
	String get ic_new_on => "lib/res/drawables/ic_new_on.png";
	String get ic_notification => "lib/res/drawables/ic_notification.png";
	String get ic_notification_selected => "lib/res/drawables/ic_notification_selected.png";
	String get ic_noti_off => "lib/res/drawables/ic_noti_off.png";
	String get ic_noti_on => "lib/res/drawables/ic_noti_on.png";
	String get ic_noti_white_on => "lib/res/drawables/ic_noti_white_on.png";
	String get ic_no_notification => "lib/res/drawables/ic_no_notification.png";
	String get ic_option => "lib/res/drawables/ic_option.png";
	String get ic_other_share => "lib/res/drawables/ic_other_share.png";
	String get ic_pause => "lib/res/drawables/ic_pause.png";
	String get ic_people => "lib/res/drawables/ic_people.png";
	String get ic_person => "lib/res/drawables/ic_person.png";
	String get ic_person_dci => "lib/res/drawables/ic_person_dci.png";
	String get ic_person_none => "lib/res/drawables/ic_person_none.png";
	String get ic_person_switch => "lib/res/drawables/ic_person_switch.png";
	String get ic_person_waiting => "lib/res/drawables/ic_person_waiting.png";
	String get ic_photo => "lib/res/drawables/ic_photo.png";
	String get ic_pin => "lib/res/drawables/ic_pin.png";
	String get ic_place => "lib/res/drawables/ic_place.png";
	String get ic_place_holder => "lib/res/drawables/ic_place_holder.png";
	String get ic_plan_seed => "lib/res/drawables/ic_plan_seed.png";
	String get ic_plan_tree => "lib/res/drawables/ic_plan_tree.png";
	String get ic_play => "lib/res/drawables/ic_play.png";
	String get ic_post => "lib/res/drawables/ic_post.png";
	String get ic_post_club => "lib/res/drawables/ic_post_club.png";
	String get ic_post_freetext => "lib/res/drawables/ic_post_freetext.png";
	String get ic_post_individual => "lib/res/drawables/ic_post_individual.png";
	String get ic_post_link => "lib/res/drawables/ic_post_link.png";
	String get ic_post_media => "lib/res/drawables/ic_post_media.png";
	String get ic_post_music => "lib/res/drawables/ic_post_music.png";
	String get ic_post_product => "lib/res/drawables/ic_post_product.png";
	String get ic_post_video => "lib/res/drawables/ic_post_video.png";
	String get ic_processing => "lib/res/drawables/ic_processing.png";
	String get ic_product => "lib/res/drawables/ic_product.png";
	String get ic_profile => "lib/res/drawables/ic_profile.png";
	String get ic_profile_association => "lib/res/drawables/ic_profile_association.png";
	String get ic_profile_club => "lib/res/drawables/ic_profile_club.png";
	String get ic_profile_selected => "lib/res/drawables/ic_profile_selected.png";
	String get ic_promoter => "lib/res/drawables/ic_promoter.png";
	String get ic_public => "lib/res/drawables/ic_public.png";
	String get ic_rank_diamond => "lib/res/drawables/ic_rank_diamond.png";
	String get ic_rank_golden => "lib/res/drawables/ic_rank_golden.png";
	String get ic_rank_silver => "lib/res/drawables/ic_rank_silver.png";
	String get ic_read => "lib/res/drawables/ic_read.png";
	String get ic_record => "lib/res/drawables/ic_record.png";
	String get ic_refreshing => "lib/res/drawables/ic_refreshing.png";
	String get ic_reject => "lib/res/drawables/ic_reject.png";
	String get ic_reload => "lib/res/drawables/ic_reload.png";
	String get ic_remove => "lib/res/drawables/ic_remove.png";
	String get ic_reply => "lib/res/drawables/ic_reply.png";
	String get ic_report => "lib/res/drawables/ic_report.png";
	String get ic_report_user => "lib/res/drawables/ic_report_user.png";
	String get ic_report_v3 => "lib/res/drawables/ic_report_v3.png";
	String get ic_rss_feed => "lib/res/drawables/ic_rss_feed.png";
	String get ic_save_file => "lib/res/drawables/ic_save_file.png";
	String get ic_search => "lib/res/drawables/ic_search.png";
	String get ic_search_selected => "lib/res/drawables/ic_search_selected.png";
	String get ic_select => "lib/res/drawables/ic_select.png";
	String get ic_select_rounded => "lib/res/drawables/ic_select_rounded.png";
	String get ic_self_practice => "lib/res/drawables/ic_self_practice.png";
	String get ic_send => "lib/res/drawables/ic_send.png";
	String get ic_setting => "lib/res/drawables/ic_setting.png";
	String get ic_settings => "lib/res/drawables/ic_settings.png";
	String get ic_setting_about => "lib/res/drawables/ic_setting_about.png";
	String get ic_setting_edit_profile => "lib/res/drawables/ic_setting_edit_profile.png";
	String get ic_setting_help => "lib/res/drawables/ic_setting_help.png";
	String get ic_setting_logo => "lib/res/drawables/ic_setting_logo.png";
	String get ic_setting_money => "lib/res/drawables/ic_setting_money.png";
	String get ic_setting_my_association => "lib/res/drawables/ic_setting_my_association.png";
	String get ic_setting_my_club => "lib/res/drawables/ic_setting_my_club.png";
	String get ic_setting_my_membership => "lib/res/drawables/ic_setting_my_membership.png";
	String get ic_setting_my_shop => "lib/res/drawables/ic_setting_my_shop.png";
	String get ic_setting_point_dci => "lib/res/drawables/ic_setting_point_dci.png";
	String get ic_setting_privacy => "lib/res/drawables/ic_setting_privacy.png";
	String get ic_setting_referral => "lib/res/drawables/ic_setting_referral.png";
	String get ic_setting_term => "lib/res/drawables/ic_setting_term.png";
	String get ic_share => "lib/res/drawables/ic_share.png";
	String get ic_share_active => "lib/res/drawables/ic_share_active.png";
	String get ic_share_seed => "lib/res/drawables/ic_share_seed.png";
	String get ic_shopping => "lib/res/drawables/ic_shopping.png";
	String get ic_shopping_cart => "lib/res/drawables/ic_shopping_cart.png";
	String get ic_sign_out => "lib/res/drawables/ic_sign_out.png";
	String get ic_social_club => "lib/res/drawables/ic_social_club.png";
	String get ic_sticker => "lib/res/drawables/ic_sticker.png";
	String get ic_study_groups => "lib/res/drawables/ic_study_groups.png";
	String get ic_switch => "lib/res/drawables/ic_switch.png";
	String get ic_sync_circle => "lib/res/drawables/ic_sync_circle.png";
	String get ic_teacher => "lib/res/drawables/ic_teacher.png";
	String get ic_three_dot_vertical => "lib/res/drawables/ic_three_dot_vertical.png";
	String get ic_tick => "lib/res/drawables/ic_tick.png";
	String get ic_time => "lib/res/drawables/ic_time.png";
	String get ic_transaction_history => "lib/res/drawables/ic_transaction_history.png";
	String get ic_translate => "lib/res/drawables/ic_translate.png";
	String get ic_trash => "lib/res/drawables/ic_trash.png";
	String get ic_trash_bin => "lib/res/drawables/ic_trash_bin.png";
	String get ic_tree_complete => "lib/res/drawables/ic_tree_complete.png";
	String get ic_tree_inprogress => "lib/res/drawables/ic_tree_inprogress.png";
	String get ic_triangle => "lib/res/drawables/ic_triangle.png";
	String get ic_unfollow => "lib/res/drawables/ic_unfollow.png";
	String get ic_un_ckeck_box => "lib/res/drawables/ic_un_ckeck_box.png";
	String get ic_user => "lib/res/drawables/ic_user.png";
	String get ic_users => "lib/res/drawables/ic_users.png";
	String get ic_user_following => "lib/res/drawables/ic_user_following.png";
	String get ic_video => "lib/res/drawables/ic_video.png";
	String get ic_vietnam => "lib/res/drawables/ic_vietnam.png";
	String get ic_view_club => "lib/res/drawables/ic_view_club.png";
	String get ic_voucher => "lib/res/drawables/ic_voucher.png";
	String get ic_web => "lib/res/drawables/ic_web.png";
	String get ic_your_club => "lib/res/drawables/ic_your_club.png";
	String get ic_youtube => "lib/res/drawables/ic_youtube.png";
	String get ic_zalo => "lib/res/drawables/ic_zalo.png";
	String get ic_zen => "lib/res/drawables/ic_zen.png";
	String get ic_zoom => "lib/res/drawables/ic_zoom.png";
	String get members => "lib/res/drawables/members.png";
	String get not_coversation_yet => "lib/res/drawables/not_coversation_yet.png";
	String get receive_point  => "lib/res/drawables/popup.png";
	String get preview_picture => "lib/res/drawables/preview_picture.png";
	String get ic_coupons => "lib/res/drawables/ic_coupons.png";
	String get ic_discount_code => "lib/res/drawables/ic_discount_code.png";
	String get ic_banner_coupons => "lib/res/drawables/ic_banner_coupons.png";
	String get ic_give_point => "lib/res/drawables/ic_give_point.gif";
	String get volume_max => "lib/res/drawables/volume_max.svg";
	String get volume_min => "lib/res/drawables/volume_min.svg";
}
