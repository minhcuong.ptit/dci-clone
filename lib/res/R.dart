// GENERATED CODE - DO NOT MODIFY
import 'generated/colors.g.dart';
import 'generated/drawables.g.dart';
import 'generated/fonts.g.dart';
import 'generated/strings.g.dart';

class R {
  R._();

  static final drawable = Drawables();
  static final string = Strings();
  static final font = Fonts();
  static final color = Colors();
}
