import 'dart:io';

import 'package:alice/alice.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hive/hive.dart';
import 'package:imi/src/data/network/response/category_data.dart';
import 'package:imi/src/data/network/response/community_data.dart';
import 'package:imi/src/utils/app_config.dart' as config;
import 'package:injectable/injectable.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'main.config.dart';
import 'src/app.dart';
import 'src/localization/localization.dart';
import 'src/utils/logger.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    logger.i('${bloc.runtimeType} $change');
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    logger.i(transition);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    logger.i("Error", error, stacktrace);
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

Alice alice = Alice(
    showNotification:
        config.AppConfig.environment == config.Environment.STAGING &&
            Platform.isAndroid);

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
void configureDependencies() {
  $initGetIt(getIt);
}

void main() async {
  await WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  Bloc.observer = SimpleBlocObserver();
  configureDependencies();
  await Firebase.initializeApp();
  HttpOverrides.global = MyHttpOverrides();
  // We're using HiveStore for persistence,
  // so we need to initialize Hive.
  await initHiveForFlutter();
  await Hive.initFlutter();
  Hive.registerAdapter(CommunityDataV2Adapter());
  Hive.registerAdapter(CategoryDataAdapter());
  await Hive.openBox<CommunityDataV2>('communityDataV2');
  await Hive.openBox<CommunityDataV2>('categories');
  if (config.AppConfig.environment == config.Environment.PRODUCTION) {
    // Force disable Crashlytics collection while doing every day development.
    // Temporarily toggle this to true if you want to test crash reporting in your app.
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  } else {
    // Handle Crashlytics enabled status when not in Debug,
    // e.g. allow your users to opt-in to crash reporting.
  }

  // final AuthLink authLink = AuthLink(
  //
  // );
  //
  // final Link link = authLink.concat(httpLink);
  //
  // ValueNotifier<GraphQLClient> client = ValueNotifier(
  //   GraphQLClient(
  //     link: link,
  //     // The default store is the InMemoryStore, which does NOT persist to disk
  //     store: GraphQLCache(store: HiveStore()),
  //   ),
  // );
  // BlocOverrides.runZoned(
  //       () => runApp(Localization.getLocalizationWidget(app: App())),
  //   blocObserver: SimpleBlocObserver(),
  // );
  runApp(Localization.getLocalizationWidget(app: App()));
}
